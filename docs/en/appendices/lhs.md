(latin_hypercube)=
# Latin Hypercube

The Latin Hypercube is a useful sampling method for uncertainty analysis, which allows to reduce the number of samples compared to Monte Carlo methods. It is designed as LHS that means Latin Hypercube Sampling.

The operating principles are:
- to split the variation interval of each parameter in a number n of equiprobable sub-intervals,
- to randomly select a value in each sub-interval for each parameter,
- to build n samples by random permutation in order to obtain only one value of each parameter in each sub-interval.

{numref}`lhs1` provides a schematic representation with two parameters and five samples.

(lhs1)=

:::{figure} /_static/images/lhs1.png
Two-dimensionnal graphic representation of a Latin Hypercube Sampling with two parameters and five samples (n=5). X1 has a normal distribution between A1 and B1, X2 has a uniform distribution between A2 and B2.
:::

To define n equiprobable sub-intervals of each probability density, the image of the distribution function has to be splitted in n regular intervals, then the distribution function has to be inverted.

## Uniform or log-uniform law

In case of an uniform or log-uniform law in the \[A,B\] interval, inversion is :

$$
\begin{cases} X=A+r\left(B-A\right)\\ \ln X=\ln A+r\left(\ln B-\ln A\right) \end{cases}
$$

where r is a randomly generated number in each of the n regular intervals that sample \[0,1\].

A example is provided {numref}`lhs2`, in case of 5 samples and a uniform law between A2 and B2.

(lhs2)=

:::{figure} /_static/images/lhs2.png
Matching between the sampled intervals on one hand, and on the other the probability density and the distribution function. Example with n=5, uniform distribution between A2 and B2.
:::

## Normal or log-normal law

In case of a normal or log-normal law, inversion is more complex and uses the error function defined as :

$$
erf\left(x\right)=\frac{2}{\sqrt{\pi}}\int_{0}^{x}e^{-t^{2}}dt
$$

which is available in informatic libraries.

The distribution function of a normal law, with mean mu  and standard deviation sigma is :

$$
F_{\mu,\sigma}\left(x\right)=\frac{1}{\sigma\sqrt{2\pi}}\int_{-\infty}^{x}e^{-\frac{\left(t-\mu\right)^{2}}{2\sigma^{2}}}=\frac{1}{2}\left[1+erf\left(\frac{x-\mu}{\sigma\sqrt{2}}\right)\right]
$$

where :

$$
x=\mu+\sigma\sqrt{2}\: erf^{-1}\left(2z-1\right)
$$

and z is a random value in each sub-interval of the repartition function image.

The parameters $\mu$ and $\sigma$ are usually not known but can be computed from the bounds A and B of the parameter's variation interval, defined as the 0.001 and 0.999 quantile, so that the formula is:

$$
B=\mu+\sigma\sqrt{2}\: erf^{-1}\left(0,998\right)=\mu+3,09\:\sigma
$$

then, as $\mu=\frac{A+B}{2}$ :

$$
\sigma=\frac{B-A}{2\sqrt{2}\: erf^{-1}\left(0,998\right)}=\frac{B-A}{6,18}
$$

An example is provided {numref}`lhs3`, with 5 samples and a normal distribution between A1 and B1.

(lhs3)=

:::{figure} /_static/images/lhs3.png
Matching between the sampled intervals on one hand, and on the other hand the probability density and the distribution function. Example with n=5, normal distribution between A1 et B1.
:::
