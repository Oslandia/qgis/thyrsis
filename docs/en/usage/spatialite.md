# Spatialite database

This chapter describes the three types of database used by the THYRSIS plugin.

## sites.sqlite database

The **sites.sqlite**  database is defined in the installation {ref}`Preferences<preferences>`  and contains different tables, describing available chemicals to use in simulation, the studied sites and the reference simulations.

### elements_chimiques table

The **elements_chimiques**  table contains the main characteristics of radionuclides : number and atomic mass, specific activity and radioactive decay.

### Sites tables

- **sites** : this table links each site to an integer id.
- **points_interet** : this table contains all interest points for each site, associated with a name and a belonging group.
- **mesures** : this table contains all the measures associated to the interest points, with a date, the measured chemical id, the concentration and its uncertainty, the piezometric level and its uncertainty.
- **forages** : this table contains general characteristics of boreholes (name, type, building date, depth, elevation).
- **lithologies** : this table provides for each borehole the rock type and its USGS code, according to the depth.
- **stratigraphies** : this table provides for each borehole the geological formation and its BRGM code, according to the depth.
- **fracturations** : this table provides for each borehole the fraturated rate, according to the depth.

(ref_simulation_table)=
### Reference simulation tables

Simulations can be stored in the sites.sqlite database (cf. {ref}`Export<export>`, § Template), and reused as starting point to new simulations. Several tables are used to store these informations :

(simulation_table)=
- **simulations** :

This table refers to a site and a hydrogeological model, and defines some parameters as follows:

  - `nombre_de_simulations` is the number of simulations to achieve, greater than 1 in case of uncertainty computation.
  - `coef_maillage` is the ratio between the mesh thickness in the middle of an unsaturated column and the thickness of the first mesh on the top, with defaultvalue 5.
  - `pas_echant_zns`  is the column thickness sampling step, used in case of column thickness grouping. It defines the column thickness of a same group. If thestep is defined to 5 m, groups will be defined as \[0,5\], \[5,10\], \[10,15\], etc. The larger the step, the smaller the number of groups.
  - `rapport_max_permeabilite` is the maximal permeability ratio for a same group of columns, by default equal to 10.
  - `permeabilite_zns_uniforme` is the saturated permeability value used for unsaturated columns in case of non-null value and missing u_permeabilite_zns.nodesat or u_permeabilite_zns.elem.sat file. Default value is null.
  - `type_injection` defines the injection type, with values among '*aucune*' (default) i.e. none, 'flux', 'concentration' or 'mass'.
  - `type_infiltration` defines the infiltration type, with values among '*permanente*' (default) i.e. permanent regime or '*transitoire*' i.e. transient regime.
  - `insature` defines if the unsatured zone has to be modeled. It can take values '*oui*' (that is "yes", default value) or '*non*' i.e no.

- **parametres** :

This table refers to the simulations table, and defines the parameter values of each stored simulation :

  - `nom` is the parameter name, among:
    - WC, cinematic porosity
    - VGA, Alpha van Genuchten ($m^{-1}$)
    - VGN, n van Genuchten
    - VGR, residual saturation
    - VGS, maximal saturation
    - VGE, inlet pression ($m$)
    - WT, total porosity
    - VM, volumic mass (kg/m³)
    - DK, partition coefficient (m³/kg)
    - SL, solubility limit (kg/m³)
    - DLZNS, ZNS longitudinal dispersivity (m)
    - DLZS, ZS longitudinal dispersivity (m)

  - `valeur` is the parameter value if the simulation number is 1,
  - `valeur_min` is the minimal parameter value in case of {ref}`Latin Hypercube <latin_hypercube>` use,
  - `valeur_max` is the maximal parameter value in case of {ref}`Latin Hypercube <latin_hypercube>` use,
  - `loi_de_variation` is the parameter probability law in case of {ref}`Latin Hypercube <latin_hypercube>` use. Available values are 'constant' (default), 'normal', 'lognormal', 'uniform' or 'loguniform'.

VGA, VGN, VGR, VGS, VGE parameters are the coefficients $\alpha$, $n$, $\theta_{r}$, $\theta_{s}$, $h_{e}$ in the Ippisch & Vogel ({ref}`ZNS <zns>`) formulation.

In case of dual-porosity simulation, the parameters listed above are duplicated, with the 2 suffix, and four parameters are added:

    - PL, bound permeability (m/s),
    - ASHAPE, factor corresponding to the average half-distance between two fractures,
    - BSHAPE, factor depending to the fractured geometry (a value of 3 corresponds to regularly spaced orthogonal fractures),
    - CSHAPE, empirical coefficient, usually equal to 0.4.

PL, ASHAPE, BSHAPE et CSHAPE parameters are the coefficients $K_{a}$, $a$, $\beta$ et $\gamma_{w}$ of the {ref}`Dual-porosity<dual-porosity>` model.

- **openfoam_options** :

This table refers to the simulations table and compiles all OPENFOAM options. The default values are appropriate for almost all cases but some modifications can be necessary for some numerically difficult cases. These options are briefly defined below, with the default values in bracket.

  - Algorithmic parameters

    - h_tolerance: tolerance for ZNS "h" solver (1.e-12).
    - h_tolerance_relative: relative tolerance for ZNS "h" solver (1.e-2).
    - h_relaxationFactor: relaxation factor for ZNS stationary "h" solver (1.e-2).
    - h_relaxationFactor_transient: relaxation factor for ZNS transient "h" solver (2.e-1).
    - picard_tolerance: tolerance for the ZNS steady flow Picard's algorithm (1.e-8).
    - picard_maxiter: maximum iteration number for the ZNS steady flow Picard's algorithm (20).
    - picard_tolerance_transient: tolerance for the ZNS transient flow Picard's algorithm (1.e-1).
    - picard_maxiter_transient: maximum iteration number for the ZNS transient flow Picard's algorithm (3).
    - newton_tolerance: tolerance for the ZNS transient Newton's algorithm (1.e-8).
    - newton_maxiter: maximum iteration number for the ZNS transient Newton's algorithm (20).
    - potential_tolerance: tolerance for ZS "potential" solver (1.e-8).
    - potential_tolerance_relative: relative tolerance for ZS transient "potential" solver (1.e-1).
    - potential_residualControl: residual control for ZS stationary "potential" solver (1.e-6).
    - potential_relaxationFactor: relaxation factor for ZS stationary "potential" solver (5.e-1).

  - Time step managing

    - delta_time_init: initial time step value (10).
    - dtfact_decrease: decreasing time step factor if Picard's or Newton's algorithm has not converged (0.8).
    - truncationError: time-scheme truncation error for time step computation (0.01).

  - Properties

    - fluid_density: fluid density \[kg/m3\] (1.e3).
    - dynamic_viscosity: dynamic fluid viscosity \[Pa.s\] (1.e-3).
    - molecular_diffusion: molecular diffusion \[m/s2\] (0).
    - tortuosity: tortuosity \[-\] (1).
    - specific_storage: specific storage \[-\] (0).
    - hwatermin: minimal groundwater thickness for ZS "potential" solver \[m\] (0.1).

- **metis_options** :

This table refers to the simulations table and compiles all METIS options. The default values are appropriate for almost all cases but some modifications can be necessary for some numerically difficult cases. These options are briefly defined below, with the default values in bracket.

  - `niter_gc_zns`: maximal iteration number for the ZNS (unsaturated zone) conjugate gradient (100).
  - `niter_gc_zs`: maximal iteration number for the ZS (saturated zone) conjugate gradient (100).

The following options affect the ZNS flow resolution in permanent regime :

  - saturation_initiale: initial saturation value used for the ZNS flow calculation in permanent regime (0.8),
  - rmult_cormax: multiplier coefficient of the maximum dimension, that defines the maximal variation of hydraulic head (0.1),
  - c_pstep: initial value of the solution splitting (0.1),
  - c_ps_inc: initial value of the increment (0.1),
  - c_ps_tol: tolerance on the parameter stepping: the solution is considered converged when c_pstep is greater than 1 - c_ps_tol (allows to shorten a laborious convergence where we tend towards 1 without ever exactly reaching it) (1.e-5 ),
  - ratio_max_res: comparison of two methods of residue calculation. Allows more precise detection of local convergence in case of residue oscillation (1.e-6),
  - critereres: residue convergence criterion. The calculation is converged when c_pstep equals 1 and redres \< critereres (1.e-5),
  - niterns: iteration number for the ZNS flow calculation in permanent regime (1000).
  - courant: 'oui'/'non' (yes/no). The Courant number is calculated from a saturation front pseudo-velocity. If courant = 'oui', time step is adjusted to constraint the front progression to one element per step. Useful for 1D abrupt front. ('oui').
  - emmagasinement: specific storage ($m^{-1}$), used to handle important leak with the option 'sat_nonsat' = on, assorted to 'courant' = 'oui' (1.e-3).
  - kdecoup_delt_zns: ZNS initial time step (10).
  - kdecoup_dmin_zns: ZNS minimal time step (1).
  - kdecoup_delt_zs: ZS initial time step (100).
  - kdecoup_dmin_zs: ZS minimal time step (1).
  - force_fin_phase: 'oui'/'non' (yes/no), forces the end of phase with a large time step when the time step becomes too small ('oui').
  - max_iter: maximal iteration number for phreatic calculation (200),
  - relax_phreat: relaxation coefficient (weight attributed to the last iteration) (0.5).
  - critere_pot: convergence criterion on potential (0.1 m),
  - phrea_min: minimal groundwater thickness (1.0 m).

(injection_table)=

- **injections** :

This table refers to the simulations table, and defines injection characteristics:

  - profondeur: injection depth (m)
  - debut: injection starting date, DD/MM/YYYY format
  - duree: injection duration, in 'day', 'month' or 'year' unit
  - nom: injection name
  - volume_eau: water volume injected, en m³
  - coefficient_surface: ratio between the real injection area and the area of the corresponding nodes (case of borehole injection, where the real injection surface is limited to the borehole surface, which can be much smaller than the influence surface of the nodes) ;
  - coefficient_injection: ratio between the real quantity injected and the maximal quantity that it is possible to inject, according to the solubility (this allows to consider only a part of the whole mass, for an injection duration calculated from the total mass - only useful in case of mass injection. In case of concentration or flux injection, it is only a multiplier of the concentration or flux) ;
  - permeabilite: ZNS permeability value, automatically computed from the permeability field of the saturated zone, or from a u_permeabilite_zns.node.sat or u_permeabilite_zns.elem.sat file if existing, or equal to the permeabilite_zns_uniforme value of the {ref}`simulations<simulation_table>` table, if not null.
  - infiltration: infiltration value on top of the unsaturated column, automatically computed from the hydrogeological model.
  - epaisseur: ZNS column thickness, automatically computed from the hydrogeological model.
  - altitude: ZNS column top altitude, automatically computed from the hydrogeological model.

- **dates_resultats** :

This table refers to simulations table and defines simulation output dates by providing as many times as necessary:

  - duree: duration, in 'day', 'month' or 'year' unit,
  - pas_de_temps: time step, which sample the duration, in 'day', 'month' or 'year' unit.

## Model databases

Hydrogeological models are saved in independant spatialite databases, located in the models folder defined in the installation {ref}`Preferences<preferences>`. Each database contains the tables defining the 2D mesh, the hydrogeological model characteristics and the paramaters used for inversion process, if appropriate.

### Mesh tables

The following tables are used to define and build the mesh. Only **contours** and **points_fixes** tables are manually edited by the user. The other tables are automatically created and are secondary.

- bords: secondary table used for the mesh building process,

- **contours** :

This table contains the definition of the contour elements used for the domain, with the following fields :

  - groupe: defines a contour class (site or river for example), but this field is not currently used,
  - nom: is the name of the contour element,
  - longueur_element: is the distance (in meters) between two nodes placed along the element (constrains the mesh size),
  - potentiel_impose: is equal to 1 if the contour element has imposed potential, otherwise equal to NULL or 0.

  - domaines: secondary table used to describe the domains to mesh, according to the contours,

  - extremites: secondary table containing the endpoints of the contour elements,

- **points_fixes** :

Table containing the points to insert into the mesh :

  - nom: fixed point name,
  - groupe: group name of the fixed point,
  - longueur_element: distance, in meter, between two nodes near to the fixed point.

:::{figure} /_static/images/table_contours.png
Contours table example
:::

:::{figure} /_static/images/table_points_fixes.png
Fixed points table example
:::

### Inversion tables

The following tables are used to definie the inversion process parameters, the OPENFOAM and METIS options, and the pilot points used for the inversion (cf. {ref}`Inversion<inversion>`) :

- **hynverse_parametres** : inversion parameters (cf. {ref}`Inversion<inversion>`),

- **openfoam_hynverse_options** : OPENFOAM options :

  - fluid_density: fluid density (1.e3).
  - dynamic_viscosity: dynamic fluid viscosity (1.e-3).
  - potential_tolerance: tolerance for ZS "potential" solver (1.e-8).
  - potential_tolerance_relative: relative tolerance for ZS "potential" solver (0.1).
  - potential_residualControl: residual control for ZS "potential" solver (1.e-6).
  - potential_relaxationFactor: relaxation factor for ZS "potential" solver (0.5).
  - hwatermin: minimal groundwater thickness (0.1).

- **metis_hynverse_options** : METIS options :

  - niter_gc_zs: maximal iteration number for the ZS (saturated zone) conjugate gradient (100),
  - max_iter: maximal iteration number for phreatic calculation (200),
  - relax_phreat: relaxation coefficient (weight attributed to the last iteration) (0.5).
  - critere_pot: convergence criterion on potential (0.1 m),
  - phrea_min: minimal groundwater thickness (1.0 m).

- **points_pilote** : pilot points used for the inversion process :

  - nom : pilot point name,
  - groupe : pilot point group, used for correlation graphic,
  - zone : pilot point zone,
  - altitude_piezo_mesuree : measured groundwater altitude (m),
  - altitude_piezo_calculee : computed groundwater altitude (m),
  - difference calcul mesure : difference between measured and computed groundwater altitude (m)

- **zones** : zone permeability, and min/max permeability values tolerated during inversion process.

### Model tables

The following tables provide hydrogeological model's meshes and nodes characteristics.

- **mailles** :

This table defines each mesh element by an ordered list of three (triangle) or four (quadrangle) node numbers, and provides for each element the following model characteristics :

  - surface: mesh surface ($m^{2}$),
  - altitude: topographic altitude (m),
  - altitude_mur: substratum altitude (m),
  - infiltration: infiltration value (m/s),
  - epaisseur_aquifere: aquifer thickness (m),
  - potentiel_reference: reference piezometric value (m),
  - potentiel: piezometric value (m),
  - flux_eau: seepage term (m/s),
  - epaisseur_zs: groundwater thickness (saturated zone) (m),
  - epaisseur_zns: unsaturated zone thickness (m),
  - permeabilite_zns: permeability of the unsaturated zone (m/s),
  - permeabilite_x: permeability on x axis (m/s),
  - permeabilite_y: permeability on y axis (m/s),
  - permeabilite_z: permeability on z axis (m/s) (unused),
  - v_x: Darcy velocity on x axis (m/s),
  - v_y: Darcy velocity on y axis (m/s),
  - v_z: Darcy velocity on z axis (m/s) (unused),
  - v_norme: Darcy velocity norm (m/s),

- **noeuds** : this table contains the same model characteristics as the table mailles, but computed at each node.

- **noeuds_contour** : this table contains the id of the contour nodes.

- **parametres** : this table contains the hydrogeolocial model parameters (cf. {ref}`Reference simulation tables<ref_simulation_table>`).

- **potentiel_impose** : this table contains the id of the nodes with an imposed potential, with the imposed value.

----

## Calculation project

For each computation project, a new computation database is created as follows :

- by copying the model database specified as input (from the mesh folder),
- by creating the sites.sqlite tables and inserting the entries corresponding to the site,
- by creating the results tables.

The new computation database contains the following tables:

- from the model database :

  - bords
  - contours
  - domaines
  - extremites
  - hynverse_parametres
  - mailles
  - metis_hynverse_options
  - noeuds
  - noeuds_contour
  - openfoam_hynverse_options
  - parametres
  - points_fixes
  - points_pilote
  - potentiel_impose
  - zones

- from the sites database for the considered site :

  - dates_resultats
  - elements_chimiques
  - forages
  - fracturations
  - injections
  - lithologies
  - mesures
  - metis_options
  - openfoam_options
  - points_interet
  - simulations
  - sites
  - stratigraphies

- with results tables creation :

  - bilan_masse
  - dates_simulation
  - isovaleur
  - mailles_zns
  - noeuds_zns
  - noeuds_injections
  - parametres_simulation
