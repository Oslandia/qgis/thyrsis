import string
import sys
from builtins import str
from subprocess import Popen

"""
convert xyz text files to tif

USAGE

    python -m thyrsis.utilities.xyz_to_tif file.xyz [EPSG=2154]

    must be run in the current directory to create file.tif
"""

if __name__ == "__main__":
    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    fil = ""
    epsg = "2154"
    for arg in sys.argv[1:]:
        if arg[:4] == "EPSG":
            epsg = arg.split("=")[1]
        elif arg[-4:] == ".xyz":
            fil = sys.argv[1][:-4]
    assert fil

    print("converting ", fil + ".xyz", " to TIF file with projection EPSG:" + epsg)

    vrt = (
        """<OGRVRTDataSource>
    <OGRVRTLayer name="$fil">
    <SrcDataSource>$fil.csv</SrcDataSource>
    <GeometryType>wkbPoint</GeometryType>
    <LayerSRS>EPSG:"""
        + epsg
        + """</LayerSRS>
    <GeometryField encoding="PointFromColumns" x="field_1" y="field_2" z="field_3"/>
    </OGRVRTLayer>
    </OGRVRTDataSource>
    """
    )

    x = []
    y = []
    with open(fil + ".xyz") as i, open(fil + ".csv", "w") as o:
        print("conversion de ", fil + ".xyz", " au format csv")
        for line in i:
            spl = line.split()
            x.append(float(spl[0]))
            y.append(float(spl[1]))
            o.write(",".join(spl) + "\n")

    nx = len(sorted(set(x))) - 1
    ny = len(sorted(set(y))) - 1

    open(fil + ".vrt", "w").write(string.Template(vrt).substitute({"fil": fil}))

    # nx = 3000
    # ny = 3000
    print("nx = ", nx, " ny = ", ny)

    print("gdal_grid => " + fil + ".tif")
    Popen(
        [
            "gdal_grid",
            "-zfield",
            "field_3",
            "-a",
            "nearest",
            "-l",
            fil,
            "-outsize",
            str(nx),
            str(ny),
            fil + ".vrt",
            fil + ".tif",
        ]
    ).communicate()

    # print "gdaldem => hillshade.tif"
    # Popen(['gdaldem', 'hillshade', fil+'.tif', 'hillshade.tif']).communicate()
