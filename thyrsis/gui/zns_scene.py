from builtins import range

import numpy
from qgis.PyQt.QtCore import QSize
from qgis.PyQt.QtGui import QImage, QPixmap
from qgis.PyQt.QtWidgets import QGraphicsScene

from ..log import logger
from ..meshlayer.glmesh import ColorLegend, GlMesh
from ..utilities import Timer
from ..zns_data_provider import ZnsDataProvider

# For max performance, all zns columns are drawn at once
# the nodes are duplicated to draw a quad for each element


class ZnsScene(QGraphicsScene):
    """Scene displaying unsaturated column"""

    def __init__(self, uri, parent=None):
        """Constructor

        :param uri: source
        :type uri: string
        :param parent: Widget parent
        :type parent: QWidget
        """
        super(ZnsScene, self).__init__(parent)
        self.__glMesh = None
        self.__timing = False
        self.__autoUpdateMinMax = False

        self.__barWidth = 25
        self.__barSpacing = 5

        self.__dataProvider = ZnsDataProvider(uri, self)
        self.__settings = self.__dataProvider.settings()
        self.__dataProvider.dataChanged.connect(self.__refresh)

        self.__labels = self.__dataProvider.labels()
        if len(self.__labels) > int(self.__settings.value("General", "nbColumnsMax")):
            self.__empty = True
            return
        self.__legendPlacement = "bottom"

        self.__dataProvider.unitsChanged.connect(self.__updateMinMax)
        self.__dataProvider.unitsChanged.connect(self.__refresh)
        self.__dataProvider.columnChanged.connect(self.__columnChanged)

        vtx = self.__dataProvider.nodeCoord()
        if not len(vtx):
            self.__empty = True
            return
        self.__empty = False

        self.__legend = ColorLegend()
        self.__legend.setParent(self)
        self.__legend.setTitle(self.__dataProvider.resultColumn())
        self.__legend.setUnits(self.__dataProvider.units())
        self.__legend.setLogScale(True)
        self.__legend.maskUnits(
            int(self.__settings.value("Variables", "maskUnitsCheck"))
        )

        self.__zmin = numpy.min(vtx[:, 2])
        self.__zmax = numpy.max(vtx[:, 2])
        bars = self.__dataProvider.bars()
        # now we reposition the bars setting their y to z, the z to zero and the x to pid
        for i, bar in enumerate(bars):
            vtx[bar[:], 0] = i
            vtx[bar[:], 1] = vtx[bar[:], 2]
            vtx[bar[:], 2] = 0
        # and we interweave offset vtx
        offsetVtx = numpy.copy(vtx)
        offsetVtx[:, 0] += float(self.__barWidth) / (
            self.__barSpacing + self.__barWidth
        )
        # vtx = numpy.vstack((vtx, offsetVtx)).reshape((-1,3), order='F')
        tmp = numpy.empty((2 * len(vtx), 3), dtype=vtx.dtype)
        tmp[0::2] = vtx
        tmp[1::2] = offsetVtx
        vtx = tmp

        # we create triangles for bars
        idx = numpy.array([], numpy.int32)
        for i, bar in enumerate(bars):
            # for each bar we have 2 triangles, so we interweave
            barIdx = numpy.vstack(
                (
                    2 * bar[:-1:2],
                    2 * bar[1::2],
                    2 * bar[:-1:2] + 1,
                    2 * bar[1::2],
                    2 * bar[1::2] + 1,
                    2 * bar[:-1:2] + 1,
                )
            ).reshape((-1,), order="F")
            idx = numpy.append(idx, barIdx)

        assert numpy.max(idx) < len(vtx)

        self.__extent = (0, self.__zmin, len(bars), self.__zmax)

        self.__imgSize = QSize((self.__barWidth + self.__barSpacing) * len(bars), 256)
        self.__legend.symbologyChanged.connect(self.__refresh)
        self.__glMesh = GlMesh(vtx, idx, self.__legend)
        self.__glMesh.setParent(self)

        self.__updateMinMax()
        self.__refresh()

    def maskUnits(self, flag):
        """Hide/show units

        :param flag: flag to mask or not units
        :type flag: bool
        """
        self.__legend.maskUnits(flag)
        self.__refresh()

    def setAutoUpdateMinMax(self, flag):
        """Set automatic scale update

        :param flag: flag to mask or not units
        :type flag: bool
        """
        self.__autoUpdateMinMax = flag
        self.__updateMinMax()
        self.__refresh()

    def __columnChanged(self, column):
        """Triggered when columns data changes

        :param column: column name
        :type column: string
        """
        self.__legend.setTitle(column)
        self.__legend.setLogScale(column in ["concentration", "vitesse_darcy_norme"])
        self.__updateMinMax()
        self.__refresh()

    def __updateMinMax(self, scratch=None):
        """update the min and max scale value

        :param scratch: unused
        :type scratch: None
        """
        column = self.__dataProvider.resultColumn()
        index = self.__dataProvider.date() if self.__autoUpdateMinMax else None
        max_ = self.__dataProvider.maxValue(index) or 0
        if "concentration" == column or "activity" == column:
            min_ = max_ * float(
                self.__settings.value("Variables", "concentrationScale")
            )
            logscale = True
        elif "vitesse_darcy_norme" == column:
            min_ = max_ * float(self.__settings.value("Variables", "darcyScale"))
            logscale = True
        else:
            min_ = self.__dataProvider.minValue(index) or 0
            logscale = False

        self.__legend.blockSignals(True)
        self.__legend.setUnits(self.__dataProvider.units())
        self.__legend.setMinValue(min_)
        self.__legend.setMaxValue(max_)
        self.__legend.setLogScale(logscale)
        self.__legend.blockSignals(False)

    def empty(self):
        """Get empty attributes

        :return: empty/not empty ZNS state
        :rtype: bool
        """
        return self.__empty

    def colorLegend(self):
        """Get legend attributes

        :return:
        :rtype:
        """
        return self.__legend

    def glMesh(self):
        """Get glMesh attributes

        :return:
        :rtype:
        """
        return self.__glMesh

    def dataProvider(self):
        """Get dataProvider attributes

        :return:
        :rtype:
        """
        return self.__dataProvider

    def __refresh(self):
        """Refresh the ZNS display"""
        timer = Timer() if self.__timing else None
        if self.empty():
            return

        if self.__autoUpdateMinMax:
            self.__updateMinMax()

        img = QImage()
        values = self.__dataProvider.nodeValues()
        values = numpy.vstack((values, values)).reshape((-1,), order="F")
        if (self.__extent[2] - self.__extent[0]) > 0:
            img = self.__glMesh.image(
                values,
                self.__imgSize,
                (
                    0.5 * (self.__extent[0] + self.__extent[2]),
                    0.5 * (self.__extent[1] + self.__extent[3]),
                ),
                (
                    float(self.__extent[2] - self.__extent[0]) / self.__imgSize.width(),
                    (self.__zmax - self.__zmin) / self.__imgSize.height(),
                ),
            )

        self.clear()
        self.addPixmap(QPixmap.fromImage(img))

        # add labels
        for i, name in enumerate(self.__labels):
            text = self.addText(name)
            text.setPos(i * (self.__barWidth + self.__barSpacing), 0)
            text.setRotation(-90)

        # add legend for altitude
        offsetY = -60
        self.addLine(-self.__barSpacing, 0, -self.__barSpacing, self.__imgSize.height())
        text = self.addText("Altitude [m]")
        nbTicks = 10
        tickSpacing = float(self.__imgSize.height()) / (nbTicks - 1)
        tickTextOrigin = -text.sceneBoundingRect().height() / 2
        text.setPos(-100, tickTextOrigin - int(tickSpacing))
        tickLength = 10
        for t in range(nbTicks):
            altitude = self.__zmax - t * (self.__zmax - self.__zmin) / (nbTicks - 1)
            text = self.addText("%4.0f" % (altitude))
            text.setPos(offsetY, tickTextOrigin + int(t * tickSpacing))
            tick = self.addLine(
                -self.__barSpacing - tickLength,
                int(t * tickSpacing),
                -self.__barSpacing,
                int(t * tickSpacing),
            )

        legendGroup = self.__legend.createItems()
        # legend = self.addPixmap(QPixmap.fromImage(self.__legend.image()))
        if self.__legendPlacement == "right":
            legendGroup.setPos(
                self.__imgSize.width() + 25, tickTextOrigin - int(tickSpacing) + 3
            )
        elif self.__legendPlacement == "bottom":
            legendGroup.setPos(-100, self.__imgSize.height() + 10)
        elif self.__legendPlacement == "left":
            legendGroup.setPos(-250, tickTextOrigin - int(tickSpacing))

        else:
            raise RuntimeError(
                "Legend placement %s not handled" % (self.__legendPlacement)
            )
        self.addItem(legendGroup)
        if self.__timing:
            logger.debug(timer.reset("draw zns"))

    def set_legend_placement(self, placement):
        """Set the legend position and refresh display

        :param placement: position of the legend ('right'/'left'/'bottom')
        :type placement: string
        """
        self.__legendPlacement = placement
        self.__refresh()
