import os
import re
import subprocess
import sys
import time
from builtins import object, range
from distutils.spawn import find_executable
from math import exp as exp_
from math import log, sqrt

import numpy

try:
    from mpi4py import MPI
except Exception:
    import site

    from thyrsis.__about__ import DIR_PLUGIN_ROOT

    site.addsitedir(DIR_PLUGIN_ROOT / "embedded_external_libs")
    from thyrsis.embedded_external_libs.mpi4py import MPI

from ..log import logger


def check_float(s):
    """checks if a string is a float and returns the float, else returns 0

    :param s: value to check
    :type s: undetermined

    :return: converted value or 0
    :rtype: float
    """
    try:
        return float(s)
    except ValueError:
        return 0


def format_(min_, max_):
    """Define format (scientific or normal)

    :param min_: interval min value
    :type min_: float
    :param max_: interval max value
    :type max_: float

    :return: format
    :type: string
    """
    format_ = "%.2e"
    if max_ < 10000 and abs(min_) >= 0.1:
        format_ = "%.2f"
    return format_


def parse_file(file_name, nb_values_per_elem, nb_of_elem):
    """parse file that contains a list of space separated value
    returns a list of tuples with the line number in last position

    :param file_name: path of the project folder
    :type file_name: string
    :param nb_values_per_elem: number of value for a element, used to parse file
    :type nb_values_per_elem: integer
    :param nb_of_elem: number of element, used to parse file
    :type nb_of_elem: integer

    :return: list of tuples with the project data
    :rtype: list
    """
    logger.debug("convert " + file_name + "... ")
    data = []
    tup = []
    with open(file_name) as fil:
        for line in fil.readlines():
            if line[0] == "#":  # remove comments
                continue
            for val in line.split():
                tup.append(float(val))
                if len(tup) == nb_values_per_elem:
                    data.append(tuple(tup + [len(data) + 1]))
                    tup = []
    assert len(data) == nb_of_elem
    return data


def read_one_column_file(filename, exclude=None):
    """reads a one-column file and returns the values as list, excluding void lines

    :param filename: path of the file to read
    :type filename: string
    :param exclude: list of excluded columns
    :type exclude: list

    :return: column values
    :rtype: list
    """

    values = []
    with open(filename) as fil:
        for line in fil.readlines():
            test = (
                (line[min(len(line), len(exclude)) - 1] != exclude) if exclude else True
            )
            if line.split() != [] and test:
                values.append(float(line.split()[0]))

    return values


def convert_nodes(t):
    """Convert an array (dates, mesh element) into an array (dates, nodes) for 1D unsaturated columns

    :param t: mesh elements array
    :type t: numpy.ndarray

    :return: nodes array
    :rtype: numpy.ndarray
    """
    import numpy

    nx = t.shape[0]
    ny = t.shape[1]
    a = numpy.zeros((nx, ny + 1))
    b = a.copy()

    for i in range(nx):
        a[i][:ny] = t[i]
        a[i][ny] = t[i][ny - 1]
        b[i][1:] = t[i]
        b[i][0] = t[i][0]

    return 0.5 * (a + b)


def toSeconds(duree):
    """Convert day/month/year duration in seconds

    :param duree: duration
    :type duree: string

    :return: duration in seconds
    :rtype: float
    """

    value = float(duree.split()[0])
    unit = duree.split()[1]

    if unit[:6] == "second":
        return value
    elif unit[:6] == "minute":
        return value * 60
    elif unit[:4] == "hour":
        return value * 3600
    elif unit[:3] == "day":
        return value * 24 * 3600
    elif unit[:5] == "month":
        return value * 24 * 3600 * 365.25 / 12.0
    elif unit[:4] == "year":
        return value * 24 * 3600 * 365.25
    else:
        raise RuntimeError("toSeconds: unit not recognized")


def number_lines(filename, strg=""):
    """Return the number of line per strg. By default, return the total lines in the file

    :param filename: path of the file to read
    :type filename: string
    :param strg: delimiter
    :type strg: string

    :return: number of line
    :rtype: integer
    """
    fil = open(filename, "r")
    n = 0
    for line in fil:
        if line[: len(strg)] == strg and line.split() != []:
            n += 1
    fil.close()
    return n


def complete_filename(name):
    """Return the absolute path of a file

    :param name: file name
    :type name: string

    :return: absolute path
    :type: string
    """
    return os.path.join(os.path.dirname(__file__), "..", name)


def complete_image_name(name):
    """Return the absolute path of a file in images subfolder

    :param name: file name
    :type name: string

    :return: absolute path
    :type: string
    """
    return os.path.join(os.path.dirname(__file__), "..", "images", name)


def unitConversionFactor(sourceUnit, targetUnit, specificActivity=0.0):
    """Return conversion factor between two units

    :param sourceUnit: sourceUnit
    :type sourceUnit: string
    :param targetUnit: targetUnit
    :type targetUnit: string
    :param specificActivity: specificActivity
    :type specificActivity: float

    :return: factor
    :rtype: float
    """
    if sourceUnit == targetUnit:
        return 1  # takes care of the adimentionnal case
    unitsConversionFactors = {
        "kg/m³": {
            "kg/L": 1e-3,
            "g/L": 1.0,
            "mg/L": 1e3,
            "µg/L": 1e6,
            "kg/m³": 1.0,
            "g/m³": 1e3,
            "mg/m³": 1e6,
            "µg/m³": 1e9,
            "GBq/L": specificActivity * 1e-9 * 1e-3,
            "MBq/L": specificActivity * 1e-6 * 1e-3,
            "kBq/L": specificActivity * 1e-3 * 1e-3,
            "Bq/L": specificActivity * 1e-3,
            "mBq/L": specificActivity * 1e3 * 1e-3,
            "µBq/L": specificActivity * 1e6 * 1e-3,
            "GBq/m³": specificActivity * 1e-9,
            "MBq/m³": specificActivity * 1e-6,
            "kBq/m³": specificActivity * 1e-3,
            "Bq/m³": specificActivity,
            "mBq/m³": specificActivity * 1e3,
            "µBq/m³": specificActivity * 1e6,
        },
        "Bq/kg": {
            "Bq/kg": 1.0,
            "Bq/g": 1e-3,
            "Bq/mg": 1.0e-6,
            "Bq/µg": 1.0e-9,
            "µBq/kg": 1e6,
            "mBq/kg": 1e3,
            "kBq/kg": 1e-3,
            "MBq/kg": 1e-6,
            "GBq/kg": 1.0e-9,
        },
        "m/s": {"m/s": 1.0, "m/an": 365.25 * 24 * 3600, "cm/j": 100.0 * 24 * 3600},
        "m": {"m": 1.0},
        "-": {"-": 1.0},
    }
    if sourceUnit not in unitsConversionFactors:
        for k, v in unitsConversionFactors.items():
            if sourceUnit in v:
                inverse_factor = 1.0 / v[sourceUnit]
                return inverse_factor * unitsConversionFactors[k][targetUnit]

    return unitsConversionFactors[sourceUnit][targetUnit]


class Timer(object):
    """Timer class"""

    def __init__(self):
        """Constructor"""
        self.start = time.time()

    def reset(self, text=""):
        """Reset start after displaying the duration since the last reset

        :param text: text to display
        :type text: string

        :return: time to display
        :rtype: string
        """
        s = self.start
        self.start = time.time()
        return "%30s % 8.4f sec" % (text, (self.start - s))


def suite_geom(height, min_length, max_length, coef):
    q = (height - min_length) / (height - max_length)
    nm = int(height / min_length) if q == 1.0 else 1 + int(log(coef) / log(q))
    eps = float(nm - 1) * log(q) - log(coef)
    if q != 1.0:
        eps = eps / (1.0 - float(nm - 1) * max_length / (height - max_length))
    r = coef * (1.0 + eps)
    a = log(r) / float(nm - 1)
    return nm, a


def zns_nodes(position, __height, coef, vgamax):
    """Return a list of nodes (x,y,z,s) of decreasing altitude for the zns
    s = surface du noeud

    :param position: (x,y,z) of the column
    :type position: tuple
    :param height: of the colum
    :type height: float
    :param coef: max/min interval length
    :type coef: float
    :param vgamax: max value of Van Genuchten alpha parameter
    :type coef: float

    :return: list of nodes (x,y,z,s) of decreasing altitude for the zns
    :rtype: list
    """

    height = max(__height, 1e-1)
    logger.notice("zns_nodes: height = ", height, __height)
    min_length_top = 1e-2
    min_length_bottom = min(1e-2, 0.1 / vgamax)
    max_length = min(
        max(min_length_top, min_length_bottom) * coef, 0.1 / vgamax, 0.1 * height
    )
    coef_top = max_length / min_length_top
    coef_bottom = max_length / min_length_bottom
    half_height = 0.5 * height

    nm_top, a_top = suite_geom(half_height, min_length_top, max_length, coef_top)
    nm_bottom, a_bottom = suite_geom(
        half_height, min_length_bottom, max_length, coef_bottom
    )

    nm = nm_top + nm_bottom
    nodes = [() for i in range(nm + 1)]
    z = [0] * (nm + 1)
    z[0] = position[2]
    z[-1] = position[2] - height
    # logger.debug("top", min_length_top, a_top, nm_top, z[0])
    # logger.debug("bottom", min_length_bottom, a_bottom, nm_bottom, z[-1])
    logger.debug(
        "zns_nodes: nm_top =", nm_top, "nm_bottom =", nm_bottom, "nm_total =", nm
    )

    ei = 0.0
    for i in range(nm_top):
        aux = min_length_top * exp_(float(i) * a_top)
        ei = ei - aux
        if i == nm_top - 1:
            ei = -half_height
        elif abs(ei) >= half_height:
            ei = 0.5 * ((ei + aux) - half_height)
        z[i + 1] = position[2] + ei
        # logger.debug("t %d %10.5f%10.5f%15.5f"%(i, aux, ei, z[i+1]))

    ei = 0.0
    for i in range(nm_bottom - 1):
        aux = min_length_bottom * exp_(float(i) * a_bottom)
        ei = ei - aux
        if abs(ei) >= half_height:
            ei = 0.5 * ((ei + aux) - half_height)
        z[-i - 2] = position[2] - height - ei
        # logger.debug("b %d %10.5f%10.5f%15.5f"%(i, aux, ei, z[-i-2]))

    logger.debug(
        "zns_nodes: e_top = %8.5f, e_half = %8.5f, e_bottom = %8.5f"
        % (z[0] - z[1], z[nm_top - 1] - z[nm_top], z[-2] - z[-1])
    )
    nodes[0] = (position[0], position[1], z[0], 0.5 * (z[0] - z[1]))
    nodes[-1] = (position[0], position[1], z[-1], 0.5 * (z[-2] - z[-1]))
    for i in range(1, nm):
        nodes[i] = (position[0], position[1], z[i], 0.5 * (z[i - 1] - z[i + 1]))

    return nodes


def cleanup_dirname(directory):
    """Return a clean dirname

    :param name: folder path
    :type name: string

    :return: folder path
    :rtype: string
    """
    "remove trailing / or \\"
    if not os.path.split(directory)[1]:
        directory = os.path.split(directory)[0]


def read_site_param(filename):
    """return a dict of parameters

    :param filename: file name
    :type filename: string

    :return: dict parameters
    :rtype: dict
    """
    param = {}
    with open(filename) as fil:
        site_name = next(fil)  # 1
        if len(site_name.split()) > 1:  # not the site name, but offset
            param["offset"] = [float(o) for o in site_name.split()[:2]]  # 2
        else:
            param["offset"] = [float(o) for o in fil.next().split()[:2]]  # 2
        param["zoom1_lower_left_corner"] = fil.next().split()[:2]  # 3
        param["zoom1_upper_right_corner"] = fil.next().split()[:2]  # 4
        param["zoom2_lower_left_corner"] = fil.next().split()[:2]  # 5
        param["zoom2_upper_right_corner"] = fil.next().split()[:2]  # 6
        param["domain_surface"] = float(fil.next().split()[0])  # 7
        param["infiltration_mm_per_year"] = float(fil.next().split()[0])  # 8
        param["rho_kg_per_m3"] = float(fil.next().split()[0])  # 9
        param["porosity_matrix"] = bool(int(fil.next().split()[0]))  # 10
    return param


def interpolationWeightsQuad(point, element):
    """Interpolation weight in quad mesh, not configured

    :param point: point xy
    :type point: tuple
    :param element: mesh element
    :type element: list of vertices coord

    :return: list of weight
    :rtype: list
    """
    # we take the average
    # return [.25, .25, .25, .25]

    #    # from https://www.particleincell.com/2012/quad-interpolation/
    #    #
    #    # assume bilinear mapping functions
    #    # x(r,s) = a_1 + a_2 r + a_3 s + a_4 r s (1)
    #    # y(r,s) = b_1 + b_2 r + b_3 s + b_4 r s (2)
    #    # for the four corners element:
    #    # (r,s) = (0,0), (1,0), (1,1) and (0,1)
    #    # while
    #    # (x,y) = (x1,y1), (x2,y2), (x3,y3) and (x4,y4)
    #    # such that:
    #    # [x1]   [  1  0  0  0 ]   [a_1]         [a_1]
    #    # [x2] = [  1  1  0  0 ] x [a_2] = [A] x [a_2]
    #    # [x3]   [  1  1  1  1 ]   [a_3]         [a_3]
    #    # [x4]   [  1  0  1  0 ]   [a_4]         [a_4]
    #    # that we use to determine the coeff a_i
    #    # the same system holds for y: [y] = [A] x [b]
    #    #
    #    # we then find r as a function of s from (1), substitute in (2)
    #    # and solve the obtained quadratic equation

    invMapping = numpy.array(
        [
            [1.0, 0.0, 0.0, 0.0],
            [-1.0, 1.0, 0.0, 0.0],
            [-1.0, 0.0, 0.0, 1.0],
            [1.0, -1.0, 1.0, -1.0],
        ]
    )

    x = numpy.array(element)[:, 0]
    y = numpy.array(element)[:, 1]
    a = invMapping.dot(x)
    b = invMapping.dot(y)

    aa = a[3] * b[2] - a[2] * b[3]
    bb = (
        a[3] * b[0]
        - a[0] * b[3]
        + a[1] * b[2]
        - a[2] * b[1]
        + point[0] * b[3]
        - point[1] * a[3]
    )
    cc = a[1] * b[0] - a[0] * b[1] + point[0] * b[1] - point[1] * a[1]

    det = bb * bb - 4 * aa * cc
    assert det > 0.0
    det = sqrt(det)
    s = (-bb + det) / (2 * aa) if abs(aa) > 1e-9 else -cc / bb
    assert s >= 0.0 and s <= 1.0

    if abs(a[1] + a[3] * s) > 1e-9:
        r = (point[0] - a[0] - a[2] * s) / (a[1] + a[3] * s)
    if abs(a[1] + a[3] * s) <= 1e-9 or r < 0 or r > 1:
        r = (point[1] - b[0] - b[2] * s) / (b[1] + b[3] * s)
    assert r >= 0.0 and r <= 1.0

    return [(1 - r) * (1 - s), r * (1 - s), r * s, (1 - r) * s]


class ConsoleLogger(object):
    """takes the place of QgsMessageBar
    { INFO = 0, WARNING = 1, CRITICAL = 2, SUCCESS = 3 }
    """

    def __init__(self):
        pass

    def pushInfo(self, title, msg):
        """Push info message

        :param title: title msg
        :type title: string
        :param msg: msg
        :type msg: string
        """
        sys.stdout.write("%s: %s\n" % (title, msg))

    def pushWarning(self, title, msg):
        """Push warning message

        :param title: title msg
        :type title: string
        :param msg: msg
        :type msg: string
        """
        sys.stderr.write("%s: %s\n" % (title, msg))

    def pushCritical(self, title, msg):
        """Push critical message

        :param title: title msg
        :type title: string
        :param msg: msg
        :type msg: string
        """
        sys.stderr.write("%s: %s\n" % (title, msg))
        raise Exception(msg)


def interpolationWeightsTri(point, element):
    """Interpolation weight in triangle mesh barycentric interpolation

    :param point: point xy
    :type point: tuple
    :param element: mesh element
    :type element: list of vertices coord

    :return: list of weight
    :rtype: list
    """
    u = (element[1][0] - element[0][0], element[1][1] - element[0][1])
    v = (element[2][0] - element[0][0], element[2][1] - element[0][1])
    w = (point[0] - element[0][0], point[1] - element[0][1])
    j = u[0] * v[1] - u[1] * v[0]
    w2 = (u[0] * w[1] - u[1] * w[0]) / j
    w1 = (w[0] * v[1] - w[1] * v[0]) / j
    w0 = 1 - w1 - w2
    assert -0.001 <= w0 and w0 <= 1.001
    assert -0.001 <= w1 and w1 <= 1.001
    assert -0.001 <= w2 and w2 <= 1.001
    return [w0, w1, w2]


def interpolationWeights(point, element):
    """give interpolation weights for a point in a triangle or quad

    :param point: point xy
    :type point: tuple
    :param element: mesh element
    :type element: list of vertices coord

    :return: list of weight
    :rtype: list
    """
    if len(element) == 4:
        return interpolationWeightsQuad(point, element)
    elif len(element) == 3:
        return interpolationWeightsTri(point, element)
    else:
        assert False  # should be a triangle or a quad


def durationCheck(duration, patterns, object_=None):
    """Check duration value

    :param duration: duration
    :type duration: string
    :param patterns: list of patterns
    :type patterns: list
    :param object: qobject
    :type object: QObject

    :return: duration
    :rtype: string
    """
    if not re.match(
        r"^ *[0-9]+(\.[0-9]+)? *({}|{}|{}) *$".format(
            patterns[2], patterns[1], patterns[0]
        ),
        duration,
    ):
        if object_:
            object_.setStyleSheet("color:#FF0000")
        return None
    if object_:
        object_.setStyleSheet("color:#000000")
    duration = duration.rstrip().lstrip().replace(" ", "")
    duration = (
        duration.replace(patterns[0], " " + patterns[0])
        .replace(patterns[1], " " + patterns[1])
        .replace(patterns[2], " " + patterns[2])
    )
    return duration


def available_mpi():
    mpi_protocols = []
    if sys.platform.startswith("win"):
        if "MSMPI_BIN" in os.environ and os.path.exists(
            os.path.join(os.environ["MSMPI_BIN"], "mpiexec.exe")
        ):
            mpi_protocols.append("MSMPI")
    else:
        mpi_implementation, _ = MPI.get_vendor()
        if mpi_implementation == "Open MPI":
            mpi_protocols.append("OPENMPI")
        elif mpi_implementation == "MPICH":
            mpi_protocols.append("HYDRA")
    return mpi_protocols


# run as script for testing
if __name__ == "__main__":
    x0, y0, z0 = 2, 3, 4
    nodes = zns_nodes((x0, y0, z0), 28.64, 20)

    with open("test_data/zns_mesh_for_28.64_coef_20.txt") as ref:
        next(ref)
        for i, line in enumerate(ref):
            zref = float(line)
            if abs(zref - (nodes[i][2] - z0)) > 1e-5:
                raise Exception(
                    "Error at node %d, zref=%f and z=%f" % (i, zref, nodes[i][2] - z0)
                )


def save_read(save_file):
    """unused"""
    d = {}
    if os.path.isfile(save_file):
        f = open(save_file)
        for line in f:
            spl = [x.strip() for x in line.split("=")]
            d[spl[0]] = int(spl[1])

    return d
