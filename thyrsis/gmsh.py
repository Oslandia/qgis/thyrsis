"""
Meshing with gmsh

USAGE:
    python -m thyrsis.gmsh database.sqlite [-t, -c, -v]

OPTIONS

   -t : test
   -r : run inversion
   -v : verbose
"""
import os
import re
import shutil
import struct
import sys
from builtins import range, str, zip
from subprocess import PIPE, Popen

import numpy
from shapely import wkt
from shapely.geometry import LinearRing, LineString, Point, Polygon

from thyrsis.exception import InvalidGmshResult
from thyrsis.log import logger


class Gmsh(object):
    """Process class for gmsh meshing"""

    def __init__(self, database, cur, settings):
        """Constructor

        :param database: sqlite mesh database
        :type database: str
        :param cur: sqlite cursor
        :type cur: cursor object
        :param settings: project settings
        :type settings: ConfigParser object

        """
        self.__database = database
        self.__cur = cur
        self.__settings = settings
        self.__tmpdir = self.mesh_tmpdir()
        self.mesh()

    def mesh_tmpdir(self):
        """Creates cfmesh directory

        :return: tempdir
        :rtype: string
        """
        dbname = self.__database
        dir_name = os.path.dirname(os.path.abspath(dbname))
        database = os.path.join(dir_name, dbname)
        tmpdir = os.path.join(dir_name, os.path.basename(dbname)[:-7] + "_gmsh")
        if os.path.isdir(tmpdir):
            shutil.rmtree(tmpdir)
        os.makedirs(tmpdir)
        logger.debug("tmpdir = ", tmpdir)

        return tmpdir

    def mesh(self):
        """Update database with mesh

        :param cur: cursor on a sqlite connection
        :type cur: sqlite3.Cursor
        """
        # nouvelle version sans duplication de lignes
        # mais qui suppose qu'il n'y a qu'un polygone extérieur et qu'un niveau d'imbrication
        logger.notice("meshing with GMSH")
        cur = self.__cur
        project_srid = str(
            cur.execute(
                "SELECT srid FROM geometry_columns WHERE f_table_name = 'contours'"
            ).fetchone()[0]
        )
        lines = [
            wkt.loads(geom)
            for geom, in cur.execute("select st_astext(GEOMETRY) from bords")
        ]
        polygons = [
            wkt.loads(geom)
            for geom, in cur.execute("select st_astext(GEOMETRY) from domaines")
        ]

        if not len(polygons):
            return False

        polygon_exterior = None
        polygons_interior = []
        if len(polygons) == 1:
            polygon_exterior = polygons[0]
        else:
            for polygon in polygons:
                if len(list(polygon.interiors)) > 0:
                    polygon_exterior = polygon
                else:
                    polygons_interior.append(polygon)

        if not polygon_exterior:
            return False

        node_map = {}
        line_map = []
        surface_map = {}
        current_id = 0
        tempdir = self.__tmpdir
        tmp_in_file = os.path.join(tempdir, "tmp_mesh.geo")
        with open(tmp_in_file, "w") as geo:
            for polygon in [polygon_exterior]:
                surface_idx = []
                for ring in list(polygon.interiors) + [polygon.exterior]:
                    ring_idx = []
                    for coord in ring.coords:
                        if coord in node_map:
                            ring_idx.append(node_map[coord])
                        else:
                            current_id += 1
                            ring_idx.append(current_id)
                            node_map[coord] = current_id
                            geo.write(
                                "Point(%d) = {%f, %f, 0, 999999};\n"
                                % (current_id, coord[0], coord[1])
                            )
                    loop_idx = []
                    for i, j in zip(ring_idx[:-1], ring_idx[1:]):
                        current_id += 1
                        geo.write("Line(%d) = {%d, %d};\n" % (current_id, i, j))
                        loop_idx.append(current_id)
                        line_map.append(sorted([i, j]))
                    current_id += 1
                    geo.write(
                        "Line Loop(%d) = {%s};\n"
                        % (current_id, ", ".join((str(i) for i in loop_idx)))
                    )
                    surface_idx.append(current_id)

                    current_id += 1
                    for p, polygon in enumerate(polygons):
                        if ring.equals(polygon.exterior):
                            surface_map[p] = current_id
                    if ring.equals(polygon_exterior.exterior):
                        geo.write(
                            "Plane Surface(%d) = {%s};\n"
                            % (current_id, ", ".join((str(i) for i in surface_idx)))
                        )
                    else:
                        geo.write(
                            "Plane Surface(%d) = {%d};\n"
                            % (current_id, surface_idx[-1])
                        )

            for p, polygon in enumerate(polygons):
                current_surface = surface_map[p]
                for line in lines:
                    if polygon.covers(line):
                        line_idx = []
                        for coord in line.coords:
                            if coord in node_map:
                                line_idx.append(node_map[coord])
                            else:
                                current_id += 1
                                line_idx.append(current_id)
                                node_map[coord] = current_id
                                geo.write(
                                    "Point(%d) = {%f, %f, 0, 999999};\n"
                                    % (current_id, coord[0], coord[1])
                                )
                        for i, j in zip(line_idx[:-1], line_idx[1:]):
                            if sorted([i, j]) not in line_map:
                                current_id += 1
                                geo.write("Line(%d) = {%d, %d};\n" % (current_id, i, j))
                                geo.write(
                                    "Line {%d} In Surface {%d};\n"
                                    % (current_id, current_surface)
                                )
                                line_map.append(sorted([i, j]))

                for point, longueur_element in [
                    (wkt.loads(geom), longueur_element)
                    for geom, longueur_element in cur.execute(
                        """
                        select st_astext(GEOMETRY), longueur_element from points_fixes"""
                    ).fetchall()
                ]:
                    if polygon.covers(point):
                        if point.coords[0] not in node_map:
                            logger.notice("adding fixed", point)
                            current_id += 1
                            node_map[point.coords[0]] = current_id
                            geo.write(
                                "Point(%d) = {%f, %f, 0, %f};\n"
                                % (
                                    current_id,
                                    point.coords[0][0],
                                    point.coords[0][1],
                                    longueur_element or "999999",
                                )
                            )
                            geo.write(
                                "Point {%d} In Surface {%d};\n"
                                % (current_id, current_surface)
                            )

        os.chmod(tmp_in_file, 0o666)
        # run gmsh
        tmp_out_file = os.path.join(tempdir, "tmp_mesh.msh")
        exe = self.__settings.value("General", "gmsh")
        cmd = [exe, "-2", tmp_in_file, "-o", tmp_out_file, "-format", "msh2"]
        if os.name == "posix":
            out, err = Popen(cmd, stdout=PIPE, stderr=PIPE).communicate()
        else:
            out, err = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE).communicate()

        logger.notice(str(out, "utf-8"))

        if err:
            logger.warning(str(err, "utf-8"))

        os.chmod(tmp_out_file, 0o666)

        # load results
        logger.notice("loading results")
        with open(tmp_out_file, "r") as out:
            while not re.match(r"^\$Nodes", out.readline()):
                pass
            nb_nodes = int(out.readline().replace("\n", ""))
            nodes = numpy.zeros((nb_nodes, 2), dtype=numpy.float64)
            for i in range(nb_nodes):
                id_, x, y, z = out.readline().replace("\n", "").split()
                nodes[int(id_) - 1, :] = numpy.float32(x), numpy.float32(y)
            if not re.match(r"^\$EndNodes", out.readline()):
                raise InvalidGmshResult("Missing EndNodes in GMSH result")
            if not re.match(r"^\$Elements", out.readline()):
                raise InvalidGmshResult("Missing EndNodes in GMSH result")
            elements = []
            nb_elem = int(out.readline().replace("\n", ""))
            for i in range(nb_elem):
                spl = out.readline().replace("\n", "").split()
                if spl[1] == "2":
                    e = [int(n) - 1 for n in spl[-3:]]
                    r = LinearRing([nodes[n] for n in e])
                    if not r.is_ccw:
                        e = e[::-1]
                    elements.append(e)

        cur.execute("DROP INDEX IF EXISTS mailles_a_idx")
        cur.execute("DROP INDEX IF EXISTS mailles_b_idx")
        cur.execute("DROP INDEX IF EXISTS mailles_c_idx")
        cur.execute("DROP INDEX IF EXISTS mailles_d_idx")
        cur.execute("DELETE FROM mailles")
        cur.execute("DELETE FROM noeuds")
        cur.execute("DELETE FROM noeuds_contour")

        logger.notice("loading nodes")
        cur.executemany(
            "INSERT INTO noeuds(OGC_FID, GEOMETRY)  VALUES (?, MakePoint(?, ?, {}))".format(
                project_srid
            ),
            [(n + 1, xy[0], xy[1]) for n, xy in enumerate(nodes)],
        )
        cur.execute("UPDATE geometry_columns_statistics set last_verified = 0")
        cur.execute("SELECT UpdateLayerStatistics('noeuds')")

        logger.notice("loading mesh")
        cur.executemany(
            """
            INSERT INTO mailles(a, b, c, GEOMETRY)  VALUES (?, ?, ?, GeomFromText(?, {}))
            """.format(
                project_srid
            ),
            [
                (
                    e[0] + 1,
                    e[1] + 1,
                    e[2] + 1,
                    Polygon([nodes[e[0]], nodes[e[1]], nodes[e[2]]]).wkt,
                )
                for e in elements
            ],
        )
        cur.execute("UPDATE geometry_columns_statistics set last_verified = 0")
        cur.execute("SELECT UpdateLayerStatistics('mailles')")
        cur.execute("CREATE INDEX mailles_a_idx ON mailles(a)")
        cur.execute("CREATE INDEX mailles_b_idx ON mailles(b)")
        cur.execute("CREATE INDEX mailles_c_idx ON mailles(c)")
        cur.execute("CREATE INDEX mailles_d_idx ON mailles(d)")

        cur.execute("UPDATE mailles SET surface = ST_Area(GEOMETRY)")
        cur.execute(
            """UPDATE noeuds SET surface =
            (SELECT SUM(ST_Area(e.geometry)/(case when e.d is null then 3 else 4 end)) FROM mailles as e
                WHERE e.a=noeuds.OGC_FID or e.b=noeuds.OGC_FID or e.c=noeuds.OGC_FID or e.d=noeuds.OGC_FID)"""
        )

        logger.notice(
            "total surface from nodes is",
            cur.execute("select sum(surface) from noeuds").fetchone()[0],
            "m2",
        )
        logger.notice(
            "total surface from mesh is",
            cur.execute("select sum(surface) from mailles").fetchone()[0],
            "m2",
        )

        logger.notice("loading contour nodes")
        cur.execute(
            """
            INSERT INTO noeuds_contour(nid)
            WITH ext AS (
                SELECT ST_Buffer(ST_ExteriorRing((select ST_Union(GEOMETRY) from domaines)), 1) AS GEOMETRY
                )
            SELECT n.OGC_FID FROM noeuds AS n, ext WHERE ST_Intersects(n.GEOMETRY, ext.GEOMETRY)
            """
        )


if __name__ == "__main__":

    from .database import sqlite
    from .mesh import mnt, mur
    from .settings import Settings

    logger.enable_console(True)

    if sys.argv[1][-7:] != ".sqlite":
        exit("first argument must be a sqlite database")
    database = sys.argv[1]
    settings = Settings(os.path.dirname(os.path.abspath(database)))
    with sqlite.connect(sys.argv[1]) as conn:
        cur = conn.cursor()

        Gmsh(database, cur, settings)

        if len(sys.argv) > 2:
            mnt(cur, sys.argv[2])
        if len(sys.argv) > 3:
            mur(cur, sys.argv[3])

        conn.commit()
