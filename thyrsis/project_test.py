import os
import sys
import tempfile

from PyQt5.QtCore import *
from qgis.core import QgsApplication, QgsPluginLayerRegistry, QgsProject

from .log import logger
from .meshlayer.meshdataproviderregistry import MeshDataProviderRegistry
from .meshlayer.meshlayer import MeshLayer, MeshLayerType
from .project import Project
from .settings import Settings
from .spatialitemeshdataprovider import SpatialiteMeshConstDataProvider

if __name__ == "__main__":
    QCoreApplication.setOrganizationName("QGIS")
    QCoreApplication.setApplicationName("QGIS2")
    QGISAPP = QgsApplication(sys.argv, True)
    QgsApplication.initQgis()
    MeshDataProviderRegistry.instance().addDataProviderType(
        SpatialiteMeshConstDataProvider.PROVIDER_KEY, SpatialiteMeshConstDataProvider
    )
    meshLayerType = MeshLayerType()
    QgsPluginLayerRegistry.instance().addPluginLayerType(meshLayerType)

    test_db = os.path.join(tempfile.tempdir, "pem_test.sqlite")
    if os.path.exists(test_db):
        os.remove(test_db)
    project = Project.create(
        test_db,
        os.path.join(
            Settings().value("General", "installDir"), "mesh", "PEM.mesh.sqlite"
        ),
    )

    QgsProject.instance().removeAllMapLayers()
    QgsPluginLayerRegistry.instance().removePluginLayerType(MeshLayer.LAYER_TYPE)

    logger.notice(test_db, "created")
