---
substitutions:
  img_inversion: |-
    :::{image} /_static/images/inverse.png
    :scale: 10 %
    :::
---

# Model creation

(model_parameters)=
## Model parameters

The model definition must be started by setting scalar parameters values in the parametres table ({numref}`tutorial_parametersTable`).

:::{figure} /_static/images/parametersTable.png
:name: tutorial_parametersTable
**parametres** table to fulfill.
:::

These parameters are typical values of the site and can be modified later in a specific study. They are defined as :

- WC, cinematic porosity
- VGA, Alpha van Genuchten ($m^{-1}$)
- VGN, n van Genuchten
- VGR, residual saturation
- VGS, maximal saturation
- VGE, inlet pression ($m$)
- WT, total porosity
- VM, volumic mass (kg/m³)
- DK, partition coefficient (m³/kg)
- SL, solubility limit (kg/m³)
- DLZNS, ZNS longitudinal dispersivity (m)
- DLZS, ZS longitudinal dispersivity (m)

Then, the permeability field must be defined by clicking on the Model button {{ img_inversion }} which offers 3 choices :

- *Create model with a constant permeability field value* : this creates an hydrogeologic model based on an uniform permeability field, whose value must be filled in the dialog,
- *Create model from a permeability field* : this creates an hydrogeologic model based on a permeability field given by an external tif file, which must be selected in the dialog,
- *Create model with inversion* : this enables an inversion process based on measured piezometric values and pilot points, see {ref}`Inversion<inversion>.

---

## Inversion

### Pilot points

Pilot points must first be defined in the *points_pilote* layer, and the zone where they belong and the measured piezometric value (altitude_piezo_mesuree) must be set. Zones must be numbered from 1 without empty number. Several pilot points can belong to the same zone.

The *points_pilote* layer provides the following fields :

- *OGC_FID* : unique ID
- *nom* : name of the pilot point
- *groupe* : group of pilot points used for correlation display
- *zone* : zone, used to associate pilot points with same permeability value
- *altitude_piezo_mesuree* : measured altitude of the water table (m)
- *altitude_piezo_calculee* : calculated altitude of the water table (m)
- *difference_calcul_mesure* : difference between calculated and measured water table altitude (m)
- *permeabilite* : permeability (m/s)

:::{note}
The four attributes *nom*, *groupe*, *zone* and *altitude_piezo_mesuree* must be filled, the others will be calculated during inversion process.
:::

It is possible to import delimited text files in QGIS (QGIS menu *Layer>Add a layer>Add a delimited text layer*) and do copy/paste actions between the imported layer and the points_pilote layer. To ease the copy/paste action, it is recommended to name the columns in the imported layer according to the points_pilote table columns (nom, zone, altitude_piezo_mesuree).

We provide a *point_pilote.csv* file which can be opened by using *Layer>Add Layer>Add Delimited Text Layer* ({numref}`tutorial_addTextDelimited`).

:::{figure} /_static/images/tutorial_addTextDelimited.png
:name: tutorial_addTextDelimited
Loading the *point_pilote.csv* file with *Add Delimited Text Layer*
:::

The QGIS dialog must be answered by selecting the right X and Y coordinates columns ({numref}`tutorial_textDelimitedDialog`).

:::{figure} /_static/images/tutorial_textDelimitedDialog.png
:name: tutorial_textDelimitedDialog
Selecting X and Y coordinates columns in the Text Delimited dialog
:::

The *points_pilote* layer can be completed by copy/paste from the loaded csv layer. Only column names with the same name as the *points_pilote* layer can be pasted.

At the end, the attribute table of the *points_pilote* layer should look like {numref}`tutorial_pilotPointsAttributes`.

(tutorial-pilotpointsattributes)=

:::{figure} /_static/images/tutorial_pilotPointsAttributes.png
:name: tutorial_pilotPointsAttributes
Attributes table of the *points_pilote* layer
:::

### Inversion parameters

The inversion process depends on the parameters of the **hynverse_parametres** table :

- *infiltration* : yearly mean infiltration in m/s. For heterogeneous infiltration, please enter 0 value and provide a [u_infiltration.node.sat or u_infiltration.elem.sat]  file.
- *icalc* : calcul type (default value : 3),
- *niter* : maximum iteration number (default value : 20, but higher values are useful to obtain a good convergence),
- *errlim* : error limit : process stops when the error is less than this value (default value : 0.1 m),
- *alfmin* : minimal value for $\alpha$ : process stops when $\alpha$ is less than this value (default value : $10^{-5}$),
- *alfa* : initial $\alpha$ value (default value : 0.5),
- *terr* : initial value of the maximum permissible growth rate of error (default value : 1.2) : if the growth rate of error is larger, the last iteration is not taken into account and a new one is done by dividing $\alpha$ by 2. This can be done *nbessaismax* times (see below),
- *nessaismax* : number of possible iterations by dividing $\alpha$ by 2 (default value : 10),
- *permini* : initial permeability value (default value : $2.10^{-5}$),
- *permin* : minimal permeability value (default value : $2.10^{-7}$),
- *permax* : maximal permeability value (default value : $2.10^{-4}$),
- *nv_npp* : number of closest nodes from the pilot points, used for pilot points potential estimation (default value : 4),
- *nv_ppm* : number of closest pilot points from mesh centers, used for meshes permeability estimation (default value : 10),
- *dv_pp* : radius (m) around pilot points to smooth permeability field by moving average (default value : 300 m),
- *d_mesh* : min length from mesh center to existing pilot point, used to add the mesh center as a new pilot point (in case of an external reference potential) (default value : 100 m).

To change these parameters, select the *hynverse_parametres* layer, click on the edit button and open the attribute table ({numref}`tutorial_hynverseParameters`).

:::{figure} /_static/images/tutorial_hynverseParameters.png
:name: tutorial_hynverseParameters
:scale: 50 %

Opening attribute table of the *hynverse_parametres* layer
:::

Here the infiltration value must be set to $4.75 10^{-9}$ (enter 4.75e-09 in the attribute table).

### Computation

Inversion process is launched by cliking on the Model button ({numref}`tutorial_modelButton`).

:::{figure} /_static/images/tutorial_modelButton.png
:name: tutorial_modelButton
Model button
:::

then by selecting *Create model with inversion* in the Model button dialog ({numref}`modelDialog`).

:::{figure} /_static/images/modelDialog.png
:name: modelDialog
Model button dialog
:::

Several layers are created :

- *potentiel_reference* : reference piezometric level (m), if an external reference potentiel layer has been specified,
- *potentiel* : calculated piezometric level (m),
- *epaisseur_zs* : groundwater thickness (saturated zone) (m),
- *epaisseur_zns* : unsaturated zone thickness (m),
- *permeabilite_x* : permeability on x axis (m/s),
- *v_norme* : Darcy velocity norm (m/s),

During the inversion process, the *permeabilite_x* layer is updated at each iteration.
Graphics showing the convergence are also updated ({numref}`tutorial_inversionCanvas`).

:::{figure} /_static/images/tutorial_inversionCanvas.png
:name: tutorial_inversionCanvas
QGIS canvas with inversion graphics
:::

The upper graphic shows the mean error between measured and estimated water table altitude. The lower graphic shows the correlation between measured and estimated water table altitude.

Finally save your mesh project by clicking to *Project>Save* or doing *Ctrl + S*
