---
substitutions:
  img_inversion: |-
    :::{image} /_static/images/inverse.png
    :scale: 10 %
    :::
  img_maillage: |-
    :::{image} /_static/images/mesh.png
    :scale: 10 %
    :::
  img_warning: |-
    :::{image} /_static/images/warning.png
    :scale: 80 %
    :::
---

(model_building)=

# Model building

A complete hydrogeologic model can be built with THYRSIS in two steps :

- defining a domain and meshing it using the [GMSH](http://gmsh.info/)  mesh builder {cite}`geuzaine-remacle:2009`,
- defining or computing a permeability field and computing all parameter fields.

## Mesh

### Operating principle

The mesh building is based on the use of the GMSH software {cite}`geuzaine-remacle:2009`. The first step is to define the bounds of the domain and sub-domains to model and the size of mesh for each one. Fixed points and lines can be integrated into the mesh.

Any contour element (polyline) can be defined as an imposed potential boundary condition. This will identify the corresponding nodes and cells associated with this boundary condition. By default, imposed potential value is equal to the topographic altitude value, in order to represent rivers or outcrops.

The mesh building needs two raster layers :

- MNT (DEM), digital elevation model,
- MUR, aquifer substratum altitude.

Once the mesh is built, the nodes and mesh tables must be filled in order to define the hydrogeological model. This can be done manually or by following the inversion process explained in the next chapter.

### Implementation

#### New mesh project creation

The mesh project will be created according to the current projection, available on the bottom right corner of QGIS  ({numref}`qgis_srid`).

:::{figure} /_static/images/qgis_srid.png
:name: qgis_srid
QGIS SRID
:::

Users should apply the wanted projection before creating the project.

To create a new mesh project, use the menu *Thyrsis>Nouveau>Maillage...* ({numref}`mesh_creation`).

:::{figure} /_static/images/mesh_creation.png
:name: mesh_creation
:scale: 150 %

New mesh creation
:::

Then enter a name for the mesh project, e.g. SACLAY_TEST. The name must :

- start with the site name ( {{ img_warning }} case sensitive),
- be extended with a suffix starting with `_`, and describing the new model.

A Spatialite database named **SACLAY_TEST.mesh.sqlite** is created, related to a `SACLAY_TEST.mesh.qgs` QGIS project. This project is opened into QGIS ({numref}`mesh_NewProject`).

:::{figure} /_static/images/mesh_NewProject.png
:name: mesh_NewProject
New mesh project created
:::

#### MNT and MUR layers definition

It is compulsary to provide a DEM layer (MNT) and an aquifer substratum altitude layer (MUR), preferably with the names mnt.tif and mur.tif. These layers should be opened into the mesh QGIS project ({numref}`mesh_layerOpening`).

The xyz_to_tif python script can be used to transform a .xyz file into a .tif file :

```bash
python -m thyrsis.utilities.xyz_to_tif file.xyz`
```

Add as many raster/vector layers useful for the mesh definition (for example : satellite images, buildings, site contours).

:::{figure} /_static/images/mesh_layerOpening.png
:name: mesh_layerOpening
Opening MNT and MUR raster layers.
:::

Zoom on one of the layer extent ({numref}`mesh_mntLayer`).

:::{figure} /_static/images/mesh_mntLayer.png
:name: mesh_mntLayer
DEM layer opened in QGIS, after a zoom on the layer.
:::

#### Contour elements definition

In order to define the contours of the model, it can be useful to import an existing contours layer ({numref}`mesh_contoursAdding`). The contours can also be defined directly into QGIS.

:::{figure} /_static/images/mesh_contoursAdding.png
:name: mesh_contoursAdding
Adding a contours layer providing the contours of the site and the domain to mesh (if existing).
:::

Select the *contours* layer and configure the snapping mode to use snap on vertex in the QGIS preferences ({numref}`mesh_snappingSelection` and {numref}`mesh_snappingOptions`).

:::{figure} /_static/images/mesh_snappingSelection.png
:name: mesh_snappingSelection
Snapping options selection
:::

:::{figure} /_static/images/mesh_snappingOptions.png
:name: mesh_snappingOptions
Snapping menu. Snap on vertex with a 10 pixels precision. Topological edition improve editing and maintaining common boundaries in polygon layers.
:::

Activate the *contours* layer edition mode and add contours manually or copy contours from an existing *contours* layer :

> - Existing contours can be copied in the *contours* layer using copy/paste action,
> - Contour parts can be defined by selecting a contour and cutting it with the "Edit > Separate entities" tools,
> - Contours parts can be merged together by selecting them and using the "Edit > Merge selected entities".

A length must be defined for each contour element, by opening and filling the attribute table of the *contours* layer. For each contour element with imposed potential boundary condition, the potentiel_impose field should be set to 1. In this case, the potential value will be computed from the DEM layer ({numref}`mesh_contoursAttribute`).

When all contour elements are defined, close the edition mode and save the changes.

:::{figure} /_static/images/mesh_contoursAttribute.png
:name: mesh_contoursAttribute
For each contour element specify its length (m) and if it corresponds to an imposed potential boundary condition (potentiel_impose set to 1).
:::

Once changes are saved, *domaines* and *extremites* layer are automatically updated. This checks the contours topological consistency ({numref}`mesh_domain`).

:::{figure} /_static/images/mesh_domain.png
:name: mesh_domain
Mesh domain after saving the *contours* layer, defining a closed domain.
:::

:::{note}
- An endpoint (from the *extremites* layer) in the middle of a contour is usually related to a wrong snapping configuration,
- Because it is not possible to use snapping on itself (QGIS limitation), a closed contour must be created by defining first an almost closed contour, then using snapping and the QGIS node tool to move the last vertex on the first one,
- Open contours inside a domain are accepted and are considered as fixed lines of the mesh.
:::

#### Fixed points definition

It is also possible to define fixed points that will be integrated to the mesh nodes. The meshing in the vicinity of a fixed node is specified by the *longueur_element* attribute. Fixed points can be copied from another layer using copy/paste ({numref}`mesh_fixedPoints`).

{{ img_warning }} Caution: element length is required for all fixed points.

:::{figure} /_static/images/mesh_fixedPoints.png
:name: mesh_fixedPoints
Adding fixed points.
:::

#### Mesh generation

Once the domains and fixed points are defined, click on the {{ img_maillage }} button to create the mesh ({numref}`mesh_meshDialog`). In the dialog, choose the DEM layer (MNT) and the aquifer substratum altitude layer (MUR). If available, add a potential layer. THYRSIS generates several layers ({numref}`mesh_meshResult`) :

- *maillage* (mesh), providing triangles of the mesh,
- *noeuds* (nodes), providing the mesh nodes,
- *altitude*, a layer that provides altitude at each mesh node, based on the MNT (DEM) layer,
- *altitude_mur*, a layer that provide altitude of the aquifer substratum at each mesh node, based on the MUR layer.
- *potentiel_reference*, providing the potential value at each node, if the potential layer has been set.

:::{figure} /_static/images/mesh_meshDialog.png
:name: mesh_meshDialog
Dialog window for the mesh building, with MNT, MUR and potential layer selection.
:::

:::{figure} /_static/images/mesh_meshResult.png
:name: mesh_meshResult
Final mesh with transparency.
:::

## Parameters definition

The model definition must be started by setting scalar parameters values in the **parametres** table ({numref}`mesh_parametersTable`).

:::{figure} /_static/images/parametersTable.png
:name: mesh_parametersTable
Parametres table to fulfill.
:::

Then, the permeability field must be defined by clicking on the Model button {{ img_inversion }} which offers 3 choices ({numref}`mesh_modelDialog`) :

- *Create model with a constant permeability field value* : this creates an hydrogeologic model based on an uniform permeability field, whose value must be filled in the dialog,
- *Create model from a permeability field* : this creates an hydrogeologic model based on a permeability field given by an external tif file, which must be selected in the dialog,
- *Create model with inversion* : this enables an inversion process based on measured piezometric values, cf. paragraph {ref}`Inversion<inversion>`.

:::{figure} /_static/images/modelDialog.png
:name: mesh_modelDialog
Model button dialog
:::
