# Création d'un maillage

*{sub-ref}`wordcount-minutes` minutes de lecture*

## Projet maillage

La première étape consiste à créer un projet maillage, lequel sera utilisé pour le calcul d'écoulement et transport 2D saturé.

Si THYRSIS est activé, un menu *Thyrsis* doit apparaître dans la barre de menu principale de QGIS. Pour créer un projet maillage, cliquer sur : `Thyrsis > Nouveau > Maillage...` {numref}`tutorial_newmesh` :

:::{figure} /_static/images/tutorial_newmesh.png
:name: tutorial_newmesh
Création d'un nouveau maillage
:::

:::{note}
THYRSIS crée le projet maillage à partir de la projection courante. Changer celle-ci dans QGIS pour être certain d'utiliser la bonne projection. Dans ce tutoriel, toutes les données sont projetées en EPSG: 27572 (Lambert II Etendu) :::

Le chemin du répertoire stockant la base de données maillage doit être renseigné. Le répertoire ``~/.thyrsis/mesh`` est celui dans lequel devra être placée la base de données créée pour pouvoir être utilisée dans THYRSIS ultérieurement, mais il est conseillé de choisir un autre emplacement pour la mise au point du modèle. La base de données doit être nommée `SITE_MODELE.mesh.sqlite`, où :
> - *SITE* désigne le nom du site - créé et sauvegardé dans la base `sites.sqlite` s'il n'existe pas déjà,
> - *MODEL* désigne le nom du modèle - un modèle étant la combinaison d'un maillage et de champs ou paramètres hydrogéologiques.

Ici la base est dénommée *SACLAY_TUTORIAL*, pour indiquer que le maillage est basé sur le site de SACLAY et qu'il s'agit du modèle TUTORIEL. THYRSIS crée également le projet QGIS (fichier .qgs). A l'issue de cette étape le répertoire de maillage doit ressembler à la {numref}`tutorial_meshdirectory`.

:::{figure} /_static/images/tutorial_meshdirectory.png
:name: tutorial_meshdirectory
:scale: 50 %

Répertoire maillage
:::

Avoir une couche de fond peut être utile pour localiser l'environnement voisin du project.
Le fond de carte OpenStreetMap peut être ajouté grâce au menu d'exploration des couches de QGIS {numref}`tutorial_osmlayer`.

:::{figure} /_static/images/tutorial_osmlayer.png
:name: tutorial_osmlayer
Canevas QGIS avec couche OSM
:::

Il est possible d'ajouter une couche de rivière, qui sera utile pour délimiter la zone du projet. Pour le tutoriel, un fichier `river.gpkg` est fourni. Cliquer sur *Couche>Ajouter une couche>Ajouter une couche vecteur* et ajouter la couche de rivière {numref}`tutorial_addVectorLayer`.

:::{figure} /_static/images/tutorial_addVectorLayer.png
:name: tutorial_addVectorLayer
Ajouter une couche vecteur
:::

Pour finir, deux rasters sont nécessaires pour la création du maillage :

- un modèle numérique de terrain (MNT), nommé  `mnt.tif`
- un raster fournissant l'altitude du mur de l'aquifère, dénommé `mur.tif`

Pour les ajouter, cliquer sur  `Couche> Ajouter une couche> Ajouter une couche raster`.

Il est recommandé de placer le fond cartographique et les couches raster sous les autres couches et d'ajouter de la transparence dans les propriétés des couches raster. Le canevas de la carte doit ressembler à :

:::{figure} /_static/images/tutorial_mntlayer.png
:name: tutorial_mntlayer
Canevas QGIS avec couche MNT
:::

Ne pas oublier de sauvegarder le projet (`CTRL + S` ou cliquer sur `Projet > Sauvegarder`)

----

## Maillage

L'étape suivante est la création du maillage 2D.

La couche *contours* doit être définie en premier. C'est une couche de polylignes avec les attributs suivants :

- *groupe* : nom du groupe auquel le contour appartient,
- *nom* : nom du contour,
- *longueur_element* : longueur caractéristique qui définit la densité du maillage dans le voisinage du contour,
- *potentiel_impose* : champ égal à 1 si le contour considéré est à potentiel imposé, égal à NULL ou 0 sinon.

:::{warning}
Pour créer les contours, activer l'[accrochage](https://docs.qgis.org/3.16/en/docs/user_manual/working_with_vector/editing_geometry_attributes.html#setting-the-snapping-tolerance-and-search-radius) dans QGIS et sélectionner l'option `couche active seulement`. Ceci est nécessaire afin de créer des contours formant des ensembles clos.
:::

:::{figure} /_static/images/tutorial_snapping.png
:name: tutorial_snapping
Activer l'accrochage
:::

Ensuite sélectionner la couche de contours, cliquer sur le bouton d'édition,

:::{figure} /_static/images/tutorial_editing.png
:name: tutorial_editing
Édition de la couche contours
:::

et activer l'outil *Ajouter une entité linéaire* ({numref}`tutorial_addLineFeature`).

:::{figure} /_static/images/tutorial_addLineFeature.png
:name: tutorial_addLineFeature
Ajouter une polyligne dans la couche contours
:::

Pour le tutoriel, nous voulons créer un maillage sur le Plateau de Saclay. Nous recommandons de créer 4 contours :

- un contour au nord le long de la rivière,
- un contour à l'est entre les deux rivières,
- un contour au sud le long de la rivière,
- un contour à l'ouest perpendiculaire aux rivières nord et sud.

ous les contours doivent se trouver dans l'étendue des couches du MNT et du mur. À chaque validation d'un contour, la fenêtre de la`tutorial_contoursAttributes` s'ouvre.

:::{figure} /_static/images/tutorial_contoursAttributes.png
:name: tutorial_contoursAttributes
Attributs de la couche *contours*
:::

Le champ *longueur_element* doit être renseigné et le champ *potentiel_impose* doit être égal à 1 si le contour correspond à une condition limite à potentiel imposé.

Des contours imbriqués peuvent être créés de manière à définir des zones avec différentes densités de maillage.

QGIS ne permet pas l’accrochage d’une entité sur elle même lors de la création de l’entité. Pour définir un contour fermé d’un seul tenant, créer un contour presque fermé, renseigner la longueur élémentaire puis utiliser l’outil de noeud pour déplacer le dernier point sur le premier (avec accrochage).

Enregistrer la couche contours. THYRSIS remplit alors la couche *domaines* avec les polygones définis par la couche *contours*.

L'absence de polygone créé dans la couche *domaines* est généralement lié à des erreurs topologiques, aucun contour fermé n'ayant été trouvé.

Le canevas devrait ressembler à l'image de la {numref}`tutorial_contoursCanvas`, avec des attributs de contours similaires.

:::{figure} /_static/images/tutorial_contoursCanvas.png
:name: tutorial_contoursCanvas
Canevas QGIS après validation de la couche *contours*
:::

Pour créer le maillage, cliquer sur le bouton *Maillage* ({numref}`tutorial_meshButton`) dans la barre d'outils de THYRSIS.

:::{figure} /_static/images/tutorial_meshButton.png
:name: tutorial_meshButton
Bouton maillage
:::

La fenêtre de la {numref}`tutorial_meshDialog` s'ouvre.

:::{figure} /_static/images/tutorial_meshDialog.png
:name: tutorial_meshDialog
Dialogue de maillage
:::

Vérifier les couches et lancer la création du maillage. Les couches *noeuds* and *mailles* sont actualisées et les couches *altitude* et *altitude_mur* sont ajoutées.

En cochant la case "Potentiel" et en spécifiant une couche raster de type "potentiel.tif", une nouvelle couche *potentiel_reference* est créée et le champ correspondant dans les tables *noeuds* et *mailles* est calculé. Cette couche peut ensuite être utilisée dans la procédure d'inversion pour créer de nouveaux points pilotes.

Le canevas QGIS de la {numref}`tutorial_meshResult` doit être obtenu.

:::{figure} /_static/images/tutorial_meshResult.png
:name: tutorial_meshResult
Canevas QGIS avec le résultat du maillage
:::
