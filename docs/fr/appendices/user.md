(user_file_fr)=
# Fichiers utilisateur

Cette annexe présente les formats des fichiers externes utilisés par le plugin THYRSIS pour modifier certaines données du modèle hydrogéologique.
Leur format est indépendant du code d'hydrogéologie utilisé.
Les unités sont des unités SI, à savoir : dates en secondes, longueurs en m, valeurs d'infiltration et de perméabilité en m/s.

## u_epaisseur_zns.node.sat or u_epaisseur_zns.elem.sat
Ces fichiers spécifient la hauteur de zone non saturée à prendre en compte au droit de chaque noeud du maillage 2D. Ils sont utiles lorsqu'on souhaite imposer une épaisseur de ZNS différente de celle du modèle.

Ils contiennent autant de lignes qu'il y a de noeuds ou d'éléments (en fonction de l'extension ".node" ou ".elem"), avec pour chaque ligne, la valeur de l'épaisseur de ZNS (en m) à considérer, trié par numéro de noeuds ou d'éléments.

Exemple :

```
27.6423
27.6636
27.6938
27.7452
27.7969
27.8473
27.9090
...
```

## u_permeabilite_zns.node.sat or u_permeabilite_zns.elem.sat

Ces fichiers spécifient la perméabilité en zone non saturée, au droit de chaque noeud du maillage 2D. Ils sont utiles lorsqu'on souhaite imposer une perméabilité en ZNS spécifique, alors que le modèle attribue à la ZNS la perméabilité en zone saturée calculée au droit de la colonne ZNS considérée.

Ils contiennent autant de lignes qu'il y a de noeuds ou d'éléments (en fonction de l'extension ".node" ou ".elem"), avec pour chaque ligne, la valeur de la perméabilité (en m/s) à considérer, trié par numéro de noeuds ou d'éléments.

Exemple :

```
0.10062337E-04
0.10151087E-04
0.10006153E-04
0.10216447E-04
0.10134652E-04
0.98510526E-05
0.10076285E-04
...
```

## u_infiltration.node.sat or u_infiltration.elem.sat

Ces fichiers spécifient l'infiltration en régime permanent à prendre en compte au droit de chaque noeud du maillage 2D. Ils sont utiles lorsqu'on souhaite imposer une infiltration en régime permanent différente de celle du modèle.

Ils contiennent autant de lignes qu'il y a de noeuds ou d'éléments (en fonction de l'extension ".node" ou ".elem"), avec pour chaque ligne, la valeur de l(infiltration (en m/s) à considérer, trié par numéro de noeuds ou d'éléments.

Exemple :

```
2.98e-09
2.98e-09
2.98e-09
2.98e-09
2.98e-09
2.98e-09
...
```

## u_infiltration_transitoire.sat and u_infiltration_transitoire.insat

Ces fichiers spécifient une infiltration dépendante du temps, mais uniforme sur le domaine ; ils sont requis dans le cas d'une simulation d'écoulement en régime transitoire, avec l'extension ".insat" lorsque la zone non saturée est prise en compte et l'extension ".sat" autrement.
Pour obtenir des résultats pertinents, la moyenne résultant de l'infiltration doit être égale à la moyenne de l'infiltration en régime permanent.

Le formalisme du fichier est présenté dans l'exemple ci-dessous. La date est indiquée après le texte "date" et l'infiltration est indiquée à la ligne suivante, avec une valeur négative correspondant à un flux d'entrée.

Exemple :

```
date  0.0000000E+00
-0.6498923E-08
date  0.2629800E+07
-0.1411746E-07
date  0.5259600E+07
-0.9015090E-08
...
```

## u_debit_entrant.sat

Ce fichier spécifie le flux de matière dans le cas d'un calcul sans ZNS. Il surcharge le fichier "debit_entrant.sat" qui est généré automatiquement par THYRSIS.

La date est indiquée après le texte "date" et les flux de matière (kg/s) sont indiqué aux lignes suivantes, avec un signe négatif correspondant au flux d'entrée et un format dépendant de l'outil de simulation :

- OpenFOAM : Les flux de matière sont donnés pour chaque cellule d'injection et précédé par les coordonnées de la cellule.
- METIS : Les flux de matière sont donnés pour chaque noeud d'injection et précédé par le numéro du noeud.

Exemple OpenFOAM :

```
date  3.155760e+05
637111.521 6847271.833 100 -2.514596e-16
637118.458 6847275.000 100 -3.023174e-16
637122.708 6847281.833 100 -1.394043e-17
637107.625 6847288.833 100 -8.280920e-19
637117.125 6847265.000 100 -2.757591e-17
...
date  6.311520e+05
637111.521 6847271.833 100 -2.859856e-16
637118.458 6847275.000 100 -3.438263e-16
637122.708 6847281.833 100 -1.585449e-17
637107.625 6847288.833 100 -9.417912e-19
637117.125 6847265.000 100 -3.136215e-17
...
date  9.467280e+05
637111.521 6847271.833 100 -2.860335e-16
637118.458 6847275.000 100 -3.438840e-16
637122.708 6847281.833 100 -1.585715e-17
637107.625 6847288.833 100 -9.419490e-19
637117.125 6847265.000 100 -3.136741e-17
...
```

Exemple METIS :

```
date  3.155760e+05
17320  -4.832293e-16
11843  -2.915937e-16
1107  -5.763685e-16
17315  -5.149194e-16
...
date  6.311520e+05
17320  -5.993523e-16
11843  -3.616655e-16
1107  -7.148734e-16
17315  -6.386578e-16
...
date  9.467280e+05
17320  -5.992676e-16
11843  -3.616144e-16
1107  -7.147724e-16
17315  -6.385675e-16
...
```

## u_debit_entrant.insat\*

Ces fichiers, dont le nom se termine par le numéro de la colonne ZNS, spécifient le flux de matière à considérer dans la ZNS. Ils surchargent les fichiers "debit_entrant\*.insat" qui sont générés automatiquement par THYRSIS.

La date est indiquée après le texte "date" et les flux de matière (kg/s) sont indiqués aux lignes suivantes, avec un signe négatif correspondant au flux d'entrée et un format dépendant de l'outil de simulation :

- OpenFOAM : Les flux de matière sont donnés pour chaque cellule d'injection et précédé par les coordonnées de la cellule en cas d'injection par profondeur ou par "top" dans le cas d'une injection en surface.
- METIS : Les flux de matière sont donnés pour chaque noeud d'injection et précédé par le numéro du noeud de la colonne ZNS du mesh.
-
Exemple OpenFOAM  (injection en surface) :

```
date 0.000000e+00
top -2.314815e-15
date 7.862400e+06
top -2.314815e-15
...
```

Exemple OpenFOAM (injection en profondeur) :

```
date 0.000000e+00
0.5 0.5 146.675 -2.314815e-15
date 7.862400e+06
0.5 0.5 146.675 -2.314815e-15
...
```

Exemple METIS :

```
date   0.0000000E+00
625   2.3148148E-15
date   7.8624000E+06
625   2.3148148E-15
...
```

## u_debit_eau_entrant.sat

Ce fichier spécifie le débit d'eau pour chaque date et noeud. Il surcharge les fichiers "debit_eau_entrant.sat" qui est généré automatiquement par THYRSIS.

La date est indiquée après le texte "date" et les débits d'eau (m3/s) sont indiqués aux lignes suivantes, avec un signe négatif correspondant au débit d'entrée et un format dépendant de l'outil de simulation :

- OpenFOAM : Les débits d'eau sont donnés en (m3/s) pour chaque cellule d'injection et précédé par les coordonnées de la cellule.
- METIS : Les débits d'eau sont donnés en (m3/s) pour chaque noeud d'injection et précédé par le numéro du noeud.

Exemple OpenFOAM :

```
date 3.155760e+05
637111.521 6847271.833 100 -1.021677e-03
637118.458 6847275.000 100 -1.228312e-03
637122.708 6847281.833 100 -5.663981e-05
637107.625 6847288.833 100 -3.364528e-06
637117.125 6847265.000 100 -1.120406e-04
...
date 6.311520e+05
637111.521 6847271.833 100 -1.021858e-03
637118.458 6847275.000 100 -1.228529e-03
637122.708 6847281.833 100 -5.664982e-05
637107.625 6847288.833 100 -3.365123e-06
637117.125 6847265.000 100 -1.120604e-04
...
date 9.467280e+05
637111.521 6847271.833 100 -1.021913e-03
637118.458 6847275.000 100 -1.228596e-03
637122.708 6847281.833 100 -5.665289e-05
637107.625 6847288.833 100 -3.365305e-06
637117.125 6847265.000 100 -1.120665e-04
...
```

Exemple METIS :

```
date 3.155760e+05
17320 -3.487842e-05 0
11843 -2.104659e-05 0
1107 -4.160100e-05 0
17315 -3.716574e-05 0
...
date 6.311520e+05
17320 -3.488461e-05 0
11843 -2.105032e-05 0
1107 -4.160838e-05 0
17315 -3.717234e-05 0
...
date 9.467280e+05
17320 -3.488655e-05 0
11843 -2.105150e-05 0
1107 -4.161070e-05 0
17315 -3.717441e-05 0
...
```
