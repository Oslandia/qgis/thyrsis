---
substitutions:
  img_forage: |-
    :::{image} /_static/images/forage.png
    :scale: 10 %
    :::
---

# Caractéristiques du site

Les sites peuvent être décrits en ajoutant des points d'intérêt, des mesures de potentiel ou de concentration, et des logs stratigraphiques de forages.

## Points d'intérêt

Il est intéressant de définir des points d'intérêt, qui peuvent être intégrés dans la table **points_interet** et la couche *points_interet*. Les résultats de simulation aux points d'intérêt sont automatiquement enregistrés dans des fichiers texte pour une possible réutilisation ultérieure.

Les points d'intérêt peuvent être chargés dans la base **sites.sqlite** à l'aide de la fonction "Load points" disponible dans les outils de traitement THYRSIS de QGIS, ou en ligne de commande :

```bash
python -m thyrsis.database.load_points site points.csv
```

où :

- *site* désigne le nom du site (str),
- *points.csv* désigne un fichier texte au format CSV avec trois à cinq colonnes séparées par des blancs, des virgules ou des point-virgules, et sur chaque ligne les valeurs :
    - abscisse x
    - ordonnée y,
    - nom du point,
    - (optionnel) entier définissant le système de projection EPSG - 4326 par défaut,
    - (optionnel) une chaîne de caractères définissant un ensemble d'appartenance (comme \"calcul\" (défaut), \"forage\", \"cheminée\", etc.).

:::{note}

- La première ligne du fichier n'est lue que pour identifier les séparateurs.
- Seuls les points du groupe "calcul" sont retenus dans les fichiers de résultats.
:::

----

## Mesures

Les mesures de concentration ou de potentiel peuvent être associées aux points d'intérêt de manière à les afficher en même temps que les résultats de simulation.

Les mesures peuvent être chargées dans la base **sites.sqlite** à l'aide de la fonction "Load measure" disponible dans les outils de traitement THYRSIS de QGIS, ou en ligne de commande :

```bash
# pour les potentiels :
python -m thyrsis.database.load_measure [-d] site potPoint.dat
# pour les concentrations :
python -m thyrsis.database.load_measure [-d] site chemical unit concPoint.dat
```

où :

- *site* désigne le nom du site (str),
- *potPoint.dat* est un fichier contenant les mesures de concentration au point 'Point',
- *concPoint.dat* est un fichier contenant les mesures de concentration au point 'Point',
- *chemical* est un élément chimique de la table **elements_chimiques** (str),
- *unit* est l'unité de concentration (str),
- L'option `-d` permet de supprimer les mesures déjà enregistrées.

*potPoint.dat* et *concPoint.dat* sont des fichiers texte avec des séparateurs blancs et le format :

```dat
dd/mm/YYYY[ HH:MM[:SS]] value[ uncertainty]
```

## Forages

Les forages et les données de stratigraphie et de fracturation peuvent être chargés dans la base **sites.sqlite** à l'aide de la fonction "Load boreholes" disponible dans les outils de traitement THYRSIS de QGIS, ou en ligne de commande :

```bash
python -m thyrsis.database.load_forages site srid forages.csv stratigraphie.csv [fracturation.csv]
```

où :

- *site* désigne le nom du site (str),
- *srid* désigne le système de projection EPSG (entier),
- *forages.csv* est un fichier csv avec séparateur ";" et les colonnes **ordonnées** suivantes :

  > - "NomComplet", "Nom", "X_m", "Y_m", "Z_m", "HauteurTube_m", "AltitudeTube_m", "Profondeur_m", "DateForage", "Type", "Localisation",

- *stratigraphie.csv* est un fichier csv avec séparateur ";" et les colonnes **ordonnées** suivantes :

  > - "Nom", "De", "À", "LithologieId", "LithologieDescription", "FormationId", "FormationDescription",

- *fracturation.csv* est un fichier csv avec séparateur ";" et les colonnes **ordonnées** suivantes :

  > - "Nom","De","À","FracturationTaux",

:::{note}

- Les noms de colonne ne sont pas lus et peuvent ête différents mais l'ordre des colonnes doit être strictement respecté,
- "LithologieId" fait référence au code de figuré USGS,  cf. {ref}`Logs stratigraphiques <stratigraphic_logs_fr>` ,
- "FormationId" fait référence à la codification CGMW, cf. {ref}`Logs stratigraphiques <stratigraphic_logs_fr>` .
:::

Des exemples de fichiers *forages.csv* et *stratigraphie.csv* sont fournis avec le tutoriel, associés au site "SACLAY" et au srid 27572.

Les forages sont alors accessibles dans le projet via le bouton {{ img_forage }}. En cliquant sur ce bouton et en sélectionnant un forage dans le canevas QGIS, un log stratigraphique s'affiche dans l'onglet Forage ({numref}`tutorial_Thyrsis_Forage`).

:::{figure} /_static/images/Thyrsis_Forage.png
:name: tutorial_Thyrsis_Forage
:scale: 50 %

Exemple de log stratigraphique
:::
