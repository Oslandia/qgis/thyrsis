"""
Thyrsis creation of latin hypercube

USAGE
    python -m thyrsis.create_latin_hypercube dbname.sqlite

    sets parametres_simulation in dbname.sqlite
"""
import os
from builtins import str

from qgis.core import QgsFeature
from qgis.PyQt import uic
from qgis.PyQt.QtCore import pyqtSignal
from qgis.PyQt.QtWidgets import QDialog, QMessageBox
from qgis.utils import iface

from ..database import _columns, sqlite
from ..settings import Settings


class ChemicalManagementDlg(QDialog):
    """Dialog for the chemical management"""

    chem_list_changed = pyqtSignal()

    def __init__(self, lyr, project, parent=None):
        super().__init__(parent)
        uic.loadUi(
            os.path.join(os.path.dirname(__file__), "chemical_management.ui"), self
        )
        self.lyr = lyr
        self.project = project
        self.settings = Settings(
            os.path.dirname(os.path.abspath(self.project.database))
        )
        self.site_db = self.settings.value("General", "defaultSite")

        self.fillChemCombobox()

        self.add_chemical_pb.clicked.connect(self.add_chemical)
        self.delete_chemical_pb.clicked.connect(self.delete_chemical)

    def fillChemCombobox(self):
        self.chemical_cb.clear()
        with sqlite.connect(self.project.database, "CrisisWidget:init") as conn:
            cur = conn.cursor()
            cur.execute("SELECT nom, id FROM elements_chimiques ORDER BY nom")
            self.chemical_cb.addItem("")
            for nom, id_ in cur.fetchall():
                self.chemical_cb.addItem(nom, id_)

    def add_chemical(self):
        """Use to add a new radionuclide in the site database"""
        new_id = max([f.id() for f in self.lyr.getFeatures()]) + 1

        with sqlite.connect(self.site_db) as conn:
            cur = conn.cursor()
            new_id = (
                cur.execute("""SELECT max(id) from elements_chimiques""").fetchone()[0]
                + 1
            )
        self.lyr.startEditing()
        ft = QgsFeature(self.lyr.fields())
        attr = ft.attributes()
        attr[0] = new_id
        ft.setAttributes(attr)
        # add feature to current layer will actualize study db
        self.lyr.addFeature(ft)
        res = iface.openFeatureForm(self.lyr, ft, showModal=True)
        if not res:
            self.lyr.rollBack()
            return
        self.lyr.commitChanges()
        with sqlite.connect(self.site_db) as dst:
            with sqlite.connect(
                self.project.database, "database:update_chemical_database:src"
            ) as src:
                cur = dst.cursor()
                cur.execute("PRAGMA foreign_keys = ON")
                destination = cur.execute("PRAGMA database_list").fetchone()[2]

                source = src.execute("PRAGMA database_list").fetchone()[2]

                # attach src to dst to enable cross-db select
                if source not in [
                    r[2] for r in cur.execute("PRAGMA database_list").fetchall()
                ]:
                    cur.execute('ATTACH "' + source + '" AS src')

                res = cur.execute(
                    "SELECT %(cols)s \
                        FROM src.elements_chimiques WHERE src.elements_chimiques.id = %(eid)d"
                    % {
                        "cols": ",".join(_columns(cur, "elements_chimiques")),
                        "eid": new_id,
                    }
                )

                # Insert new element into site_db
                cur.execute(
                    "INSERT INTO main.elements_chimiques(%(cols)s) SELECT %(cols)s \
                        FROM src.elements_chimiques WHERE src.elements_chimiques.id = %(eid)d"
                    % {
                        "cols": ",".join(_columns(cur, "elements_chimiques")),
                        "eid": new_id,
                    }
                )
                cur.execute("COMMIT")
        self.chem_list_changed.emit()
        self.fillChemCombobox()

    def delete_chemical(self):
        chem_id = self.chemical_cb.currentData()

        with sqlite.connect(self.site_db) as conn:
            cur = conn.cursor()
            site_nb_sim = cur.execute(
                """SELECT COUNT(1) FROM simulations WHERE eid = {}""".format(chem_id)
            ).fetchone()[0]
            site_nb_mesures = cur.execute(
                """SELECT COUNT(1) FROM mesures WHERE eid = {}""".format(chem_id)
            ).fetchone()[0]
        with sqlite.connect(self.project.database) as conn:
            cur = conn.cursor()
            proj_nb_sim = cur.execute(
                """SELECT COUNT(1) FROM simulations WHERE eid = {}""".format(chem_id)
            ).fetchone()[0]
            proj_nb_mesures = cur.execute(
                """SELECT COUNT(1) FROM mesures WHERE eid = {}""".format(chem_id)
            ).fetchone()[0]

        if site_nb_sim or site_nb_mesures or proj_nb_sim or proj_nb_mesures:
            text = "".join(
                [
                    self.tr("You will erase a chemical element that is used :\n"),
                    str(site_nb_sim),
                    self.tr(" times in the simulations table of the sites database\n"),
                    str(site_nb_mesures),
                    self.tr(" times in the mesures table of the sites database\n"),
                    str(proj_nb_sim),
                    self.tr(
                        " times in the simulations table of the computation database\n"
                    ),
                    str(proj_nb_mesures),
                    self.tr(
                        " times in the mesures table of the computation database\n"
                    ),
                    self.tr("Do you want to confirm ?"),
                ]
            )
            dlg = QMessageBox(
                QMessageBox.Information,
                "Confirmation",
                text,
                QMessageBox.Yes | QMessageBox.No,
            )
            return_value = dlg.exec()
            if return_value == QMessageBox.No:
                return

        with sqlite.connect(self.site_db) as conn:
            cur = conn.cursor()
            cur.execute(
                """DELETE FROM elements_chimiques WHERE id={}""".format(chem_id)
            )
            cur.execute("""COMMIT""")
        with sqlite.connect(self.project.database) as conn:
            cur = conn.cursor()
            cur.execute(
                """DELETE FROM elements_chimiques WHERE id={}""".format(chem_id)
            )
            cur.execute("""COMMIT""")
        self.chem_list_changed.emit()
        self.fillChemCombobox()
