# Préférences des outils de simulation

Après l'installation de THYRSIS, il est nécessaire de définir les chemins vers les outils de simulation dans la fenêtre des préférences de THYRSIS
After THYRSIS installation, user must define path to simulation software in THYRSIS preference dialog

:::{figure} /_static/images/Preferences_installation.png
:name: Preferences_installation
Preferences menu, installation window
:::

## GMSH

GMSH est utilisé pour la création des mesh. Le chemin vers l'exécutable GMSH doit être défini par l'utilisateur.

Par défaut, la valeur est définie avec une recherche depuis le `PATH`.

## OpenFoam

Pour l'utilisation d'OpenFoam, l'utilisateur doit définir le répertoire projet OpenFoam (`WM_PROJECT_DIR`).

Par défaut, la valeur est définie par une recherche de l'exécutable `blockMesh` depuis le `PATH`.

Si l'exécutable n'est pas disponible, de nouveaux chemins de recherche sont utilisés :

- Windows: `%APPDATA%/ESI-OpenCFD/OpenFOAM/v2406/msys64/home/ofuser/OpenFOAM/OpenFOAM-v2406/platforms/win64MingwDPnt32Opt/bin`
- Linux:  `/usr/lib/openfoam/openfoam2406/platforms/linux64GccDPInt32Opt/bin`

## PMF

Pour l'utilisation de PMF, l'utilisateur doit définir le répertoire d'installation de PMF.

Par défault, la valeur est definie par une recherche de l'exécutable `groundwaterFoam` depuis le `PATH`.

Si l'exécutable n'est pas disponible, de nouveaux chemins de recherche sont utilisés :

- Linux: `$HOME/OpenFOAM/$USERNAME-v2406/platforms/linux64GccDPInt32Opt/bin`

Pour Windows, merci d'indiquer le chemin version le répertoire d'extraction des binaires pré-compilés.
