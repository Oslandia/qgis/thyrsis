# Tutoriel

Ce tutoriel explique à partir d'un exemple comment créer un nouveau modèle hydrogéologique et comment créer et exploiter une nouvelle étude.

Les données du tutoriel peuvent être trouvées dans le répertoire `thyrsis/tutorial_data`.

Un modèle hydrogéologique est associé à un site et à la combinaison d'un maillage et de données scalaires et spatiales.

Le site peut être caractérisé par des points d'intérêt, des mesures et la description de forages.

```{toctree}
---
maxdepth: 2
---
tutorial/mesh_creation
tutorial/model_creation
tutorial/radionuclides
tutorial/site_characterization
tutorial/study_creation
```
