"""
Crée des colonnes d'injection regroupant le domaine par épaisseur et perméabilité

USAGE

    python -m thyrsis.create_injection_zones database.sqlite
"""
import getopt
import sys
from builtins import range, str

from shapely import wkt
from shapely.ops import unary_union

from ..database import create_injection_triggers, delete_injection_triggers, sqlite
from ..log import logger
from ..utilities import Timer


def create_injection_zones(dbname, debug=False):
    """Launch parrallel process as configured

    :param dbname: path to database
    :type dbname: string
    :param debug: verbose flag
    :type debug: bool
    """
    logger.notice("\nCREATION DES ZONES D'INJECTION")

    conn = sqlite.connect(dbname)
    cur = conn.cursor()

    delete_injection_triggers(cur)
    cur.execute("DROP INDEX IF EXISTS noeuds_injections_injection_idx")
    cur.execute("DROP INDEX IF EXISTS mailles_injections_injection_idx")

    timer = Timer()
    cur.execute("DELETE FROM noeuds_injections")
    logger.notice(timer.reset("noeuds_injections table deleted"))
    cur.execute("DELETE FROM mailles_injections")
    logger.notice(timer.reset("mailles_injections table deleted"))
    cur.execute("DELETE FROM injections")
    logger.notice(timer.reset("injections table deleted"))

    project_srid = str(
        cur.execute(
            "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
        ).fetchone()[0]
    )
    pas_echant_zns, rapport_max_permeabilite = cur.execute(
        "SELECT pas_echant_zns, rapport_max_permeabilite FROM simulations"
    ).fetchone()

    if pas_echant_zns > 0 and rapport_max_permeabilite > 0:
        cur.execute(
            """
            insert into mailles_injections(maille, injection, ratio_surface)
            with epaisseur as (
                select max(epaisseur_zns) as mxe, min(epaisseur_zns) as mne
                from mailles
            ),
            grp_epaisseur as (
                select m.OGC_FID, round((m.epaisseur_zns - e.mne)/s.pas_echant_zns) + 1  as grpe, coalesce(permeabilite_zns, permeabilite_x) as permeabilite
                from mailles m, epaisseur e, simulations s
            ),
            permeabilite as (
                select grpe, max(permeabilite) as mxp, min(permeabilite) as mnp
                from grp_epaisseur
                group by grpe
            ),
            grp_permeabilite as (
                select ge.OGC_FID, ge.grpe, floor(ln(ge.permeabilite/p.mnp)/ln(s.rapport_max_permeabilite)) as grpp
                from grp_epaisseur ge
                join permeabilite p on p.grpe = ge.grpe
                join simulations s on 1
            )
            select OGC_FID, dense_rank() over(order by grpe, grpp), 1
            from grp_permeabilite
            ;
        """
        )
    else:
        logger.notice("Each mesh is used as injection zone")
        cur.execute(
            """
            insert into mailles_injections(maille, injection, ratio_surface)
            select OGC_FID, OGC_FID, 1 from mailles
            ;
        """
        )

    nb_injections = cur.execute(
        "select max(injection) from mailles_injections"
    ).fetchone()[0]
    logger.notice(
        timer.reset(
            "created table mailles_injections with %d injections" % (nb_injections)
        )
    )
    conn.commit()

    for i in range(1, nb_injections + 1):
        cur.execute(
            """select st_astext(m.GEOMETRY), m.OGC_FID from mailles as m
        join mailles_injections as mi on mi.maille = m.OGC_FID where mi.injection={}""".format(
                i
            )
        )
        polygons = [wkt.loads(raw[0]) for raw in cur.fetchall()]
        merged_polygons = unary_union(polygons)
        cur.execute(
            """insert into injections(GEOMETRY, OGC_FID) VALUES (CastToMultiPolygon(GeomFromText('{}',{})),{})""".format(
                merged_polygons.wkt, project_srid, i
            )
        )

    logger.notice(timer.reset("created table injections"))
    conn.commit()

    cur.execute(
        """
        insert into noeuds_injections(noeud, injection, ratio_surface)
        select noeud, injection, sum(ratio_surface) from (
        select m.a as noeud, mi.injection, sum((1.0/3.0)*m.surface)/n.surface as ratio_surface
        from mailles_injections as mi
        join mailles as m on m.OGC_FID = mi.maille
        join noeuds as n on n.OGC_FID = m.a
        group by m.a, mi.injection
        union all
        select m.b as noeud, mi.injection, sum((1.0/3.0)*m.surface)/n.surface as ratio_surface
        from mailles_injections as mi
        join mailles as m on m.OGC_FID = mi.maille
        join noeuds as n on n.OGC_FID = m.b
        group by m.b, mi.injection
        union all
        select m.c as noeud, mi.injection, sum((1.0/3.0)*m.surface)/n.surface as ratio_surface
        from mailles_injections as mi
        join mailles as m on m.OGC_FID = mi.maille
        join noeuds as n on n.OGC_FID = m.c
        group by m.c, mi.injection
        union all
        select m.d as noeud, mi.injection, sum((1.0/3.0)*m.surface)/n.surface as ratio_surface
        from mailles_injections as mi
        join mailles as m on m.OGC_FID = mi.maille
        join noeuds as n on n.OGC_FID = m.d
        where m.d is not null
        group by m.d, mi.injection) group by noeud, injection
        ;
    """
    )
    conn.commit()
    cur.execute(
        """
        update noeuds_injections set ratio = (
            select ratio_surface*n.surface/AREA(i.GEOMETRY)
            from noeuds as n, injections as i
            where n.OGC_FID = noeud and i.OGC_FID = injection)
        ;
    """
    )
    conn.commit()
    nb_noeuds = cur.execute("select COUNT(1) from noeuds").fetchone()[0]
    nb_noeuds_injections = cur.execute(
        "select COUNT(1) from noeuds_injections"
    ).fetchone()[0]
    logger.notice(
        timer.reset(
            "created table noeuds_injections with %d injection nodes on %d nodes"
            % (nb_noeuds_injections, nb_noeuds)
        )
    )

    cur.execute(
        """
        update mailles_injections set ratio = (
            select ratio_surface*m.surface/AREA(i.GEOMETRY)
            from mailles as m, injections as i
            where m.OGC_FID = maille and i.OGC_FID = injection)
        ;
    """
    )
    conn.commit()
    cur.execute(
        """
        UPDATE injections SET
            permeabilite = (
              SELECT Coalesce(EXP(SUM(LN(m.permeabilite_zns))/COUNT(1)),
                               EXP(SUM(LN(m.permeabilite_x))/COUNT(1)))
              FROM mailles AS m, mailles_injections AS mi
              WHERE mi.injection = injections.OGC_FID
              AND m.OGC_FID = mi.maille ),
            infiltration = (
              SELECT AVG(m.infiltration)
              FROM mailles AS m, mailles_injections AS mi
              WHERE mi.injection = injections.OGC_FID
              AND m.OGC_FID = mi.maille ),
            epaisseur = (
              SELECT AVG(m.epaisseur_zns)
              FROM mailles AS m, mailles_injections AS mi
              WHERE mi.injection = injections.OGC_FID
              AND m.OGC_FID = mi.maille ),
            altitude = (
              SELECT AVG(m.altitude)
              FROM mailles AS m, mailles_injections AS mi
              WHERE mi.injection = injections.OGC_FID
              AND m.OGC_FID = mi.maille ),
            debut = (SELECT debut FROM simulations)
        ;
        """
    )
    conn.commit()

    cur.execute(
        """
        CREATE INDEX noeuds_injections_injection_idx ON noeuds_injections(injection)"""
    )
    cur.execute(
        """
        CREATE INDEX mailles_injections_injection_idx ON mailles_injections(injection)"""
    )

    conn.commit()
    conn.close()


if __name__ == "__main__":
    try:
        optlist, args = getopt.getopt(sys.argv[1:], "hd", ["help", "debug"])
    except Exception as e:
        sys.stderr.write(str(e) + "\n")
        exit(1)

    logger.enable_console(True)
    logger.set_level("debug")

    optlist = dict(optlist)
    debug = "-d" in optlist or "--debug" in optlist

    if "-h" in optlist or "--help" in optlist:
        help(sys.modules[__name__])
        exit(0)

    if len(args) != 1:
        sys.stderr.write("error: missing database\n")
        exit(1)
    dbname = args[0]

    # use provider for node coord and triangle read

    create_injection_zones(dbname, debug)
