"""
load points in site database

USAGE

    python -m thyrsis.database.load_forages site srid forages.csv stratigraphie.csv [fracturation.csv]

    load forage into the database

    site : site name (str)

    srid : EPSG id

    the expected .csv format is:

        - forages.csv: "Nom","Nom_reduit","X","Y","Z","H (tube_sol)","Z tube","Profondeur","Date de réalisation","Type","Localisation"

        - stratigraphie.csv: "Forage","From","To","code_roche", "description_roche", "code_formation", "formation"

        - (facultatif) fracturation.csv: "Forage","From","To","Taux_Frac"


"""
import csv
import os
from builtins import input, str

from ..log import logger


def get_float(string):
    """convert a string into float, even with a ',' decimal limit

    :param string: value to convert
    :type string: string

    :return: converted value
    :rtype: float
    """
    return float(string.replace(",", ".")) if string != "" else 0.0


def load_forages(
    cur, site, srid, forage, stratigraphie, fracturation="", delete_confirm=True
):
    """load borehole values into the current database

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param site: name of the site
    :type site: string
    :param srid: epsg code
    :type srid: string
    :param forage: path to a csv file of boreholes
    :type forage: string
    :param stratigraphie: path to a csv file of stratigraphic data
    :type stratigraphie: string
    :param fracturation: path to a csv file of fracturation data, if provided
    :type fracturation: string

    :return: None
    """
    (sid,) = cur.execute("SELECT id from sites where nom='%s'" % (site)).fetchone()
    srid = int(srid)

    (nforages,) = cur.execute(
        "SELECT COUNT(1) from forages where sid='%d'" % (sid)
    ).fetchone()

    delete = nforages > 0
    if delete and delete_confirm:
        ans = input(
            str(nforages)
            + " boreholes already exist for this site : do you want to remove them ? (yes/no) "
        )
        if ans[0] != "y":
            return
    if delete:
        logger.notice("Removing existing boreholes")
        cur.execute("pragma foreign_keys=ON")
        cur.execute("delete from forages where sid='%d'" % (sid))

    values = []
    with open(forage) as fgs:
        for i, row in enumerate(csv.reader(fgs, delimiter=";")):
            if i == 0 or len(row) != 11 or row[4] == "" or row[7] == "":
                continue

            x = get_float(row[2])
            y = get_float(row[3])
            zsol = get_float(row[4])
            zbas = get_float(row[4]) - get_float(row[7])
            date = str(row[8])
            typ = str(row[9])
            loc = str(row[10])
            prof = get_float(row[7])
            htube = get_float(row[5])
            nom_reduit = str(row[1])
            values.append(
                (
                    x,
                    y,
                    zsol,
                    x,
                    y,
                    zsol - prof,
                    x,
                    y,
                    zsol,
                    date,
                    typ,
                    loc,
                    prof,
                    htube,
                    zsol,
                    nom_reduit,
                    sid,
                )
            )

    cur.executemany(
        """
        INSERT INTO forages(GEOMETRY, POINT, date_realisation, type, localisation, profondeur, h_tube_sol, zsol, nom, sid)
        VALUES (ST_TRANSFORM(GeometryFromText(
                'LINESTRING Z('||?||' '||?||' '||?||','||
                                 ?||' '||?||' '||?||')', {srid}),4326),
                ST_TRANSFORM(GeometryFromText(
                'POINT Z('||?||' '||?||' '||?||')', {srid}),4326),
                ?, ?, ?, ?, ?, ?, ?, ?)
        """.format(
            srid=srid
        ),
        values,
    )

    (nforages,) = cur.execute(
        "SELECT COUNT(1) from forages where sid='%d'" % (sid)
    ).fetchone()
    logger.notice(nforages, " boreholes imported")

    with open(forage) as fgs:
        name_map = {
            str(row[1]): str(row[0])
            for i, row in enumerate(csv.reader(fgs, delimiter=";"))
            if len(row) == 11 and i > 0  # skip empty rows  # skip header
        }
    id_map = {
        name_map[nom]: id_
        for nom, id_ in cur.execute(
            """
        SELECT nom, OGC_FID FROM forages WHERE sid={}
        """.format(
                sid
            )
        ).fetchall()
    }
    # create a point map with points that lie on the ground
    with open(forage) as fgs:
        point_map = {
            str(row[0]): (
                get_float(row[2]),  # X_L2E
                get_float(row[3]),  # Y_L2E
                get_float(row[4]),  # Zsol
            )
            for i, row in enumerate(csv.reader(fgs, delimiter=";"))
            if len(row) == 11 and i > 0  # skip empty rows  # skip header
        }

    values = []
    with open(stratigraphie) as stgs:
        for i, row in enumerate(csv.reader(stgs, delimiter=";")):
            if len(row) >= 7 and i > 0:  # skip empty rows and header
                if row[1] == "" or row[2] == "":
                    continue
                point = point_map[str(row[0])]
                values.append(
                    (
                        point[0],
                        point[1],
                        point[2] - get_float(row[1]),
                        point[0],
                        point[1],
                        point[2] - get_float(row[2]),
                        id_map[str(row[0])],
                        get_float(row[1]),
                        get_float(row[2]),
                        str(row[5]).lower(),
                        str(row[6]),
                    )
                )

    cur.executemany(
        """
        INSERT INTO stratigraphies(GEOMETRY, forage, debut, fin, code, formation)
        VALUES (ST_TRANSFORM(GeometryFromText(
                'LINESTRING Z('||?||' '||?||' '||?||','||
                                 ?||' '||?||' '||?||')', {srid}),4326), ?, ?, ?, ?, ?)
        """.format(
            srid=srid
        ),
        values,
    )

    (nstrat,) = cur.execute("SELECT COUNT(1) from stratigraphies").fetchone()
    logger.notice(nstrat, " entries in table stratigraphies")

    values = []
    with open(stratigraphie) as stgs:
        for i, row in enumerate(csv.reader(stgs, delimiter=";")):
            if len(row) >= 7 and i > 0:  # skip empty rows and header
                if row[1] == "" or row[2] == "":
                    continue
                point = point_map[str(row[0])]
                values.append(
                    (
                        point[0],
                        point[1],
                        point[2] - get_float(row[1]),
                        point[0],
                        point[1],
                        point[2] - get_float(row[2]),
                        id_map[str(row[0])],
                        get_float(row[1]),
                        get_float(row[2]),
                        str(row[3]) if "usgs" in row[3] else "usgs" + str(row[3]),
                        str(row[4]),
                    )
                )

    cur.executemany(
        """
        INSERT INTO lithologies(GEOMETRY, forage, debut, fin, code, roche)
        VALUES (ST_TRANSFORM(GeometryFromText(
                'LINESTRING Z('||?||' '||?||' '||?||','||
                                 ?||' '||?||' '||?||')', {srid}),4326), ?, ?, ?, ?, ?)
        """.format(
            srid=srid
        ),
        values,
    )

    (nlitho,) = cur.execute("SELECT COUNT(1) from lithologies").fetchone()
    logger.notice(nlitho, " entries in table lithologies")

    if not os.path.exists(fracturation):
        return

    values = []
    with open(fracturation) as fracs:
        for i, row in enumerate(csv.reader(fracs, delimiter=";")):
            if len(row) == 4 and i > 0:  # skip empty rows and header
                if row[1] == "" or row[2] == "":
                    continue
                point = point_map[str(row[0])]
                values.append(
                    (
                        point[0],
                        point[1],
                        point[2] - get_float(row[1]),
                        point[0],
                        point[1],
                        point[2] - get_float(row[2]),
                        id_map[str(row[0])],
                        get_float(row[1]),
                        get_float(row[2]),
                        get_float(row[3]),
                    )
                )

    cur.executemany(
        """
        INSERT INTO fracturations(GEOMETRY, forage, debut, fin, taux)
        VALUES (ST_TRANSFORM(GeometryFromText(
                'LINESTRING Z('||?||' '||?||' '||?||','||
                                 ?||' '||?||' '||?||')', {srid}),4326), ?, ?, ?, ?)
        """.format(
            srid=srid
        ),
        values,
    )

    (nfract,) = cur.execute("SELECT COUNT(1) from fracturations").fetchone()
    logger.notice(nfract, " entries in table fracturations")


if __name__ == "__main__":
    import sys

    from ..database import sqlite
    from ..settings import Settings

    logger.enable_console(True)

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    if len(sys.argv) not in (5, 6):
        logger.error(
            "wrong number of arguments (try python -m thyrsis.database.load_forages --help)"
        )
        exit(1)
    elif len(sys.argv) == 5:
        fracturation = ""
    else:
        fracturation = sys.argv[5]

    if not Settings().contains("General", "defaultSite"):
        logger.error("defaultSite undefined, use preferences_dialog.py")
        exit(1)

    sitesdb = str(Settings().value("General", "defaultSite"))

    conn = sqlite.connect(sitesdb)

    load_forages(
        conn.cursor(), sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], fracturation
    )
    conn.commit()
