import math
import os
import re
import shutil
import sys
import tempfile
from builtins import object, range, str
from datetime import datetime
from functools import partial
from pathlib import Path
from subprocess import PIPE, Popen

import numpy
from osgeo.osr import SpatialReference
from PyQt5.QtGui import QBrush, QColor, QFont, QFontMetrics, QIcon, QImage, QPainter
from PyQt5.QtWidgets import (
    QAction,
    QActionGroup,
    QApplication,
    QCheckBox,
    QDockWidget,
    QFileDialog,
    QMenu,
    QMessageBox,
    QToolBar,
    QWidget,
)
from qgis.core import (
    Qgis,
    QgsApplication,
    QgsCoordinateReferenceSystem,
    QgsCoordinateTransform,
    QgsMapLayer,
    QgsPointXY,
    QgsProject,
    QgsProviderRegistry,
    QgsRenderContext,
    QgsSettings,
    QgsVectorLayer,
)
from qgis.PyQt.QtCore import (
    QCoreApplication,
    QLocale,
    QObject,
    QPoint,
    QSettings,
    QSize,
    Qt,
    QTimer,
    QTranslator,
    pyqtSignal,
)

from .database import add_second_milieu, create_site_tables, sqlite
from .gui.chemical_management import ChemicalManagementDlg
from .gui.crisis_dialog import CrisisWidget
from .gui.isovalue_dialog import IsovalueDialog
from .gui.lithology import BoreHoleWindow
from .gui.make_model import MakeModelDialog
from .gui.mesh_rasters import MeshRasterDialog
from .gui.point_tool import PointTool
from .gui.preferences_dialog import PreferenceDialog
from .gui.probability_dialog import ProbabilityDialog
from .gui.raster_resolution import RasterResolutionDialog
from .gui.result_control import ResultControl
from .gui.srid_confirm import SridConfirmDialog
from .gui.task_manager import TaskManager
from .gui.time_control import TimeControl
from .gui.time_plot import TimePlot
from .gui.time_plot_widget import TimePlotWidget
from .gui.zns_window import ZnsWindow
from .layer_to_tif import layer_to_tif
from .log import logger
from .mesh_project import MeshProject, get_layer
from .meshlayer.meshdataprovider import MeshDataProvider
from .meshlayer.meshdataprovidermetadata import MeshDataProviderMetadata
from .meshlayer.meshdataproviderregistry import MeshDataProviderRegistry
from .meshlayer.meshlayer import MeshLayer, MeshLayerType
from .meshlayer.utilities import format_

# handle the case where the user has not installed the plugin dependencies
try:
    from .gui.hynverse_plot import HynverseWindow
    from .project import Project
    from .simulation.metis import Metis
except ImportError:
    Project = None

from thyrsis.__about__ import DIR_PLUGIN_ROOT, __title__
from thyrsis.exception import (
    IsoValueCreationError,
    MeshCreationError,
    ModelCreationError,
    ProjectCreationError,
    ProjectLoadError,
    SecondMilieuCreationError,
)
from thyrsis.toolbelt import PlgLogger
from thyrsis.toolbelt.documentation import PlgDocumentationHelpers

from .processing.provider import Provider
from .settings import Settings
from .simulation.openfoam import OpenFoam
from .spatialitemeshdataprovider import (
    SpatialiteMeshConstDataProvider,
    SpatialiteMeshDataProvider,
)
from .utilities import Timer, complete_image_name, read_one_column_file
from .viewer_3d import Viewer3d, ViewerControls


class WaitCursor(object):
    """Lock cursor"""

    def __enter__(self):
        QApplication.setOverrideCursor(Qt.WaitCursor)

    def __exit__(self, exc_type, exc_value, tb):
        QApplication.restoreOverrideCursor()


def wait_cursor():
    """Return wait cursor

    :return: wait cursor
    :rtype: WaitCursor
    """
    return WaitCursor()


class TitleSetter(QObject):
    """Put title into legend"""

    def __init__(self, legend, parent=None):
        """Constructor

        :param legend: ColorLegend
        :type legend: ColorLegend
        :param parent: Qt parent
        :type parent: QtWidget
        """
        super(TitleSetter, self).__init__(parent)
        self.__legend = legend

    def update(self, title):
        """Set title

        :param title: title
        :type title: string
        """
        self.__legend.setTitle(SpatialiteMeshDataProvider.displayName[title])


class MinMaxUpdater(QObject):
    """Scale Updater"""

    def __init__(
        self, layer, result_control, time_control, scaleAutoUpdateCheckBox, parent=None
    ):
        """Constructor

        :param layer: layer
        :type layer: MeshLayer
        :param result_control: result controler
        :type result_control: ResultControl
        :param time_control: time controler
        :type time_control: TimeControl
        :param scaleAutoUpdateCheckBox: auto scale checkbox
        :type scaleAutoUpdateCheckBox: QCheckBox
        :param parent: Qt parent
        :type parent: QtWidget
        """
        super(MinMaxUpdater, self).__init__(parent)
        self.__layer = layer
        self.__result_control = result_control
        self.__time_control = time_control
        self.__scaleAutoUpdateCheckBox = scaleAutoUpdateCheckBox
        logger.debug("MinMaxUpdater:init:settings")
        self.__settings = layer.dataProvider().settings()

    def update(self, scratch=None):
        """Update min max value of the scale"""
        # make sure the current date is properly set,
        # the updateMinMax can be called by the auto-update of min max
        self.__layer.dataProvider().setDate(self.__time_control.timeSlider.value())

        column = self.__result_control.columnComboBox.itemData(
            self.__result_control.columnComboBox.currentIndex()
        )

        column = column.replace("2", "")
        self.__layer.colorLegend().setLogScale(
            column in ["concentration", "activity", "vitesse_darcy_norme"]
        )

        column = column.replace("vitesse_darcy_norme", "darcy")
        # self.__layer.dataProvider().setUnits(self.__settings.value("Variables", column+"Unit", "-"))
        self.__layer.colorLegend().setUnits(self.__layer.dataProvider().units())

        column = column.replace("activity", "concentration")
        scale = float(self.__settings.value("Variables", column + "Scale", 0))

        index = (
            self.__time_control.timeSlider.value()
            if self.__scaleAutoUpdateCheckBox.isChecked()
            else None
        )

        if not self.__layer.colorLegend().graduated():
            max_ = self.__layer.dataProvider().maxValue(index) or 0
            min_ = self.__layer.dataProvider().minValue(index) or 0
            self.__layer.colorLegend().setMinValue(max_ * scale if scale else min_)
            self.__layer.colorLegend().setMaxValue(max_)
        else:
            max_ = self.__layer.dataProvider().maxValue(index) or 0
            min_ = self.__layer.dataProvider().minValue(index) or 0
            self.__layer.colorLegend().setMinValue(max_ * scale if scale else min_)
            self.__layer.colorLegend().setMaxValue(max_)
            img = self.__layer.colorLegend().colorRampImg()
            nbClass = self.__layer.colorLegend().nbClass()
            if nbClass > 0:
                values = self.__layer.colorLegend().values(nbClass + 1)
                dh = (img.size().height() - 1) / (nbClass - 1) if nbClass > 1 else 0
                x = img.size().width() / 2
                classes = []
                for row in range(nbClass):
                    min_ = values[row + 1]
                    max_ = values[row]
                    classes.append(
                        (QColor(img.pixel(int(x), int(dh * row))), min_, max_)
                    )
                self.__layer.colorLegend().setGraduation(classes)

    def autoUpdate(self, scratch=None):
        """Activate auto scale"""
        if self.__scaleAutoUpdateCheckBox.isChecked():
            self.update()


class ThyrsisPlugin(QWidget):
    """Major class of Thyrsis plugin"""

    maskUnits = pyqtSignal(bool)

    def __init__(self, iface):
        """Constructor

        :param iface: qgis interface
        :type iface:QgisInterface
        """
        super(ThyrsisPlugin, self).__init__()

        # Save reference to the QGIS interface and get the log tool
        self.iface = iface
        self.log = PlgLogger().log
        self.doc_helper = PlgDocumentationHelpers()

        # initialize the locale
        self.locale: str = QgsSettings().value("locale/userLocale", QLocale().name())[
            0:2
        ]
        locale_path: Path = DIR_PLUGIN_ROOT / f"resources/i18n/thyrsis_{self.locale}.qm"
        self.log(message=f"Translation: {self.locale}, {locale_path}", log_level=4)
        if locale_path.exists():
            self.translator = QTranslator()
            self.translator.load(str(locale_path.resolve()))
            QCoreApplication.installTranslator(self.translator)

        self.provider = None
        self.inverse_window = None  # inversion
        self.tw = None  # 3D
        self.zns = None  # ZNS
        self.plw = None  # 1D
        self.crisis = None  #
        self.three_d_controls = None  #
        self.timer = QTimer(self)
        self.constantLayers = {}
        self.project = None
        self.simulate = None  #
        self.export_submenu = None  #
        self.create_submenu = None  #
        self.litho = None  # Forage
        self.mesh_toolbar = None  #
        self.minMaxUpdater = None  #
        self.titleSetter = None  #
        self.time_control = None  #
        logger.debug("plugin:init:settings")
        self.__settings = Settings()
        self.__settingsModified = False
        self.data_control_toolbar = None
        self.oldTool = None
        self.task_manager = QDockWidget(self.tr("Tasks manager"))
        self.task_manager.setObjectName(self.tr("Tasks manager"))
        self.task_manager.setWidget(TaskManager(parent=self))
        self.iface.addDockWidget(Qt.LeftDockWidgetArea, self.task_manager)

        provider_register = QgsProviderRegistry.instance()
        thyrsis_const_mesh_provider = MeshDataProviderMetadata(
            SpatialiteMeshConstDataProvider.providerKey(),
            SpatialiteMeshConstDataProvider.description(),
            SpatialiteMeshConstDataProvider.createProvider,
        )
        provider_register.registerProvider(thyrsis_const_mesh_provider)

        thyrsis_mesh_provider = MeshDataProviderMetadata(
            SpatialiteMeshDataProvider.providerKey(),
            SpatialiteMeshDataProvider.description(),
            SpatialiteMeshDataProvider.createProvider,
        )
        provider_register.registerProvider(thyrsis_mesh_provider)

        mesh_provider = MeshDataProviderMetadata(
            MeshDataProvider.providerKey(),
            MeshDataProvider.description(),
            MeshDataProvider.createProvider,
        )
        provider_register.registerProvider(mesh_provider)

        self.frame_rate_timer = Timer()

        logger.enable_console(int(self.__settings.value("General", "console", 0)))
        logger.set_iface(iface)

    def check_dependencies(self) -> None:
        """Check if all dependencies are satisfied. If not, warn the user and disable plugin."""
        # if import failed
        if Project is None:
            self.log(
                message=self.tr("Error importing dependencies. Plugin disabled."),
                log_level=2,
                push=True,
                button=True,
                button_connect=partial(
                    self.doc_helper.open_help,
                    f"{self.locale}/installation.html",
                ),
                duration=0,
            )
            # disable plugin widgets
            self.menu.setEnabled(False)
            self.data_control_toolbar.setEnabled(False)
            self.result_control_toolbar.setEnabled(False)
            self.task_manager.setEnabled(False)
            self.time_control_toolbar.setEnabled(False)

            # add tooltip over menu
            msg_disable = self.tr(
                "Plugin disabled. Please install all dependencies and then restart QGIS. Refer to the documentation for more information."
            )
            self.menu.setToolTip(msg_disable)
            self.data_control_toolbar.setToolTip(msg_disable)
            self.result_control_toolbar.setToolTip(msg_disable)
            self.task_manager.setToolTip(msg_disable)
            self.time_control_toolbar.setToolTip(msg_disable)
        else:
            self.log(message=self.tr("Dependencies satisfied"), log_level=3)

    def unload(self):
        """Triggered when Thyrsis closes"""
        QgsApplication.processingRegistry().removeProvider(self.provider)
        self.timer.stop()
        QgsProject.instance().readProject.disconnect(self.qgisProjectLoaded)

        if self.project:
            self.iface.newProject()
            QgsProject.instance().layersAdded.disconnect(self.layersAdded)
            QgsProject.instance().layerWillBeRemoved.disconnect(self.layerWillBeRemoved)
            QgsApplication.pluginLayerRegistry().removePluginLayerType(
                MeshLayer.LAYER_TYPE
            )
            self.removeProjectDependantWidgets()
            self.removeMeshProjectDependantWidgets()

        self.minMaxUpdater and self.minMaxUpdater.setParent(None)
        self.titleSetter and self.titleSetter.setParent(None)

        self.menu.setParent(None)
        self.menu = None
        self.data_control_toolbar.setParent(None)
        self.data_control_toolbar = None
        self.time_control_toolbar.setParent(None)
        self.time_control_toolbar = None
        self.result_control_toolbar.setParent(None)

        QApplication.restoreOverrideCursor()
        self.iface.removeDockWidget(self.task_manager)

    def tr(self, message):
        """Get the translation for a string using Qt translation API.
        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        return QCoreApplication.translate(self.__class__.__name__, message)

    def qgisProjectLoaded(self, doc=None):
        """Triggered when a qgis project is loaded"""
        logger.debug("qgisProjectLoaded")

        self.removeProjectDependantWidgets()
        self.removeMeshProjectDependantWidgets()

        logger.debug("qgisProjectLoaded cleared")

        compdb = QgsProject.instance().readEntry("thyrsis", "compdb", None)[0]
        meshdb = QgsProject.instance().readEntry("thyrsis", "meshdb", None)[0]
        logger.debug("compdb {} meshdb {}".format(compdb, meshdb))
        if compdb:
            databasePath = os.path.dirname(os.path.abspath(compdb))
            if not self.__settingsModified and os.path.isfile(
                os.path.join(databasePath, ".thyrsis.ini")
            ):
                self.__settings = Settings(databasePath)
            else:
                self.__settings.save(databasePath)
            self.project = Project(compdb)
            self.project.add_qgis_layers()
            self.createProjectDependantWidgets()
            self.__settings = Settings(os.path.dirname(os.path.abspath(compdb)))
            self.__settings.save()
        elif meshdb:
            databasePath = os.path.dirname(os.path.abspath(meshdb))
            if not self.__settingsModified and os.path.isfile(
                os.path.join(databasePath, ".thyrsis.ini")
            ):
                self.__settings = Settings(databasePath)
            else:
                self.__settings.save(databasePath)
            self.project = MeshProject(meshdb)
            self.checkProj()
            self.createMeshProjectDependantWidgets()
            self.__settings = Settings(os.path.dirname(os.path.abspath(meshdb)))
            self.__settings.save()
        else:
            self.project = None

        [
            self.layout_watch(l.name())
            for l in QgsProject.instance().layoutManager().layouts()
        ]
        QgsProject.instance().layoutManager().layoutAdded.connect(self.layout_watch)

    def checkProj(self):
        """Check if the project has the same projection as the data in the database"""
        with sqlite.connect(self.project.database, "plugin:checkProj") as conn:
            cur = conn.cursor()
            data_srid = str(
                cur.execute(
                    """
                SELECT srid FROM geometry_columns WHERE f_table_name LIKE 'contours'"""
                ).fetchone()[0]
            )
            if str(QgsProject.instance().crs()).split(":")[-1][:-1] != data_srid:
                text = str(
                    self.tr("The project SRID (EPSG:")
                    + str(QgsProject.instance().crs()).split(":")[-1][:-1]
                    + self.tr(") doesn't fit with the data (EPSG:")
                    + data_srid
                    + self.tr(")\nDo you want to reproject the project ?")
                )
                dlg = QMessageBox(
                    QMessageBox.Information,
                    "SRID management",
                    text,
                    QMessageBox.Yes | QMessageBox.No,
                )
                return_value = dlg.exec()
                if return_value == QMessageBox.Yes:
                    proj = SpatialReference()
                    proj.ImportFromEPSG(int(data_srid))
                    crs = QgsCoordinateReferenceSystem(proj.ExportToWkt())
                    QgsProject.instance().setCrs(crs)
                    for layer in QgsProject.instance().mapLayers():
                        if (
                            self.project.database
                            in QgsProject.instance()
                            .mapLayer(layer)
                            .dataProvider()
                            .uri()
                            .uri()
                        ):
                            QgsProject.instance().mapLayer(layer).setCrs(crs)
                    filename = QgsProject.instance().fileName()
                    QgsProject.instance().write(filename)
                    QgsProject.instance().clear()
                    QgsProject.instance().read(filename)

    def _createBoreholeWidgets(self):
        """Create borehole widget without displaying it"""
        self.litho = QDockWidget(self.tr("Borehole"))
        self.litho.setObjectName(self.tr("Borehole"))
        uri = (
            self.project.database
            if isinstance(self.project, Project)
            else self.__settings.value("General", "defaultSite")
        )
        self.litho.setWidget(BoreHoleWindow("dbname=" + uri, logger))
        self.iface.addDockWidget(Qt.LeftDockWidgetArea, self.litho)
        self.iface.mainWindow().tabifyDockWidget(
            self.iface.mainWindow().findChild(QDockWidget, "Layers"), self.litho
        )
        self.litho.hide()

    def _removeBoreholeWidgets(self):
        """remove borehole widget"""
        if self.litho:
            self.iface.removeDockWidget(self.litho)
            self.litho and self.litho.setParent(None)
            self.litho = None

    def createMeshProjectDependantWidgets(self):
        """Create widgets that are dependant to a mesh project"""
        self.removeMeshProjectDependantWidgets()

        self.mesh_toolbar = QToolBar("mesh_toolbar")
        self.mesh_toolbar.setObjectName("mesh_toolbar")
        self.iface.addToolBar(self.mesh_toolbar)
        action = self.mesh_toolbar.addAction(
            QIcon(complete_image_name("mesh.svg")), self.tr("mesh")
        )
        action.triggered.connect(self.mesh)

        action = self.mesh_toolbar.addAction(
            QIcon(complete_image_name("inverse.svg")), self.tr("model")
        )
        action.triggered.connect(self.make_model)

        self._createBoreholeWidgets()

        # Add export raster submenu
        submenu = self.menu.addMenu(
            QIcon.fromTheme(
                "document-save", QIcon(complete_image_name("document-save.svg"))
            ),
            self.tr("Export"),
        )
        submenu.addAction("Raster").triggered.connect(self.exportRaster)
        self.export_submenu = submenu

    def removeMeshProjectDependantWidgets(self):
        """Remove widgets that are dependant to a mesh project"""
        self.mesh_toolbar and self.mesh_toolbar.setParent(None)
        self.mesh_toolbar = None
        self.inverse_window and self.inverse_window.setParent(None)
        self.inverse_window = None

        self._removeBoreholeWidgets()

    def createProjectDependantWidgets(self):
        """Create widgets that are dependant to a project"""
        self.removeProjectDependantWidgets()

        with sqlite.connect(
            self.project.database, "plugin:createProjectDependantWidgets"
        ) as conn:
            (nbColumns,) = (
                conn.cursor().execute("SELECT COUNT(1) from injections").fetchone()
            )
        if nbColumns <= int(self.__settings.value("General", "nbColumnsMax")):
            self.crisis = QDockWidget("Thyrsis")
            self.crisis.setObjectName("Thyrsis")
            self.crisis.setWidget(CrisisWidget(self.project.database, self.iface, self))
            self.crisis.widget().simulateButton.clicked.connect(
                partial(self.runSimulation, True, True)
            )
            self.iface.addDockWidget(Qt.LeftDockWidgetArea, self.crisis)
            self.iface.mainWindow().tabifyDockWidget(
                self.iface.mainWindow().findChild(QDockWidget, "Layers"), self.crisis
            )
            self.crisis.show()
            self.crisis.raise_()

        logger.debug("createProjectDependantWidgets 1")

        submenu = self.menu.addMenu(
            QIcon.fromTheme("system-run", QIcon(complete_image_name("system-run.svg"))),
            self.tr("Simulate"),
        )
        submenu.addAction(self.tr("Initialize")).triggered.connect(
            partial(self.runSimulation, True, False)
        )
        submenu.addAction(self.tr("Compute without initialization")).triggered.connect(
            partial(self.runSimulation, False, True)
        )
        submenu.addAction(self.tr("Initialize and compute")).triggered.connect(
            partial(self.runSimulation, True, True)
        )
        self.simulate = submenu

        submenu = self.menu.addMenu(
            QIcon.fromTheme(
                "applications-utilities",
                QIcon(complete_image_name("applications-utilities.svg")),
            ),
            self.tr("Create"),
        )
        submenu.addAction(self.tr("Injection zones")).triggered.connect(
            self.createInjectionZones
        )
        submenu.addAction(self.tr("Latin hypercube")).triggered.connect(
            self.createLatinHypercube
        )
        submenu.addAction(self.tr("Probability map")).triggered.connect(
            self.probability
        )
        submenu.addAction(self.tr("Isovalues")).triggered.connect(
            partial(self.createIsovalues, 0)
        )
        submenu.addAction(self.tr("Isovalues (gdal)")).triggered.connect(
            partial(self.createIsovalues, 0.1)
        )
        submenu.addAction(self.tr("Evolution graph")).triggered.connect(self.plot)
        submenu.addAction(self.tr("Second medium")).triggered.connect(
            self.second_milieu
        )
        submenu.addAction(self.tr("Chemical element")).triggered.connect(
            self.radionuclide
        )
        self.create_submenu = submenu

        submenu = self.menu.addMenu(
            QIcon.fromTheme(
                "document-save", QIcon(complete_image_name("document-save.svg"))
            ),
            self.tr("Export"),
        )
        submenu.addAction("Raster").triggered.connect(self.exportRaster)
        submenu.addAction("Animation").triggered.connect(self.exportAnimation)
        submenu.addAction("Video").triggered.connect(self.exportVideo)
        submenu.addAction("Template").triggered.connect(self.exportTemplate)
        self.export_submenu = submenu

        self._createBoreholeWidgets()

        logger.debug("createProjectDependantWidgets done")

    def removeProjectDependantWidgets(self):
        """Delete widgets that are dependant to a mesh project"""
        self.crisis and self.crisis.setParent(None)
        self.crisis = None
        self.simulate and self.simulate.setParent(None)
        self.simulate = None
        self.export_submenu and self.export_submenu.setParent(None)
        self.export_submenu = None
        self.create_submenu and self.create_submenu.setParent(None)
        self.create_submenu = None
        self.inverse_window and self.inverse_window.setParent(None)
        self.inverse_window = None

        self._removeBoreholeWidgets()

    def openPreferences(self):
        """Open the Preference dialog"""
        dlg = PreferenceDialog(
            self.__settings, self.project.database if self.project else None
        )
        dlg.exec_()

        self.__settingsModified = dlg.getSettingsModified()

        self.maskUnits.emit(int(self.__settings.value("Variables", "maskUnitsCheck")))
        logger.enable_console(int(self.__settings.value("General", "console")))
        logger.set_level(
            "debug" if int(self.__settings.value("General", "debug")) else "notice"
        )
        if self.time_control:
            self.time_control.setFramePerSecond(
                int(self.__settings.value("Animation", "framePerSecond", 10))
            )
        if self.project and self.__settingsModified:
            self.project.setSettings(self.__settings)
        self._reload_data_control_toolbar()
        self._removeBoreholeWidgets()
        if self.project:
            self._createBoreholeWidgets()

    def _reload_data_control_toolbar(self):
        """Reload control toolbar"""
        if self.data_control_toolbar:
            self.data_control_toolbar.setParent(None)
        self.data_control_toolbar = QToolBar("data_control_toolbar")
        self.data_control_toolbar.setObjectName("data_control_toolbar")
        self.data_control_toolbar.setMaximumWidth(150)
        self.iface.addToolBar(self.data_control_toolbar)
        # make the two actions mutually exclusive by adding them to a QActionGroup
        group = QActionGroup(self)

        action = self.data_control_toolbar.addAction(
            QIcon(complete_image_name("forage.svg")), self.tr("borehole info")
        )
        action.triggered.connect(self.pickBoreHoleToggled)
        action.setCheckable(True)
        group.addAction(action)

        action = self.data_control_toolbar.addAction(
            QIcon(complete_image_name("inverse_plot.svg")),
            self.tr("correlation errors"),
        )
        action.triggered.connect(self.inverse_plot)

        action = self.data_control_toolbar.addAction(
            QIcon(complete_image_name("inverse.svg")), self.tr("invert")
        )
        action.triggered.connect(self.inverse)

    def initProcessing(self):
        self.provider = Provider()
        QgsApplication.processingRegistry().addProvider(self.provider)

    def initGui(self):
        """initGui function, do not rename it"""
        self.doc_helper.has_online_access()
        # if site.sqlite doesn't exist, we must create it
        site_db = self.__settings.value("General", "defaultSite")
        if not os.path.exists(site_db):
            with sqlite.connect(site_db, "plugin:initGui") as conn:
                cur = conn.cursor()
                cur.execute("SELECT InitSpatialMetaData(1)")
                create_site_tables(cur, with_cid=True, project_SRID=4326)
                conn.commit()

        # connect layer added to setup the mesh layers and zns_scene
        QgsProject.instance().layersAdded.connect(self.layersAdded)
        QgsProject.instance().layerWillBeRemoved.connect(self.layerWillBeRemoved)

        self.initProcessing()

        MeshDataProviderRegistry.instance().addDataProviderType(
            SpatialiteMeshDataProvider.PROVIDER_KEY, SpatialiteMeshDataProvider
        )
        MeshDataProviderRegistry.instance().addDataProviderType(
            SpatialiteMeshConstDataProvider.PROVIDER_KEY,
            SpatialiteMeshConstDataProvider,
        )
        self.meshLayerType = MeshLayerType()
        QgsApplication.pluginLayerRegistry().addPluginLayerType(self.meshLayerType)

        # connect project opening to detect if it's a thyrsis project
        # to load result if needed
        QgsProject.instance().readProject.connect(self.qgisProjectLoaded)

        self.menu = QMenu("&Thyrsis", self)
        self.iface.mainWindow().menuBar().addMenu(self.menu)

        self.__sitesMenu = self.menu.addMenu(
            QIcon.fromTheme(
                "document-new", QIcon(complete_image_name("document-new.svg"))
            ),
            self.tr("New"),
        )
        self.__sitesMenu.aboutToShow.connect(self.updateSitesMenu)

        self.menu.addAction(
            QIcon.fromTheme(
                "document-open", QIcon(complete_image_name("document-open.svg"))
            ),
            self.tr("Open"),
        ).triggered.connect(self.openProject)
        self.menu.addAction(
            QIcon.fromTheme(
                "preferences-system",
                QIcon(complete_image_name("preferences-system.svg")),
            ),
            self.tr("Preferences"),
        ).triggered.connect(self.openPreferences)
        self.menu.addAction(
            QIcon(QgsApplication.iconPath("mActionHelpContents.svg")),
            self.tr("Help"),
        ).triggered.connect(
            partial(self.doc_helper.open_help, f"{self.locale}/index.html")
        )

        self._reload_data_control_toolbar()

        self.time_control_toolbar = QToolBar(self.tr("time_control_toolbar"))
        self.time_control_toolbar.setObjectName("time_control_toolbar")

        self.time_control_toolbar.addAction(
            QIcon(complete_image_name("config_simulation.svg")),
            self.tr("run_simulation"),
        ).triggered.connect(partial(self.runSimulation, True, True))

        self.time_control = TimeControl(blocker=True)
        self.time_control_toolbar.addWidget(self.time_control)

        self.iface.mapCanvas().renderComplete.connect(self.time_control.nextFrame)

        self.scaleAutoUpdateCheckBox = QCheckBox(self.tr("Adjust scale"))
        self.scaleAutoUpdateCheckBox.toggled.connect(self._auto_scale_toggled)
        self.time_control_toolbar.addWidget(self.scaleAutoUpdateCheckBox)
        self.iface.addToolBar(self.time_control_toolbar)

        self.result_control_toolbar = QToolBar(self.tr("result_control_toolbar"))
        self.result_control_toolbar.setObjectName("result_control_toolbar")
        self.iface.addToolBar(self.result_control_toolbar)

        self.result_control = ResultControl()
        self.result_control_toolbar.addWidget(self.result_control)

        action = self.result_control_toolbar.addAction(
            QIcon(complete_image_name("pick.svg")), self.tr("display value")
        )
        action.triggered.connect(self.pickButtonToggled)
        action.setCheckable(True)

        action = self.result_control_toolbar.addAction(
            QIcon(complete_image_name("plot.svg")), self.tr("plot")
        )
        action.triggered.connect(self.plot)
        action.setCheckable(True)

        self.result_control_toolbar.addAction(
            QIcon(complete_image_name("plot_bilan.svg")), self.tr("plot mass balance")
        ).triggered.connect(self.plot_bilan)

        # make sure we have the plugin dir in SVG paths
        svg_path = os.path.dirname(__file__)
        settings = QSettings()
        paths = settings.value("svg/searchPathsForSVG", svg_path)
        if paths is None:
            paths = [svg_path]
        elif svg_path not in paths:
            paths += "|" + svg_path
        settings.setValue("svg/searchPathsForSVG", paths)

        # No need to go further if the dependencies are not installed
        self.check_dependencies()

    def make_model(self):
        """Model creation dialog (from pilot point, permeability field or constant value"""
        if not self.project:
            return

        dlg = MakeModelDialog()
        if dlg.exec_():
            params = dlg.params()
            if not params[0]:
                self.inverse()
            elif params[0]:
                if type(params[1]) in (float, int):
                    pass
                else:
                    self.model_no_inverse(params[1], const=params[2])

    def model_no_inverse(self, param, const=True, test=False):
        """Launch creation model without inversion

        :param param: perm parameter (raster or constant values)
        :type param: string or float
        :param const: flag for constant value use
        :type const: bool
        :param test: flag to use when no GUI
        :type test: bool
        """
        if not self.project:
            return

        # chargement des constant layers
        layers = []
        for column, units in SpatialiteMeshConstDataProvider.sourceUnits.items():
            layer = get_layer(column, MeshLayer)
            if not layer and column != "permeabilite_x":
                project_SRID = self.project.get_project_SRID()
                layer = MeshLayer(
                    "dbname="
                    + self.project.database
                    + " crs=epsg:{} column=".format(project_SRID)
                    + column,
                    column,
                    SpatialiteMeshConstDataProvider.PROVIDER_KEY,
                )
                logger.notice("adds constant layer ", column)
                layer.colorLegend().setTitle(column)
                layer.colorLegend().setUnits(units)
                maxValue = layer.dataProvider().maxValue()
                layer.colorLegend().setMaxValue(layer.dataProvider().maxValue())
                if column == "v_norme":
                    layer.colorLegend().setLogScale(True)
                    layer.colorLegend().setMinValue(
                        float(self.__settings.value("Variables", "darcyScale"))
                        * layer.colorLegend().maxValue()
                    )
                else:
                    layer.colorLegend().setMinValue(layer.dataProvider().minValue())
                if not layer.isValid():
                    raise ModelCreationError(
                        f"Can't create mesh layer for column {column}"
                    )
                QgsProject.instance().addMapLayer(layer)
                layer_node = QgsProject.instance().layerTreeRoot().findLayer(layer.id())
                layer_node.setExpanded(False)
                layer_node.setItemVisibilityChecked(False)
            elif column == "permeabilite_x":
                if layer:
                    QgsProject.instance().removeMapLayer(layer.id())
                if const == False:
                    logger.notice("Add permeabilite field from " + param)
                layer = self.project.add_permeabilite(param, const)
                layer_node = QgsProject.instance().layerTreeRoot().findLayer(layer.id())
                layer.colorLegend().setLogScale(True)
                layer_node.setItemVisibilityChecked(True)
                layer_node.setExpanded(True)
                layer.dataProvider().update_values()
                min_ = layer.dataProvider().minValue()
                max_ = layer.dataProvider().maxValue()
                fmt = format_(min_, max_)
                layer.colorLegend().setMinValue(fmt % min_)
                layer.colorLegend().setMaxValue(fmt % max_)
                self.iface.mapCanvas().refresh()
            layers.append(layer)

        cmd = [
            "python3",
            "-u",
            "-m",
            "thyrsis.simulation.hynverse",
            "-v",
            "-r",
            "-perm",
            self.project.database,
        ]
        logger.debug("running task:", " ".join(cmd))
        my_env = os.environ.copy()
        # cleanup inverse data to avoid plotting old results
        self.project.execute("DELETE from zones")
        self.project.execute("DELETE from hynverse_erreurs")
        self.project.commit()

        my_env["PYTHONPATH"] = (
            my_env["PYTHONPATH"]
            + (";" if os.name == "nt" else ":")
            + os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
            if "PYTHONPATH" in my_env
            else "" + os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
        )

        if sys.platform.startswith("win"):
            my_env["PYTHONPATH"] += (
                os.pathsep + my_env["OSGEO4W_ROOT"] + "\\apps\\qgis\\python"
            )
            my_env["PYTHONPATH"] += (
                os.pathsep + my_env["OSGEO4W_ROOT"] + "\\apps\\qgis-ltr\\python"
            )

        def update_display_after_hynverse():
            self.timer.stop()
            for layer in layers:
                layer.dataProvider().update_values()
                if self.scaleAutoUpdateCheckBox.isChecked():
                    min_ = layer.dataProvider().minValue()
                    max_ = layer.dataProvider().maxValue()
                    fmt = format_(min_, max_)
                    layer.colorLegend().setMinValue(fmt % min_)
                    layer.colorLegend().setMaxValue(fmt % max_)
                elif layer.colorLegend().minValue() != layer.colorLegend().minValue():
                    min_ = layer.dataProvider().minValue()
                    max_ = layer.dataProvider().maxValue()
                    fmt = format_(min_, max_)
                    layer.colorLegend().setMinValue(fmt % min_)
                    layer.colorLegend().setMaxValue(fmt % max_)
            self.iface.mapCanvas().refresh()

        if not test:
            task = self.task_manager.widget().addTask(
                "hynverse", " ".join(cmd), cmd, env=my_env
            )
            task.finished.connect(update_display_after_hynverse)

        else:
            proc = Popen(cmd, env=my_env)
            proc.wait()
            update_display_after_hynverse()

    def inverse(self, test=False):
        """Launch creation model with inversion

        :param test: flag to use when no GUI
        :type test: bool
        """
        if not self.project:
            return

        # Check before lauching inversion
        # Check on points_pilote table
        layer = QgsProject.instance().mapLayersByName("points_pilote")
        if len(layer) == 0:
            project_SRID = self.project.get_project_SRID("points_pilote")
            layer = QgsVectorLayer(
                "dbname="
                + self.project.database
                + " crs=epsg:{} table=points_pilote".format(project_SRID),
                "points_pilote",
                "spatialite",
            )
            if layer.featureCount() == 0:
                self.iface.messageBar().pushMessage(
                    self.tr("Warning"),
                    self.tr(
                        "No pilot point found in points_pilote layer, inversion aborted"
                    ),
                    level=Qgis.Warning,
                )
            return
        elif layer[0].featureCount() == 0:
            self.iface.messageBar().pushMessage(
                self.tr("Warning"),
                self.tr(
                    "No pilot point found in points_pilote layer, inversion aborted"
                ),
                level=Qgis.Warning,
            )
            return

        # Check on infiltration parameter
        layer = QgsProject.instance().mapLayersByName("hynverse_parametres")
        if len(layer) == 0:
            layer = QgsVectorLayer(
                "dbname=" + self.project.database + " table=hynverse_parametres",
                "points_pilote",
                "spatialite",
            )
            if layer.featureCount() == 0:
                self.iface.messageBar().pushMessage(
                    self.tr("Warning"),
                    self.tr(
                        "No parameters found in hynverse_parametres layer, inversion aborted"
                    ),
                    level=Qgis.Warning,
                )
            elif (
                next(layer.getFeatures()).attribute(1) == 0
                and not os.path.exists(
                    os.path.join(
                        os.path.dirname(self.project.database), "u_infiltration.node.sat"
                    )
                )
                and not os.path.exists(
                    os.path.join(
                        os.path.dirname(self.project.database), "u_infiltration.elem.sat"
                    )
                )
            ):
                self.iface.messageBar().pushMessage(
                    self.tr("Warning"),
                    self.tr(
                        "Infiltration parameters in hynverse_parametres layer has 0 value but no infiltration user file is provided , inversion aborted"
                    ),
                    level=Qgis.Warning,
                )
                return
        elif (
            next(layer[0].getFeatures()).attribute(1) == 0
            and not os.path.exists(
                os.path.join(
                    os.path.dirname(self.project.database), "u_infiltration.node.sat"
                )
            )
            and not os.path.exists(
                os.path.join(
                    os.path.dirname(self.project.database), "u_infiltration.elem.sat"
                )
            )
        ):
            self.iface.messageBar().pushMessage(
                self.tr("Warning"),
                self.tr(
                    "Infiltration parameters in hynverse_parametres layer has 0 value but no infiltration user file is provided , inversion aborted"
                ),
                level=Qgis.Warning,
            )
            return

        # chargement des constant layers
        layers = []
        for column, units in SpatialiteMeshConstDataProvider.sourceUnits.items():
            layer = get_layer(column, MeshLayer)
            if not layer:
                project_SRID = self.project.get_project_SRID()
                layer = MeshLayer(
                    "dbname="
                    + self.project.database
                    + " crs=epsg:{} column=".format(project_SRID)
                    + column,
                    column,
                    SpatialiteMeshConstDataProvider.PROVIDER_KEY,
                )
                logger.notice("adds constant layer ", column)
                layer.colorLegend().setTitle(column)
                layer.colorLegend().setUnits(units)
                maxValue = layer.dataProvider().maxValue()
                layer.colorLegend().setMaxValue(layer.dataProvider().maxValue())
                if column == "v_norme":
                    layer.colorLegend().setLogScale(True)
                    layer.colorLegend().setMinValue(
                        float(self.__settings.value("Variables", "darcyScale"))
                        * layer.colorLegend().maxValue()
                    )
                else:
                    layer.colorLegend().setMinValue(layer.dataProvider().minValue())
                if not layer.isValid():
                    raise ModelCreationError(
                        f"Can't create mesh layer for column {column}"
                    )
                QgsProject.instance().addMapLayer(layer)
                layer_node = QgsProject.instance().layerTreeRoot().findLayer(layer.id())
                layer_node.setExpanded(False)
                layer_node.setItemVisibilityChecked(False)
                if column == "permeabilite_x":
                    layer.colorLegend().setLogScale(True)
                    layer_node.setItemVisibilityChecked(True)
                    layer_node.setExpanded(True)

            layers.append(layer)

        cmd = [
            "python3",
            "-u",
            "-m",
            "thyrsis.simulation.hynverse",
            "-v",
            "-r",
            self.project.database,
        ]
        logger.debug("running task:", " ".join(cmd))
        my_env = os.environ.copy()

        # Define min/max values for layer if not defined
        if math.isnan(self.project.permeabilite_x_layer.colorLegend().minValue()):
            self.project.permeabilite_x_layer.colorLegend().setMinValue(
                self.project.execute(
                    "SELECT permin FROM hynverse_parametres"
                ).fetchone()[0]
            )

        if math.isnan(self.project.permeabilite_x_layer.colorLegend().maxValue()):
            self.project.permeabilite_x_layer.colorLegend().setMaxValue(
                self.project.execute(
                    "SELECT permax FROM hynverse_parametres"
                ).fetchone()[0]
            )

        # cleanup inverse data to avoid plotting old results
        self.project.execute("DELETE from zones")
        self.project.execute("DELETE from hynverse_erreurs")
        self.project.commit()

        self.inverse_window and self.inverse_window.setParent(None)
        self.inverse_window = QDockWidget("Inversion")
        self.inverse_window.setObjectName("Inversion")
        self.inverse_window.setWidget(
            HynverseWindow(
                self.project.database,
                self.__settings,
                self.iface.mapCanvas(),
                self.project.permeabilite_x_layer,
                self.scaleAutoUpdateCheckBox,
            )
        )
        self.iface.addDockWidget(Qt.RightDockWidgetArea, self.inverse_window)
        self.timer.start(1000)
        self.timer.timeout.connect(self.inverse_window.widget().update)

        my_env["PYTHONPATH"] = (
            my_env["PYTHONPATH"]
            + (";" if os.name == "nt" else ":")
            + os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
            if "PYTHONPATH" in my_env
            else "" + os.path.join(os.path.abspath(os.path.dirname(__file__)), "..")
        )

        if sys.platform.startswith("win"):
            my_env["PYTHONPATH"] += (
                os.pathsep + my_env["OSGEO4W_ROOT"] + "\\apps\\qgis\\python"
            )
            my_env["PYTHONPATH"] += (
                os.pathsep + my_env["OSGEO4W_ROOT"] + "\\apps\\qgis-ltr\\python"
            )

        def update_display_after_hynverse():
            self.timer.stop()
            for layer in layers:
                layer.dataProvider().update_values()
                if self.scaleAutoUpdateCheckBox.isChecked():
                    min_ = layer.dataProvider().minValue()
                    max_ = layer.dataProvider().maxValue()
                    fmt = format_(min_, max_)
                    layer.colorLegend().setMinValue(fmt % min_)
                    layer.colorLegend().setMaxValue(fmt % max_)
                elif layer.colorLegend().minValue() != layer.colorLegend().minValue():
                    min_ = layer.dataProvider().minValue()
                    max_ = layer.dataProvider().maxValue()
                    fmt = format_(min_, max_)
                    layer.colorLegend().setMinValue(fmt % min_)
                    layer.colorLegend().setMaxValue(fmt % max_)
            self.iface.mapCanvas().refresh()

        if not test:
            task = self.task_manager.widget().addTask(
                "hynverse", " ".join(cmd), cmd, env=my_env
            )
            task.finished.connect(update_display_after_hynverse)
        else:
            proc = Popen(cmd, env=my_env)
            proc.wait()
            update_display_after_hynverse()

    def inverse_plot(self):
        """Open hynverse result plot"""
        if not self.project:
            return

        self.inverse_window and self.inverse_window.setParent(None)
        self.inverse_window = QDockWidget(self.tr("Inversion"))
        self.inverse_window.setObjectName(self.tr("Inversion"))
        self.inverse_window.setWidget(
            HynverseWindow(
                self.project.database,
                self.__settings,
                self.iface.mapCanvas(),
                self.project.permeabilite_x_layer,
                self.scaleAutoUpdateCheckBox,
            )
        )
        self.iface.addDockWidget(Qt.RightDockWidgetArea, self.inverse_window)

    def mesh(self):
        """Open mesh dialog, and then launch mesh process"""
        if not self.project or not isinstance(self.project, MeshProject):
            return

        dlg = MeshRasterDialog()
        if dlg.exec_():
            try:
                self.project.mesh(*dlg.layers())
            except MeshCreationError as exc:
                QMessageBox.critical(
                    self.iface.mainWindow(), self.tr("Mesh creation error"), str(exc)
                )

    def createLatinHypercube(self):
        """Open LatinHypercube dialog"""
        self.project.create_latin_hypercube()

    def createInjectionZones(self):
        """Create automatic injection zone from zones"""
        if QMessageBox.Ok == QMessageBox.warning(
            self.iface.mainWindow(),
            self.tr("Warning"),
            self.tr("This action removes existing injections"),
            QMessageBox.Ok | QMessageBox.Cancel,
        ):
            with wait_cursor():
                self.project.create_injection_zones()
                injectionsLayer = get_layer("injections")
                if injectionsLayer:
                    injectionsLayer.triggerRepaint()
                    self.crisis.widget()._CrisisWidget__setupInterface()

    def pickBoreHoleToggled(self, checked):
        """Activate borehole maptool to display borehole lithology

        :param checked: flag
        :type checked: bool
        """
        logger.debug("pickBoreHoleToggled:", checked)
        if checked:
            self.oldTool = self.iface.mapCanvas().mapTool()
            self.clickTool = PointTool(self.iface.mapCanvas())
            self.iface.mapCanvas().setMapTool(self.clickTool)
            self.clickTool.pointClicked.connect(self.displayBoreHole)
            self.litho.show()
            self.litho.raise_()
        else:
            self.iface.mapCanvas().setMapTool(self.oldTool)
            self.clickTool.pointClicked.disconnect()
            self.iface.mainWindow().findChild(QDockWidget, "Layers").raise_()

    def displayBoreHole(self, point):
        """Display borehole lithology

        :param point: click point
        :type point: QgsPoint
        """
        size = self.iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel() * 10
        database = (
            self.__settings.value("General", "defaultSite")
            if isinstance(self.project, MeshProject)
            else self.project.database
        )
        site_name = (
            self.__settings.value("General", "defaultSite")
            if isinstance(self.project, MeshProject)
            else self.project.site
        )

        project_SRID = self.project.get_project_SRID()

        sql = """
            SELECT OGC_FID, f.nom FROM forages as f
            WHERE PtDistWithin(st_transform(GEOMETRY, {}), MakePoint({}, {}, {}), {})
            """.format(
            project_SRID, point.x(), point.y(), project_SRID, size
        )

        for id_, borehole_name_, in (
            sqlite.connect(database).cursor().execute(sql).fetchall()
        ):
            if self.litho is not None:
                self.litho.widget().scene.set_current_id(id_)
                self.litho.show()
                self.litho.raise_()
            return True
        return False

    def _auto_scale_toggled(self):
        """
        Slot called when auto scale check box is toogled. Use to :
        - update min/max value for permeability layer if available

        """
        if self.scaleAutoUpdateCheckBox.isChecked():
            # If permeability layer available, update min/max values
            if self.project.permeabilite_x_layer is not None:
                min_ = self.project.permeabilite_x_layer.dataProvider().minValue()
                max_ = self.project.permeabilite_x_layer.dataProvider().maxValue()
                fmt = format_(min_, max_)
                self.project.permeabilite_x_layer.colorLegend().setMinValue(fmt % min_)
                self.project.permeabilite_x_layer.colorLegend().setMaxValue(fmt % max_)

    def pickButtonToggled(self, flag):
        """Activate maptool to display mesh value

        :param flag: flag
        :type flag: bool
        """
        logger.debug("flag:", flag)
        if flag:
            self.oldTool = self.iface.mapCanvas().mapTool()
            self.clickTool = PointTool(self.iface.mapCanvas())
            self.iface.mapCanvas().setMapTool(self.clickTool)
            self.clickTool.pointClicked.connect(self.displayValue)
        else:
            self.iface.mapCanvas().setMapTool(self.oldTool)
            self.clickTool.pointClicked.disconnect()

    def displayValue(self, point) -> bool:
        """Display mesh value

        :param point: click point
        :type point: QgsPoint
        """
        thrysis_providers = [
            SpatialiteMeshConstDataProvider.PROVIDER_KEY,
            SpatialiteMeshDataProvider.PROVIDER_KEY,
        ]
        thyrsis_layer = [
            l
            for l in self.iface.layerTreeView().selectedLayers()
            if l.dataProvider() and l.dataProvider().name() in thrysis_providers
        ]
        for layer in thyrsis_layer:
            provider = layer.dataProvider()
            val = None
            unit = None
            try:
                project_SRID = self.project.get_project_SRID()
                proj = SpatialReference()
                proj.ImportFromEPSG(int(project_SRID))
                if QgsCoordinateReferenceSystem(proj.ExportToWkt()) != self.project.crs:
                    transform = QgsCoordinateTransform(
                        self.project.crs,
                        QgsCoordinateReferenceSystem(proj.ExportToWkt()),
                        QgsProject.instance(),
                    )
                    point = transform.transform(QgsPointXY(point))
                vals = provider.valuesAt((point.x(), point.y()))
                if vals is not None:
                    if (
                        layer.dataProvider().name()
                        == SpatialiteMeshDataProvider.PROVIDER_KEY
                    ):
                        val = vals[provider.date()]
                        unit = provider.units()
                        date_val = provider.dates()[provider.date()]
                        date_str = datetime.strptime(
                            date_val, "%Y-%m-%d %H:%M:%S"
                        ).strftime("%d/%m/%Y %H:%M:%S")
                    else:
                        val = vals
                        unit = provider.sourceUnits[provider.resultColumn()]
                        date_str = None
                    msg = "{} = {:g} {} at point {:0.0f}, {:0.0f}".format(
                        provider.resultColumn(), val, unit, point.x(), point.y()
                    )
                    if date_str:
                        msg += " for date {}".format(date_str)
                    self.iface.messageBar().pushMessage(msg, level=Qgis.Info)
                    return True
            except RuntimeError as e:
                self.iface.messageBar().pushMessage(
                    self.tr("Warning"), str(e), level=Qgis.Warning
                )

        return False

    def exportRaster(self, fil=None):
        """Export the active layer as a image file"""
        # if not self.project.has_results:
        #    return
        layer = self.iface.activeLayer()
        if not layer:
            self.iface.messageBar().pushInfo("Info", "No active layer to export")
            return
        elif not isinstance(layer.dataProvider(), MeshDataProvider):
            self.iface.messageBar().pushInfo(
                "Info", "Export tool is available for mesh layer only"
            )
            return

        if not fil:
            fil, extension = QFileDialog.getSaveFileName(
                None,
                self.tr("Export raster"),
                os.path.join(
                    os.path.dirname(os.path.abspath(self.project.database)),
                    "{}".format(layer.name()),
                ),
                self.tr("PNG file (*.png);; Geotiff file (*.tif)"),
            )
        if not fil:
            return
        # default to png
        if len(fil) < 4 or fil[-4:] not in [".png", ".tif"]:
            fil += extension.split("*")[-1][:-1]

        if os.path.exists(fil):
            os.remove(fil)

        if fil[-4:] == ".png":
            if hasattr(self.project, "site"):
                site = self.project.site
            else:
                site = ""  # Mesh projects have no site attribute : leave it blank

            layer = self.iface.activeLayer()
            legend = (
                layer.colorLegend().image() if hasattr(layer, "colorLegend") else None
            )
            if hasattr(self.project, "result_layer"):
                date = (
                    self.time_control.dateComboBox.currentText()
                    if layer == self.project.result_layer
                    else ""
                )
            else:  # Mesh projects have no date attribute
                date = ""  # leave it blank
            legend_width = legend.size().width() if legend else 0

            fm = QFontMetrics(QFont())
            lw = max([legend_width, fm.width(date), fm.width(site)])
            image = QImage(
                QSize(lw, 0) + self.iface.mapCanvas().size(), QImage.Format_ARGB32
            )
            image.fill(self.iface.mapCanvas().backgroundBrush().color())
            painter = QPainter()
            painter.begin(image)
            self.iface.mapCanvas().render(painter)
            indent = image.size().width() - lw
            painter.drawText(indent, 30, site)
            painter.drawText(indent, 60, date)
            if legend:
                painter.drawImage(indent, 90, legend)
            painter.end()
            image.save(fil)

        elif fil[-4:] == ".tif":
            layer = self.iface.activeLayer()
            if layer.dataProvider().name() in [
                "thyrsis_const_provider",
                "thyrsis_provider",
            ]:
                res = self.chooseRasterResolution()  # Customised raster resolution
                layer_to_tif(layer.dataProvider(), res, fil)
            else:
                self.iface.messageBar().pushMessage(
                    self.tr("Error"),
                    self.tr(
                        "The raster export in tif format is only available for thyrsis mesh layer"
                    ),
                    level=Qgis.Info,
                )

    def chooseRasterResolution(self):
        """Open a dialog before saving a .tif image file to define the resolution image"""
        resolution_dlg = RasterResolutionDialog()
        resolution_dlg.show()
        if resolution_dlg.exec_():
            return resolution_dlg.doubleSpinBox.value()

    def composition(self):
        """Build a layout to create animation

        :return: layout parameter
        :rtype: dict
        """
        composition = (
            {
                "Thyrsis_2d": {
                    "size": self.iface.mainWindow().centralWidget().size(),
                    "pos": self.iface.mainWindow().centralWidget().pos(),
                }
            }
            if self.iface.mapCanvas().size().width() > 100
            else {}
        )

        for c in self.iface.mainWindow().findChildren(QDockWidget):
            if (
                c.objectName() in ["Thyrsis_1d", "Thyrsis_zns", "Thyrsis_3d"]
                and not c.visibleRegion().isEmpty()
            ):
                pos = QPoint(c.geometry().x(), c.geometry().y())
                if c.objectName() == "Thyrsis_1d":
                    for gc in self.plw.findChildren(QDockWidget):
                        if not gc.visibleRegion().isEmpty():
                            logger.debug("Thyrsis_1d", gc.objectName(), gc.geometry())
                            composition["Thyrsis_1d " + gc.objectName()] = {
                                "size": gc.size(),
                                "pos": pos
                                + QPoint(gc.geometry().x(), gc.geometry().y()),
                                "widget": gc.widget(),
                            }

                else:
                    composition[c.objectName()] = {"size": c.size(), "pos": pos}

        dimensions = numpy.array(
            [
                (
                    value["pos"].x(),
                    value["pos"].y(),
                    value["pos"].x() + value["size"].width(),
                    value["pos"].y() + value["size"].height(),
                )
                for value in list(composition.values())
            ]
        )
        upper_left = QPoint(numpy.min(dimensions[:, 0]), numpy.min(dimensions[:, 1]))
        lower_right = QPoint(numpy.max(dimensions[:, 2]), numpy.max(dimensions[:, 3]))
        extend = lower_right - upper_left
        size = QSize(extend.x(), extend.y())
        for key in composition:
            composition[key]["pos"] -= upper_left
        composition["size"] = QSize(extend.x(), extend.y())

        return composition

    def exportVideo(self, fil=None, composition=None, test=False):
        """Export project visualization as a video file (if results layer exists)"""
        if not self.project.has_results:
            return

        if not fil:
            fil, __ = QFileDialog.getSaveFileName(
                None,
                self.tr("Video file"),
                os.path.join(
                    os.path.dirname(os.path.abspath(self.project.database)), "video.avi"
                ),
                self.tr("video file (*.avi, *.mp4, *.mpeg)"),
            )

        if not fil:
            return

        if len(fil) <= 4 or fil[-4:] not in [".avi", ".mpeg", ".mp4"]:
            self.iface.messageBar().pushMessage(
                self.tr("Error"),
                self.tr(
                    "The video export file must have a recognized extension (.avi, .mpeg ou .mp4)"
                ),
                level=Qgis.Critical,
            )
            return

        if os.path.exists(fil):
            os.remove(fil)
        fps = float(self.__settings.value("Animation", "framePerSecond", 10))
        temdir = tempfile.mkdtemp()
        basename = os.path.split(os.path.splitext(fil)[0])[1]

        self.exportAnimation(
            os.path.join(temdir, basename + ".png"), composition=composition, test=test
        )
        out, err = Popen(
            [
                "ffmpeg",
                "-y",
                "-r",
                str(fps),
                "-i",
                os.path.join(temdir, basename + "_%04d.png"),
                fil,
            ],
            stdout=PIPE,
            stderr=PIPE,
            stdin=PIPE,
        ).communicate()
        logger.debug(err)
        logger.debug(out)

    def exportAnimation(self, fil=None, composition=None, test=False):
        """Export project animation as a image file (if results layer exists)

        :param fil: file path
        :type fil: string
        """
        if not self.project.has_results:
            return

        numeric_filename = fil is not None

        if not fil:
            fil, __ = QFileDialog.getSaveFileName(
                None,
                self.tr("Image file"),
                os.path.join(
                    os.path.dirname(os.path.abspath(self.project.database)), "video.png"
                ),
                self.tr("image file (*.png *.jpg)"),
            )

            if not fil:
                return

        if len(fil) <= 4 or fil[-4:] not in [".jpg", ".png"]:
            self.iface.messageBar().pushMessage(
                self.tr("Error"),
                self.tr(
                    "The animation export file must have a recognized extension (.jpg or .png)"
                ),
                level=Qgis.Critical,
            )
            return

        if not composition:
            composition = self.composition()

        ext = fil[-4:]
        fil = fil[:-4]

        # stop animation if running and stop normal play function of the
        # pause/play button
        self.time_control.suspendPlayFunction()
        self.time_control.playButton.setChecked(True)
        created_files = []
        created_3d_files = []
        for file_idx, date in enumerate(
            range(self.time_control.value(), self.time_control.maximum() + 1)
        ):
            # if user clicked pause animation
            if not self.time_control.playButton.isChecked():
                break

            self.time_control.setValue(date)
            if not test:
                QApplication.instance().processEvents()
                while self.iface.mapCanvas().isDrawing():
                    QApplication.instance().processEvents()

            filename = (
                fil
                + "_"
                + datetime.strptime(self.time_control.text(), "%d/%m/%Y").strftime(
                    "%Y-%m-%d"
                )
                + ext
                if not numeric_filename
                else "%s_%04d.png" % (fil, file_idx)
            )

            logger.debug("saving " + filename)
            image = QImage(composition["size"], QImage.Format_ARGB32)
            painter = QPainter()
            painter.begin(image)
            image.fill(self.iface.mapCanvas().backgroundBrush().color())
            painter.setBackgroundMode(Qt.OpaqueMode)
            painter.setBackground(QBrush(QColor(255, 255, 255)))

            if "Thyrsis_2d" in composition:
                legend = self.project.result_layer.colorLegend().image()
                fm = QFontMetrics(QFont())
                font = painter.font()
                font.setPointSizeF(font.pointSizeF() * 1.5)
                painter.setFont(font)
                painter.translate(composition["Thyrsis_2d"]["pos"])
                self.iface.mapCanvas().render(painter)
                painter.translate(-composition["Thyrsis_2d"]["pos"])
                painter.drawImage(QPoint(10, 40), legend)

            if "Thyrsis_3d" in composition:
                painter.drawImage(
                    composition["Thyrsis_3d"]["pos"],
                    self.tw.widget().image(composition["Thyrsis_3d"]["size"]),
                )

            if "Thyrsis_zns" in composition:
                painter.translate(composition["Thyrsis_zns"]["pos"])
                painter.scale(0.5, 0.5)
                self.zns.widget().scene.render(painter)
                painter.scale(2, 2)
                painter.translate(-composition["Thyrsis_zns"]["pos"])

            for key in composition:
                if key[:10] == "Thyrsis_1d":
                    _fil, tmpfile = tempfile.mkstemp(suffix=".png")
                    composition[key]["widget"].save(
                        tmpfile, composition[key]["size"], 100
                    )
                    painter.drawImage(composition[key]["pos"], QImage(tmpfile))
                    painter.drawText(
                        composition[key]["pos"] + QPoint(0, 10),
                        composition[key]["widget"].windowTitle(),
                    )

            painter.drawText(10, 30, self.time_control.text())

            painter.end()
            image.save(filename)
            created_files.append(filename)

        # reconnect the normal play function
        self.time_control.playButton.setChecked(False)
        self.time_control.restorePlayFunction()
        return created_files

    def runSimulation(self, init=True, run=True):
        """Launch simultation

        :param init: init flag
        :type init: bool
        :param run: run flag
        :type run: bool
        """
        if not self.project:
            return
        res = get_layer("resultats", MeshLayer)
        if res:
            if "probability" in [i[1] for i in res.dataProvider().availableColumns()]:
                res.dataProvider().removeColumn("probability")
            QgsProject.instance().removeMapLayer(res.id())
        try:
            # check if user has fulfill simulation parameters.
            if (
                len(
                    [
                        i.attribute(2)
                        for i in QgsProject.instance()
                        .mapLayersByName("parametres")[0]
                        .getFeatures()
                        if i.attribute(2) != 0.0
                    ]
                )
                == 0
            ):
                self.iface.messageBar().pushWarning(
                    "thyrsis",
                    self.tr(
                        "All simulation parameters are equal to 0.0 , please fulfill 'parametres' table"
                    ),
                )
                return None
            update = (
                self.crisis.widget().updateDatabase(update_injection_geometries=False)
                if init
                else True
            )
            if update:
                self.project.simulate(init, run)
        except RuntimeError as e:
            self.iface.messageBar().pushCritical(self.tr("Error"), str(e))

    def plot_bilan(self):
        """Display mass balance plot"""
        if not self.plw:
            return

        self.plw.show()
        self.plw.raise_()

        labels = [
            self.tr("ZNS mass"),
            self.tr("ZS mass"),
            self.tr("Output mass"),
            self.tr("Total mass"),
            self.tr("mass "),
        ]
        plot = TimePlot.mass_balance(
            self.project.result_layer.dataProvider(), self.__settings, labels
        )
        if not self.plw.widget().add_plot(self.tr("Balance"), plot):
            self.iface.messageBar().pushMessage(
                self.tr("Warning"),
                self.tr("No mass balance for this study"),
                level=Qgis.Warning,
            )

    def plot(self, flag):
        """Display plot dock and activate maptool to display plot

        :param flag: flag
        :type flag: bool
        """
        if not self.plw:
            return

        self.plw.show()
        self.plw.raise_()

        def name_sorted(tbl):
            srt = sorted(tbl)  # alphabetical sort
            # bublle sort by numery value if numeric pattern found in name
            finished = False
            while not finished:
                finished = True
                for i in range(len(srt) - 1):
                    m0 = re.match(r"^([A-z_]*)([0-9]+)", srt[i])
                    m1 = re.match(r"^([A-z_]*)([0-9]+)", srt[i + 1])
                    if (
                        m0
                        and m1
                        and m0.group(1) == m1.group(1)
                        and int(m0.group(2)) > int(m1.group(2))
                    ):
                        finished = False
                        srt[i], srt[i + 1] = srt[i + 1], srt[i]
            return srt

        column = self.result_control.columnComboBox.itemData(
            self.result_control.columnComboBox.currentIndex()
        )
        timer = Timer()
        colorsMeasures = {}  # stocke les couleurs de chaque type de mesure
        colors = (
            "#ff0000",
            "#00ff00",
            "#0000ff",
            "#ffa500",
            "#9acd32",
            "#a020f0",
            "#ffc0cb",
            "#00ffff",
            "#ee82ee",
            "#fa8072",
            "#40e0d0",
            "#a52a2a",
            "#ffff00",
        )

        points = {}
        i = 0
        if (
            self.iface.activeLayer()
            and self.iface.activeLayer().type() == QgsMapLayer.VectorLayer
        ):
            for feature in self.iface.activeLayer().selectedFeatures():
                point = feature.geometry().asPoint()
                if "nom" in [f.name() for f in feature.fields()]:
                    points[feature["nom"]] = (point.x(), point.y())
                else:
                    i += 1
                    points[self.tr("unnamed %d") % (i)] = (point.x(), point.y())

        if len(points):
            # then all points
            for name in name_sorted(list(points.keys())):
                try:
                    timer.reset()
                    plot = TimePlot.create(
                        self.project.result_layer.dataProvider(),
                        points[name],
                        name,
                        colorsMeasures,
                        colors,
                        self.__settings,
                    )
                    self.plw.widget().add_plot(name, plot)
                except RuntimeError as e:
                    self.iface.messageBar().pushMessage(
                        self.tr("Warning"), str(e), level=Qgis.Warning
                    )
            self.sender().setChecked(False)

        else:
            if flag:
                self.oldTool = self.iface.mapCanvas().mapTool()
                self.clickTool = PointTool(self.iface.mapCanvas())
                self.iface.mapCanvas().setMapTool(self.clickTool)
                self.clickTool.pointClicked.connect(self.add_plots)
            elif self.oldTool:
                self.iface.mapCanvas().setMapTool(self.oldTool)
                self.clickTool.pointClicked.disconnect()

    def add_plots(self, point):
        """Add plots to the plot dock

        :param point: click point
        :type point: QgsPoint
        """

        size = self.iface.mapCanvas().getCoordinateTransform().mapUnitsPerPixel() * 10
        colorsMeasures = {}  # stocke les couleurs de chaque type de mesure
        colors = (
            "#ff0000",
            "#00ff00",
            "#0000ff",
            "#ffa500",
            "#9acd32",
            "#a020f0",
            "#ffc0cb",
            "#00ffff",
            "#ee82ee",
            "#fa8072",
            "#40e0d0",
            "#a52a2a",
            "#ffff00",
        )
        project_SRID = self.project.get_project_SRID()
        proj = SpatialReference()
        proj.ImportFromEPSG(int(project_SRID))
        if QgsCoordinateReferenceSystem(proj.ExportToWkt()) != self.project.crs:
            transform = QgsCoordinateTransform(
                self.project.crs,
                QgsCoordinateReferenceSystem(proj.ExportToWkt()),
                QgsProject.instance(),
            )
            point = transform.transform(QgsPointXY(point))
        point_interet = self.project.execute(
            """
                SELECT nom, X(GEOMETRY), Y(GEOMETRY) FROM points_interet
                WHERE PtDistWithin(GEOMETRY, MakePoint({}, {}, {}), {})
                """.format(
                point.x(), point.y(), project_SRID, size
            )
        ).fetchall()
        logger.debug(
            "type point_interet : {}, longueur {}, valeur {}".format(
                type(point_interet), len(point_interet), point_interet
            )
        )
        provider = self.project.result_layer.dataProvider()
        if point_interet:
            nom, x, y = point_interet[0]
        else:
            x = point.x()
            y = point.y()
            nom = "({:0.2f}, {:0.2f})".format(x, y)

        plot = TimePlot.create(
            provider,
            (x, y),
            nom,
            colorsMeasures,
            colors,
            self.__settings,
        )
        self.plw.widget().add_plot(nom, plot)

    def exportTemplate(self, flag=None):
        """Export simulation template

        :param flag: unused
        :type flag: bool
        """
        self.project.export_simulation()

    def openProject(self, flag=None):
        """Open QFileDialog to seek project

        :param flag: unused
        :type flag: bool
        """
        fil, __ = QFileDialog.getOpenFileName(
            None,
            self.tr("Open a computational database"),
            os.path.dirname(os.path.abspath(self.project.database))
            if self.project
            else os.path.abspath("."),
            self.tr("project file (*.sqlite)"),
        )

        if not fil:
            return

        if not os.path.exists(fil):
            self.iface.messageBar().pushMessage(
                self.tr("Error"),
                self.tr("File {} does not exist").format(fil),
                level=Qgis.Critical,
            )
            return

        if os.path.exists(fil[:-7] + ".qgs"):
            if QMessageBox.No == QMessageBox.question(
                None,
                self.tr("Warning"),
                self.tr("The .qgs project file exists, to you want to overwrite it ?"),
                QMessageBox.No | QMessageBox.Yes,
            ):
                return
            os.remove(fil[:-7] + ".qgs")

        project_mesh = "mesh" in os.path.basename(fil)

        self.iface.newProject()
        try:
            if project_mesh:
                self.project = MeshProject.load(
                    fil, self.__settings, self.iface.mapCanvas()
                )
                logger.debug("mesh project loaded")
                self.createMeshProjectDependantWidgets()
            else:
                self.project = Project.load(
                    fil, self.__settings, self.iface.mapCanvas()
                )
                logger.debug("project loaded")
                self.createProjectDependantWidgets()

            [
                self.layout_watch(l.name)
                for l in QgsProject.instance().layoutManager().layouts()
            ]
            QgsProject.instance().layoutManager().layoutAdded.connect(self.layout_watch)
        except ProjectLoadError as exc:
            QMessageBox.critical(
                self.iface.mainWindow(), self.tr("Mesh project load error"), str(exc)
            )

    def layout_watch(self, name):
        layout = QgsProject.instance().layoutManager().layoutByName(name)
        layout.refreshed.connect(self.last_layout)

    def last_layout(self):
        for l in QgsProject.instance().layoutManager().layouts():
            if self.sender() == l:
                # QgsRenderContext is a singleton
                QgsRenderContext.last_layout = l

    def zoomFull(self, dummy=None):
        """Unused"""
        contours = QgsProject.instance().mapLayersByName("contours")[0]
        contours.updateExtents()
        self.iface.setActiveLayer(contours)
        # next 3 lines: hack to ensure the zoom is taken into account
        QApplication.instance().processEvents()
        while self.iface.mapCanvas().isDrawing():
            QApplication.instance().processEvents()
        self.iface.actionZoomToLayer()

    def updateSitesMenu(self):
        """Populate site menu"""
        self.__sitesMenu.clear()
        mesh_dir = self.__settings.value("General", "defaultMeshDir")
        site_db = self.__settings.value("General", "defaultSite")
        with sqlite.connect(site_db, "plugin:updateSitesMenu") as conn:
            cur = conn.cursor()
            for [site, id_] in cur.execute("SELECT nom, id FROM sites").fetchall():
                menu = self.__sitesMenu.addMenu(site)
                subtitle = menu.addAction(self.tr("Models:"))
                ft = subtitle.font()
                ft.setBold(True)
                subtitle.setFont(ft)
                for file_ in os.listdir(mesh_dir):
                    m = re.match(r"^%s_?(.*)\.mesh.sqlite" % (site), file_)
                    if m:
                        mesh_name = m.group(1) or "defaut"
                        action = QAction(mesh_name, menu)
                        action.setData(
                            {"site": site, "mesh": os.path.join(mesh_dir, file_)}
                        )
                        action.triggered.connect(self.importSite)
                        menu.addAction(action)

                self.__sitesMenu.addSeparator()
                subtitle = menu.addAction(self.tr("Studies:"))
                ft = subtitle.font()
                ft.setBold(True)
                subtitle.setFont(ft)
                cur.execute(
                    "SELECT nom, nom_db_maillage FROM simulations WHERE sid=%d" % (id_)
                )
                for [simulation, mesh] in cur.fetchall():
                    action = QAction(simulation, menu)
                    action.setData(
                        {
                            "site": site,
                            "simulation": simulation,
                            "mesh": os.path.join(mesh_dir, mesh),
                        }
                    )
                    action.triggered.connect(self.importSite)
                    menu.addAction(action)
        self.__sitesMenu.addAction(self.tr("Mesh...")).triggered.connect(self.newMesh)

    def newMesh(self):
        """Open dialog to create a new mesh project and create mesh database"""
        project_srid = QgsProject.instance().crs()
        dlg = SridConfirmDialog(str(project_srid).split(":")[-1][:-1])
        confirm = dlg.exec_()
        if confirm:
            fil, __ = QFileDialog.getSaveFileName(
                None,
                self.tr("New mesh database"),
                self.__settings.value("General", "defaultMeshDir"),
                self.tr("SpatiaLite file (*.mesh.sqlite)"),
            )
            if not fil:
                return
            if fil[-12:] != ".mesh.sqlite":
                fil += ".mesh.sqlite"

            self.iface.newProject()
            QgsProject().instance().setCrs(project_srid)

            if os.path.exists(fil):
                os.remove(fil)
            try:
                self.project = MeshProject.create(
                    fil, self.__settings, self.iface.mapCanvas()
                )
                self.createMeshProjectDependantWidgets()
            except ProjectLoadError as exc:
                QMessageBox.critical(
                    self.iface.mainWindow(),
                    self.tr("Mesh project load error"),
                    str(exc),
                )

    def importSite(self, flag):
        """Open dialog to create a new computational project

        :param flag: unused
        :type flag: bool
        """
        project_srid = QgsProject.instance().crs()
        dlg = SridConfirmDialog(str(project_srid).split(":")[-1][:-1], calc_DB=True)
        confirm = dlg.exec_()
        if confirm:
            fil, __ = QFileDialog.getSaveFileName(
                None,
                self.tr("New computational database"),
                os.path.abspath(self.project.database if self.project else "."),
                self.tr("SpatiaLite file (*.sqlite)"),
            )
            if not fil:
                return
            if fil[-7:] != ".sqlite":
                fil += ".sqlite"

            self.iface.newProject()
            QgsProject().instance().setCrs(project_srid)

            # make sure all references to the SQLite file is closed before deleting it
            self.removeProjectDependantWidgets()
            if self.project is not None:
                self.project.__del__()
            self.project = None

            if os.path.exists(fil):
                logger.debug("### remove file", fil)
                os.remove(fil)

            databasePath = os.path.dirname(os.path.abspath(fil))
            if not self.__settingsModified and os.path.isfile(
                os.path.join(fil, ".thyrsis.ini")
            ):
                self.__settings = Settings(databasePath)
            else:
                self.__settings.save(databasePath)

            try:
                self.project = Project.create(
                    fil,
                    self.__settings,
                    self.sender().data()["mesh"],
                    self.sender().data()["simulation"]
                    if "simulation" in self.sender().data()
                    else None,
                    self.iface.mapCanvas(),
                )

                self.createProjectDependantWidgets()
            except ProjectCreationError as exc:
                QMessageBox.critical(
                    self.iface.mainWindow(),
                    self.tr("Project creation error"),
                    str(exc),
                )

    def layerWillBeRemoved(self, layerId):
        """Triggered when a layer will be removed

        :param layerId: layer id
        :type layerId: string
        """
        # if QgsProject.instance().mapLayer(layerId).dataProvider().name()== 'thyrsis_const_provider' : #not sure about the new condition but avoid texture not deleted
        if QgsProject.instance().mapLayer(layerId).dataProvider().name() in [
            "thyrsis_const_provider",
            "thyrsis_provider",
        ]:
            QgsProject.instance().mapLayer(layerId).clean_texture()
        if (
            self.project
            and isinstance(self.project, Project)
            and QgsProject.instance().mapLayer(layerId) == self.project.result_layer
        ):
            logger.debug("removing ", layerId)
            self.minMaxUpdater and self.minMaxUpdater.setParent(None)
            self.minMaxUpdater = None
            self.titleSetter and self.titleSetter.setParent(None)
            self.titleSetter = None
            self.result_control.setProvider(None)
            self.time_control.setDates([])

            if self.plw:
                self.plw.widget().clear()
                self.iface.removeDockWidget(self.plw)
                self.plw.setParent(None)
                self.plw = None

            if self.tw:
                self.iface.removeDockWidget(self.three_d_controls)
                self.three_d_controls.setParent(None)
                self.three_d_controls = None
                self.iface.removeDockWidget(self.tw)
                self.tw.widget().cleanTextures()
                self.tw.setParent(None)
                self.tw = None

            if self.zns:
                self.iface.removeDockWidget(self.zns)
                self.zns.setParent(None)
                self.zns.widget().__del__()
                self.zns = None
            if hasattr(self.project.result_layer.dataProvider(), "PROVIDER_KEY"):
                self.project.result_layer.dataProvider().conn().close()

    def layersAdded(self, layers):
        """Triggered when layers are added

        :param layers: layers
        :type layers: list
        """
        for layer in layers:
            if (
                hasattr(layer, "dataProvider")
                and hasattr(layer.dataProvider(), "PROVIDER_KEY")
                and layer.dataProvider().PROVIDER_KEY == "thyrsis_provider"
            ):
                # setup ui according to layer settings
                dates = layer.dataProvider().dates()
                self.time_control.setDates(layer.dataProvider().dates())
                self.time_control.setValue(layer.dataProvider().date())
                self.time_control.setFramePerSecond(
                    int(self.__settings.value("Animation", "framePerSecond", 10))
                )
                self.time_control.setDatabasePath(
                    layer.dataProvider().getDatabasePath()
                )
                self.result_control.setProvider(layer.dataProvider())

                # connect special signals to layer
                self.time_control.timeChanged.connect(layer.dataProvider().setDate)
                self.titleSetter = TitleSetter(layer.colorLegend(), self)
                layer.dataProvider().columnChanged.connect(self.titleSetter.update)
                layer.dataProvider().unitsChanged.connect(layer.colorLegend().setUnits)
                layer.colorLegend().maskUnits(
                    int(self.__settings.value("Variables", "maskUnitsCheck"))
                )
                self.maskUnits.connect(layer.colorLegend().maskUnits)

                # create associated ZNS
                zns = ZnsWindow(
                    "dbname=" + layer.dataProvider().database(), self.iface.messageBar()
                )
                # setup zns columns
                if not zns.scene.empty():
                    # connect UI to znsScene
                    zns.scene.dataProvider().setDate(self.time_control.value())
                    self.time_control.timeChanged.connect(
                        zns.scene.dataProvider().setDate
                    )
                    self.maskUnits.connect(zns.scene.maskUnits)
                    self.zns = QDockWidget("ZNS")
                    self.zns.setObjectName("Thyrsis_zns")
                    self.zns.setWidget(zns)
                    self.iface.addDockWidget(Qt.RightDockWidgetArea, self.zns)
                else:
                    zns.setParent(None)
                    zns = None

                # 3D
                self.tw = QDockWidget("3D")
                self.tw.setObjectName("Thyrsis_3d")
                viewer3d = Viewer3d(
                    layer.dataProvider(),
                    layer.colorLegend(),
                    zns.scene.dataProvider() if zns else None,
                    zns.scene.colorLegend() if zns else None,
                )
                self.tw.setWidget(viewer3d)
                self.three_d_controls = QDockWidget(self.tr("3D parameters"))
                self.three_d_controls.setObjectName("Thyrsis_3d_parameter")
                self.three_d_controls.setWidget(ViewerControls(viewer3d, self.iface))
                self.iface.addDockWidget(Qt.LeftDockWidgetArea, self.three_d_controls)
                self.iface.addDockWidget(Qt.RightDockWidgetArea, self.tw)
                self.iface.mainWindow().tabifyDockWidget(
                    self.iface.mainWindow().findChild(QDockWidget, "Layers"),
                    self.three_d_controls,
                )
                if self.crisis is not None:
                    self.crisis.show()
                    self.crisis.raise_()

                if zns:
                    self.iface.mainWindow().tabifyDockWidget(self.tw, self.zns)

                # update min/max on unit change

                minMaxUpdater = MinMaxUpdater(
                    layer,
                    self.result_control,
                    self.time_control,
                    self.scaleAutoUpdateCheckBox,
                    self,
                )
                self.minMaxUpdater = minMaxUpdater
                minMaxUpdater.update()
                layer.dataProvider().unitsChanged.connect(minMaxUpdater.update)
                layer.dataProvider().columnChanged.connect(minMaxUpdater.update)

                self.time_control.timeChanged.connect(minMaxUpdater.autoUpdate)
                self.scaleAutoUpdateCheckBox.toggled.connect(minMaxUpdater.update)

                self.plw = QDockWidget("1D")
                self.plw.setObjectName("Thyrsis_1d")
                self.plw.resize(800, 600)
                self.plw.setWidget(TimePlotWidget(self.time_control))
                self.iface.addDockWidget(Qt.RightDockWidgetArea, self.plw)
                self.iface.mainWindow().tabifyDockWidget(self.tw, self.plw)
                self.plw.hide()

    def createIsovalues(self, pixels_per_meter=0):
        """Create isovalue from result layer

        :param pixel_per_meter: used for image output
        :type pixel_per_meter: int
        """
        layer = self.iface.activeLayer()
        date = ""
        if not (
            hasattr(layer, "dataProvider")
            and hasattr(layer.dataProvider(), "PROVIDER_KEY")
            and layer.dataProvider().PROVIDER_KEY == "thyrsis_const_provider"
            and hasattr(layer, "colorLegend")
        ):
            if (
                self.project.has_results
                and self.project.result_layer.dataProvider().resultColumn()
            ):
                layer = self.project.result_layer
                date = self.time_control.dateComboBox.currentText()
            else:
                self.iface.messageBar().pushMessage(
                    self.tr("Error"),
                    self.tr("You must choose a valid layer first"),
                    level=Qgis.Warning,
                )
                return

        isovalues = None
        provider = layer.dataProvider()
        column = provider.resultColumn().replace("vitesse_darcy_norme", "darcy")
        nbClasses = (
            int(self.__settings.value("Variables", column + "Classes"))
            if column in ("potentiel", "darcy", "concentration")
            else 10
        )
        scale = "log" if column in ("darcy", "concentration") else "lin"

        isovalues = QgsProject.instance().readEntry("thyrsis", column + "Isovalues")[0]

        # isovalues stored as a space separated list with the last element beeing the unit
        values_default, unit_default, scale_default = (
            layer.colorLegend().values(nbClasses),
            layer.colorLegend().units(),
            "log" if layer.colorLegend().hasLogScale() else "linear",
        )
        values, unit, scale = (
            (values_default, unit_default, scale_default)
            if not isovalues
            else (
                [float(v) for v in isovalues.split()[:-2]],
                isovalues.split()[-2],
                isovalues.split()[-1],
            )
        )

        values, unit, scale, nbPixelsPerMeter = IsovalueDialog(
            values, unit, scale, pixels_per_meter
        ).exec_()
        if not values:  # user cancel
            return

        # store isovalues settings in project
        strIso = " ".join([str(v) for v in values]) + " " + unit + " " + scale
        QgsProject.instance().writeEntry("thyrsis", column + "Isovalues", strIso)

        # the requested isovalues may not have the same unit as the
        # displayed one, we must convert
        conversionFactor = provider.conversionFactor(unit)
        values = [v / conversionFactor for v in values]

        if nbPixelsPerMeter == 0:
            lines = layer.isovalues(values)
            fmt = format_(min(values), max(values)) + " " + unit

            # add isovalues to the db
            did = provider.date() + 1
            try:
                self.project.add_isovalues(
                    did,
                    date,
                    column,
                    lines,
                    [value * provider.conversionFactor() for value in values],
                    [fmt % (value * conversionFactor) for value in values],
                )
            except IsoValueCreationError as exc:
                QMessageBox.critical(
                    self.iface.mainWindow(),
                    self.tr("IsoValue creation error"),
                    str(exc),
                )

        else:
            from osgeo import ogr

            # génération d'un raster
            current_dir = os.path.dirname(os.path.abspath(self.project.database))
            raster_file = os.path.join(current_dir, column + ".tif")
            layer_to_tif(provider, nbPixelsPerMeter, raster_file)

            # gdal_contour
            contour_file = os.path.join(current_dir, column + ".gpkg")
            values.sort()
            tmp_raster_file = os.path.join(
                os.path.dirname(raster_file), "temp_concentration.tif"
            )
            factor = (
                "1e" + str(values[0]).split("e-")[-1]
                if len(str(values[0]).split("e-")) == 2
                else "1"
            )
            cmd = [
                "gdal_calc.py",
                "-A",
                raster_file,
                "--outfile",
                tmp_raster_file,
                "--calc",
                "A*" + factor,
            ]
            logger.notice(" ".join(cmd))
            Popen(cmd).communicate()
            tmp_values = [v * float(factor) for v in values]
            # cmd = ['gdal_contour', '-a', 'elev', raster_file, contour_file, '-fl']+[str(v) for v in values]
            # logger.notice(" ".join(cmd))
            cmd = [
                "gdal_contour",
                "-a",
                "elev",
                tmp_raster_file,
                contour_file,
                "-fl",
            ] + [str(v) for v in tmp_values]
            logger.notice(" ".join(cmd))

            Popen(cmd).communicate()

            # actualize contour value with factor
            inverse_factor = float("1e-" + factor.split("e")[-1])
            ds = ogr.Open(contour_file, 1)
            lyr = ds.GetLayerByIndex(0)
            fieldDefn = ogr.FieldDefn("concentr", ogr.OFTReal)
            fieldDefn.SetWidth(20)
            fieldDefn.SetPrecision(18)
            lyr.CreateField(fieldDefn)
            for feat in lyr:
                val = feat.GetField("elev") * inverse_factor
                feat.SetField("concentr", val)
                lyr.SetFeature(feat)
            ds = None

            # chargement dans QGIS
            self.iface.addVectorLayer(contour_file, column, "ogr")

    def probability(self, seuil=None):
        """Create probability map, according to result layer and a threshold (several simulation are needed)"""
        if (
            self.project.has_results
            and self.project.result_layer.dataProvider().resultColumn()
        ):
            layer = self.project.result_layer
            date = self.time_control.dateComboBox.currentText()
            name = self.project.result_layer.dataProvider().resultColumn()
        else:
            self.iface.messageBar().pushMessage(
                self.tr("Warning"), self.tr("No valid result layer"), level=Qgis.Warning
            )
            return

        codeHydro = self.__settings.value("General", "codeHydro")
        # if not os.path.isfile(codeCommand):
        #     raise RuntimeError(self.tr("unable to find simulation code: ")+codeCommand)

        dbname = self.project.database
        if codeHydro == "metis":
            codeCommand = self.__settings.value("General", "codeHydroCommand")
            code = Metis(dbname, codeCommand)
        elif codeHydro == "openfoam":
            code = OpenFoam(dbname, self.__settings)
        else:
            raise NotImplementedError(self.tr("simulation code not implemented"))

        dir_name = os.path.dirname(os.path.abspath(dbname))
        basename = os.path.basename(dbname)[:-7]
        tmpdir = os.path.join(dir_name, basename + "_tmp")

        with sqlite.connect(self.project.database, "plugin:probability") as conn:
            cur = conn.cursor()
            nsamples = cur.execute(
                "SELECT COUNT(1) FROM parametres_simulation"
            ).fetchone()[0]
            (nbNodes_sat,) = cur.execute("SELECT COUNT(1) FROM noeuds").fetchone()
            (nbElem_sat,) = cur.execute("SELECT COUNT(1) FROM mailles").fetchone()

        variables_sat = {
            "concentration": "nodes",
            "potentiel": "nodes",
            "darcy": "mailles",
        }
        if name in list(variables_sat.keys()):
            nbVal_sat = nbNodes_sat if variables_sat[name] == "nodes" else nbElem_sat
        else:
            self.iface.messageBar().pushMessage(
                self.tr("Warning"),
                self.tr("Cannot compute probability map for the layer ") + name,
                level=Qgis.Warning,
            )
            return

        if nsamples > 1:
            if not seuil:
                seuil = 0.0
                unit = layer.dataProvider().units()
                seuil = ProbabilityDialog(seuil, unit).exec_()
                logger.notice("seuil = ", seuil, layer.dataProvider().sourceUnits[name])
                if not seuil:
                    return
        else:
            self.iface.messageBar().pushMessage(
                self.tr("Warning"),
                self.tr(
                    "Several sets of parameters are needed to obtain a probability map"
                ),
                level=Qgis.Warning,
            )
            return

        dates = read_one_column_file(os.path.join(tmpdir, "dates_simulation.txt"))
        nbDates = len(dates)
        proba = numpy.zeros((nbDates, nbVal_sat), dtype=numpy.float32)
        for sample_idx in range(nsamples):

            sample_dir = os.path.join(tmpdir, "sample%05d" % (sample_idx + 1))
            if not os.path.isdir(sample_dir):
                raise RuntimeError(
                    self.tr("directory {} doesn't exist").format(sample_dir)
                )

            # saturé
            sat_dir = os.path.join(sample_dir, "sature")
            if not os.path.isdir(sat_dir):
                raise RuntimeError(
                    self.tr("directory {} doesn't exist").format(sat_dir)
                )

            sat_file = os.path.join(sat_dir, name + ".sat")
            sat_flag = os.path.exists(sat_file)
            if sat_flag:
                results = code.read_file(sat_file, dates)
                proba[results >= seuil] += 1

        proba = proba / nsamples
        numpy.save(os.path.join(dir_name, basename + ".probability.node"), proba)

        self.project.add_results()
        self.result_control.columnComboBox.setCurrentIndex(
            self.result_control.columnComboBox.findText("Probabilité")
        )

    def second_milieu(self, flag=None):
        """Use the current project to create a new computational database with a dual-porisity simulation"""
        if not self.project:
            return

        if self.project.has_second_milieu():
            logger.error(self.tr("project already has a second medium"))
            return

        fil, __ = QFileDialog.getSaveFileName(
            None,
            self.tr("New computational database"),
            os.path.abspath(self.project.database if self.project else "."),
            self.tr("SpatiaLite file (*.sqlite)"),
        )
        if not fil:
            return
        if fil[-7:] != ".sqlite":
            fil += ".sqlite"

        initial_db = str(self.project.database)
        if os.path.abspath(initial_db) != os.path.abspath(fil):
            shutil.copy2(initial_db, fil)

        try:
            add_second_milieu(fil)
            QMessageBox.information(
                self.iface.mainWindow(),
                "Les tables suivantes ont été copiées et/ou modifiées dans la base :",
                "noeuds_second_milieu\nmailles_second_milieu\n"
                + "parametres\nparametres_simulation\nsimulations_second_milieu\ninjections_second_milieu\n\n"
                + "Il est maintenant nécessaire de créer un nouveau projet avec la nouvelle base (Thyrsis>Ouvrir)",
            )

            if os.path.exists(fil[:-7] + ".qgs"):
                os.remove(fil[:-7] + ".qgs")

            # NOTE: weird bug when trying to do that, use openProject afterward instead
            # self.iface.newProject()
            # self.project = Project.load(fil, self.iface.mapCanvas())
            # logger.debug("project loaded")
            # self.createProjectDependantWidgets()
        except SecondMilieuCreationError as exc:
            QMessageBox.critical(
                self.iface.mainWindow(),
                self.tr("Seconde milieu creation error"),
                str(exc),
            )

    def radionuclide(self):
        if not self.project:
            return

        if QgsProject.instance().mapLayersByName("elements_chimiques"):
            lyr = QgsProject.instance().mapLayersByName("elements_chimiques")[0]
        else:
            return

        dlg = ChemicalManagementDlg(lyr, self.project)
        dlg.chem_list_changed.connect(self.crisis.widget().fillChemCombobox)
        dlg.exec_()
