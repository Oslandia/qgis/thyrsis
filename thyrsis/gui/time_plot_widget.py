from qgis.PyQt.QtCore import QSize, Qt
from qgis.PyQt.QtWidgets import (
    QDialogButtonBox,
    QDockWidget,
    QFileDialog,
    QMainWindow,
    QMenu,
    QToolBar,
    QToolButton,
)

from .time_plot import AdjustWidget, TimePlot
from .time_plot_control import TimePlotControl


class TimePlotWidget(QMainWindow):
    """Time plot panel"""

    def __init__(self, time_control, parent=None):
        """Constructor

        :param time_control: time_control widget
        :type time_control: TimeControl
        :param parent: Qt parent
        :type parent: QtWidget
        """
        super(TimePlotWidget, self).__init__(parent)
        toolbar = QToolBar()
        self.addToolBar(toolbar)
        adj = QToolButton()
        adj.setText(self.tr("Fit"))
        adj.setMenu(QMenu())
        toolbar.addWidget(adj)
        toolbar.addAction(self.tr("Style")).triggered.connect(self.__show_style)
        adj.menu().addAction(self.tr("fit all")).triggered.connect(self.__fit_them_all)
        adj.menu().addAction(self.tr("vertical fit")).triggered.connect(
            self.__fit_them_all_y
        )
        adj.menu().addAction(self.tr("horizontal fit")).triggered.connect(
            self.__fit_them_all_x
        )
        adj.menu().addAction(self.tr("RAZ")).triggered.connect(self.__reset_them_all)
        adj.setPopupMode(QToolButton.InstantPopup)
        toolbar.addAction(self.tr("Save")).triggered.connect(self.__save_them_all)
        toolbar.addAction(self.tr("Close")).triggered.connect(self.__hide)

        self.plot_docks = []
        self.__time_control = time_control

        self.ctrl = TimePlotControl()
        self.ctrl.buttonBox.clicked.connect(self.__line_style)

    def __hide(self):
        self.clear()
        self.parent().hide()

    def __show_style(self):
        self.ctrl.show()

    def __line_style(self, btn):
        if self.ctrl.buttonBox.standardButton(btn) != QDialogButtonBox.Cancel:
            for child in self.findChildren(TimePlot):
                child.set_linestyle(
                    style=self.ctrl.style.currentText(),
                    width=self.ctrl.width.value(),
                    color=self.ctrl.color.color().name(),
                    marker=self.ctrl.marker.currentText(),
                )

    def __fit_them_all(self):
        for child in self.findChildren(AdjustWidget):
            child.adjustXY()

    def __fit_them_all_x(self):
        for child in self.findChildren(AdjustWidget):
            child.adjustX()

    def __save_them_all(self):
        basename, __ = QFileDialog.getSaveFileName(
            None,
            self.tr("File basename (with extension)"),
            self.__time_control.databasePath(),
            self.tr("Image files (*.png, *.jpg)"),
        )

        if not basename:
            return

        for o in self.plot_docks:
            o.widget().save(
                basename[:-4] + o.objectName() + basename[-4:], QSize(800, 600)
            )

    def __fit_them_all_y(self):
        for child in self.findChildren(AdjustWidget):
            child.adjustY()

    def __reset_them_all(self):
        for child in self.findChildren(AdjustWidget):
            child.reset()

    def clear(self):
        for plt in self.plot_docks:
            plt.setParent(None)
        self.plot_docks = []

    def add_plot(self, name, plot):
        if not plot:
            return False

        for o in self.plot_docks:
            if name == o.objectName():
                o.show()
                o.raise_()
                return True

        plot.set_linestyle(
            style=self.ctrl.style.currentText(),
            width=self.ctrl.width.value(),
            color=self.ctrl.color.color().name(),
            marker=self.ctrl.marker.currentText(),
        )

        self.__time_control.timeChanged.connect(plot.set_end)
        dock = QDockWidget(name)
        dock.setObjectName(name)
        self.plot_docks.append(dock)
        dock.setWidget(plot)
        self.addDockWidget(Qt.LeftDockWidgetArea, dock)
        if len(self.plot_docks) > 1:
            self.tabifyDockWidget(self.plot_docks[-2], self.plot_docks[-1])
            self.plot_docks[-1].show()
            self.plot_docks[-1].raise_()

        return True


if __name__ == "__main__":
    import sys

    from qgis.PyQt.QtWidgets import QApplication

    app = QApplication(sys.argv)
    w = TimePlotWidget(None)
    w.show()
    app.exec_()
