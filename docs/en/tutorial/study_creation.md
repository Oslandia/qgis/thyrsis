---
substitutions:
  img_run_simulation: |-
    :::{image} /_static/images/config_simulation.png
    :scale: 10 %
    :::
---

# Study creation

A study is created from a model and describes one or several injections on the ground surface, inside the unsaturated zone or directly into the groundwater. Transient infiltration can also be modeled.

## Computational database

A new study is created by using the *Thyrsis* menu, selecting the *New* menu and the name of the model previously created ({numref}`tutorial_newStudy`).

:::{figure} /_static/images/tutorial_newStudy.png
:name: tutorial_newStudy
New study creation
:::

A project name must be chosen and a study directory where the computational database and all the results will be saved.

THYRSIS creates a new QGIS project and load it ({numref}`tutorial_newStudyCanvas`).

:::{figure} /_static/images/tutorial_newStudyCanvas.png
:name: tutorial_newStudyCanvas
QGIS canvas at the creation of a new study
:::

The model is displayed and a new tab is opened on the left.

## Simulation parameters

The study uses the default parameters associated to the hydrogeolgical models (cf {ref}`Model parameters <model_parameters>`) but these parameters can be modified either directly by modifying the **parametres** table, or by editing the {ref}`Latin Hypercube <latin_hypercube>` menu (*Thyrsis>Create>Latin Hypercube*) which can also be used to generate several sets of parameters in the context of uncertainty study. For each parameter a reference value, a minimal and a maximal value and a variation law should be defined.

For the tutorial simulations, we suggest the parameters of {numref}`tutorial_parameters`.

:::{figure} /_static/images/tutorial_parameters.png
:name: tutorial_parameters
:scale: 50 %

Parameters definition for a new study
:::

## Injection

{ref}`Injection characteristics<injection_characteristics_en>` are defined through the {ref}`THYRSIS panel<thyrsis_panel>` described in the user manual.

For this tutorial we will define a HTO flux on an unique injection zone.

The HTO is selected via the *chemical element* combobox.

The injection is defined by clicking on the *Flux* button ({numref}`tutorial_injectionButtons`).

:::{figure} /_static/images/tutorial_injectionButtons.png
:name: tutorial_injectionButtons
Injection buttons
:::

and filling the form with the following values :

- Begin : 01/01/2012
- Duration : 0.00021 year (can also be defined in month, or day)
- Flux : $9.8 10^{-9}$ (enter `9.8e-9`)
- Area : 59 (or use the pen that allow to draw the area on the map)
- Location : use the ${\odot}$ button and click on map to define center of the injection
- Depth : 0
- Water volume : 0

Then click on the Save button : the injection zone is added to the injection layer.

## Simulation and results

To launch simulation, click on the engine button {{ img_run_simulation }}. After the calculation, a *resultats* layer is added to the canvas.

The ZNS tab is opened showing simulated concentrations displayed on an 1D column ({numref}`tutorial_ZNStab`).

:::{figure} /_static/images/tutorial_ZNStab.png
:name: tutorial_ZNStab
:scale: 50 %

ZNS tab
:::

The time control bar ({numref}`tutorial_timeControlBar`) and the result control bar ({numref}`tutorial_resultControlBar`) of the THYRSIS menu bar are updated :

- the time combobox is filled with the simulation dates,
- the result combobox and the unit combobox are updated with the informations of the simulated variables.

:::{figure} /_static/images/tutorial_timeControlBar.png
:name: tutorial_timeControlBar
Time control bar
:::

:::{figure} /_static/images/tutorial_resultControlBar.png
:name: tutorial_resultControlBar
Result control bar
:::

The results can be displayed in the canvas ({numref}`tutorial_resultCanvas`) and in the 1D window ({numref}`tutorial_1D`) at any simulation date through the time combobox or by using the arrows in the menu bar.

:::{figure} /_static/images/tutorial_resultCanvas.png
:name: tutorial_resultCanvas
QGIS canvas with simulation result
:::

:::{figure} /_static/images/tutorial_1D.png
:name: tutorial_1D
:height: 300 px
:width: 300 px

1D graphic
:::

at any simulation date through the time combobox or by using the arrows in the menu bar.

Futhermore a 3D canvas can be used to display results ({numref}`tutorial_3D`).

:::{figure} /_static/images/tutorial_3D.png
:name: tutorial_3D
3D graphic
:::

:::{note}
3D scene can be configured through the 3D parameters tab. To display OpenStreetMap background map on 3D scene, add OpenStreetMap to the QGIS canvas and activate *Texture* check box.
:::
