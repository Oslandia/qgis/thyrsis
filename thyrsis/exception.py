class MeshCreationError(RuntimeError):
    pass


class InvalidGmshResult(MeshCreationError):
    pass


class DatabaseMeshLayerCreationError(MeshCreationError):
    pass


class ProjectLoadError(RuntimeError):
    pass


class ModelCreationError(RuntimeError):
    pass


class ProjectCreationError(RuntimeError):
    pass


class SimulationError(RuntimeError):
    pass


class IsoValueCreationError(RuntimeError):
    pass


class MeshProviderInitializationError(RuntimeError):
    pass


class ZnsDisplayError(RuntimeError):
    pass


class SecondMilieuCreationError(RuntimeError):
    pass


class DatabaseUpgradeError(RuntimeError):
    pass


class MeasureLoadError(RuntimeError):
    pass
