# CentOS et distributions apparentées

:::{note}
Selon la version de CentOS, le nom des paquets peut changer. Les paquets à installer sont détaillés dans [yum_requirements.txt](https://gitlab.com/Oslandia/qgis/thyrsis/-/blob/master/requirements/yum.txt)
:::

Avec certaines versions (CentOS6 par exemple), il n'est pas possible d'accéder au répertoire caché depuis l'explorateur de fichier QGIS. Afin de contourner cette limitation, il est recommandé de créer un lien symbolique entre le répertoire d'installation du plugin et un répertoire accessible, par exemple :

```sh
ln -s .local/share/QGIS/QGIS3/profiles/default/python/plugins/thyrsis ~/.
```
