"""Thyrsis unit test for mesh building

.. note:: This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
"""

import os
import unittest

import pytest
from qgis.core import QgsProject, QgsVectorLayer

TEST_DIR = os.path.join(os.path.dirname(__file__), os.path.splitext(__file__)[0])
TUTORIAL_DATA_DIR = os.path.join(os.path.dirname(__file__), "../..", "tutorial_data")
TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "../..", "test_data")

SITE_DB = os.path.join(TEST_DIR, ".thyrsis", "sites.sqlite")
MESH_DB = os.path.join(TEST_DIR, ".thyrsis", "mesh", "SACLAY_mesh.mesh.sqlite")


@pytest.fixture()
def test_dir() -> str:
    return TEST_DIR


@pytest.fixture()
def mesh_db() -> str:
    return MESH_DB


@pytest.fixture()
def site_db() -> str:
    return SITE_DB


@pytest.mark.parametrize(
    "mesh_generator",
    ["gmsh", "cfmesh"]
)
def test_mesh_project_creation(clean_test_dir, mesh_project, mesh_generator):
    """
    Test default file creation for an empty mesh project

    @param clean_test_dir: fixture to have a clean test directory
    @param mesh_project: fixture to create a default mesh project and initialize thyrsis plugin
    """
    # Mesh database is created
    assert os.path.exists(MESH_DB)
    # A Qgis project is created
    assert os.path.exists(os.path.splitext(MESH_DB)[0] + ".qgs")
    # A default contours layer is available
    assert type(QgsProject.instance().mapLayersByName("contours")[0]) == QgsVectorLayer


@pytest.mark.parametrize(
    "mesh_generator",
    ["gmsh", "cfmesh"]
)
def test_mesh_creation_no_point_fixe(clean_test_dir, create_mesh, mesh_generator):
    """
    Test mesh creation without point fixe values

    @param clean_test_dir: fixture to have a clean test directory
    @param create_mesh: fixture to create mesh from tutorial data
    """
    # Check no points_fixes are available
    db_lyr_pf = QgsProject.instance().mapLayersByName("points_fixes")[0]
    assert len([i for i in db_lyr_pf.getFeatures()]) == 0

    db_lyr_noeuds = QgsProject.instance().mapLayersByName("noeuds")[0]
    db_lyr_mailles = QgsProject.instance().mapLayersByName("mailles")[0]

    assert len([i for i in db_lyr_noeuds.getFeatures()]) > 700
    if mesh_generator == 'gmsh':
        assert len([i for i in db_lyr_mailles.getFeatures()]) > 1400
    else:
        assert len([i for i in db_lyr_mailles.getFeatures()]) > 1110



@pytest.mark.parametrize(
    "mesh_generator",
    ["gmsh", "cfmesh"]
)
def test_mesh_creation_with_point_fixe(clean_test_dir, copy_point_fixes, create_mesh, mesh_generator):
    """
    Test mesh creation without point fixe values

    @param clean_test_dir: fixture to have a clean test directory
    @param copy_point_fixes: fixture to copy point fixe from tutorial data
    @param create_mesh: fixture to create mesh from tutorial data
    """
    # Check points_fixes are available
    db_lyr_pf = QgsProject.instance().mapLayersByName("points_fixes")[0]
    assert len([i for i in db_lyr_pf.getFeatures()]) == 17

    db_lyr_noeuds = QgsProject.instance().mapLayersByName("noeuds")[0]
    db_lyr_mailles = QgsProject.instance().mapLayersByName("mailles")[0]

    assert len([i for i in db_lyr_noeuds.getFeatures()]) > 840
    if mesh_generator == 'gmsh':
        assert len([i for i in db_lyr_mailles.getFeatures()]) > 1600
    else:
        assert len([i for i in db_lyr_mailles.getFeatures()]) > 1110



if __name__ == "__main__":
    unittest.main()
