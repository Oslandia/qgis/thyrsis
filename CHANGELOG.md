# Changelog

This file is inspired from [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), this
project respects the [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- ## Unreleased

### Added

### Modified

### Fixed -->

## 2.3.0 - 2025-02-14

This version contains several enhancement and fixes for PMF v2406.2 use.

Several version of the plugin are also released:

- thyrsis.2.2.0.zip : no embedded libraries are available in the archive. We are using [qpip](https://github.com/opengisch/qpip) to install the dependencies. They will be installed at first launch.
- thyrsis.2.2.0.python-v3.9.5.zip : embedded libraries are available in the archive. The libraries are compatible with python 3.9.5.
- thyrsis.2.2.0.python-v3.12.3.zip : embedded libraries are available in the archive. The libraries are compatible with python 3.12.3.

### Added

- replacement of eventFileInfiltration by infiltrationEventFile in simulation/openfoam.py !146
- Add specific parameters in `openfoam_options` table to manage some specific cases (h_relaxationFactor_transient, picard_tolerance_transient, picard_maxiter_transient) !144
- Upgrade `fvSolution` for `groundwaterFoam`/`groundwaterTransportFoam` and add `minIter` parameter !144
- Replace MIN by AVG for thickness compute in `create_injection_zones.py` !144
- Add display option in `simulation/compute_validation.py` and transform seconds in years.
- feat(docker): use new version v2406.1 for PMF docker image !150
- docs: release docs dependencies upper versions !151
- update transient infiltration file documentation !154
- Doc processing functions !155
- Update(tooling): update vs code config and some tooling !156
- Removing wm !157
- feat(ci): create embedded_libs for several python version !161
- feat(docker): update for PMF v2406.2 / QGIS 3.34.1 / OpenFoam 2406 !164
- feat(openfoam): update for 2406 use !165
- Calcul phi !162

### Fixed

- fixing reading u_infiltration.node.sat and u_infiltration.elem.sat for inversion !158
- fix(mesh): don't update id for created layers use Autogenerate feature !149
- fix(doc): sphinx-autobuild infinite run !148
- fix(shapely): support for 2.0.0 version and MultiLineString !147
- Fix `update_injections` and `create_injection_triggers.sql` for injection characterisitics definition : use mailles_injections instead of noeuds_injections. !144
- Fix database upgrade function in case of schema modification !144
- replace min or max value when set to None (table parametres, bug _import_simulation !152
- Purge simulations !153
- fixing nprocs computation when nbColumns = 0 !159
- fix(shapely): crash when contours intersects !160
- fix(mpi): need to use PATH and add QGIS to PYTHONPATH for windows !163

## 2.2.0 - 2023-12-07

- Add point load in Thyrsis menu #16
- Add forages load in Thyrsis menu #17
- Add measure load in Thyrsis menu #18
- Fix use for int argument with pyQt upgrade !133 !131
- Fix documentation hyperlink !132
- Use button for delete instead of text !128
- Indicate error for invalid values for injection area !127
- Limit values for injections !126
- Display value for point for all thyrsis layers !124
- Add option in PMF docker file image to define used version !121
- Use new PMF version !120
- Fix mpiexec run !119
- Add support for second minute hour duration definition in injection UI !117 !116
- Remove/replace every assert instructions #14
- feat(mpi): automatic mpi protocol definition from MPI vendor !138
- feat(injection): check line edit values !139
- fix(mpi): use MSMPI_BIN environnement variable to get mpiexec on Windows !143

## 2.1.1 - 2022-10-10

- fix translation generation on CI !104
- fixing number of arguments of mhynos_results when used alone !109
- improving mesh_zns to avoid millimetric cells !109
- fixing read_openfoam_file to manage columns with few cells (which are written differently - on one line) !109
- Improving hynverse plots and adding settings for HynverseWindow !108
- Adding min values of colorLegend for layers after meshing (altitude, altitude_mur, potentiel_reference) !108
- Taking into account reference potential when calculating imposed potential after meshing !108

## 2.1.0 - 2022-09-22

Release 2.1.0.

- update french documentation !92
- replace gnuplot use by matplotlib !99
- update krtheta to use linear schema !98
- update thyrsis.database.build_model for windows use !102 !103

## 2.0.0 - 2022-05-19
Release 2.0.0.

### Added

- mesh with OpenFOAM `cfMesh` (need OpenFOAM-v2112 on Windows)
- bibtext integration for documentation references

### Modified

- QGIS minimum version is now 3.22

### Fixed

- default min/max value for permeability layer
- piezo curve display when several group are used for pilot points definition
- portalocker import on windows

## 2.0.0-rc1 - 2022-02-22
Release candidate 2.0.0-rc1 for test of new openfoam integration on Windows

### Added

- windows support of openfoam without WSL use (pre-compiled openfoam.com version used)
- windows packaging with external libraries integration
- openfoam and PMF simulation code doesn't need to be in PATH, paths must be defined in Thyrsis configuration
- add QGIS processing for chemicals import into site database

### Modified

- update logo
- review of unit test to use pytest and remove tests dependencies
- update documentation for markdown use
- update qgis plugin structure
- new packaging procedure : use of [qgis-plugin-ci](https://github.com/opengisch/qgis-plugin-ci)
- new docker image for unit tests run on debian
- docker image is now stored in [gitlab containter registry](https://gitlab.com/Oslandia/qgis/thyrsis/container_registry)
- update import from experimental database for new station_type (`Smokestack` instead of `Chimney`)
- remove point of interest use if outside mesh
- temporal graph can be display for any point on the mesh and not just the point of interest

### Fixed

- installation error because of SIP
- import from osgeo.osr instead of osr in python
- fix nodes contour calculation, some node where not used (update of buffer for search to 1 m)
- fix import of sites from experimental database (invalid UNION)
- fix database update from user files

## 1.3.1 - 2021-07-16

- openfoam template

## 1.3.0 - 2021-07-05

- raster export improvement
- update documentation
- Fix bugs when loading data from expdb to site database (some sites without a kind of station, missing domaine table in site db)
- Fix bug when rendering mesh legend node in layout.

## 1.2.0 - 2021-02-15

- Experimental database can be used to fill site database or computational database
- Chemical element management has been changed and a form has been added to add or remove a chemical.
- OpenFOAM and METIS templates

## 1.1.1-beta1 - 2020-12-16

THYRSIS v1.1.1beta can be use for QGIS version >= 3.16

## 1.1.1-alpha1 - 2020-12-16

This version can be used with QGIS software from 3.10 to 3.14. Some changes in QGIS 3.16 break the API functionnalities. If you use a QGIS version >= 3.16, please use THYRSIS v1.1.1beta or a version > 1.1.1

- mesh layer metadata are added to fix crash when closing THYRSIS project.

## 1.1.0 (2020-09-10)

First release. Features are detailed in https://oslandia.gitlab.io/qgis/thyrsis/en/html/
