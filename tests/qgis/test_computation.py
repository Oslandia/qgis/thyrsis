"""Thyrsis unit test for computational database test

.. note:: This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
"""

import os
import shutil
import unittest
from datetime import datetime
from distutils.spawn import find_executable
from functools import partial
from pathlib import Path

import pytest
from pytest import approx
from qgis.core import (
    QgsCoordinateReferenceSystem,
    QgsPoint,
    QgsPointXY,
    QgsProject,
    QgsRectangle,
    QgsVectorLayer,
)
from qgis.PyQt.QtCore import QPoint, QSize
from qgis.PyQt.QtWidgets import QDockWidget

from tests.qgis.test_hynverse import define_hynverse_parameters
from thyrsis.database import add_second_milieu, sqlite
from thyrsis.gui.create_latin_hypercube import HypercubeDialog
from thyrsis.gui.crisis_dialog import CrisisWidget
from thyrsis.gui.isovalue_dialog import IsovalueDialog
from thyrsis.gui.probability_dialog import ProbabilityDialog
from thyrsis.gui.time_plot import TimePlot
from thyrsis.meshlayer.meshlayer import MeshLayer
from thyrsis.meshlayer.utilities import format_
from thyrsis.project import Project
from thyrsis.settings import Settings
from thyrsis.spatialitemeshdataprovider import SpatialiteMeshDataProvider

TEST_DIR = os.path.join(os.path.dirname(__file__), os.path.splitext(__file__)[0])
TUTORIAL_DATA_DIR = os.path.join(os.path.dirname(__file__), "../..", "tutorial_data")
TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "../..", "test_data")

SITE_DB = os.path.join(TEST_DIR, ".thyrsis", "sites.sqlite")
MESH_DB = os.path.join(TEST_DIR, ".thyrsis", "mesh", "SACLAY_mesh.mesh.sqlite")
COMPUTATION_DB = os.path.join(TEST_DIR, ".thyrsis", "etude", "SACLAY_etude.sqlite")


@pytest.fixture()
def test_dir():
    return TEST_DIR


@pytest.fixture()
def mesh_db():
    return MESH_DB


@pytest.fixture()
def site_db():
    return SITE_DB


@pytest.fixture()
def mesh_generator():
    return "gmsh"


@pytest.fixture()
def setup_mesh_project(
    mesh_project,
    thyrsis,
    load_chemicals_csv,
    load_forages_csv,
    copy_point_pilotes,
    create_mesh_project,
):
    """
    Fixture to setup a mesh project with all needed data for computation tests

    @param mesh_project: fixture used to create a default mesh project
    @param thyrsis: fixture to get thyrsis plugin
    @param load_chemicals_csv: fixture to insert chemical into site database
    @param load_forages_csv: fixture to insert forage into site database
    @param copy_point_pilotes: fixture to copy point pilote to database for inversion
    @param create_mesh_project: fixture to use mesh project in thyrsis plugin and configure openfoam
    """
    infiltration = 4.75e-09
    define_hynverse_parameters(MESH_DB, infiltration)
    thyrsis.inverse(test=True)


@pytest.fixture()
def create_computation_project(openfoam_project_dir, pmf_install_dir):
    """
    Fixture to create computation project with openfoam configuration

    @param openfoam_project_dir: openfoam project directory fixture
    @param pmf_install_dir: pmf install directory fixture
    """
    Path(COMPUTATION_DB).parent.mkdir(parents=True, exist_ok=True)
    settings = Settings(os.path.dirname(COMPUTATION_DB))
    settings.setValue("General", "codeHydro", "openfoam")
    settings.setValue("General", "openfoam", openfoam_project_dir)
    settings.setValue("General", "pmf", pmf_install_dir)
    codeHydroPath = find_executable("openfoam")
    settings.setValue("General", "codeHydroCommand", codeHydroPath)
    settings.setValue("General", "debug", 1)
    settings.setValue("General", "console", 1)
    settings.save(os.path.dirname(COMPUTATION_DB))
    Project.create(COMPUTATION_DB, settings, MESH_DB)


def test_check_site_db_creation(clean_test_dir, setup_mesh_project):
    """
    Check site db exist after mesh project setup

    @param clean_test_dir: fixture to have a clean test directory
    @param setup_mesh_project: fixture to setup mesh_project
    """
    assert os.path.exists(SITE_DB)


def test_computation_project_creation(
    clean_test_dir, setup_mesh_project, create_computation_project
):
    """
    Check if computation project files are created

    @param clean_test_dir: fixture to have a clean test directory
    @param setup_mesh_project: fixture to setup mesh_project
    @param create_computation_project: fixture to setup computation project
    """
    assert os.path.exists(MESH_DB)
    assert os.path.exists(os.path.splitext(MESH_DB)[0] + ".qgs")
    assert type(QgsProject.instance().mapLayersByName("contours")[0]) == QgsVectorLayer


@pytest.mark.dependency(name="create_injection")
def test_configuration_injection(
    clean_test_dir, setup_mesh_project, create_computation_project, mock_iface
):
    """
    Test injection configuration widget

    @param clean_test_dir: fixture to have a clean test directory
    @param setup_mesh_project: fixture to setup mesh_project
    @param create_computation_project: fixture to setup computation project
    @param mock_iface: QgsInterface mock
    """
    _configuration_injection(mock_iface)


def _configuration_injection(mock_iface):
    """
    Test configuration widget and add a final flux injection

    @param mock_iface: QgsInterface mock
    """
    QgsProject.instance().setCrs(QgsCoordinateReferenceSystem("EPSG:27572"))
    crisis_widget = CrisisWidget(COMPUTATION_DB, mock_iface)

    ind = crisis_widget.chemicalComboBox.findText("HTO")
    crisis_widget.chemicalComboBox.setCurrentIndex(ind)
    assert crisis_widget.chemicalComboBox.currentText() == "HTO"

    crisis_widget.durationsLayout.itemAt(0).widget().durationEdit.set_duration(
        "20 year"
    )

    # no injection is filled
    assert crisis_widget.injectionsLayout.count() == 0

    # Concentration
    crisis_widget.concentrationButton.clicked.emit()

    # concentration injection is selected
    assert crisis_widget.injectionsLayout.count() == 1
    crisis_widget.injectionsLayout.itemAt(0).widget().durationEdit.set_duration(
        "0.00021 year"
    )
    _define_injection(crisis_widget, 0.041263158, 50, 586181, 2414034)

    expected_simulations_table_res = (
        "SACLAY",
        "SACLAY_mesh.mesh.sqlite",
        1,
        "concentration",
    )
    expected_injections_table_res = (
        "0.00021 year",
        "Sans nom",
        0.0,
        1.0,
        1.0,
        4.02e-05,
        4.75e-09,
        49.91,
        156.0,
        0.041263158,
    )
    _check_simulations_and_injections_table(
        expected_injections_table_res, expected_simulations_table_res
    )

    # delete
    _delete_injection(crisis_widget)
    assert crisis_widget.injectionsLayout.count() == 0

    # Mass
    crisis_widget.massButton.clicked.emit()
    assert crisis_widget.injectionsLayout.count() == 1

    crisis_widget.slEdit.setText("0.041263158")
    _define_injection(crisis_widget, 1, 50, 586181, 2414034)

    expected_simulations_table_res = ("SACLAY", "SACLAY_mesh.mesh.sqlite", 1, "masse")
    expected_injections_table_res = (
        "0 year",
        "Sans nom",
        0.0,
        1.0,
        1.0,
        4.02e-05,
        4.75e-09,
        49.91,
        156.0,
        1.0,
    )
    _check_simulations_and_injections_table(
        expected_injections_table_res, expected_simulations_table_res
    )

    # delete
    _delete_injection(crisis_widget)
    assert crisis_widget.injectionsLayout.count() == 0

    # Flux
    crisis_widget.fluxButton.clicked.emit()
    assert crisis_widget.injectionsLayout.count() == 1

    crisis_widget.injectionsLayout.itemAt(0).widget().durationEdit.set_duration(
        "0.00021 year"
    )
    _define_injection(crisis_widget, 9.8e-09, 50, 586181, 2414034)
    expected_simulations_table_res = ("SACLAY", "SACLAY_mesh.mesh.sqlite", 1, "flux")
    expected_injections_table_res = (
        "0.00021 year",
        "Sans nom",
        0.0,
        1.0,
        1.0,
        4.02e-05,
        4.75e-09,
        49.91,
        156.0,
        9.8e-09,
    )
    _check_simulations_and_injections_table(
        expected_injections_table_res, expected_simulations_table_res
    )


def _delete_injection(crisis_widget):
    crisis_widget.injectionsLayout.itemAt(0).widget().deleteButton.clicked.emit()
    crisis_widget.injectionsLayout.takeAt(0).widget().setParent(None)


def _check_simulations_and_injections_table(
    expected_injections_table_res, expected_simulations_table_res
):
    with sqlite.connect(COMPUTATION_DB) as conn:
        cur = conn.cursor()
        res = cur.execute(
            "SELECT nom, nom_db_maillage, nombre_de_simulations, type_injection FROM simulations"
        ).fetchone()
        for i, j in zip(res, expected_simulations_table_res):
            assert i == j

        res = cur.execute(
            """SELECT duree, nom, volume_eau, coefficient_surface, coefficient_injection,
                round(permeabilite,7), infiltration, round(epaisseur,2), round(altitude), input
                FROM injections"""
        ).fetchone()
        for i, j in zip(res, expected_injections_table_res):
            approx(i, j)


def _define_injection(crisis_widget, input: float, surface: float, x: float, y: float):
    crisis_widget.injectionsLayout.itemAt(0).widget().inputEdit.setText(str(input))
    crisis_widget.injectionsLayout.itemAt(0).widget().surfaceEdit.setText(str(surface))
    crisis_widget.injectionsLayout.itemAt(0).widget().xEdit.setText(str(x))
    crisis_widget.injectionsLayout.itemAt(0).widget().yEdit.setText(str(y))
    crisis_widget.injectionsLayout.itemAt(0).widget().yEdit.setText(str(y))

    crisis_widget.injectionsLayout.itemAt(0).widget().yEdit.editingFinished.emit()
    crisis_widget.buttonBox.accepted.emit()


@pytest.mark.dependency(name="configure_simulation")
def test_hypercube_and_simulation_parameters(
    clean_test_dir, setup_mesh_project, create_computation_project
):
    """
    Test hypercube and simulation parameters configuration

    @param clean_test_dir: fixture to have a clean test directory
    @param setup_mesh_project: fixture to setup mesh_project
    @param create_computation_project: fixture to setup computation project
    """
    _hypercube_and_simulation_parameters_definition()


def _hypercube_and_simulation_parameters_definition():
    with sqlite.connect(COMPUTATION_DB) as conn:
        hypercube = HypercubeDialog(conn)
        hypercube.nbSimulationsSpinBox.setValue(1)
        params = {
            "WC": (0.27, 0.1, 0.5, "constant"),
            "VGA": (13.0, 10.0, 15.0, "normal"),
            "VGN": (1.43, 1.3, 1.6, "normal"),
            "VGR": (0.055, 0.01, 0.1, "normal"),
            "VGS": (0.9, 0.8, 1.0, "normal"),
            "VGE": (0.0, 0.0, 0.0, "constant"),
            "WT": (0.37, 0.37, 0.37, "constant"),
            "VM": (1850.0, 1850.0, 1850.0, "constant"),
            "DK": (0.0, 0.0, 0.0, "constant"),
            "SL": (0.1, 0.01, 1.0, "lognormal"),
            "DLZNS": (1.0, 0.2, 2.0, "lognormal"),
            "DLZS": (100.0, 20.0, 200.0, "lognormal"),
        }
        for i, val in enumerate(params.values()):
            hypercube.tableWidget.item(i, 0).setText(str(val[0]))
            hypercube.tableWidget.item(i, 1).setText(str(val[1]))
            hypercube.tableWidget.item(i, 2).setText(str(val[2]))
            hypercube.tableWidget.cellWidget(i, 3).setCurrentIndex(
                hypercube.tableWidget.cellWidget(i, 3).findText(val[3])
            )

        hypercube.buttonBox.accepted.emit()

        cur = conn.cursor()
        res = cur.execute("SELECT COUNT(*) FROM parametres_simulation").fetchone()

        # assert number of lines in parametres_simulation is 1
        assert res[0] == 1

        res = cur.execute(
            "SELECT wc, vga, vgn, vgr, vgs, vge, wt, vm, dk, sl, dlzns, dlzs FROM parametres_simulation"
        ).fetchone()
        expected_res = [val[0] for val in params.values()]

        # assert parametres_simulation table is as expected
        for i, j in zip(res, expected_res):
            assert i == j

        hypercube.nbSimulationsSpinBox.setValue(13)
        hypercube.buttonBox.accepted.emit()
        res = cur.execute("SELECT COUNT(*) FROM parametres_simulation").fetchone()
        # assert number of lines in parametres_simulation is 13
        assert res[0] == 13

        res = cur.execute(
            "SELECT wc, vga, vgn, vgr, vgs, vge, wt, vm, dk, sl, dlzns, dlzs FROM parametres_simulation"
        ).fetchone()
        expected_range = [(val[1], val[2]) for val in params.values()]
        # assert parameter values are included in the specified range
        for sim in range(1, 14):
            for i, j in zip(res, expected_range):
                assert j[0] <= i <= j[1]


@pytest.mark.dependency(
    name="run_simulation", depends=["create_injection", "configure_simulation"]
)
def test_run_simulation(
    clean_test_dir, setup_mesh_project, thyrsis, create_computation_project, mock_iface
):
    """
    Run a simulation and check that results are available

    @param clean_test_dir: fixture to have a clean test directory
    @param setup_mesh_project: fixture to setup mesh_project
    @param thyrsis: fixture to get thyrsis plugin
    @param create_computation_project: fixture to setup computation project
    @param mock_iface: QgsInterface mock
    """
    # Configure injection and simulation parameters
    _configuration_injection(mock_iface)
    _hypercube_and_simulation_parameters_definition()

    thyrsis.project = Project(COMPUTATION_DB, layers=[])
    crisis_widget = CrisisWidget(COMPUTATION_DB, mock_iface)
    thyrsis.crisis = QDockWidget("Thyrsis")
    thyrsis.crisis.setObjectName("Thyrsis")
    thyrsis.crisis.setWidget(crisis_widget)
    crisis_widget.simulateButton.clicked.connect(
        partial(thyrsis.runSimulation, True, True)
    )
    crisis_widget.simulateButton.clicked.emit()

    # check resultats layer
    result = QgsProject.instance().mapLayersByName("resultats")[0]
    assert type(result) == MeshLayer

    # assert result.colorLegend() is updated
    assert (
        result.colorLegend().minValue() != 0
        and result.colorLegend().maxValue() != 1
        and result.colorLegend().minValue() < result.colorLegend().maxValue()
    )

    # Remove result
    QgsProject.instance().removeMapLayers(
        [QgsProject.instance().mapLayersByName("resultats")[0].id()]
    )


@pytest.mark.dependency(depends=["run_simulation"])
def test_time_control(thyrsis, mock_iface):
    """
    Test time control widget

    @param thyrsis: fixture to get thyrsis plugin
    @param mock_iface: QgsInterface mock
    """
    thyrsis.project = Project(COMPUTATION_DB, layers=[])
    thyrsis.project.add_results()
    with sqlite.connect(COMPUTATION_DB) as conn:
        cur = conn.cursor()
        cur.execute("SELECT date FROM dates_simulation ORDER BY id")
        dates = [date for [date] in cur.fetchall()]

    # check the number of simulation dates
    assert thyrsis.time_control.dateComboBox.count() == 21

    # check date display value
    for i, date in enumerate(dates):
        thyrsis.time_control.dateComboBox.setCurrentIndex(i)
        assert thyrsis.time_control.dateComboBox.currentText() == datetime.strptime(
            date, "%Y-%m-%d %H:%M:%S"
        ).strftime("%d/%m/%Y %H:%M:%S")

    # check dates are sorted
    thyrsis.time_control.firstDate()
    first_date = thyrsis.time_control.dateComboBox.currentText()
    thyrsis.time_control.nextDate()
    next_date = thyrsis.time_control.dateComboBox.currentText()
    thyrsis.time_control.lastDate()
    last_date = thyrsis.time_control.dateComboBox.currentText()
    assert (
        datetime.strptime(first_date, "%d/%m/%Y %H:%M:%S")
        < datetime.strptime(next_date, "%d/%m/%Y %H:%M:%S")
        < datetime.strptime(last_date, "%d/%m/%Y %H:%M:%S")
    )

    # check dates combo are cleaned when user remove resultats layer
    QgsProject.instance().removeMapLayers(
        [QgsProject.instance().mapLayersByName("resultats")[0].id()]
    )
    assert thyrsis.time_control.dateComboBox.count() == 0


@pytest.mark.dependency(depends=["run_simulation"])
def test_columns_control(thyrsis, mock_iface):
    """
    Test result colums control

    @param thyrsis: fixture to get thyrsis plugin
    @param mock_iface: QgsInterface mock
    """
    thyrsis.project = Project(COMPUTATION_DB, layers=[])
    assert thyrsis.result_control.columnComboBox.count() == 0
    thyrsis.project.add_results()
    assert thyrsis.result_control.columnComboBox.count() == 1

    # Remove added result
    QgsProject.instance().removeMapLayers(
        [QgsProject.instance().mapLayersByName("resultats")[0].id()]
    )


@pytest.mark.dependency(depends=["run_simulation"])
def test_probability_map(thyrsis, mock_iface):
    """
    Test probability map creation

    @param thyrsis: fixture to get thyrsis plugin
    @param mock_iface: QgsInterface mock
    """
    thyrsis.project = Project(COMPUTATION_DB, layers=[])
    thyrsis.project.add_results()
    unit = thyrsis.project.result_layer.dataProvider().units()
    probability_dialog = ProbabilityDialog(0, unit)
    probability_dialog.thresholdValue.setText("1.28e-12")
    threshold = probability_dialog._ProbabilityDialog__value

    settings = Settings(os.path.dirname(COMPUTATION_DB))
    settings.setValue("General", "codeHydro", "openfoam")
    settings.setValue("General", "codeHydroCommand", "")
    thyrsis._Plugin__settings = settings
    thyrsis.probability(seuil=threshold)

    # assert probability item is added when selecting probability map
    assert thyrsis.result_control.columnComboBox.count() == 2

    # Remove added result
    QgsProject.instance().removeMapLayers(
        [QgsProject.instance().mapLayersByName("resultats")[0].id()]
    )


@pytest.mark.dependency(depends=["run_simulation"])
def test_isovalues(thyrsis, mock_iface):
    """
    Test isovalues creation

    @param thyrsis: fixture to get thyrsis plugin
    @param mock_iface: QgsInterface mock
    """
    thyrsis.project = Project(COMPUTATION_DB, layers=[])
    thyrsis.project.add_results()
    thyrsis.result_control.columnComboBox.setCurrentIndex(0)
    column = thyrsis.result_control.columnComboBox.currentText()
    values_default, unit_default, scale_default = (
        thyrsis.project.result_layer.colorLegend().values(5),
        thyrsis.project.result_layer.colorLegend().units(),
        "log" if thyrsis.project.result_layer.colorLegend().hasLogScale() else "linear",
    )
    isovalue = IsovalueDialog(
        values_default, unit_default, scale_default, nbPixelsPerMeter=0
    )
    values = isovalue._IsovalueDialog__values
    unit = isovalue._IsovalueDialog__unit
    scale = isovalue._IsovalueDialog__scale
    nbPixelsPerMeter = isovalue._IsovalueDialog__nbPixelsPerMeter

    # store isovalues settings in project
    strIso = " ".join([str(v) for v in values]) + " " + unit + " " + scale
    QgsProject.instance().writeEntry("thyrsis", column + "Isovalues", strIso)

    # the requested isovalues may not have the same unit as the
    # displayed one, we must convert
    provider = thyrsis.project.result_layer.dataProvider()
    conversionFactor = provider.conversionFactor(unit)
    values = [v / conversionFactor for v in values]

    if nbPixelsPerMeter == 0:
        lines = thyrsis.project.result_layer.isovalues(values)
        fmt = format_(min(values), max(values)) + " " + unit

        # add isovalues to the db
        did = provider.date() + 1
        date = thyrsis.time_control.dateComboBox.currentText()
        thyrsis.project.add_isovalues(
            did,
            date,
            column,
            lines,
            [value * provider.conversionFactor() for value in values],
            [fmt % (value * conversionFactor) for value in values],
        )

    layer_name = (
        thyrsis.result_control.columnComboBox.currentText()
        + " "
        + thyrsis.time_control.dateComboBox.currentText()
    )
    # assert isovalues layer is added when selecting isovalues
    assert len(QgsProject.instance().mapLayersByName(layer_name)) == 1

    # Remove added result
    QgsProject.instance().removeMapLayers(
        [QgsProject.instance().mapLayersByName("resultats")[0].id()]
    )


@pytest.mark.dependency(depends=["run_simulation"])
def test_time_plot(thyrsis, mock_iface):
    """
    Test time plot creation

    @param thyrsis: fixture to get thyrsis plugin
    @param mock_iface: QgsInterface mock
    """
    thyrsis.project = Project(COMPUTATION_DB, layers=[])
    thyrsis.project.add_results()
    labels = ["ZNS mass", "ZS mass", "Output mass", "Total mass", "mass "]
    settings = Settings(os.path.dirname(COMPUTATION_DB))
    plot = TimePlot.mass_balance(
        thyrsis.project.result_layer.dataProvider(),
        settings,
        labels,
    )
    # assert timePlot is created
    assert type(plot) == TimePlot

    provider = SpatialiteMeshDataProvider(
        "dbname=%s crs=epsg:27572 resultColumn=concentration" % (COMPUTATION_DB)
    )
    x, y = (586181, 2414034)
    nom = "whatever"
    colorsMeasures = {}  # stocke les couleurs de chaque type de mesure
    # colors = ('red','green','blue','orange','yellowGreen','purple','pink','cyan','violet','salmon','turquoise','brown','yellow')
    colors = (
        "#ff0000",
        "#00ff00",
        "#0000ff",
        "#ffa500",
        "#9acd32",
        "#a020f0",
        "#ffc0cb",
        "#00ffff",
        "#ee82ee",
        "#fa8072",
        "#40e0d0",
        "#a52a2a",
        "#ffff00",
    )
    plot2 = TimePlot.create(
        provider,
        (x, y),
        nom,
        colorsMeasures,
        colors,
        settings,
    )
    # assert timePlot is created
    assert type(plot2) == TimePlot

    # Remove added result
    QgsProject.instance().removeMapLayers(
        [QgsProject.instance().mapLayersByName("resultats")[0].id()]
    )


@pytest.mark.dependency(depends=["run_simulation"])
def test_display_value(thyrsis, mock_iface):
    """
    Test raster value display

    @param thyrsis: fixture to get thyrsis plugin
    @param mock_iface: QgsInterface mock
    """
    thyrsis.project = Project(COMPUTATION_DB, layers=[])
    thyrsis.project.add_results()
    QgsProject.instance().setCrs(QgsCoordinateReferenceSystem("EPSG:27572"))

    mock_iface.setTreeViewSelectedLayers([thyrsis.project.result_layer])

    state = thyrsis.displayValue(QgsPoint(586181, 2414034))
    # assert value is displayed
    assert state

    state = thyrsis.displayValue(QgsPoint(0, 0))
    # assert value is not displayed, point is outside domain
    assert not state

    # remove added result
    QgsProject.instance().removeMapLayers(
        [QgsProject.instance().mapLayersByName("resultats")[0].id()]
    )


def test_display_borehole(
    clean_test_dir, setup_mesh_project, thyrsis, create_computation_project
):
    """
    Test borehole display

    @param clean_test_dir: fixture to have a clean test directory
    @param setup_mesh_project: fixture to setup mesh_project
    @param thyrsis: fixture to get thyrsis plugin
    @param create_computation_project: fixture to setup computation project
    """
    thyrsis.project = Project(COMPUTATION_DB, layers=[])
    QgsProject.instance().setCrs(QgsCoordinateReferenceSystem("EPSG:27572"))

    state = thyrsis.displayBoreHole(QgsPoint(586238, 2414146))
    # assert borehole is displayed
    assert state

    state = thyrsis.displayBoreHole(QgsPoint(0, 0))
    # assert borehole is not displayed, point is outside domain
    assert not state


def test_create_injections(
    clean_test_dir, setup_mesh_project, thyrsis, create_computation_project
):
    """
    Test injections zone creation

    @param clean_test_dir: fixture to have a clean test directory
    @param setup_mesh_project: fixture to setup mesh_project
    @param thyrsis: fixture to get thyrsis plugin
    @param create_computation_project: fixture to setup computation project
    """
    shutil.copyfile(COMPUTATION_DB, COMPUTATION_DB + "saved")
    thyrsis.project = Project(COMPUTATION_DB, layers=[])
    thyrsis.project.create_injection_zones()

    with sqlite.connect(COMPUTATION_DB) as conn:
        cur = conn.cursor()
        res = cur.execute("SELECT COUNT(*) FROM injections").fetchone()[0]
        # Number of injection depends on gmsh version, need more than or 43 injections
        assert res >= 43


@pytest.mark.skip(
    reason="Export raster need resolution that must be defined by user interaction"
)
@pytest.mark.dependency(depends=["run_simulation"])
def test_export_functions(thyrsis, mock_iface, qgis_canvas):
    """
    Test all thyrsis export functions

    @param thyrsis: fixture to get thyrsis plugin
    @param mock_iface: QgsInterface mock
    @param qgis_canvas: QgsMapCanvas
    """
    # Load Project
    thyrsis.project = Project(COMPUTATION_DB, layers=[])

    # Zoom on zone of interest
    QgsProject.instance().setCrs(QgsCoordinateReferenceSystem("EPSG:27572"))
    extent = QgsRectangle(
        QgsPointXY(593279.5, 2419791.5), QgsPointXY(581262.5, 2411132.25)
    )
    qgis_canvas.zoomToFeatureExtent(extent)
    qgis_canvas.setLayers(QgsProject.instance().mapLayers().values())
    qgis_canvas.refresh()
    for i in qgis_canvas.layers():
        if i.name() == "contours":
            qgis_canvas.zoomToSelected(i)
        if i.name() == "resultats":
            mock_iface.setActiveLayer(i)

    # Prepare export
    export_dir = os.path.join(os.path.dirname(COMPUTATION_DB), "export")
    os.mkdir(export_dir)

    # Export image
    thyrsis.time_control.firstDate()

    # assert export raster to png file
    thyrsis.exportRaster(fil=os.path.join(export_dir, "img.png"))
    assert os.path.exists(os.path.join(export_dir, "img.png"))

    # assert export raster to tif file
    thyrsis.exportRaster(fil=os.path.join(export_dir, "img.tif"))
    assert os.path.exists(os.path.join(export_dir, "img.tif"))

    # Export Animation
    thyrsis.time_control.firstDate()
    composition = {
        "Thyrsis_2d": {"size": QSize(1231, 386), "pos": QPoint()},
        "Thyrsis_zns": {"size": QSize(295, 766), "pos": QPoint(1237, 0)},
        "size": QSize(1532, 766),
    }
    thyrsis.exportAnimation(
        fil=os.path.join(export_dir, "img_anim.png"),
        composition=composition,
        test=True,
    )
    # assert export animation to png files
    for i in ["%.2d" % i for i in range(21)]:
        assert os.path.exists(os.path.join(export_dir, "img_anim_00{}.png".format(i)))

    # Export Video
    thyrsis.time_control.firstDate()
    # assert export video to avi file
    thyrsis.exportVideo(
        fil=os.path.join(export_dir, "video.avi"),
        composition=composition,
        test=True,
    )
    assert os.path.exists(os.path.join(export_dir, "video.avi"))

    # Export Template
    thyrsis.exportTemplate()
    print("assert export template to sites.sqlite tables")
    with sqlite.connect(SITE_DB) as conn:
        cur = conn.cursor()
        assert cur.execute("SELECT COUNT(*) FROM simulations").fetchone()[0] == 1
        assert cur.execute("SELECT COUNT(*) FROM dates_resultats").fetchone()[0] == 1
        assert cur.execute("SELECT COUNT(*)%13 FROM parametres").fetchone()[0] == 0
        assert cur.execute("SELECT COUNT(*) FROM metis_options").fetchone()[0] == 1
        assert cur.execute("SELECT COUNT(*) FROM openfoam_options").fetchone()[0] == 1
        assert cur.execute("SELECT COUNT(*) FROM injections").fetchone()[0] == 1


@pytest.mark.dependency(depends=["run_simulation"])
def test_double_milieu(thyrsis, mock_iface):
    """
    Test double milieu add

    @param thyrsis: fixture to get thyrsis plugin
    @param mock_iface: QgsInterface mock
    """
    # Load Project
    thyrsis.project = Project(COMPUTATION_DB, layers=[])

    fil = os.path.splitext(COMPUTATION_DB)[0] + "_double.sqlite"

    initial_db = str(thyrsis.project.database)
    if os.path.abspath(initial_db) != os.path.abspath(fil):
        shutil.copy2(initial_db, fil)

    add_second_milieu(fil)

    with sqlite.connect(fil) as conn:
        cur = conn.cursor()
        nb_node = cur.execute("SELECT COUNT(*) FROM noeuds").fetchone()[0]
        nb_double_node = cur.execute(
            "SELECT COUNT(*) FROM noeuds_second_milieu"
        ).fetchone()[0]

        # assert noeuds_second_milieu is copied from noeuds
        assert nb_node == nb_double_node

        nb_node = cur.execute("SELECT COUNT(*) FROM mailles").fetchone()[0]
        nb_double_node = cur.execute(
            "SELECT COUNT(*) FROM mailles_second_milieu"
        ).fetchone()[0]

        # assert mailles_second_milieu is copied from mailles
        assert nb_node == nb_double_node


if __name__ == "__main__":
    unittest.main()
