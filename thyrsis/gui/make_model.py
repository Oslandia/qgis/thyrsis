# coding : utf-8
import os
import re
from builtins import range

from PyQt5.QtWidgets import QDialog
from qgis.core import QgsMapLayerProxyModel
from qgis.PyQt import uic


class MakeModelDialog(QDialog):
    def __init__(self, parent=None):
        super(MakeModelDialog, self).__init__(parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "make_model.ui"), self)

        self.permComboBox.setEnabled(False)
        self.cpermLineEdit.setEnabled(False)
        self.pilotRadioButton.setChecked(True)
        self.pilotRadioButton.toggled.connect(
            lambda: self.btnstate(self.pilotRadioButton)
        )
        self.permRadioButton.toggled.connect(
            lambda: self.btnstate(self.permRadioButton)
        )
        self.cpermRadioButton.toggled.connect(
            lambda: self.btnstate(self.cpermRadioButton)
        )

        self.permComboBox.setFilters(QgsMapLayerProxyModel.RasterLayer)
        for i in range(self.permComboBox.count()):
            if re.match("permeability", self.permComboBox.itemText(i), re.I):
                self.permComboBox.setCurrentIndex(i)
                break

        self.cpermLineEdit.textChanged.connect(self.checkFloat)
        self.buttonBox.accepted.connect(self.__accept)
        self.buttonBox.rejected.connect(self.close)

    def btnstate(self, button):
        if button.objectName() == "pilotRadioButton":
            if button.isChecked():
                self.permComboBox.setEnabled(False)
                self.cpermLineEdit.setEnabled(False)
        elif button.objectName() == "permRadioButton":
            if button.isChecked():
                self.permComboBox.setEnabled(True)
                self.cpermLineEdit.setEnabled(False)
        elif button.objectName() == "cpermRadioButton":
            if button.isChecked():
                self.permComboBox.setEnabled(False)
                self.cpermLineEdit.setEnabled(True)

    def params(
        self,
    ):
        if self.pilotRadioButton.isChecked():
            return (False, None)
        elif self.permRadioButton.isChecked():
            return (True, self.permComboBox.currentLayer().source(), False)
        elif self.cpermRadioButton.isChecked():
            return (True, self.cpermLineEdit.text(), True)

    def checkFloat(self):
        try:
            float(self.cpermLineEdit.text())
            self.buttonBox.setEnabled(True)
        except:
            self.buttonBox.setEnabled(False)

    def __accept(self):
        return True
