# coding = utf-8
"""
converts mesh data as seen from a const data provider to a raster file

USAGE

    python -m thyrsis.layer_to_tif uri resolution output.tif

    where resolution is in pixel per meter
"""
from builtins import range

from numpy import amax, amin, repeat
from OpenGL.GL import (
    GL_COLOR_ARRAY,
    GL_COLOR_BUFFER_BIT,
    GL_DEPTH_BUFFER_BIT,
    GL_FLOAT,
    GL_MODELVIEW,
    GL_RGBA,
    GL_TRIANGLES,
    GL_VERTEX_ARRAY,
    glClear,
    glClearColor,
    glColorPointerf,
    glDrawElementsui,
    glEnableClientState,
    glLoadIdentity,
    glMatrixMode,
    glReadPixels,
    glScalef,
    glTranslatef,
    glVertexPointerf,
    glViewport,
)
from osgeo import gdal, osr
from PyQt5.QtCore import QSize
from PyQt5.QtGui import (
    QOffscreenSurface,
    QOpenGLContext,
    QOpenGLFramebufferObject,
    QOpenGLFramebufferObjectFormat,
)
from qgis.core import QgsApplication

from .spatialitemeshdataprovider import SpatialiteMeshConstDataProvider


def layer_to_tif(provider, pixel_per_meter, outfile):
    """Export a mesh layer to tif file

    :param provider: provider if defined
    :type provider: MeshDataProvider
    :param pixel_per_meter: used for image output
    :type pixel_per_meter: int
    :param outfile: output file path
    :type outfile: string
    """
    print("layer_to_tif", outfile)
    vtx = provider.nodeCoord()
    vtx[:, 2] = 0
    tri = provider.triangles()
    values = provider.nodeValues()
    extent = provider.extent()

    vmin, vmax = amin(values), amax(values)
    dv = vmax - vmin

    color = repeat((values - vmin) / dv, 3).reshape((len(vtx), 3))

    width, height = int(extent.width() * pixel_per_meter) + 1, int(
        extent.height() * pixel_per_meter
    )
    xmax, xmin, ymax, ymin = (
        extent.xMaximum(),
        extent.xMinimum(),
        extent.yMaximum(),
        extent.yMinimum(),
    )
    gt = [
        extent.xMinimum(),
        1.0 / pixel_per_meter,
        0.0,
        extent.yMaximum(),
        0.0,
        -1.0 / pixel_per_meter,
    ]

    buf_w, buf_h = 1024, 1024

    driver = gdal.GetDriverByName("GTiff")
    outRaster = driver.Create(outfile, width, height, 1, gdal.GDT_Float32)
    srs = osr.SpatialReference()
    srs.ImportFromWkt(provider.crs().toWkt())
    outRaster.SetProjection(srs.ExportToWkt())
    outRaster.SetGeoTransform(gt)
    outband = outRaster.GetRasterBand(1)
    nodata = -9999
    outband.SetNoDataValue(nodata)

    gl_ctx = QOpenGLContext()
    gl_surface = QOffscreenSurface()
    gl_surface.create()
    gl_ctx.create()

    fmt = QOpenGLFramebufferObjectFormat()

    gl_ctx.makeCurrent(gl_surface)
    pixBuf = QOpenGLFramebufferObject(QSize(buf_w, buf_h), fmt)
    pixBuf.bind()
    glEnableClientState(GL_VERTEX_ARRAY)
    glEnableClientState(GL_COLOR_ARRAY)
    glViewport(0, 0, buf_w, buf_h)
    glMatrixMode(GL_MODELVIEW)

    for i in range(width // buf_w + 1):
        for j in range(height // buf_h + 1):
            xmax, xmin, ymax, ymin = (
                gt[0] + float((i + 1) * buf_w) * gt[1],
                gt[0] + float(i * buf_w) * gt[1],
                gt[3] + float((j + 1) * buf_h) * gt[5],
                gt[3] + float(j * buf_h) * gt[5],
            )

            glLoadIdentity()
            glScalef(2.0 / (xmax - xmin), 2.0 / (ymax - ymin), 1)
            glTranslatef(-0.5 * (xmax + xmin), -0.5 * (ymax + ymin), 0)
            glClearColor(0.0, 0.0, 0.0, 0.0)
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            glColorPointerf(color)
            glVertexPointerf(vtx)
            glDrawElementsui(GL_TRIANGLES, tri)

            data = glReadPixels(0, 0, buf_w, buf_h, GL_RGBA, GL_FLOAT)
            level = data[:, :, 0] * dv + vmin
            level[data[:, :, 3] == 0.0] = nodata
            outband.WriteArray(
                level[
                    : min(buf_h, height - j * buf_h), : min(buf_w, width - i * buf_w)
                ],
                i * buf_w,
                j * buf_h,
            )

    gl_ctx.doneCurrent()

    outband.GetStatistics(True, True)
    outband.FlushCache()
    driver, outRaster, outband = None, None, None


if __name__ == "__main__":
    import sys

    assert len(sys.argv) == 4
    uri = sys.argv[1]
    pixel_per_meter = float(sys.argv[2])
    outfile = sys.argv[3]

    app = QgsApplication(sys.argv, True)
    QgsApplication.initQgis()

    provider = SpatialiteMeshConstDataProvider(uri)
    layer_to_tif(provider, pixel_per_meter, outfile)
