FROM qgis/qgis:final-3_34_12

ARG OPENFOAM_PKG_VERSION=2406
ARG PMF_TAG=v2406.2

# prevent some packages from prompting interactive input
ENV DEBIAN_FRONTEND=noninteractive

# System requirements
COPY requirements/apt.txt requirements.txt


RUN apt update \
    # && xargs -a "<(awk '! /^ *(#|$)/' requirements.txt)" -r -- sudo apt install -y \
    && apt install -y $(grep -vE "^\s*#" requirements.txt | tr "\n" " ") \
    && rm -rf /var/lib/apt/lists/*

# Install OpenFOAM
COPY containers/ubuntu_qgis_openfoam_pmf/openfoam.gpg openfoam.gpg
RUN gpg --dearmor -o /etc/apt/keyrings/openfoam.gpg openfoam.gpg
RUN echo "deb [arch=amd64 signed-by=/etc/apt/keyrings/openfoam.gpg] https://dl.openfoam.com/repos/deb $(sed -ne 's/^DISTRIB_CODENAME=//p' /etc/lsb-release) main" > "/etc/apt/sources.list.d/openfoam.list"
RUN apt update
RUN apt install -y openfoam$OPENFOAM_PKG_VERSION-dev \
    && rm -rf /var/lib/apt/lists/*

# RUN openfoam$OPENFOAM_PKG_VERSION --version \
#     && . /usr/lib/openfoam/openfoam$OPENFOAM_PKG_VERSION/etc/bashrc \
#     && echo $WM_PROJECTDIR

# Install Porous Multiphase Foam (PMF)
RUN git clone --depth=1 --branch=$PMF_TAG --single-branch https://github.com/phorgue/porousMultiphaseFoam.git \
    && cd porousMultiphaseFoam/ \
    && . /usr/lib/openfoam/openfoam$OPENFOAM_PKG_VERSION/etc/bashrc \
    && ./Allwmake \
    && ./Allwclean

RUN cd ..

# Install cfMesh
RUN git clone https://develop.openfoam.com/Community/integration-cfmesh \
    && cd integration-cfmesh \
    && . /usr/lib/openfoam/openfoam$OPENFOAM_PKG_VERSION/etc/bashrc \
    && ./Allwmake \
    && ./Allwclean