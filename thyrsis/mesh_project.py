import os
from builtins import str

from osgeo.osr import SpatialReference
from qgis.core import (
    QgsCoordinateReferenceSystem,
    QgsDataSourceUri,
    QgsMapLayer,
    QgsProject,
    QgsRasterLayer,
    QgsVectorLayer,
)
from qgis.gui import QgsMapCanvas
from qgis.PyQt.QtCore import QFileInfo, QObject, Qt

from thyrsis.exception import DatabaseMeshLayerCreationError, ProjectLoadError

from .cfmesh import cfMesh
from .database import check_and_upgrade, create_mesh_tables, sqlite
from .gmsh import Gmsh
from .log import logger
from .mesh import (
    mnt,
    mur,
    permeabilite,
    polygonize,
    potentiel_impose,
    potentiel_reference,
)
from .meshlayer.meshlayer import MeshLayer
from .settings import Settings
from .spatialitemeshdataprovider import SpatialiteMeshConstDataProvider
from .utilities import complete_filename


def get_layer(name: str, layer_type: type = None):
    """Return a layer if existing, according to the name requested

    :param name: layer name
    :type name: string
    :param layer_type: expected layer type (default None for any available layer type)
    :type layer_type: type
    """
    layers = QgsProject.instance().mapLayersByName(name)
    if layers:
        for layer in layers:
            if layer_type is None or type(layer) == layer_type:
                return layer
    else:
        return None


class MeshProject(QObject):
    """A thyrsis mesh project is a .mesh.sqlite database along with a .qgs project file"""

    def __init__(self, database, layers=None, parent=None):
        """Constructor

        :param database: database path
        :type database: string
        :param layers: layers to display
        :type layers: list
        :param parent: Qt parent
        :type parent: QtWidget
        """
        check_and_upgrade(database)
        super(MeshProject, self).__init__(parent)
        self.__databasePath = os.path.dirname(os.path.abspath(database))
        self.__database = database
        self.__conn = sqlite.connect(database)
        self.__cur = self.__conn.cursor()
        self.__layers = []
        self.__settings = Settings(self.__databasePath)
        contours = get_layer("contours")
        contours.editingStopped.connect(self.polygonize)
        # TODO set snapping for layer
        # sanpping_config = QgsProject.instance().snappingConfig()
        # sanpping_config.
        # .(contours.id(), True, QgsSnapper.SnapToVertex,  QgsTolerance.Pixels, 10, True)

    def get_project_SRID(self, table="noeuds"):
        return str(
            self.__cur.execute(
                "SELECT srid FROM geometry_columns WHERE f_table_name = '{}'".format(
                    table
                )
            ).fetchone()[0]
        )

    def polygonize(self):
        """Polygonize domaines from coutour entities"""
        polygonize(self.__cur)
        self.__conn.commit()
        get_layer("domaines") and get_layer("domaines").triggerRepaint()
        get_layer("extremites") and get_layer("extremites").triggerRepaint()
        self.__layers = []

    def mesh(self, altitude_raster, altitude_mur_raster, potentiel_reference_raster):
        """Launch mesh build

        :param altitude_raster: DEM raster
        :type altitude_raster: string
        :param altitude_mur_raster: water table altitude raster
        :type altitude_mur_raster: string
        :param potentiel_reference_raster: potential reference raster
        :type potentiel_reference_raster: raster
        """
        for column, units in SpatialiteMeshConstDataProvider.sourceUnits.items():
            get_layer(column, MeshLayer) and QgsProject.instance().removeMapLayer(
                get_layer(column, MeshLayer).id()
            )
        get_layer("altitude") and QgsProject.instance().removeMapLayer(
            get_layer("altitude").id()
        )
        get_layer("altitude_mur") and QgsProject.instance().removeMapLayer(
            get_layer("altitude_mur").id()
        )
        get_layer("potentiel_reference") and QgsProject.instance().removeMapLayer(
            get_layer("potentiel_reference").id()
        )
        self.__layers = []

        if self.__settings.value("General", "meshGenerator") == "gmsh":
            Gmsh(self.__database, self.__cur, self.__settings)
        else:
            cfMesh(self.__database, self.__cur, self.__settings)
        self.__conn.commit()

        get_layer("mailles") and get_layer("mailles").triggerRepaint()
        get_layer("noeuds") and get_layer("noeuds").triggerRepaint()

        project_SRID = self.get_project_SRID()

        mnt(self.__cur, altitude_raster, project_SRID)
        self.__conn.commit()
        layer = MeshLayer(
            "dbname="
            + self.__database
            + " crs=epsg:{} column=altitude".format(project_SRID),
            "altitude",
            SpatialiteMeshConstDataProvider.PROVIDER_KEY,
        )
        logger.debug(
            "dbname="
            + self.__database
            + " crs=epsg:{} column=altitude".format(project_SRID),
            layer.isValid(),
        )
        layer.colorLegend().setTitle("altitude")
        layer.colorLegend().setUnits("m")
        layer.colorLegend().setMaxValue(layer.dataProvider().maxValue())
        layer.colorLegend().setMinValue(layer.dataProvider().minValue())
        if not layer.isValid():
            raise DatabaseMeshLayerCreationError(
                "Invalid altitude MeshLayer after mesh creation"
            )
        QgsProject.instance().addMapLayer(layer)
        self.__layers.append(layer)

        if potentiel_reference_raster is not None:
            potentiel_reference(self.__cur, potentiel_reference_raster)
            self.__conn.commit()
            layer = MeshLayer(
                "dbname="
                + self.__database
                + " crs=epsg:{} column=potentiel_reference".format(project_SRID),
                "potentiel_reference",
                SpatialiteMeshConstDataProvider.PROVIDER_KEY,
            )
            logger.debug(
                "dbname="
                + self.__database
                + " crs=epsg:{} column=potentiel_reference".format(project_SRID),
                layer.isValid(),
            )
            layer.colorLegend().setTitle("potentiel_reference")
            layer.colorLegend().setUnits("m")
            layer.colorLegend().setMaxValue(layer.dataProvider().maxValue())
            layer.colorLegend().setMinValue(layer.dataProvider().minValue())
            if not layer.isValid():
                raise DatabaseMeshLayerCreationError(
                    "Invalid reference potential MeshLayer after mesh creation"
                )
            QgsProject.instance().addMapLayer(layer)
            self.__layers.append(layer)

        mur(self.__cur, altitude_mur_raster)
        self.__conn.commit()
        layer = MeshLayer(
            "dbname="
            + self.__database
            + " crs=epsg:{} column=altitude_mur".format(project_SRID),
            "altitude_mur",
            SpatialiteMeshConstDataProvider.PROVIDER_KEY,
        )
        logger.debug(
            "dbname="
            + self.__database
            + " crs=epsg:{} column=altitude_mur".format(project_SRID),
            layer.isValid(),
        )
        layer.colorLegend().setTitle("altitude_mur")
        layer.colorLegend().setUnits("m")
        layer.colorLegend().setMaxValue(layer.dataProvider().maxValue())
        layer.colorLegend().setMinValue(layer.dataProvider().minValue())
        if not layer.isValid():
            raise DatabaseMeshLayerCreationError(
                "Invalid altitude_mur MeshLayer after mesh creation"
            )
        QgsProject.instance().addMapLayer(layer)
        self.__layers.append(layer)

        potentiel_impose(self.__cur)
        self.__conn.commit()

    def add_permeabilite(self, permeability, const):
        """Add permeability values from raster

        :param permeability: permeability raster or constant value
        :type permeability: string
        :param const: constant permeability flag
        :type const: bool

        :return: permeability layer
        :rtype: MeshLayer
        """
        permeabilite(self.__cur, permeability, const)
        self.__conn.commit()
        project_SRID = self.get_project_SRID()
        layer = MeshLayer(
            "dbname="
            + self.__database
            + " crs=epsg:{} column=permeabilite_x".format(project_SRID),
            "permeabilite_x",
            SpatialiteMeshConstDataProvider.PROVIDER_KEY,
        )
        logger.debug(
            "dbname="
            + self.__database
            + " crs=epsg:{} column=permeabilite_x".format(project_SRID),
            layer.isValid(),
        )
        layer.colorLegend().setTitle("permeabilite_x")
        layer.colorLegend().setUnits("m/s")
        maxValue = layer.dataProvider().maxValue()
        layer.colorLegend().setMaxValue(layer.dataProvider().maxValue())
        if not layer.isValid():
            raise DatabaseMeshLayerCreationError(
                "Invalid permeability MeshLayer after mesh creation"
            )
        QgsProject.instance().addMapLayer(layer)
        self.__layers.append(layer)
        return layer

    def __del__(self):
        self.__conn.close()

    def __getattr__(self, name):
        if name == "database":
            return self.__database
        elif name == "qgs":
            return self.__database[:-7] + ".qgs"
        elif name == "permeabilite_x_layer":
            res = QgsProject.instance().mapLayersByName("permeabilite_x")
            return res[0] if res else None
        elif name == "crs":
            return QgsProject.instance().crs()
        else:
            raise AttributeError

    def setSettings(self, settings):
        """Set Thyrsis settings

        :param settings: settings for Thyrsis use
        :type settings: Settings
        """
        self.__settings = settings

    @staticmethod
    def load(database, settings, canvas=None):
        """Load mesh project

        :param canvas: qgis canvas
        :type canvas: QgsMapCanvas

        :return: mesh project
        :rtype: MeshProject
        """
        if not database[-12:] == ".mesh.sqlite":
            raise ProjectLoadError(
                "Invalid database file. Suffix should be .mesh.sqlite"
            )
        if not os.path.exists(database):
            raise ProjectLoadError(f"File {database} is not available.")
        if os.path.exists(database[:-7] + ".qgs"):
            raise ProjectLoadError(f"File {database[:-7] + '.qgs'} must not exists.")
        layers = MeshProject.create_qgis_project(database, settings, canvas)
        project = MeshProject(database, layers)
        return project

    @staticmethod
    def create(database, settings, canvas=None):
        """create a new project, the mesh_name is given without extension
        nor path ans should be present in the thyrsis/defaultMeshDir with
        and extension .mesh.sqlite

        :param database: database path
        :type database: string
        :param canvas: qgis canvas
        :type canvas: QgsMapCanvas

        :return: mesh project
        :rtype: MeshProject
        """
        if not database[-12:] == ".mesh.sqlite":
            raise ProjectLoadError(
                "Invalid database file. Suffix should be .mesh.sqlite"
            )
        if os.path.exists(database):
            raise ProjectLoadError(f"File {database} must not exists.")
        logger.debug(database)

        site = os.path.basename(database)[:-12].split("_")[0]
        site_db = settings.value("General", "defaultSite")
        conn = sqlite.connect(site_db)
        cur = conn.cursor()
        if not cur.execute(
            "SELECT COUNT(1) FROM sites WHERE nom='%s'" % (site)
        ).fetchone()[0]:
            cur.execute("INSERT INTO sites(nom) VALUES('%s')" % (site))
            conn.commit()
        conn.close()

        project_SRID = str(QgsProject.instance().crs()).split(":")[-1][:-1]

        dst = sqlite.connect(database)
        cur = dst.cursor()
        cur.execute("SELECT InitSpatialMetaData(1)")
        create_mesh_tables(cur, project_SRID=project_SRID)

        dst.commit()
        dst.close()

        layers = MeshProject.create_qgis_project(database, settings, canvas)
        return MeshProject(database, layers)

    @staticmethod
    def create_qgis_project(database, settings, canvas):
        """returns a list of layers

        :param database: database path
        :type database: string
        :param canvas: qgis canvas
        :type canvas: QgsMapCanvas

        :return: layers
        :rtype: list
        """
        logger.notice("create_qgis_project", database[:-7] + ".qgs")
        QgsProject.instance().setFileName(database[:-7] + ".qgs")

        QgsProject.instance().removeAllMapLayers()

        layers = []

        QgsProject.instance().writeEntry("Paths", "/Absolute", True)

        # base maps
        maps_dir = str(settings.value("General", "maps"))
        if maps_dir:
            for fil in os.listdir(maps_dir):
                if fil[-4:] == ".vrt":
                    file_name = os.path.join(maps_dir, fil)
                    file_info = QFileInfo(file_name)
                    base_name = file_info.baseName()
                    layer = QgsRasterLayer(file_name, base_name)
                    if not layer.isValid():
                        raise ProjectLoadError(f"Invalid base map layer {file_name}")
                    QgsProject.instance().addMapLayer(layer)
                    layers.append(layer)
                    layer_node = (
                        QgsProject.instance().layerTreeRoot().findLayer(layer.id())
                    )
                    layer_node.setExpanded(False)
                    layer_node.setItemVisibilityChecked(Qt.Unchecked)

        for layer_name in [
            "parametres",
            "noeuds_contour",
            "zones",
            "potentiel_impose",
            "hynverse_parametres",
            "hynverse_erreurs",
            "metis_hynverse_options",
            "openfoam_hynverse_options",
        ]:
            uri = QgsDataSourceUri()
            uri.setDatabase(database)
            uri.setDataSource("", layer_name, "")
            layer = QgsVectorLayer(uri.uri(), layer_name, "spatialite")
            if not layer.isValid():
                raise ProjectLoadError(
                    f"Can't load layer {layer_name} from spatialite databse"
                )
            QgsProject.instance().addMapLayer(layer)
            layers.append(layer)

        for layer_name in [
            "domaines",
            "mailles",
            "noeuds",
            "contours",
            "extremites",
            "points_fixes",
            "points_pilote",
            "forages",
            "topology_errors",
            "bad_triangles",
        ]:

            # if layer_name != 'forages':
            #    layer = QgsVectorLayer('{}|layername={}'.format(database, layer_name), layer_name, 'ogr')
            # else:
            uri = QgsDataSourceUri()
            uri.setDatabase(
                database
                if layer_name != "forages"
                else settings.value("General", "defaultSite")
            )
            uri.setDataSource(
                "", layer_name, "GEOMETRY" if layer_name != "forages" else "POINT"
            )
            layer = QgsVectorLayer(uri.uri(), layer_name, "spatialite")

            style = complete_filename("styles/" + layer_name + ".qml")
            if os.path.exists(style):
                layer.loadNamedStyle(style)
            if not layer.isValid():
                raise ProjectLoadError(
                    f"Can't load layer {layer_name} from spatialite databse"
                )
            QgsProject.instance().addMapLayer(layer)
            layers.append(layer)

        QgsProject.instance().writeEntry("thyrsis", "meshdb", database)

        project_SRID = int(
            str(
                QgsProject.instance().mapLayersByName("noeuds")[0].dataProvider().crs()
            ).split(":")[-1][:-1]
        )
        canvas = canvas or QgsMapCanvas()
        proj = SpatialReference()
        proj.ImportFromEPSG(project_SRID)
        QgsProject.instance().setCrs(QgsCoordinateReferenceSystem(proj.ExportToWkt()))
        canvas.setDestinationCrs(QgsCoordinateReferenceSystem(proj.ExportToWkt()))
        canvas.refresh()

        QgsProject.instance().write()
        logger.notice("saving ", database[:-7] + ".qgs")
        return layers

    def execute(self, sql):
        """Execute sql query

        :param sql: sql query
        :param sql: string

        :return: cursor
        :rtype: sqlite3.cursor
        """
        return self.__cur.execute(sql)

    def commit(self):
        """Commit changes"""
        self.__conn.commit()
