# coding : utf-8
import os
import re
from builtins import range

from PyQt5.QtWidgets import QDialog
from qgis.core import QgsMapLayerProxyModel
from qgis.PyQt import uic


class MeshRasterDialog(QDialog):
    """Dialog to design the mesh (GMSH software)"""

    def __init__(self, parent=None):
        """Constructor

        :param parent: widget parent
        :type parent: QWidget
        """
        super(MeshRasterDialog, self).__init__(parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "mesh_rasters.ui"), self)
        self.mnt.setFilters(QgsMapLayerProxyModel.RasterLayer)
        self.mur.setFilters(QgsMapLayerProxyModel.RasterLayer)
        self.potentiel.setFilters(QgsMapLayerProxyModel.RasterLayer)
        for i in range(self.mnt.count()):
            if re.match("mnt", self.mnt.itemText(i), re.I):
                self.mnt.setCurrentIndex(i)
                break
        for i in range(self.mur.count()):
            if re.match("mur", self.mur.itemText(i), re.I):
                self.mur.setCurrentIndex(i)
                break
        for i in range(self.potentiel.count()):
            if re.match("potentiel", self.potentiel.itemText(i), re.I):
                self.potentiel.setCurrentIndex(i)
                break

    def layers(self):
        """Return source of the chosen layer (DEM, water table altitude, potential [optional])"""
        return (
            self.mnt.currentLayer().source(),
            self.mur.currentLayer().source(),
            self.potentiel.currentLayer().source()
            if self.potentielCheckBox.isChecked()
            else None,
        )
