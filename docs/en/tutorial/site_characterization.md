---
substitutions:
  img_forage: |-
    :::{image} /_static/images/forage.png
    :scale: 10 %
    :::
---

# Site characterization

The sites can be described by adding points of interest, potential or concentration measurements and boreholes stratigraphy description.

## Points of interest

It is interesting to define some points of interest, which will be inserted into the **points_interet** table and the *points_interet* QGIS layer. Simulation results at points of interest are automatically saved in a text file for further use.

Points of interest can be loaded in the **sites.sqlite** database by using the THYRSIS function "Load points" available in the processing QGIS panel or the following command :

```bash
python -m thyrsis.database.load_points site points.csv
```

where :

- *site* is the site name (str),
- *points.csv* is a CSV text file with three to five columns separated by blanks, commas or semicolons and on each line the sorted values :
    - x_coordinate,
    - y_coordinate,
    - point name,
    - (optional) integer defining the EPSG value - default is 4326,
    - (optional) string defining a set of points (like "calcul" (default), "borehole", "chimney"...)

:::{note}

- The first line is read only for detecting separators.
- Only points from "calcul" group are selected for result files.
:::

----

## Measurements

Concentration or potential measurements can be associated to the points of interest in order to display them with the simulation results.

Measurements can be loaded in the **sites.sqlite** database by using the THYRSIS function "Load measure" available in the processing QGIS panel or the following command :

```bash
# for potential :
python -m thyrsis.database.load_measure [-d] site potPoint.dat
# for concentration :
python -m thyrsis.database.load_measure [-d] site chemical unit concPoint.dat
```

where :

- *site* is the site name (str),
- *potPoint.dat* is a file with measured potential at the point 'Point',
- *concPoint.dat* is a file with measured concentration at the point 'Point',
- *chemical* is a chemical element of the **elements_chimiques** table (str),
- *unit* is the unit of the concentration (str),
- the `-d` option is used to remove previous inserted measured values.

*potPoint.dat* and *concPoint.dat* are blank-separated text files with format :

```dat
dd/mm/YYYY[ HH:MM[:SS]] value[ uncertainty]
```

## Boreholes

Boreholes and stratigraphic or fracturing data can be loaded in the **sites.sqlite** database by using the THYRSIS function "Load boreholes" available in the processing QGIS panel or the following command :

```bash
python -m thyrsis.database.load_forages site srid forages.csv stratigraphie.csv [fracturation.csv]
```

where :

- *site* is the site name (str),
- *srid* is the EPSG value (int),
- *forages.csv* is a csv file with ";" separator and **sorted** columns :

  > - "FullName", "Name", "X_m", "Y_m", "Z_m", "TubeHeight_m", "TubeAltitude_m", "Depth_m", "BuildingDate", "Type", "Location",

- *stratigraphie.csv* is a csv file with ";" separator and **sorted** columns :

  > - "Name", "From", "To", "RockId", "RockDescription", "FormationId", "FormationDescription",

- *fracturation.csv* is a csv file with ";" separator and **sorted** columns :

  > - "Name","From","To","FracturationRate",

:::{note}

- The column names are not read and can be different but the column's order must be strictly respected,
- "RockId" refers to the USGS lithology pattern'id, cf. {ref}`Stratigraphic logs <stratigraphic_logs>` ,
- "FormationId" refers to the CGMW codification, cf. {ref}`Stratigraphic logs <stratigraphic_logs>` .
:::

Examples of *forages.csv* and *stratigraphie.csv* files are provided with the tutorial, with site name "SACLAY" and srid set to 27572.

Boreholes are then available in the THYRSIS project via the {{ img_forage }} button. By clicking on this button and then selecting a borehole in the QGIS canvas, a stratigraphic log is displayed in the Borehole tab ({numref}`tutorial_Thyrsis_Forage`).

:::{figure} /_static/images/Thyrsis_Forage.png
:name: tutorial_Thyrsis_Forage
:scale: 50 %

Borehole example
:::
