"""
Thyrsis creation of latin hypercube

USAGE
    python -m thyrsis.create_latin_hypercube dbname.sqlite

    sets parametres_simulation in dbname.sqlite
"""
import os
from builtins import str

from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QComboBox, QDialog, QTableWidgetItem

from ..database import create_latin_hypercube
from ..log import logger


class HypercubeDialog(QDialog):
    """Dialog for the Hypercube Latin configuration"""

    def __init__(self, conn, parent=None):
        """Constructor

        :param conn: sqlite3 connection
        :type conn: sqlite3.Connection
        """
        super(HypercubeDialog, self).__init__(parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "latin_hypercube.ui"), self)
        self.__conn = conn
        cur = self.__conn.cursor()
        (nb_simulation,) = cur.execute(
            """
                SELECT nombre_de_simulations FROM simulations
                """
        ).fetchone()
        self.nbSimulationsSpinBox.setValue(nb_simulation)

        second_milieu = (
            cur.execute("SELECT COUNT(1) FROM noeuds_second_milieu").fetchone()[0] > 0
        )

        self.__params = (
            [
                "WC",
                "VGA",
                "VGN",
                "VGR",
                "VGS",
                "VGE",
                "WT",
                "VM",
                "DK",
                "SL",
                "DLZNS",
                "DLZS",
            ]
            if not second_milieu
            else [
                "ASHAPE",
                "BSHAPE",
                "CSHAPE",
                "PL",
                "WC",
                "VGA",
                "VGN",
                "VGR",
                "VGS",
                "VGE",
                "WT",
                "VM",
                "DK",
                "SL",
                "DLZNS",
                "DLZS",
                "WC2",
                "VGA2",
                "VGN2",
                "VGR2",
                "VGS2",
                "VGE2",
                "WT2",
                "VM2",
                "DK2",
                "SL2",
                "DLZNS2",
                "DLZS2"
            ]
        )

        self.tableWidget.clear()
        self.tableWidget.setRowCount(len(self.__params))
        self.tableWidget.setColumnCount(4)
        self.tableWidget.setHorizontalHeaderLabels(
            [
                self.tr("Value"),
                self.tr("Min value"),
                self.tr("Max value"),
                self.tr("Variation law"),
            ]
        )
        self.tableWidget.setVerticalHeaderLabels(self.__params)
        for row, param in enumerate(self.__params):
            value, min_, max_, law = cur.execute(
                "SELECT valeur, valeur_min, valeur_max, loi_de_variation FROM parametres WHERE nom='%s'"
                % (param)
            ).fetchone()
            self.tableWidget.setItem(row, 0, QTableWidgetItem(str(value)))
            self.tableWidget.setItem(
                row, 1, QTableWidgetItem(str(min_) if min_ else "0")
            )
            self.tableWidget.setItem(
                row, 2, QTableWidgetItem(str(max_) if max_ else "0")
            )

            cmb = QComboBox()
            cmb.addItems(["constant", "normal", "lognormal", "uniform", "loguniform"])
            cmb.setCurrentIndex(cmb.findText(law))
            self.tableWidget.setCellWidget(row, 3, cmb)

    def reject(self):
        """Rollback changes, cross button"""
        self.__conn.rollback()
        QDialog.reject(self)

    def accept(self):
        """Update values in the parametres table"""
        cur = self.__conn.cursor()
        for row, param in enumerate(self.__params):
            sql = """
                UPDATE parametres SET
                    valeur={valeur}, valeur_min={valeur_min}, valeur_max={valeur_max}, loi_de_variation='{loi_de_variation}'
                WHERE nom='{nom}'""".format(
                nom=param,
                valeur=self.tableWidget.item(row, 0).text() or "NULL",
                valeur_min=self.tableWidget.item(row, 1).text() or "NULL",
                valeur_max=self.tableWidget.item(row, 2).text() or "NULL",
                loi_de_variation=self.tableWidget.cellWidget(row, 3).currentText()
                or "NULL",
            )

            logger.debug(sql)
            cur.execute(sql)
        create_latin_hypercube(cur, self.nbSimulationsSpinBox.value())
        self.__conn.commit()
        QDialog.accept(self)


if __name__ == "__main__":

    import logging
    import os
    import sys

    from qgis.PyQt.QtWidgets import QApplication

    from ..database import sqlite

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    if len(sys.argv) != 2:
        logging.error("wrong number of arguments (try create_latin_hypercube --help)")
        exit(1)
    app = QApplication(sys.argv)
    conn = sqlite.connect(sys.argv[1])
    dlg = HypercubeDialog(sqlite.connect(sys.argv[1]))
    dlg.show()
    app.exec_()
