# MS-MPI on Windows

If you want to use multiple processors on a local or remote machine under Windows, you need to ensure that the service is running on the machine.

Here is a link to the MS-MPI documentation: https://github.com/microsoft/Microsoft-MPI/blob/master/docs/RunningMSMPI.md

You can start the service on the machine with the following command:

```
smpd -p 8677
```

The `smpd` executable should be in your `PATH`. If it is not, it can be found in the directory defined by the `MSMPI_BIN` variable.
