# Installation

1D and 2D simulations are realized with an external software for hydrogeological flow and transport in porous environnement.

Two softwares are currently connected to THYRSIS:

- OPENFOAM, through the (opensource) [porousMultiphaseFOAM](https://github.com/phorgue/porousMultiphaseFoam/) toolbox {cite}`horgue-soulaine:2015`.
- METIS (*Modélisation des Écoulements et des Transports avec Interaction en milieu Saturé*), developed by the Ecole des Mines de Paris {cite}`goblet:1989`.

The simulation software is chosen by using the THYRSIS {ref}`Preferences<preferences>` dialog.

```{toctree}
---
maxdepth: 2
glob: on
---
installation/debian.md
installation/windows.md
installation/centos.md
installation/simulation_software_preferences.md
installation/plugin.md
installation/mpi.md
```
