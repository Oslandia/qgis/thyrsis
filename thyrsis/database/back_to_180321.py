"""
upgrade mesh files to integrate mesh neighbours

USAGE

    python -m thyrsis.database.back_to_180321 [database.sqlite]

    upgrade mesh files to integrate mesh neighbours
"""
import os
import sys
from builtins import str

from ..database import sqlite, table_exists, version
from ..log import logger
from ..settings import Settings

SCHEMA_VERSION = "18.03.21"


def upgrade_mesh(db_list, dir_name):
    for database in db_list:
        dbpath = os.path.join(dir_name, database)
        with sqlite.connect(dbpath) as conn:
            cur = conn.cursor()
            v = version(cur)
            logger.notice(dbpath, ": schema version is", v)
            # if v == SCHEMA_VERSION:
            #    continue

            if table_exists(dbpath, "mesh_neighbours"):
                logger.notice("Table mesh_neighbours exists : droping")
                cur.execute("DROP TABLE IF EXISTS mesh_neighbours")

            if v != SCHEMA_VERSION:
                cur.execute("UPDATE metadata SET version='{}'".format(SCHEMA_VERSION))
            conn.commit()


if __name__ == "__main__":
    logger.enable_console(True)

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    if len(sys.argv) == 2 and sys.argv[1][-7:] == ".sqlite":
        db_list = [sys.argv[1]]
        dir_name = "."
        logger.notice("upgrading", sys.argv[1], "database")
    else:
        mesh_dir = str(Settings().value("General", "defaultMeshDir"))
        db_list = sorted(os.listdir(mesh_dir))
        dir_name = mesh_dir
        logger.notice("upgrading", mesh_dir, "directory")

    upgrade_mesh(db_list, dir_name)
