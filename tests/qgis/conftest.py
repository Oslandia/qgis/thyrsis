import os
import shutil
from distutils.spawn import find_executable
from pathlib import Path
from typing import List

import pytest
from PyQt5.QtCore import QObject
from qgis.core import (
    QgsCoordinateReferenceSystem,
    QgsFeature,
    QgsMapLayer,
    QgsProject,
    QgsVectorLayer,
)

from thyrsis.database import load_chemicals, load_forages, sqlite
from thyrsis.log import logger
from thyrsis.mesh_project import MeshProject
from thyrsis.plugin import ThyrsisPlugin
from thyrsis.settings import Settings

TUTORIAL_DATA_DIR = os.path.join(os.path.dirname(__file__), "../..", "tutorial_data")


class DummyLayerTreeView(object):
    def __init__(self):
        """
        Dummy QgsLayerTreeView for test.
        """
        self.selected_layers = []

    def selectedLayers(self) -> List[QgsMapLayer]:
        return self.selected_layers

    def setSelectedLayers(self, layers: List[QgsMapLayer]) -> None:
        self.selected_layers = layers


class DummyInterface(object):
    def __init__(self, canvas):
        """
        Dummy QgsInterface for test.

        @param canvas: QgsCanvas
        """
        self.canvas = canvas
        self.layer = None
        self.tree_view = DummyLayerTreeView()

    def __getattr__(self, *args, **kwargs):
        def dummy(*args, **kwargs):
            return self

        return dummy

    def __iter__(self):
        return self

    def next(self):
        raise StopIteration

    def layers(self):
        # simulate iface.legendInterface().layers()
        return QgsMapLayerRegistry.instance().mapLayers().values()

    def newProject(self):
        QgsProject.instance().clear()

    def mapCanvas(self):
        return self.canvas

    def setCanvas(self, canvas):
        self.canvas = canvas

    def actionDraw(self):
        # Return dummy action for connection simulation
        return Trigger()

    def activeLayer(self):
        return self.layer

    def setActiveLayer(self, layer):
        self.layer = layer

    def layerTreeView(self) -> DummyLayerTreeView:
        return self.tree_view

    def setTreeViewSelectedLayers(self, layers: List[QgsMapLayer]) -> None:
        self.tree_view.setSelectedLayers(layers)


class Trigger(QObject):
    def __init__(self, i=False):
        super().__init__()
        if not i:
            # Return a dummy connection but only once
            self.triggered = Trigger(True)

    def connect(self, *args, **kwargs):
        pass


def pytest_addoption(parser):
    pmf_default_install_dir = ""
    if os.getenv("USERNAME"):
        pmf_default_install_dir = (
            Path.home()
            / "OpenFOAM"
            / str(os.getenv("USERNAME") + "-v2406")
            / "platforms"
            / "linux64GccDPInt32Opt"
        )

    parser.addoption(
        "--openfoam_project_dir",
        action="store",
        default="/usr/lib/openfoam/openfoam2406",
    )
    parser.addoption(
        "--pmf_install_dir", action="store", default=pmf_default_install_dir
    )


@pytest.fixture(autouse=True)
def setup(qgis_app):
    qgis_app.initQgis()


@pytest.fixture(autouse=True)
def teardown(qgis_app):
    qgis_app.exit()


@pytest.fixture()
def mock_iface(qgis_canvas):
    return DummyInterface(qgis_canvas)


@pytest.fixture()
def openfoam_project_dir(request):
    return request.config.getoption("--openfoam_project_dir")


@pytest.fixture()
def pmf_install_dir(request):
    return request.config.getoption("--pmf_install_dir")


@pytest.fixture()
def clean_test_dir(test_dir):
    if os.path.exists(test_dir):
        shutil.rmtree(test_dir, ignore_errors=True)
    Path(test_dir).mkdir(exist_ok=True)
    os.environ["HOME"] = test_dir


@pytest.fixture()
def thyrsis(mock_iface) -> ThyrsisPlugin:
    """
    Fixture to get thyrsis plugin

    @param mock_iface: QgsInterface mock
    @return: thyrsis plugin
    """
    thyrsis = ThyrsisPlugin(mock_iface)
    thyrsis.initGui()

    logger.enable_console(True)
    logger.set_level("debug")

    return thyrsis


@pytest.fixture()
def mesh_project(
    thyrsis: ThyrsisPlugin,
    mesh_db: str,
    openfoam_project_dir,
    pmf_install_dir,
    qgis_canvas,
    mesh_generator,
) -> MeshProject:
    """
    Fixture to create mesh project in a mesh database

    @param openfoam_project_dir: openfoam project directory fixture
    @param thyrsis: thyrsis plugin
    @param mesh_db: path to mesh database
    @param qgis_canvas: QgsCanvas
    @param mesh_generator: mesh generator used (cfmesh or gmsh)
    @return: MeshProject
    """
    QgsProject.instance().setCrs(QgsCoordinateReferenceSystem("EPSG:27572"))
    settings = Settings(os.path.dirname(mesh_db))
    settings.setValue("General", "meshGenerator", mesh_generator)
    settings.setValue("General", "openfoam", openfoam_project_dir)
    settings.setValue("General", "pmf", pmf_install_dir)
    settings.setValue("General", "debug", 1)
    settings.setValue("General", "console", 1)
    settings.save(os.path.dirname(mesh_db))
    return MeshProject.create(mesh_db, settings, qgis_canvas)


@pytest.fixture()
def load_chemicals_csv(site_db: str):
    """
    Load tutorial data chemical csv into site database

    @param site_db: path to site database
    """
    with sqlite.connect(site_db, "ThyrsisMeshTest") as conn:
        # check chemicals loading
        cur = conn.cursor()
        load_chemicals.load_chemicals(
            cur, os.path.join(TUTORIAL_DATA_DIR, "radionuclides.csv")
        )
        conn.commit()


@pytest.fixture()
def insert_saclay_site(site_db: str):
    """
    Insert saclay into site database

    @param site_db: path to site database
    """
    with sqlite.connect(site_db, "ThyrsisMeshTest") as conn:
        cur = conn.cursor()
        assert cur.execute("INSERT INTO sites(id,nom) values (1,'SACLAY')")
        conn.commit()


@pytest.fixture()
def load_forages_csv(site_db):
    """
    Load tutorial data forage csv into site database

    @param site_db: path to site database
    """
    with sqlite.connect(site_db, "ThyrsisMeshTest") as conn:
        # check forages loading
        cur = conn.cursor()
        load_forages.load_forages(
            cur,
            "SACLAY",
            27572,
            os.path.join(TUTORIAL_DATA_DIR, "forages.csv"),
            os.path.join(TUTORIAL_DATA_DIR, "stratigraphie.csv"),
        )
        conn.commit()


@pytest.fixture()
def domains_creation():
    """
    Create domains from tutorial data

    """
    # Check tutorial data contours
    ds_contours = os.path.join(TUTORIAL_DATA_DIR, "contours_example.gpkg")
    lyr_contours = QgsVectorLayer(ds_contours, "contours_example", "ogr")
    assert lyr_contours.isValid()

    features = [ft for ft in lyr_contours.getFeatures()]

    # Default contour is empty
    db_lyr_contours = QgsProject.instance().mapLayersByName("contours")[0]
    assert db_lyr_contours.isValid()
    assert db_lyr_contours.featureCount() == 0

    # Default domaines is empty
    db_lyr_domaines = QgsProject.instance().mapLayersByName("domaines")[0]
    assert db_lyr_domaines.isValid()
    assert len([i for i in db_lyr_domaines.getFeatures()]) == 0

    # Add tutorial data to contour
    db_lyr_contours.startEditing()
    db_lyr_contours.addFeatures(features)
    db_lyr_contours.commitChanges()


@pytest.fixture()
def copy_point_fixes(mesh_project: MeshProject, mesh_db: str):
    """
    Copy point fixes tutorial data into mesh database

    @param mesh_project: fixture to initialize a mesh project
    @param mesh_db: path to mesh database
    """
    # Import tutorial data .csv as delimitedtext layer
    lyr_points_fixes = QgsVectorLayer(
        "file:///"
        + os.path.join(TUTORIAL_DATA_DIR, "point_fixe.csv")
        + "?delimiter=%s&crs=epsg:27572&xField=%s&yField=%s" % (",", "X", "Y"),
        "point_fixe",
        "delimitedtext",
    )
    assert lyr_points_fixes.isValid()
    assert lyr_points_fixes.featureCount() == 17

    features = [ft for ft in lyr_points_fixes.getFeatures()]

    db_lyr_pf = QgsProject.instance().mapLayersByName("points_fixes")[0]

    # Copy tutorial data layer into points_fixes layer
    fts = []
    for i in features:
        ft = QgsFeature(db_lyr_pf.fields())
        ft.setGeometry(i.geometry())
        attr = []
        for fld in db_lyr_pf.fields():
            if fld.name() == "OGC_FID":
                attr.append("Autogenerate")
            elif fld.name() in lyr_points_fixes.fields().names():
                attr.append(i.attribute(fld.name()))
            else:
                attr.append(None)
        ft.setAttributes(attr)
        fts.append(ft)

    assert len(fts) == 17

    db_lyr_pf.startEditing()
    db_lyr_pf.addFeatures(fts)
    db_lyr_pf.commitChanges()

    assert len([i for i in db_lyr_pf.getFeatures()]) == 17


@pytest.fixture()
def copy_point_pilotes(mesh_project, mesh_db):
    """
    Copy point pilotes tutorial data into mesh database

    @param mesh_project: fixture to initialize a mesh project
    @param mesh_db: path to mesh database
    """
    # Import tutorial data .csv as delimitedtext layer
    lyr_point_pilot = QgsVectorLayer(
        "file:///"
        + os.path.join(TUTORIAL_DATA_DIR, "point_pilote.csv")
        + "?delimiter=%s&crs=epsg:27572&xField=%s&yField=%s"
        % (",", "X_L2E%20(m)", "Y_L2E%20(ma)"),
        "point_pilote",
        "delimitedtext",
    )
    assert lyr_point_pilot.isValid()
    assert lyr_point_pilot.featureCount() == 11

    features = [ft for ft in lyr_point_pilot.getFeatures()]
    db_lyr_pp = QgsProject.instance().mapLayersByName("points_pilote")[0]

    # Copy tutorial data layer into points_pilote layer
    fts = []
    for i in features:
        ft = QgsFeature(db_lyr_pp.fields())
        ft.setGeometry(i.geometry())
        attr = []
        for fld in db_lyr_pp.fields():
            if fld.name() == "OGC_FID":
                attr.append("Autogenerate")
            elif fld.name() in lyr_point_pilot.fields().names():
                attr.append(i.attribute(fld.name()))
            else:
                attr.append(None)
        ft.setAttributes(attr)
        fts.append(ft)
    db_lyr_pp.startEditing()
    db_lyr_pp.addFeatures(fts)
    db_lyr_pp.commitChanges()

    assert len([i for i in db_lyr_pp.getFeatures()]) == 11


@pytest.fixture()
def create_mesh(mesh_project: MeshProject, domains_creation):
    """
    Create mesh from mesh project

    @param mesh_project: fixture to get mesh project
    @param domains_creation: fixture to create domains from tutorial data
    """
    mnt = os.path.join(TUTORIAL_DATA_DIR, "mnt.tif")
    mur = os.path.join(TUTORIAL_DATA_DIR, "mur.tif")

    mesh_project.mesh(mnt, mur, None)


@pytest.fixture()
def create_mesh_project(
    mesh_project,
    thyrsis,
    openfoam_project_dir,
    pmf_install_dir,
    mesh_db,
    create_mesh,
    mesh_generator,
):
    """
    Create a mesh project and configure openfoam

    @param mesh_project: fixture to get mesh project
    @param thyrsis: fixture to get thyrsis plugin
    @param openfoam_project_dir: openfoam project directory
    @param pmf_install_dir: pmf install directory
    @param mesh_db: path to mesh database
    @param create_mesh: fixture to create mesh
    @param mesh_generator: mesh generator used (cfmesh or gmsh)
    """
    # Define current mesh project for thyrsis
    thyrsis.project = mesh_project

    # Configure settings for openfoam use
    settings = Settings(os.path.dirname(mesh_db))
    settings.setValue("General", "codeHydro", "openfoam")
    settings.setValue("General", "openfoam", openfoam_project_dir)
    settings.setValue("General", "pmf", pmf_install_dir)
    codeHydroPath = find_executable("openfoam")
    settings.setValue("General", "codeHydroCommand", codeHydroPath)
    settings.setValue("General", "meshGenerator", mesh_generator)
    settings.setValue("General", "debug", 1)
    settings.setValue("General", "console", 1)
    settings.save(os.path.dirname(mesh_db))
