import os
import warnings
from builtins import str
from datetime import datetime

import numpy
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QApplication
from qgis.core import QgsApplication, QgsDataProvider, QgsRectangle

from thyrsis.exception import MeshProviderInitializationError

from .database import elem_to_node, sqlite
from .meshlayer.meshdataprovider import MeshDataProvider
from .settings import Settings
from .utilities import interpolationWeights, unitConversionFactor


def _getElementContaining(conn, point):
    """fetch triangle/quad in which requested point lies and
    return node ids of element

    :param conn: connection on a sqlite database
    :type conn: sqlite3.Connection
    :param point: point xy
    :type point: tuple

    :return: mesh element
    :rtype: list
    """
    cur = conn.cursor()
    # fetch triangle/quad in which requested point lies
    project_SRID = str(
        cur.execute(
            "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
        ).fetchone()[0]
    )
    cur.execute(
        "SELECT a, b, c, d FROM mailles\
        WHERE Intersects(MakePoint("
        + ",".join([str(point[0]), str(point[1]), project_SRID])
        + "), \
                         GEOMETRY)\
        LIMIT 1"
    )  # the limit 1 is for point at the boundary
    res = cur.fetchone()
    if not res:
        warnings.warn("Le point " + str(point) + " n'est pas dans le domaine")
        return None
        # raise RuntimeError("Le point "+str(point)+" n'est pas dans le domaine")
    return res if res[-1] else res[:-1]  # d is null for triangles


class SpatialiteMeshConstDataProvider(MeshDataProvider):
    """Spatialite const mesh data provider"""

    PROVIDER_KEY = "thyrsis_const_provider"

    sourceUnits = {
        "altitude": "m",
        "altitude_mur": "m",
        "potentiel_reference": "m",
        "potentiel": "m",
        "epaisseur_zns": "m",
        "epaisseur_zs": "m",
        "permeabilite_x": "m/s",
        "v_norme": "m/s",
    }

    @classmethod
    def providerKey(cls):
        """Returns the spatialite mesh provider key"""
        return "thyrsis_const_provider"

    @classmethod
    def description(cls):
        """Returns the spatialite mesh provider description"""
        return "mesh const data provider for thyrsis maps"

    @classmethod
    def createProvider(cls, uri, providerOptions):
        return SpatialiteMeshConstDataProvider(uri, providerOptions)

    def __init__(
        self, uri="", providerOptions=QgsDataProvider.ProviderOptions(), flags=None
    ):
        """Constructor

        :param uri: spatialite uri
        :type uri: string
        """
        MeshDataProvider.__init__(self, uri, providerOptions, flags)
        self._flags = flags
        self.__database = str(self.uri().database())
        if not self.__database:
            raise RuntimeError("Missing dbname in uri:" + uri)
        if not self.uri().hasParam("column"):
            raise RuntimeError("Missing column in uri:" + uri)
        self.__column = self.uri().param("column")
        if self.__column not in SpatialiteMeshConstDataProvider.sourceUnits:
            raise RuntimeError("Unknown column %s" % (self.__column))

        self.__second_milieu = (
            self.uri().hasParam("second_milieu")
            and self.uri().param("second_milieu") == "yes"
        )

        self.__nodeValues = []
        self.__elementValues = []  # values at mesh (simulation mesh)
        self.__glElementValues = (
            []
        )  # values at glMesh (for openGL rendering - see triangles)
        self.__mapping = []
        self.__unitConversionFactor = 1.0

        self.update_values()

    def update_values(self):
        """Update mesh values"""
        with sqlite.connect(
            self.__database, "SpatialiteMeshConstDataProvider:update_values"
        ) as conn:
            cur = conn.cursor()

            if self.valueAtElement():
                if self.__second_milieu:
                    cur.execute(
                        "SELECT s.%s \
                            FROM mailles_second_milieu AS s \
                            JOIN mailles AS m ON m.OGC_FID=s.OGC_FID \
                            ORDER BY s.OGC_FID "
                        % (self.__column)
                    )
                    self.__elementValues = numpy.require(cur.fetchall(), numpy.float32)
                    cur.execute(
                        "SELECT s.%s \
                            FROM mailles_second_milieu AS s \
                            JOIN mailles AS m ON m.OGC_FID=s.OGC_FID \
                            WHERE m.d IS NULL \
                            ORDER BY s.OGC_FID "
                        % (self.__column)
                    )
                    res1 = numpy.require(cur.fetchall(), numpy.float32)
                    cur.execute(
                        "SELECT s.%s \
                            FROM mailles_second_milieu AS s \
                            JOIN mailles AS m ON m.OGC_FID=s.OGC_FID \
                            WHERE m.d IS NOT NULL \
                            ORDER BY s.OGC_FID "
                        % (self.__column)
                    )
                    res2 = numpy.require(cur.fetchall(), numpy.float32)
                else:
                    cur.execute(
                        "SELECT %s \
                            FROM mailles \
                            ORDER BY OGC_FID "
                        % (self.__column)
                    )
                    self.__elementValues = numpy.require(cur.fetchall(), numpy.float32)
                    cur.execute(
                        "SELECT %s \
                            FROM mailles \
                            WHERE d IS NULL \
                            ORDER BY OGC_FID "
                        % (self.__column)
                    )
                    res1 = numpy.require(cur.fetchall(), numpy.float32)
                    cur.execute(
                        "SELECT %s \
                            FROM mailles \
                            WHERE d IS NOT NULL \
                            ORDER BY OGC_FID "
                        % (self.__column)
                    )
                    res2 = numpy.require(cur.fetchall(), numpy.float32)
                if len(res1) and len(res2):
                    self.__glElementValues = numpy.concatenate((res1, res2, res2))
                elif len(res1):
                    self.__glElementValues = res1
                else:
                    self.__glElementValues = numpy.concatenate((res2, res2))
            else:
                cur.execute(
                    "SELECT %s FROM noeuds%s ORDER BY OGC_FID"
                    % (self.__column, "_second_milieu" if self.__second_milieu else "")
                )
                self.__nodeValues = numpy.require(cur.fetchall(), numpy.float32, "F")

            self.dataChanged.emit()

    def name(self):
        """Return provider key

        :return: key
        :rtype: string
        """
        return SpatialiteMeshConstDataProvider.PROVIDER_KEY

    def extent(self):
        """Return extent

        :return: extent
        :rtype: QgsRectangle
        """
        with sqlite.connect(
            self.__database, "SpatialiteMeshConstDataProvider:extent"
        ) as conn:
            cur = conn.cursor()
            cur.execute(
                """SELECT
                MIN(X(GEOMETRY)), MIN(Y(GEOMETRY)), MAX(X(GEOMETRY)), MAX(Y(GEOMETRY))
                FROM noeuds"""
            )
            [xmin, ymin, xmax, ymax] = cur.fetchone()
            return QgsRectangle(xmin, ymin, xmax, ymax)

    def nodeCoord(self, zColumn="0"):
        """return a list of coordinates

        :param zColumn: formula for column height or altitude
        :type zColumn: string

        :return: return a list of coordinates
        :rtype: ndarray
        """
        with sqlite.connect(
            self.__database, "SpatialiteMeshConstDataProvider:nodeCoord"
        ) as conn:
            cur = conn.cursor()
            cur.execute(
                "SELECT X(GEOMETRY), Y(GEOMETRY), %s FROM noeuds ORDER BY OGC_FID"
                % (zColumn)
            )
            return numpy.require(cur.fetchall(), numpy.float64, "F")

    def triangles(self):
        """return a list of triangles described by node indices

        :return: triangles
        :rtype: list
        """
        with sqlite.connect(
            self.__database, "SpatialiteMeshConstDataProvider:triangles"
        ) as conn:
            cur = conn.cursor()
            cur.execute(
                "SELECT a, b, c, OGC_FID FROM mailles WHERE d IS NULL ORDER BY OGC_FID"
            )
            idx1 = numpy.require(cur.fetchall(), numpy.int32, "F")
            cur.execute(
                "SELECT a, b, c, OGC_FID FROM mailles WHERE d IS NOT NULL ORDER BY OGC_FID"
            )
            idx2 = numpy.require(cur.fetchall(), numpy.int32, "F")
            cur.execute(
                "SELECT a, c, d, OGC_FID  FROM mailles WHERE d IS NOT NULL ORDER BY OGC_FID"
            )
            idx3 = numpy.require(cur.fetchall(), numpy.int32, "F")
            idx = None
            if len(idx1) and len(idx2):  # triangles and quads
                idx = numpy.concatenate((idx1[:, :3], idx2[:, :3], idx3[:, :3]))
                mapping = numpy.concatenate((idx1[:, 3], idx2[:, 3], idx3[:, 3]))
            elif len(idx1):  # only triangles
                idx = idx1[:, :3]
                mapping = idx1[:, 3]
            else:  # only quads
                idx = numpy.concatenate((idx2[:, :3], idx3[:, :3]))
                mapping = numpy.concatenate((idx2[:, 3], idx3[:, 3]))
            idx -= 1
            mapping -= 1
            self.__mapping = mapping
            return idx

    def nodeValues(self):
        """return values at nodes

        :return: values at nodes
        :rtype: ndarray
        """
        if len(self.__nodeValues):
            return self.__nodeValues
        elif len(self.__elementValues):
            cur = sqlite.connect(self.__database).cursor()
            noeuds = [
                x[0]
                for x in cur.execute(
                    "SELECT OGC_FID FROM noeuds ORDER BY OGC_FID"
                ).fetchall()
            ]
            mailles = cur.execute(
                "SELECT OGC_FID, a, b, c, d FROM mailles ORDER BY OGC_FID"
            ).fetchall()
            values = self.__elementValues
            elem_values = [(v, j + 1) for j, v in enumerate(values)]
            return [v for (v, j) in elem_to_node(noeuds, mailles, elem_values)]
        else:
            return []

    def elementValues(self):
        """return values at openGL elements - for openGL rendering

        :return: values at nodes
        :rtype: ndarray
        """
        if not len(self.__glElementValues):
            return []
        else:
            return self.__unitConversionFactor * self.__glElementValues

    def maxValue(self, index=None):
        """Return max mesh value

        :param index: node index
        :type index: dict

        :return: max value
        :rtype: float
        """
        return (
            numpy.max(self.__nodeValues)
            if not self.valueAtElement()
            else numpy.max(self.__elementValues)
        )

    def minValue(self, index=None):
        """Return max mesh value

        :param index: node index
        :type index: dict

        :return: max value
        :rtype: float
        """
        return (
            numpy.min(self.__nodeValues)
            if not self.valueAtElement()
            else numpy.min(self.__elementValues)
        )

    def resultColumn(self):
        """Return current column

        :return: current column
        :rtype: string
        """
        return str(self.uri().param("column"))

    def valueAtElement(self):
        """Return if the mesh is element based

        :return: state
        :rtype: bool
        """
        return self.uri().param("column") in ("_v_norme", "permeabilite_x")

    def valuesAt(self, point):
        """return a list of values at a given point,
        interpolate values if needed

        :param point: point xy
        :type point: tuple

        :return: value
        :rtype: float
        """
        with sqlite.connect(
            self.__database, "SpatialiteMeshConstDataProvider:valuesAt"
        ) as conn:
            # conn.execute("PRAGMA journal_mode=WAL")
            cur = conn.cursor()
            if not self.valueAtElement():
                element = []
                values = []
                res = _getElementContaining(conn, point)
                if res == None:
                    return None
                for nid in res:
                    cur.execute(
                        "SELECT X(GEOMETRY), Y(GEOMETRY) FROM noeuds \
                            WHERE OGC_FID = "
                        + str(nid)
                    )
                    [x, y] = cur.fetchone()
                    element.append((x, y))
                    values.append(self.__nodeValues[nid - 1, 0])
                values = self.__unitConversionFactor * numpy.array(values)

                weights = interpolationWeights(point, element)
                for i, w in enumerate(weights):
                    values[i] *= w
                return numpy.sum(values, axis=0)
            else:
                project_SRID = str(
                    cur.execute(
                        "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
                    ).fetchone()[0]
                )
                eid = cur.execute(
                    "SELECT OGC_FID FROM mailles \
                        WHERE Intersects(GEOMETRY, MakePoint(%f, %f, %s)) \
                        LIMIT 1"
                    % (point[0], point[1], project_SRID)
                ).fetchone()
                if not eid:
                    raise RuntimeError(
                        "Le point " + str(point) + " n'est pas dans le domaine"
                    )
                else:
                    return (
                        self.__elementValues[eid[0] - 1, 0]
                        * self.__unitConversionFactor
                    )

    def conversionFactor(self, units=None):
        """conversion from the set units to any units,
        if units is not specify, return in SI units

        :param units: units
        :type units: string

        :return: factor
        :rtype: float
        """

        sourceUnits = SpatialiteMeshConstDataProvider.sourceUnits[self.resultColumn()]

        return (
            1.0 / self.__unitConversionFactor
            if not units
            else unitConversionFactor(sourceUnits, units) / self.__unitConversionFactor
        )


class SpatialiteMeshDataProvider(MeshDataProvider):
    """Spatialite mesh data provider"""

    PROVIDER_KEY = "thyrsis_provider"

    unitsChanged = pyqtSignal(str)
    columnChanged = pyqtSignal(str)
    sourceUnits = {
        "concentration": "kg/m³",
        "vitesse_darcy_norme": "m/s",
        "potentiel": "m",
        "saturation": "-",
        "probability": "-",
        "concentration2": "kg/m³",
        "vitesse_darcy_norme2": "m/s",
        "potentiel2": "m",
        "saturation2": "-",
        "probability2": "-",
    }
    displayName = {
        "concentration": "Concentration",
        "vitesse_darcy_norme": "Vitesse de Darcy",
        "potentiel": "Potentiel",
        "saturation": "Saturation",
        "probability": "Probabilité",
        "concentration2": "Concentration second milieu",
        "vitesse_darcy_norme2": "Vitesse de Darcy second milieu",
        "potentiel2": "Potentiel second milieu",
        "saturation2": "Saturation second milieu",
        "probability2": "Probabilité second milieu",
    }

    @classmethod
    def providerKey(cls):
        """Returns the spatialite mesh provider key"""
        return "thyrsis_provider"

    @classmethod
    def description(cls):
        """Returns the spatialite mesh provider description"""
        return 'mesh data provider for thyrsis sqlite backend"'

    @classmethod
    def createProvider(cls, uri, providerOptions):
        return SpatialiteMeshDataProvider(uri, providerOptions)

    def __init__(
        self, uri="", providerOptions=QgsDataProvider.ProviderOptions(), flags=None
    ):
        """Constructor

        :param uri: spatialite uri
        :type uri: string
        """
        MeshDataProvider.__init__(self, uri, flags)
        self._flags = flags
        self.__database = str(self.uri().database())
        if not self.__database:
            raise RuntimeError("Missing dbname in uri:" + uri)
        self.__databasePath = os.path.dirname(os.path.abspath(self.__database))
        self.__settings = Settings(self.__databasePath)
        self.__activite_specifique = 0.0
        self.__conn = sqlite.connect(self.__database)
        cur = self.__conn.cursor()
        cur.execute("SELECT date FROM dates_simulation ORDER BY id")
        self.setDates([date for [date] in cur.fetchall()])
        self.__basename = cur.execute("PRAGMA database_list").fetchone()[2][:-7]
        c = cur.execute(
            """SELECT activite_specifique
             FROM elements_chimiques WHERE id = (SELECT eid FROM simulations)"""
        ).fetchone()
        self.__activite_specifique = c[0] if c else 0.0
        self.__contours = cur.execute(
            "SELECT nom, groupe, AsText(GEOMETRY) FROM contours"
        ).fetchall()
        self.__scatters = []
        c = cur.execute(
            """SELECT nom, groupe, X(GEOMETRY), Y(GEOMETRY), altitude
            FROM points_interet
            union SELECT nom, 'forage', X(POINT), Y(POINT), zsol
            FROM forages"""
        ).fetchall()
        for nom, groupe, x, y, z in c:
            if _getElementContaining(self.__conn, (x, y)):
                self.__scatters.append(
                    {
                        "name": nom,
                        "point": [
                            x,
                            y,
                            z if z is not None else self.altitudeAt((x, y)),
                        ],
                        "group": groupe,
                    }
                )

        self.__nodeValues = []
        self.__nodeSValues = []
        self.__elementValues = []
        self.__elementSValues = []
        self.__mapping = []
        self.__unitConversionFactor = 1.0
        self.__units = ""
        self.__availableColumns = self.availableColumns()
        if len(self.__availableColumns) == 0:
            raise MeshProviderInitializationError(
                f"No columns available for file {self.__databasePath}"
            )

        if self.uri().hasParam("resultColumn"):
            self.setResultColumn(self.resultColumn())
        elif self.__availableColumns:
            self.setResultColumn(self.__availableColumns[0][1])
        if not self.resultColumn():
            raise MeshProviderInitializationError(
                f"No result column defined for file {self.__databasePath}"
            )

        if not self.uri().hasParam("units") and self.resultColumn():
            self.setUnits(SpatialiteMeshDataProvider.sourceUnits[self.resultColumn()])
        elif self.resultColumn():
            self.setUnits(self.uri().param("units"))

        self.xmlLoaded.connect(self.__updateUnits)
        self.setDate(len(self.dates()) - 1)

    def getDatabasePath(self):
        return self.__databasePath

    def __updateUnits(self):
        self.setUnits(self.units())

    def settings(self):
        """Return Thyrsis settings

        :return: Thyrsis settings
        :rtype: Settings
        """
        return self.__settings

    def contours(self):
        """Return a map with the contour (name, group) as key and an array of vertices
        as values, contours are 3D

        :return: contours
        :rtype: dict
        """
        res = {}
        for nom, group, geom in self.__contours:
            res[(nom, group)] = numpy.array(
                [
                    [float(c) for c in coord.split()]
                    for coord in geom.replace("LINESTRING Z", "")
                    .replace("(", "")
                    .replace(")", "")
                    .split(",")
                ]
            )
        return res

    def scatters(self):
        """return a map with the contour name as key and an point as value

        :return: scatters
        :rtype: list
        """
        return self.__scatters

    def name(self):
        """Return provider key

        :return: key
        :rtype: string
        """
        return SpatialiteMeshDataProvider.PROVIDER_KEY

    def setUnits(self, units):
        """Set units

        :param units: units
        :type units: string
        """
        self.__units = units
        if self.uri().hasParam("units"):
            self.uri().removeParam("units")
        self.uri().setParam("units", units)
        sourceUnits = SpatialiteMeshDataProvider.sourceUnits[self.resultColumn()]
        if units != sourceUnits:
            self.__unitConversionFactor = unitConversionFactor(
                sourceUnits, units, self.__activite_specifique
            )
        else:
            self.__unitConversionFactor = 1.0
        self.unitsChanged.emit(units)
        self.dataChanged.emit()

    def availableUnits(self):
        """Return available units

        :return: available units
        :rtype: dict
        """
        return (
            {
                "concentration": [
                    "kg/L",
                    "g/L",
                    "mg/L",
                    "µg/L",
                    "kg/m³",
                    "g/m³",
                    "mg/m³",
                    "µg/m³",
                    "GBq/L",
                    "MBq/L",
                    "kBq/L",
                    "Bq/L",
                    "mBq/L",
                    "µBq/L",
                    "GBq/m³",
                    "MBq/m³",
                    "kBq/m³",
                    "Bq/m³",
                    "mBq/m³",
                    "µBq/m³",
                ],
                "vitesse_darcy_norme": ["m/s", "m/s", "m/an", "cm/j"],
                "potentiel": ["m"],
                "saturation": ["-"],
                "probability": ["-"],
            }[self.resultColumn().replace("2", "")]
            if self.resultColumn()
            else []
        )

    def conversionFactor(self, units=None):
        """conversion from the set units to any units,
        if units is not specify, return in SI units

        :param units: units
        :type units: string
        """

        sourceUnits = SpatialiteMeshDataProvider.sourceUnits[self.resultColumn()]

        return (
            1.0 / self.__unitConversionFactor
            if not units
            else unitConversionFactor(sourceUnits, units, self.__activite_specifique)
            / self.__unitConversionFactor
        )

    def units(self):
        """Return current units

        :return: units
        :rtype: string
        """
        return self.__units

    def availableColumns(self):
        """returns a list (display name, colummn name)
        for available results

        :return: available column
        :rtype: list
        """
        # Il faut faire un listdir pour synchroniser en cas de calcul mpi (pb nfs)
        dirname = os.path.dirname(os.path.abspath(self.__database))

        available = []
        npy_files = [
            x.split(".")[1]
            for x in os.listdir(dirname)
            if ".npy" in x and not ".insat" in x
        ]
        for columnName in list(SpatialiteMeshDataProvider.sourceUnits.keys()):
            if columnName in npy_files:
                available.append(
                    (SpatialiteMeshDataProvider.displayName[columnName], columnName)
                )
        return available

    def resultColumn(self):
        """Return current column

        :return: current column
        :rtype: string
        """
        return str(self.uri().param("resultColumn"))

    def valueAtElement(self):
        """Return if the mesh is element based

        :return: state
        :rtype: bool
        """
        return os.path.exists(self.__basename + "." + self.resultColumn() + ".elem.npy")

    def setResultColumn(self, columnName):
        """Set the current column

        :param columnName: columnName
        :type columnName: string
        """
        # load results since it's the bottleneck for rendering
        reloadNeeded = columnName and (
            self.resultColumn() != columnName
            or not (len(self.__nodeValues) + len(self.__elementValues))
        )
        if self.uri().hasParam("resultColumn"):
            self.uri().removeParam("resultColumn")
        self.uri().setParam("resultColumn", columnName)
        if reloadNeeded:
            self.__elementValues = []
            self.__elementSValues = []
            self.__nodeValues = []
            self.__nodeSValues = []

            binary_file = self.__basename + "." + columnName
            pbinary_file = self.__basename + ".s" + columnName
            if self.valueAtElement():
                extension = ".elem.npy"
                if os.path.exists(binary_file + extension):
                    self.__elementValues = numpy.load(binary_file + extension)
                if os.path.exists(pbinary_file + extension):
                    self.__elementSValues = numpy.load(pbinary_file + extension)
            else:
                extension = ".node.npy"
                if os.path.exists(binary_file + extension):
                    self.__nodeValues = numpy.load(binary_file + extension)
                if os.path.exists(pbinary_file + extension):
                    self.__nodeSValues = numpy.load(pbinary_file + extension)

            column = columnName.replace("2", "").replace("vitesse_darcy_norme", "darcy")
            self.setUnits(
                self.__settings.value(
                    "Variables",
                    column + "Unit",
                    SpatialiteMeshDataProvider.sourceUnits[columnName],
                )
            )

            self.columnChanged.emit(columnName)

    def removeColumn(self, columnName):
        """Use to remove npy files when re-run simulation
        otherwise mesh form previous simulation will be available
        like probability map

        :param columnName: columnName
        :type columnName: string
        """
        binary_file = self.__basename + "." + columnName
        pbinary_file = self.__basename + ".s" + columnName
        if self.valueAtElement():
            extension = ".elem.npy"
            if os.path.exists(binary_file + extension):
                os.remove(binary_file + extension)
            if os.path.exists(pbinary_file + extension):
                os.remove(pbinary_file + extension)
        else:
            extension = ".node.npy"
            if os.path.exists(binary_file + extension):
                os.remove(binary_file + extension)
            if os.path.exists(pbinary_file + extension):
                os.remove(pbinary_file + extension)

    def database(self):
        """Return db path

        :return: db path
        :rtype: string
        """
        return self.__database

    def extent(self):
        """Return extent

        :return: extent
        :rtype: QgsRectangle
        """
        cur = self.__conn.cursor()
        cur.execute(
            """SELECT
            MIN(X(GEOMETRY)), MIN(Y(GEOMETRY)), MAX(X(GEOMETRY)), MAX(Y(GEOMETRY))
            FROM noeuds"""
        )
        [xmin, ymin, xmax, ymax] = cur.fetchone()
        return QgsRectangle(xmin, ymin, xmax, ymax)

    def nodeCoord(self, zColumn="0"):
        """return a list of coordinates

        :param zColumn: formula for column height or altitude
        :type zColumn: string

        :return: return a list of coordinates
        :rtype: ndarray
        """
        cur = self.__conn.cursor()
        cur.execute(
            "SELECT X(GEOMETRY), Y(GEOMETRY), %s FROM noeuds ORDER BY OGC_FID"
            % (zColumn)
        )
        return numpy.require(cur.fetchall(), numpy.float64, "F")

    def triangles(self):
        """return a list of triangles described by node indices

        :return: triangles
        :rtype: list
        """
        cur = self.__conn.cursor()
        cur.execute(
            "SELECT a, b, c, OGC_FID FROM mailles WHERE d IS NULL ORDER BY OGC_FID"
        )
        idx1 = numpy.require(cur.fetchall(), numpy.int32, "F")
        cur.execute(
            "SELECT a, b, c, OGC_FID FROM mailles WHERE d IS NOT NULL ORDER BY OGC_FID"
        )
        idx2 = numpy.require(cur.fetchall(), numpy.int32, "F")
        cur.execute(
            "SELECT a, c, d, OGC_FID  FROM mailles WHERE d IS NOT NULL ORDER BY OGC_FID"
        )
        idx3 = numpy.require(cur.fetchall(), numpy.int32, "F")
        idx = None
        if len(idx1) and len(idx2):  # triangles and quads
            idx = numpy.concatenate((idx1[:, :3], idx2[:, :3], idx3[:, :3]))
            mapping = numpy.concatenate((idx1[:, 3], idx2[:, 3], idx3[:, 3]))
        elif len(idx1):  # only triangles
            idx = idx1[:, :3]
            mapping = idx1[:, 3]
        else:  # only quads
            idx = numpy.concatenate((idx2[:, :3], idx3[:, :3]))
            mapping = numpy.concatenate((idx2[:, 3], idx3[:, 3]))
        idx -= 1
        mapping -= 1
        self.__mapping = mapping
        return idx

    def nodeValues(self):
        """return values at nodes

        :return: values at nodes
        :rtype: ndarray
        """
        if len(self.__nodeValues):
            return self.__unitConversionFactor * self.__nodeValues[self.date()]
        elif len(self.__elementValues):
            cur = self.__conn.cursor()
            noeuds = [
                x[0]
                for x in cur.execute(
                    "SELECT OGC_FID FROM noeuds ORDER BY OGC_FID"
                ).fetchall()
            ]
            mailles = cur.execute(
                "SELECT OGC_FID, a, b, c, d FROM mailles ORDER BY OGC_FID"
            ).fetchall()
            values = (
                self.__unitConversionFactor
                * self.__elementValues[self.date()][self.__mapping]
            )
            elem_values = [(v, j + 1) for j, v in enumerate(values)]
            return [v for (v, j) in elem_to_node(noeuds, mailles, elem_values)]
        else:
            return []

    def elementValues(self):
        """return values at elements

        :return: values at nodes
        :rtype: ndarray
        """
        if not len(self.__elementValues):
            return []
        else:
            return (
                self.__unitConversionFactor
                * self.__elementValues[self.date()][self.__mapping]
            )

    def maxValue(self, index=None):
        """Return max mesh value

        :param index: node index
        :type index: dict

        :return: max value
        :rtype: float
        """
        array = self.__nodeValues if not self.valueAtElement() else self.__elementValues
        if not len(array):
            return float("NaN")
        if index:
            return self.__unitConversionFactor * numpy.max(array[index])
        else:
            return self.__unitConversionFactor * max(
                [max_ for max_ in [numpy.max(values) for values in array]]
            )

    def minValue(self, index=None):
        """Return min mesh value

        :param index: node index
        :type index: dict

        :return: min value
        :rtype: float
        """
        array = self.__nodeValues if not self.valueAtElement() else self.__elementValues
        if not len(array):
            return float("NaN")
        if index:
            return self.__unitConversionFactor * numpy.min(array[index])
        else:
            return self.__unitConversionFactor * min(
                [min_ for min_ in [numpy.min(values) for values in array]]
            )

    def valuesAt(self, point, svalue=False):
        """return a list of values at a given point,
        interpolate values if needed, returns sigma values if required and available

        :param point: point xy
        :type point: tuple
        :param svalue: sigma values flag
        :type svalue: bool

        :return: value
        :rtype: float
        """
        cur = self.__conn.cursor()
        if not self.valueAtElement():
            element = []
            values = []
            svalues = []
            hassval = len(self.__nodeSValues)
            res = _getElementContaining(self.__conn, point)
            if res == None:
                return None
            for nid in res:
                cur.execute(
                    "SELECT X(GEOMETRY), Y(GEOMETRY) FROM noeuds \
                        WHERE OGC_FID = "
                    + str(nid)
                )
                [x, y] = cur.fetchone()
                element.append((x, y))
                values.append(self.__nodeValues[:, nid - 1])
                if hassval:
                    svalues.append(self.__nodeSValues[:, nid - 1])
            values = self.__unitConversionFactor * numpy.array(values)
            svalues = self.__unitConversionFactor * numpy.array(svalues)

            weights = interpolationWeights(point, element)
            avg_values = numpy.average(values, axis=0, weights=weights)
            if hassval:
                avg_svalues = numpy.average(svalues, axis=0, weights=weights)
            else:
                avg_svalues = []
        else:
            project_SRID = str(
                cur.execute(
                    "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
                ).fetchone()[0]
            )
            # FR200729 calcul par moyenne sur toutes les mailles contenant le point
            #   pour traiter le cas d'un point confondu avec un noeud
            hassval = len(self.__elementSValues)
            elements = cur.execute(
                """SELECT OGC_FID, a, b, c, d FROM mailles
                            WHERE PtDistWithin(GEOMETRY, MakePoint(%f, %f, %s), 0.1)
                        """
                % (point[0], point[1], project_SRID)
            ).fetchall()
            if not elements:
                warnings.warn("Le point " + str(point) + " n'est pas dans le domaine")
                return None
            elif len(elements) == 1:
                # FR210713 the point is located inside a mesh :
                # first computing values at mesh nodes then interpolating
                nodes_values = []
                nodes_svalues = []
                nodes_element = []
                for n in elements[0][1:]:
                    if n:
                        xy_node = cur.execute(
                            "SELECT X(GEOMETRY), Y(GEOMETRY) from noeuds where OGC_FID=%s"
                            % (n)
                        ).fetchone()
                        elts_node = [
                            m[0]
                            for m in cur.execute(
                                """SELECT OGC_FID FROM mailles
                            WHERE PtDistWithin(GEOMETRY, MakePoint(%f, %f, %s), 0.1)
                        """
                                % (xy_node[0], xy_node[1], project_SRID)
                            ).fetchall()
                        ]

                        # computing distances from point to adjacent meshes
                        dists = []
                        for m in elts_node:
                            cur.execute(
                                """SELECT ST_Distance(MakePoint(%f, %f, %s), CENTROID(GEOMETRY))
                                            FROM mailles
                                            WHERE OGC_FID = %s"""
                                % (xy_node[0], xy_node[1], project_SRID, m)
                            )
                            dists.append(cur.fetchone()[0])
                        weights = [
                            numpy.prod([x for i, x in enumerate(dists) if i != j])
                            for j, y in enumerate(dists)
                        ]
                        weights = weights / sum(weights)
                        values = [
                            self.__elementValues[:, m - 1] * self.__unitConversionFactor
                            for m in elts_node
                        ]
                        avg_values = numpy.average(values, axis=0, weights=weights)
                        if hassval:
                            svalues = [
                                self.__elementSValues[:, m - 1]
                                * self.__unitConversionFactor
                                for m in elts_node
                            ]
                            avg_svalues = numpy.average(
                                svalues, axis=0, weights=weights
                            )
                        else:
                            avg_svalues = []

                        nodes_element.append(xy_node)
                        nodes_values.append(avg_values)
                        nodes_svalues.append(avg_svalues)

                # barycentric interpolation on nodes
                nodes_values = numpy.array(nodes_values)
                nodes_svalues = numpy.array(nodes_svalues)
                weights = interpolationWeights(point, nodes_element)
                avg_values = numpy.average(nodes_values, axis=0, weights=weights)
                avg_svalues = numpy.average(nodes_svalues, axis=0, weights=weights)

            else:
                # point is a node
                dists = []
                for e in elements:
                    cur.execute(
                        """SELECT ST_Distance(MakePoint(%f, %f, %s), CENTROID(GEOMETRY))
                                    FROM mailles
                                    WHERE OGC_FID = %s"""
                        % (point[0], point[1], project_SRID, e[0])
                    )
                    dists.append(cur.fetchone()[0])
                weights = [
                    numpy.prod([x for i, x in enumerate(dists) if i != j])
                    for j, y in enumerate(dists)
                ]
                weights = weights / sum(weights)
                values = [
                    self.__elementValues[:, e[0] - 1] * self.__unitConversionFactor
                    for e in elements
                ]
                avg_values = numpy.average(values, axis=0, weights=weights)
                if hassval:
                    svalues = [
                        self.__elementSValues[:, e[0] - 1] * self.__unitConversionFactor
                        for e in elements
                    ]
                    avg_svalues = numpy.average(svalues, axis=0, weights=weights)
                else:
                    avg_svalues = []

        if svalue:
            return (avg_values, avg_svalues)
        else:
            return avg_values

    def measuresAt(self, point, withinDistance=0.5):
        """returns a map of measures in the vicinity of the considered point
        the map as the mesure type as key and
        [(date, value, uncertainty)] as values

        :param point: point xy
        :type point: tuple
        :param withinDistance: snap distance
        :type withinDistance: float

        :return: measures
        :rtype: dict
        """
        if not self.resultColumn().startswith(
            "potentiel"
        ) and not self.resultColumn().startswith("concentration"):
            return {}
        measures = {}
        cur = self.__conn.cursor()
        project_SRID = str(
            cur.execute(
                "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
            ).fetchone()[0]
        )

        cur.execute(
            "SELECT OGC_FID, nom FROM points_interet \
                WHERE PtDistWithin(GEOMETRY, MakePoint(%f, %f, %s), %f)"
            % (point[0], point[1], project_SRID, withinDistance)
        )
        for pid, nom in cur.fetchall():
            if self.resultColumn().startswith("potentiel"):
                cur.execute(
                    "SELECT type, date, potentiel, incertitude_potentiel \
                        FROM mesures WHERE pid=%d AND potentiel IS NOT NULL"
                    % (pid)
                )
            if self.resultColumn().startswith("concentration"):
                cur.execute(
                    "SELECT type, date, concentration, incertitude_concentration \
                        FROM mesures WHERE pid=%d AND concentration IS NOT NULL \
                        AND eid=(SELECT eid FROM simulations)"
                    % (pid)
                )
            for type_, date, valeur, incertitude in cur.fetchall():
                if type_ not in measures:
                    measures[type_] = []
                format = "%Y-%m-%d %H:%M:%S" if ":" in date else "%Y-%m-%d"
                measures[type_].append(
                    (
                        datetime.strptime(date.replace("T", " "), format),
                        valeur * self.__unitConversionFactor,
                        (incertitude * self.__unitConversionFactor)
                        if incertitude
                        else 0,
                    )
                )
        return measures

    def altitudeAt(self, point):
        """return altitude given point,
        interpolate values if needed

        :param point: point xy
        :type point: tuple

        :return: value
        :rtype: float
        """

        cur = self.__conn.cursor()
        element = []
        altitudes = []
        for nid in _getElementContaining(self.__conn, point):
            cur.execute(
                "SELECT X(GEOMETRY), Y(GEOMETRY), altitude FROM noeuds \
                    WHERE OGC_FID = "
                + str(nid)
            )
            [x, y, z] = cur.fetchone()
            element.append((x, y))
            altitudes.append(z)
        altitudes = numpy.array(altitudes)
        weights = interpolationWeights(point, element)

        for i, w in enumerate(weights):
            altitudes[i] *= w
        return numpy.sum(altitudes, axis=0)

    def mass_balance(self):
        """compute mass balance

        :return sat: sat values
        :rtype sat: list
        :return out: out values
        :rtype out: list
        :return insat: insat values
        :rtype insat: list
        """
        cur = self.__conn.cursor()
        cur.execute("SELECT sat, out FROM bilan_masse ORDER BY did")
        values = numpy.require(cur.fetchall(), numpy.float32)
        if not len(values):
            return None, None, None
        sat = values[:, 0]
        out = values[:, 1]
        cur.execute("SELECT zns FROM bilan_masse WHERE zns IS NOT NULL ORDER BY did")
        insat = numpy.array(cur.fetchall(), numpy.float32)
        return sat, out, insat

    def conn(self):
        """Return database connection

        :retunr: db connection
        :rtype: sqlite3.Connection
        """
        return self.__conn


if __name__ == "__main__":
    # test the spatialite data provider
    # USAGE: spatialitemeshdataprovider.py uri output image
    import sys

    from thyrsis.meshlayer.meshdataproviderregistry import MeshDataProviderRegistry
    from thyrsis.meshlayer.meshlayer import MeshLayer

    assert len(sys.argv) == 3

    QgsApplication.setPrefixPath("/usr/local", True)
    QgsApplication.initQgis()
    app = QApplication(sys.argv)

    MeshDataProviderRegistry.instance().addDataProviderType(
        SpatialiteMeshDataProvider.PROVIDER_KEY, SpatialiteMeshDataProvider
    )

    layer = MeshLayer(
        "dbname=%s crs=epsg:27572 resultColumn=%s" % (sys.argv[1], sys.argv[2]),
        "test_layer",
        SpatialiteMeshDataProvider.PROVIDER_KEY,
    )

    MeshDataProviderRegistry.instance().removeDataProviderType(
        SpatialiteMeshDataProvider.PROVIDER_KEY
    )
