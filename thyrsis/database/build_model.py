"""
Build model from data contained by a folder

USAGE

    python -m thyrsis.database.build_model [-o dot_thyrsis_path] path_to_data_folder
"""

import getopt
import os
import shutil
import sys
from distutils.spawn import find_executable
from pathlib import Path

from osgeo.osr import SpatialReference
from qgis.core import (
    QgsApplication,
    QgsCoordinateReferenceSystem,
    QgsDataSourceUri,
    QgsFeature,
    QgsProject,
    QgsVectorLayer,
)
from qgis.gui import QgsMapCanvas

from thyrsis.database import sqlite
from thyrsis.exception import ModelCreationError
from thyrsis.log import logger
from thyrsis.mesh_project import MeshProject, get_layer
from thyrsis.meshlayer.meshlayer import MeshLayer
from thyrsis.meshlayer.utilities import format_
from thyrsis.plugin import ThyrsisPlugin
from thyrsis.settings import Settings
from thyrsis.spatialitemeshdataprovider import SpatialiteMeshConstDataProvider


class DummyInterface(object):
    def __init__(self, canvas):
        self.canvas = canvas

    def __getattr__(self, *args, **kwargs):
        def dummy(*args, **kwargs):
            return self

        return dummy

    def __iter__(self):
        return self

    def next(self):
        raise StopIteration

    def layers(self):
        # simulate iface.legendInterface().layers()
        return QgsProject.instance().mapLayers().values()

    def newProject(self):
        QgsProject.instance().clear()

    def mapCanvas(self):
        return self.canvas


class Model:
    def __init__(
        self,
        folder,
        project_param,
        hynverse_param,
        simulation_param,
        contours,
        mnt,
        mur,
        potentiel=None,
        points_fixes=None,
        points_pilote=None,
        permeability_raster=None,
        const_perm=None,
    ):
        self.site_db = os.path.join(folder, ".thyrsis", "sites.sqlite")
        with open(project_param) as file:
            for line in file.readlines():
                key, value = line.split(":")
                if key == "dbname":
                    self.dbname = value.replace("\n", "")
                elif key == "srid":
                    self.srid = int(value.replace("\n", ""))
                elif key == "codeHydro":
                    self.codeHydro = value.replace("\n", "")
                elif key == "meshGenerator":
                    self.meshGenerator = value.replace("\n", "")
        self.site = self.dbname.split("_")[0]
        self.mesh_db = os.path.join(
            folder, ".thyrsis", "mesh", self.dbname + ".mesh.sqlite"
        )
        if os.path.exists(self.mesh_db):
            os.remove(self.mesh_db)
        self.settings = Settings(os.path.dirname(self.mesh_db))
        print("codeHydro =", self.codeHydro)
        self.settings.setValue("General", "codeHydro", self.codeHydro)
        codeHydroPath = find_executable(self.codeHydro)
        print("codeHydroPath =", codeHydroPath)
        self.settings.setValue("General", "codeHydroCommand", codeHydroPath)

        print("meshGenerator =", self.meshGenerator)
        self.settings.setValue("General", "meshGenerator", self.meshGenerator)

        self.settings.save(os.path.dirname(self.mesh_db))

        self.canvas = QgsMapCanvas()
        self.iface = DummyInterface(self.canvas)
        self.thyrsis = ThyrsisPlugin(self.iface)
        self.thyrsis.initGui()

        self.contours = contours
        self.mnt = mnt
        self.mur = mur
        self.potentiel = potentiel
        self.hynverse_param = hynverse_param
        self.simulation_param = simulation_param
        self.points_fixes = points_fixes

        if points_pilote:
            self.method = 1
            self.data = points_pilote
        elif permeability_raster:
            self.method = 2
            self.data = permeability_raster
        else:
            self.method = 3
            self.data = const_perm

    def createMeshProject(self):
        logger.notice("==Create Mesh Project==")
        with sqlite.connect(self.site_db, "ThyrsisBuildModel") as conn:
            cur = conn.cursor()
            existing_sites = cur.execute("SELECT id,nom from sites").fetchall()
            ids = [site[0] for site in existing_sites]
            names = [site[1] for site in existing_sites]
            if self.site not in names:
                cur.execute(
                    "INSERT INTO sites(id,nom) values ({},'{}')".format(
                        max(ids if len(ids) > 0 else [-1]) + 1, self.site
                    )
                )
                conn.commit()

        proj = SpatialReference()
        proj.ImportFromEPSG(self.srid)
        QgsProject.instance().setCrs(QgsCoordinateReferenceSystem(proj.ExportToWkt()))
        MeshProject.create(self.mesh_db, self.settings, self.canvas)

    def createDomain(self):
        logger.notice("==Create Domain==")
        meshProject = MeshProject(self.mesh_db)
        self.thyrsis.project = meshProject
        lyr_contours = QgsVectorLayer(self.contours, "contours_in", "ogr")
        features = [ft for ft in lyr_contours.getFeatures()]

        db_lyr_contours = QgsProject.instance().mapLayersByName("contours")[0]
        for i in features:
            ft = QgsFeature(db_lyr_contours.fields())
            ft.setGeometry(i.geometry())
            attr = []
            for fld in db_lyr_contours.fields():
                if fld.name() == "OGC_FID":
                    attr.append(None)
                elif fld.name() in lyr_contours.fields().names():
                    attr.append(i.attribute(fld.name()))
                else:
                    attr.append(None)
            ft.setAttributes(attr)
            db_lyr_contours.startEditing()
            db_lyr_contours.addFeatures([ft])
            db_lyr_contours.commitChanges()

        if self.points_fixes:
            lyr_points_fixes = QgsVectorLayer(
                "file:///"
                + self.points_fixes
                + "?delimiter=%s&crs=epsg:%s&xField=%s&yField=%s"
                % (",", str(self.srid), "X", "Y"),
                "points_fixes",
                "delimitedtext",
            )
            features = [ft for ft in lyr_points_fixes.getFeatures()]

            # Using loaded layer leads to locked databased, due to a 17 schema error
            uri = QgsDataSourceUri()
            uri.setDatabase(self.mesh_db)
            uri.setDataSource("", "points_fixes", "GEOMETRY")
            db_lyr_pf = QgsVectorLayer(uri.uri(), "points_fixes", "spatialite")

            fts = []
            for i in features:
                ft = QgsFeature(db_lyr_pf.fields())
                ft.setGeometry(i.geometry())
                attr = []
                for fld in db_lyr_pf.fields():
                    if fld.name() == "OGC_FID":
                        attr.append("Autogenerate")
                    elif fld.name() in lyr_points_fixes.fields().names():
                        attr.append(i.attribute(fld.name()))
                    else:
                        attr.append(None)
                ft.setAttributes(attr)
                fts.append(ft)
            db_lyr_pf.startEditing()
            db_lyr_pf.addFeatures(fts)
            db_lyr_pf.commitChanges()

            origin_lyr = QgsProject.instance().mapLayersByName("points_fixes")[0]
            QgsProject.instance().removeMapLayer(origin_lyr.id())
            QgsProject.instance().addMapLayer(db_lyr_pf)

    def createMesh(self):
        logger.notice("==Create Mesh==")
        meshProject = MeshProject(self.mesh_db)
        self.thyrsis.project = meshProject
        meshProject.mesh(self.mnt, self.mur, self.potentiel)

    def createModel(self):
        logger.notice("==Create Model==")
        with open(self.hynverse_param) as file, sqlite.connect(self.mesh_db) as conn:
            cur = conn
            for line in file:
                key = line.split(":")[0]
                value = line.split(":")[1]
                cur.execute("UPDATE hynverse_parametres set {}={}".format(key, value))
            conn.commit()

        if self.simulation_param:
            with open(self.simulation_param) as file, sqlite.connect(
                self.mesh_db
            ) as conn:
                cur = conn
                for line in file:
                    key = line.split(":")[0]
                    value = float(line.split(":")[1])
                    sql = "UPDATE parametres set valeur={}, valeur_min={}, valeur_max={} where nom='{}'".format(
                        value, value, value, key
                    )
                    print(sql)
                    cur.execute(sql)
                conn.commit()

        if self.method == 1:

            lyr_point_pilot = QgsVectorLayer(
                "file:///"
                + self.data
                + "?delimiter=%s&crs=epsg:%s&xField=%s&yField=%s"
                % (",", str(self.srid), "X", "Y"),
                "points_pilote",
                "delimitedtext",
            )
            features = [ft for ft in lyr_point_pilot.getFeatures()]

            # Using loaded layer leads to locked databased, due to a 17 schema error
            uri = QgsDataSourceUri()
            uri.setDatabase(self.mesh_db)
            uri.setDataSource("", "points_pilote", "GEOMETRY")
            db_lyr_pp = QgsVectorLayer(uri.uri(), "points_pilote", "spatialite")

            fts = []
            for i in features:
                ft = QgsFeature(db_lyr_pp.fields())
                ft.setGeometry(i.geometry())
                attr = []
                for fld in db_lyr_pp.fields():
                    if fld.name() == "OGC_FID":
                        attr.append("Autogenerate")
                    elif fld.name() in lyr_point_pilot.fields().names():
                        attr.append(i.attribute(fld.name()))
                    else:
                        attr.append(None)
                ft.setAttributes(attr)
                fts.append(ft)
            db_lyr_pp.startEditing()
            db_lyr_pp.addFeatures(fts)
            db_lyr_pp.commitChanges()

            # update layer in canvas
            origin_lyr = QgsProject.instance().mapLayersByName("points_pilote")[0]
            QgsProject.instance().removeMapLayer(origin_lyr.id())
            QgsProject.instance().addMapLayer(db_lyr_pp)

            self.thyrsis.inverse(test=True)

        elif self.method == 2:
            self.thyrsis.model_no_inverse(param=self.data, const=False, test=True)
        else:
            self.thyrsis.model_no_inverse(param=self.data, const=True, test=True)

        layers = []
        for column, units in SpatialiteMeshConstDataProvider.sourceUnits.items():
            layer = get_layer(column, MeshLayer)
            if layer:
                QgsProject.instance().removeMapLayer(layer)

            layers.append(
                MeshLayer(
                    "dbname="
                    + self.mesh_db
                    + " crs=epsg:{} column=".format(self.srid)
                    + column,
                    column,
                    SpatialiteMeshConstDataProvider.PROVIDER_KEY,
                )
            )
            logger.notice("adds constant layer ", column)
            layers[-1].colorLegend().setTitle(column)
            layers[-1].colorLegend().setUnits(units)
            layers[-1].colorLegend().setMaxValue(layers[-1].dataProvider().maxValue())
            if column == "v_norme":
                layers[-1].colorLegend().setLogScale(True)
                layers[-1].colorLegend().setMinValue(
                    float(self.settings.value("Variables", "darcyScale"))
                    * layers[-1].colorLegend().maxValue()
                )
            else:
                layers[-1].colorLegend().setMinValue(
                    layers[-1].dataProvider().minValue()
                )
            if not layers[-1].isValid():
                raise ModelCreationError(f"Invalid layer for {column} column")
            QgsProject.instance().addMapLayer(layers[-1])
            layers_node = (
                QgsProject.instance().layerTreeRoot().findLayer(layers[-1].id())
            )
            layers_node.setExpanded(False)
            layers_node.setItemVisibilityChecked(True)
            if column == "permeabilite_x":
                layers[-1].colorLegend().setLogScale(True)
                layers_node.setItemVisibilityChecked(True)
                layers_node.setExpanded(True)
            layers[-1].dataProvider().update_values()
            min_ = layers[-1].dataProvider().minValue()
            max_ = layers[-1].dataProvider().maxValue()
            fmt = format_(min_, max_)
            layers[-1].colorLegend().setMinValue(fmt % min_)
            layers[-1].colorLegend().setMaxValue(fmt % max_)
            layers[-1].triggerRepaint()

        QgsProject.instance().writeEntry("Paths", "/Absolute", True)
        QgsProject.instance().write()

    def buildModel(self):
        logger.enable_console(True)
        logger.set_level("notice")
        self.createMeshProject()
        self.createDomain()
        self.createMesh()
        self.createModel()


if __name__ == "__main__":

    logger.enable_console(True)
    logger.set_level("notice")
    logger.notice("building model")

    try:
        optlist, args = getopt.getopt(sys.argv[1:], "o:h", ["help"])
    except Exception as e:
        sys.stderr.write(str(e) + "\n")
        exit(1)
    optlist = dict(optlist)

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    if len(sys.argv) < 2:
        print("wrong number of arguments (try database.py --help)")
        exit(1)

    if os.path.isdir(sys.argv[-1]):
        folderpath = sys.argv[-1]
    else:
        print("The folder doesn't exist")
        exit(1)

    project_param = os.path.join(
        os.path.abspath(folderpath), "project_parameters.txt"
    ).replace("\n", "")
    if not os.path.isfile(project_param):
        print("no project parameters file found in the folder")
        exit(1)

    hynverse_param = os.path.abspath(
        os.path.join(folderpath, "hynverse_parameters.txt")
    )
    if not os.path.exists(hynverse_param):
        print("no hynverse parameters file found in the folder")
        exit(1)

    simulation_param = os.path.abspath(
        os.path.join(folderpath, "simulation_parameters.txt")
    )
    if not os.path.exists(simulation_param):
        simulation_param = None

    contours = os.path.abspath(os.path.join(folderpath, "contours.gpkg"))
    if not os.path.exists(contours):
        print("No contours.gpkg file found in the folder")
        exit(1)

    mnt = os.path.abspath(os.path.join(folderpath, "mnt.tif"))
    if not os.path.exists(mnt):
        print("No mnt.tif' file found in the folder")
        exit(1)

    mur = os.path.abspath(os.path.join(folderpath, "mur.tif"))
    if not os.path.exists(mur):
        print("No mur.tif' file found in the folder")
        exit(1)

    potentiel = os.path.abspath(os.path.join(folderpath, "potentiel.tif"))
    if not os.path.exists(potentiel):
        potentiel = None

    points_fixes = os.path.abspath(os.path.join(folderpath, "points_fixes.csv"))
    if not os.path.exists(points_fixes):
        points_fixes = None

    points_pilote = os.path.abspath(os.path.join(folderpath, "points_pilote.csv"))
    if not os.path.exists(points_pilote):
        points_pilote = None

    permeability_raster = os.path.abspath(os.path.join(folderpath, "permeabilite.tif"))
    if not os.path.exists(permeability_raster):
        permeability_raster = None

    const_perm = os.path.abspath(os.path.join(folderpath, "const_perm.txt"))
    if not os.path.exists(const_perm):
        const_perm = None
    else:
        with open(const_perm, "r") as file:
            const_perm = float(file.readline())

    if "-o" in optlist:
        dot_thyrsis_path = str(optlist["-o"])
    else:
        dot_thyrsis_path = folderpath

    if const_perm is None and permeability_raster is None and points_pilote is None:
        print(
            "No information to build model (points_pilote.csv / permeabilite.tif / const_perm.txt)"
        )
        exit(1)
    elif bool(const_perm) + bool(permeability_raster) + bool(points_pilote) != 1:
        print(
            "More than one information to build model (points_pilote.csv / permeabilite.tif / const_perm.txt). Can't decide how build the model"
        )
        exit(1)
    else:
        if os.path.exists(os.path.join(folderpath, ".local")):
            shutil.rmtree(os.path.join(folderpath, ".local"))
        if os.path.exists(os.path.join(folderpath, ".thyrsis")):
            shutil.rmtree(os.path.join(folderpath, ".thyrsis"))

        # Copy default QGIS THYRSIS install settings file if no provided
        output_path = Path(folderpath) / ".thyrsis"
        input_folder_settings = os.path.join(output_path / "thyrsis.ini")
        if not os.path.exists(input_folder_settings):
            if sys.platform.startswith("linux"):
                default_path = os.environ["HOME"]
            else:
                default_path = os.environ["USERPROFILE"]
            default_settings_file = os.path.join(
                default_path, Path(".thyrsis") / "thyrsis.ini"
            )
            if os.path.exists(default_settings_file):
                os.makedirs(output_path, exist_ok=True)
                shutil.copyfile(default_settings_file, input_folder_settings)

        # Update current HOME or USERPROFILE to change current THYRSIS install folder
        # where THYRSIS will look for settings
        os.environ["HOME"] = folderpath
        os.environ["USERPROFILE"] = folderpath

        qgs = QgsApplication([], True)

        # Load providers
        qgs.initQgis()

        model = Model(
            dot_thyrsis_path,
            project_param,
            hynverse_param,
            simulation_param,
            contours,
            mnt,
            mur,
            potentiel,
            points_fixes,
            points_pilote,
            permeability_raster,
            const_perm,
        )
        model.buildModel()
