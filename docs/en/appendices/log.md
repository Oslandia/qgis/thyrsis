(stratigraphic_logs)=
# Stratigraphic logs

This appendix presents the codifications used for the graphic representation of the stratigraphic logs :


- geological time scale codification from the BRGM ({numref}`brgm`),
- RGB colorimetric codification from the Commission for the Geological Map of the World (CGMW) ({numref}`rgb`),
- the lithology pattern chart from USGS ({numref}`usgs1`, {numref}`usgs2`, {numref}`usgs3`).

:::{figure} /_static/images/BRGM_temps_geologiques.png
:name: brgm
Geological time scale codification from the BRGM.
:::

:::{figure} /_static/images/RGB.png
:name: rgb
RGB colorimetric codification from the CGMW.
:::

:::{figure} /_static/images/USGS1.png
:name: usgs1
Lithology pattern codification from USGS (1).
:::

:::{figure} /_static/images/USGS2.png
:name: usgs2
Lithology pattern codification from USGS (2).
:::

:::{figure} /_static/images/USGS3.png
:name: usgs3
Lithology pattern codification from USGS (3).
:::
