# MS-MPI sous Windows

Si vous souhaitez utiliser plusieurs processeurs sur une machine locale ou distante sous Windows, il est nécessaire de vérifier que le service est lancé sur la machine.

Voici un lien vers la documentation de MS-MPI : https://github.com/microsoft/Microsoft-MPI/blob/master/docs/RunningMSMPI.md

Vous pouvez lancer le service sur la machine avec la commande suivante:

```
smpd -p 8677
```

L'executable `smpd` devrait être dans votre `PATH`. Si ce n'est pas le cas il est disponible dans le répertoire défini par la variable `MSMPI_BIN`.