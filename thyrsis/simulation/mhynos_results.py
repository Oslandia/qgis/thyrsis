"""This module converts results from mhynos calculations into .npy files

    python -m thyrsis.simulation.mhynos_results [-v] db.sqlite

"""
import os
import sys
import time
from builtins import range, str

import numpy

from thyrsis.exception import SimulationError

from ..database import sqlite
from ..log import logger
from .mhynos_database import get_injections_pids


def mhynos_results(code, dates, settings, MPI=None):
    """Read ouput file and update database

    :param code: Simulation code
    :type code: Metis or OpenFoam
    :param dates: list of dates
    :type dates: list
    :param settings: Thyrsis settings
    :type settings: Settings
    :param MPI: MPI parameter
    :type MPI: MPI
    """
    if MPI is not None and MPI.COMM_WORLD.Get_rank() != 0:
        # because of the computation of e.g. max among insat results
        # it is difficult to make this code //
        # without first a major refactoring
        return

    total_time_in_insat_conversion = 0
    total_time_in_sat_conversion = 0

    logger.debug("\nmhynos_results")
    logger.notice("\nRESULTATS")

    dbname = code.database
    dir_name = os.path.dirname(os.path.abspath(dbname))
    basename = os.path.basename(dbname)[:-7]
    tmpdir = os.path.join(dir_name, basename + "_tmp")

    conn = sqlite.connect(dbname)
    cur = conn.cursor()
    nb_simulations = cur.execute(
        "SELECT COUNT(1) FROM parametres_simulation"
    ).fetchone()[0]
    if nb_simulations == 0:
        logger.notice("\nAucun résultat !")
        return

    pids = get_injections_pids(conn)
    cur.execute("SELECT type_injection, insature FROM simulations")
    type_injection, insature = cur.fetchone()

    filenames = [
        "concentration.sat",
        "potentiel.sat",
        "darcy.sat",
        "debit.sat",
        "saturation.insat",
        "saturation_perm.insat",
        "darcy.insat",
        "darcy_perm.insat",
        "concentration.insat",
        "activity.insat",
    ]

    if code.has_second_milieu:
        filenames += [
            "concentration2.sat",
            "potentiel2.sat",
            "darcy2.sat",
            "debit2.sat",
        ]

    save_flag = {}
    for f in filenames:
        option = (
            f.split(".")[0].replace("_perm", "").replace("2", "")
            + f.split(".")[1].replace("insat", "ZNS").replace("sat", "ZS")
            + "Check"
        )
        save_flag[f] = bool(int(settings.value("Variables", option)))
    # on ne crée par les fichiers insat.npy s'il y a trop de colonnes
    for f in list(save_flag.keys()):
        if (
            len(pids) > int(settings.value("General", "nbColumnsMax"))
            and f[-5:] == "insat"
        ):
            save_flag[f] = False

    (nb_elem,) = cur.execute(
        """SELECT COUNT(1) FROM elements_chimiques
        WHERE id = (SELECT eid FROM simulations LIMIT 1)"""
    ).fetchone()
    if nb_elem > 1:
        raise SimulationError(
            f"Only one chemical element supported for mhynos result. {nb_elem} found in simulation"
        )

    # décroissance radioactive pour le bilan de masse
    decrad = (
        cur.execute(
            """
        SELECT decroissance_radioactive FROM elements_chimiques
        WHERE id = (SELECT eid FROM simulations LIMIT 1)"""
        ).fetchone()[0]
        if nb_elem
        else 0.0
    )
    # activité spécifique pour l'activité massique en zns
    specificActivity = (
        cur.execute(
            """
        SELECT activite_specifique FROM elements_chimiques
        WHERE id = (SELECT eid FROM simulations LIMIT 1)"""
        ).fetchone()[0]
        if nb_elem
        else 0.0
    )

    nbDates = len(dates)
    (nbNodes_sat,) = cur.execute("SELECT COUNT(1) FROM noeuds").fetchone()
    (nbElem_sat,) = cur.execute("SELECT COUNT(1) FROM mailles").fetchone()
    (nbNodes_contour,) = cur.execute("SELECT COUNT(1) FROM noeuds_contour").fetchone()

    masse_mean_sat = numpy.zeros(nbDates, dtype=numpy.float32)
    masse_sigma_sat = numpy.zeros(nbDates, dtype=numpy.float32)
    masse_mean_insat = numpy.zeros(nbDates, dtype=numpy.float32)
    masse_sigma_insat = numpy.zeros(nbDates, dtype=numpy.float32)
    masse_mean_out = numpy.zeros(nbDates, dtype=numpy.float32)
    masse_sigma_out = numpy.zeros(nbDates, dtype=numpy.float32)

    saturation_flag = False
    darcy_flag = False
    sat_flag = dict()
    mean_sat = dict()
    sigma_sat = dict()
    variables_sat = code.variables_sat
    if variables_sat["concentration"] == "nodes":
        surf_ep_sat = numpy.array(
            cur.execute(
                "SELECT surface*epaisseur_zs FROM noeuds ORDER BY OGC_FID"
            ).fetchall()
        ).flatten()
    else:
        surf_ep_sat = numpy.array(
            cur.execute(
                "SELECT surface*epaisseur_zs FROM mailles ORDER BY OGC_FID"
            ).fetchall()
        ).flatten()

    if code.has_second_milieu:
        variables_sat.update(code.variables_sat2)

    for name, node_flag in variables_sat.items():
        sat_flag[name] = True
        if node_flag == "nodes":
            nbVal_sat = nbNodes_sat
        elif node_flag == "mailles":
            nbVal_sat = nbElem_sat
        else:
            nbVal_sat = 1
        mean_sat[name] = numpy.zeros((nbDates, nbVal_sat), dtype=numpy.float32)
        sigma_sat[name] = numpy.zeros((nbDates, nbVal_sat), dtype=numpy.float32)

    if insature == "oui":
        nbNodes_insat = []
        nbElem_insat = []
        surfaces_insat = []
        cumul_surface_insat = []
        nbNodes_insat_total = 0
        sample_dir = os.path.join(tmpdir, "sample%05d" % (1))
        for i, pid in enumerate(pids):
            insat_dir = os.path.join(sample_dir, "insat%05d" % (i + 1))
            nnodes, surfaces = code.read_mesh_insat(insat_dir)
            surfaces_insat += surfaces
            nelem = nnodes - 1
            if not nnodes or not nelem:
                raise SimulationError(
                    "At least one nodes or one element should be defined in mhynos mesh insat."
                )
            nbNodes_insat_total += nnodes
            nbNodes_insat.append(nnodes)
            nbElem_insat.append(nelem)
            cur.execute(
                """
                    SELECT SUM(n.surface)
                    FROM noeuds_injections AS ni, noeuds AS n
                    WHERE ni.injection = """
                + str(pid)
                + """
                    AND ni.noeud = n.OGC_FID
                    ORDER BY n.OGC_FID"""
            )
            cumul_surface_insat += [cur.fetchone()[0]] * nnodes
        cumul_surface_insat = numpy.array(cumul_surface_insat)

        # en insaturé, on rapporte toutes les variables aux noeuds
        nbVal_insat_total = nbNodes_insat_total

        insat_flag = dict()
        mean_insat = dict()
        sigma_insat = dict()
        variables_insat = code.variables_insat
        for name, node_flag in variables_insat.items():
            if save_flag[name + ".insat"]:
                insat_flag[name] = True
                mean_insat[name] = numpy.zeros(
                    (nbDates, nbVal_insat_total), dtype=numpy.float32
                )
                sigma_insat[name] = numpy.zeros(
                    (nbDates, nbVal_insat_total), dtype=numpy.float32
                )
            else:
                insat_flag[name] = False
        # l'activité sera déduite de la concentration et de la saturation
        insat_flag["activity"] = False

    param_names = ["WC", "WT", "VM", "DK"]
    tup = cur.execute(
        "SELECT wc, wt, vm, dk FROM parametres_simulation ORDER BY id"
    ).fetchall()
    nsamples = len(tup)
    for sample_idx, param_values in enumerate(tup):
        if len(param_names) != len(param_values):
            raise SimulationError(
                f"Error in parameters_simulation in database. {len(param_names)} parameters expected. {len(param_values)} found."
            )
        param = {param_names[i]: param_values[i] for i in range(len(param_names))}
        porosite_retard = param["WC"] + param["DK"] * param["VM"] * (1.0 - param["WT"])
        coef_activity = (
            specificActivity
            * (param["DK"] + param["WC"] / param["VM"] / (1.0 - param["WT"]))
            if param["VM"] > 0 and param["WT"] < 1
            else 0.0
        )

        sample_dir = os.path.join(tmpdir, "sample%05d" % (sample_idx + 1))
        if not os.path.isdir(sample_dir):
            raise RuntimeError("répertoire " + sample_dir + " inexistant")

        # saturé
        sat_dir = os.path.join(sample_dir, "sature")
        if not os.path.isdir(sat_dir):
            raise RuntimeError("répertoire " + sat_dir + " inexistant")

        for name, node_flag in variables_sat.items():
            start_sat = time.time()

            sat_file = os.path.join(sat_dir, name + ".sat")
            sat_flag[name] &= code.file_exists(sat_file)
            # si le calcul saturé n'a pas eu lieu, on force la concentration à 0
            if name[:13] == "concentration" and type_injection != "aucune":
                sat_flag[name] = True
            if sat_flag[name]:

                logger.debug("reading {}".format(sat_file))
                results = code.read_file(sat_file, dates)

                logger.debug("{} shape is {}".format(name, mean_sat[name].shape))
                mean_sat[name] += results
                if nb_simulations > 1:
                    sigma_sat[name] += results * results

                if name[:13] == "concentration":
                    logger.debug("computing saturated mass")
                    logger.debug(
                        "porosite_retard is {}, results shape is {}, surf_ep_sat shape is {}".format(
                            porosite_retard, results.shape, surf_ep_sat.shape
                        )
                    )
                    masse_sat = numpy.sum(
                        results * surf_ep_sat * porosite_retard, axis=1
                    )
                    masse_mean_sat += masse_sat
                    if nb_simulations > 1:
                        masse_sigma_sat += masse_sat * masse_sat

                if name[:5] == "debit":
                    logger.debug("computing output mass")
                    debit_sat = results
                    # debit_sat = numpy.maximum(results, numpy.zeros_like(results))
                    debc = numpy.zeros(nbDates, dtype=numpy.float32)
                    debc[0] = 0.5 * debit_sat[0] * dates[0]
                    for i in range(1, nbDates):
                        debc[i] = debc[i - 1] * numpy.exp(
                            -decrad * (dates[i] - dates[i - 1])
                        ) + 0.5 * (debit_sat[i] + debit_sat[i - 1]) * (
                            dates[i] - dates[i - 1]
                        )
                    masse_mean_out += debc
                    if nb_simulations > 1:
                        masse_sigma_out += debc * debc

            total_time_in_sat_conversion += time.time() - start_sat

        # insaturé
        if insature == "oui":
            # il faut que le fichier saturation soit traité avant saturation_perm et avant concentration
            # idem pour darcy et darcy_perm
            for name in (
                "saturation",
                "saturation_perm",
                "darcy",
                "darcy_perm",
                "concentration",
            ):

                if not insat_flag[name]:
                    continue

                node_flag = variables_insat[name]
                results = numpy.zeros((nbDates, nbVal_insat_total), dtype=numpy.float32)

                n1 = 0
                n2 = 0
                for i, pid in enumerate(pids):
                    start_at = time.time()
                    insat_dir = os.path.join(sample_dir, "insat%05d" % (i + 1))
                    if not os.path.isdir(insat_dir):
                        raise RuntimeError("répertoire " + insat_dir + " inexistant")

                    insat_file = os.path.join(insat_dir, name + ".insat")
                    insat_flag[name] &= code.file_exists(insat_file)
                    if insat_flag[name]:
                        logger.notice("converting ", insat_file)
                        nbVal_insat = nbNodes_insat[i] if node_flag else nbElem_insat[i]
                        n2 += nbNodes_insat[i]
                        results[:, n1:n2] = code.read_file(
                            insat_file, dates, not node_flag
                        )
                        n1 += nbNodes_insat[i]

                    total_time_in_insat_conversion += time.time() - start_at

                # si on trouve le fichier saturation.insat,
                # on ne traite pas le fichier saturation_perm.insat
                if name == "saturation" and insat_flag[name]:
                    insat_flag["saturation_perm"] = False
                # si on trouve le fichier darcy.insat,
                # on ne traite pas le fichier darcy_perm.insat
                if name == "darcy" and insat_flag[name]:
                    insat_flag["darcy_perm"] = False

                # avec OPENFOAM, il faut convertir la teneur en eau en saturation
                if code.codename == "openfoam" and param["WC"] and "saturation" in name:
                    results = results / param["WC"]

                if insat_flag[name]:
                    logger.debug("name = ", name)

                    mean_insat[name] += results
                    if nb_simulations > 1:
                        sigma_insat[name] += results * results

                    if name == "saturation":
                        saturation = results
                        saturation_flag = True
                    elif name == "saturation_perm" and not saturation_flag:
                        saturation = results
                        saturation_flag = True
                    elif name == "darcy":
                        darcy = results
                        darcy_flag = True
                    elif name == "darcy_perm" and not darcy_flag:
                        darcy = results
                        darcy_flag = True
                    elif name == "concentration" and saturation_flag:
                        surf_ep_insat = numpy.array(surfaces_insat)
                        masse_insat = numpy.sum(
                            saturation * results * surf_ep_insat * porosite_retard,
                            axis=1,
                        )
                        masse_mean_insat += masse_insat
                        if nb_simulations > 1:
                            masse_sigma_insat += masse_insat * masse_insat

            if insat_flag["concentration"] and saturation_flag and specificActivity > 0:
                saturation = (
                    mean_insat["saturation"]
                    if insat_flag["saturation"]
                    else mean_insat["saturation_perm"]
                )
                insat_flag["activity"] = True
                mean_insat["activity"] = (
                    saturation * mean_insat["concentration"] * coef_activity
                )
                sigma_insat["activity"] = (
                    saturation * sigma_insat["concentration"] * coef_activity
                )

    logger.debug("nsamples = ", nsamples)
    for name, node_flag in variables_sat.items():
        if sat_flag[name]:
            if node_flag == "nodes":
                ext = ".node"
            elif node_flag == "mailles":
                ext = ".elem"
            else:
                ext = "." + node_flag
            if name == "darcy_perm" or name == "darcy":
                save_name = "vitesse_darcy_norme" + ext
            elif name == "darcy_perm2" or name == "darcy2":
                save_name = "vitesse_darcy_norme2" + ext
            else:
                save_name = name + ext
            if nsamples > 1:
                logger.debug("nsample > 1 : name = ", name)
                mean_sat[name] = mean_sat[name] / nsamples
                sigma_sat[name] = numpy.sqrt(
                    numpy.maximum(
                        0.0,
                        sigma_sat[name] / nsamples - mean_sat[name] * mean_sat[name],
                    )
                )
                if save_flag[name + ".sat"]:
                    numpy.save(
                        os.path.join(dir_name, basename + ".s" + save_name),
                        sigma_sat[name],
                    )
            if save_flag[name + ".sat"]:
                numpy.save(
                    os.path.join(dir_name, basename + "." + save_name), mean_sat[name]
                )

    if sat_flag["concentration"] and nsamples > 1:
        masse_mean_sat = masse_mean_sat / nsamples
        masse_sigma_sat = numpy.sqrt(
            numpy.maximum(
                0.0, masse_sigma_sat / nsamples - masse_mean_sat * masse_mean_sat
            )
        )

    if sat_flag["debit"] and nsamples > 1:
        masse_mean_out = masse_mean_out / nsamples
        masse_sigma_out = numpy.sqrt(
            numpy.maximum(
                0.0, masse_sigma_out / nsamples - masse_mean_out * masse_mean_out
            )
        )

    if insature == "oui":
        for name, node_flag in variables_insat.items():
            if insat_flag[name]:
                if name == "saturation_perm":
                    save_name = "saturation"
                elif name == "darcy_perm" or name == "darcy":
                    save_name = "vitesse_darcy_norme"
                else:
                    save_name = name
                if nsamples > 1:
                    mean_insat[name] = mean_insat[name] / nsamples
                    sigma_insat[name] = numpy.sqrt(
                        numpy.maximum(
                            0.0,
                            sigma_insat[name] / nsamples
                            - mean_insat[name] * mean_insat[name],
                        )
                    )
                    if save_flag[name + ".insat"]:
                        numpy.save(
                            os.path.join(
                                dir_name, basename + ".s" + save_name + ".insat"
                            ),
                            sigma_insat[name],
                        )
                if save_flag[name + ".insat"]:
                    numpy.save(
                        os.path.join(dir_name, basename + "." + save_name + ".insat"),
                        mean_insat[name],
                    )

        if insat_flag["concentration"] and nsamples > 1:
            masse_mean_insat = masse_mean_insat / nsamples
            masse_sigma_insat = numpy.sqrt(
                numpy.maximum(
                    0.0,
                    masse_sigma_insat / nsamples - masse_mean_insat * masse_mean_insat,
                )
            )

        logger.debug(
            "time spent in insat conversion {}sec".format(
                total_time_in_insat_conversion
            )
        )

    if type_injection != "aucune":
        with open(os.path.join(dir_name, basename + ".bilan_masse.csv"), "w") as fil:
            fil.write("%s %s %s %s %s\n" % ("date", "sature", "zns", "out", "total"))
            for i, date in enumerate(dates):
                cur.execute(
                    """
                    INSERT INTO bilan_masse(did, sat, zns, out)
                    VALUES(%d, %e, %e, %e)"""
                    % (i + 1, masse_mean_sat[i], masse_mean_insat[i], masse_mean_out[i])
                )

                fil.write(
                    "%.8e %.8e %.8e %.8e %.8e\n"
                    % (
                        date,
                        masse_mean_sat[i],
                        masse_mean_insat[i],
                        masse_mean_out[i],
                        masse_mean_sat[i] + masse_mean_insat[i] + masse_mean_out[i],
                    )
                )

    conn.commit()
    conn.close()

    logger.debug(
        "time spent in sat conversion {}sec".format(total_time_in_sat_conversion)
    )


# run as script if invoqued as such
if __name__ == "__main__":
    import sys

    from ..settings import Settings
    from .metis import Metis
    from .mhynos_compute import load_dates_simulation
    from .openfoam import OpenFoam

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    logger.enable_console(True)

    if len(sys.argv) not in [2, 3]:
        logger.error("wrong number of arguments (try compute.py --help)")
        exit(1)

    verbose = False
    dbname = None
    for arg in sys.argv[1:]:
        if arg == "-v":
            verbose = True
        elif arg[-7:] == ".sqlite":
            dbname = arg

    if not dbname:
        logger.error(
            "cannot find a '.sqlite' file in arguments (try mesh_to_spatialite.py --help)"
        )
        exit(1)

    dir_name = os.path.dirname(os.path.abspath(dbname))
    basename = os.path.basename(dbname)[:-7]
    tmpdir = os.path.join(dir_name, basename + "_tmp")

    conn = sqlite.connect(dbname)
    dates_seconds = load_dates_simulation(
        conn, os.path.join(tmpdir, "dates_simulation.txt")
    )
    conn.close()

    settings = Settings(os.path.dirname(os.path.abspath(dbname)))
    debug = int(settings.value("General", "debug")) or verbose
    logger.set_level("debug" if debug else "notice")

    codeCommand = settings.value("General", "codeHydroCommand")
    codeHydro = settings.value("General", "codeHydro")
    if codeHydro == "metis" and os.path.exists(codeCommand):
        code = Metis(dbname, codeCommand)
    elif codeHydro == "openfoam":
        code = OpenFoam(dbname, settings)
    else:
        raise NotImplementedError(
            "hydrogeologic computation software not yet implemented"
        )

    logger.debug("code hydro = ", codeHydro)
    mhynos_results(code, dates_seconds, settings)
