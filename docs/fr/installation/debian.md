# Debian et distributions apparentées

:::{note}
Le plugin a été développé et testé avec Ubuntu 20.04 et QGIS 3.22.
:::

## Exigences

- {{ qgis_version_min }} =< QGIS =< {{ qgis_version_max }}

### Outils de simulation

#### OpenFOAM et porousMultiphaseFoam

:::{note}
Le plugin a été testé avec OpenFOAM v2406 en version `dev` (voir la [documentation officiel à propos des sub-packages](https://develop.openfoam.com/Development/openfoam/-/wikis/precompiled#sub-packages)).
:::

Afin de faciliter l'installation ou pour une utilisation hors ligne, les instructions principales pour l'installation sont recopiées ici mais il est préférable de regarder [la documentation officielle](https://develop.openfoam.com/Development/openfoam/-/wikis/precompiled/debian) pour des éventuelles mises à jour des étapes:

```bash
curl -s https://dl.openfoam.com/add-debian-repo.sh | sudo bash
sudo apt install -y openfoam2406-dev
```

Le binaire `cartesian2DMesh` n'est plus disponible dans l'installation d'OpenFoam dans la version 2406. Il y a eu un [problème](https://develop.openfoam.com/Development/openfoam/-/issues/3198) lors de la génération du paquet.

Pour avoir un support du maillage avec `cfMesh`, il est nécessaire d'installer le paquet `integration-cfmesh` du dépôt d'OpenFoam:

```bash
git clone https://develop.openfoam.com/Community/integration-cfmesh
cd integration-cfmesh
. /usr/lib/openfoam/openfoam2406/etc/bashrc
./Allwmake
./Allwclean
```

Ensuite, installez le paquet `porousMultiphaseFoam` :

```bash
git clone --depth=1 --branch=v2406.2 --single-branch https://github.com/phorgue/porousMultiphaseFoam.git
cd porousMultiphaseFoam/
. /usr/lib/openfoam/openfoam2406/etc/bashrc
./Allwmake
./Allwclean
```

#### Metis

> TO DOC

### Autres

:::{note}
Les librairies externes et les paquets python a installé sont listés dans le fichier [requirements/apt.txt](https://gitlab.com/Oslandia/qgis/thyrsis/-/blob/master/requirements/apt.txt). Merci de référer à ce fichier pour obtenir la liste mise à jour des librairies à installer.
:::

- Gmsh (version 4.*): à récupérer depuis [le site officiel](https://gmsh.info/).
- [FFMPEG](https://ffmpeg.org/) (pour les exports vidéo)
- [GNU Plot](http://www.gnuplot.info/)
- [MPICH](https://www.mpich.org/)

Typiquement:

```bash
sudo apt install gmsh=4* ffmpeg gnuplot mpich
```

### Paquets Python

Depuis le dépôt Debian:

```bash
sudo apt install python3-mpi4py python3-opengl python3-rtree
```

Depuis Python Package Index (PyPi):

```bash
python3 -m pip install --upgrade mpi4py==3.1.3 rtree==0.9.7
```
