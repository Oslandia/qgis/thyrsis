"""
stadalone 3d viewer

"""

import sys

from PyQt5.QtCore import *
from PyQt5.QtGui import *

if __name__ == "__main__":

    from thyrsis.gui.zns_scene import ZnsDataProvider
    from thyrsis.meshlayer.glmesh import ColorLegend
    from thyrsis.spatialitemeshdataprovider import SpatialiteMeshDataProvider
    from thyrsis.viewer_3d import *

    app = QApplication(sys.argv)

    QCoreApplication.setOrganizationName("QGIS")
    QCoreApplication.setApplicationName("QGIS2")

    assert len(sys.argv) >= 2

    column = sys.argv[2] if len(sys.argv) > 2 else None
    uri = (
        "dbname="
        + sys.argv[1]
        + " crs=epsg:27572"
        + (" resultColumn=" + column if column else "")
    )
    dataProvider = SpatialiteMeshDataProvider(uri)
    znsDataProvider = ZnsDataProvider(uri)
    legend = ColorLegend()
    znsLegend = ColorLegend()
    win = ViewerWindow(dataProvider, legend, znsDataProvider, znsLegend)
    win.show()

    if len(sys.argv) > 3:
        extent = dataProvider.extent()
        win.viewer.scene.setTerrainTexture(
            QImage(sys.argv[3]),
            [
                extent.xMinimum(),
                extent.yMinimum(),
                extent.xMaximum(),
                extent.yMaximum(),
            ],
        )

    def setDate(didx):
        znsDataProvider.setDate(didx)
        dataProvider.setDate(didx)

        legend.setLogScale(True)
        legend.setMaxValue(dataProvider.maxValue(didx))
        legend.setMinValue(legend.maxValue() * 1e-3)
        znsLegend.setLogScale(True)
        znsLegend.setMaxValue(znsDataProvider.maxValue(didx))
        znsLegend.setMinValue(znsLegend.maxValue() * 1e-3)

    dateSlider = QSlider(Qt.Horizontal, win)
    dateSlider.setMaximum(len(dataProvider.dates()) - 1)
    dateSlider.valueChanged.connect(setDate)
    dateSlider.setValue(10)
    dateSlider.show()
    win.controls.widget().scrollLayout.addWidget(dateSlider)

    saveBtn = QPushButton("save image")
    label = QLabel()

    def savePic(dummy=None):
        img = win.viewer.image(QSize(512, 512))
        label.setPixmap(QPixmap.fromImage(img))
        label.show()

    saveBtn.clicked.connect(savePic)

    win.controls.widget().scrollLayout.addWidget(saveBtn)

    sys.exit(app.exec_())
