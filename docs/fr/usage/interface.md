---
substitutions:
  img_backward: |-
    :::{image} /_static/images/media-seek-backward.png
    :scale: 20 %
    :::
  img_draw: |-
    :::{image} /_static/images/draw.png
    :scale: 10 %
    :::
  img_first: |-
    :::{image} /_static/images/media-skip-backward.png
    :scale: 20 %
    :::
  img_forage: |-
    :::{image} /_static/images/forage.png
    :scale: 10 %
    :::
  img_forward: |-
    :::{image} /_static/images/media-seek-forward.png
    :scale: 20 %
    :::
  img_inverse_plot: |-
    :::{image} /_static/images/inverse_plot.png
    :scale: 10 %
    :::
  img_inversion: |-
    :::{image} /_static/images/inverse.png
    :scale: 10 %
    :::
  img_last: |-
    :::{image} /_static/images/media-skip-forward.png
    :scale: 20 %
    :::
  img_list-add: |-
    :::{image} /_static/images/list-add.png
    :scale: 50 %
    :::
  img_list-remove: |-
    :::{image} /_static/images/list-remove.png
    :scale: 50 %
    :::
  img_maillage: |-
    :::{image} /_static/images/mesh.png
    :scale: 10 %
    :::
  img_pause: |-
    :::{image} /_static/images/media-playback-pause.png
    :scale: 20 %
    :::
  img_pick: |-
    :::{image} /_static/images/pick.png
    :scale: 10 %
    :::
  img_play: |-
    :::{image} /_static/images/media-playback-start.png
    :scale: 20 %
    :::
  img_plot: |-
    :::{image} /_static/images/plot.png
    :scale: 10 %
    :::
  img_plot_bilan: |-
    :::{image} /_static/images/plot_bilan.png
    :scale: 10 %
    :::
  img_run_simulation: |-
    :::{image} /_static/images/config_simulation.png
    :scale: 10 %
    :::
  img_view_refresh: |-
    :::{image} /_static/images/view-refresh.png
    :scale: 50 %
    :::
---

# Interface

(preferences_fr)=
## Préférences

Le menu Préférences permet d'accéder aux choix généraux de configuration sauvegardés :

- dans le fichier `.thyrsis.ini` du répertoire de projet - celui de la base de données de calcul,
- dans le fichier `thyrsis.ini` du répertoire `$HOME/.thyrsis`.

À la première utilisation du plugin, un dossier `$HOME/.thyrsis` est créé et un fichier de configuration `.thyrsis.ini` est créé avec valeurs par défaut. La fenêtre de Préférences est ouverte et doit être renseignée (cf. infra).

Lorsqu'un projet est ouvert à nouveau, ce sont les données du fichier `.thyrsis.ini` du répertoire de la base de données qui sont prises en compte. Cela permet de définir des préférences pour chaque projet.

### Installation

:::{figure} /_static/images/Preferences_installation.png
:name: Preferences_installation
Menu Préférences, fenêtre Installation
:::

Les préférences d'installation permettent de définir les emplacements des principaux fichiers et répertoires nécessaires au fonctionnement du plugin, à savoir :

- Le chemin vers l'exécutable GMSH

- Répertoire projet OpenFoam et répertoire d'installation PMF

> Ses variables sont définies avec des valeurs par défaut. Merci de vérifier les valeurs avant d'utiliser THYRSIS.

- le répertoire contenant les fichiers .vrt des fonds de carte d'intérêt général,

- le fichier de configuration MPI, indiquant les ordinateurs à utiliser et le nombre de processeurs pour chacun d'eux, sous la forme :

```
tata:8
titi:8
toto:4
```

De plus, il est aussi possible de préciser :

- le nombre de processeurs à utiliser pour la parallélisation des calculs,
- le nombre maximum de colonnes 1D verticales à afficher,
- le générateur de maillage à utiliser (GMSH ou CFMESH)
:::{important}
Pour l'utilisation de CFMESH sous Windows, la version minimale pour OpenFoam est v2112. Il y a un problème pour l'utilisation de cfMesh avec les versions précédentes.
:::
- l'outil de simulation hydrogéologique utilisé (OPENFOAM ou METIS)

Enfin, deux cases à cocher permettent de préciser :

- si les informations d'exécution sont à afficher dans la console,
- si une impression détaillée est souhaitée (mode verbeux).

### Variables

:::{figure} /_static/images/Preferences_variables.png
:name: Preferences_variables
Menu Préférences, fenêtre Variables
:::

Les préférences de variables permettent de spécifier les grandeurs à évaluer et stocker, parmi :

- potentiel,
- vitesse de Darcy,
- concentration,
- activité,
- saturation,

et les paramètres d'affichage, en définissant :

- l'unité,
- l'échelle,
- le nombre de classes.

L'échelle définit le rapport de la valeur minimale à afficher, à la valeur maximale de la grandeur considérée, telle qu'obtenue par la simulation numérique. Cette échelle est de 0.001 par défaut, mais une valeur inférieure permet de mieux visualiser l'extension d'un panache.

Le nombre de classes définit le nombre de codes de couleur à utiliser pour l'échantillonnage de la grandeur considérée.

Cette fenêtre de préférences permet également de définir l'unité de masse pour l'affichage du bilan de masse et de cocher une case évitant l'affichage des unités, à des fins de communication.

### Animation

:::{figure} /_static/images/Preferences_animation.png
:name: Preferences_animation
:scale: 50 %

Menu Préférences, fenêtre Animation
:::

Cette fenêtre permet seulement de définir le nombre d'images par secondes, utilisé dans la construction d'une vidéo.

### Matplotlib

:::{figure} /_static/images/Preferences_matplotlib.png
:name: Preferences_matplotlib
:scale: 50 %

Menu Préférences, fenêtre Matplotlib
:::

Cette fenêtre de préférences permet de configurer certains paramètres d'affichage de matplotlib :

- taille des étiquettes de graduation,
- taille de la police du titre des ordonnées,
- taille de la police de la légende.

### Base de données

:::{figure} /_static/images/Preference_database.png
:name: Preferences_database
:scale: 50 %

Menu Préférences, fenêtre base de données
:::

Cette fenêtre de préférences permet de définir une connexion à une base de données expérimentales.

Le bouton *Synchroniser la base expérimentale avec la base site* permet de charger dans la table de mesures de la base **sites.sqlite** les forages et leurs caractéristiques, ainsi que les points d'intérêt et les mesures de niveau piézométrique et de concentration disponibles en ces points.

----

## Menu général

:::{figure} /_static/images/Thyrsis_menu_base.png
:name: Thyrsis_menu_base
:scale: 300 %

Menu THYRSIS avant l'ouverture d'un projet Thyrsis.
:::

À l'ouverture de QGIS, si aucun projet THYRSIS n'est encore ouvert, le menu THYRSIS se présente sous la forme simplifiée de la {numref}`Thyrsis_menu_base`, avec les actions suivantes :

- ouverture d'un nouveau projet THYRSIS,
- ouverture d'une base de calcul THYRSIS déjà existante,
- ouverture de la fenêtre des {ref}`Préférences<preferences_fr>`,
- ouverture de l'aide en ligne, dans le navigateur par défaut du système.

### Nouveau

L'ouverture d'un nouveau projet s'effectue, via un menu déroulant, par le choix d'un site ou la construction d'un maillage, pour créer un nouveau modèle ({numref}`Thyrsis_Nouveau`) :

:::{figure} /_static/images/Thyrsis_Nouveau.png
:name: Thyrsis_Nouveau
Menu Nouveau permettant de sélectionner un site ou de construire un maillage
:::

- La sélection d'un site ouvre un nouveau menu déroulant, qui permet de choisir parmi différents modèles ou parmi différentes études sauvegardées (table **simulations** de la base **sites.sqlite**) ({numref}`Thyrsis_Nouveau_SACLAY`).
- La sélection de *Maillage...* donne accès à la définition d'un nouveau maillage : un nouveau projet, de type *maillage* est créé et un nouveau modèle peut être défini (cf. {ref}`Création de modèle<model_building_fr>`).

:::{figure} /_static/images/Thyrsis_Nouveau_SACLAY.png
:name: Thyrsis_Nouveau_SACLAY
Menu de sélection d'un modèle ou d'une étude
:::

### Ouvrir

L'action *Ouvrir* du menu THYRSIS permet d'ouvrir une base de calcul spatialite **calcul.sqlite** déjà existante en indiquant son emplacement dans l'arborescence du système.
Si un projet `calcul.qgs` existe dans le même répertoire que la base **calcul.sqlite**, un message d'alerte s'affiche, demandant si ce projet doit être écrasé :

- si la réponse est "oui", un nouveau projet `calcul.qgs` est construit à partir de la base **calcul.sqlite**, remplaçant le projet existant ;
- si la réponse est "non", aucune action n'est réalisée.

Pour ouvrir un projet `calcul.qgs` - intégrant la base **calcul.sqlite** - il faut utiliser le menu *Projet>Ouvrir* de QGIS.

Lorsqu'un projet THYRSIS est ouvert, le menu THYRSIS s'enrichit comme indiqué {numref}`Thyrsis_menu_detail`, avec plusieurs actions possibles, détaillées ci-après.

:::{figure} /_static/images/Thyrsis_menu_detail.png
:name: Thyrsis_menu_detail
:scale: 300 %

Menu THYRSIS après l'ouverture d'un projet THYRSIS
:::

### Simuler

L'action *Simuler* du menu Thyrsis permet ({numref}`Thyrsis_menu_simuler`) :

- d'initialiser les calculs,
- de lancer les calculs après initialisation,
- d'initialier et lancer les calculs. Cette action est directement réalisable en appuyant sur le bouton {{ img_run_simulation }} dans la {ref}`Barre de menu<menu_bar_fr>` ou de l'{ref}`Onglet Thyrsis<thyrsis_panel_fr>`.

:::{figure} /_static/images/Thyrsis_menu_simuler.png
:name: Thyrsis_menu_simuler
:scale: 300 %

Menu *Thyrsis>Créer*
:::

À l'issue des calculs, les barres de menu sont mises à jour (dates, "variables, unités), la couche "résultats" s'affiche dans QGIS, à la date ultime de la simulation, et les colonnes verticales de zone non saturée s'affichent dans la fenêtre ZNS.

#### Organisation des calculs

Une simulation crée dans le répertoire de la base de calcul *calcul.sqlite* un répertoire *calcul_tmp* contenant les fichiers d'entrée-sortie de tous les calculs d'hydrogéologie. Ceux-ci sont organisés par sous-répertoires de la façon suivante :

- des répertoires *samplexxxxx*, où xxxxx désigne le numéro du jeu de paramètres (table **parametres_simulation**), correspondant à chaque jeu de paramètres,

- dans chacun de ces répertoires :

  > - des répertoires *insatyyyyy*, où yyyyy désigne le numéro de la zone d'injection. Ces répertoires contiennent les fichiers d'entrée-sortie de chacun des calculs en zone non saturée,
  > - un répertoire *sature*, contenant les fichiers d'entrée-sortie du calcul en zone saturée.

À partir de l'ensemble de ces résultats, le plugin THYRSIS génère, dans le répertoire de la base de calcul, des fichiers numpy qui sont exploités pour l'affichage des résultats dans l'interface. Ces fichiers sont :

- calcul.variable.type.npy : valeurs de la variable « variable » à chaque date demandée (zone saturée), aux noeuds du maillage si type=node ou aux centres de mailles si type=elem,
- calcul.variable.insat.npy : valeurs de la variable \"variable\" aux noeuds de chaque colonne ZNS et à chaque date demandée (zone non saturée).

La variable peut être, selon ce qui a été spécifié dans les {ref}`Préférences<preferences_fr>` :

- en zone saturée :

  > - concentration,
  > - potentiel,
  > - vitesse de Darcy,
  > - debit

- en zone non saturée :

  > - saturation,
  > - vitesse de Darcy,
  > - concentration,
  > - activité

Le répertoire *calcul_tmp* peut être supprimé sans dommage pour l'utilisation de l'interface QGIS, excepté pour le calcul des cartes de probabilité, qui nécessite de disposer de l'ensemble des résultats de calcul pour chaque jeu de paramètres.
Ce répertoire est écrasé à chaque exécution de l'action "Simuler".

#### Fichiers externes
Dans le cas général, tous les fichiers d'entrée nécessaires au fonctionnement du code d'hydrogéologie sont générés automatiquement.
Certains cas particuliers nécessitent cependant de prendre en compte des fichiers spécifiques, dits “ fichiers utilisateur ”, préfixés par "u\_" et placés au même niveau que la base de calcul calcul.sqlite, et le projet QGIS calcul.qgs.
Ces fichiers particuliers sont présentés en annexe {ref}`Fichiers Utilisateur<user_files_fr>`.

### Créer

:::{figure} /_static/images/Thyrsis_menu_creer.png
:name: Thyrsis_menu_creer
:scale: 300 %

Menu *Thyrsis>Créer*
:::

Le menu *Thyrsis>Créer* donne accès aux actions suivantes ({numref}`Thyrsis_menu_creer`) :

- **Zones d'injections**

> Cette action supprime les injections existantes éventuelles et crée automatiquement des zones d'injection sur l'ensemble du domaine, en regroupant les colonnes 1D ZNS par taille et perméabilité, selon les valeurs de pas_echant_zns et rapport_max_permeabilite, définis dans la table {ref}`simulations<simulation_table_fr>` ({ref}`Dépôt réparti sur tout le domaine <full_domain_injection_fr>`). Voir exemple {numref}`Thyrsis_zones_injection`.

:::{figure} /_static/images/Thyrsis_zones_injection.png
:name: Thyrsis_zones_injection
Exemple de domaine décomposé en zones d'injection.
:::

- **Hypercube latin**

  > L'action "Hypercube Latin" permet de générer par {ref}`Hypercube Latin<latin_hypercube_fr>` plusieurs jeux de paramètres qui définissent autant de simulations à réaliser. Les informations à renseigner sont ({numref}`Thyrsis_Hypercube_Latin`) :
  >
  > - le nombre de simulations - qui définit le nombre de jeux de paramètres à générer,
  >
  > et pour chaque paramètre (de la table **parametres**) :
  >
  > - sa valeur minimale,
  > - sa valeur maximale,
  > - tsa loi de probabilité, à choisir parmi "constant", "uniform", "loguniform", "normal", "lognormal".
  >
  > Les jeux de paramètres sont créés dans la table **parametres_simulation**.
  >
  > L'action *simuler* génère autant de simulations qu'il y a de lignes dans cette table. Ces simulations sont parallélisées sur les processeurs disponibles.
  >
  > À l'issue des calculs, THYRSIS génère deux fichiers de résultats, contenant la moyenne et l'écart-type, pour chaque noeud et chaque date. Des cartes de probabilité de dépassement de seuil sont accessibles par le menu *Thyrsis>Créer>Carte de probabilité*

:::{figure} /_static/images/Thyrsis_Hypercube_Latin.png
:name: Thyrsis_Hypercube_Latin
Menu *Thyrsis>Créer>Hypercube Latin*
:::

- **Carte de probabilité**

  > L'action *Carte de probabilité* permet de construire une carte de probabilité de dépassement de seuil à partir de plusieurs simulations. Une fenêtre permet d'indiquer la valeur du seuil et son unité ({numref}`Thyrsis_seuil_probabilite`).
  > Le résultat de calcul de probabilité est considéré comme une couche de résultat, au même titre que la concentration ou le potentiel. Il est donc accessible par le menu déroulant de la barre de menu THYRSIS ({ref}`Barre de menu<menu_bar_fr>`).

:::{figure} /_static/images/Thyrsis_seuil_probabilite.png
:name: Thyrsis_seuil_probabilite
Menu *Thyrsis>Créer>Carte de probabilité*
:::

- **Isovaleurs**

  > L'action *Isovaleurs* permet de générer des lignes d'isovaleurs sur la couche active, qui peut être aussi bien la couche de résultats que l'une des couches raster décrivant les propriétés du modèle (cf. {ref}`Fenêtre 2D<2D_panel_fr>`) pourvu qu'elle soit associée à une légende.
  > La définition des isovaleurs s'opère via une fenêtre de dialogue ({numref}`Thyrsis_Isovaleurs`). Par défaut, la fenêtre propose les valeurs définies dans la légende de la couche considérée. Ces valeurs peuvent être modifiées manuellement, avec la possibilité d'en supprimer ou d'en ajouter. Elles peuvent aussi être générées automatiquement, en indiquant l'unité, le nombre, les valeurs minimale et maximale, ainsi que le type d'échelle, linéaire ou logarithmique. La génération des valeurs est déclenchée par action du bouton {{ img_view_refresh }}.

:::{figure} /_static/images/Thyrsis_Isovaleurs.png
:name: Thyrsis_Isovaleurs
:scale: 50 %

Menu *Thyrsis>Isovaleurs*
:::

- **Isovaleurs (gdal)**

  > L'action *Isovaleurs (gdal)* permet de générer des lignes d'isovaleurs sur la couche active, de la même manière que l'action *Isovaleurs* précédente, mais cette génération utilise l'outil *gdal_contour*, qui s'appuie sur la,création d'un fichier raster intermédiaire, dont la résolution doit être,précisée au moyen du nombre de pixels par mètre, par défaut égal à 0,1 ({numref}`Thyrsis_Isovaleurs_gdal`). Cette procédure évite quelques problèmes rencontrés avec la méthode directe précédente.

:::{figure} /_static/images/Thyrsis_Isovaleurs_gdal.png
:name: Thyrsis_Isovaleurs_gdal
:scale: 50 %

Menu *Thyrsis>Isovaleurs (gdal)*
:::

- **Courbes d'évolution**

  > L'action *Courbes d'évolution* permet d'ouvrir la fenêtre 1D, dans laquelle peuvent être représentées les évolutions en fonction du temps de la variable d'intérêt, aux points sélectionnés à l'aide de la souris. On peut déclencher cette action plus rapidement en cliquant sur le bouton {{ img_plot }} de la {ref}`Barre de menu<menu_bar_fr>`.

- **Second milieu**

  > L'action "Second milieu" permet de copier la base de calcul courante dans une nouvelle base de calcul, adaptée à un modèle {ref}`Double milieu<dual-porosity_fr>`:
  >
  > - les tables **noeuds_second_milieu**, **mailles_second_milieu**, **simulations_second_milieu** et **injections_second_milieu** sont renseignées par copie des tables **noeuds**, **mailles**, **simulations** et **injections** respectivement ;
  > - les entrées ASHAPE, BSHAPE, CSHAPE et PL sont ajoutées à la table paramètres
  > - la table **parametres_simulation** est réinitialisée.
  >
  > Pour exploiter cette nouvelle base de calcul **double milieu**, il est nécessaire d'ouvrir un nouveau projet (*Thyrsis>Ouvrir*) fondé sur cette base.

- **Élément chimique**

  > Cette action permet de créer ou supprimer un élément chimique.

(thyrsis-chemical-element)=

:::{figure} /_static/images/Thyrsis_chemical_element.png
:name: Thyrsis_chemical_element
:scale: 50 %

Dialogue de modification d'élément chimique
:::

(export_fr)=

### Exporter

(thyrsis-menu-exporter)=

:::{figure} /_static/images/Thyrsis_menu_exporter.png
:name: Thyrsis_menu_exporter
:scale: 200%

Menu *Thyrsis>Exporter*
:::

Le menu *Thyrsis>Exporter* donne accès aux actions suivantes :

- **Raster**

  > L'action *Raster* permet de créer une image au format PNG ou TIF de la représentation 2D courante, intégrant la date et la légende.

- **Animation**

  > L'action *Animation* permet de créer les images au format PNG correspondant au déroulement d'une simulation, en intégrant la représentation 2D (fenêtre QGIS) ainsi que la fenêtre ZNS et la fenêtre 1D. L'enregistrement commence en appuyant sur le bouton {{ img_play }} de la {ref}`Barre de menu<menu_bar_fr>`, et il s'arrête en appuyant sur le même bouton, devenu {{ img_pause }}.

- **Video**

  > L'action *Vidéo* permet de générer une vidéo au format AVI, par concaténation d'images PNG intégrant la représentation 2D (fenêtre QGIS) ainsi que la fenêtre ZNS et la fenêtre 1D. L'enregistrement commence à la date courante et s'interrompt en appuyant sur le bouton {{ img_pause }}.

- **Template**

  > L'action *Template* permet de sauvegarder l'étude en cours dans la base **sites.sqlite**, par ajout d'une entrée dans les tables :
  >
  > > - **simulations**,
  > > - **dates_resultats**,
  > > - **parametres**,
  > > - **metis_options**,
  > > - **injections**.
  >
  > Si une étude ayant le même nom existe déjà, l'ancienne étude est écrasée.

(menu_bar_fr)=
## Barre de menu

:::{figure} /_static/images/Thyrsis_barre_menu.png
:name: Thyrsis_barre_menu
Barre de menu
:::

La barre de menu donne accès aux principales actions du plugin THYRSIS :

- {{ img_forage }} permet de représenter la lithologie des forages qui sont renseignés dans la base de données. Ceux-ci s'affichent dans la fenêtre "Forage",
- {{ img_inverse_plot }} permet de représenter les résultats du processus d'inversion, en termes d'évolution de l'erreur en fonction de l'itération, et de corrélation entre les valeurs piézométriques mesurées et calculées,
- {{ img_inversion }} permet de (re)lancer le processus d'inversion, sous réserve que les tables points_pilote et hynverse_parametres soient renseignées,
- {{ img_run_simulation }} lance la simulation (voir la section {ref}`Onglet Thyrsis<thyrsis_panel_fr>` pour la définition des injections),
- le premier menu déroulant contient les dates de simulation et permet d'accéder aux résultats d'une date précise,
- le curseur offre une façon plus rapide d'accéder aux résultats aux différentes dates,
- {{ img_play }} permet de lancer une animation, en déroulant la visualisation des résultats aux différentes dates,
- {{ img_first }} permet d'afficher les résultats à la première date,
- {{ img_backward }} permet d'afficher les résultats à la date précédente,
- {{ img_forward }} permet d'afficher les résultats à la date suivante,
- {{ img_last }} permet d'afficher les résultats à la dernière date,
- la case à cocher "Ajuster l'échelle" permet d'adapter l'échelle de couleur aux résultats de chaque date. Non cochée, l'échelle de couleur est calculée sur l'ensemble des dates,
- les deux menus déroulants suivants permettent de choisir la grandeur à afficher et son unité,
- {{ img_pick }} permet de demander la valeur de la grandeur affichée sur la carte 2D en un point quelconque désigné par la souris,
- {{ img_plot }} ouvre la fenêtre matplotlib permettant de visulaliser aux points d'intérêt l'évolution en fonction du temps de la grandeur sélectionnée,
- {{ img_plot_bilan }} permet de visualiser le bilan de masse, en représentant l'évolution dans le temps des masses de composé injecté, présentes dans les zones non saturée et saturée, et sorties du domaine.

## Fenêtres

(2D_panel_fr)=
### Fenêtre 2D

:::{figure} /_static/images/Thyrsis_2D.png
:name: Thyrsis_2D
Exemple de fenêtre 2D, avec affichage de résultats de simulation
:::

La fenêtre 2D est la fenêtre centrale de QGIS. En interaction avec la fenêtre "Couches", elle permet de représenter en 2D, sur des fonds cartographiques variés, les grandeurs caractéristiques du modèle hydrogéologique :

- noeuds du maillage,
- maillage,
- points d'intérêt,
- forages,
- contours,
- potentiel (niveau piézométrique),
- perméabilité,
- altitude topographique (MNT),
- altitude du mur de l'aquifère,
- vitesse de Darcy,
- épaisseur de la zone non saturée,
- épaisseur de la nappe (en régime permanent),
- potentiel de référence, qui est un champ de potentiel obtenu par interpolation de données piézométriques, et qui a pu servir pour l'élaboration du modèle hydrogéologique, par inversion.

Les zones d'injection, définies dans l'{ref}`Onglet Thyrsis<thyrsis_panel_fr>`, s'affichent dans la fenêtre 2D.

À l'issue d'un calcul, les résultats s'affichent également, la date d'affichage et la variable à afficher étant contrôlées par la {ref}`Barre de menu<menu_bar_fr>`.

(thyrsis_panel_fr)=

### Onglet Thyrsis

:::{figure} /_static/images/Thyrsis_Injection_defaut.png
:name: Thyrsis_Injection_defaut
:scale: 50 %

Exemple de fenêtre Thyrsis, permettant de spécifier et caractériser les zones d'injection
:::

L'onglet THYRSIS permet de spécifier et caractériser les zones d'injection.

Les premières grandeurs à renseigner sont :

- le nom de la simulation,
- l'élément chimique à considérer, accessible par un menu déroulant,
- le coefficient de rétention (ou de partage) et, dans le cas d'une injection de masse, la limite de solubilité,
- la date de début de la simulation, au format JJ/MM/AAAA [hh:mm:ss],
- un ensemble de couples (durée, pas de temps), définissant les dates de sortie des résultats.

Puis le type d'injection est à sélectionner en cliquant sur le bouton adapté :

- flux,
- concentration,
- masse.

:::{figure} /_static/images/Thyrsis_Flux.png
:name: Thyrsis_Flux
:scale: 50 %

Exemple de fenêtre THYRSIS, permettant de définir un flux sur une zone d'injection
:::

Selon le bouton sélectionné, les champs à renseigner diffèrent, comme indiqué à la section {ref}`injection<injection_table_fr>`. Un volume d'eau non nul correspond à une fuite et induit une simulation d'écoulement en régime transitoire.

Il est possible d'ajouter autant d'injections que souhaité en appuyant à nouveau sur le bouton sélectionné, mais il n'est pas possible de mélanger plusieurs types d'injection au sein d'une même simulation.

Les coordonnées du centre de la zone d'injection peuvent être entrée manuellement ou en cliquant sur : ${\odot}$ et en désignant le point avec la souris.

De même, la surface peut être définie manuellement dans le champ Surface, ou calculée automatiquement en cliquant sur {{ img_draw }}  et en dessinant la zone concernée directement dans la fenêtre 2D.

En bas de l'onglet THYRSIS, trois boutons permettent d'annuler ou d'enregistrer les modifications, et de lancer les calculs :

- Le bouton "Annuler" permet d'annuler les modifications en cours et de recharger les informations telles qu'elles sont enregistrées dans la base de calcul.
- Le bouton "Enregistrer" permet de sauvegarder les modifications des injections (données et géométries) dans la base de calcul.
- {{ img_run_simulation }} permet de lancer les simulations.

:::{note}
Une fois les injections renseignées, il est important de cliquer sur le bouton *Enregistrer* avant de lancer les calculs en cliquant sur le bouton {{ img_run_simulation }}.
:::

### Forage

:::{figure} /_static/images/Thyrsis_Forage.png
:name: Thyrsis_Forage
:scale: 50 %

Exemple de log stratigraphique s'affichant dans la fenêtre de forage
:::

Lorque les informations de stratigraphie et de lithologie sont renseignées dans la base sites.sqlite, le log stratigraphique peut être affiché dans la fenêtre Forage, en cliquant sur le bouton |forage| de la barre de menu, et en sélectionnant directement dans la fenêtre 2D le forage d'intérêt.
Si le taux de fracturation est connu, celui est representé en correspondance du log stratigraphique.

Cette représentation graphique est fondée sur différentes codifications présentées en annexe {ref}`Logs stratigraphiques <stratigraphic_logs_fr>` :

- la codification de l'échelle des temps géologiques du BRGM,
- le code colorimétrique RGB de la Commission Mondiale des Cartes Géologiques (CGMW),
- la codification USGS des figurés.

### Fenêtre 3D

:::{figure} /_static/images/Thyrsis_3D.png
:name: Thyrsis_3D
Exemple de fenêtre 3D
:::

La fenêtre 3D permet une représentation en trois dimensions des trois niveaux suivants :

- altitude du mur de l'aquifère,
- le potentiel (niveau piézométrique),
- l'altitude topographique (MNT).

Si des résultats de simulation sont disponibles, ils s'affichent sur la couche potentiel, avec le code de couleur défini en 2D.

Cette fenêtre est contrôlée via les options de la fenêtre "Paramètres 3D" :

- le curseur permet d'ajuster l'échelle verticale,
- des cases à cocher permettent d'afficher ou non :
    - la texture,
    - les contours,
    - les forages,
    - les points de calcul.

La couleur du terrain (MNT) et des contours peut être modifiée.

### Zone non saturée (ZNS)

:::{figure} /_static/images/Thyrsis_ZNS.png
:name: Thyrsis_ZNS
Exemple de résultats s'affichant dans la fenêtre dédiée à la zone non saturée
:::

La fenêtre ZNS offre une représentation de l'évolution temporelle des variables simulées, dans les colonnes de zone non saturée modélisées.

Ces variables et leurs unités doivent être sélectionnées dans les menus déroulants de la barre de menu de la fenêtre.

Quelques options sont proposées, relatives à la légende :

- Mise à jour automatique de la légende : la légende s'adapte aux valeurs selon la date courante. Sinon, la légende est calculée sur l'ensemble des dates simulées.
- Légende : cette action ouvre une fenêtre permettant de personnaliser la légende,
- Position légende : permet de positionner la légende à gauche, à droite ou sous les colonnes.

### Fenêtre 1D

Les courbes d'évolution temporelle s'obtiennent à l'aide des boutons {{ img_plot }} et {{ img_plot_bilan }} dans la {ref}`Barre de menu<menu_bar_fr>`, et sont tracées dans une fenêtre ancrable :

- {{ img_plot_bilan }} trace l'évolution temporelle du bilan de masse (s'il existe).
- {{ img_plot }} trace l'évolution temporelle de la variable sélectionnée dans le menu déroulant de la {ref}`Barre de menu<menu_bar_fr>` , pour les points sélectionnés à l'aide de la souris dans la fenêtre 2D de QGIS.

:::{figure} /_static/images/Thyrsis_1D_bilan.png
:name: Thyrsis_1D_bilan
Exemple de représentation 1D de l'évolution temporelle du bilan de masse
:::

:::{figure} /_static/images/Thyrsis_1D_potentiel.png
:name: Thyrsis_1D_potentiel
Exemple de représentation 1D de l'évolution temporelle du potentiel, avec affichage des points de mesure
:::
