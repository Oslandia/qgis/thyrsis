---
substitutions:
  img_run_simulation: |-
    :::{image} /_static/images/config_simulation.png
    :scale: 10 %
    :::
---

# Création d'une étude

Une étude est créée à partir d'un modèle et décrit une ou plusieurs injections à la surface du sol, au sein de la zone insaturée ou directement dans la nappe. Une infiltration transitoire peut être modélisée.

## Base de calcul

Une nouvelle étude est créée en utilisant le menu *Thyrsis*, en sélectionnant le menu *Nouveau* puis le nom du modèle créé précédemment ({numref}`tutorial_newStudy`).

:::{figure} /_static/images/tutorial_newStudy.png
:name: tutorial_newStudy
Création d'une nouvelle étude
:::

Un nom de projet doit être choisi, ainsi qu'un répertoire d'étude, où sera créée la base de calcul et où seront sauvés tous les résultats.

THYRSIS crée un nouveau projet QGIS et le charge ({numref}`tutorial_newStudyCanvas`).

:::{figure} /_static/images/tutorial_newStudyCanvas.png
:name: tutorial_newStudyCanvas
Canevas QGIS à la création d'une nouvelle étude
:::

Le modèle est chargé dans QGIS et un nouvel onglet est ouvert sur la gauche.

## Paramètres de simulation

L'étude utilise les paramètres par défaut associés au modèle hydrogéologique (cf {ref}`Paramètres du modèle <model_parameters_fr>`) mais ces paramètres peuvent être modifiés, soit directement dans la table attributaire **parametres**, soit en sélectionnant le menu {ref}`Hypercube Latin<latin_hypercube_fr>` (*Thyrsis>Créer>Hypercube Latin*) qui peut aussi être utilisé pour générer plusieurs ensembles de paramètres,dans le cadre de calculs d'incertitude. Pour chaque paramètre, une valeur de référence, une valeur minimale et maximale ainsi qu'une loi de probabilité doivent être définies.

Pour le tutoriel, nous proposons les valeurs de paramètres de la{numref}`tutorial_parameters`.

:::{figure} /_static/images/tutorial_parameters.png
:name: tutorial_parameters
:scale: 50 %

Définition des paramètres pour une nouvelle étude
:::

## Injection

{ref}`Les caractéristiques de l'injection<injection_characteristics_fr>` sont définies dans l'{ref}`Onglet Thyrsis<thyrsis_panel_fr>` décrit dans le manuel utilisateur.

Pour ce tutoriel, un flux de HTO est défini en une unique zone d'injection.

Le choix de HTO s'effectue via le menu déroulant *Elément chimique*.

L'injection est définie en cliquant sur le bouton *Flux* ({numref}`tutorial_injectionButtons`).

:::{figure} /_static/images/tutorial_injectionButtons.png
:name: tutorial_injectionButtons
Boutons d'injection
:::

et en renseignant le formulaire avec les valeurs suivantes :

- Début : 01/01/2012
- Duration : 0.00021 year (peut aussi être défini en mois ou en jours
- Flux : $9.8 10^{-9}$ (entrer `9.8e-9`)
- Surface : 59 (ou utiliser le crayon pour dessiner la zone sur la carte)
- Localisation : utiliser le bouton ${\odot}$ et cliquer sur la carte pour définir le centre de l'injection
- Profondeur : 0
- Volume d'eau : 0

Ensuite, cliquer sur le bouton Enregistrer : la zone d'injection a été ajoutée à la couche d'injection.

## Simulation et résultats

Pour lancer la simulation, cliquer sur le bouton {{ img_run_simulation }}. Après le calcul, une couche *resultats* est ajoutée au canevas.

L'onglet ZNS s'ouvre avec les concentrations simulées représentées dans une colonne 1D ({numref}`tutorial_ZNStab`).

:::{figure} /_static/images/tutorial_ZNStab.png
:name: tutorial_ZNStab
:scale: 50 %

Onglet ZNS
:::

La barre de contrôle des dates ({numref}`tutorial_timeControlBar`) et celle des résultats ({numref}`tutorial_resultControlBar`) de la barre de menu THYRSIS sont mis à jour :

- la barre de contrôle des dates contient les dates de simulation,
- la barre de contrôle des résultats et le menu déroulant des unités sont mis à jour avec les informations des variables simulées.

:::{figure} /_static/images/tutorial_timeControlBar.png
:name: tutorial_timeControlBar
Barre de contrôle des dates
:::

:::{figure} /_static/images/tutorial_resultControlBar.png
:name: tutorial_resultControlBar
Barre de contrôle des résultats
:::

Les résultats sont affichés dans le canevas ({numref}`tutorial_resultCanvas`) et dans une fenêtre 1D ({numref}`tutorial_1D`) à chaque date de simulation, via le menu déroulant des dates ou en utilisant les flèches de la barre de menu.

:::{figure} /_static/images/tutorial_resultCanvas.png
:name: tutorial_resultCanvas
Canevas QGIS avec le résultat de la simulation
:::

:::{figure} /_static/images/tutorial_1D.png
:name: tutorial_1D
:height: 300 px
:width: 300 px

Graphique 1D
:::

à chaque date de simulation via le menu déroulant des dates ou en utilisant les flèches de la barre de menu.

De plus, un canevas 3D permet de visualiser les résultats en 3D ({numref}`tutorial_3D`).

:::{figure} /_static/images/tutorial_3D.png
:name: tutorial_3D
Graphique 3D
:::

:::{note}
La scène 3D peut être configurée à partir du menu de paramètres 3D. Pour projeter le fond Open Street Map sur la scène 3D, ajouter la couche au canevas de QGIS et cocher la case *Texture*.
:::
