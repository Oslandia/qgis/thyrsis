# coding : utf-8
import os

from PyQt5.QtWidgets import QDialog
from qgis.PyQt import uic


class SridConfirmDialog(QDialog):
    def __init__(self, current_srid, calc_DB=False, parent=None):
        super(SridConfirmDialog, self).__init__(parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "srid_confirm.ui"), self)

        self.srid_label.setText("EPSG : " + str(current_srid))

        if calc_DB == True:
            self.label.setText(
                self.tr(
                    "You will create a computational database.\nIt will have the same projection as your mesh database."
                )
            )
            self.srid_label.setText("")

        self.buttonBox.accepted.connect(self.__accept)
        self.buttonBox.rejected.connect(self.close)

    def __accept(self):
        return True
