# coding = UTF-8

import os
from datetime import datetime

from qgis.PyQt import uic
from qgis.PyQt.QtCore import QTimer, pyqtSignal
from qgis.PyQt.QtWidgets import QWidget


class TimeControl(QWidget):
    """Time Control toolbar"""

    timeChanged = pyqtSignal(int)

    def __init__(self, dates=None, blocker=None, framePerSecond=10, parent=None):
        """Constructor

        :param dates: dates of simulation
        :type dates: list
        :param blocker: the blocker prevents time update, \
                        if initialized to True, it will wait
                        for the nextFrame function to be called
        :type blocker: bool
        :param framePerSecond: refresh time for animation
        :type framePerSecond: int
        :param parent: widget parent
        :type parent: QWidget

        :note:the blocker prevents time update, if initialized to True, it will wait
        for the nextFrame function to be called
        """
        super(TimeControl, self).__init__(parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "time_control.ui"), self)
        self.timer = QTimer(None)
        if dates is not None:
            self.setDates(dates)
        self.timeSlider.valueChanged.connect(self.timeChanged.emit)
        self.playButton.toggled.connect(self.play)
        self.firstButton.clicked.connect(self.firstDate)
        self.lastButton.clicked.connect(self.lastDate)
        self.previousButton.clicked.connect(self.previousDate)
        self.nextButton.clicked.connect(self.nextDate)
        self.__blocker = blocker
        self.__framePerSecond = framePerSecond

    def nextFrame(self):
        """Unlock blocker"""
        self.__blocker = False

    def setDates(self, dates):
        """initialize the time control"""
        self.dateComboBox.clear()
        self.dateComboBox.addItems(
            [
                datetime.strptime(date, "%Y-%m-%d %H:%M:%S").strftime(
                    "%d/%m/%Y %H:%M:%S"
                )
                for date in dates
            ]
        )
        self.timeSlider.setMinimum(0)
        self.timeSlider.setMaximum(len(dates) - 1 if len(dates) != 0 else 0)

    def __animate(self):
        """Use to restart the time slider during animation"""
        if self.__blocker is None or not self.__blocker:
            self.__blocker = True
            self.timeSlider.setValue(
                (self.timeSlider.value() + 1) % (self.timeSlider.maximum() + 1)
            )

    def play(self, checked):
        """Launch/Stop time animation, according to the play button state"""
        if checked and not self.timer.isActive():
            self.timer.timeout.connect(self.__animate)
            self.timer.start(int(1000.0 / self.__framePerSecond))
        elif self.timer.isActive():
            self.timer.stop()

    def firstDate(self):
        """Set time slider on first date"""
        self.timeSlider.setValue(0)

    def previousDate(self):
        """Set time slider on previous date"""
        self.timeSlider.setValue(
            (self.timeSlider.value() - 1) % (self.timeSlider.maximum() + 1)
        )

    def nextDate(self):
        """Set time slider on next date"""
        self.timeSlider.setValue(
            (self.timeSlider.value() + 1) % (self.timeSlider.maximum() + 1)
        )

    def lastDate(self):
        """Set time slider on last date"""
        self.timeSlider.setValue(self.timeSlider.maximum())

    def suspendPlayFunction(self, flag=None):
        """Set the animation on pause"""
        self.playButton.setChecked(False)
        self.playButton.toggled.disconnect(self.play)

    def restorePlayFunction(self, flag=None):
        """re-launch the animation on pause"""
        self.playButton.toggled.connect(self.play)

    def value(self):
        """Get current date index

        :return: index of the current date
        :rtype: integer
        """
        return self.timeSlider.value()

    def setValue(self, idx):
        """Set current date index

        :param idx: index of the date
        :type idx: integer
        """
        self.timeSlider.setValue(idx)

    def maximum(self):
        """Get max date index

        :return: index of the max date
        :rtype: integer
        """
        return self.timeSlider.maximum()

    def text(self):
        """Get current date text

        :return: current date
        :rtype: string
        """
        return self.dateComboBox.currentText()

    def setFramePerSecond(self, framePerSecond):
        """Set frame per second

        :param framePerSecond: animation refresh time
        :type framePerSecond: integer
        """
        self.__framePerSecond = framePerSecond

    def setDatabasePath(self, dbpath):
        """Set dbpath

        :param dbpath: db path
        :type dbpath: string
        """
        self.__dbpath = dbpath

    def databasePath(self):
        """Get the db path

        :return: db path
        :rtype: string
        """
        return self.__dbpath
