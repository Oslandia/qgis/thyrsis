from PyQt5.QtCore import QMutex, QSize, QThread, pyqtSignal
from PyQt5.QtGui import QImage, QPainter
from PyQt5.QtWidgets import QApplication
from qgis.core import (
    QgsCoordinateReferenceSystem,
    QgsLayoutMeasurementConverter,
    QgsMapLayerRenderer,
    QgsPluginLayer,
    QgsPluginLayerType,
    QgsRectangle,
    QgsRenderContext,
)

from .utilities import Timer


class OpenGlLayerType(QgsPluginLayerType):
    """Type for opengl layer"""

    def __init__(self, type_=None):
        """Constructor

        :param type_: type of provider
        :type type_: Providerclass

        """
        QgsPluginLayerType.__init__(self, type_ or OpenGlLayer.LAYER_TYPE)
        # self.__dlg = None

    def createLayer(self):
        """Create a OpenGlLayer

        :return: OpenGlLayer
        :rtype: OpenGlLayer"""
        return OpenGlLayer()

    def showLayerProperties(self, layer):
        """Open the mesh layer property dialog

        :param layer: QgsMapLayer
        :type layer: QgsMapLayer

        :return: success state
        :rtype: bool
        """
        # self.__dlg = PropertyDialog(layer)
        return False


class OpenGlRenderer(QgsMapLayerRenderer):
    """OpenGlRenderer"""

    def __init__(self, layerId, rendererContext, layer):
        """Constructor

        :param layerId:map layer id
        :type layerId: string
        :param rendererContext: openGL context
        :type rendererContext: QOpenGLContext
        :param layer: map layer
        :type layer: QgsMapLayer
        """
        super(OpenGlRenderer, self).__init__(layerId)
        self.rendererContext = rendererContext
        self.layer = layer

    def render(self):
        """Render the opengl mesh

        :return: image
        :rtype: QImage
        """
        return self.layer.draw(self.rendererContext)


class OpenGlLayer(QgsPluginLayer):
    """Base class to encapsulate the tricks to create OpenGL layers
    Be careful, the layer is drawn in main thread due to current Qt limitations
    care must be taken not to stall the event loop while requesting
    a render job since since the rendering thread signal will not be
    passed to the main thread.

    Child class must implement the image method
    """

    LAYER_TYPE = "opengl_layer"

    __msg = pyqtSignal(str)
    __drawException = pyqtSignal(str)
    __imageChangeRequested = pyqtSignal()

    def __print(self, msg):
        """Print a message in the console

        :param msg: message
        :type msg: string
        """
        print(msg)

    def __raise(self, err):
        """Raise exception

        :param err: message
        :type err: string
        """
        raise Exception(err)

    def __init__(self, type_=None, name=None):
        """Constructor

        :param type_: type of provider
        :type type_: Providerclass
        :param name: layer name
        :type name: string
        """
        QgsPluginLayer.__init__(
            self, type_ if type_ is not None else OpenGlLayer.LAYER_TYPE, name
        )
        self.__imageChangedMutex = QMutex()
        self.__imageChangeRequested.connect(self.__drawInMainThread)
        self.__img = None
        self.__rendererContext = None
        self.__drawException.connect(self.__raise)
        self.__msg.connect(self.__print)
        self.setExtent(QgsRectangle(-1e9, -1e9, 1e9, 1e9))
        self.setCrs(QgsCoordinateReferenceSystem("EPSG:2154"))
        self.__destCRS = None
        self.setValid(True)
        self.__timing = False

    def image(self, rendererContext, size):
        """This is the function that should be overwritten
        the rendererContext does not have a painter and an
        image must be returned instead

        :param rendererContext: openGL context
        :type rendererContext: QOpenGLContext
        :param size: size
        :type size: QSize
        """
        ext = rendererContext.extent()
        mapToPixel = rendererContext.mapToPixel()
        windowSize = QSize(
            int((ext.xMaximum() - ext.xMinimum()) / mapToPixel.mapUnitsPerPixel()),
            int((ext.yMaximum() - ext.yMinimum()) / mapToPixel.mapUnitsPerPixel()),
        )
        img = QImage(windowSize, QImage.Format_ARGB32)
        painter = QPainter()
        painter.begin(img)
        painter.drawText(100, 100, "GlMesh.image default implementation")
        painter.end()
        img.save("/tmp/toto.png")
        print("default image, we should not be here")
        return img

    def __drawInMainThread(self):
        self.__imageChangedMutex.lock()
        self.__img = self.image(self.__rendererContext, self.__size)
        self.__imageChangedMutex.unlock()

    def draw(self, rendererContext):
        """This function is called by the rendering thread. \
        GlMesh must be created in the main thread.

        :param rendererContext: openGL context
        :type rendererContext: QOpenGLContext

        :return: success state
        :rtype: bool
        """
        timer = Timer() if self.__timing else None
        try:
            # /!\ DO NOT PRINT IN THREAD
            painter = rendererContext.painter()
            self.__imageChangedMutex.lock()
            self.__rendererContext = QgsRenderContext(rendererContext)
            self.__rendererContext.setPainter(None)
            self.__size = painter.viewport().size()
            self.__img = None
            self.__imageChangedMutex.unlock()
            if QApplication.instance().thread() != QThread.currentThread():
                self.__imageChangeRequested.emit()
                while not self.__img and not rendererContext.renderingStopped():
                    # active wait to avoid deadlocking if event loop is stopped
                    # this happens when a render job is cancellled
                    QThread.msleep(1)
                if rendererContext.renderingStopped():
                    self.__msg.emit("rendering stopped")

                if not rendererContext.renderingStopped():
                    painter.drawImage(0, 0, self.__img)
            else:
                # QgsRenderContext painter size is not appropriate to print because
                # viewport doesn't fit to QgsLayoutItemMap.
                # We identify the current layout used for export.
                if rendererContext.last_layout:
                    layout = rendererContext.last_layout
                    size = layout.referenceMap().sizeWithUnits()
                    converter = QgsLayoutMeasurementConverter()
                    converter.setDpi(layout.renderContext().dpi())
                    size = converter.convert(size, 7)
                    self.__size = QSize(size.width(), size.height())
                self.__drawInMainThread()
                painter.drawImage(0, 0, self.__img)
            if self.__timing:
                self.__msg.emit(timer.reset("OpenGlLayer.draw"))
            return True
        except Exception as e:
            # since we are in a thread, we must re-raise the exception
            self.__drawException.emit(e.__traceback__.format_exc())
            return False

    def createMapRenderer(self, rendererContext):
        """create the renderer for the open_gl layer

        :param rendererContext: openGL context
        :type rendererContext: QOpenGLContext

        :return: OpenGlRenderer
        :rtype: OpenGlRenderer
        """
        return OpenGlRenderer(self.id(), rendererContext, self)

    def setTransformContext(self, context):
        """Contains information about the context in which a coordinate transform is executed. UNUSED"""
        return
