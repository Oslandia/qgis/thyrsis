"""This module contains the class to create a job queue
"""


class JobQueue:
    """
    Simple class to create a job queue

    The jobs are entered as a list of string, the call to next() returns
    the next job to perform

    The process of rank 0 is used as manager

    If MPI is not defined, the job queue is just a simple list

    Inspired by http://etutorials.org/Linux+systems/cluster+computing+with+linux/Part+II+Parallel+Programming/Chapter+8+Parallel+Programming+with+MPI/8.2+Manager+Worker+Example/
    """

    def __init__(self, jobs, MPI, managed=True):
        """Constructor

        :param jobs: list of jobs
        :type jobs: list
        :param MPI: MPI parameter
        :type MPI: MPI
        :param managed: unused flag
        :type managed: bool
        """
        self.__MPI = MPI if MPI is not None and MPI.COMM_WORLD.Get_size() > 1 else None
        self.__rank = MPI.COMM_WORLD.Get_rank() if MPI is not None else 0
        self.__size = len(jobs)
        self.__current_job = None
        self.__jobs = jobs
        self.__managed = managed

        if (
            self.__MPI is not None and not managed
        ):  # split computation between processes
            self.__jobs = [
                job
                for i, job in enumerate(jobs)
                if i % MPI.COMM_WORLD.Get_size() == self.__rank
            ]
            print(
                "process ",
                MPI.COMM_WORLD.Get_rank(),
                " deals with\n",
                "\n".join(self.__jobs),
            )

        elif self.__MPI is not None and self.__rank == 0:
            numsent = 0
            for rk in range(1, min(MPI.COMM_WORLD.Get_size(), self.__size + 1)):
                MPI.COMM_WORLD.send(jobs[numsent], dest=rk)
                numsent += 1
            # receive 'done' from workers
            for i in range(self.__size):
                status = MPI.Status()
                msg = MPI.COMM_WORLD.recv(source=MPI.ANY_SOURCE, status=status)
                assert msg == "job done"
                sender = status.Get_source()
                if numsent < self.__size:
                    MPI.COMM_WORLD.send(jobs[numsent], dest=sender)
                    numsent += 1
                else:  # no more work
                    MPI.COMM_WORLD.send("all done", dest=sender)

    def __iter__(self):
        """Make that the class is in iterable"""
        return self

    def __next__(self):
        """Return the next job"""
        if self.__MPI is None or not self.__managed:  # simple list of tasks
            if not len(self.__jobs):
                raise StopIteration
            else:
                return self.__jobs.pop()
        else:
            if self.__rank == 0 or self.__rank > self.__size:
                raise StopIteration

            if self.__current_job is not None:
                self.__MPI.COMM_WORLD.send("job done", dest=0)

            self.__current_job = self.__MPI.COMM_WORLD.recv(source=0)
            if self.__current_job == "all done":
                raise StopIteration
            else:
                return self.__current_job
