"""
load potentiel_reference in mesh database

USAGE

    python -m thyrsis.database.load_potentiel_reference mesh_database potentiel_reference

    try to load potentiel_reference into the specified mesh database

    potentiel_reference must be calculated at nodes
"""

from ..log import logger


def load_potentiel_reference(conn, fileName):
    """load reference potential values into the current database

    :param conn: connection on a sqlite database
    :type cur: sqlite3.Connection
    :param fileName: path to a csv file of interests points
    :type fileName: string

    :return: None
    """
    cur = conn.cursor()

    with open(fileName) as fil:
        values = [(float(x), i + 1) for i, x in enumerate(fil.read().splitlines())]
        cur.executemany(
            "UPDATE noeuds SET potentiel_reference=? WHERE OGC_FID=?", values
        )


if __name__ == "__main__":
    import sys

    from ..database import sqlite

    # from ..settings import Settings

    logger.enable_console(True)

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    if len(sys.argv) != 3:
        logger.error("wrong number of arguments (try database.py --help)")
        exit(1)

    conn = sqlite.connect(sys.argv[1])

    load_potentiel_reference(conn, sys.argv[2])
    conn.commit()
