"""
Simple gui to define isovalues and their units
"""

import os

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from qgis.PyQt import uic

from ..utilities import unitConversionFactor


class ProbabilityDialog(QDialog):
    """Dialog to define probability map parameter"""

    def __init__(self, value, unit):
        """Constructor

        :param value: threshold value
        :type: float
        :param: unit
        :type: string
        """
        super(ProbabilityDialog, self).__init__()
        uic.loadUi(os.path.join(os.path.dirname(__file__), "probability.ui"), self)

        self.__value = value
        self.__unit = unit
        self.__unitsGroup = None

        fmt = "%.1f" if value < 10000 and value >= 0.1 else "%.2e"
        self.thresholdValue.setText(fmt % value)

        unitGroups = {
            "kg/m³": [
                "kg/L",
                "g/L",
                "mg/L",
                "µg/L",
                "kg/m³",
                "g/m³",
                "mg/m³",
                "µg/m³",
                "GBq/L",
                "MBq/L",
                "kBq/L",
                "Bq/L",
                "mBq/L",
                "µBq/L",
                "GBq/m³",
                "MBq/m³",
                "kBq/m³",
                "Bq/m³",
                "mBq/m³",
                "µBq/m³",
            ],
            "m": ["m"],
            "m/s": ["m/s", "m/an", "cm/j"],
        }

        for unitKey, unitsGroup in unitGroups.items():
            if unit in unitsGroup:
                self.__unitsGroup = unitKey
                self.unitsComboBox.addItems([u for u in unitsGroup])
                self.unitsComboBox.setCurrentIndex(unitsGroup.index(unit))

        self.unitsComboBox.currentIndexChanged.connect(self.__unitChanged)

        self.thresholdValue.textChanged.connect(self.__setThresholdValue)

    def __setThresholdValue(self):
        """Set the threshold value from combobox"""
        fct = unitConversionFactor(self.unitsComboBox.currentText(), self.__unitsGroup)
        try:
            self.__value = (
                fct * float(self.thresholdValue.text())
                if len(self.thresholdValue.text()) > 0
                else 0
            )
            self.buttonBox.setEnabled(True)
        except ValueError:
            self.buttonBox.setEnabled(False)
        # print "value changed :", self.__value, self.__unitsGroup

    def __unitChanged(self):
        """Triggered when units changes"""
        self.__unit = self.unitsComboBox.currentText()
        fct = unitConversionFactor(self.unitsComboBox.currentText(), self.__unitsGroup)
        try:
            self.__value = fct * float(self.thresholdValue.text())
            self.buttonBox.setEnabled(True)
        except ValueError:
            self.buttonBox.setEnabled(False)
        # print "unit changed :", self.__value, self.__unitsGroup

    def exec_(self):
        """Return the threshold value"""
        return self.__value if QDialog.exec_(self) else None


if __name__ == "__main__":

    import sys

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    app = QApplication(sys.argv)

    dlg = ProbabilityDialog(0, "µg/L")
    value = dlg.exec_()
    print(value)
