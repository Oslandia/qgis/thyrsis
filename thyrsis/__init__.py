try:
    import matplotlib

    matplotlib.use("QT5Agg")
except:
    print("Ok for packaging, else please install sip")


def classFactory(iface):
    """Plugin call

    :param iface: qgis interface
    :type iface: QgisInterface

    :return: ThyrsisPlugin
    :rtype: plugin.ThyrsisPlugin"""
    from .plugin import ThyrsisPlugin

    return ThyrsisPlugin(iface)
