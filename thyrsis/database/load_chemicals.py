"""
load chemical in site database

USAGE

    python -m thyrsis.database.load_chemicals chemicals.csv

    load chemical into the database

    the expected .csv format is:

        - chemicals.csv: "name", "period", "atomic_mass", "radioactive_decay", "specific_activity", "water_quality_limit", "atomic_number", "masse_number"

"""
import csv
from builtins import input, str

from ..log import logger


def get_float(string):
    """convert a string into float, even with a ',' decimal limit

    :param string: value to convert
    :type string: string

    :return: converted value
    :rtype: float
    """
    return float(string.replace(",", ".")) if string != "" else 0.0


def load_chemicals(cur, chemicals, delete_confirm=True):
    """load chemicals values into the current database

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param chemicals: path to a csv file of chemicals
    :type chemicals: string
    :param delete_confirm: True if user must confirm chemicals delete
    :type delete_confirm: boolean

    :return: None
    """
    (nchemicals,) = cur.execute("SELECT COUNT(1) from elements_chimiques ").fetchone()
    delete = nchemicals > 0
    if delete and delete_confirm:
        ans = input(
            str(nchemicals)
            + " chemicals already exist : do you want to remove it ? (yes/no) "
        )
        if ans[0] != "y":
            return
    if delete:
        logger.notice("Remove chemicals")
        cur.execute("DELETE FROM elements_chimiques")

    values = []
    with open(chemicals) as chs:
        for i, row in enumerate(csv.reader(chs, delimiter=";")):
            if i == 0 or len(row) != 8:
                continue

            id = i
            nom = str(row[0])
            period = get_float(row[1])
            atomic_mass = get_float(row[2])
            rad_decay = get_float(row[3])
            spe_act = get_float(row[4])
            water_limit = get_float(row[5])
            atomic_nb = int(row[6])
            masse_nb = int(row[7])
            values.append(
                (
                    id,
                    nom,
                    period,
                    atomic_mass,
                    rad_decay,
                    spe_act,
                    water_limit,
                    atomic_nb,
                    masse_nb,
                )
            )

        values.append((i + 1, "Tracer", None, None, 0.0, 0.0, 0.0, None, None))
        cur.executemany(
            """
            INSERT INTO elements_chimiques(id, nom, periode, masse_atomique, decroissance_radioactive, activite_specifique, limite_qualite_eau, numero_atomique, nombre_de_masse)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)
            """,
            values,
        )
        cur.execute("COMMIT")

        (nchemicals,) = cur.execute(
            "SELECT COUNT(1) from elements_chimiques"
        ).fetchone()
        logger.notice(nchemicals, " chemicals registered")


if __name__ == "__main__":
    import sys

    from ..database import sqlite
    from ..settings import Settings

    logger.enable_console(True)

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    if len(sys.argv) != 2:
        logger.error("wrong number of arguments (try load_chemicals.py --help)")
        exit(1)

    if not Settings().contains("General", "defaultSite"):
        logger.error("defaultSite undefined, use preferences_dialog.py")
        exit(1)

    sitesdb = str(Settings().value("General", "defaultSite"))

    conn = sqlite.connect(sitesdb)

    load_chemicals(conn.cursor(), sys.argv[1])
    conn.commit()
