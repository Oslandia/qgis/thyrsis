"""
Thyrsis spatialite database structure

USAGE
    python -m thyrsis.database siteName simulationName ouput.sqlite

    create a computation db from template

"""
import os
import random
import re
import shutil
from builtins import object, range, str, zip
from math import exp as exp_
from math import floor, log, sqrt

import numpy
import psycopg2
from scipy.special import erfinv

from thyrsis.exception import (
    DatabaseUpgradeError,
    ProjectCreationError,
    SecondMilieuCreationError,
)

from ..log import logger
from ..settings import Settings
from . import sqlite

# @note we use connections directly here, so we don't need to import
#      pyspatialite


DEFAULT_SRID = "4326"  # WGS84
ADMISSIBLE_COORD_BOX = [-22841.3905, 1712212.6192, 1087335.9605, 2703971.1254]
#SCHEMA_VERSION = "18.03.21"
SCHEMA_VERSION_240119 = "2024.01.19"
SCHEMA_VERSION = "2024.08.02"
MESH_TABLES = [
    "contours",
    "bords",
    "domaines",
    "extremites",
    "points_fixes",
    "parametres",
    "noeuds",
    "potentiel_impose",
    "noeuds_contour",
    "mailles",
    "zones",
    "points_pilote",
    "metis_hynverse_options",
    "openfoam_hynverse_options",
    "hynverse_parametres",
    "hynverse_erreurs",
]

SITE_TABLES = [
    "sites",
    "elements_chimiques",
    "simulations",
    "parametres",
    "metis_options",
    "openfoam_options",
    "dates_resultats",
    "injections",
    "points_interet",
    "mesures",
    "forages",
    "stratigraphies",
    "lithologies",
    "fracturations",
]

COMP_TABLES = [
    "parametres_simulation",
    "noeuds_injections",
    "mailles_injections",
    "dates_simulation",
    "noeuds_zns",
    "mailles_zns",
    "bilan_masse",
    "isovaleur",
]


class DebugCursor(object):
    """Unused"""

    def __init__(self, cur):
        self.__cur = cur

    def execute(self, sql):
        logger.debug(sql)
        return self.__cur.execute(sql)

    def executemany(self, sql, arg):
        logger.debug(sql)
        return self.__cur.executemany(sql, arg)

    def fetchone(self):
        return self.__cur.fetchone()

    def fetchall(self):
        return self.__cur.fetchall()


def _execute_statements(cur, filename, project_SRID=None):
    """Execute sql queries written in a file

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param filename: file name path
    :type cur: string
    :param project_SRID: EPSG code
    :type project_SRID: string
    """
    with open(os.path.join(os.path.dirname(__file__), filename)) as f:
        for statement in f.read().split(";;"):
            if project_SRID != " " and project_SRID:
                statement = statement.replace("SRID", str(project_SRID))
            else:
                statement = statement.replace("SRID", DEFAULT_SRID)
            cur.execute(statement)


def db_node_to_elem(cur, column, node_values):
    """updates mesh table from nodes table

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param column: column name
    :type column: string
    :param node_values: list of node value for a column
    :type node_values: list
    """
    # node_values est une sortie de parse_file, sous la forme ((value, id), ...)

    mailles = cur.execute(
        "SELECT OGC_FID, a, b, c, d FROM mailles ORDER BY OGC_FID"
    ).fetchall()
    values = node_to_elem(mailles, node_values)
    if values:
        cur.executemany(
            "UPDATE mailles SET " + column + " = ? WHERE OGC_FID = ? ", values
        )


def node_to_elem(mailles, node_values):
    """returns mesh values with format ((value, id), ...)

    :param mailles: mesh elements
    :type mailles: list
    :param node_values: list of node value for a column
    :type node_values: list

    :return: mesh values
    :rtype: list
    """
    # node_values est une sortie de parse_file, sous la forme ((value, id), ...)

    input_values = {i: value for value, i in node_values}

    if None not in list(input_values.values()):
        values = []
        for [mid, a, b, c, d] in mailles:
            if d:
                values.append(
                    (
                        (
                            input_values[a]
                            + input_values[b]
                            + input_values[c]
                            + input_values[d]
                        )
                        * 0.25,
                        mid,
                    )
                )
            else:
                values.append(
                    ((input_values[a] + input_values[b] + input_values[c]) / 3.0, mid)
                )

        return values
    else:
        return None


def db_elem_to_node(cur, column, elem_values):
    """updates nodes table from mesh table

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param column: column name
    :type column: string
    :param elem_values: list of element value for a column
    :type elem_values: list
    """
    # elem_values est une sortie de parse_file, sous la forme ((value, id), ...)

    noeuds = [
        x[0]
        for x in cur.execute("SELECT OGC_FID FROM noeuds ORDER BY OGC_FID").fetchall()
    ]
    mailles = cur.execute(
        "SELECT OGC_FID, a, b, c, d FROM mailles ORDER BY OGC_FID"
    ).fetchall()
    values = elem_to_node(noeuds, mailles, elem_values)
    if values:
        cur.executemany(
            "UPDATE noeuds SET " + column + " = ? WHERE OGC_FID = ? ", values
        )


def elem_to_node(noeuds, mailles, elem_values):
    """returns node values with format ((value, id), ...)

    :param noeuds: nodes
    :type noeuds: list
    :param mailles: mesh elements
    :type mailles: list
    :param elem_values: list of element value for a column
    :type elem_values: list

    :return: nodes values
    :rtype: list
    """

    # elem_values est une sortie de parse_file, sous la forme ((value, id), ...)

    input_values = {i: value for value, i in elem_values}

    if None not in list(input_values.values()):
        dict_values = {n: [] for n in noeuds}
        for [mid, a, b, c, d] in mailles:
            dict_values[a].append(input_values[mid])
            dict_values[b].append(input_values[mid])
            dict_values[c].append(input_values[mid])
            if d:
                dict_values[d].append(input_values[mid])

        return [(numpy.mean(x), i) for i, x in list(dict_values.items())]
    else:
        return None


def mesh_tables_exists(cur):
    """test if mesh tables exist

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor

    :return: success state
    :rtype: bool
    """
    cur.execute(
        """SELECT COUNT(1) FROM geometry_columns
        WHERE f_table_name IN ('noeuds', 'mailles')"""
    )
    [count] = cur.fetchone()
    return count == 2


def parametres_simulation_exists(cur):
    """Check if the simulation parameters exists

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor

    :return: success state
    :rtype: bool
    """
    return (
        cur.execute(
            """
        SELECT count(1) FROM sqlite_master WHERE type='table' AND name='parametres_simulation'
        """
        ).fetchone()[0]
        == 1
    )


def version(cur):
    """return db version, None if metadata is not there

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor

    :return: db version
    :rtype: string
    """
    cur.execute(
        "SELECT count(1) FROM sqlite_master WHERE type='table' AND name='metadata'"
    )
    [count] = cur.fetchone()
    return (
        None if not count else cur.execute("SELECT version FROM metadata").fetchone()[0]
    )


def create_metadata_table(cur):
    """Create metadata table in the current database

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    """
    if not version(cur):
        cur.execute(
            """
        CREATE TABLE metadata(
            id INTEGER PRIMARY KEY DEFAULT 1 CHECK (id=1),
            version TEXT NOT NULL DEFAULT '{}')
        """.format(
                SCHEMA_VERSION
            )
        )
        cur.execute("INSERT INTO metadata DEFAULT VALUES")


def create_mesh_tables(cur, project_SRID=None):
    """create tables for mesh (noeuds and mailles)
    The spatial index of maille may be corrupted by inserts/update

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param project_SRID: EPSG code
    :type project_SRID: string
    """
    create_metadata_table(cur)
    _execute_statements(cur, "create_mesh_tables.sql", project_SRID=project_SRID)


def create_site_tables(cur, with_cid=False, project_SRID=None):
    """create site tables without cid and sid columns (only one site and one simulation

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param with_cid: cid option, create a cid column in tables
    :type with_cid: bool
    :param project_SRID: EPSG code
    :type project_SRID: string
    """
    create_metadata_table(cur)
    if not project_SRID:
        project_SRID = int(DEFAULT_SRID)
    with open(os.path.join(os.path.dirname(__file__), "create_site_tables.sql")) as f:
        for statement in f.read().split(";;"):
            statement = statement.replace("SRID", str(project_SRID))
            statement = (
                statement
                if with_cid
                else statement.replace(
                    "    cid INTEGER REFERENCES simulations(id) ON UPDATE CASCADE ON DELETE CASCADE,",
                    "",
                ).replace(
                    "    sid INTEGER NOT NULL REFERENCES sites(id) ON UPDATE CASCADE ON DELETE CASCADE,",
                    "",
                )
            )
            cur.execute(statement)


def create_injection_triggers(cur):
    """Create injection triggers in the current database

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    """
    logger.notice("create_injection_triggers")
    _execute_statements(cur, "create_injection_triggers.sql")


def delete_injection_triggers(cur):
    """Delete injection triggers in the current database

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    """
    cur.execute("DROP TRIGGER IF EXISTS maj_injections_update")
    cur.execute("DROP TRIGGER IF EXISTS maj_injections_insert")


def has_second_milieu(cur):
    """Check if the current database is made for dual-porosity simulation

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    """
    return cur.execute("SELECT COUNT(1) FROM noeuds_second_milieu").fetchone()[0] > 0


def _create_result_tables(cur, project_SRID=None):
    """Create result tables in the current database

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param project_SRID: EPSG code
    :type project_SRID: string
    """
    if not has_second_milieu(cur):
        cur.execute(
            """
            CREATE TABLE parametres_simulation(
                id INTEGER PRIMARY KEY,
                wc REAL NOT NULL,
                vga REAL NOT NULL,
                vgn REAL NOT NULL,
                vgr REAL NOT NULL,
                vgs REAL NOT NULL,
                vge REAL NOT NULL DEFAULT 0,
                wt REAL NOT NULL,
                vm REAL NOT NULL,
                dk REAL NOT NULL,
                sl REAL NOT NULL,
                dlzns REAL NOT NULL,
                dlzs REAL NOT NULL)"""
        )
    else:

        cur.execute(
            """
            CREATE TABLE parametres_simulation(
                id INTEGER PRIMARY KEY REFERENCES parametres_simulation(id) ON DELETE CASCADE ON UPDATE CASCADE,
                ashape REAL NOT NULL,
                bshape REAL NOT NULL,
                cshape REAL NOT NULL,
                pl REAL NOT NULL,
                wc REAL NOT NULL,
                vga REAL NOT NULL,
                vgn REAL NOT NULL,
                vgr REAL NOT NULL,
                vgs REAL NOT NULL,
                vge REAL NOT NULL,
                wt REAL NOT NULL,
                vm REAL NOT NULL,
                dk REAL NOT NULL,
                sl REAL NOT NULL,
                dlzns REAL NOT NULL,
                dlzs REAL NOT NULL,
                wc2 REAL NOT NULL,
                vga2 REAL NOT NULL,
                vgn2 REAL NOT NULL,
                vgr2 REAL NOT NULL,
                vgs2 REAL NOT NULL,
                vge2 REAL NOT NULL,
                wt2 REAL NOT NULL,
                vm2 REAL NOT NULL,
                dk2 REAL NOT NULL,
                sl2 REAL NOT NULL,
                dlzns2 REAL NOT NULL,
                dlzs2 REAL NOT NULL
                )"""
        )

    # ratio_surface is the fraction of mesh/node surface used
    # ratio is the fraction of the mesh/node surface used on the total injection surface
    cur.execute(
        """
        CREATE TABLE noeuds_injections(
           noeud INTEGER NOT NULL REFERENCES noeuds(OGC_FID) ON DELETE CASCADE,
           injection INTEGER NOT NULL REFERENCES injections(OGC_FID) ON DELETE CASCADE,
           ratio_surface REAL,
           ratio REAL)"""
    )
    cur.execute(
        """
        CREATE INDEX noeuds_injections_injection_idx ON noeuds_injections(injection)"""
    )
    cur.execute(
        """
        CREATE TABLE mailles_injections(
           maille INTEGER NOT NULL REFERENCES mailles(OGC_FID) ON DELETE CASCADE,
           injection INTEGER NOT NULL REFERENCES injections(OGC_FID) ON DELETE CASCADE,
           ratio_surface REAL,
           ratio REAL)"""
    )
    cur.execute(
        """
        CREATE INDEX mailles_injections_injection_idx ON mailles_injections(injection)"""
    )
    create_injection_triggers(cur)

    _execute_statements(cur, "create_result_tables.sql", project_SRID=project_SRID)


def delete_results(cur):
    """Delete result tables in the current database

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    """
    cur.execute("PRAGMA foreign_keys = OFF")
    cur.execute("DROP TABLE IF EXISTS isovaleur")
    cur.execute("DROP TABLE IF EXISTS bilan_masse")
    cur.execute("DROP TABLE IF EXISTS mailles_zns")
    cur.execute("DROP TABLE IF EXISTS idx_mailles_zns_GEOMETRY")
    cur.execute("DROP TABLE IF EXISTS noeuds_zns")
    cur.execute("DROP TABLE IF EXISTS idx_noeuds_zns_GEOMETRY")
    cur.execute(
        "DELETE FROM geometry_columns WHERE f_table_name='noeuds_zns' OR f_table_name='mailles_zns' OR f_table_name='isovaleur'"
    )
    cur.execute("DROP INDEX IF EXISTS idx_noeuds_zns_GEOMETRY")
    cur.execute("DROP INDEX IF EXISTS idx_mailles_zns_GEOMETRY")
    cur.execute("DROP TABLE IF EXISTS dates_simulation")
    cur.execute("COMMIT")
    cur.execute("VACUUM")
    # delete bynary files
    project_SRID = str(
        cur.execute(
            "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
        ).fetchone()[0]
    )
    basename = cur.execute("PRAGMA database_list").fetchone()[2][:-7]
    for col in [
        "concentration",
        "potentiel",
        "debit",
        "saturation",
        "vitesse_darcy_norme",
        "activity",
    ]:
        for binary in [
            basename + "." + col + ".npy",
            basename + "." + col + ".insat.npy",
            basename + ".s" + col + ".npy",
        ]:
            if os.path.exists(binary):
                os.remove(binary)
    _execute_statements(cur, "create_result_tables.sql", project_SRID=project_SRID)


def delete_mesh(cur, test=False):
    """Delete mesh data in the current database

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    """
    basename = cur.execute("PRAGMA database_list").fetchone()[2][:-7]
    for column in ["concentration", "potentiel", "darcy"]:
        if os.path.exists(basename + "." + column):
            os.remove(basename + "." + column)
    # dcur.execute("PRAGMA foreign_keys = ON") # would probably be slower
    if not test:
        delete_results(cur)
    cur.execute("DELETE FROM mailles")
    cur.execute("DELETE FROM noeuds_contour")
    cur.execute("DELETE FROM potentiel_impose")
    cur.execute("DELETE FROM noeuds")
    cur.execute("COMMIT")
    cur.execute("VACUUM")


def _columns(cur, table, exclude=[]):
    """Return columns in the requested table

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param table: table name
    :type table: string
    :param exclude: columns to exclude
    :type exclude: list

    :return: table columns
    :rtype: list
    """
    return [
        c
        for [dum0, c, dum1, dum2, dum3, dum4] in cur.execute(
            "pragma table_info('%s')" % (table)
        ).fetchall()
        if not c in exclude
    ]


def _import_site(src, dst, siteName=None, project_SRID=None, exp_db=None):
    """import one site in an empty database, return site id

    :param src: source database path
    :type src: string
    :param dst: destination database path
    :type dst: string
    :param siteName: site name
    :type siteName: string
    :param project_SRID: EPSG code
    :type project_SRID: string

    :return: site id
    :rtype: string
    """
    if not project_SRID:
        project_SRID = 2154

    cur = dst.cursor()
    cur.execute("PRAGMA foreign_keys = ON")
    destination = cur.execute("PRAGMA database_list").fetchone()[2]

    source = src.execute("PRAGMA database_list").fetchone()[2]

    # attach src to dst to enable cross-db select
    if source not in [r[2] for r in cur.execute("PRAGMA database_list").fetchall()]:
        cur.execute('ATTACH "' + source + '" AS src')

    # check that no site exist in destination
    if cur.execute("SELECT COUNT(1) FROM main.sites").fetchone()[0]:
        raise RuntimeError("Un site existe deja dans %s" % (source))

    (sid,) = cur.execute(
        "SELECT id FROM src.sites WHERE nom = '%s'" % (siteName)
    ).fetchone()

    cur.execute(
        "INSERT INTO main.sites(%(cols)s) SELECT %(cols)s \
            FROM src.sites WHERE src.sites.id = %(sid)d"
        % {"cols": ",".join(_columns(cur, "sites")), "sid": sid}
    )

    cur.execute(
        "INSERT INTO main.elements_chimiques(%(cols)s) SELECT %(cols)s \
            FROM src.elements_chimiques"
        % {"cols": ",".join(_columns(cur, "elements_chimiques"))}
    )

    imported = False
    if exp_db:
        imported = _import_site_from_exp_db(exp_db, cur, sid, project_SRID, siteName)
    if not imported:
        cur.execute(
            "INSERT INTO main.points_interet(%(cols_out)s) SELECT %(cols_in)s \
                FROM src.points_interet WHERE src.points_interet.sid = %(sid)d"
            % {
                "cols_out": ",".join(_columns(cur, "points_interet", ["sid"])),
                "cols_in": ",".join(_columns(cur, "points_interet", ["sid"])).replace(
                    "GEOMETRY", "ST_Transform(GEOMETRY,{})".format(str(project_SRID))
                ),
                "sid": sid,
            }
        )

        cur.execute(
            "INSERT INTO main.mesures(%(cols)s) SELECT %(cols)s \
                FROM src.mesures \
                WHERE src.mesures.pid IN (SELECT OGC_FID FROM main.points_interet)"
            % {"cols": ",".join(_columns(cur, "mesures"))}
        )

        cur.execute(
            "INSERT INTO main.forages(%(cols_out)s) SELECT %(cols_in)s \
                FROM src.forages \
                WHERE src.forages.sid = %(sid)d"
            % {
                "cols_out": ",".join(_columns(cur, "forages", ["sid"])),
                "cols_in": ",".join(_columns(cur, "forages", ["sid"]))
                .replace(
                    "GEOMETRY", "ST_Transform(GEOMETRY,{})".format(str(project_SRID))
                )
                .replace("POINT", "ST_Transform(POINT,{})".format(str(project_SRID))),
                "sid": sid,
            }
        )

        cur.execute(
            "INSERT INTO main.fracturations(%(cols_out)s) SELECT %(cols_in)s \
                FROM src.fracturations \
                WHERE src.fracturations.forage IN (SELECT OGC_FID FROM main.forages)"
            % {
                "cols_out": ",".join(_columns(cur, "fracturations")),
                "cols_in": ",".join(_columns(cur, "fracturations")).replace(
                    "GEOMETRY", "ST_Transform(GEOMETRY,{})".format(str(project_SRID))
                ),
            }
        )

        cur.execute(
            "INSERT INTO main.stratigraphies(%(cols_out)s) SELECT %(cols_in)s \
                FROM src.stratigraphies \
                WHERE src.stratigraphies.forage IN (SELECT OGC_FID FROM main.forages)"
            % {
                "cols_out": ",".join(_columns(cur, "stratigraphies")),
                "cols_in": ",".join(_columns(cur, "stratigraphies")).replace(
                    "GEOMETRY", "ST_Transform(GEOMETRY,{})".format(str(project_SRID))
                ),
            }
        )

        cur.execute(
            "INSERT INTO main.lithologies(%(cols_out)s) SELECT %(cols_in)s \
                FROM src.lithologies \
                WHERE src.lithologies.forage IN (SELECT OGC_FID FROM main.forages)"
            % {
                "cols_out": ",".join(_columns(cur, "lithologies")),
                "cols_in": ",".join(_columns(cur, "lithologies")).replace(
                    "GEOMETRY", "ST_Transform(GEOMETRY,{})".format(str(project_SRID))
                ),
            }
        )

    cur.execute("UPDATE main.geometry_columns_statistics set last_verified = 0")
    cur.execute("SELECT UpdateLayerStatistics('main.points_interet')")
    return sid


def _import_site_from_exp_db(exp_db, cur, sid, project_SRID, name, site_db_maj=False):
    try:
        with psycopg2.connect(exp_db) as pg_con:
            pg_cur = pg_con.cursor()
            # name is used for exp_db sync
            pg_cur.execute(
                "SELECT id from qgis.site where name='{}'".format(name.lower())
            )
            site_id = pg_cur.fetchone()
            if not site_db_maj:
                if cur.execute("SELECT count(1) FROM domaines").fetchone()[0] == 0:
                    contours = cur.execute(
                        "SELECT AsText(CASTtoXY(GUNION(ST_BuildArea(GEOMETRY)))) from contours"
                    ).fetchone()[0]
                    print(contours)
                    cur.execute(
                        "INSERT INTO domaines(GEOMETRY) VALUES (GeomFromText('{countours}',{srid}))".format(
                            countours=contours, srid=project_SRID
                        )
                    )
                    cur.execute("COMMIT")
                cur.execute("SELECT AsText(GUNION(GEOMETRY)) as GEOMETRY from domaines")
                domaines = cur.fetchone()[0]
                if site_id:
                    site_id = site_id[0]
                    condition = "target.site_id={site_id} AND \
                                st_Contains(st_GeomFromText('{domaines}', \
                                                            {srid}), \
                                            st_transform(target.point, {srid}) \
                                            )".format(
                        site_id=site_id, domaines=domaines, srid=project_SRID
                    )
                else:
                    print("No {} site in experimental database".format(name))
                    return
            else:
                if site_id:
                    site_id = site_id[0]
                    condition = "target.site_id={site_id}".format(site_id=site_id)
                else:
                    print("No {} site in experimental database".format(name))
                    return
            pg_cur.execute(
                "WITH station_union_ids AS (SELECT DISTINCT station_id \
                                  FROM measure.groundwater_level WHERE measure_value IS NOT null \
                                  UNION ALL \
                                  SELECT DISTINCT station_id \
                                  FROM measure.manual_groundwater_level WHERE measure_value IS NOT null \
                                  UNION ALL \
                                  SELECT DISTINCT station_id FROM measure.volumic_concentration where measure_value is not null), \
                            station_ids AS (SELECT DISTINCT station_id FROM station_union_ids) \
                            SELECT target.id, target.name, 'calcul' as station_type, \
                            target.ground_altitude, st_asText(st_transform(target.point,{srid})) \
                            FROM qgis.station as target, station_ids csi WHERE target.station_type not in  ('DrainWell', 'Smokestack') AND \
                            target.id = csi.station_id AND {condition} \
                            UNION ALL \
                            SELECT target.id, target.name, 'puits_sables' as station_type, \
                            target.ground_altitude, st_asText(st_transform(target.point,{srid})) \
                            FROM qgis.station as target WHERE target.station_type = 'DrainWell' AND {condition} \
                            UNION ALL \
                            SELECT target.id, target.name, 'cheminée'  as station_type, \
                            target.ground_altitude, st_asText(st_transform(target.point,{srid})) \
                            FROM qgis.station as target WHERE target.station_type = 'Smokestack' AND {condition} ".format(
                    srid=project_SRID, condition=condition
                )
            )
            stations = pg_cur.fetchall()
            station_ids = [str(s[0]) for s in stations]

            cur.execute("SELECT id, nom FROM elements_chimiques")
            chem_list = ",".join(
                ["(" + str(i) + ",'" + str(j) + "')" for i, j in cur.fetchall()]
            )

            if station_ids:
                pg_cur.execute(
                    "WITH chemical_ids as (SELECT cid, chemical from (VALUES {chem_list}) AS chem (cid, chemical)) \
                                SELECT m.station_id as pid, m.measure_time as date, \
                                m.measure_legend as type, cid.cid as eid, \
                                m.measure_value as concentration, m.measure_uncertainty as incertitude_concentration, \
                                null as potentiel, null as incertitude_potentiel \
                                FROM qgis.measure_volumic_concentration m, chemical_ids cid \
                                WHERE m.station_id in ({station_ids}) AND m.measure_value is not NULL AND \
                                (m.radionuclide = cid.chemical OR m.chemical_element = cid.chemical) \
                                UNION ALL \
                                SELECT m.station_id as pid, m.measure_time as date, \
                                m.measure_legend as type, null as eid, \
                                null as concentration, null as incertitude_concentration, \
                                m.measure_value as potentiel, null as incertitude_potentiel \
                                FROM qgis.measure_groundwater_level m \
                                WHERE m.station_id in ({station_ids}) AND m.measure_value IS NOT NULL \
                                ".format(
                        chem_list=chem_list, station_ids=",".join(station_ids)
                    )
                )
                measures = pg_cur.fetchall()
            pg_cur.execute(
                "WITH borehole_ids AS (SELECT DISTINCT station_id FROM measure.stratigraphic_logvalue) \
                            SELECT s.id, target.name, target.drilling_date, target.station_type, target.location, target.ground_altitude, target.casing_height, target.total_depth, \
                            st_asText(st_transform(ST_SetSRID(ST_MakePoint(st_x(target.point),st_y(target.point), target.ground_altitude),st_srid(target.point)), {srid})), \
                            st_asText(st_transform(ST_SetSRID(ST_MakeLine( \
                                                                    ST_MakePoint(st_x(target.point),st_y(target.point), target.ground_altitude), \
                                                                    ST_MakePoint(st_x(target.point),st_y(target.point), target.ground_altitude-target.total_depth)\
                                                                         ), \
                                                             st_srid(target.point))\
                                                        , {srid})) \
                            FROM qgis.borehole target, qgis.station s, borehole_ids b \
                            WHERE target.name = s.name AND target.ground_altitude=s.ground_altitude AND s.id = b.station_id AND ".format(
                    srid=project_SRID
                )
                + condition
            )
            boreholes = pg_cur.fetchall()
            boreholes_ids = [str(s[0]) for s in boreholes]
            if boreholes_ids:
                pg_cur.execute(
                    "SELECT m.station_id as forage, \
                                cast( lower(m.depth) as float), cast(upper(m.depth) as float), \
                                m.formation_code, m.formation_description, \
                                st_asText(st_transform(ST_SetSRID(ST_MakeLine( \
                                                                        ST_MakePoint(st_x(b.point),st_y(b.point), b.ground_altitude-lower(m.depth)), \
                                                                        ST_MakePoint(st_x(b.point),st_y(b.point), b.ground_altitude-upper(m.depth)) \
                                                                             ), \
                                                                 st_srid(b.point))\
                                                            , {srid})) \
                                FROM qgis.measure_stratigraphic_logvalue m, qgis.borehole b, qgis.station s where m.station_id in ({ids}) and \
                                                                                                                  m.station_id=s.id and \
                                                                                                                  b.ground_altitude=s.ground_altitude and \
                                                                                                                  s.name=b.name and \
                                                                                                                  m.formation_code is not null".format(
                        srid=project_SRID, ids=",".join(boreholes_ids)
                    )
                )
                stratigraphies = pg_cur.fetchall()
                pg_cur.execute(
                    "SELECT m.station_id as forage, \
                                cast( lower(m.depth) as float), cast(upper(m.depth) as float), \
                                'usgs'||m.rock_code, m.rock_description, \
                                st_asText(st_transform(ST_SetSRID(ST_MakeLine( \
                                                                        ST_MakePoint(st_x(b.point),st_y(b.point), b.ground_altitude-lower(m.depth)), \
                                                                        ST_MakePoint(st_x(b.point),st_y(b.point), b.ground_altitude-upper(m.depth)) \
                                                                             ), \
                                                                 st_srid(b.point))\
                                                            , {srid})) \
                                FROM qgis.measure_stratigraphic_logvalue m, qgis.borehole b, qgis.station s where m.station_id in ({ids}) and \
                                                                                                                  m.station_id=s.id and \
                                                                                                                  b.ground_altitude=s.ground_altitude and \
                                                                                                                  s.name=b.name and \
                                                                                                                  m.rock_code is not null".format(
                        srid=project_SRID, ids=",".join(boreholes_ids)
                    )
                )
                lithologies = pg_cur.fetchall()
                pg_cur.execute(
                    "SELECT m.station_id as forage, \
                                cast( lower(m.depth) as float), cast(upper(m.depth) as float), \
                                m.value, \
                                st_asText(st_transform(ST_SetSRID(ST_MakeLine( \
                                                                        ST_MakePoint(st_x(b.point),st_y(b.point), b.ground_altitude-lower(m.depth)), \
                                                                        ST_MakePoint(st_x(b.point),st_y(b.point), b.ground_altitude-upper(m.depth)) \
                                                                             ), \
                                                                 st_srid(b.point))\
                                                            , {srid})) \
                                FROM qgis.measure_fracturing_rate m, qgis.borehole b, qgis.station s where m.station_id in ({ids}) and \
                                                                                                           m.station_id=s.id and \
                                                                                                           b.ground_altitude=s.ground_altitude and \
                                                                                                           s.name=b.name and \
                                                                                                           m.value is not null".format(
                        srid=project_SRID, ids=",".join(boreholes_ids)
                    )
                )
                fracturations = pg_cur.fetchall()

        cur.executemany(
            "INSERT INTO points_interet(sid, OGC_FID, nom, groupe, altitude, GEOMETRY) VALUES (?,?,?,?,?, GeomFromText(?, {srid}))".format(
                srid=project_SRID
            ),
            [[sid] + list(s) for s in stations],
        )
        if station_ids:
            cur.executemany(
                "INSERT INTO mesures(pid,date,type,eid,concentration, incertitude_concentration, potentiel, incertitude_potentiel) VALUES (?,?,?,?,?,?,?,?)",
                measures,
            )
        if boreholes_ids:
            cur.executemany(
                "INSERT INTO forages(sid,OGC_FID,nom,date_realisation,type,localisation,zsol,h_tube_sol,profondeur,POINT, GEOMETRY) VALUES (?,?,?,?,?,?,?,?,?,GeomFromText(?,{srid}),GeomFromText(?,{srid}))".format(
                    srid=project_SRID
                ),
                [[sid] + list(b) for b in boreholes],
            )
            cur.executemany(
                "INSERT INTO stratigraphies(forage,debut,fin,code,formation,GEOMETRY) VALUES (?,?,?,?,?,GeomFromText(?,{srid}))".format(
                    srid=project_SRID
                ),
                stratigraphies,
            )
            cur.executemany(
                "INSERT INTO lithologies(forage,debut,fin,code,roche,GEOMETRY) VALUES (?,?,?,?,?,GeomFromText(?,{srid}))".format(
                    srid=project_SRID
                ),
                lithologies,
            )
            cur.executemany(
                "INSERT INTO fracturations(forage,debut,fin,taux,GEOMETRY) VALUES (?,?,?,?,GeomFromText(?,{srid}))".format(
                    srid=project_SRID
                ),
                fracturations,
            )
        if name:
            print("{} site imported".format(name))
        return True
    except psycopg2.OperationalError:
        return False


def export_simulation(src, dst):
    """export the simulation in src to the site database

    :param src: source database path
    :type src: string
    :param dst: destination database path
    :type dst: string
    """
    cur = dst.cursor()
    # check available id for simulation
    (new_id,) = cur.execute(
        """
        SELECT COALESCE(MAX(id)+1, 1) FROM simulations
        """
    ).fetchone()

    # update source simulation id
    cur = src.cursor()
    cur.execute("PRAGMA foreign_keys = ON")
    cur.execute(
        """
        UPDATE simulations SET id={}
        """.format(
            new_id
        )
    )
    # mise à jour des paramètres FR181120
    (nombre_de_simulations,) = cur.execute(
        "SELECT nombre_de_simulations FROM simulations"
    ).fetchone()
    if nombre_de_simulations == 1:
        names = cur.execute("PRAGMA table_info(parametres_simulation)").fetchall()
        param_names = [name[1].upper() for name in names][1:]
        param_values = cur.execute(
            "SELECT %s FROM parametres_simulation ORDER BY id"
            % (", ".join(param_names))
        ).fetchall()[0]
        cur.executemany(
            "UPDATE parametres SET valeur = ? WHERE nom = ?",
            list(zip(param_values, param_names)),
        )

    src.commit()

    cur = dst.cursor()

    source = src.execute("PRAGMA database_list").fetchone()[2]

    if source not in [r[2] for r in cur.execute("PRAGMA database_list").fetchall()]:
        cur.execute('ATTACH "' + source + '" AS src')

    # nom de la simulation
    names = [x[0] for x in cur.execute("SELECT nom FROM main.simulations").fetchall()]
    nom_simulation = cur.execute("SELECT nom FROM src.simulations").fetchone()[0]
    if nom_simulation in names:
        logger.notice("A simulation with the same name exists and will be replaced")
        cur.execute("DELETE FROM main.simulations WHERE nom='" + nom_simulation + "'")

    # sid du site
    siteName = (
        cur.execute("SELECT nom_db_maillage FROM src.simulations")
        .fetchone()[0]
        .split("_")[0]
    )
    sid = cur.execute(
        "SELECT * FROM main.sites where nom='" + siteName + "'"
    ).fetchone()[0]

    cur.execute(
        """INSERT INTO main.simulations(id, sid, eid, nom, nom_db_maillage, debut, nombre_de_simulations,
                    coef_maillage, pas_echant_zns, rapport_max_permeabilite, permeabilite_zns_uniforme,
                    type_injection, type_infiltration, inverse, insature)
                SELECT id, '{}', eid, nom, nom_db_maillage, debut, nombre_de_simulations,
                    coef_maillage, pas_echant_zns, rapport_max_permeabilite, permeabilite_zns_uniforme,
                    type_injection, type_infiltration, inverse, insature
                FROM src.simulations""".format(
            sid
        )
    )

    # ajout de l'identifiant de simulation FR181119
    cur.execute(
        "INSERT INTO main.dates_resultats(%(cols_dst)s) SELECT %(cid)s, %(cols_src)s \
            FROM src.dates_resultats"
        % {
            "cols_dst": ",".join(_columns(cur, "dates_resultats", ["id"])),
            "cid": str(new_id),
            "cols_src": ",".join(_columns(cur, "dates_resultats", ["id", "cid"])),
        }
    )

    cur.execute(
        "INSERT INTO main.parametres(%(cols_dst)s) SELECT %(cid)s, %(cols_src)s \
            FROM src.parametres"
        % {
            "cols_dst": ",".join(_columns(cur, "parametres", ["id"])),
            "cid": str(new_id),
            "cols_src": ",".join(_columns(cur, "parametres", ["id", "cid"])),
        }
    )

    cur.execute(
        "INSERT INTO main.metis_options(%(cols)s) SELECT %(cols)s \
            FROM src.metis_options"
        % {"cols": ",".join(_columns(cur, "metis_options"))}
    )

    cur.execute(
        "INSERT INTO main.openfoam_options(%(cols)s) SELECT %(cols)s \
            FROM src.openfoam_options"
        % {"cols": ",".join(_columns(cur, "openfoam_options"))}
    )

    cur.execute(
        "INSERT INTO main.injections(%(cols_dst)s) SELECT %(cid)s, %(cols_src)s \
            FROM src.injections"
        % {
            "cols_dst": ",".join(_columns(cur, "injections", ["OGC_FID"])),
            "cid": str(new_id),
            "cols_src": ",".join(
                _columns(cur, "injections", ["OGC_FID", "cid", "GEOMETRY"])
                + ["ST_Transform(GEOMETRY,4326)"]
            ),
        }
    )

    cur.execute("UPDATE main.geometry_columns_statistics set last_verified = 0")
    cur.execute("SELECT UpdateLayerStatistics('main.injections')")
    dst.commit()
    logger.notice("Simulation", nom_simulation, " exportée")

def ref_value(v, vref):
    return v if v else vref

def _import_simulation(src, dst, simulationName, project_SRID=None):
    """import one simulation in a database that contains one site

    :param src: source database path
    :type src: string
    :param dst: destination database path
    :type dst: string
    :param simulationName: simulation name
    :type simulationName: string
    :param project_SRID: EPSG code
    :type project_SRID: string
    """
    if not project_SRID:
        project_SRID = 2154
    # cur = DebugCursor(dst.cursor())
    cur = dst.cursor()
    cur.execute("PRAGMA foreign_keys = ON")
    destination = cur.execute("PRAGMA database_list").fetchone()[2]

    source = src.execute("PRAGMA database_list").fetchone()[2]

    # attach src to dst to enable cross-db select
    if source not in [r[2] for r in cur.execute("PRAGMA database_list").fetchall()]:
        cur.execute('ATTACH "' + source + '" AS src')

    # check that no simulation exist in destination
    if cur.execute("SELECT COUNT(1) FROM main.simulations").fetchone()[0]:
        raise RuntimeError("Une simulation existe deja dans %s" % (source))

    (sid,) = cur.execute("SELECT id FROM main.sites").fetchone()
    # only one simulation
    if cur.fetchone():
        raise ProjectCreationError("Only one simulation must be defined in database.")

    cur.execute(
        "INSERT INTO main.simulations(%(cols)s) SELECT %(cols)s \
            FROM src.simulations \
            WHERE src.simulations.sid = %(sid)d \
            AND src.simulations.nom = '%(simulationName)s'"
        % {
            "cols": ",".join(_columns(cur, "simulations", ["sid"])),
            "simulationName": simulationName,
            "sid": sid,
        }
    )

    (cid,) = cur.execute("SELECT id FROM main.simulations").fetchone()
    # only one simulation
    if cur.fetchone():
        raise ProjectCreationError("Only one simulation must be defined in database.")

    cur.execute(
        "INSERT INTO main.metis_options(%(cols)s) SELECT %(cols)s \
            FROM src.metis_options \
            WHERE src.metis_options.id = %(cid)d"
        % {"cols": ",".join(_columns(cur, "metis_options")), "cid": cid}
    )

    cur.execute(
        "INSERT INTO main.openfoam_options(%(cols)s) SELECT %(cols)s \
            FROM src.openfoam_options \
            WHERE src.openfoam_options.id = %(cid)d"
        % {"cols": ",".join(_columns(cur, "openfoam_options")), "cid": cid}
    )

    cur.execute(
        "INSERT INTO main.dates_resultats(%(cols)s) SELECT %(cols)s \
            FROM src.dates_resultats WHERE src.dates_resultats.cid = %(cid)d"
        % {"cols": ",".join(_columns(cur, "dates_resultats", ["cid"])), "cid": cid}
    )

    for nom, valeur, valeur_min, valeur_max, loi_de_variation in cur.execute(
        """
            SELECT nom, valeur, valeur_min, valeur_max, loi_de_variation
            FROM  src.parametres
            WHERE src.parametres.cid = %(cid)d
            """
        % {"cid": cid}
    ).fetchall():
        cur.execute(
            """
            UPDATE main.parametres SET
            valeur = {},
            valeur_min = {},
            valeur_max = {},
            loi_de_variation = '{}'
            WHERE nom = '{}'
            """.format(
                valeur, ref_value(valeur_min, valeur), ref_value(valeur_max, valeur), loi_de_variation, nom
            )
        )

    (nombre_de_simulations,) = cur.execute(
        "SELECT nombre_de_simulations FROM simulations"
    ).fetchone()
    create_latin_hypercube(cur, nombre_de_simulations)

    cur.execute(
        "INSERT INTO main.injections(%(cols_out)s) SELECT %(cols_in)s \
            FROM src.injections \
            WHERE src.injections.cid = %(cid)d"
        % {
            "cols_out": ",".join(_columns(cur, "injections", ["cid"])),
            "cols_in": ",".join(_columns(cur, "injections", ["cid"])).replace(
                "GEOMETRY", "ST_Transform(GEOMETRY,{})".format(str(project_SRID))
            ),
            "cid": cid,
        }
    )

    cur.execute("UPDATE main.geometry_columns_statistics set last_verified = 0")
    cur.execute("SELECT UpdateLayerStatistics('main.injections')")
    dst.commit()


def simulation_mesh(cur, simulation):
    """Get mesh name from simulation table

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param simulation: simulation name
    :type simulation: string

    :return: nom_db_maillage
    :rtype: string
    """
    return cur.execute(
        "SELECT nom_db_maillage FROM simulations WHERE nom='{}'".format(simulation)
    ).fetchone()[0]


def create_computation_database(
    database, mesh_db=None, simulation_name=None, exp_db=None
):
    """Create a computation database from a mesh site

    :param database: database path
    :type database: string
    :param mesh_db: mesh database path
    :type mesh_db: string
    :param simulation_name: simulation name
    :type simulation_name: string
    """
    if database[-7:] != ".sqlite":
        raise ProjectCreationError("Database filename suffix must be .sqlite")
    if os.path.exists(database):
        raise ProjectCreationError(f"File {database} already exists.")
    logger.debug("database is", mesh_db)

    settings = Settings(os.path.dirname(os.path.abspath(database)))
    site_db = settings.value("General", "defaultSite")
    check_and_upgrade(site_db)
    mesh_dir = settings.value("General", "defaultMeshDir")

    with sqlite.connect(site_db, "database:create_computation_database:src") as src:
        mesh_db = (
            mesh_db
            if mesh_db is not None
            else os.path.join(mesh_dir, simulation_mesh(src.cursor(), simulation_name))
        )
        if not os.path.exists(mesh_db):
            raise ProjectCreationError(f"File {mesh_db} must exist.")
        check_and_upgrade(mesh_db)

        shutil.copyfile(mesh_db, database)
        mesh_name = os.path.split(mesh_db)[1]
        with sqlite.connect(
            database, "database:create_computation_database:dst"
        ) as dst:
            project_SRID = str(
                dst.cursor()
                .execute(
                    "SELECT srid FROM geometry_columns WHERE f_table_name LIKE 'noeuds'"
                )
                .fetchone()[0]
            )
            create_site_tables(dst.cursor(), project_SRID=project_SRID)
            _create_result_tables(dst.cursor(), project_SRID=project_SRID)

            site_name = None
            for [site, id_] in src.execute("SELECT nom, id FROM sites").fetchall():
                if re.match("^%s(.*).mesh.sqlite" % (site), os.path.split(mesh_db)[1]):
                    site_name = site
                    site_id = id_
            if not site_name:
                raise ProjectCreationError("Not site defined in database.")
            sid = _import_site(
                src, dst, site_name, project_SRID=project_SRID, exp_db=exp_db
            )

            if simulation_name is not None:
                _import_simulation(src, dst, simulation_name, project_SRID=project_SRID)
                dst.commit()
                cur = dst.cursor()
                (nombre_de_simulations,) = cur.execute(
                    "SELECT nombre_de_simulations FROM simulations"
                ).fetchone()
                create_latin_hypercube(cur, nombre_de_simulations)
            else:
                logger.debug("project : inserting simulation")
                # setup create new simulation
                cur = dst.cursor()
                cur.execute(
                    "INSERT INTO simulations(nom, nom_db_maillage) VALUES (\
                        '%s', '%s')"
                    % (site_name, mesh_name)
                )
                cur.execute("INSERT INTO dates_resultats(duree) VALUES ('10 year')")
                cur.execute("""INSERT INTO metis_options DEFAULT VALUES""")
                cur.execute("""INSERT INTO openfoam_options DEFAULT VALUES""")
                logger.debug("project : latin_hypercube")
                create_latin_hypercube(cur)
            dst.commit()


def create_latin_hypercube(cur, nombre_de_simulations=1):
    """Use latin hypercube to create n simulations

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param nombre_de_simulations: n simulations
    :type nombre_de_simulations: integer
    """
    pas = 1.0 / nombre_de_simulations

    cur.execute("SELECT COUNT(1) FROM parametres")
    nombre_de_facteurs = cur.fetchone()[0]

    values = [
        [None for i in range(nombre_de_facteurs)] for j in range(nombre_de_simulations)
    ]
    noms = []
    cur.execute(
        """SELECT nom, valeur, valeur_min, valeur_max, loi_de_variation
                   FROM parametres ORDER BY nom"""
    )

    if nombre_de_simulations > 1:
        for i, [nom, valeur, valeur_min, valeur_max, loi_de_variation] in enumerate(
            cur.fetchall()
        ):
            noms.append(nom)

            if (
                loi_de_variation == "loguniform" or loi_de_variation == "lognormal"
            ) and (valeur_min <= 0 or valeur_max <= 0):
                raise RuntimeError(
                    "Les valeurs min ou max ne peuvent être nulles avec une loi de variation log"
                )

            r = [float(random.random()) for useless in range(nombre_de_simulations)]
            iper = [j for j in range(nombre_de_simulations)]
            if loi_de_variation != "constant":
                # random permutations
                for j in range(len(iper)):
                    jr = int(floor(len(iper) * random.random()))
                    iper[j], iper[jr] = iper[jr], iper[j]

            for j in range(nombre_de_simulations):
                x = pas * (r[j] + j)
                k = iper[j]
                if loi_de_variation == "constant":
                    values[j][i] = 0.5 * (valeur_max + valeur_min)
                elif loi_de_variation == "uniform":
                    values[k][i] = valeur_min + x * (valeur_max - valeur_min)
                elif loi_de_variation == "loguniform":
                    values[k][i] = exp_(
                        log(valeur_min) + x * (log(valeur_max) - log(valeur_min))
                    )
                elif loi_de_variation == "normal":
                    x = 0.001 + 0.998 * pas * (r[j] + j)
                    mean = 0.5 * (valeur_max + valeur_min)
                    sigma = (valeur_max - valeur_min) / 6.18
                    values[k][i] = mean + sqrt(2) * sigma * erfinv(2 * x - 1)
                elif loi_de_variation == "lognormal":
                    x = 0.001 + 0.998 * pas * (r[j] + j)
                    mean = 0.5 * (log(valeur_max) + log(valeur_min))
                    sigma = (log(valeur_max) - log(valeur_min)) / 6.18
                    values[k][i] = exp_(mean + sqrt(2) * sigma * erfinv(2 * x - 1))
                else:
                    raise RuntimeError(
                        "Type de distibution '%s' inconnu" % loi_de_variation
                    )
    else:
        for i, [nom, valeur, valeur_min, valeur_max, loi_de_variation] in enumerate(
            cur.fetchall()
        ):
            noms.append(nom)
            values[0][i] = valeur

    cur.execute("DELETE FROM parametres_simulation")
    cur.executemany(
        """
        INSERT INTO parametres_simulation(%s) VALUES(%s)"""
        % (",".join(noms), ",".join(["?"] * len(noms))),
        values,
    )
    cur.execute(
        "UPDATE simulations SET nombre_de_simulations=%d" % (nombre_de_simulations)
    )


def database_type(cur):
    """return database type

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor

    :return: db_type
    :rtype: string
    """
    db_type = None
    if not mesh_tables_exists(cur):
        db_type = "site"
    elif parametres_simulation_exists(cur):
        db_type = "comp"
    else:
        db_type = "mesh"
    return db_type


def copy_table(scur, dcur, table):
    if (
        dcur.execute(
            """
            SELECT count(1) FROM src.sqlite_master WHERE type='table' AND name='{table}'
            """.format(
                table=table
            )
        ).fetchone()[0]
        == 1
    ):
        dcur.execute("DELETE FROM main.{table}".format(table=table))
        sql = """
            INSERT INTO main.{table}({cols}) SELECT {cols} FROM src.{table}
            """.format(
            table=table,
            cols=", ".join(
                list(
                    set(_columns(dcur, table)).intersection(
                        set(_columns(scur, table))
                    )
                )
            ),
        )
        logger.notice("update table ", table)
        dcur.execute(sql)


def upgrade_to_last(src, dst, project_SRID=None):
    """upgrade database to last schema version,
    raise exception if not possible
    do nothing if the version is already OK

    :param src: source database path
    :type src: string
    :param dst: destination database path
    :type dst: string
    :param project_SRID: EPSG code
    :type project_SRID: string
    """
    # test the type of db (mesh, site, or computation)
    scur = src.cursor()
    db_type = database_type(scur)

    dcur = dst.cursor()
    dcur.execute("SELECT InitSpatialMetaData(1)")
    create_metadata_table(dcur)
    source = src.execute("PRAGMA database_list").fetchone()[2]
    dcur.execute('ATTACH "' + source + '" AS src')

    if db_type == "mesh" or db_type == "comp":
        if not project_SRID:
            project_SRID = str(
                scur
                .execute(
                    "SELECT srid FROM geometry_columns WHERE f_table_name LIKE 'noeuds'"
                )
                .fetchone()[0]
            )
        create_mesh_tables(dcur, project_SRID=project_SRID)
        for table in MESH_TABLES:
            copy_table(scur, dcur, table)

    if db_type == "site" or db_type == "comp":
        if not project_SRID:
            project_SRID = str(
                scur
                .execute(
                    "SELECT srid FROM geometry_columns WHERE f_table_name LIKE 'injections'"
                )
                .fetchone()[0]
            )
        create_site_tables(dcur, with_cid=(db_type == "site"), project_SRID=project_SRID)
        for table in SITE_TABLES:
            copy_table(scur, dcur, table)

    if db_type == "comp":
        _create_result_tables(dcur, project_SRID=project_SRID)
        delete_injection_triggers(dcur)
        for table in COMP_TABLES:
            copy_table(scur, dcur, table)

        create_injection_triggers(dcur)

    dst.commit()


def check_and_upgrade(database):
    """check the database current schema version,
    and upgrade it if the version is old

    :param database: database path
    :type database: string
    """
    with sqlite.connect(database, "database:check_and_upgrade") as src:
        src_version = version(src.cursor())
        if src_version == SCHEMA_VERSION_240119:
            upgrade_from_240119_to_240802(database)
        elif src_version != SCHEMA_VERSION:
            upgrade(database)

def upgrade_from_240119_to_240802(database):
    """Remove WM parameter from database

    :param database: database path
    :type database: string
    """
    logger.notice("convert ", database, " to version ", SCHEMA_VERSION, " (removing WM)")
    with sqlite.connect(database, "database:upgrade_from_240119_to_240802") as src:
        scur = src.cursor()
        db_type = database_type(scur)
        scur.execute("DELETE FROM parametres WHERE nom='WM'")
        scur.execute("UPDATE metadata SET version='{}'".format(SCHEMA_VERSION))
        src.commit()

def upgrade(database):
    """upgrade database to current schema version,
    raise exception if not possible
    do nothing if the version is already OK

    :param database: database path
    :type database: string
    """

    if database[-7:] != ".sqlite":
        raise DatabaseUpgradeError("Database filename suffix must be .sqlite")
    dst_name = database[:-7] + ".new.sqlite"
    if os.path.exists(dst_name):
        os.remove(dst_name)

    with sqlite.connect(database, "database:upgrade:src") as src:
        with sqlite.connect(dst_name, "database:upgrade:dst") as dst:
            logger.notice("convert ", database, " to version ", SCHEMA_VERSION)
            upgrade_to_last(src, dst)

    old_name = database[:-7] + ".old.sqlite"
    os.rename(database, old_name)
    os.rename(dst_name, database)


def table_exists(database, table):
    with sqlite.connect(database) as conn:
        return table in [
            t[0]
            for t in conn.cursor()
            .execute(
                """SELECT tbl_name
            FROM sqlite_master WHERE type = 'table'"""
            )
            .fetchall()
        ]

def add_second_milieu(database):
    """Copy existing table to create a dual-porosity simulation

    :param database: database path
    :type database: string
    """

    with sqlite.connect(database, "database:add_second_milieu") as conn:
        cur = conn.cursor()
        db_type = database_type(cur)

        if db_type not in ["mesh", "comp"]:
            raise SecondMilieuCreationError(
                f"Initialization database type must be mesh or comp. {db_type} found."
            )

        if has_second_milieu(cur):
            logger.error("{} a deja des noeuds pour le second milieu", database)
            conn.close()
            return

        cur.execute(
            """
            INSERT INTO noeuds_second_milieu(OGC_FID, potentiel_reference, potentiel,
                flux_eau, epaisseur_zs, epaisseur_zns,
                epaisseur_aquifere, permeabilite_x, permeabilite_y, permeabilite_zns,
                infiltration)
            SELECT OGC_FID, potentiel_reference, potentiel,
                flux_eau, epaisseur_zs, epaisseur_zns,
                epaisseur_aquifere, permeabilite_x, permeabilite_y, permeabilite_zns,
                infiltration
            FROM noeuds
            """
        )

        cur.execute(
            """
            INSERT INTO mailles_second_milieu(OGC_FID, permeabilite_x, permeabilite_y, v_x, v_y, v_norme)
            SELECT OGC_FID, permeabilite_x, permeabilite_y, v_x, v_y, v_norme
            FROM mailles
            """
        )

        if db_type == "comp":
            cur.execute("DROP TABLE parametres_simulation")
            cur.execute(
                """
                CREATE TABLE parametres_simulation(
                    id INTEGER PRIMARY KEY REFERENCES parametres_simulation(id) ON DELETE CASCADE ON UPDATE CASCADE,
                    ashape REAL NOT NULL,
                    bshape REAL NOT NULL,
                    cshape REAL NOT NULL,
                    pl REAL NOT NULL,
                    wc REAL NOT NULL,
                    vga REAL NOT NULL,
                    vgn REAL NOT NULL,
                    vgr REAL NOT NULL,
                    vgs REAL NOT NULL,
                    vge REAL NOT NULL,
                    wt REAL NOT NULL,
                    vm REAL NOT NULL,
                    dk REAL NOT NULL,
                    sl REAL NOT NULL,
                    dlzns REAL NOT NULL,
                    dlzs REAL NOT NULL,
                    wc2 REAL NOT NULL,
                    vga2 REAL NOT NULL,
                    vgn2 REAL NOT NULL,
                    vgr2 REAL NOT NULL,
                    vgs2 REAL NOT NULL,
                    vge2 REAL NOT NULL,
                    wt2 REAL NOT NULL,
                    vm2 REAL NOT NULL,
                    dk2 REAL NOT NULL,
                    sl2 REAL NOT NULL,
                    dlzns2 REAL NOT NULL,
                    dlzs2 REAL NOT NULL
                    )"""
            )
            cur.execute(
                """
                INSERT INTO simulations_second_milieu(id, permeabilite_zns_uniforme)
                SELECT id, permeabilite_zns_uniforme FROM simulations
                """
            )
            cur.execute(
                """
                INSERT INTO injections_second_milieu(OGC_FID, permeabilite, infiltration, epaisseur)
                SELECT OGC_FID, permeabilite, infiltration, epaisseur FROM injections
                """
            )
            cur.execute(
                """
                INSERT INTO parametres(nom, valeur, valeur_min, valeur_max, loi_de_variation)
                SELECT nom||'2', valeur, valeur_min, valeur_max, loi_de_variation FROM parametres
                """
            )
            cur.execute("INSERT INTO parametres(nom, valeur) VALUES ('ASHAPE', 15.)")
            cur.execute("INSERT INTO parametres(nom, valeur) VALUES ('BSHAPE', 3.)")
            cur.execute("INSERT INTO parametres(nom, valeur) VALUES ('CSHAPE', .4)")
            cur.execute("INSERT INTO parametres(nom, valeur) VALUES ('PL', 1.e-4)")

        conn.commit()

