"""
Point tool for the crisis interface
"""


from PyQt5.QtCore import Qt, pyqtSignal
from qgis.core import QgsPoint
from qgis.gui import QgsMapTool


class PointTool(QgsMapTool):
    """PointTool, declined from QgsMapTool"""

    pointClicked = pyqtSignal(QgsPoint)
    rightClicked = pyqtSignal(QgsPoint)

    def __init__(self, canvas):
        """Constructor

        :param canvas: qgis interface canvas
        :type canvas: QgsMapCanvas
        """
        super(PointTool, self).__init__(canvas)
        self.setCursor(Qt.CrossCursor)

    def canvasMoveEvent(self, event):
        """Triggered when the mouse is moved on canvas

        :param event: mouse event
        :type event: QEvent
        """
        x = event.pos().x()
        y = event.pos().y()

        point = self.canvas().getCoordinateTransform().toMapCoordinates(x, y)

    def canvasPressEvent(self, event):
        """Triggered when the mouse is used on canvas

        :param event: mouse event
        :type event: QEvent
        """
        # Get the click
        x = event.pos().x()
        y = event.pos().y()
        point = self.canvas().getCoordinateTransform().toMapCoordinates(x, y)
        if event.button() == Qt.RightButton:
            self.rightClicked.emit(QgsPoint(point.x(), point.y()))
        else:
            self.pointClicked.emit(QgsPoint(point.x(), point.y()))

    def isZoomTool(self):
        """Overwrite QgsMapTool function
        to tell PointTool is not a zoom tool"""
        return False

    def isTransient(self):
        """Overwrite QgsMapTool function
        to tell PointTool is a zoom tool"""
        return True

    def isEditTool(self):
        """Overwrite QgsMapTool function
        to tell PointTool is a transient (one shoot) tool"""
        return True
