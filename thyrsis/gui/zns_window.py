import os
from builtins import str

from PyQt5.QtPrintSupport import QPrinter
from qgis.core import QgsDataSourceUri
from qgis.PyQt import uic
from qgis.PyQt.QtCore import Qt
from qgis.PyQt.QtGui import QIcon, QImage, QPainter
from qgis.PyQt.QtSvg import QSvgGenerator
from qgis.PyQt.QtWidgets import QFileDialog, QMainWindow, QMenu, QToolBar, QToolButton

from ..meshlayer.meshlayerpropertydialog import MeshLayerPropertyDialog
from ..utilities import complete_image_name
from .result_control import ResultControl
from .zns_scene import ZnsScene


class ZnsWindow(QMainWindow):
    """Main window for zns with menu and toolbar"""

    def __init__(self, uri, logger, parent=None):
        """Constructor

        :param uri: source
        :type uri: string
        :param logger: logger object
        :type logger: Logger
        :param parent: Qt parent
        :type parent: QtWidget
        """
        super(ZnsWindow, self).__init__(parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "zns.ui"), self)

        self.__uri = QgsDataSourceUri(uri)
        self.__logger = logger

        self.scene = ZnsScene(uri, self)
        self.scene.set_legend_placement("left")
        self.graphicsView.setScene(self.scene)

        self.toolbar = QToolBar()
        self.addToolBar(self.toolbar)
        # self.toolbar.addAction(QIcon(complete_image_name('print.svg')), "test").triggered.connect(self.__printZns)
        self.toolbar.addAction(
            QIcon.fromTheme("document-save", QIcon(complete_image_name("print.svg"))),
            "save_image",
        ).triggered.connect(self.__printZns)
        self.toolbar.addWidget(ResultControl(self.scene.dataProvider()))

        self.options = QToolButton()
        self.toolbar.addWidget(self.options)
        self.options.setText(self.tr("Options"))
        self.options.setPopupMode(QToolButton.InstantPopup)
        menu = QMenu()
        self.options.setMenu(menu)
        self.autoAdjustAction = menu.addAction(self.tr("Automatic scale update"))
        self.autoAdjustAction.setCheckable(True)
        self.autoAdjustAction.triggered.connect(self.__autoUpdate)
        self.options.setArrowType(Qt.NoArrow)
        menu.addAction(self.tr("Legend")).triggered.connect(self.__showLegend)
        submenu = menu.addMenu(self.tr("Legend location"))
        submenu.addAction(self.tr("Left")).triggered.connect(self.__legendLeft)
        submenu.addAction(self.tr("Right")).triggered.connect(self.__legendRight)
        submenu.addAction(self.tr("Below")).triggered.connect(self.__legendBottom)

    def __del__(self):
        if self.scene.glMesh() != None:
            if self.scene.glMesh()._GlMesh__gl_ctx:
                self.scene.glMesh()._GlMesh__gl_ctx.makeCurrent(
                    self.scene.glMesh()._GlMesh__gl_surface
                )
            if self.scene.colorLegend().tex != None:
                self.scene.colorLegend().tex.destroy()

    def __autoUpdate(self, useless):
        """Set automatic scale update

        :param useless: useless
        :type flag: None
        """
        self.scene.setAutoUpdateMinMax(self.autoAdjustAction.isChecked())

    def __showLegend(self, useless):
        """Show the legend

        :param useless: useless
        :type flag: None
        """
        MeshLayerPropertyDialog(self.scene).exec_()

    def __legendLeft(self, useless):
        """Place the legend on the left

        :param useless: useless
        :type flag: None
        """
        self.scene.set_legend_placement("left")

    def __legendRight(self, useless):
        """Place the legend on the right

        :param useless: useless
        :type flag: None
        """
        self.scene.set_legend_placement("right")

    def __legendBottom(self, useless):
        """Place the legend on the bottom

        :param useless: useless
        :type flag: None
        """
        self.scene.set_legend_placement("bottom")

    def __printZns(self):
        """Export the unsaturated column"""
        fil, __ = QFileDialog.getSaveFileName(
            None,
            self.tr("Save the plot ..."),
            os.path.dirname(os.path.abspath(str(self.__uri.database()))),
            self.tr("Image files (*.png *.svg *.bmp *.jpg *.pdf)"),
        )

        if not fil:
            return

        img = None
        if not "." in fil:
            fil = fil + ".png"
        if fil[-4:] == ".svg":
            img = QSvgGenerator()
            img.setFileName(fil)
            rect = self.graphicsView.scene().sceneRect()
            img.setSize(rect.size().toSize())
            # img.setViewBox(rect)
            img.setTitle("ZNS")
        elif fil[-4:] == ".pdf":
            img = QPrinter()
            img.setOutputFileName(fil)
            img.setOutputFormat(QPrinter.PdfFormat)
        elif fil[-4:] in [".png", ".jpg", ".bmp"]:
            img = QImage(
                self.graphicsView.scene().sceneRect().size().toSize(),
                QImage.Format_ARGB32,
            )
            img.fill(Qt.transparent)
        else:
            self.__logger.pushWarning(
                "Warning", self.tr("Export file must have a recognized extension")
            )
            return

        with QPainter(img) as p:
            self.graphicsView.scene().render(p)

        if fil[-4:] not in [".svg", ".pdf"]:  # those are saved directly
            img.save(fil)


if __name__ == "__main__":
    import sys

    from qgis.PyQt.QtWidgets import QApplication

    from thyrsis.gui.time_control import TimeControl
    from thyrsis.log import ConsoleLogger

    QApplication.setOrganizationName("QGIS")
    QApplication.setOrganizationDomain("qgis.org")
    QApplication.setApplicationName("QGIS2")

    app = QApplication(sys.argv)

    dbname = sys.argv[1]

    win = ZnsWindow("dbname=" + dbname + " resultColumn=concentration", ConsoleLogger())
    win.show()
    ctrl = TimeControl(win.scene.dataProvider().dates())
    ctrl.timeSlider.valueChanged.connect(win.scene.dataProvider().setDate)
    win.toolbar.addWidget(ctrl)
    win.resize(600, 600)

    app.exec_()

    win.setParent(None)
    del win
