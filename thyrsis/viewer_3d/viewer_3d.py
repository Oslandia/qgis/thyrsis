from math import ceil, log

from OpenGL import GLU
from OpenGL.GL import (
    GL_COLOR_BUFFER_BIT,
    GL_DEPTH_BUFFER_BIT,
    GL_DEPTH_TEST,
    GL_MODELVIEW,
    GL_PROJECTION,
    glClear,
    glClearColor,
    glEnable,
    glLoadIdentity,
    glMatrixMode,
    glViewport,
)
from PyQt5.QtCore import QSize
from PyQt5.QtGui import QColor, QOpenGLFramebufferObject, QVector3D
from PyQt5.QtOpenGL import QGLFormat, QGLWidget
from PyQt5.QtWidgets import QApplication, QDockWidget, QMainWindow

from .camera import Camera
from .scene import Scene
from .viewer_controls import ViewerControls

# from qgis.gui import *
# from qgis.core import *


class Viewer3d(QGLWidget):
    """3D widget"""

    # def __del__(self):
    #    print "Viewer3d.__del__", self

    def __init__(
        self, dataProvider, colorLegend, znsDataProvider, znsLegend, parent=None
    ):
        """Constructor

        :param dataProvider:  data provider
        :type dataProvider: MeshDataProvider
        :param colorLegend: ColorLegend
        :type colorLegend: ColorLegend
        :param znsDataProvider:  unsaturated zone data provider
        :type znsDataProvider: znsDataProvider
        :param znsLegend: color legend
        :type znsLegend: ColorLegend
        :param parent: widget parent
        :type parent: QWidget
        """
        super(Viewer3d, self).__init__(parent)
        self.scene = Scene(dataProvider, colorLegend, znsDataProvider, znsLegend, self)

        self.scene.setTerrainColor(QColor(0, 255, 0, 128))

        at = self.scene.center
        ext_y = self.scene.extent[3] - self.scene.extent[1]
        eye = at + QVector3D(0, -1.5 * ext_y, 0.5 * ext_y)
        self.camera = Camera(eye, at)

        dataProvider.dataChanged.connect(self.update)
        self.scene.changed.connect(self.update)
        self.destroyed.connect(self.cleanTextures)

    def cleanTextures(self):
        """Clean 3D mesh texture to avoid memory leak"""
        self.makeCurrent()
        if self.scene.bottomMesh().colorLegend().tex:
            self.scene.bottomMesh().colorLegend().tex.destroy()
        if self.scene.topMesh().colorLegend().tex:
            self.scene.topMesh().colorLegend().tex.destroy()
        for znsColumn in self.scene.znsColumns():
            if znsColumn.colorLegend().tex:
                znsColumn.colorLegend().tex.destroy()

    def initializeGL(self):
        """Initialize the OpenGL scene"""
        self.makeCurrent()
        self.scene.initializeGL(self.bindTexture)

    def resizeGL(self, width, height):
        """Resize GL widget

        :param width: width
        :type width: float
        :param height: height
        :type height: float
        """
        height = 1 if not height else height
        glViewport(0, 0, width, height)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        GLU.gluPerspective(45.0, float(width) / height, 100, 100000)
        glMatrixMode(GL_MODELVIEW)

    def paintGL(self, context=None, camera=None):
        """OpenGL rendering

        :param context: openGL context
        :type context: QOpenGLContext
        :param camera: 3D camera
        :type camera: Camera
        """
        context = context or self
        glClearColor(1.0, 1.0, 1.0, 1.0)
        glEnable(GL_DEPTH_TEST)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        c = camera or self.camera
        dist = (c.at - c.eye).length()
        GLU.gluPerspective(
            45.0, float(context.width()) / context.height(), 0.1 * dist, 10 * dist
        )

        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        GLU.gluLookAt(
            c.eye.x(),
            c.eye.y(),
            c.eye.z(),
            c.at.x(),
            c.at.y(),
            c.at.z(),
            c.up.x(),
            c.up.y(),
            c.up.z(),
        )

        leftv = QVector3D.crossProduct(c.up, c.at - c.eye).normalized()
        upv = QVector3D.crossProduct(c.at - c.eye, leftv).normalized()
        self.scene.rendergl(leftv, upv, c.eye, context.height(), context)

    def image(self, size):
        """Export the 3D image view

        :param size: size
        :type size: QSize
        """
        # we want all the scene in image, regardless of aspect ratio
        zoom_out = max(
            float(self.width()) / size.width(), float(self.height()) / size.height()
        )
        camera = Camera(self.camera.eye, self.camera.at, self.camera.up)
        # camera.eye = camera.at + (camera.eye-camera.at)*zoom_out
        w, h = size.width(), size.height()
        roundupSz = QSize(pow(2, ceil(log(w) / log(2))), pow(2, ceil(log(h) / log(2))))
        fmt = QGLFormat()
        # fmt.setAlpha(True)
        context = QOpenGLFramebufferObject(roundupSz, fmt)
        context.makeCurrent()
        self.scene.initializeGL(context.bindTexture)
        self.scene.requireShaderRecompile()
        self.paintGL(context, camera)
        img = context.toImage()
        context.doneCurrent()

        return img.copy(
            0.5 * (roundupSz.width() - w), 0.5 * (roundupSz.height() - h), w, h
        )

    def mouseMoveEvent(self, event):
        """Triggered on mouse move event

        :param event: event description
        :type event: QEvent
        """
        delta_x = float(event.x() - self.oldx) / self.width()
        delta_y = float(self.oldy - event.y()) / self.height()
        self.camera.move(delta_x, delta_y, event.buttons(), event.modifiers())
        self.oldx = event.x()
        self.oldy = event.y()
        self.update()

    def mousePressEvent(self, event):
        """Triggered on mouse press event

        :param event: event description
        :type event: QEvent
        """
        self.oldx = event.x()
        self.oldy = event.y()

    def keyPressEvent(self, event):
        """Triggered on key press event

        :param event: event description
        :type event: QEvent
        """
        if event.key() == Qt.Key_0:
            self.camera.reset()
            self.update()

    def wheelEvent(self, event):
        """Triggered on mouse wheel use, zoom

        :param event: QEvent
        :type event: QEvent
        """
        dt = -event.angleDelta().y() / 100.0
        self.camera.wheel_zoom(dt)
        self.update()


class ViewerWindow(QMainWindow):
    """3D main window"""

    def __init__(self, dataProvider, legend, znsDataProvider, znsLegend, parent=None):
        """Constructor

        :param dataProvider:  data provider
        :type dataProvider: MeshDataProvider
        :param legend: ColorLegend
        :type legend: ColorLegend
        :param znsDataProvider:  unsaturated zone data provider
        :type znsDataProvider: znsDataProvider
        :param znsLegend: color legend
        :type znsLegend: ColorLegend
        :param parent: widget parent
        :type parent: QWidget
        """
        super(ViewerWindow, self).__init__(parent)
        self.resize(900, 400)
        self.legend = legend
        self.viewer = Viewer3d(dataProvider, legend, znsDataProvider, znsLegend, self)
        self.viewer.show()
        self.setCentralWidget(self.viewer)
        self.controls = QDockWidget(self)
        self.controls.setWidget(ViewerControls(self.viewer, self))

        self.addDockWidget(Qt.RightDockWidgetArea, self.controls)


if __name__ == "__main__":
    import sys

    from PyQt5.QtGui import *

    from thyrsis.gui.zns_scene import ZnsDataProvider
    from thyrsis.meshlayer.glmesh import ColorLegend
    from thyrsis.spatialitemeshdataprovider import SpatialiteMeshDataProvider

    app = QApplication(sys.argv)

    QCoreApplication.setOrganizationName("QGIS")
    QCoreApplication.setApplicationName("QGIS2")

    assert len(sys.argv) >= 2

    column = sys.argv[2] if len(sys.argv) > 2 else None
    uri = (
        "dbname="
        + sys.argv[1]
        + " crs=epsg:27572"
        + (" resultColumn=" + column if column else "")
    )
    dataProvider = SpatialiteMeshDataProvider(uri)
    znsDataProvider = ZnsDataProvider(uri)
    legend = ColorLegend()
    znsLegend = ColorLegend()
    w = Viewer3d(dataProvider, legend, znsDataProvider, znsLegend)
