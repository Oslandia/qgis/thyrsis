import os

from qgis.PyQt import uic
from qgis.PyQt.QtWidgets import QWidget

from thyrsis.log import logger


class DurationEditWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        uic.loadUi(
            os.path.dirname(os.path.realpath(__file__)) + "/duration_edit_widget.ui",
            self,
        )

        self.duration_type_combobox.addItem(self.tr("year"), "year")
        self.duration_type_combobox.addItem(self.tr("month"), "month")
        self.duration_type_combobox.addItem(self.tr("day"), "day")
        self.duration_type_combobox.addItem(self.tr("hour"), "hour")
        self.duration_type_combobox.addItem(self.tr("minute"), "minute")
        self.duration_type_combobox.addItem(self.tr("second"), "second")

    def set_duration(self, duration: str) -> None:
        vals = duration.split(" ")
        if len(vals) != 2:
            logger.warning(self.tr("Invalid duration format : {}").format(duration))
        else:
            val = float(vals[0])
            self.value_spinbox.setValue(val)
            duration_type = vals[1]
            if self.tr("year") == duration_type or "year" == duration_type:
                self.duration_type_combobox.setCurrentText(self.tr("year"))
            if self.tr("month") == duration_type or "month" == duration_type:
                self.duration_type_combobox.setCurrentText(self.tr("month"))
            if self.tr("day") == duration_type or "day" == duration_type:
                self.duration_type_combobox.setCurrentText(self.tr("day"))
            if self.tr("hour") == duration_type or "hour" == duration_type:
                self.duration_type_combobox.setCurrentText(self.tr("hour"))
            if self.tr("minute") == duration_type or "minute" == duration_type:
                self.duration_type_combobox.setCurrentText(self.tr("minute"))
            if self.tr("second") == duration_type or "second" == duration_type:
                self.duration_type_combobox.setCurrentText(self.tr("second"))

    def get_duration(self) -> str:
        return (
            f"{self.value_spinbox.value()} {self.duration_type_combobox.currentData()}"
        )
