PRAGMA foreign_keys = ON;;

CREATE TABLE sites(
    id INTEGER PRIMARY KEY,
    nom TEXT UNIQUE NOT NULL);;

CREATE TABLE elements_chimiques(
    id INTEGER PRIMARY KEY,
    nom TEXT NOT NULL,
    periode REAL,
    masse_atomique REAL,
    decroissance_radioactive REAL,
    activite_specifique REAL NOT NULL DEFAULT 0,
    limite_qualite_eau REAL NOT NULL DEFAULT 0,
    numero_atomique INTEGER,
    nombre_de_masse INTEGER);;

CREATE TABLE simulations(
    id INTEGER PRIMARY KEY,
    sid INTEGER NOT NULL REFERENCES sites(id) ON UPDATE CASCADE ON DELETE CASCADE,
    eid INTEGER REFERENCES elements_chimiques(id) ON UPDATE CASCADE ON DELETE CASCADE,
    nom TEXT UNIQUE NOT NULL,
    nom_db_maillage TEXT,
    debut DATETIME DEFAULT CURRENT_TIMESTAMP, -- contains units in format YY-MM-DD HH:MM:SS
    nombre_de_simulations INTEGER DEFAULT 1,
    coef_maillage REAL DEFAULT 5.,
    pas_echant_zns REAL DEFAULT 5,
    rapport_max_permeabilite REAL DEFAULT 10,
    permeabilite_zns_uniforme REAL,
    type_injection TEXT DEFAULT 'aucune' CHECK (type_injection IN ('aucune', 'masse', 'concentration', 'flux')),
    type_infiltration TEXT DEFAULT 'permanente' CHECK (type_infiltration IN ('permanente', 'transitoire')),
    inverse TEXT DEFAULT 'non' CHECK (inverse IN ('oui', 'non')),
    insature TEXT DEFAULT 'oui' CHECK (insature IN ('oui', 'non')));;

CREATE TABLE simulations_second_milieu(
    id INTEGER PRIMARY KEY REFERENCES simulations(id) ON UPDATE CASCADE ON DELETE CASCADE,
    permeabilite_zns_uniforme);;

CREATE TABLE IF NOT EXISTS parametres(
    id INTEGER PRIMARY KEY,
    cid INTEGER REFERENCES simulations(id) ON UPDATE CASCADE ON DELETE CASCADE,
    nom TEXT,
    valeur REAL,
    valeur_min REAL,
    valeur_max REAL,
    loi_de_variation TEXT CONSTRAIN loi_de_variation_cstr DEFAULT 'constant'
        CHECK (loi_de_variation IN ('constant', 'normal', 'lognormal',
                                    'uniform', 'loguniform')));;

CREATE TABLE metis_options(
    id INTEGER PRIMARY KEY REFERENCES simulations(id) ON UPDATE CASCADE ON DELETE CASCADE,
    niter_gc_zns INTEGER DEFAULT 100,
    niter_gc_zs INTEGER DEFAULT 100,
    saturation_initiale REAL DEFAULT 0.8,
    rmult_cormax REAL DEFAULT 0.1,
    c_pstep REAL DEFAULT 0.1,
    c_ps_inc REAL DEFAULT 0.1,
    c_ps_tol REAL DEFAULT 1.e-5,
    ratio_max_res REAL DEFAULT 1.e-6,
    critereres REAL DEFAULT 1.e-5,
    niterns INTEGER DEFAULT 1000,
    courant TEXT DEFAULT 'non' CHECK (courant IN ('oui', 'non')),
    emmagasinement REAL DEFAULT 1.e-3,
    kdecoup_delt_zns REAL DEFAULT 10.,
    kdecoup_dmin_zns REAL DEFAULT 1.,
    kdecoup_delt_zs REAL DEFAULT 100.,
    kdecoup_dmin_zs REAL DEFAULT 1.,
    force_fin_phase TEXT DEFAULT 'oui' CHECK (force_fin_phase IN ('oui', 'non')),
    max_iter INTEGER DEFAULT 200,
    relax_phreat REAL DEFAULT 0.5,
    critere_pot REAL DEFAULT 0.1,
    phrea_min REAL DEFAULT 1.0);;

CREATE TABLE openfoam_options(
    id INTEGER PRIMARY KEY REFERENCES simulations(id) ON UPDATE CASCADE ON DELETE CASCADE,
    delta_time_init REAL DEFAULT 10.,
    h_tolerance REAL DEFAULT 1.e-12,
    h_tolerance_relative REAL DEFAULT 0.01,
    h_relaxationFactor REAL DEFAULT 0.01,
    h_relaxationFactor_transient REAL DEFAULT 0.2,
    picard_tolerance REAL DEFAULT 1.e-08,
    picard_tolerance_transient REAL DEFAULT 1.e-01,
    picard_maxiter INTEGER DEFAULT 20,
    picard_maxiter_transient INTEGER DEFAULT 3,
    newton_tolerance REAL DEFAULT 1.e-08,
    newton_maxiter INTEGER DEFAULT 20,
    dtfact_decrease REAL DEFAULT 0.8,
    truncationError REAL DEFAULT 0.01,
    fluid_density REAL DEFAULT 1.e3,
    dynamic_viscosity REAL DEFAULT 1.e-3,
    molecular_diffusion REAL DEFAULT 0.,
    tortuosity REAL DEFAULT 1.,
    potential_tolerance REAL DEFAULT 1.e-9,
    potential_tolerance_relative REAL DEFAULT 0.1,
    potential_residualControl REAL DEFAULT 1.e-8,
    potential_relaxationFactor REAL DEFAULT 0.1,
    hwatermin REAL DEFAULT 0.1,
    specific_storage REAL DEFAULT 0.
    );;

CREATE TABLE dates_resultats(
    id INTEGER PRIMARY KEY,
    cid INTEGER REFERENCES simulations(id) ON UPDATE CASCADE ON DELETE CASCADE,
    idx INTEGER,
    duree TEXT default '10 year', -- contains units e.g. '33 days'
    pas_de_temps TEXT DEFAULT '1 year'-- contains units '1 year'
    );;

CREATE TABLE injections(
    OGC_FID INTEGER PRIMARY KEY,
    cid INTEGER REFERENCES simulations(id) ON UPDATE CASCADE ON DELETE CASCADE,
    profondeur REAL DEFAULT 0,
    debut DATETIME DEFAULT CURRENT_TIMESTAMP, -- contains units in format YY-MM-DD HH:MM:SS
    duree TEXT, -- contains units e.g. '33 days'
    nom TEXT,
    volume_eau REAL DEFAULT 0,
    coefficient_surface REAL DEFAULT 1.,
    coefficient_injection REAL DEFAULT 1.,
    permeabilite REAL,
    infiltration REAL DEFAULT 0,
    epaisseur REAL DEFAULT 0,
    altitude REAL DEFAULT 0,
    input REAL);;
SELECT AddGeometryColumn('injections', 'GEOMETRY', SRID, 'MULTIPOLYGON', 'XY');;
SELECT CreateSpatialIndex('injections', 'GEOMETRY');;

CREATE TABLE injections_second_milieu(
    OGC_FID INTEGER PRIMARY KEY REFERENCES injections(OGC_FID) ON UPDATE CASCADE ON DELETE CASCADE,
    permeabilite REAL,
    infiltration REAL DEFAULT 0,
    epaisseur REAL DEFAULT 0);;

CREATE TABLE points_interet(
    OGC_FID INTEGER PRIMARY KEY,
    sid INTEGER REFERENCES sites(id) ON UPDATE CASCADE ON DELETE CASCADE,
    altitude REAL,
    altitude_mur REAL,
    nom TEXT,
    groupe TEXT);;
SELECT AddGeometryColumn('points_interet', 'GEOMETRY', SRID, 'POINT', 'XY');;
SELECT CreateSpatialIndex('points_interet', 'GEOMETRY');;

CREATE TABLE mesures(
    id INTEGER PRIMARY KEY,
    pid INTEGER REFERENCES points_interet(OGC_FID) ON UPDATE CASCADE ON DELETE CASCADE,
    date DATETIME,
    type TEXT,
    eid INTEGER REFERENCES elements_chimiques(id) ON UPDATE CASCADE ON DELETE CASCADE,
    concentration REAL,
    incertitude_concentration REAL,
    potentiel REAL,
    incertitude_potentiel REAL,
    source INTEGER -- should be a ref to a table with measurement campains
    );;
CREATE INDEX mesures_pid_idx ON mesures(pid);;

CREATE TABLE forages(
        OGC_FID INTEGER PRIMARY KEY,
        sid INTEGER REFERENCES sites(id) ON UPDATE CASCADE ON DELETE CASCADE,
        nom TEXT NOT NULL,
        date_realisation TEXT,
        type TEXT,
        localisation TEXT,
        zsol FLOAT,
        h_tube_sol FLOAT,
        profondeur FLOAT
        );;
SELECT AddGeometryColumn('forages', 'GEOMETRY', SRID, 'LINESTRING', 'XYZ');;
SELECT AddGeometryColumn('forages', 'POINT', SRID, 'POINT', 'XYZ');;

CREATE TABLE stratigraphies(
        OGC_FID INTEGER PRIMARY KEY,
        forage INTEGER NOT NULL REFERENCES forages(OGC_FID) ON UPDATE CASCADE ON DELETE CASCADE,
        debut FLOAT NOT NULL,
        fin FLOAT NOT NULL,
        code TEXT NOT NULL,
        formation TEXT);;
SELECT AddGeometryColumn('stratigraphies', 'GEOMETRY', SRID, 'LINESTRING', 'XYZ');;

CREATE TABLE lithologies(
    OGC_FID INTEGER PRIMARY KEY,
    forage INTEGER NOT NULL REFERENCES forages(OGC_FID) ON UPDATE CASCADE ON DELETE CASCADE,
    debut FLOAT NOT NULL,
    fin FLOAT NOT NULL,
    code TEXT NOT NULL,
    roche TEXT);;
SELECT AddGeometryColumn('lithologies', 'GEOMETRY', SRID, 'LINESTRING', 'XYZ');;

CREATE TABLE fracturations(
        OGC_FID INTEGER PRIMARY KEY,
        forage INTEGER NOT NULL REFERENCES forages(OGC_FID) ON UPDATE CASCADE ON DELETE CASCADE,
        debut FLOAT NOT NULL,
        fin FLOAT NOT NULL,
        taux FLOAT NOT NULL);;
SELECT AddGeometryColumn('fracturations', 'GEOMETRY', SRID, 'LINESTRING', 'XYZ');;
