# Embedded requirements
#
# Python packages required by the plugin and which are not included into the embedded Python in QGIS (mainly Windows).
#
# Typical command to install:
# python -m pip install --no-deps -U -r requirements/embedded.txt -t thyrsis/embedded_external_libs
# -----------------------

# gmsh==4.9.3 # for a next iteration using the pre-wrapped version of Gmsh
mpi4py==3.1.5
rtree==1.3.0
portalocker==2.3.2 ; sys_platform == 'win32'
