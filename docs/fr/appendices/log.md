(stratigraphic_logs_fr)=

# Logs stratigraphiques

Cette annexe présente les différentes codifications utilisées pour la représentation graphique des logs stratigraphiques :

- la codification de l'échelle des temps géologiques du BRGM ({numref}`brgm`),
- le code colorimétrique RGB de la Commission Mondiale des Cartes Géologiques (CGMW) ({numref}`rgb`),
- la codification USGS des figurés ({numref}`usgs1`, {numref}`usgs2`, {numref}`usgs3`).

:::{figure} /_static/images/BRGM_temps_geologiques.png
:name: brgm
Codification BRGM de l'échelle des temps géologiques.
:::

:::{figure} /_static/images/RGB.png
:name: rgb
Codification colorimétrique RGB de l'échelle des temps géologiques, selon CGMW.
:::

:::{figure} /_static/images/USGS1.png
:name: usgs1
Codification des figurés lithologiques, selon USGS (1).
:::

:::{figure} /_static/images/USGS2.png
:name: usgs2
Codification des figurés lithologiques, selon USGS (2).
:::

:::{figure} /_static/images/USGS3.png
:name: usgs3
Codification des figurés lithologiques, selon USGS (3).
:::
