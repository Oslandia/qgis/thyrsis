"""
Meshing with cfMesh

USAGE:
    python -m thyrsis.cfmesh database.sqlite [-t, -c, -v]

OPTIONS

   -t : test
   -r : run inversion
   -v : verbose
"""

import os
import shutil
import sys
from subprocess import PIPE, Popen

from shapely import wkt
from shapely.geometry import LinearRing, LineString, Point, Polygon

from .log import logger
from .simulation.openfoam import OpenFoam


def distance(pt, pf):
    "return the minimal distance from point pt to all points in pf"
    dmin = -1
    for p in pf:
        d = pt.distance(p)
        if dmin < 0 or d < dmin:
            dmin = d
    return dmin


class cfMesh(object):
    """Process class for cfMesh meshing"""

    def __init__(self, database, cur, settings):
        """Constructor

        :param database: sqlite mesh database
        :type database: str
        :param cur: sqlite cursor
        :type cur: cursor object
        :param settings: project settings
        :type settings: ConfigParser object

        """
        self.__database = database
        self.__cur = cur
        self.__settings = settings
        self.__tmpdir = self.mesh_tmpdir()
        self.__project_SRID = self.get_project_SRID()
        logger.notice("project_SRID = ", self.__project_SRID)
        self.__polygon_exterior, self.__polygons_interior = self.get_polygons()
        if not self.__polygon_exterior:
            logger.notice("no exterior polygon found")
            return

        self.__code = OpenFoam(database, settings)
        OpenFoam.create_tree(self.__tmpdir, "steadyGroundwater2DFoam")
        self.create_parameters()
        self.__contour_points = self.get_contour_points()
        self.__fmsFile = self.create_fms()
        self.create_meshDict()
        self.mesh()
        self.read_cfmesh()

    def mesh_tmpdir(self):
        """Creates cfmesh directory

        :return: tempdir
        :rtype: string
        """
        dbname = self.__database
        dir_name = os.path.dirname(os.path.abspath(dbname))
        database = os.path.join(dir_name, dbname)
        tmpdir = os.path.join(dir_name, os.path.basename(dbname)[:-7] + "_cfmesh")
        if os.path.isdir(tmpdir):
            shutil.rmtree(tmpdir)
        os.makedirs(tmpdir)
        logger.debug("tmpdir = ", tmpdir)

        return tmpdir

    def get_project_SRID(self):
        return str(
            self.__cur.execute(
                "SELECT srid FROM geometry_columns WHERE f_table_name = 'contours'"
            ).fetchone()[0]
        )

    def get_lines(self):
        lines = [
            (wkt.loads(geom), longueur_element, epaisseur)
            for geom, longueur_element, epaisseur in self.__cur.execute(
                """
                select st_astext(GEOMETRY), longueur_element, epaisseur from contours"""
            ).fetchall()
        ]
        return lines

    def get_fixed_points(self):
        fixed_points = [
            (wkt.loads(geom), longueur_element, epaisseur)
            for geom, longueur_element, epaisseur in self.__cur.execute(
                """
                select st_astext(GEOMETRY), longueur_element, epaisseur from points_fixes"""
            ).fetchall()
        ]
        logger.notice(len(fixed_points), "fixed points found")
        return fixed_points

    def get_polygons(self):
        polygons = [
            wkt.loads(geom)
            for geom, in self.__cur.execute("select st_astext(GEOMETRY) from domaines")
        ]

        if not len(polygons):
            return False

        polygon_exterior = None
        polygons_interior = []
        if len(polygons) == 1:
            polygon_exterior = polygons[0]
        else:
            for polygon in polygons:
                if len(list(polygon.interiors)) > 0:
                    polygon_exterior = polygon
                else:
                    polygons_interior.append(polygon)

        return polygon_exterior, polygons_interior

    def get_contour_points(self):
        points = []
        for polygon in [self.__polygon_exterior]:
            for ring in [polygon.exterior]:
                for coord in ring.coords:
                    points.append(coord)
        if points[-1][0] == points[0][0] and points[-1][1] == points[0][1]:
            del points[-1]
        return points

    def create_fms(self, zmin=0, zmax=100):
        fmsFile = os.path.join(self.__tmpdir, "geom.fms")
        logger.notice("creating", fmsFile)

        n = len(self.__contour_points)
        with open(fmsFile, "w") as fil:
            fil.write("1(\n")
            fil.write("model empty)\n\n")
            fil.write("%d\n" % (2 * n))
            fil.write("(\n")
            for pt in self.__contour_points:
                fil.write("(%.4f %.3f %d)\n" % (pt[0], pt[1], zmin))
                fil.write("(%.4f %.3f %d)\n" % (pt[0], pt[1], zmax))
            fil.write(")\n\n")

            fil.write("%d\n" % (2 * n))
            fil.write("(\n")
            for ipt in range(n):
                fil.write(
                    "((%d %d %d) 0)\n" % (2 * ipt, 2 * ipt + 1, (2 * ipt - 2) % (2 * n))
                )
                fil.write(
                    "((%d %d %d) 0)\n"
                    % (2 * ipt + 1, (2 * ipt - 1) % (2 * n), (2 * ipt - 2) % (2 * n))
                )
            fil.write(")\n\n")

            fil.write("%d\n" % (2 * n))
            fil.write("(\n")
            for ipt in range(n):
                fil.write("(%d %d)\n" % (2 * ipt, (2 * ipt + 2) % (2 * n)))
                fil.write("(%d %d)\n" % (2 * ipt + 1, (2 * ipt + 3) % (2 * n)))
            fil.write(")\n\n")

            fil.write("0()\n")
            fil.write("0()\n")
            fil.write("0()\n")

        return fmsFile

    def get_maxCellSize(self):
        longueurs = [
            float(x[0])
            for x in self.__cur.execute(
                "SELECT longueur_element FROM contours"
            ).fetchall()
        ]
        return max(longueurs)

    def create_parameters(self, end_time=1000, write_interval=50):
        parFile = os.path.join(self.__tmpdir, "parameters.txt")
        logger.notice("creating", parFile)

        # openfoam options
        options = self.__code.get_hynverse_options()

        with open(parFile, "w") as fil:

            fil.write("end_time %e;\n" % (end_time))
            fil.write("write_interval %e;\n" % (write_interval))

            fil.write("potential_tolerance %e;\n" % (options["potential_tolerance"]))
            fil.write(
                "potential_tolerance_relative %e;\n"
                % (options["potential_tolerance_relative"])
            )
            fil.write(
                "potential_residualControl %e;\n"
                % (options["potential_residualControl"])
            )
            fil.write(
                "potential_relaxationFactor %e;\n"
                % (options["potential_relaxationFactor"])
            )
            fil.write("hwatermin %f;\n" % (options["hwatermin"]))

            fil.write("fluid_density %e;\n" % (options["fluid_density"]))
            fil.write("dynamic_viscosity %e;\n" % (options["dynamic_viscosity"]))

    def create_meshDict(self, zmin=0, zmax=100):
        mshFile = os.path.join(self.__tmpdir, "system", "meshDict")
        logger.notice("creating", mshFile)
        OpenFoam.entete(
            os.path.join(self.__tmpdir, "system"), "meshDict", "dictionary", "system"
        )
        maxCellSize = self.get_maxCellSize()
        logger.notice("maxCellSize = ", maxCellSize)
        lines = self.get_lines()
        with open(mshFile, "a") as fil:
            fil.write("""surfaceFile "geom.fms";\n""")
            fil.write("maxCellSize %d;\n" % (maxCellSize))

            # polygons_interior
            lines_no_polygons_interior = lines
            for i, p in enumerate(self.__polygons_interior):
                logger.notice("polygon %d" % (i))
                csize = []
                thick = []
                for line in lines:
                    if p.intersects(line[0]):
                        csize.append(line[1])
                        thick.append(line[2] or 10 * line[1])
                        lines_no_polygons_interior.remove(line)
                if not csize:
                    break
                cellSize = sum(csize) / len(csize)
                thickness = sum(thick) / len(thick)
                logger.notice("%d lines intersect polygon %d" % (len(csize), i))
                logger.notice(
                    "cellSize, thickness for polygon %d are %.1f, %.1f"
                    % (i, cellSize, thickness)
                )
                zmean = 0.5 * (zmin + zmax)
                centre = p.centroid
                bounds = p.bounds
                fil.write("objectRefinements\n")
                fil.write("{\n")
                fil.write("  box_%d\n" % (i))
                fil.write("  {\n")
                fil.write("    type                 box;\n")
                fil.write("    cellSize             %.1f;\n" % (cellSize))
                fil.write(
                    "    centre               (%.3f %.3f %.1f);\n"
                    % (centre.x, centre.y, zmean)
                )
                fil.write(
                    "    lengthX              %.1f;\n"
                    % (bounds[2] - bounds[0] + thickness)
                )
                fil.write(
                    "    lengthY              %.1f;\n"
                    % (bounds[3] - bounds[1] + thickness)
                )
                fil.write("    lengthZ              %.1f;\n" % (zmax - zmin))
                fil.write("  }\n")
                fil.write("}\n")

            # lines
            for i, line in enumerate(lines_no_polygons_interior):
                cellSize = line[1]
                thickness = line[2] or 10 * line[1]
                coords = line[0].coords
                logger.notice(
                    "cellSize, thickness for line %d are %.1f, %.1f"
                    % (i, cellSize, thickness)
                )
                for j in range(len(coords) - 1):
                    fil.write("objectRefinements\n")
                    fil.write("{\n")
                    fil.write("  line_%d_%d\n" % (i, j))
                    fil.write("  {\n")
                    fil.write("    type                 line;\n")
                    fil.write("    cellSize             %.1f;\n" % (cellSize))
                    fil.write(
                        "    p0                   (%.3f %.3f %.1f);\n"
                        % (coords[j][0], coords[j][1], zmin)
                    )
                    fil.write(
                        "    p1                   (%.3f %.3f %.1f);\n"
                        % (coords[j + 1][0], coords[j + 1][1], zmin)
                    )
                    fil.write("    refinementThickness  %.1f;\n" % (thickness))
                    fil.write("  }\n")
                    fil.write("}\n")

            # points
            pf = []
            for i, pt in enumerate(self.get_fixed_points()):
                cellSize = pt[1]
                thickness = pt[2] or 10 * pt[1]
                if i > 0 and distance(pt[0], pf) < cellSize:
                    logger.notice("point %d to close of other points, removed" % (i))
                    continue
                pf.append(pt[0])
                logger.notice(
                    "cellSize, thickness for point %d are %.1f, %.1f"
                    % (i, cellSize, thickness)
                )
                fil.write("objectRefinements\n")
                fil.write("{\n")
                fil.write("  point_%d\n" % (i))
                fil.write("  {\n")
                fil.write("    type                 cone;\n")
                fil.write("    cellSize             %.1f;\n" % (cellSize))
                fil.write(
                    "    p0                   (%.3f %.3f %.1f);\n"
                    % (pt[0].x, pt[0].y, zmin)
                )
                fil.write(
                    "    p1                   (%.3f %.3f %.1f);\n"
                    % (pt[0].x, pt[0].y, zmax)
                )
                fil.write("    radius0              %.1f;\n" % (thickness))
                fil.write("    radius1              %.1f;\n" % (thickness))
                fil.write("  }\n")
                fil.write("}\n")

    def mesh(self):
        logger.notice("meshing with cfMesh")
        log_file_path = os.path.join(self.__tmpdir, "cartesian2DMesh.log")
        with open(log_file_path, "w") as logfile:
            self.__code.run_cartesian_2D_mesh(logfile, self.__tmpdir)

    def read_cfmesh(self):
        pointsFile = os.path.join(self.__tmpdir, "constant", "polyMesh", "points")
        logger.notice("reading", pointsFile)
        nodes = OpenFoam.read_openfoam_file(pointsFile, False)
        nodes = [(x[0], x[1]) for x in nodes]

        facesFile = os.path.join(self.__tmpdir, "constant", "polyMesh", "faces")
        logger.notice("reading", facesFile)
        f = OpenFoam.read_openfoam_file(facesFile, False)
        # we extract the number of points at the beginning of each face
        f = [
            [int(str(x[0])[0]), [int(float(str(x[0])[1:]))] + list(map(int, x[1:]))]
            for x in f
        ]

        bdyFile = os.path.join(self.__tmpdir, "constant", "polyMesh", "boundary")
        logger.notice("reading", bdyFile)
        with open(bdyFile, "r") as fil:
            for line in fil:
                if "bottomEmptyFaces" in line:
                    break
            for line in fil:
                if "nFaces" in line:
                    nFaces = int(line.split()[1].split(";")[0])
                    break
            for line in fil:
                if "startFace" in line:
                    startFace = int(line.split()[1].split(";")[0])
                    break
        logger.notice("startFace =", startFace, "nFaces =", nFaces)
        faces = f[startFace : startFace + nFaces]

        triangles = []
        quads = []
        for f in faces:
            if f[0] == 4:
                quads.append(f[1])
            elif f[0] == 5:
                quads.append(f[1][:4])
                triangles.append([f[1][0], f[1][3], f[1][4]])
            elif f[0] == 6:
                quads.append(f[1][:4])
                quads.append([f[1][0], f[1][3], f[1][4], f[1][5]])
            elif f[0] == 7:
                quads.append(f[1][:4])
                quads.append([f[1][0], f[1][3], f[1][4], f[1][5]])
                triangles.append([f[1][0], f[1][5], f[1][6]])

        elements = quads + triangles
        # nodes should be renumeroted
        flatlist = [n for sublist in elements for n in sublist]
        sorted_nodes = list(set(flatlist))
        dic_nodes = {x: i for i, x in enumerate(sorted_nodes)}

        cur = self.__cur
        cur.execute("DROP INDEX IF EXISTS mailles_a_idx")
        cur.execute("DROP INDEX IF EXISTS mailles_b_idx")
        cur.execute("DROP INDEX IF EXISTS mailles_c_idx")
        cur.execute("DROP INDEX IF EXISTS mailles_d_idx")
        cur.execute("DELETE FROM mailles")
        cur.execute("DELETE FROM noeuds")
        cur.execute("DELETE FROM noeuds_contour")

        logger.notice("loading %d nodes" % (len(sorted_nodes)))
        cur.executemany(
            "INSERT INTO noeuds(OGC_FID, GEOMETRY)  VALUES (?, MakePoint(?, ?, {}))".format(
                self.__project_SRID
            ),
            [(dic_nodes[n] + 1, nodes[n][0], nodes[n][1]) for n in sorted_nodes],
        )
        cur.execute("UPDATE geometry_columns_statistics set last_verified = 0")
        cur.execute("SELECT UpdateLayerStatistics('noeuds')")

        logger.notice("loading %d quadrangles" % (len(quads)))
        cur.executemany(
            """
            INSERT INTO mailles(a, b, c, d, GEOMETRY)  VALUES (?, ?, ?, ?, GeomFromText(?, {}))
            """.format(
                self.__project_SRID
            ),
            [
                (
                    dic_nodes[e[3]] + 1,
                    dic_nodes[e[2]] + 1,
                    dic_nodes[e[1]] + 1,
                    dic_nodes[e[0]] + 1,
                    Polygon([nodes[e[3]], nodes[e[2]], nodes[e[1]], nodes[e[0]]]).wkt,
                )
                for e in quads
            ],
        )
        if triangles:
            logger.notice("loading %d triangles" % (len(triangles)))
            cur.executemany(
                """
                INSERT INTO mailles(a, b, c, GEOMETRY)  VALUES (?, ?, ?, GeomFromText(?, {}))
                """.format(
                    self.__project_SRID
                ),
                [
                    (
                        dic_nodes[e[2]] + 1,
                        dic_nodes[e[1]] + 1,
                        dic_nodes[e[0]] + 1,
                        Polygon([nodes[e[2]], nodes[e[1]], nodes[e[0]]]).wkt,
                    )
                    for e in triangles
                ],
            )

        cur.execute("UPDATE geometry_columns_statistics set last_verified = 0")
        cur.execute("SELECT UpdateLayerStatistics('mailles')")
        cur.execute("CREATE INDEX mailles_a_idx ON mailles(a)")
        cur.execute("CREATE INDEX mailles_b_idx ON mailles(b)")
        cur.execute("CREATE INDEX mailles_c_idx ON mailles(c)")
        cur.execute("CREATE INDEX mailles_d_idx ON mailles(d)")

        cur.execute("UPDATE mailles SET surface = ST_Area(GEOMETRY)")
        cur.execute(
            """UPDATE noeuds SET surface =
            (SELECT SUM(ST_Area(e.geometry)/(case when e.d is null then 3 else 4 end)) FROM mailles as e
                WHERE e.a=noeuds.OGC_FID or e.b=noeuds.OGC_FID or e.c=noeuds.OGC_FID or e.d=noeuds.OGC_FID)"""
        )

        logger.notice("total number of elements is %d" % (len(triangles) + len(quads)))
        logger.notice(
            "total surface from nodes is",
            cur.execute("select sum(surface) from noeuds").fetchone()[0],
            "m2",
        )
        logger.notice(
            "total surface from mesh is",
            cur.execute("select sum(surface) from mailles").fetchone()[0],
            "m2",
        )

        logger.notice("loading contour nodes")
        cur.execute(
            """
            INSERT INTO noeuds_contour(nid)
            WITH ext AS (
                SELECT ST_Buffer(ST_ExteriorRing((select ST_Union(GEOMETRY) from domaines)), 1) AS GEOMETRY
                )
            SELECT n.OGC_FID FROM noeuds AS n, ext WHERE ST_Intersects(n.GEOMETRY, ext.GEOMETRY)
            """
        )


# run as script if invoqued as such
if __name__ == "__main__":
    from .database import sqlite
    from .mesh import mnt, mur
    from .settings import Settings

    logger.enable_console(True)

    if sys.argv[1][-7:] != ".sqlite":
        exit("first argument must be a sqlite database")
    database = sys.argv[1]
    settings = Settings(os.path.dirname(os.path.abspath(database)))
    with sqlite.connect(sys.argv[1]) as conn:
        cur = conn.cursor()

        cfMesh(database, cur, settings)

        if len(sys.argv) > 2:
            mnt(cur, sys.argv[2])
        if len(sys.argv) > 3:
            mur(cur, sys.argv[3])

        conn.commit()
