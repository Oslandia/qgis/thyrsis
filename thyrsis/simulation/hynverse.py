"""
Inversion : determining hydraulic conductivities by inversing hydraulic heads

USAGE:
    python -m thyrsis.simulation.hynverse database.sqlite [-t, -c, -v, -p]

OPTIONS

   -t : test
   -r : run inversion
   -p : plot only
   -v : verbose
"""
import os
import shutil
import sys
from math import exp as exp_
from math import log, sqrt

import matplotlib.pyplot as plt

try:
    from rtree import index
except Exception:
    import site

    from thyrsis.__about__ import DIR_PLUGIN_ROOT

    site.addsitedir(DIR_PLUGIN_ROOT / "embedded_external_libs")
    from thyrsis.embedded_external_libs.rtree import index

import numpy

from ..database import elem_to_node, node_to_elem, sqlite
from ..log import logger
from ..settings import Settings
from ..spatialitemeshdataprovider import SpatialiteMeshConstDataProvider
from ..utilities import parse_file
from ..utilities.lock import ExclusiveLock
from .metis import Metis
from .openfoam import OpenFoam

LOCK_NAME = "hynverse"


class Zone_perm(object):
    """Class object for permeability zone"""

    def __init__(self, num, perm, pmin, pmax, liste_pp):
        """Constructor

        :param num: zone id
        :type num: int
        :param perm: permeability
        :type perm: float
        :param pmin: min permeability tolerated
        :type pmin: float
        :param pmax: max permeability tolerated
        :type pmax: float
        :param liste_pp: pilot point list
        :type liste_pp: list
        """
        self.num = num
        self.perm = perm
        self.pmin = pmin
        self.pmax = pmax
        self.liste_pp = liste_pp


class Point_pilote(object):
    """Class object for pilot point"""

    def __init__(self, x, y, num, nom, groupe, zone, hmes, ogc_fid):
        """Constructor

        :param x: x coord
        :type x: float
        :param y: y coord
        :type y: float
        :param num: numero dans liste_pp totale
        :type num: int
        :param nom: pilot point name
        :type nom: string
        :param groupe: pilot point group name
        :type groupe: string
        :param zone: zone id
        :type zone: int
        :param zmnt: topographic altitude
        :type zmnt: float
        :param zmur: substratum topographic altitude
        :type zmur: float
        :param hmes: measured hydraulic head
        :type hmes: float
        :param ogc_fid : unique identifier in database
        :type ogc_fid: int
        """
        self.x = x
        self.y = y
        self.num = num  # numero dans liste_pp totale
        self.nom = nom
        self.groupe = groupe
        self.zone = zone
        self.zmnt = 0
        self.zmur = 0
        self.hmes = hmes
        self.ogc_fid = ogc_fid
        self.hcal = []
        self.perm = []


def distance(x1, y1, x2, y2):
    """Compute distance between two points

    :param x1: x1 coord
    :type x1: float
    :param y1: y1 coord
    :type y1: float
    :param x2: x2 coord
    :type x2: float
    :param y2: y2 coord
    :type y2: float

    :return: distance
    :rtype: float
    """
    return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))


def distance2(x1, y1, x2, y2):
    """Compute distance² between two points

    :param x1: x1 coord
    :type x1: float
    :param y1: y1 coord
    :type y1: float
    :param x2: x2 coord
    :type x2: float
    :param y2: y2 coord
    :type y2: float

    :return: distance²
    :rtype: float
    """
    return (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)


def poids(x, y, nearest, xy):
    """Compute weight between point, used for indexing

    :param x: x1 coord
    :type x: float
    :param y: y1 coord
    :type y: float
    :param nearest: nearest points list
    :type nearest: list
    :param xy: pilots point xy
    :type xy: list

    :return: weight
    :rtype: float
    """
    if distance2(x, y, xy[nearest[0]][0], xy[nearest[0]][1]) == 0:
        return numpy.array([1] + [0 for i in range(len(nearest) - 1)])
    else:
        pds = numpy.array([1.0 / distance2(x, y, xy[n][0], xy[n][1]) for n in nearest])
        return pds / numpy.sum(pds)


def fun1(x, y):
    """Return coefficient 1/-1

    :param x: x1 coord
    :type x: float
    :param y: y1 coord
    :type y: float

    :return: coefficient
    :rtype: int
    """
    if x > y:
        return 1.0
    elif x < -y:
        return -1
    else:
        return 0.0


class Hynverse(object):
    """Process class for inversion calculation"""

    def __init__(self, code, perm):
        """Constructor

        :param code: Simulation code
        :type code: Metis or OpenFoam
        :param perm: permeability field provided flag
        :type perm: bool

        """
        logger.notice("\nINITIALISATION")
        self.init_flag = True
        self.__code = code
        self.__database = code.database
        self.__parametres = self.hynverse_param()
        self.perm = perm
        if self.perm:
            self.__parametres["niter"] = 1
            logger.notice("Using a permeability field to create the model")
        logger.notice("parameters = ", self.__parametres)
        self.__tmpdir = self.hynverse_tmpdir()
        if not self.perm:
            if not self.init_flag:
                return
            self.__idx_n, self.__xy_n = self.index_nodes()
            self.__idx_m, self.__xy_m = self.index_mailles()
            (
                self.__idx_pp,
                self.__xy_pp,
                self.__liste_pp,
                self.__liste_pp_groups,
            ) = self.hynverse_pp()
            # liste_pp_ref = pilot points list ordered by group and excluding added pilot points
            self.__liste_pp_ref = []
            for g in sorted(self.__liste_pp_groups.keys()):
                self.__liste_pp_ref += [pp for pp in self.__liste_pp_groups[g]]
            if not self.init_flag:
                return
            self.__idx_pppp = self.index_pppp()
            self.__idx_npp, self.__pds_npp = self.index_npp()
            self.__idx_ppm, self.__pds_ppm = self.index_ppm()
            (
                self.__idx_ppn,
                self.__pds_ppn,
            ) = self.index_ppn()  # pour le calcul de la permeabilite aux noeuds
            self.__dict_zones = self.hynverse_zones()
        code.create_hydrostationary_files(self.__tmpdir, with_permeabilities=False)
        if self.__parametres["niter"] <= 0:
            self.init_flag = False

        with ExclusiveLock(LOCK_NAME):
            with sqlite.connect(self.__database) as conn:
                cur = conn.cursor()
                self.__altitude = [
                    x[0]
                    for x in cur.execute(
                        "SELECT altitude FROM noeuds ORDER BY OGC_FID"
                    ).fetchall()
                ]
                self.__altitude_mur = [
                    x[0]
                    for x in cur.execute(
                        "SELECT altitude_mur FROM noeuds ORDER BY OGC_FID"
                    ).fetchall()
                ]
                cur.execute("DELETE from hynverse_erreurs")
                conn.commit()

    def hynverse_param(self):
        """Returns hynverse parameters from database

        :return: parameters
        :rtype: dict
        """
        # parametres de l'inversion
        with sqlite.connect(self.__database) as conn:
            cur = conn.cursor()
            names = cur.execute("PRAGMA table_info(hynverse_parametres)").fetchall()
            param_names = [names[i][1] for i in range(1, len(names))]
            logger.notice("param_names = ", param_names)
            param_values = cur.execute(
                "SELECT %s FROM hynverse_parametres ORDER BY OGC_FID"
                % (",".join(param_names))
            ).fetchone()
            return {param_names[i]: param_values[i] for i in range(len(param_names))}

    def get_params(self):
        """Returns hynverse parameters

        :return: parameters
        :rtype: dict
        """
        return self.__parametres

    def hynverse_tmpdir(self):
        """Creates hynverse directory with removing existing directory

        :return: tempdir
        :rtype: string
        """
        # création du répertoire de calcul
        dbname = self.__database
        dir_name = os.path.dirname(os.path.abspath(dbname))
        database = os.path.join(dir_name, dbname)
        tmpdir = os.path.join(dir_name, os.path.basename(dbname)[:-7] + "_tmp")
        if os.path.isdir(tmpdir):
            shutil.rmtree(tmpdir)
        os.makedirs(tmpdir)
        logger.debug("tmpdir = ", tmpdir)

        # fichiers utilisateur
        for filename in os.listdir(dir_name):
            if filename[0:2] == "u_":
                if os.path.isdir(filename):
                    shutil.copytree(
                        os.path.join(dir_name, filename),
                        os.path.join(tmpdir, filename[2:]),
                    )
                else:
                    logger.notice("Copying file ", filename)
                    shutil.copyfile(
                        os.path.join(dir_name, filename),
                        os.path.join(tmpdir, filename[2:]),
                    )

        # infiltration
        with ExclusiveLock(LOCK_NAME):
            with sqlite.connect(self.__database) as conn:
                cur = conn.cursor()
                if self.__parametres["infiltration"] > 0.0:
                    logger.notice("homogeneous infiltration")
                    cur.execute(
                        "UPDATE noeuds SET infiltration = %g"
                        % (self.__parametres["infiltration"])
                    )
                    cur.execute(
                        "UPDATE mailles SET infiltration = %g"
                        % (self.__parametres["infiltration"])
                    )
                elif os.path.exists(os.path.join(tmpdir, "infiltration.node.sat")):
                    logger.notice("reading file infiltration.node.sat")
                    nnoeuds = cur.execute("SELECT COUNT(1) FROM noeuds").fetchone()[0]
                    infiltration = parse_file(
                        os.path.join(tmpdir, "infiltration.node.sat"), 1, nnoeuds
                    )
                    cur.executemany(
                        "UPDATE noeuds SET infiltration = ? WHERE OGC_FID = ? ",
                        infiltration,
                    )
                    node_to_elem(cur, infiltration)
                    os.remove(os.path.join(tmpdir, "infiltration.node.sat"))
                elif os.path.exists(os.path.join(tmpdir, "infiltration.elem.sat")):
                    logger.notice("reading file infiltration.elem.sat")
                    nmailles = cur.execute("SELECT COUNT(1) FROM mailles").fetchone()[0]
                    infiltration = parse_file(
                        os.path.join(tmpdir, "infiltration.elem.sat"), 1, nmailles
                    )
                    cur.executemany(
                        "UPDATE mailles SET infiltration = ? WHERE OGC_FID = ? ",
                        infiltration,
                    )
                    elem_to_node(cur, infiltration)
                    os.remove(os.path.join(tmpdir, "infiltration.elem.sat"))
                else:
                    logger.error(
                        "Null infiltration and file u_infiltration.node.sat or u_infiltration.elem.sat missing"
                    )
                    self.init_flag = False

                # epaisseur aquifere
                cur.execute(
                    "UPDATE noeuds SET epaisseur_aquifere = altitude - altitude_mur"
                )
                cur.execute(
                    "UPDATE mailles SET epaisseur_aquifere = altitude - altitude_mur"
                )

                conn.commit()
        return tmpdir

    def index_nodes(self):
        """Computes index for nodes coordinates

        :return idx_n: nodes index
        :rtype idx_n: rtree.Index
        :return xy_n: nodes coordinates list
        :rtype xy_n: list
        """
        # indexage des noeuds
        idx_n = index.Index()
        i_n = 0
        xy_n = []
        with sqlite.connect(self.__database) as conn:
            cur = conn.cursor()
            cur.execute("SELECT X(GEOMETRY), Y(GEOMETRY) FROM noeuds ORDER BY OGC_FID")
            for x, y in cur.fetchall():
                idx_n.insert(i_n, (x, y, x, y))
                xy_n.append((x, y))
                i_n += 1
        return idx_n, xy_n

    def index_mailles(self):
        """Computes index for mesh

        :return idx_m: mesh index
        :rtype idx_m: rtree.Index
        :return xy_m: mesh coordinates list
        :rtype xy_m: list
        """
        # indexage des centres de maille
        idx_m = index.Index()
        i_m = 0
        xy_m = []
        with sqlite.connect(self.__database) as conn:
            cur = conn.cursor()
            cur.execute(
                "SELECT X(Centroid(GEOMETRY)), Y(Centroid(GEOMETRY)) FROM mailles ORDER BY OGC_FID"
            )
            for x, y in cur.fetchall():
                idx_m.insert(i_m, (x, y, x, y))
                xy_m.append((x, y))
                i_m += 1
        return idx_m, xy_m

    def hynverse_pp(self):
        """Defines pilot points and indexes them

        :return idx_pp: pp index
        :rtype idx_pp: rtree.Index
        :return xy_pp: pp coordinates list
        :rtype xy_pp: list
        :return list_pp: Point_pilote list
        :rtype list_pp: list
        """
        # points pilotes

        with ExclusiveLock(LOCK_NAME):
            with sqlite.connect(self.__database) as conn:
                cur = conn.cursor()
                # deleting pilote points from a precedent potentiel_reference
                cur.execute("DELETE FROM points_pilote WHERE SUBSTR(nom,1,6)='SUPPL_'")

                liste_pp = []
                idx_pp = index.Index()
                n_pp = 0
                xy_pp = []
                n_pp_groupes = 0
                n_pp_zones = 0
                cur.execute(
                    """SELECT X(GEOMETRY), Y(GEOMETRY), nom, groupe, zone,
                        altitude_piezo_mesuree, OGC_FID FROM points_pilote ORDER BY nom"""
                )
                for x, y, nom, groupe, zone, hmes, ogc_fid in cur.fetchall():
                    liste_pp.append(
                        Point_pilote(x, y, n_pp + 1, nom, groupe, zone, hmes, ogc_fid)
                    )
                    idx_pp.insert(n_pp, (x, y, x, y))
                    xy_pp.append((x, y))
                    n_pp_groupes = max(n_pp_groupes, groupe)
                    n_pp += 1
                n_pp_ref = n_pp
                logger.notice("Number of pilot points = ", n_pp)

                # resampling zones from 1 to nzones in case of non-continuous values
                logger.notice("resampling zones")
                zones = [pp.zone for pp in liste_pp]
                new_zones = [(j + 1, v) for j, v in enumerate(sorted(set(zones)))]
                new_dzones = {v: j for (j, v) in new_zones}
                n_pp_zones = len(sorted(set(zones)))
                for pp in liste_pp:
                    pp.zone = new_dzones[pp.zone]
                    logger.notice(pp.nom, "new zone is", pp.zone)
                cur.executemany(
                    "UPDATE points_pilote SET zone=? WHERE zone=?", new_zones
                )
                conn.commit()

                # potentiel de reference
                dmesh = self.__parametres["d_mesh"]
                dmesh2 = dmesh * dmesh
                n_pp_sup = 0
                if (
                    cur.execute(
                        "SELECT COUNT(*) from noeuds WHERE potentiel_reference is NULL"
                    ).fetchone()[0]
                    == 0
                ):
                    logger.notice("Adding pilot points")
                    # indexage des noeuds à potentiel imposé
                    idx_pot_imp = index.Index()
                    nodes_pot_imp = list(
                        set(
                            [
                                x[0] - 1
                                for x in cur.execute(
                                    "SELECT nid from potentiel_impose order by nid"
                                ).fetchall()
                            ]
                        )
                    )
                    for i, node in enumerate(nodes_pot_imp):
                        idx_pot_imp.insert(
                            i,
                            (
                                self.__xy_n[node][0],
                                self.__xy_n[node][1],
                                self.__xy_n[node][0],
                                self.__xy_n[node][1],
                            ),
                        )

                    project_SRID = str(
                        cur.execute(
                            "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
                        ).fetchone()[0]
                    )
                    provider = SpatialiteMeshConstDataProvider(
                        "dbname=%s crs=epsg:%s column=%s"
                        % (self.__database, project_SRID, "potentiel_reference")
                    )

                    for m, xy in enumerate(self.__xy_m):
                        # on exclut les mailles trop proches des noeuds à potentiel imposé
                        nearest = list(
                            idx_pot_imp.nearest((xy[0], xy[1], xy[0], xy[1]), 1)
                        )
                        if (
                            distance2(
                                xy[0],
                                xy[1],
                                self.__xy_n[nearest[0]][0],
                                self.__xy_n[nearest[0]][1],
                            )
                            < dmesh2
                        ):
                            continue
                        nearest = list(idx_pp.nearest((xy[0], xy[1], xy[0], xy[1]), 1))
                        if (
                            distance2(
                                xy[0],
                                xy[1],
                                liste_pp[nearest[0]].x,
                                liste_pp[nearest[0]].y,
                            )
                            > dmesh2
                        ):
                            if n_pp == n_pp_ref:
                                n_pp_groupes += 1
                            n_pp += 1
                            n_pp_sup += 1
                            n_pp_zones += 1
                            value = float(provider.valuesAt(xy))
                            nom = "SUPPL_" + str(n_pp_sup)
                            idx_pp.insert(n_pp - 1, (xy[0], xy[1], xy[0], xy[1]))
                            xy_pp.append((xy[0], xy[1]))
                            cur.execute(
                                """INSERT INTO points_pilote
                                (GEOMETRY, nom, groupe, zone, altitude_piezo_mesuree)
                                VALUES ( MakePoint( ?, ?, {}), ?, ?, ?, ?)""".format(
                                    project_SRID
                                ),
                                (xy[0], xy[1], nom, n_pp_groupes, n_pp_zones, value),
                            )

                            new_ogc_fid = cur.lastrowid

                            # print("new pilot point = ", xy[0], xy[1], nom, n_pp_groupes, n_pp_zones, value)
                            liste_pp.append(
                                Point_pilote(
                                    xy[0],
                                    xy[1],
                                    n_pp,
                                    nom,
                                    n_pp_groupes,
                                    n_pp_zones,
                                    value,
                                    new_ogc_fid,
                                )
                            )

                    conn.commit()

        logger.notice("Number of added pilot points = ", n_pp_sup)
        logger.notice("Total number of pilot points = ", n_pp)
        if n_pp == 0:
            logger.notice("No pilot point !")
            self.init_flag = False

        # groups without added pilot points
        groups = list(set([pp.groupe for pp in liste_pp if pp.nom[:6] != "SUPPL_"]))
        liste_pp_groups = {}
        for g in groups:
            liste_pp_groups[g] = [pp for pp in liste_pp if pp.groupe == g]

        return idx_pp, xy_pp, liste_pp, liste_pp_groups

    def index_pppp(self):
        """Computes index of pilot points from themself

        :return idx_pppp: pp index
        :rtype idx_pppp: list
        """
        # index des points pilotes par rapport à eux-mêmes
        # et nombre de plus proches dans un rayon donné (dv_pp)
        nnearest = 100
        dv_pp = self.__parametres["dv_pp"]
        dv_pp2 = dv_pp * dv_pp
        idx_pppp = []
        for pp in self.__liste_pp:
            nearest = list(self.__idx_pp.nearest((pp.x, pp.y, pp.x, pp.y), nnearest))
            idx_pppp.append([nearest[0]])
            for n in nearest[1:]:
                if (
                    distance2(pp.x, pp.y, self.__xy_pp[n][0], self.__xy_pp[n][1])
                    <= dv_pp2
                ):
                    idx_pppp[-1].append(n)
        return idx_pppp

    def index_ppm(self):
        """Computes index of pilot point from center mesh elements

        :return idx_ppm: ppm index
        :rtype idx_ppm: list
        """
        # index des points pilote les plus proches des centres de mailles
        logger.notice("Indexing nearest pilot points from meshes")
        idx_ppm = []
        pds_ppm = []
        with sqlite.connect(self.__database, "hynverse:index_ppm") as conn:
            cur = conn.cursor()
            cur.execute(
                "SELECT X(Centroid(GEOMETRY)), Y(Centroid(GEOMETRY)) FROM mailles ORDER BY OGC_FID"
            )
            for x, y in cur.fetchall():
                nearest = list(
                    self.__idx_pp.nearest((x, y, x, y), self.__parametres["nv_ppm"])
                )
                idx_ppm.append(nearest)
                pds_ppm.append(poids(x, y, nearest, self.__xy_pp))

        return idx_ppm, pds_ppm

    def index_ppn(self):
        """Computes index of pilot point from nodes

        :return idx_ppn: ppn index
        :rtype idx_ppn: list
        """
        # index des points pilote les plus proches des noeuds (pour le calcul de la permeabilite aux noeuds)
        logger.notice("Indexing nearest pilot points from nodes")
        idx_ppn = []
        pds_ppn = []
        with sqlite.connect(self.__database, "hynverse:index_ppn") as conn:
            cur = conn.cursor()
            cur.execute(
                "SELECT X(Centroid(GEOMETRY)), Y(Centroid(GEOMETRY)) FROM noeuds ORDER BY OGC_FID"
            )
            for x, y in cur.fetchall():
                nearest = list(
                    self.__idx_pp.nearest((x, y, x, y), self.__parametres["nv_ppm"])
                )
                idx_ppn.append(nearest)
                pds_ppn.append(poids(x, y, nearest, self.__xy_pp))

        return idx_ppn, pds_ppn

    def index_npp(self):
        """Computes index of nearest nodes from pilot point

        :return idx_npp: npp index
        :rtype idx_npp: list
        :return pds_npp: weight of nearest nodes
        :rtype pds_npp: list

        """
        # index des noeuds les plus proches des points pilote
        logger.notice("Indexing nearest nodes from pilot points")

        idx_npp = []
        pds_npp = []
        for pp in self.__liste_pp:
            nearest = list(
                self.__idx_n.nearest(
                    (pp.x, pp.y, pp.x, pp.y), self.__parametres["nv_npp"]
                )
            )
            idx_npp.append(nearest)
            pds_npp.append(poids(pp.x, pp.y, nearest, self.__xy_n))

        return idx_npp, pds_npp

    def hynverse_zones(self):
        """Creates hynverse zones from pilot points

        :return: zones dictionnary
        :rtype: dict
        """
        # zones
        dict_zones = dict()
        with ExclusiveLock(LOCK_NAME):
            with sqlite.connect(self.__database, "hynverse:hynverse_zones") as conn:
                cur = conn.cursor()
                cur.execute("DELETE from zones")
                for pp in self.__liste_pp:
                    if pp.zone in list(dict_zones.keys()):
                        dict_zones[pp.zone].liste_pp.append(pp)
                    elif pp.zone > 0:
                        dict_zones[pp.zone] = Zone_perm(
                            pp.zone,
                            self.__parametres["permini"],
                            self.__parametres["permin"],
                            self.__parametres["permax"],
                            [pp],
                        )
                        cur.execute(
                            "INSERT INTO zones(numero, permeabilite, permeabilite_min, permeabilite_max) VALUES(?, ?, ?, ?)",
                            (
                                pp.zone,
                                self.__parametres["permini"],
                                self.__parametres["permin"],
                                self.__parametres["permax"],
                            ),
                        )

                conn.commit()

        logger.notice("Number of zones = ", len(dict_zones))

        return dict_zones

    def calcul(self):
        """Hynverse proccess"""
        sat_dir = self.__tmpdir

        it = 1
        ie = 1
        imin = 1
        nessais = 0
        icalc = self.__parametres["icalc"]
        icalc0 = icalc
        if icalc != 1:
            icalc = 1
        alfa = self.__parametres["alfa"]
        alfmin = self.__parametres["alfmin"]
        errlim = self.__parametres["errlim"]
        erreur = errlim + 1.0
        erreurs = []
        terr = self.__parametres["terr"]
        errmin = 1.0e10
        nessaismax = self.__parametres["nessaismax"]
        niter = self.__parametres["niter"]

        if not self.perm:
            dict_zones = self.__dict_zones
            liste_pp = self.__liste_pp
            liste_pp_ref = self.__liste_pp_ref

            # zmnt and zmur at pilot points for results purpose
            for pp in liste_pp:
                for i, n in enumerate(self.__idx_npp[pp.num - 1]):
                    pp.zmnt += self.__altitude[n] * self.__pds_npp[pp.num - 1][i]
                    pp.zmur += self.__altitude_mur[n] * self.__pds_npp[pp.num - 1][i]

            if len(dict_zones) == 0:
                logger.notice("No zone defined")
                return

            delt0 = numpy.zeros(len(dict_zones), dtype=numpy.float32)
            delt1 = numpy.zeros(len(dict_zones), dtype=numpy.float32)
            perm0 = numpy.array([dict_zones[i].perm for i in sorted(dict_zones.keys())])
            perm1 = numpy.zeros(len(dict_zones), dtype=numpy.float32)
            popt = numpy.zeros(len(dict_zones), dtype=numpy.float32)
            permin = numpy.array(
                [dict_zones[i].pmin for i in sorted(dict_zones.keys())]
            )
            permax = numpy.array(
                [dict_zones[i].pmax for i in sorted(dict_zones.keys())]
            )

        while (
            it <= niter and erreur >= errlim and alfa >= alfmin and nessais < nessaismax
        ):

            logger.notice("ITERATION ", str(it))

            if not self.perm:
                # détermination des perméabilités en chaque zone
                deltm = numpy.max(numpy.abs(delt0))
                if icalc == 1:
                    coef = numpy.array(
                        [
                            fun1(delt0[i - 1], 1.0e-3 * errlim)
                            for i in sorted(dict_zones.keys())
                        ]
                    )
                    perm1 = perm0 + alfa * coef * perm0
                elif deltm > 0.0:
                    perm1 = perm0 + alfa * delt0 * perm0 / deltm
                perm1 = numpy.maximum(perm1, permin)
                perm1 = numpy.minimum(perm1, permax)

                for j, i in enumerate(sorted(dict_zones.keys())):
                    dict_zones[i].perm = perm1[j]

                # construction du champ de perméabilités aux mailles

                # lissage par moyenne mobile
                liste_ppk = []  # perméabilités aux pp
                for i, pp in enumerate(liste_pp):
                    p = 0.0
                    for n in self.__idx_pppp[i]:
                        p += log(dict_zones[liste_pp[n].zone].perm)
                    liste_ppk.append(p / len(self.__idx_pppp[i]))

                # on réaffecte la perméabilité de chaque zone par moyenne sur les pp de la zone
                for z, zone in list(dict_zones.items()):
                    p = 0.0
                    for pp in zone.liste_pp:
                        p += liste_ppk[pp.num - 1]
                    dict_zones[z].perm = exp_(p / len(zone.liste_pp))

                # permeabilites aux mailles
                liste_pm = []
                for j, m in enumerate(self.__idx_ppm):
                    p = 0.0
                    for k, i in enumerate(m):
                        p += (
                            log(dict_zones[liste_pp[i].zone].perm)
                            * self.__pds_ppm[j][k]
                        )
                    p = exp_(p)
                    liste_pm.append((p, p, j + 1))

                with ExclusiveLock(LOCK_NAME):
                    with sqlite.connect(
                        self.__database, "hynverse:calcul:update permeabilities"
                    ) as conn:
                        cur = conn.cursor()
                        logger.notice("updating permeabilities at mesh")
                        self.__code.create_permeabilities_sat(
                            sat_dir, liste_pm=liste_pm
                        )
                        cur.executemany(
                            "UPDATE mailles SET permeabilite_x=?, permeabilite_y=? WHERE OGC_FID=?",
                            liste_pm,
                        )

                        # permeabilites aux noeuds
                        liste_pn = []
                        for j, m in enumerate(self.__idx_ppn):
                            p = 0.0
                            for k, i in enumerate(m):
                                p += (
                                    log(dict_zones[liste_pp[i].zone].perm)
                                    * self.__pds_ppn[j][k]
                                )
                            p = exp_(p)
                            liste_pn.append((p, p, j + 1))

                        logger.notice("updating permeabilities at nodes")
                        cur.executemany(
                            "UPDATE noeuds SET permeabilite_x=?, permeabilite_y=? WHERE OGC_FID=?",
                            liste_pn,
                        )
                        conn.commit()

                # calcul hydro

                self.__code.compute_hynverse(sat_dir)

                # calcul des potentiels aux noeuds

                logger.notice("Pilot points hydraulic head computation")
                potentiel = self.__code.read_potential_at_nodes(sat_dir, [0])

                # calcul des potentiels aux points pilotes et de l'erreur

                erreur = 0.0
                logger.debug(
                    "{0:20s} {1:5s} {2:5s} {3:8s} {4:8s} {5:8s} {6:s}".format(
                        "nom", "num", "zone", "hmes", "hcal", "hc-hm", "perm"
                    )
                )
                for iz, zone in enumerate(sorted(dict_zones.keys())):
                    delt1[iz] = 0.0
                    for pp in dict_zones[zone].liste_pp:
                        hc = 0.0
                        for i, n in enumerate(self.__idx_npp[pp.num - 1]):
                            hc += potentiel[0, n] * self.__pds_npp[pp.num - 1][i]
                        pp.hcal.append(hc)
                        pp.perm.append(dict_zones[pp.zone].perm)

                        dh = pp.hcal[-1] - pp.hmes
                        if pp.nom[:6] != "SUPPL_":
                            logger.debug(
                                "{0:20s} {1:5d} {2:5d} {3:8.2f} {4:8.2f} {5:8.2f} {6:.2e}".format(
                                    pp.nom,
                                    pp.num,
                                    pp.zone,
                                    pp.hmes,
                                    pp.hcal[-1],
                                    dh,
                                    pp.perm[-1],
                                )
                            )
                        delt1[iz] += dh
                        if icalc0 == 2:
                            erreur += dh * dh
                        else:
                            erreur += abs(dh)

                    delt1[iz] = delt1[iz] / len(dict_zones[zone].liste_pp)

                if icalc0 == 2:
                    erreur = sqrt(erreur)
                else:
                    erreur = erreur / len(liste_pp)

                erreurs.append(erreur)
                logger.notice("Pilot points error = ", erreur)

                with ExclusiveLock(LOCK_NAME):
                    with sqlite.connect(
                        self.__database, "hynverse:calcul:update points_pilote"
                    ) as conn:
                        cur = conn.cursor()

                        cur.execute(
                            """INSERT INTO hynverse_erreurs(iteration, iteration_vraie, nessais, erreur, alfa, terr)
                            VALUES(?,?,?,?,?,?)""",
                            (it, ie, nessais, erreur, alfa, terr),
                        )
                        # points pilote
                        for pp in liste_pp:
                            cur.execute(
                                """UPDATE points_pilote SET altitude_piezo_calculee=?, difference_calcul_mesure=?,
                                permeabilite=? WHERE OGC_FID=?""",
                                (
                                    pp.hcal[-1],
                                    pp.hcal[-1] - pp.hmes,
                                    pp.perm[-1],
                                    pp.ogc_fid,
                                ),
                            )
                        conn.commit()

                if erreur < terr * errmin:
                    logger.notice("acceptation")
                    nessais = 0
                    perm0 = perm1
                    numpy.copyto(delt0, delt1)

                    if erreur < errmin:
                        imin = ie
                        errmin = erreur
                        popt = perm0

                    ie += 1

                else:
                    if icalc0 != 1 and icalc == 1:
                        logger.notice("Too large error, switching to mode 3")
                        icalc = icalc0
                        alfa = 0.5
                    else:
                        logger.notice("Too large error, decreasing computation step")
                        alfa = 0.5 * alfa

                    terr = 1.0 + 0.5 * (terr - 1.0)
                    terr = max(terr, 1.1)
                    nessais += 1
                    perm0 = popt
                    logger.notice("alfa = ", alfa)
                    logger.notice("terr - 1 = ", terr - 1)

                it += 1
            else:
                # self.perm = True
                with sqlite.connect(
                    self.__database, "hynverse:calcul:select permeabilities"
                ) as conn:
                    cur = conn.cursor()
                    liste_pm = cur.execute(
                        "SELECT permeabilite_x, permeabilite_y, OGC_FID FROM mailles"
                    ).fetchall()
                self.__code.create_permeabilities_sat(sat_dir, liste_pm=liste_pm)
                # calcul hydro
                self.__code.compute_hynverse(sat_dir)

                # calcul des potentiels aux noeuds
                logger.notice("Pilot points hydraulic head computation")
                potentiel = self.__code.read_potential_at_nodes(sat_dir, [0])

                it += 1
        # fin de la boucle while
        if not self.perm:
            if erreur < errlim:
                logger.notice("Minimal error reached")
            if alfa < alfmin:
                logger.notice("Minimal step reached")
            if it > niter:
                logger.notice("Maximal number of iterations reached")
            if nessais > nessaismax:
                logger.notice("Maximal number of tests reached")

            itmax = it - 1

            with open(os.path.join(self.__tmpdir, "hynverse_error.log"), "w") as fil:
                for e in erreurs:
                    fil.write("%10.4f\n" % (e))

            with open(os.path.join(self.__tmpdir, "hynverse_results.log"), "w") as fil:
                fil.write(
                    "%20s %5s %5s %8s %8s %8s %8s %8s %10s %8s %8s\n"
                    % (
                        "nom",
                        "num",
                        "zone",
                        "zmnt",
                        "zmur",
                        "hmes",
                        "hcal",
                        "hc-hm",
                        "perm",
                        "ezns",
                        "ezs",
                    )
                )
                for pp in liste_pp_ref:
                    fil.write(
                        "%20s %5d %5d %8.2f %8.2f %8.2f %8.2f %8.2f %10.2e %8.2f %8.2f\n"
                        % (
                            pp.nom,
                            pp.num,
                            pp.zone,
                            pp.zmnt,
                            pp.zmur,
                            pp.hmes,
                            pp.hcal[-1],
                            pp.hcal[-1] - pp.hmes,
                            dict_zones[pp.zone].perm,
                            pp.zmnt - pp.hcal[-1],
                            pp.hcal[-1] - pp.zmur,
                        )
                    )

            with open(
                os.path.join(self.__tmpdir, "hynverse_hcal.log"), "w"
            ) as fil, open(
                os.path.join(self.__tmpdir, "hynverse_dh.log"), "w"
            ) as fild, open(
                os.path.join(self.__tmpdir, "hynverse_perm.log"), "w"
            ) as filp:
                formated_labels = "%10s " % ("PP")
                formated_labels += " ".join(["%10s" for pp in liste_pp_ref]) % (
                    tuple([pp.nom for pp in liste_pp_ref])
                )
                formated_labels += "\n"
                fil.write(formated_labels)
                fild.write(formated_labels)
                filp.write(formated_labels)

                formated_values = "#%10s " % ("zone")
                formated_values += " ".join(["%10s" for pp in liste_pp_ref]) % (
                    tuple([pp.zone for pp in liste_pp_ref])
                )
                formated_values += "\n"
                fil.write(formated_values)

                formated_values = "#%10s " % ("perm")
                formated_values += " ".join(["%10.2e" for pp in liste_pp_ref]) % (
                    tuple([dict_zones[pp.zone].perm for pp in liste_pp_ref])
                )
                formated_values += "\n"
                fil.write(formated_values)

                formated_values = "#%10s " % ("hmes")
                formated_values += " ".join(["%10.2f" for pp in liste_pp_ref]) % (
                    tuple([pp.hmes for pp in liste_pp_ref])
                )
                formated_values += "\n"
                fil.write(formated_values)

                for i in range(len(liste_pp_ref[0].hcal)):
                    formated_values = "%10s " % ("hcal" + str(i + 1))
                    formated_values += " ".join(["%10.2f" for pp in liste_pp_ref]) % (
                        tuple([pp.hcal[i] for pp in liste_pp_ref])
                    )
                    formated_values += "\n"
                    fil.write(formated_values)

                    formated_values = "%10s " % ("dh" + str(i + 1))
                    formated_values += " ".join(["%10.2f" for pp in liste_pp_ref]) % (
                        tuple([(pp.hcal[i] - pp.hmes) for pp in liste_pp_ref])
                    )
                    formated_values += "\n"
                    fild.write(formated_values)

                    formated_values = "%10s " % ("perm" + str(i + 1))
                    formated_values += " ".join(["%10.2e" for pp in liste_pp_ref]) % (
                        tuple([pp.perm[i] for pp in liste_pp_ref])
                    )
                    formated_values += "\n"
                    filp.write(formated_values)

            logger.notice("Ploting results")
            hynverse_plot(self.__database)

        logger.notice("Updating database")
        with ExclusiveLock(LOCK_NAME):
            with sqlite.connect(
                self.__database, "hynverse:calcul:update zones"
            ) as conn:
                cur = conn.cursor()
                # zones
                if not self.perm:
                    for i in sorted(dict_zones.keys()):
                        cur.execute(
                            "UPDATE zones SET permeabilite=? WHERE numero=?",
                            (dict_zones[i].perm, i),
                        )
                self.__code.update_database(conn, sat_dir)

                conn.commit()


def get_tmpdir(dbname):
    """Returns calculation temporary directory

    :return: directory name
    :rtype: string
    """
    dir_name = os.path.dirname(os.path.abspath(dbname))
    tmpdir = os.path.join(dir_name, os.path.basename(dbname)[:-7] + "_tmp")
    logger.notice("temporary directory is", tmpdir)
    return tmpdir


def get_ngplot(dbname):
    # returns the number of pilot points of the first group (needed for plotting)
    logger.notice("getting number of pilot points of the first group")
    with ExclusiveLock(LOCK_NAME):
        with sqlite.connect(dbname) as conn:
            cur = conn.cursor()
            ngplot = cur.execute(
                """SELECT COUNT() FROM points_pilote
                WHERE groupe = (SELECT groupe FROM points_pilote ORDER BY groupe LIMIT 1)"""
            ).fetchone()[0]
            logger.notice("number of pilot points of the first group is", ngplot)
            return ngplot


def plot(tmpdir, ngplot, dpisize, short_name, name, unit, yscale="linear"):
    logger.notice("plotting " + name)
    with open(os.path.join(tmpdir, "hynverse_" + short_name + ".log")) as fil:
        liste_pp = next(fil).split()[1 : ngplot + 1]
        skip_lines = 3 if short_name == "hcal" else 0
        for s in range(skip_lines):
            next(fil)

        values = {}
        lines = fil.readlines()
        for pp in liste_pp:
            values[pp] = []
        for line in lines:
            spl = line.split()[1:]
            for i, pp in enumerate(liste_pp):
                values[pp].append(float(spl[i]))

    fig, ax = plt.subplots(figsize=(11.75, 8.25), constrained_layout=True)
    plt.yscale(yscale)
    ax.set_xlabel("iteration number")
    ax.set_ylabel(name + " (" + unit + ")")
    len_errors = len(values[liste_pp[0]])
    rge = range(0, len_errors, int(len_errors / 20) + 1)
    ax.set_xticks(list(rge))
    ax.set_xticklabels([str(i + 1) for i in rge])
    ax.grid()
    for pp in liste_pp:
        ax.plot(values[pp], "+-", label=pp)
    if short_name == "dh":
        ax.plot([0, len(values[liste_pp[0]]) - 1], [0, 0], "r--")
    ax.legend(loc="upper left", bbox_to_anchor=(1, 1))
    fig.savefig(os.path.join(tmpdir, short_name + ".png"), format="png", dpi=dpisize)
    plt.close(fig)


def hynverse_plot(dbname):
    dpisize = 100
    tmpdir = get_tmpdir(dbname)
    ngplot = get_ngplot(dbname)

    logger.notice("plotting errors")
    with open(os.path.join(tmpdir, "hynverse_error.log")) as fil:
        errors = [float(x.strip()) for x in fil.readlines()]
    fig, ax = plt.subplots(figsize=(11.75, 8.25))
    ax.set_xlabel("iteration number")
    ax.set_ylabel("error (m)")
    len_errors = len(errors)
    rge = range(0, len_errors, int(len_errors / 20) + 1)
    ax.set_xticks(list(rge))
    ax.set_xticklabels([str(i + 1) for i in rge])
    ax.grid()
    ax.plot(list(range(len(errors))), errors, "b+-")
    fig.savefig(os.path.join(tmpdir, "error.png"), format="png", dpi=dpisize)
    plt.close(fig)

    for k, v in {
        "hcal": ["calculated hydraulic head", "m", "linear"],
        "dh": ["(calculated - measured) hydraulic head", "m", "linear"],
        "perm": ["hydraulic conductivity", "m/s", "log"],
    }.items():

        plot(tmpdir, ngplot, dpisize, k, v[0], v[1], v[2])


def hynverse(dbname, run=False, perm=False, verbose=False, plot_only=False):
    """Launch MHYNVERSE process

    :param dbname: path to database
    :type dbname: string
    :param run: run flag
    :type run: bool
    :param perm: permeability field provided flag
    :type perm: bool
    :param verbose: verbose flag
    :type verbose: bool

    :return: success state
    :rtype: bool
    """
    if plot_only:
        hynverse_plot(dbname)
        return

    settings = Settings(os.path.dirname(os.path.abspath(dbname)))
    debug = int(settings.value("General", "debug")) or verbose
    logger.set_level("debug" if debug else "notice")

    logger.notice("*************************")
    logger.notice("*                       *")
    logger.notice("*    H Y N V E R S E    *")
    logger.notice("*                       *")
    logger.notice("*  F.Renard 07/09/2022  *")
    logger.notice("*************************")

    codeHydro = settings.value("General", "codeHydro")
    codeCommand = settings.value("General", "codeHydroCommand")

    if not codeHydro:
        raise RuntimeError("hydrogeologic software not configured")

    logger.notice("\n-------------------------")
    logger.notice("CODE HYDRO = ", codeHydro.upper())
    logger.notice("-------------------------\n")

    if codeHydro == "metis" and os.path.exists(codeCommand):
        code = Metis(dbname, codeCommand)
    elif codeHydro == "openfoam":
        code = OpenFoam(dbname, settings)
    else:
        raise NotImplementedError("hydrogeologic software not installed")

    _hynverse = Hynverse(code, perm)

    if run and _hynverse.init_flag:
        _hynverse.calcul()

    logger.notice("*****************************")
    logger.notice("*  F I N   H Y N V E R S E  *")
    logger.notice("*****************************")


# run as script if invoqued as such
if __name__ == "__main__":
    import sys

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    logger.enable_console(True)

    if len(sys.argv) not in [2, 3, 4, 5]:
        logger.error("wrong number of arguments (try compute.py --help)")
        exit(1)

    run = False
    plot_only = False
    verbose = False
    perm = False
    dbname = None
    for arg in sys.argv[1:-1]:
        if arg == "-r":
            run = True
        if arg == "-p":
            plot_only = True
        elif arg == "-v":
            verbose = True
        elif arg == "-perm":
            perm = True

    dbname = sys.argv[-1]

    if not dbname:
        logger.error(
            "cannot find a '.sqlite' file in arguments (try python -m thyrsis.simulation.hynverse --help)"
        )
        exit(1)

    hynverse(dbname, run, perm, verbose, plot_only)
