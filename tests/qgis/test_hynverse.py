"""Thyrsis unit test for mesh building

.. note:: This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
"""

import os
import unittest
from typing import List

import pytest
from pytest import approx

from thyrsis.database import sqlite

TEST_DIR = os.path.join(os.path.dirname(__file__), os.path.splitext(__file__)[0])
TUTORIAL_DATA_DIR = os.path.join(os.path.dirname(__file__), "../..", "tutorial_data")
TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "../..", "test_data")

SITE_DB = os.path.join(TEST_DIR, ".thyrsis", "sites.sqlite")
MESH_DB = os.path.join(TEST_DIR, ".thyrsis", "mesh", "SACLAY_mesh.mesh.sqlite")


@pytest.fixture()
def test_dir() -> str:
    return TEST_DIR


@pytest.fixture()
def mesh_db() -> str:
    return MESH_DB


@pytest.fixture()
def site_db() -> str:
    return SITE_DB


@pytest.fixture()
def mesh_generator():
    return "gmsh"


def test_inversion_permeability_field(clean_test_dir, thyrsis, create_mesh_project):
    """
    Test inversion with permeability field raster

    @param clean_test_dir: fixture to have a clean test directory
    @param thyrsis: fixture to initialize thyrsis and return thyrsis plugin
    @param create_mesh_project: fixture to create mesh from mnt with tutorial data and point fixes use
    """
    infiltration = 4.75e-09
    define_hynverse_parameters(MESH_DB, infiltration)

    permeability = os.path.join(TUTORIAL_DATA_DIR, "permeability.tif")
    thyrsis.model_no_inverse(param=permeability, const=False, test=True)

    expected_res = [
        infiltration,
        106.541065,
        0.0,
        19.271068356933597,
        4.389880268718116e-05,
    ]

    _check_inversion_result(expected_res)


def test_inversion_const_permeability(clean_test_dir, thyrsis, create_mesh_project):
    """
    Test inversion with const permeability field

    @param clean_test_dir: fixture to have a clean test directory
    @param thyrsis: fixture to initialize thyrsis and return thyrsis plugin
    @param create_mesh_project: fixture to create mesh from mnt with tutorial data and point fixes use
    """
    infiltration = 4.75e-09
    define_hynverse_parameters(MESH_DB, infiltration)

    permeability = 2.55e-5
    thyrsis.model_no_inverse(param=2.55e-5, const=True, test=True)

    expected_res = [
        infiltration,
        117.1054418,
        0.0,
        29.835445156933588,
        permeability,
    ]
    _check_inversion_result(expected_res)


def test_inversion_point_pilotes(
        clean_test_dir, thyrsis, copy_point_pilotes, create_mesh_project
):
    """
    Test inversion with permeability field

    @param clean_test_dir: fixture to have a clean test directory
    @param thyrsis: fixture to initialize thyrsis and return thyrsis plugin
    @param copy_point_pilotes: fixture to copy point pilotes from tutorial data
    @param create_mesh_project: fixture to create mesh from mnt with tutorial data and point fixes use
    """
    infiltration = 4.75e-09
    define_hynverse_parameters(MESH_DB, infiltration)
    thyrsis.inverse(test=True)

    expected_res = [
        infiltration,
        102.9124426,
        0.0,
        15.6424459569336,
        6.052164691711216e-05,
    ]

    _check_inversion_result(expected_res)


def _check_inversion_result(expected_res: List[float]):
    with sqlite.connect(MESH_DB) as conn:
        cur = conn.cursor()
        res = cur.execute(
            "SELECT infiltration, potentiel, flux_eau, epaisseur_zs, permeabilite_x FROM mailles"
        ).fetchone()

        for i, j in zip(res, expected_res):
            approx(i, j)


def define_hynverse_parameters(mesh_db: str, infiltration: float):
    """
    Define hynverse infiltration parameter in mesh database
    @param mesh_db: path to mesh database
    @param infiltration: infiltration value
    """
    with sqlite.connect(mesh_db) as conn:
        cur = conn.cursor()
        cur.execute(f"UPDATE hynverse_parametres SET infiltration={infiltration}")
        conn.commit()


if __name__ == "__main__":
    unittest.main()
