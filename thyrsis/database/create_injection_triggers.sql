PRAGMA foreign_keys = ON;;

CREATE TRIGGER maj_injections_update AFTER UPDATE OF GEOMETRY ON injections
BEGIN
    INSERT INTO noeuds_injections(noeud, injection, ratio)
    SELECT n.OGC_FID, NEW.OGC_FID, n.surface/AREA(NEW.GEOMETRY)
    FROM noeuds as n
    WHERE Intersects(n.GEOMETRY, NEW.GEOMETRY)
    UNION
    SELECT r.noeud, r.injection, r.ratio FROM (
      SELECT i.OGC_FID AS injection, n.OGC_FID AS noeud, MIN(Distance(n.GEOMETRY, i.GEOMETRY)) as d,
        n.surface/AREA(i.GEOMETRY) as ratio
      FROM injections AS i, noeuds AS n, mailles AS m
      WHERE Intersects(m.GEOMETRY, n.GEOMETRY)
      AND n.ROWID IN (SELECT ROWID FROM SpatialIndex
                      WHERE f_table_name = 'noeuds'
                      AND search_frame = m.GEOMETRY)
      AND Intersects(i.GEOMETRY, m.GEOMETRY)
      AND m.ROWID IN (SELECT ROWID FROM SpatialIndex
                      WHERE f_table_name = 'mailles'
                      AND search_frame = i.GEOMETRY)
      AND i.OGC_FID = NEW.OGC_FID) AS r WHERE d > 0
    ;

    UPDATE noeuds_injections SET
      ratio_surface = (SELECT AREA(i.GEOMETRY)/n.surface/(
        SELECT COUNT(1) FROM noeuds_injections AS ni WHERE injection = ni.injection)
        FROM noeuds AS n, injections AS i
        WHERE injection = i.OGC_FID
        AND noeud = n.OGC_FID )
    ;
    UPDATE noeuds_injections SET
      ratio = ratio_surface/(
        SELECT SUM(ratio_surface) FROM noeuds_injections AS ni WHERE injection = ni.injection)
    ;

    INSERT INTO mailles_injections(maille, injection, ratio, ratio_surface)
    SELECT m.OGC_FID, NEW.OGC_FID,
        AREA(Intersection(m.GEOMETRY, NEW.GEOMETRY))/AREA(NEW.GEOMETRY),
        AREA(Intersection(m.GEOMETRY, NEW.GEOMETRY))/AREA(m.GEOMETRY)
    FROM mailles as m
    WHERE Intersects(m.GEOMETRY, NEW.GEOMETRY)
    ;

    UPDATE injections SET
        permeabilite = (
          SELECT Coalesce(EXP(SUM(LN(m.permeabilite_zns))/COUNT(1)),
                           EXP(SUM(LN(m.permeabilite_x))/COUNT(1)))
          FROM mailles AS m, mailles_injections AS mi
          WHERE mi.injection = injections.OGC_FID
          AND m.OGC_FID = mi.maille ),
        infiltration = (
          SELECT AVG(m.infiltration)
          FROM mailles AS m, mailles_injections AS mi
          WHERE mi.injection = injections.OGC_FID
          AND m.OGC_FID = mi.maille ),
        epaisseur = (
          SELECT AVG(m.epaisseur_zns)
          FROM mailles AS m, mailles_injections AS mi
          WHERE mi.injection = injections.OGC_FID
          AND m.OGC_FID = mi.maille ),
        altitude = (
          SELECT AVG(m.altitude)
          FROM mailles AS m, mailles_injections AS mi
          WHERE mi.injection = injections.OGC_FID
          AND m.OGC_FID = mi.maille )
    WHERE injections.OGC_FID = NEW.OGC_FID
    ;
END
;;

CREATE TRIGGER maj_injections_insert AFTER INSERT ON injections
BEGIN
    INSERT INTO noeuds_injections(noeud, injection, ratio)
    SELECT n.OGC_FID, NEW.OGC_FID, n.surface/AREA(NEW.GEOMETRY)
    FROM noeuds as n
    WHERE Intersects(n.GEOMETRY, NEW.GEOMETRY)
    UNION
    SELECT r.noeud, r.injection, r.ratio FROM (
      SELECT i.OGC_FID AS injection, n.OGC_FID AS noeud, MIN(Distance(n.GEOMETRY, i.GEOMETRY)) as d,
        n.surface/AREA(i.GEOMETRY) as ratio
      FROM injections AS i, noeuds AS n, mailles AS m
      WHERE Intersects(m.GEOMETRY, n.GEOMETRY)
      AND n.ROWID IN (SELECT ROWID FROM SpatialIndex
                      WHERE f_table_name = 'noeuds'
                      AND search_frame = m.GEOMETRY)
      AND Intersects(i.GEOMETRY, m.GEOMETRY)
      AND m.ROWID IN (SELECT ROWID FROM SpatialIndex
                      WHERE f_table_name = 'mailles'
                      AND search_frame = i.GEOMETRY)
      AND i.OGC_FID = NEW.OGC_FID) AS r WHERE d > 0
    ;

    UPDATE noeuds_injections SET
      ratio_surface = (SELECT AREA(i.GEOMETRY)/n.surface/(
        SELECT COUNT(1) FROM noeuds_injections AS ni WHERE injection = ni.injection)
        FROM noeuds AS n, injections AS i
        WHERE injection = i.OGC_FID
        AND noeud = n.OGC_FID )
    ;
    UPDATE noeuds_injections SET
      ratio = ratio_surface/(
        SELECT SUM(ratio_surface) FROM noeuds_injections AS ni WHERE injection = ni.injection)
    ;

    INSERT INTO mailles_injections(maille, injection, ratio, ratio_surface)
    SELECT m.OGC_FID, NEW.OGC_FID,
        AREA(Intersection(m.GEOMETRY, NEW.GEOMETRY))/AREA(NEW.GEOMETRY),
        AREA(Intersection(m.GEOMETRY, NEW.GEOMETRY))/AREA(m.GEOMETRY)
    FROM mailles as m
    WHERE Intersects(m.GEOMETRY, NEW.GEOMETRY)
    ;

    UPDATE injections SET
        permeabilite = (
          SELECT Coalesce(EXP(SUM(LN(m.permeabilite_zns))/COUNT(1)),
                           EXP(SUM(LN(m.permeabilite_x))/COUNT(1)))
          FROM mailles AS m, mailles_injections AS mi
          WHERE mi.injection = injections.OGC_FID
          AND m.OGC_FID = mi.maille ),
        infiltration = (
          SELECT AVG(m.infiltration)
          FROM mailles AS m, mailles_injections AS mi
          WHERE mi.injection = injections.OGC_FID
          AND m.OGC_FID = mi.maille ),
        epaisseur = (
          SELECT AVG(m.epaisseur_zns)
          FROM mailles AS m, mailles_injections AS mi
          WHERE mi.injection = injections.OGC_FID
          AND m.OGC_FID = mi.maille ),
        altitude = (
          SELECT AVG(m.altitude)
          FROM mailles AS m, mailles_injections AS mi
          WHERE mi.injection = injections.OGC_FID
          AND m.OGC_FID = mi.maille )
    WHERE injections.OGC_FID = NEW.OGC_FID
    ;
END
;;
