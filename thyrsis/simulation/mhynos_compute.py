"""This module contains the mhynos computing functions
"""
import os
from builtins import range, str

from thyrsis.exception import SimulationError

from ..database import sqlite
from ..log import logger
from ..utilities import Timer, read_one_column_file
from .jobqueue import JobQueue
from .mhynos_database import get_injections_pids, get_simulation_data
from .mhynos_probes import mhynos_probes
from .mhynos_results import mhynos_results


def load_dates_simulation(conn, filename):
    """Read dates from dates_simulations.txt file, save them in db

    :param conn: connection on a sqlite database
    :type conn: sqlite3.Connection
    :param fileName: path to dates_simulations.txt
    :type fileName: string

    :return: dates in seconds
    :rtype: list
    """
    dates_seconds = read_one_column_file(filename)

    cur = conn.cursor()

    logger.debug("convert ", filename)
    cur.execute("SELECT debut FROM simulations")
    res = cur.fetchall()
    if len(res) != 1:
        raise SimulationError(
            f"Only one debut value must be defined in simulations table. {len(res)} found."
        )

    data = [(res[0][0], "+" + str(d) + " seconds") for d in dates_seconds]

    cur.execute("DELETE from dates_simulation")
    cur.executemany("INSERT INTO dates_simulation (date) VALUES (datetime(?,?))", data)

    return dates_seconds


def compute_flux_sat(code, sample_dir, name="debit"):
    """Compute injected flux in saturated zone

    :param code: Simulation code
    :type code: Metis or OpenFoam
    :param sat_dir: sample folder path
    :type sat_dir: string
    :param name: name 'debit'/'debit eau' (water flux)
    :type name: string

    :return: initial time
    :rtype: float
    """
    logger.debug("compute_flux_sat")

    sat_dir = os.path.join(sample_dir, "sature")

    if os.path.isfile(os.path.join(sat_dir, name + "_entrant.sat")):
        return 0.0

    with sqlite.connect(code.database) as conn:
        cur = conn.cursor()

        pids = get_injections_pids(conn)

        dates = set()
        nodes = set()  # list of all injection nodes
        meshes = set()  # list of all injection meshes
        ldates = [[] for i in range(len(pids))]
        lflux = [[] for i in range(len(pids))]
        lnodes = [
            [] for i in range(len(pids))
        ]  # list of injection nodes lists par injection
        lmeshes = [
            [] for i in range(len(pids))
        ]  # list of injection meshes lists par injection

        for inj, pid in enumerate(pids):
            l = cur.execute(
                """
                SELECT n.OGC_FID, ni.ratio, ni.ratio_surface
                FROM noeuds_injections AS ni, noeuds AS n
                WHERE ni.injection = """
                + str(pid)
                + """
                AND n.OGC_FID = ni.noeud
                ORDER BY n.OGC_FID"""
            ).fetchall()

            # normalisation des ratios
            sum_ratio = sum([t[1] for t in l])
            lnodes[inj] = [(t[0], t[1] / sum_ratio, t[2]) for t in l]

            l = cur.execute(
                """
                SELECT m.OGC_FID, mi.ratio, mi.ratio_surface
                FROM mailles_injections AS mi, mailles AS m
                WHERE mi.injection = """
                + str(pid)
                + """
                AND m.OGC_FID = mi.maille
                ORDER BY m.OGC_FID"""
            ).fetchall()

            # normalisation des ratios
            sum_ratio = sum([t[1] for t in l])
            lmeshes[inj] = [(t[0], t[1] / sum_ratio, t[2]) for t in l]

            insat_dir = os.path.join(sample_dir, "insat%05d" % (inj + 1))

            ldates[inj], lflux[inj] = code.read_insat_flux(insat_dir, name)

            dates.update(ldates[inj])
            nodes.update([t[0] for t in lnodes[inj]])
            meshes.update([t[0] for t in lmeshes[inj]])

    return code.write_sat_flux(
        sat_dir, name, dates, nodes, meshes, ldates, lnodes, lmeshes, lflux
    )


def mhynos_compute(code, settings, MPI=None):
    """Calculation process

    :param code: Simulation code
    :type code: Metis or OpenFoam
    :param settings: Thyrsis settings
    :type settings: Settings
    :param MPI: MPI parameter
    :type MPI: MPI
    """

    timer = Timer()

    dbname = code.database
    dir_name = os.path.dirname(os.path.abspath(dbname))
    tmpdir = os.path.join(dir_name, os.path.basename(dbname)[:-7] + "_tmp")
    if not os.path.isdir(tmpdir):
        logger.error("Computation has to be initialized first")
        return

    num_process = "process " + str(MPI.COMM_WORLD.Get_rank()) if MPI else ""
    logger.notice("\nCALCUL", num_process)

    with sqlite.connect(dbname) as conn:
        cur = conn.cursor()

        cur.execute(
            "SELECT type_infiltration, type_injection, insature FROM simulations"
        )
        type_infiltration, type_injection, insature = cur.fetchone()
        insat_needed = insature == "oui"

        nsamples = cur.execute("SELECT COUNT(1) FROM parametres_simulation").fetchone()[
            0
        ]
        logger.debug("nombre de simulations = ", nsamples)

        (fuite,) = cur.execute(
            "SELECT COUNT(1) FROM injections WHERE volume_eau > 0"
        ).fetchone()
        total_time = float(get_simulation_data(conn)["TFIN"])

        pids = get_injections_pids(conn)

    sample_dirs = [
        os.path.join(tmpdir, "sample%05d" % (sample_idx + 1))
        for sample_idx in range(nsamples)
    ]

    insat_dirs = (
        [
            os.path.join(sample_dir, "insat%05d" % (inj + 1))
            for inj in range(len(pids))
            for sample_dir in sample_dirs
        ]
        if insat_needed
        else []
    )

    # check directories
    for sample_dir in sample_dirs:
        if not os.path.isdir(sample_dir):
            raise RuntimeError("répertoire " + sample_dir + " inexistant")
    for insat_dir in insat_dirs:
        if not os.path.isdir(insat_dir):
            raise RuntimeError("répertoire " + insat_dir + " inexistant")

    logger.debug(timer.reset("start"))

    for insat_dir in JobQueue(insat_dirs, MPI, True):
        if MPI:
            logger.debug(
                "process ", MPI.COMM_WORLD.Get_rank(), " deals with", insat_dir
            )
        code.compute(insat_dir, total_time, MPI)

    if MPI:
        MPI.COMM_WORLD.barrier()  # wait here before writing sat input from insat output

    logger.debug(timer.reset("insat"))

    sat_needed = True
    for sample_dir in JobQueue(sample_dirs, MPI):
        tini = 0.0
        tini_eau = 0.0
        if insat_needed and type_injection != "aucune":
            tini = compute_flux_sat(code, sample_dir, "debit")
        if insat_needed and (type_infiltration == "transitoire" or fuite):
            tini_eau = compute_flux_sat(code, sample_dir, "debit_eau")
        tini = max(tini, tini_eau) if tini >= 0 else tini

        if MPI:
            logger.debug(
                "process", MPI.COMM_WORLD.Get_rank(), "starting", sample_dir, "at", tini
            )
        if sat_needed and tini >= 0.0:
            sat_dir = os.path.join(sample_dir, "sature")
            code.modif_data(sat_dir, tini)
            code.compute(sat_dir, total_time, MPI)
        elif tini < 0.0:
            logger.notice("Aucun flux n'est arrivé à la nappe")

    logger.debug(timer.reset("sat"))

    if MPI:
        MPI.COMM_WORLD.barrier()  # wait here before writing sat input from insat output
        if MPI.COMM_WORLD.Get_rank() != 0:
            return

    try:
        with sqlite.connect(dbname) as conn:
            dates_seconds = load_dates_simulation(
                conn, os.path.join(tmpdir, "dates_simulation.txt")
            )
            conn.commit()

        logger.debug(timer.reset("loading dates"))

        mhynos_results(code, dates_seconds, settings, MPI)

        logger.debug(timer.reset("loading results"))

        mhynos_probes(code.database, MPI)

        logger.debug(timer.reset("computing results at probes"))

    except IOError as e:
        raise RuntimeError("Probleme lors du calcul, un fichier n'a pas ete genere")
