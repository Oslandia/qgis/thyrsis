"""preference dialog for the thyrsis plugin, it can also be used standalone

USAGE
    preference_dialog.py
"""
import os
import subprocess
import sys
from distutils.spawn import find_executable
from pathlib import Path

from PyQt5.QtCore import QCoreApplication, QDir
from PyQt5.QtGui import QDoubleValidator
from PyQt5.QtWidgets import QApplication, QDialog, QFileDialog, QMessageBox
from qgis.PyQt import uic

from ..database import _import_site_from_exp_db
from ..database.sqlite import connect
from ..settings import Settings


def str_to_bool(s):
    """Convert str to bool (based on non-empty string)"""
    return s != ""


def bool_to_str(b):
    """Convert bool to str True => 'true', False = ''"""
    return "true" if b else ""


class PreferenceDialog(QDialog):
    """Dialog ofr the preference parameters"""

    def __init__(self, settings, database=None):
        """Constructor

        :param settings: settings for Thyrsis use
        :type settings: Settings
        """
        super(PreferenceDialog, self).__init__()
        uic.loadUi(os.path.join(os.path.dirname(__file__), "preferences.ui"), self)

        self.mapsButton.clicked.connect(self.mapsButtonClicked)
        self.mpiConfigButton.clicked.connect(self.mpiConfigButtonClicked)

        self.ZNSCheckBox.clicked.connect(self.ZNSCheckBoxClicked)
        self.ZSCheckBox.clicked.connect(self.ZSCheckBoxClicked)

        self.concentrationScaleLineEdit.setValidator(QDoubleValidator())
        self.darcyScaleLineEdit.setValidator(QDoubleValidator())

        self.okButton.clicked.connect(self.okButtonClicked)
        self.cancelButton.clicked.connect(self.cancelButtonClicked)

        self.__settings = settings
        self.__settingsModified = False
        self.__databasePath = (
            os.path.dirname(os.path.abspath(database)) if database else None
        )

        # setup general values
        self.mapsLineEdit.setText(self.__settings.value("General", "maps", ""))

        # mpi parameters
        self.mpiConfigLineEdit.setText(
            self.__settings.value("General", "mpiConfig", self.mpiConfigLineEdit.text())
        )

        self.nbMpiProcessSpinBox.setValue(
            int(
                self.__settings.value(
                    "General", "nbMpiProcess", self.nbMpiProcessSpinBox.value()
                )
            )
        )

        # nb de colonnes maximal
        self.nbColumnsMaxSpinBox.setValue(
            int(
                self.__settings.value(
                    "General", "nbColumnsMax", self.nbColumnsMaxSpinBox.value()
                )
            )
        )

        if not self.__settings.value("General", "gmsh"):
            self.__settings.setValue("General", "gmsh", find_executable("gmsh"))
        if not self.__settings.value("General", "openfoam"):
            self.__settings.setValue(
                "General", "openfoam", self.__settings.get_openfoam_root_dir()
            )
        if not self.__settings.value("General", "pmf"):
            self.__settings.setValue(
                "General", "pmf", self.__settings.get_pmf_root_dir()
            )

        self.gmshSelectButton.clicked.connect(self.select_gmsh)
        self._define_line_edit_from_setting(self.gmshLineEdit, "General", "gmsh")

        self.openfoamSelectButton.clicked.connect(self.select_openfoam)
        self._define_line_edit_from_setting(
            self.openfoamLineEdit, "General", "openfoam"
        )

        self.pmfSelectButton.clicked.connect(self.select_pmf)
        self._define_line_edit_from_setting(self.pmfLineEdit, "General", "pmf")

        self.meshGeneratorComboBox.addItem("CFMESH")
        if self.__settings.value("General", "gmsh"):
            self.meshGeneratorComboBox.addItem("GMSH")

        self.meshGeneratorComboBox.setCurrentIndex(
            self.meshGeneratorComboBox.findText(
                self.__settings.value("General", "meshGenerator", "").upper()
            )
        )

        self.codeHydroComboBox.addItem("OPENFOAM")
        # code Hydro
        # install directory is now hardcoded to be ~/.thyrsis
        # we fill the codeHydroComboBox accordingly
        # if find_executable("groundwaterFoam"):
        # self.codeHydroComboBox.addItem("OPENFOAM")

        if find_executable("metis") or self.metisIsInLocalDir():
            self.codeHydroComboBox.addItem("METIS")

        self.codeHydroComboBox.setCurrentIndex(
            self.codeHydroComboBox.findText(
                self.__settings.value("General", "codeHydro", "").upper()
            )
        )

        # impressions
        self.consoleCheckBox.setCheckState(
            int(
                self.__settings.value(
                    "General", "console", self.consoleCheckBox.checkState()
                )
            )
        )
        self.debugCheckBox.setCheckState(
            int(
                self.__settings.value(
                    "General", "debug", self.debugCheckBox.checkState()
                )
            )
        )

        # setup Variables values
        self.potentielZSCheckBox.setCheckState(
            int(
                self.__settings.value(
                    "Variables",
                    "potentielZSCheck",
                    self.potentielZSCheckBox.checkState(),
                )
            )
        )
        self.darcyZNSCheckBox.setCheckState(
            int(
                self.__settings.value(
                    "Variables", "darcyZNSCheck", self.darcyZNSCheckBox.checkState()
                )
            )
        )
        self.darcyZSCheckBox.setCheckState(
            int(
                self.__settings.value(
                    "Variables", "darcyZSCheck", self.darcyZSCheckBox.checkState()
                )
            )
        )
        self.concentrationZNSCheckBox.setCheckState(
            int(
                self.__settings.value(
                    "Variables",
                    "concentrationZNSCheck",
                    self.concentrationZNSCheckBox.checkState(),
                )
            )
        )
        self.concentrationZSCheckBox.setCheckState(
            int(
                self.__settings.value(
                    "Variables",
                    "concentrationZSCheck",
                    self.concentrationZSCheckBox.checkState(),
                )
            )
        )
        self.activityZNSCheckBox.setCheckState(
            int(
                self.__settings.value(
                    "Variables",
                    "activityZNSCheck",
                    self.activityZNSCheckBox.checkState(),
                )
            )
        )
        self.saturationZNSCheckBox.setCheckState(
            int(
                self.__settings.value(
                    "Variables",
                    "saturationZNSCheck",
                    self.saturationZNSCheckBox.checkState(),
                )
            )
        )

        self.darcyUnitComboBox.setCurrentIndex(
            self.darcyUnitComboBox.findText(
                self.__settings.value(
                    "Variables", "darcyUnit", self.darcyUnitComboBox.currentText()
                )
            )
        )
        self.concentrationUnitComboBox.setCurrentIndex(
            self.concentrationUnitComboBox.findText(
                self.__settings.value(
                    "Variables",
                    "concentrationUnit",
                    self.concentrationUnitComboBox.currentText(),
                )
            )
        )
        self.activityUnitComboBox.setCurrentIndex(
            self.activityUnitComboBox.findText(
                self.__settings.value(
                    "Variables", "activityUnit", self.activityUnitComboBox.currentText()
                )
            )
        )
        self.massUnitComboBox.setCurrentIndex(
            self.massUnitComboBox.findText(
                self.__settings.value(
                    "Variables", "massUnit", self.massUnitComboBox.currentText()
                )
            )
        )

        self.concentrationScaleLineEdit.setText(
            self.__settings.value(
                "Variables",
                "concentrationScale",
                self.concentrationScaleLineEdit.text(),
            )
        )
        self.darcyScaleLineEdit.setText(
            self.__settings.value(
                "Variables", "darcyScale", self.darcyScaleLineEdit.text()
            )
        )

        self.concentrationClassesSpinBox.setValue(
            int(
                self.__settings.value(
                    "Variables",
                    "concentrationClasses",
                    self.concentrationClassesSpinBox.value(),
                )
            )
        )
        self.potentielClassesSpinBox.setValue(
            int(
                self.__settings.value(
                    "Variables",
                    "potentielClasses",
                    self.potentielClassesSpinBox.value(),
                )
            )
        )
        self.darcyClassesSpinBox.setValue(
            int(
                self.__settings.value(
                    "Variables", "darcyClasses", self.darcyClassesSpinBox.value()
                )
            )
        )

        self.maskUnitsCheckBox.setCheckState(
            int(
                self.__settings.value(
                    "Variables", "maskUnitsCheck", self.maskUnitsCheckBox.checkState()
                )
            )
        )

        # setup animation parameters
        self.framePerSecondSpinBox.setValue(
            int(
                self.__settings.value(
                    "Animation", "framePerSecond", self.framePerSecondSpinBox.value()
                )
            )
        )

        # setup matplotlib parameters
        self.ticksLabelSizeSpinBox.setValue(
            int(
                self.__settings.value(
                    "Matplotlib", "ticksLabelSize", self.ticksLabelSizeSpinBox.value()
                )
            )
        )
        self.ylabelFontSizeSpinBox.setValue(
            int(
                self.__settings.value(
                    "Matplotlib", "ylabelFontSize", self.ylabelFontSizeSpinBox.value()
                )
            )
        )
        self.legendFontSizeSpinBox.setValue(
            int(
                self.__settings.value(
                    "Matplotlib", "legendFontSize", self.legendFontSizeSpinBox.value()
                )
            )
        )

        # setup database parameters
        self.remoteDbGroup.setChecked(
            str_to_bool(
                self.__settings.value(
                    "Database",
                    "useRemoteDb",
                    bool_to_str(self.remoteDbGroup.isChecked()),
                )
            )
        )
        self.dbConnectionEdit.setText(
            self.__settings.value(
                "Database", "connectionString", self.dbConnectionEdit.text()
            )
        )
        self.syncExpDbButton.clicked.connect(self.load_expdb_to_site_db)

    def _define_line_edit_from_setting(self, line_edit, section, value):
        line_edit.setText(self.__settings.value(section, value, line_edit.text()))

    def select_gmsh(self):
        filepath, filter_use = QFileDialog.getOpenFileName(
            self, "Select GMSH executable", self.gmshLineEdit.text()
        )
        if filepath:
            self.gmshLineEdit.setText(filepath)

    def select_openfoam(self):
        filepath = QFileDialog.getExistingDirectory(
            self, "Select openfoam directory", self.openfoamLineEdit.text()
        )
        if filepath:
            self.openfoamLineEdit.setText(filepath)

    def select_pmf(self):
        filepath = QFileDialog.getExistingDirectory(
            self, "Select PMF install directory", self.pmfLineEdit.text()
        )
        if filepath:
            self.pmfLineEdit.setText(filepath)

    def metisIsInLocalDir(self):
        """Check if Metis software exists in the local directory

        :return: state
        :rtype: bool
        """
        return os.path.exists(
            os.path.join(
                self.__settings.install_dir,
                "lin64" + os.sep + "metis"
                if sys.platform.startswith("linux")
                else "win32" + os.sep + "metis.exe",
            )
        )

    def mapsButtonClicked(self):
        """Triggered on map button click, to define map directory"""
        dlg = QFileDialog()
        dlg.setOption(QFileDialog.DontUseNativeDialog)
        dlg.setDirectory(self.mapsLineEdit.text())
        dlg.setWindowTitle("Fonds de carte")
        dlg.setFilter(QDir.Hidden | QDir.AllDirs)
        dlg.setFileMode(QFileDialog.Directory)
        if dlg.exec_():
            fil = dlg.selectedFiles()[0]
            self.mapsLineEdit.setText(fil)

    def mpiConfigButtonClicked(self):
        """Triggered on map button click, to define mpi directory"""
        dlg = QFileDialog()
        dlg.setOption(QFileDialog.DontUseNativeDialog)
        dlg.setDirectory(self.mpiConfigLineEdit.text())
        dlg.setWindowTitle("Fichier de configuration MPI")
        dlg.setFilter(QDir.Hidden | QDir.AllDirs | QDir.Files)
        dlg.setNameFilter("*.txt")
        if dlg.exec_():
            fil = dlg.selectedFiles()[0]
            self.mpiConfigLineEdit.setText(fil)

    def ZNSCheckBoxClicked(self):
        """Triggered on ZNS checkbox click, to define all dependent checkbox"""
        self.darcyZNSCheckBox.setCheckState(self.ZNSCheckBox.checkState())
        self.concentrationZNSCheckBox.setCheckState(self.ZNSCheckBox.checkState())
        self.activityZNSCheckBox.setCheckState(self.ZNSCheckBox.checkState())
        self.saturationZNSCheckBox.setCheckState(self.ZNSCheckBox.checkState())

    def ZSCheckBoxClicked(self):
        """Triggered on ZS checkbox click, to define all dependent checkbox"""
        self.potentielZSCheckBox.setCheckState(self.ZSCheckBox.checkState())
        self.darcyZSCheckBox.setCheckState(self.ZSCheckBox.checkState())
        self.concentrationZSCheckBox.setCheckState(self.ZSCheckBox.checkState())

    def load_expdb_to_site_db(self):
        dlg = QMessageBox(
            QMessageBox.Information,
            self.tr("Experimental database import"),
            self.tr(
                "This action will erase content of the following table :\n\
                          - points_interet\n\
                          - mesures\n\
                          - forages\n\
                          - stratigraphies\n\
                          - lithologies \n\
                          - fracturations\n\
                    Do you really want to do it ?"
            ),
            QMessageBox.Yes | QMessageBox.No,
        )
        return_value = dlg.exec()
        if return_value == QMessageBox.Yes:
            sitedb = os.path.join(os.path.expanduser("~"), ".thyrsis", "sites.sqlite")
            with connect(sitedb) as conn:
                cur = conn.cursor()
                cur.execute("DELETE FROM points_interet")
                cur.execute("DELETE FROM mesures")
                cur.execute("DELETE FROM forages")
                cur.execute("DELETE FROM stratigraphies")
                cur.execute("DELETE FROM lithologies")
                cur.execute("DELETE FROM fracturations")
                conn.commit()
                cur.execute("SELECT id, nom FROM sites")
                sites = cur.fetchall()

                for sid, nom in sites:
                    print("Try import site : {}".format(nom))
                    _import_site_from_exp_db(
                        self.dbConnectionEdit.text(), cur, sid, 4326, nom, True
                    )
                conn.commit()

    def okButtonClicked(self):
        """Triggered on ok button click, to save preference"""
        # setup general values
        self.__settings.setValue(
            "General", "codeHydro", self.codeHydroComboBox.currentText().lower()
        )
        if self.codeHydroComboBox.currentText() == "METIS":
            if self.metisIsInLocalDir():
                codeHydroDir = os.path.join(
                    self.__settings.install_dir,
                    "lin64" if sys.platform.startswith("linux") else "win32",
                )
                self.__settings.setValue(
                    "General",
                    "codeHydroCommand",
                    os.path.join(
                        codeHydroDir,
                        "metis" if sys.platform.startswith("linux") else "metis.exe",
                    ),
                )
            else:
                self.__settings.setValue(
                    "General",
                    "codeHydroCommand",
                    find_executable("metis")
                    if sys.platform.startswith("linux")
                    else os.path.abspath(find_executable("metis.exe")),
                )
        elif self.codeHydroComboBox.currentText() == "OPENFOAM":
            self.__settings.setValue("General", "codeHydroCommand", "")
        else:
            self.__settings.setValue(
                "General",
                "codeHydroCommand",
                find_executable(self.codeHydroComboBox.currentText().lower()),
            )

        self.__settings.setValue(
            "General", "meshGenerator", self.meshGeneratorComboBox.currentText().lower()
        )

        self.__settings.setValue("General", "gmsh", self.gmshLineEdit.text())
        self.__settings.setValue("General", "openfoam", self.openfoamLineEdit.text())
        self.__settings.setValue("General", "pmf", self.pmfLineEdit.text())

        self.__settings.setValue("General", "maps", self.mapsLineEdit.text())

        self.__settings.setValue("General", "mpiConfig", self.mpiConfigLineEdit.text())
        self.__settings.setValue(
            "General", "nbMpiProcess", self.nbMpiProcessSpinBox.value()
        )
        self.__settings.setValue(
            "General", "nbColumnsMax", self.nbColumnsMaxSpinBox.value()
        )

        self.__settings.setValue(
            "General", "console", self.consoleCheckBox.checkState()
        )
        self.__settings.setValue("General", "debug", self.debugCheckBox.checkState())

        # setup Variables values
        self.__settings.setValue(
            "Variables", "potentielZSCheck", self.potentielZSCheckBox.checkState()
        )
        self.__settings.setValue(
            "Variables", "darcyZNSCheck", self.darcyZNSCheckBox.checkState()
        )
        self.__settings.setValue(
            "Variables", "darcyZSCheck", self.darcyZSCheckBox.checkState()
        )
        self.__settings.setValue(
            "Variables",
            "concentrationZNSCheck",
            self.concentrationZNSCheckBox.checkState(),
        )
        self.__settings.setValue(
            "Variables",
            "concentrationZSCheck",
            self.concentrationZSCheckBox.checkState(),
        )
        self.__settings.setValue(
            "Variables", "activityZNSCheck", self.activityZNSCheckBox.checkState()
        )
        self.__settings.setValue(
            "Variables", "saturationZNSCheck", self.saturationZNSCheckBox.checkState()
        )
        # ajout debit
        self.__settings.setValue("Variables", "debitZSCheck", 1)

        self.__settings.setValue("Variables", "potentielUnit", "m")
        self.__settings.setValue(
            "Variables", "darcyUnit", self.darcyUnitComboBox.currentText()
        )
        self.__settings.setValue(
            "Variables",
            "concentrationUnit",
            self.concentrationUnitComboBox.currentText(),
        )
        self.__settings.setValue(
            "Variables", "activityUnit", self.activityUnitComboBox.currentText()
        )
        self.__settings.setValue(
            "Variables", "massUnit", self.massUnitComboBox.currentText()
        )

        self.__settings.setValue(
            "Variables", "concentrationScale", self.concentrationScaleLineEdit.text()
        )
        self.__settings.setValue(
            "Variables", "darcyScale", self.darcyScaleLineEdit.text()
        )

        self.__settings.setValue(
            "Variables",
            "concentrationClasses",
            self.concentrationClassesSpinBox.value(),
        )
        self.__settings.setValue(
            "Variables", "potentielClasses", self.potentielClassesSpinBox.value()
        )
        self.__settings.setValue(
            "Variables", "darcyClasses", self.darcyClassesSpinBox.value()
        )

        self.__settings.setValue(
            "Variables", "maskUnitsCheck", self.maskUnitsCheckBox.checkState()
        )

        self.__settings.setValue(
            "Animation", "framePerSecond", self.framePerSecondSpinBox.value()
        )

        self.__settings.setValue(
            "Matplotlib", "ticksLabelSize", self.ticksLabelSizeSpinBox.value()
        )
        self.__settings.setValue(
            "Matplotlib", "ylabelFontSize", self.ylabelFontSizeSpinBox.value()
        )
        self.__settings.setValue(
            "Matplotlib", "legendFontSize", self.legendFontSizeSpinBox.value()
        )

        self.__settings.setValue(
            "Database", "useRemoteDb", bool_to_str(self.remoteDbGroup.isChecked())
        )
        self.__settings.setValue(
            "Database", "connectionString", self.dbConnectionEdit.text()
        )

        self.__settings.save(self.__databasePath)
        self.__settingsModified = True

        self.close()

    def cancelButtonClicked(self):
        """Triggered on cancel button click, close dialog without save"""
        self.close()

    def getSettingsModified(self):
        return self.__settingsModified


if __name__ == "__main__":

    QCoreApplication.setOrganizationName("QGIS")
    QCoreApplication.setOrganizationDomain("qgis.org")
    QCoreApplication.setApplicationName("QGIS2")

    app = QApplication(sys.argv)

    settings = Settings()
    dlg = PreferenceDialog(settings)
    dlg.load_expdb_to_site_db()
    # dlg.exec_()
    # dlg.show()
    # app.exec_()
