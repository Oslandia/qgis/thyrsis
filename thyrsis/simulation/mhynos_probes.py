"""This module computes results for probes

    python -m thyrsis.simulation.mhynos_probes [-v] db.sqlite

"""
import os

from thyrsis.exception import SimulationError

from ..database import sqlite
from ..log import logger
from ..spatialitemeshdataprovider import (
    SpatialiteMeshDataProvider,
    _getElementContaining,
)
from ..utilities import read_one_column_file
from .mhynos_database import get_simulation_data


def mhynos_probes(database, MPI=None):
    """Read ouput file and update database

    :param database: spatialite database
    :type database: spatialite file
    :param MPI: MPI parameter
    :type MPI: MPI
    """
    if MPI is not None and MPI.COMM_WORLD.Get_rank() != 0:
        return

    logger.debug("\nmhynos_probes")
    logger.notice("\nPROBES")

    # probes
    with sqlite.connect(database) as conn:
        simulation_data = get_simulation_data(conn)
        cur = conn.cursor()
        points = cur.execute(
            """SELECT nom, X(GEOMETRY), Y(GEOMETRY)
             FROM points_interet WHERE groupe='calcul' ORDER BY nom"""
        ).fetchall()
        # removing points outside the domain
        for i, pt in enumerate(points):
            if not _getElementContaining(conn, pt[1:]):
                points.pop(i)
        project_SRID = str(
            cur.execute(
                "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
            ).fetchone()[0]
        )

    column = (
        "concentration"
        if simulation_data["TYPE_INJECTION"] != "aucune"
        else "potentiel"
    )
    provider = SpatialiteMeshDataProvider(
        "dbname=%s crs=epsg:%s resultColumn=%s" % (database, project_SRID, column)
    )
    unit = "kg/m³" if column == "concentration" else "m"
    logger.notice("unit set to", unit)
    provider.setUnits(unit)

    dates_string = provider.dates()
    database_dir = os.path.dirname(os.path.abspath(database))
    tmpdir = os.path.join(database_dir, os.path.basename(database)[:-7] + "_tmp")
    date_simulation_file = os.path.join(tmpdir, "dates_simulation.txt")
    dates_seconds = read_one_column_file(date_simulation_file)
    if len(dates_seconds) != len(dates_string):
        raise SimulationError(
            f"Invald number of simulation date in {date_simulation_file}. {len(dates_string)} expected. {len(dates_seconds)} found."
        )

    probes_file = os.path.join(database_dir, column + ".probes")
    with open(probes_file, "w") as fil:
        fil.write(" %s" % ("Time"))
        values = {}
        for pt in points:
            fil.write(" %s" % (pt[0]))
            values[pt[0]] = provider.valuesAt(pt[1:])
        fil.write("\n")
        for i, d in enumerate(dates_string):
            fil.write("%15.7E" % (dates_seconds[i]))
            for pt in points:
                fil.write("%15.7E" % (values[pt[0]][i]))
            fil.write("\n")


# run as script if invoqued as such
if __name__ == "__main__":
    import sys

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    logger.enable_console(True)

    if len(sys.argv) not in [2]:
        logger.error("wrong number of arguments (try compute.py --help)")
        exit(1)

    verbose = False
    dbname = None
    for arg in sys.argv[1:]:
        if arg == "-v":
            verbose = True
        elif arg[-7:] == ".sqlite":
            dbname = arg

    if not dbname:
        logger.error(
            "cannot find a '.sqlite' file in arguments (try python -m thyrsis.simulation.mhynos_probes --help)"
        )
        exit(1)

    mhynos_probes(dbname)
