# Radionucléides

THYRSIS tient compte de la décroissance radioactive et de l'activité spécifique des radionucléides. La décroissance radioactive est utilisée dans les simulations de transport et l'activité spécifique est utilisée pour convertir les concentrations en activités.

Ces valeurs sont lues dans la table **elements_chimiques**, qui peut être remplie à l'aide de la commande :

```bash
python -m thyrsis.database.load_chemicals radionuclides.csv
```

Le fichier `radionuclides.csv` est fourni dans le répertoire `thyrsis/tutorial_data` directory. C'est un fichier csv avec séparateur `;` contenant les colonnes **ordonnées** suivantes:

- `nom`
- `période`
- `masse atomique`
- `décroissance radioactive`
- `activité spécifique`
- `limite de qualité des eaux`
- `numéro atomique`
- `nombre de masse`
