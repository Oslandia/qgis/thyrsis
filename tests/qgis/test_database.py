"""Thyrsis unit test for database utilities

.. note:: This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
"""

import os
import shutil
import unittest
from pathlib import Path

import pytest

from thyrsis import database
from thyrsis.database import sqlite

TEST_DIR = os.path.join(os.path.dirname(__file__), os.path.splitext(__file__)[0])
TUTORIAL_DATA_DIR = os.path.join(os.path.dirname(__file__), "../..", "tutorial_data")
TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "../..", "test_data")

os.environ["HOME"] = TEST_DIR

if os.path.exists(TEST_DIR):
    shutil.rmtree(TEST_DIR)
os.mkdir(TEST_DIR)

SITE_DB = os.path.join(TEST_DIR, ".thyrsis", "sites.sqlite")
MESH_DB = os.path.join(TEST_DIR, ".thyrsis", "mesh", "SACLAY_mesh.mesh.sqlite")
COMPUTATION_DB = os.path.join(TEST_DIR, ".thyrsis", "etude", "SACLAY_mesh.mesh.sqlite")


@pytest.fixture()
def test_dir() -> str:
    return TEST_DIR


@pytest.fixture()
def mesh_db() -> str:
    return MESH_DB


@pytest.fixture()
def site_db() -> str:
    return SITE_DB


@pytest.fixture()
def create_site_db(site_db):
    """
    Fixture to create site database

    @param site_db: path to site database
    """
    Path(site_db).parent.mkdir(parents=True, exist_ok=True)

    with sqlite.connect(site_db, "ThyrsisDatabaseTest") as conn:
        cur = conn.cursor()
        cur.execute("SELECT InitSpatialMetaData(1)")
        database.create_site_tables(cur, with_cid=True, project_SRID=4326)
        conn.commit()


@pytest.fixture()
def create_mesh_db(mesh_db):
    """
    Fixture to create mesh database

    @param mesh_db: path to mesh database
    """

    Path(mesh_db).parent.mkdir(parents=True, exist_ok=True)

    with sqlite.connect(mesh_db, "ThyrsisDatabaseTest") as conn:
        cur = conn.cursor()
        cur.execute("SELECT InitSpatialMetaData(1)")
        database.create_mesh_tables(cur, project_SRID=2154)
        conn.commit()


def test_create_site_db(clean_test_dir, create_site_db):
    """
    Test site database creation

    @param clean_test_dir: fixture to have a clean test directory
    @param create_site_db: fixture to create a site database
    """
    assert os.path.exists(SITE_DB)
    with sqlite.connect(SITE_DB, "ThyrsisDatabaseTest") as conn:
        cur = conn.cursor()

        res = cur.execute(
            "SELECT name FROM sqlite_master WHERE type='table'"
        ).fetchall()
        # Number of expected table depends on current spatialite version (depending on QGIS version)
        assert len(res) == 47 or len(res) == 45


def test_load_chemicals_forages_csv(
    clean_test_dir,
    create_site_db,
    load_chemicals_csv,
    insert_saclay_site,
    load_forages_csv,
):
    """
    Test chemicals and forage load from csv

    @param clean_test_dir: fixture to have a clean test directory
    @param create_site_db: fixture to create a site database
    @param load_chemicals_csv: fixture to insert chemical into site database
    @param insert_saclay_site: fixture to insert saclay site
    @param load_forages_csv: fixture to insert forage into site database
    """
    with sqlite.connect(SITE_DB, "ThyrsisDatabaseTest") as conn:
        cur = conn.cursor()

        res = cur.execute("SELECT COUNT(*) FROM elements_chimiques").fetchone()[0]
        assert res == 617

        res = cur.execute("SELECT COUNT(*) FROM sites").fetchone()[0]
        assert res == 1

        res = cur.execute("SELECT COUNT(*) FROM forages").fetchone()[0]
        assert res == 17


def test_create_mesh_db(clean_test_dir, create_mesh_db):
    """
    Test mesh database creation

    @param clean_test_dir: fixture to have a clean test directory
    @param create_mesh_db: fixture to create a mesh database
    """
    assert os.path.exists(MESH_DB)
    with sqlite.connect(MESH_DB, "ThyrsisDatabaseTest") as conn:
        cur = conn.cursor()

        res = cur.execute(
            "SELECT name FROM sqlite_master WHERE type='table'"
        ).fetchall()
        # Number of expected table depends on current spatialite version (depending on QGIS version)
        assert len(res) == 73 or len(res) == 71

        srid = cur.execute(
            "SELECT srid FROM geometry_columns WHERE f_table_name LIKE 'noeuds'"
        ).fetchone()[0]
        assert srid == 2154


def test_create_compute_db(
    clean_test_dir, create_mesh_db, create_site_db, insert_saclay_site
):
    """
    Test computation database creation

    @param clean_test_dir: fixture to have a clean test directory
    @param create_mesh_db: fixture to create a mesh database
    @param create_site_db: fixture to create a site database
    @param insert_saclay_site: fixture to insert saclay site
    """
    os.mkdir(os.path.join(TEST_DIR, ".thyrsis", "etude"))
    database.create_computation_database(COMPUTATION_DB, MESH_DB)
    assert os.path.exists(COMPUTATION_DB)
    with sqlite.connect(COMPUTATION_DB, "ThyrsisDatabaseTest") as conn:
        cur = conn.cursor()
        res = cur.execute(
            "SELECT name FROM sqlite_master WHERE type='table'"
        ).fetchall()
        # Number of expected table depends on current spatialite version (depending on QGIS version)
        assert len(res) == 112 or len(res) == 110

        srid = cur.execute(
            "SELECT srid FROM geometry_columns WHERE f_table_name LIKE 'noeuds'"
        ).fetchone()[0]
        assert srid == 2154


if __name__ == "__main__":
    unittest.main()
