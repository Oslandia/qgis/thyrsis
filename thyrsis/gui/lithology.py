import math
import os
from builtins import range

from qgis.core import QgsDataSourceUri
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QObject, Qt
from qgis.PyQt.QtGui import QBrush, QColor, QImage, QPen, QWheelEvent
from qgis.PyQt.QtWidgets import QGraphicsScene, QMainWindow

from ..database import sqlite

# coding=utf-8


class BoreHoleScene(QGraphicsScene):
    """Borehole scene (rectangle) to display lithology"""

    def __init__(self, uri, parent=None):
        """Constructor

        :param uri: source
        :type uri: string
        """
        super(BoreHoleScene, self).__init__(parent)
        self.__uri = QgsDataSourceUri(uri)
        self.__id = None
        self.m_per_pixel = -0.2

        self.__color = {
            "q": QColor(249, 249, 127),
            "q4": QColor(254, 242, 236),
            "q3": QColor(255, 242, 211),
            "q2": QColor(255, 242, 199),
            "q1": QColor(255, 242, 186),
            "p": QColor(255, 255, 153),
            "p3": QColor(255, 237, 179),
            "p2": QColor(255, 255, 191),
            "p1": QColor(255, 255, 179),
            "m": QColor(255, 255, 0),
            "m6": QColor(255, 255, 115),
            "m5": QColor(255, 255, 102),
            "m4": QColor(255, 255, 89),
            "m3": QColor(255, 255, 77),
            "m2": QColor(255, 255, 65),
            "m1": QColor(255, 255, 51),
            "g": QColor(253, 192, 122),
            "g2": QColor(254, 230, 170),
            "g1": QColor(254, 217, 154),
            "e": QColor(253, 180, 108),
            "e7": QColor(253, 205, 161),
            "e6": QColor(253, 192, 145),
            "e5": QColor(252, 180, 130),
            "e4": QColor(252, 167, 115),
            "e3": QColor(253, 191, 111),
            "e2": QColor(254, 191, 101),
            "e1": QColor(253, 180, 98),
            "c": QColor(166, 216, 74),
            "c6": QColor(242, 250, 140),
            "c5": QColor(230, 244, 127),
            "c4": QColor(217, 239, 116),
            "c3": QColor(204, 233, 104),
            "c2": QColor(191, 227, 93),
            "c1": QColor(179, 222, 83),
            "n": QColor(140, 205, 87),
            "n6": QColor(204, 234, 151),
            "n5": QColor(191, 228, 138),
            "n4": QColor(179, 223, 127),
            "n3": QColor(166, 217, 117),
            "n2": QColor(153, 211, 106),
            "n1": QColor(140, 205, 96),
            "j": QColor(179, 227, 238),
            "j7": QColor(217, 241, 247),
            "j6": QColor(204, 236, 244),
            "j5": QColor(191, 231, 241),
            "j4": QColor(191, 231, 229),
            "j3": QColor(179, 226, 227),
            "j2": QColor(166, 221, 224),
            "j1": QColor(154, 217, 221),
            "l": QColor(66, 174, 208),
            "l4": QColor(153, 206, 227),
            "l3": QColor(128, 197, 221),
            "l2": QColor(103, 188, 216),
            "l1": QColor(78, 179, 211),
            "i": QColor(66, 174, 208),
            "i4": QColor(153, 206, 227),
            "i3": QColor(128, 197, 221),
            "i2": QColor(103, 188, 216),
            "i1": QColor(78, 179, 211),
        }
        self.__redraw = True

    #    def texture(self, code):
    #        return self.__textures[code] if code in self.__textures else QImage()
    def texture(self, code):
        """Return image to use for lithology texture

        :param code: lithology code
        :type code: string

        :return: texture
        :rtype: QImage
        """
        fil = os.path.join(
            os.path.dirname(__file__), "..", "styles", "qgis_well_log", code + ".svg"
        )
        if os.path.exists(fil):
            return QImage(fil)
        else:
            return QImage()

    def color(self, code):
        """Return color if it is a recognized code

        :param code: lithology code
        :type code: string

        :return: texture
        :rtype: QImage
        """
        return (
            self.__color[code[:2].lower()]
            if code[:2].lower() in self.__color
            else QColor(255, 255, 255)
        )

    def set_m_per_pixel(self, m_per_pixel):
        """set meter per pixel

        :param m_per_pixel: scale to use for display
        :type m_per_pixel: int
        """
        self.m_per_pixel = m_per_pixel
        self.__redraw = True
        self.invalidate()

    def set_current_id(self, id_):
        """set id

        :param id_: id (OGC_id) of the feature to display
        :type id_: int
        """
        self.__id = id_
        self.__redraw = True
        self.invalidate()

    def drawForeground(self, painter, rect):
        """Draw the lithologic foreground form the db data

        :param painter: low-level painting on widget device
        :type painter: QPainter
        :param rect: rectangle to paint on
        :type rect: QRectF
        """
        # print "BoreHoleScene.drawForeground"
        if self.__redraw:
            self.__redraw = False
            self.clear()
            fm = painter.fontMetrics()

            if self.__id is None:
                QGraphicsScene.drawForeground(self, painter, rect)
                return

            with sqlite.connect(self.__uri.database()) as conn:
                cur = conn.cursor()
                res = cur.execute(
                    "SELECT AsText(GEOMETRY), zsol, nom FROM forages WHERE OGC_FID={}".format(
                        self.__id
                    )
                ).fetchone()

                if not res:
                    QGraphicsScene.drawForeground(self, painter, rect)
                    return

                fract = cur.execute(
                    "SELECT AsText(GEOMETRY), taux/100 FROM fracturations WHERE forage={}".format(
                        self.__id
                    )
                ).fetchall()
                fracturation = len(fract) > 0

                geom, zsol, nom = res
                line = [
                    (float(pt.split()[2]) - zsol)
                    for pt in geom.replace("LINESTRING Z(", "")
                    .replace(")", "")
                    .split(",")
                ]
                tick_width = 20
                spacing = 5
                tick_text_offset = -10
                tabs = [40, 45, 100, 150, 250] if fracturation else [40, 45, 100, 150]
                zmin, zmax = (
                    max(line) / self.m_per_pixel,
                    (min(line) - 5) / self.m_per_pixel,
                )

                text = self.addText(nom)
                text.setPos(tabs[1], zmin - 5 * fm.height())

                label = self.tr("Depth [m]")
                text = self.addText(label)
                text.setRotation(-90)
                text.setPos(0, fm.width(label))
                text = self.addText(self.tr("stratigraphic\nlogs"))
                text.setPos(tabs[1], zmin - 3 * fm.height())
                if fracturation:
                    text = self.addText(self.tr("Fracturing\nrate"))
                    text.setPos(tabs[3], zmin - 3 * fm.height())

                top = zmin - 3 * fm.height()
                pen = QPen()
                pen.setWidth(3)
                ytabs = (
                    [tabs[1], tabs[3], tabs[4]] if fracturation else [tabs[1], tabs[3]]
                )
                for tab in ytabs:
                    self.addLine(tab, top, tab, zmax, pen)
                self.addLine(tabs[1], zmin, tabs[-1], zmin, pen)
                self.addLine(tabs[1], zmax, tabs[-1], zmax, pen)
                self.addLine(tabs[1], top, tabs[-1], top, pen)

                # depth ticks
                for z in range(math.ceil(max(line) / 10) * 10, int(min(line) - 5), -10):
                    text = "% 4.0f" % (-z)
                    z /= self.m_per_pixel
                    width = fm.width(text)
                    text = self.addText(text)
                    text.setPos(tabs[0] - width - spacing, tick_text_offset + int(z))
                    self.addLine(tabs[0], z, tabs[1], z)
                    if fracturation:
                        self.addLine(tabs[3], z, tabs[4], z)

                # strati color
                res = cur.execute(
                    "SELECT AsText(GEOMETRY), code FROM stratigraphies WHERE forage={}".format(
                        self.__id
                    )
                ).fetchall()

                for geom, code in res:
                    line = [
                        (float(pt.split()[2]) - zsol)
                        for pt in geom.replace("LINESTRING Z(", "")
                        .replace(")", "")
                        .split(",")
                    ]
                    z_start = line[0] / self.m_per_pixel
                    z_end = line[-1] / self.m_per_pixel
                    brush = QBrush()
                    brush.setStyle(Qt.SolidPattern)
                    brush.setColor(self.color(code))
                    self.addRect(
                        tabs[1],
                        z_start,
                        tabs[2] - tabs[1],
                        z_end - z_start,
                        brush=brush,
                    )
                    width = fm.width(code)
                    text = self.addText(code)
                    text.setPos(
                        tabs[2] + spacing,
                        tick_text_offset + int(0.5 * (z_start + z_end)),
                    )
                    self.addLine(tabs[2], z_start, tabs[3], z_start)
                    self.addLine(tabs[2], z_end, tabs[3], z_end)

                # litho image
                res = cur.execute(
                    "SELECT AsText(GEOMETRY), code FROM lithologies WHERE forage={}".format(
                        self.__id
                    )
                ).fetchall()

                for geom, code in res:
                    line = [
                        (float(pt.split()[2]) - zsol)
                        for pt in geom.replace("LINESTRING Z(", "")
                        .replace(")", "")
                        .split(",")
                    ]
                    z_start = line[0] / self.m_per_pixel
                    z_end = line[-1] / self.m_per_pixel
                    brush = QBrush()
                    brush.setTextureImage(self.texture(code))
                    self.addRect(
                        tabs[1],
                        z_start,
                        tabs[2] - tabs[1],
                        z_end - z_start,
                        brush=brush,
                    )

            # fracturation diagram
            if fracturation:
                # bar diagram grid
                for i in range(1, 10):
                    pen.setWidth(1 if i != 5 else 2)
                    x = tabs[3] + (tabs[4] - tabs[3]) * float(i) / 10
                    self.addLine(x, zmin, x, zmax, pen)

                for geom, taux in fract:
                    line = [
                        (float(pt.split()[2]) - zsol)
                        for pt in geom.replace("LINESTRING Z(", "")
                        .replace(")", "")
                        .split(",")
                    ]
                    z_start = line[0] / self.m_per_pixel
                    z_end = line[-1] / self.m_per_pixel
                    brush = QBrush()
                    brush.setStyle(Qt.SolidPattern)
                    brush.setColor(QColor(155, 51, 49))
                    self.addRect(
                        tabs[3],
                        z_start,
                        (tabs[4] - tabs[3]) * taux,
                        z_end - z_start,
                        brush=brush,
                    )

            self.setSceneRect(self.itemsBoundingRect())

        QGraphicsScene.drawForeground(self, painter, rect)

    class ScrollFilter(QObject):
        def __init__(self, parent):
            super(BoreHoleScene.ScrollFilter, self).__init__(parent)

        def eventFilter(self, obj, event):
            if isinstance(event, QWheelEvent):
                if event.angleDelta().y() < 0:
                    self.parent().set_m_per_pixel(self.parent().m_per_pixel * 1.2)
                else:
                    self.parent().set_m_per_pixel(self.parent().m_per_pixel * 0.8)
            return False

    def scroll_filter(self):
        """Scroll filter, defined for the borehole window"""
        self.__scroll_filter = BoreHoleScene.ScrollFilter(self)
        return self.__scroll_filter


class BoreHoleWindow(QMainWindow):
    """Borehole panel where several lithology can be displayed"""

    def __init__(self, uri, logger, parent=None):
        """Constructor

        :param uri: source
        :type uri: string
        :param logger: logger object
        :type logger: Logger
        """
        super(BoreHoleWindow, self).__init__(parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "lithology.ui"), self)
        self.scene = BoreHoleScene(uri, self)
        self.graphicsView.setScene(self.scene)
        self.graphicsView.installEventFilter(self.scene.scroll_filter())


if __name__ == "__main__":
    import sys

    from qgis.PyQt.QtWidgets import QApplication

    from thyrsis.utilities import ConsoleLogger

    app = QApplication(sys.argv)

    view = BoreHoleWindow("dbname=" + sys.argv[1], ConsoleLogger())
    view.scene.set_current_id(int(sys.argv[2]))
    view.show()

    app.exec_()
