# Contributors

Here are the main contributors for THYRSIS.

|Role|Contributors|
|------|-------|
|Conceptualization|François RENARD (CEA), Vincent MORA (Oslandia)|
|Funding Acquisition|François RENARD (CEA)|
|Investigation|François RENARD (CEA)|
|Methodology|François RENARD (CEA)|
|Project Administration|François RENARD (CEA), Jean-Marie KERLOCH (Oslandia)|
|Resources|François RENARD (CEA)|
|Software architecture|François RENARD (CEA), Vincent MORA (Oslandia)|
|Software development|François RENARD (CEA), Vincent MORA (Oslandia), Sébastien PEILLET (Oslandia), Quy Thy TRUONG (Oslandia), Jean-Marie KERLOCH (Oslandia)|
|Software quality|Julien MOURA (Oslandia), Jean-Marie KERLOCH (Oslandia)|
|Software documentation|François RENARD (CEA), Julien MOURA (Oslandia)|
|Validation|François RENARD (CEA), Jean-Marie KERLOCH (Oslandia)|
