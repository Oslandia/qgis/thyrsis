"""Creates all databases and run computations for templates defined in /home/user/.thyrsis/compute_validation.csv
    This file is a csv file with ';' separators and should have the columns :
    - template : str, name of the template
    - point : str, name of the point (from table points_interet)
    - date : str, date with format 'YYYY-MM-DD'
    - unit : str, unit
    - value : float, the value to test
    - phiReconstruction : true or false
    - comment : str
    the first line is the header with names of columns :
    'template;point;date;unit;value;comment'

USAGE:
    python -m thyrsis.simulation.compute_validation
        [-dir=DIRNAME -raz -init -run -plot -results -a -m -mts -of -oy -d] [template] [-template] [-code=CODENAME_LIST]

OPTIONS

   -dir : name of results directory [default: thyrsis_compute_validation_AAMMJJ]
   -raz : delete previous results directory
   -init : create databases
   -run : run simulations (compute)
   -results : calculate results
   -plot : plot results
   -m : add measures to plots
   -mts : add direct results from METIS to plots
   -of : add direct results from OpenFOAM to plots
   -oy : optimize vertical adjustment
   -a : all : init, compute, results and plot
   template: ne traite que le cas template
   -template: ne traite pas le cas template
   -code : nom du code de calcul, sinon code déterminé par Settings
   -d : display information about name and offset
"""
import os
import shutil
import sys
from builtins import str
from datetime import date, datetime
from shutil import which
import matplotlib.pyplot as plt
from scipy import interpolate, optimize
import numpy

import pandas as pd

from ..database import sqlite
from ..log import logger
from ..settings import Settings
from ..spatialitemeshdataprovider import SpatialiteMeshDataProvider
from ..utilities import Timer
from .compute import compute_mpi

AN2S = 3.15576e7

def compute_validation(topdir, init, run, resu, plot, measures, mts, of, opty, results, codes, raz, display=True):

    settings = Settings()
    top_dir = os.path.join(os.getcwd(), topdir)
    if raz and os.path.exists(top_dir):
        logger.notice("removing directory %s" % (top_dir))
        shutil.rmtree(top_dir)
    if not os.path.exists(top_dir):
        logger.notice("creating directory %s" % (top_dir))
        os.mkdir(top_dir)

    thyrsis_dir = os.environ["THYRSIS"]
    data_dir = os.path.join(thyrsis_dir, "data")

    report_path = os.path.join(top_dir, "report.csv")
    if os.path.isfile(report_path):
        logger.notice("reading file %s" % (report_path))
        report = pd.read_csv(report_path, sep=";")
        report.set_index("template", inplace=True)
    else:
        logger.notice("creating file %s" % (report_path))
        l = [
            [code + "_value", code + "_difference_%", code + "_cputime"]
            for code in codes
        ]
        report = pd.DataFrame(
            columns=["template", "reference"] + [y for x in l for y in x] + ["comment"]
        )
        report["template"] = sorted(results.keys())
        report["reference"] = [
            results[template]["value"] for template in sorted(results.keys())
        ]
        report["comment"] = [
            results[template]["comment"] for template in sorted(results.keys())
        ]
        report.set_index("template", inplace=True)

    timer = Timer()
    for template in sorted(results.keys()):
        logger.notice("\n" + template)
        sitename = template.split("_")[0]

        template_dir = os.path.join(top_dir, template)
        if not os.path.exists(template_dir):
            os.mkdir(template_dir)
        for code in codes:

            cputime = ""

            logger.notice("setting code to", code)
            settings.setValue("General", "codeHydro", code)
            settings.setValue("General", "codeHydroCommand", codes[code])
            settings.save()

            code_dir = os.path.join(template_dir, code)
            dbname = os.path.join(code_dir, template.lower() + ".sqlite")
            if init:
                if os.path.exists(code_dir):
                    shutil.rmtree(code_dir)
                os.makedirs(code_dir)
                tpltname = template.split("_")[2]
                if "TRANSITOIRE" in tpltname:
                    u_infiltration = os.path.join(
                        data_dir, sitename, "u_infiltration_transitoire.txt"
                    )
                    if os.path.exists(u_infiltration):
                        if "ZNS" in tpltname:
                            shutil.copy2(
                                u_infiltration,
                                os.path.join(
                                    code_dir, "u_infiltration_transitoire.insat"
                                ),
                            )
                        elif "ZS" in tpltname:
                            shutil.copy2(
                                u_infiltration,
                                os.path.join(
                                    code_dir, "u_infiltration_transitoire.sat"
                                ),
                            )
                    else:
                        logger.error("infiltration file missing")
                        continue

                #    if sitename == 'SACLAY' and init:
                #        u_ezns = os.path.join(install_dir, 'sites', sitename, 'input', 'u_epaisseurZNS.sat')
                #        if os.path.exists(u_ezns):
                #            shutil.copy2(u_ezns, code_dir)
                #        else:
                #            logger.error(u"fichier d'épaisseur ZNS absent")

                os.system("python -m thyrsis.database " + template + " " + dbname)
                logger.notice("\ninitializing in " + code_dir + "\n")
                compute_mpi(dbname, True, False)

                # set phiReconstruction false; in openfoam saturated parameters file (for old cases)
                tmp_dir = os.path.join(code_dir, template.lower() + "_tmp")
                if os.path.exists(tmp_dir) and code == "openfoam" and not results[template]["phiReconstruction"]:
                    logger.notice("\nsetting phiReconstruction to false\n")
                    for sample_dir in os.listdir(tmp_dir):
                        if "sample" in sample_dir:
                            param_file = os.path.join(tmp_dir, sample_dir, "sature", "parameters.txt")
                            tmp_param_file = os.path.join(tmp_dir, sample_dir, "sature", "tmp_parameters.txt")
                            if os.path.exists(param_file):
                                logger.notice("\nrenaming parameters.txt\n")
                                os.rename(param_file, tmp_param_file)
                                with open(tmp_param_file) as filin, open(param_file, "w") as fil:
                                    for line in filin:
                                        if "phiReconstruction" in line:
                                            fil.write(line.replace("true", "false"))
                                        else:
                                            fil.write(line)

            if run:
                timer.reset("")
                if not os.path.exists(code_dir):
                    exit("initializing has to be done first")
                logger.notice("\nrunning in " + code_dir + "\n")
                compute_mpi(dbname, False, True)
                cputime = timer.reset("")

            if resu:
                if not os.path.exists(code_dir):
                    exit("initializing and running have to be done first")
                with sqlite.connect(dbname) as conn:
                    cur = conn.cursor()
                    project_SRID = str(
                        cur.execute(
                            "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
                        ).fetchone()[0]
                    )
                    xy = cur.execute(
                        "SELECT X(GEOMETRY), Y(GEOMETRY) FROM points_interet WHERE nom='%s'"
                        % (results[template]["point"])
                    ).fetchone()
                notice = (
                    results[template]["point"]
                    + " "
                    + str(xy[0])
                    + " "
                    + str(xy[1])
                    + " "
                    + results[template]["date"]
                )
                logger.notice(template)
                logger.notice(notice)
                column = (
                    "potentiel" if results[template]["unit"] == "m" else "concentration"
                )
                provider = SpatialiteMeshDataProvider(
                    "dbname=%s crs=epsg:%s resultColumn=%s"
                    % (dbname, project_SRID, column)
                )
                provider.setUnits(results[template]["unit"])
                (date_idx,) = [
                    i
                    for i, d in enumerate(provider.dates())
                    if d[:10] == results[template]["date"]
                ]
                # print xy, date_idx, provider.dates()[date_idx]
                value = provider.valuesAt(xy)[date_idx]
                vref = results[template]["value"]
                d = 100 * (value - vref) / vref
                notice = "%.5f;%.1f;%s;" % (value, d, cputime.strip())
                report.loc[
                    template,
                    [code + "_value", code + "_difference_%", code + "_cputime"],
                ] = [value, d, cputime.strip()]
                report[code + "_value"] = report[code + "_value"].astype(float)
                report[code + "_difference_%"] = report[code + "_difference_%"].astype(
                    float
                )
                logger.notice("%.5f;" % (vref) + notice + "\n")
                report.to_csv(
                    report_path,
                    encoding="utf-8",
                    index=True,
                    header=True,
                    sep=";",
                    float_format="%.5f",
                )

        if plot:
            create_plots(template_dir, codes, measures, mts, of, display)


def create_plots(template_dir, codes, measures, mts, of, display):

    create_plots_zns(template_dir, codes)
    create_plots_probes(template_dir, codes, measures, mts, of, opty, display)


def create_plots_zns(template_dir, codes):
    logger.notice("creating plots for zns")
    dpisize = 100
    dzns = {}
    sat_dir = {}
    lzns = None
    loutput = None
    label_code = {}
    for code in codes:
        logger.notice("code %s" % (code))
        label_code[code] = "METIS" if code == "metis" else "PMF"
        sample_dir = os.path.join(
            code, os.path.basename(template_dir).lower() + "_tmp", "sample00001"
        )
        sat_dir[code] = os.path.join(sample_dir, "sature")
        full_sample_dir = os.path.join(template_dir, sample_dir)
        if not os.path.isdir(full_sample_dir):
            dzns[code] = None
            continue
        dzns[code] = [x for x in os.listdir(full_sample_dir) if "insat" in x]
        if dzns[code] and not lzns:
            lzns = dzns[code]
        if dzns[code]:
            if code == "openfoam" and not loutput:
                loutput = [
                    x.split(".")[0].replace("CmassBalance", "debit").replace("waterMassBalance", "debit_eau")
                    for x in os.listdir(os.path.join(full_sample_dir, dzns[code][0]))
                    if "massbalance" in x.lower()
                ]
            if code == "metis" and not loutput:
                loutput = [
                    x.split(".")[0]
                    for x in os.listdir(os.path.join(full_sample_dir, dzns[code][0]))
                    if "debit" in x and "debit_entrant" not in x
                ]
            dzns[code] = [os.path.join(full_sample_dir, x) for x in dzns[code]]

    ncodes_zns = len([x for x in dzns if dzns[x]])

    if lzns == None:
        logger.notice("no zns to plot")
        return

    for output in loutput:
        logger.notice("zns output =", output)
        if output == "debit_eau":
            name_mapping = {
                "metis": "debit_eau.insat",
                "openfoam": "waterMassBalance.csv",
            }
            column_mapping = {"metis": 1, "openfoam": 1}
            ylabel = "water flow (m3/s)"
            flux_entrant = "debit_eau_entrant"
            label_inflow = "zs_water_inflow"
            label_outflow = "zns_water_outflow"
        elif output == "debit":
            name_mapping = {"metis": "debit.insat", "openfoam": "CmassBalance.csv"}
            column_mapping = {"metis": 1, "openfoam": 2}
            ylabel = "mass flow (kg/s)"
            flux_entrant = "debit_entrant"
            label_inflow = "zs_mass_inflow"
            label_outflow = "zns_mass_outflow"
        else:
            logger.notice("output : %s unknown for zns" % (output))
            continue

        times = {}
        values = {}
        for ic, code in enumerate(codes):
            times[code] = []
            values[code] = []
            filename = os.path.join(template_dir, sat_dir[code], flux_entrant + "_plot.sat")
            if os.path.isfile(filename):
                logger.notice("reading %s" % (filename))
                with open(filename) as fil:
                    for line in fil:
                        if line[0] == "#":
                            continue
                        spl = line.split()
                        times[code].append(float(spl[0])/AN2S)
                        values[code].append(float(spl[1]))

        fig, ax = plt.subplots(figsize = (11.75, 8.25))
        ax.set_xlabel("time (year)")
        ax.set_ylabel(ylabel)
        ax.grid()
        for ic, code in enumerate(codes):
            if times[code]:
                ax.plot(times[code], values[code], label=label_code[code], marker="+")

        legend = ax.legend(loc="lower right")

        ax.text(
            0.05,
            0.05,
            label_inflow,
            ha="left",
            transform=ax.transAxes
        )
        plotname = os.path.join(template_dir, flux_entrant + ".png")
        fig.savefig(plotname, format='png', dpi=dpisize)
        plt.close(fig)

        for iz, zns in enumerate(lzns):
            logger.notice("zns = ", zns)

            times = {}
            values = {}
            for ic, code in enumerate(codes):
                times[code] = []
                values[code] = []
                filename = os.path.join(dzns[code][iz], name_mapping[code])
                if os.path.isfile(filename):
                    logger.notice("Reading %s" % (filename))
                    with open(filename) as fil:
                        for line in fil:
                            if line[0] == "#":
                                continue
                            spl = line.split()
                            times[code].append(float(spl[0])/AN2S)
                            values[code].append(float(spl[column_mapping[code]]))

            fig, ax = plt.subplots(figsize = (11.75, 8.25))
            ax.set_xlabel("time (year)")
            ax.set_ylabel(ylabel)
            ax.grid()
            for ic, code in enumerate(codes):
                if times[code]:
                    ax.plot(times[code], values[code], label=label_code[code], marker="+")

            legend = ax.legend(loc="upper right")

            ax.text(
                0.05,
                0.95,
                label_outflow + zns[5:],
                ha="left",
                transform=ax.transAxes
            )
            plotname = os.path.join(template_dir, output + zns[5:] + ".png")
            fig.savefig(plotname, format='png', dpi=dpisize)
            plt.close(fig)


def create_plots_probes(template_dir, codes, measures, mts, of, opty, display):
    logger.notice("creating plots at probes")
    probes = {}
    points = None
    output = None
    label_code = {}
    for code in codes:
        label_code[code] = "METIS" if code == "metis" else "PMF"
        code_dir = os.path.join(template_dir, code)
        if not os.path.isdir(code_dir):
            probes[code] = None
            continue
        lprobes = [x for x in os.listdir(code_dir) if "probes" in x]
        probes[code] = lprobes[0] if lprobes else None
        if probes[code] and not points:
            output = probes[code].split(".")[0]
            with open(os.path.join(code_dir, probes[code])) as fil:
                points = next(fil).split()[1:]

    ncodes_probes = len([x for x in probes if probes[x]])

    logger.notice("probes output =", output)
    if output == "concentration":
        unit = "µg/L"
        coef_unit = 1e6
        of_name = "C"
        ylabel = "concentration"
    elif output == "potentiel":
        unit = "m"
        coef_unit = 1
        of_name = "potential"
        ylabel = "hydraulic head"
    else:
        logger.notice("no result to plot")
        return

    # measures
    times_measures = [[] for pt in points]
    values_measures = [[] for pt in points]
    if measures:
        for code in codes:
            database = os.path.join(template_dir, code, os.path.basename(template_dir).lower() + ".sqlite")
            if os.path.isfile(database):
                break
            else:
                database = None
        if database:
            with sqlite.connect(database) as conn:
                logger.notice("connection to", database)
                cur = conn.cursor()
                debut = datetime.fromisoformat(cur.execute("SELECT debut FROM simulations").fetchone()[0])
                for ipt, point in enumerate(points):
                    logger.notice("reading measures for point", point)
                    pid = cur.execute(
                        "SELECT OGC_FID FROM points_interet WHERE nom='%s'" % (point)
                    ).fetchone()[0]
                    if output == "concentration":
                        eid = cur.execute("SELECT eid FROM simulations").fetchone()[0]
                        values = cur.execute("SELECT date, concentration FROM mesures \
                            WHERE pid=%d and concentration IS NOT NULL and eid=%d \
                            ORDER BY date" % (pid, eid)).fetchall()
                    elif output == "potentiel":
                        values = cur.execute("SELECT date, potentiel FROM mesures \
                            WHERE pid=%d and potentiel IS NOT NULL \
                            ORDER BY date" % (pid)).fetchall()
                    times_measures[ipt] = [(datetime.fromisoformat(x[0]) - debut).total_seconds()/AN2S for x in values]
                    values_measures[ipt] = [coef_unit*x[1] for x in values]
                    times_measures[ipt] = [x for x in times_measures[ipt] if x >=0]
                    del values_measures[ipt][0:len(values_measures[ipt]) - len(times_measures[ipt])]


    times_of = []
    values_of = [[] for pt in points]
    if "openfoam" in codes and of:
        of_probe_dir = os.path.join(
            "openfoam",
            os.path.basename(template_dir).lower() + "_tmp",
            "sample00001",
            "sature",
            "postProcessing",
            "probes",
        )
        full_of_probe_dir = os.path.join(template_dir, of_probe_dir)
        of_probe_file = os.path.join(
            of_probe_dir, os.listdir(full_of_probe_dir)[0], of_name
        )
        full_of_probe_file = os.path.join(template_dir, of_probe_file)
        if os.path.isfile(full_of_probe_file):
            logger.notice("reading", full_of_probe_file)
            with open(full_of_probe_file) as fil:
                for line in fil:
                    if line[0] == "#":
                        continue
                    spl = line.split()
                    times_of.append(float(spl[0])/AN2S)
                    for ipt, value in enumerate(spl[1:]):
                        values_of[ipt].append(coef_unit*float(value))

    times_mts = []
    values_mts = [[] for pt in points]
    if "metis" in codes and mts:
        metis_probe_file = os.path.join(
            "metis",
            os.path.basename(template_dir).lower() + "_tmp",
            "sample00001",
            "sature",
            output + "_probes.txt",
        )
        full_metis_probe_file = os.path.join(template_dir, metis_probe_file)
        if os.path.isfile(full_metis_probe_file):
            logger.notice("reading", full_metis_probe_file)
            with open(full_metis_probe_file) as fil:
                for line in fil:
                    if line[0] == "#":
                        continue
                    spl = line.split()
                    times_mts.append(float(spl[0])/AN2S)
                    for ipt, value in enumerate(spl[1:]):
                        values_mts[ipt].append(coef_unit*float(value))

    times_code = {}
    values_code = {}
    for ic, code in enumerate(codes):
        times_code[code] = []
        values_code[code] = [[] for pt in points]
        if probes[code]:
            full_code_probe_file = os.path.join(template_dir, code, probes[code])
            logger.notice("reading", full_code_probe_file)
            with open(full_code_probe_file) as fil:
                next(fil)
                for line in fil:
                    spl = line.split()
                    times_code[code].append(float(spl[0])/AN2S)
                    for ipt, value in enumerate(spl[1:]):
                        values_code[code][ipt].append(coef_unit*float(value))

    dpisize = 100
    for ipt, point in enumerate(points):

        fig, ax = plt.subplots(figsize = (11.75, 8.25))
        ax.set_xlabel("time (year)")
        ax.set_ylabel("%s (%s)" % (ylabel, unit))
        ax.grid()
        dy = 0
        if times_measures[ipt] and "openfoam" in codes:
            if times_code["openfoam"] and output == "potentiel":
                if opty:
                    def func(t, dy):
                        return numpy.interp(t, times_code["openfoam"], values_code["openfoam"][ipt]) + dy
                    popt, pcov = optimize.curve_fit(
                        func,
                        times_measures[ipt],
                        values_measures[ipt],
                    )
                    dy = popt[0]
                    perr = numpy.sqrt(numpy.diag(pcov))[0]
                    logger.notice("dy set to {:.3f} by optimization for point {} with error {:.3f}".format(dy, point, perr))
                else:
                    f = interpolate.interp1d(times_code["openfoam"], values_code["openfoam"][ipt])
                    dy = - f(times_measures[ipt][0]) + values_measures[ipt][0]

            ax.plot(times_measures[ipt], values_measures[ipt], "r+--", label="Measures", markersize=5)
        if times_of:
            values = [v + dy for v in values_of[ipt]]
            ax.plot(times_of, values, label="PMF")
        if times_mts:
            values = [v + dy for v in values_mts[ipt]]
            ax.plot(times_mts, values, label="METIS")
        for ic, code in enumerate(codes):
            if times_code[code]:
                values = [v + dy for v in values_code[code][ipt]]
                ax.plot(times_code[code], values, label=label_code[code])   #+".thyrsis")

        legend = ax.legend(loc="upper right")

        if display:
            ax.text(
                0.05,
                0.95,
                point,
                ha="left",
                transform=ax.transAxes
            )
            if dy != 0:
                ax.text(
                    0.05,
                    0.90,
                    "dy = {:.3f}".format(dy),
                    ha="left",
                    transform=ax.transAxes
                )
        plotname = os.path.join(template_dir, point + ".png")
        fig.savefig(plotname, format='png', dpi=dpisize)
        plt.close(fig)


if __name__ == "__main__":
    logger.enable_console(True)
    logger.set_level("debug")

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    topdir = "thyrsis_compute_validation_" + datetime.strftime(date.today(), "%y%m%d")
    init = False
    run = False
    resu = False
    plot = False
    measures = False
    mts = False
    of = False
    opty = False
    raz = False
    display = False

    references_file = os.path.join(
        os.getenv("HOME"), ".thyrsis", "compute_validation.csv"
    )
    if not os.path.isfile(references_file):
        exit("File %s does not exist. Stop" % (references_file))
    logger.notice("reading file %s" % (references_file))
    ref = pd.read_csv(references_file, sep=";")
    REFERENCES = ref.set_index("template").to_dict("index")

    results = REFERENCES
    results_raz = False

    codes = {c: which(c) for c in ["metis", "openfoam"] if which(c)}
    if which("groundwaterFoam"):
        codes["openfoam"] = ""
    codes_all = codes

    for arg in sys.argv[1:]:
        if arg == "-init":
            init = True
        elif arg == "-run":
            run = True
        elif arg == "-raz":
            raz = True
        elif arg == "-results":
            resu = True
        elif arg == "-plot":
            plot = True
        elif arg == "-m":
            measures = True
        elif arg == "-mts":
            mts = True
        elif arg == "-of":
            of = True
        elif arg == "-oy":
            opty = True
        elif arg[:5] == "-dir=":
            topdir = arg[5:]
        elif arg == "-a":
            init = True
            run = True
            resu = True
            plot = True
        elif arg == "-d":
            display = True
        elif arg[0] == "-" and arg[1:] in list(REFERENCES.keys()):
            del results[arg[1:]]
        elif arg in list(REFERENCES.keys()):
            if not results_raz:
                results = {}
                results_raz = True
            results[arg] = REFERENCES[arg]
        elif arg[:6] == "-code=":
            codes = {
                c: which(c) for c in arg[6:].split(",") if c in codes_all
            }
        else:
            logger.error("argument not recognized", arg)
            exit(1)

    compute_validation(topdir, init, run, resu, plot, measures, mts, of, opty, results, codes, raz, display)
