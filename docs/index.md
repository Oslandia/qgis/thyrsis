# {{ title }} - Documentation

> **Description :** {{ description }}  
> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

Welcome to the THYRSIS plugin documentation !

```{toctree}
---
caption: Available languages
maxdepth: 1
---
en/index
fr/index
```

----

```{toctree}
---
caption: Development
maxdepth: 1
---

development/contribute
development/documentation
development/translation
development/environment
development/docker
development/packaging
development/testing
development/history
development/contributors
Code documentation <_apidoc/thyrsis>
```
