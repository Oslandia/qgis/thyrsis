"""METIS specific functions
"""
import os
import shutil
import string
import warnings
from builtins import object, range, str
from subprocess import PIPE, STDOUT, Popen

import numpy

from ..database import (
    db_elem_to_node,
    db_node_to_elem,
    has_second_milieu,
    sqlite,
    table_exists,
)
from ..log import logger
from ..utilities import (
    Timer,
    check_float,
    convert_nodes,
    number_lines,
    parse_file,
    read_one_column_file,
)
from .nearest_nodes import Nearest


class Metis(object):
    """Metis process class"""

    def __init__(self, database, command):
        """Constructor

        :param database: database
        :type database: string
        :param command: command to execute
        :type command: string
        """
        self.codename = "metis"
        self.database = database
        self.command = command
        self.has_second_milieu = self.get_second_milieu()
        self.variables_sat = {
            "concentration": "nodes",
            "potentiel": "nodes",
            "darcy": "mailles",
            "debit": "contour",
        }
        self.variables_sat2 = {
            "concentration2": "nodes",
            "potentiel2": "nodes",
            "darcy2": "mailles",
            "debit2": "contour",
        }
        self.variables_insat = {
            "saturation": False,
            "saturation_perm": False,
            "darcy": False,
            "darcy_perm": False,
            "concentration": True,
            "activity": True,
        }

        self.points_interet = []
        # nearest nodes of points of interest in order to compute at this points
        if table_exists(self.database, "points_interet"):
            self.points_interet = Nearest(
                self.database, os.path.dirname(self.database), nodes_number=4
            ).get_points()

    def __del__(self):
        """Destructor"""
        return
        # conn.close()

    def get_second_milieu(self):
        """Return if second milieu is used

        :return: True/False
        :rtype: bool
        """
        with sqlite.connect(self.database) as conn:
            return has_second_milieu(conn.cursor())

    def get_options(self):
        """Specific options for METIS

        :return: Metis options
        :rtype: dict
        """

        with sqlite.connect(self.database) as conn:
            (
                idx,
                niter_gc_zns,
                niter_gc_zs,
                saturation_initiale,
                rmult_cormax,
                c_pstep,
                c_ps_inc,
                c_ps_tol,
                ratio_max_res,
                critereres,
                niterns,
                courant,
                emmagasinement,
                kdecoup_delt_zns,
                kdecoup_dmin_zns,
                kdecoup_delt_zs,
                kdecoup_dmin_zs,
                force_fin_phase,
                max_iter,
                relax_phreat,
                critere_pot,
                phrea_min,
            ) = (
                conn.cursor().execute("SELECT * FROM metis_options").fetchone()
            )

        # parametres gradient conjugue mot-clé reso
        igrconj = "1"  # obligatoire pour résoudre le gradient conjugué (inutile)
        niveau_zns = "21"  # niveau de préconditionnement ZNS (inutile)
        niveau_zs = "21"  # niveau de préconditionnement ZS (inutile)
        mnconn_zns = "3"  # nombre maximum de connexions d'un noeud ZNS (1D) (inutile)
        mnconn_zs = "10"  # nombre maximum de connexions d'un noeud ZS (2D) (inutile)
        lmat5 = "0"  # longueur des matrices d'éléments finis (inutile)
        lmac5 = "0"  # longueur de la matrice de préconditionnement (inutile)
        epsh = "0."  # tolérance sur la résolution en écoulement (inutile)
        epsc = "0."  # tolérance sur la résolution en transport (inutile)
        istgrc = "1"  # indice obligatoire d'arret en cas de non convergence (inutile)
        # niter_gc_zns = "100" # nombre maximum d'itérations ZNS
        # niter_gc_zs = "100" # nombre maximum d'itérations ZS

        reso_zns = "reso = " + ",".join(
            (
                igrconj,
                niveau_zns,
                mnconn_zns,
                lmat5,
                lmac5,
                epsh,
                epsc,
                istgrc,
                str(niter_gc_zns),
            )
        )
        reso_zs = "reso = " + ",".join(
            (
                igrconj,
                niveau_zs,
                mnconn_zs,
                lmat5,
                lmac5,
                epsh,
                epsc,
                istgrc,
                str(niter_gc_zs),
            )
        )

        # options insature mot-clé knonsat
        # rmult_cormax = "0.1" # coefficient multiplicateur de la dimension maximale, définissant la variation maximale de la charge hydraulique
        # c_pstep = "0.1" # valeur initiale du fractionnement de la solution
        # c_ps_inc = "0.1" # valeur initiale de l'incrément
        # c_ps_tol = "1.e-5" # tolérance sur le paramètre de parameter stepping: on considère qu'on a convergé quand c_pstep est plus grand que 1 - c_ps_tol (permet d'abréger une convergence laborieuse où on tent vers 1 sans jamais exactement l'atteindre)
        # ratio_max_res = "1.e-6" # comparaison de deux modes de calcul des résidus. Permet de détecter plus précisément la convergence locale dans des cas où les résidus oscillent un peu
        # critereres = "1.e-5" # critère de convergence des résidus. Le calcul est convergé quand c_pstep vaut 1 et redres < critereres
        # niterns = "1000" # nombre d'itérations
        # courant = "off" # Le nombre de Courant est calculé à partir d'une pseudo-vitesse du front de saturation. Si courant = on, le pas de temps sera ajusté pour que le front ne franchisse pas plus d'un élément par pas. Ca ne marche que pour des problèmes 1D. Ca peut aider pour des fronts très abrupts. En général, ça ne sert à rien
        # emmagasinement = 1.e-3 # emmagasinement spécifique (m-1), utilisé pour gérer les fuites importantes avec l'option sat_nonsat = on, associée à courant = on

        # option de kdecoup
        # kdecoup_delt_zns = "10." # pas de temps initial ZNS
        # kdecoup_dmin_zns = "1." # pas de temps minimal ZNS

        # kdecoup_delt_zs = "100." # pas de temps initial ZS
        # kdecoup_dmin_zs = "1." # pas de temps minimal ZS

        # force_fin_phase # force la fin de phase avec un grand pas de temps lorsque le pas de temps devient trop petit

        # opt_phreatique
        # max_iter = 200 # nombre maximal d'itérations en phréatique
        # relax_phreat = 0.5 # coefficient de relaxation (poids attribué à la dernière itération)
        # critere_pot = 0.1 # critère de convergence sur le potentiel (m)
        # phrea_min = 1.0 # epaisseur_mouillee_mini (m)

        return {
            "RESO_ZNS": reso_zns,
            "RESO_ZS": reso_zs,
            "SATURATION_INITIALE": saturation_initiale,
            "RMULT_CORMAX": rmult_cormax,
            "C_PSTEP": c_pstep,
            "C_PS_INC": c_ps_inc,
            "C_PS_TOL": c_ps_tol,
            "RATIO_MAX_RES": ratio_max_res,
            "CRITERERES": critereres,
            "NITERNS": niterns,
            "COURANT": "on" if courant == "oui" else "off",
            "BEMM": emmagasinement,
            "KDECOUP_DELT_ZNS": kdecoup_delt_zns,
            "KDECOUP_DMIN_ZNS": kdecoup_dmin_zns,
            "KDECOUP_DELT_ZS": kdecoup_delt_zs,
            "KDECOUP_DMIN_ZS": kdecoup_dmin_zs,
            "FORCE_FIN_PHASE": "force_fin_phase" if force_fin_phase == "oui" else "",
            "MAX_ITER": max_iter,
            "RELAX_PHREAT": relax_phreat,
            "CRITERE_POT": critere_pot,
            "PHREA_MIN": phrea_min,
        }

    def get_hynverse_options(self):
        """Specific options for inversion process METIS

        :return: Metis options
        :rtype: dict
        """

        with sqlite.connect(self.database) as conn:
            (idx, niter_gc_zs, max_iter, relax_phreat, critere_pot, phrea_min) = (
                conn.cursor().execute("SELECT * FROM metis_hynverse_options").fetchone()
            )

        # parametres gradient conjugue mot-clé reso
        igrconj = "1"  # obligatoire pour résoudre le gradient conjugué (inutile)
        niveau_zs = "21"  # niveau de préconditionnement ZS (inutile)
        mnconn_zs = "10"  # nombre maximum de connexions d'un noeud ZS (2D) (inutile)
        lmat5 = "0"  # longueur des matrices d'éléments finis (inutile)
        lmac5 = "0"  # longueur de la matrice de préconditionnement (inutile)
        epsh = "0."  # tolérance sur la résolution en écoulement (inutile)
        epsc = "0."  # tolérance sur la résolution en transport (inutile)
        istgrc = "1"  # indice obligatoire d'arret en cas de non convergence (inutile)
        # niter_gc_zs = "100" # nombre maximum d'itérations ZS

        reso_zs = "reso = " + ",".join(
            (
                igrconj,
                niveau_zs,
                mnconn_zs,
                lmat5,
                lmac5,
                epsh,
                epsc,
                istgrc,
                str(niter_gc_zs),
            )
        )

        # opt_phreatique
        # max_iter 200 # nombre maximal d'itérations en phréatique
        # relax_phreat 0.5 # coefficient de relaxation (poids attribué à la dernière itération)
        # critere_pot 0.1 # critère de convergence sur le potentiel (m)
        # phrea_min # epaisseur_mouillee_mini (m)

        return {
            "RESO_ZS": reso_zs,
            "MAX_ITER": max_iter,
            "RELAX_PHREAT": relax_phreat,
            "CRITERE_POT": critere_pot,
            "PHREA_MIN": phrea_min,
        }

    def nok(self, log_file):
        """ Returns False if the METIS computation is OK, \
        by testing the 'fin du job metis' string in the log file

        :param log_file: log file path
        :type log_file: string

        :return: success state
        :rtype: bool
        """

        with open(log_file, "r") as fil:
            lines = fil.readlines()
            if len(lines) >= 10:
                for i in range(10):
                    if "fin du job metis" in lines[-i]:
                        return False

            logger.notice(
                "METIS calculation aborted : last lines of file " + log_file + " :\n"
            )
            for i in range(min(len(lines), 20)):
                logger.notice(lines[-len(lines) + i][:-1])

            return True
            # raise RuntimeError("METIS calculation aborted : refer back to file "+log_file)

    def file_exists(self, result_file):
        """Returns True if the result file exists (useful for other codes than METIS)

        :return: path of result file
        :rtype: string
        """
        return os.path.exists(result_file)

    def read_mesh_insat(self, insat_dir):
        """Reads the METIS mesh file and returns the nodes number and the nodes surfaces

        :param insat_dir: path of insaturated folder
        :type insat_dir: string

        :return nnodes: nodes number
        :rtype nnodes: list
        :return surfaces: nodes surfaces
        :rtype surfaces: list
        """

        metis_mesh_file = os.path.join(insat_dir, "data.mail")
        if not os.path.isfile(metis_mesh_file):
            raise RuntimeError("fichier " + metis_mesh_file + " inexistant")

        with open(metis_mesh_file, "r") as fil:
            lines = fil.readlines()
            nnodes = int(lines[0].split()[0])
            znodes = [float(x.split()[1]) for x in lines[1 : nnodes + 1]]

        surfaces = (
            [0.5 * (znodes[0] - znodes[1])]
            + [0.5 * (znodes[i - 1] - znodes[i + 1]) for i in range(1, nnodes - 1)]
            + [0.5 * (znodes[-2] - znodes[-1])]
        )
        return nnodes, surfaces

    def read_potential_at_nodes(self, sat_dir, dates):
        """Reads the potential at node for each dates

        :param sat_dir: path of insaturated folder
        :type sat_dir: string
        :param dates: list of dates
        :type dates: list

        :return: result
        :rtype: numpy.ndarray
        """
        return self.read_file(os.path.join(sat_dir, "potentiel.sat"), dates)

    def read_file(self, result_file, dates, convert=False):
        """reads an METIS results file

        :param result_file: path of the result file
        :type result_file: string
        :param dates: list of dates
        :type dates: list
        :param convert: convert flag to get ZNS columns data
        :type convert: bool

        :return: result
        :rtype: numpy.ndarray
        """
        name_file = os.path.basename(result_file).split(".")[0]
        if name_file in ("concentration", "potentiel", "concentration2", "potentiel2"):
            logger.notice("converting binary", result_file)
            metis_dates, nbVal = self.read_dates_binaire(result_file)
            return self.read_file_binaire(
                result_file, metis_dates, dates, nbVal, convert
            )
        else:
            logger.notice("converting formated", result_file)
            metis_dates, nbVal = self.read_dates_formate(result_file)
            return self.read_file_formate(
                result_file, metis_dates, dates, nbVal, convert
            )

    def read_dates_formate(self, result_file):
        """reads a METIS results file and returns the dates and the number of dates

        :param result_file: path of the result file
        :type result_file: string

        :return dates: list of dates
        :rtype dates: list
        :return nbVal: number of values
        :rtype nbVal: integer
        """

        dates = []
        nbVal = 0
        ndates = 0
        with open(result_file, "r") as fil:
            for line in fil:
                if line[:5] == "#date":
                    dates.append(float(line.split()[1]))
                    ndates += 1
                elif line[0] != "#" and line.split() != [] and ndates == 1:
                    nbVal += 1

        logger.notice(len(dates), "dates and", nbVal, "values in file", result_file)

        return dates, nbVal

    def read_file_formate(self, result_file, metis_dates, dates, nbVal, convert=False):
        """reads an METIS results file, adding values in case of missing dates

        :param result_file: path of the result file
        :type result_file: string
        :param metis_dates: list of metis dates
        :type metis_dates: list
        :param dates: list of dates
        :type dates: list
        :param nbVal: number of values
        :type nbVal: integer
        :param convert: convert flag to get ZNS columns data
        :type convert: bool

        :return: result
        :rtype: numpy.ndarray
        """

        nbDates = len(dates)
        # dans le cas où le fichier n'existe pas, on renvoie un tableau de zéros
        results = numpy.zeros((nbDates, nbVal), dtype=numpy.float32)
        if not os.path.exists(result_file):
            logger.notice("File " + result_file + " is missing : zeros are returned")
            return results

        # cas d'une seule date => permanent
        if len(metis_dates) == 1:
            dmin = len(dates) - 1
        elif metis_dates[0] in dates:
            dmin = dates.index(metis_dates[0])
        else:
            raise RuntimeError("metis_read_file: metis first date not recognized")

        with open(result_file) as fil:
            n, d = 0, dmin
            logger.debug("nbDates, nbVal, d, n = ", nbDates, nbVal, d, n)
            for line in fil:
                if line[0] != "#" and line != "\n":
                    results[d, n] = float(line)
                    n += 1
                if n == nbVal:
                    n = 0
                    d += 1

        # on remplit les valeurs pour les dates non renseignées :
        #  = 0 pour la concentration
        #  = première valeur renseignée pour potentiel et darcy
        if dmin > 0:
            if os.path.basename(result_file)[:4] == "conc":
                results[0:dmin, :] = 0.0
            else:
                results[0:dmin, :] = results[dmin, :]

        # pour le débit, on ne conserve que la somme
        if os.path.basename(result_file)[:5] == "debit":
            results = numpy.sum(results, axis=1).reshape((nbDates, 1))

        if convert:
            return convert_nodes(results)
        else:
            return results

    def read_dates_binaire(self, result_file):
        """reads a METIS binary results file and returns the dates and the number of dates

        :param result_file: path of the result file
        :type result_file: string

        :return dates: list of dates
        :rtype dates: list
        :return nbVal: number of values
        :rtype nbVal: integer
        """

        dates = []
        nbVal = 0
        with open(result_file, "rb") as fil:
            test = True
            while test:
                lread = numpy.frombuffer(fil.read(4), dtype=numpy.int32)
                test = lread.size > 0
                if test:
                    dates.append(
                        numpy.frombuffer(fil.read(lread[0]), dtype=numpy.float64)[0]
                    )
                    numpy.frombuffer(fil.read(4), dtype=numpy.int32)
                    lread = numpy.frombuffer(fil.read(4), dtype=numpy.int32)
                    if nbVal == 0:
                        nbVal = lread[0] // 8
                    numpy.frombuffer(fil.read(lread[0]), dtype=numpy.float64)
                    numpy.frombuffer(fil.read(4), dtype=numpy.int32)

        logger.notice(
            len(dates), "dates et", nbVal, "valeurs dans le fichier", result_file
        )

        return dates, nbVal

    def read_file_binaire(self, result_file, metis_dates, dates, nbVal, convert=False):
        """reads an METIS binary results file, adding values in case of missing dates

        :param result_file: path of the result file
        :type result_file: string
        :param metis_dates: list of metis dates
        :type metis_dates: list
        :param dates: list of dates
        :type dates: list
        :param nbVal: number of values
        :type nbVal: integer
        :param convert: convert flag to get ZNS columns data
        :type convert: bool

        :return: result
        :rtype: numpy.ndarray
        """

        nbDates = len(dates)
        # dans le cas où le fichier n'existe pas, on renvoie un tableau de zéros
        results = numpy.zeros((nbDates, nbVal), dtype=numpy.float32)
        if not os.path.exists(result_file):
            logger.notice("File " + result_file + " is missing : zeros are returned")
            return results

        # cas d'une seule date => permanent
        if len(metis_dates) == 1:
            dmin = len(dates) - 1
        elif metis_dates[0] in dates:
            dmin = dates.index(metis_dates[0])
        else:
            raise RuntimeError("metis_read_file: metis first date not recognized")

        with open(result_file, "rb") as fil:
            logger.debug("nbDates, nbVal = ", nbDates, nbVal)
            test = True
            while test:
                lread = numpy.frombuffer(fil.read(4), dtype=numpy.int32)
                test = lread.size > 0
                if test:
                    metis_date = numpy.frombuffer(
                        fil.read(lread[0]), dtype=numpy.float64
                    )[0]
                    if metis_date in dates:
                        d = dates.index(metis_date)
                    elif len(metis_dates) == 1:
                        d = 0
                    else:
                        raise RuntimeError("metis_read_file: metis_date not recognized")
                    numpy.frombuffer(fil.read(4), dtype=numpy.int32)
                    lread = numpy.frombuffer(fil.read(4), dtype=numpy.int32)
                    results[d, :] = numpy.frombuffer(
                        fil.read(lread[0]), dtype=numpy.float64
                    )
                    numpy.frombuffer(fil.read(4), dtype=numpy.int32)

        # on remplit les valeurs pour les dates non renseignées :
        #  = 0 pour la concentration
        #  = première valeur renseignée pour potentiel et darcy
        if dmin > 0:
            if os.path.basename(result_file)[:4] == "conc":
                results[0:dmin, :] = 0.0
            else:
                results[0:dmin, :] = results[dmin, :]

        if convert:
            return convert_nodes(results)
        else:
            return results

    def create_maillage_insat(self, insat_dir, znodes):
        """creates the METIS mesh files for unsaturated computations

        :param insat_dir: path of insaturated folder
        :type insat_dir: string
        :param znodes: znodes (list of z coordinate)
        :type znodes: list
        """

        with open(os.path.join(insat_dir, "data.mail"), "w") as fil:
            fil.write("%12d%12d\n" % (len(znodes), len(znodes) - 1))
            # noeuds
            for z in znodes:
                fil.write("%15.3f%15.5f\n" % (0, z))

            # mailles
            for i, z in enumerate(znodes[:-1]):
                fil.write("%12d%12d%12d\n" % (3, i + 1, i + 2))

    def create_insat_files(
        self, insat_dir, simulation_data, injection_data, param, settings, znodes
    ):
        """builds the input files for METIS unsaturated

        :param insat_dir: path of insaturated folder
        :type insat_dir: string
        :param simulation_data: dictionnary of simulation data
        :type simulation_data: dict
        :param injections_data: list, containing a dictionnary for each injection
        :type injections_data: list
        :param param: parameters dictionnary
        :type param: dict
        :param settings: Thyrsis settings
        :type settings: Settings
        :param znodes: znodes (list of z coordinate)
        :type znodes: list
        """

        self.create_maillage_insat(insat_dir, znodes)

        type_infiltration = simulation_data["TYPE_INFILTRATION"]
        dispersion = simulation_data["TYPE_INJECTION"] != "aucune"
        fuite = simulation_data["FUITE"]

        aectran = type_infiltration != "permanente" or fuite
        aecditr = dispersion and aectran

        if aecditr:
            code_calcul = "aecditr"
            mode_calcul = "%ecoulement + dispersion transitoire"
        elif dispersion:
            code_calcul = "aditran"
            mode_calcul = "%dispersion transitoire"
        else:
            code_calcul = "aectran"
            mode_calcul = "%ecoulement transitoire"

        tmpdir = os.path.dirname(os.path.dirname(insat_dir))
        # fichier debit_entrant.insat
        if dispersion:
            if os.path.isfile(
                os.path.join(tmpdir, "debit_entrant.insat_%05d" % (int(insat_dir[-5:])))
            ):
                shutil.copyfile(
                    os.path.join(
                        tmpdir, "debit_entrant.insat_%05d" % (int(insat_dir[-5:]))
                    ),
                    os.path.join(insat_dir, "debit_entrant.insat"),
                )
            elif injection_data["FLUX"] == None:
                raise RuntimeError(
                    "Le flux est nul ou inconnu, debit_entrant.insat ne peut être créé"
                )
            else:
                with open(
                    os.path.join(insat_dir, "debit_entrant.insat"), "w"
                ) as edma_insat:
                    edma_insat.write(
                        "date %15.7E\n"
                        "%d %15.7E\n"
                        "date %15.7E\n"
                        "%d %15.7E\n"
                        % (
                            injection_data["T0"],
                            injection_data["NOEUD_INJ"],
                            injection_data["FLUX"],
                            injection_data["TINJ"],
                            injection_data["NOEUD_INJ"],
                            injection_data["FLUX"],
                        )
                    )

        # fichier data.insat
        if os.path.isfile(os.path.join(tmpdir, "data.insat")):
            shutil.copy2(os.path.join(tmpdir, "data.insat"), insat_dir)
            return

        templates_dir = os.path.join(os.path.dirname(__file__), "templates", "metis")

        infiltration = injection_data["INFILTRATION"]
        simulation_data["FUITE1"] = "%%"
        simulation_data["FUITE2"] = "%%"

        # TPHA = date de fin de phase
        injection_data["TPHA"] = simulation_data["TFIN"]
        if fuite:
            simulation_data["FUITE1"] = ""
            if injection_data["TINJ"] < simulation_data["TFIN"]:
                simulation_data["FUITE2"] = ""
                injection_data["TPHA"] = injection_data["TINJ"]

        dict_substitute = {
            "DISPERSION": "" if dispersion else "%%",
            "AECTRAN": "" if aectran else "%%",
            "AECDITR": "" if aecditr else "%%",
            "INF_PERMANENTE": "" if type_infiltration == "permanente" else "%%",
            "INF_TRANSITOIRE": "" if type_infiltration != "permanente" else "%%",
            "TITRE_CALCUL": "% ecoulement + dispersion"
            if dispersion
            else "% ecoulement seul",
            "TYPE_CALCUL": 1 if dispersion else 0,
            "MODE_CALCUL": mode_calcul,
            "CODE_CALCUL": code_calcul,
            "VGM": 1.0 - 1.0 / param["VGN"],
            "DTZNS": 0.2 * param["DLZNS"],
            "NBDATES": number_lines(os.path.join(insat_dir, "dates_simulation.txt")),
        }
        dict_substitute.update(param)
        dict_substitute.update(self.settings_to_dict(settings))
        dict_substitute.update(simulation_data)
        dict_substitute.update(injection_data)
        options = self.get_options()
        # if fuite:
        #    options['COURANT'] = "on"
        dict_substitute.update(options)

        with open(os.path.join(insat_dir, "data_tmp.insat"), "w") as data_insat, open(
            os.path.join(templates_dir, "metis.insat")
        ) as metis_insat:
            data_insat.write(
                string.Template(metis_insat.read()).substitute(dict_substitute)
            )

        self.modif_data(insat_dir)

    def convert_infiltration_transitoire_insature(self, compute_dir):
        """Copy transient infiltration file to compute_dir

        :param compute_dir: path of compute folder
        :type compute_dir: string
        """
        # the esui-like infiltration_transitoire.insat file is copied from tmpdir to compute_dir
        file_esui = os.path.join(
            os.path.dirname(os.path.dirname(compute_dir)),
            "infiltration_transitoire.insat",
        )
        if os.path.exists(file_esui):
            shutil.copyfile(
                file_esui, os.path.join(compute_dir, "infiltration_transitoire.insat")
            )

    def convert_infiltration_transitoire_sature(self, compute_dir):
        """Copy transient infiltration file to compute_dir

        :param compute_dir: path of compute folder
        :type compute_dir: string
        """
        # the esui-like infiltration_transitoire.insat file is copied from tmpdir to compute_dir
        file_esui = os.path.join(
            os.path.dirname(os.path.dirname(compute_dir)),
            "infiltration_transitoire.sat",
        )
        if os.path.exists(file_esui):
            shutil.copyfile(
                file_esui, os.path.join(compute_dir, "infiltration_transitoire.sat")
            )

    def compute(self, compute_dir, total_time, MPI=None):
        """computes saturated or unsaturated

        :param compute_dir: path of compute folder
        :type compute_dir: string
        :param total_time: simulation time duration in second
        :type total_time: float
        """

        timer = Timer()

        if os.path.basename(compute_dir)[0] == "s":
            name_file = "data.sat"
            type_computation = "METIS saturated"
        else:
            name_file = "data.insat"
            type_computation = "METIS unsaturated"

        log_file = os.path.join(compute_dir, self.codename + ".log")
        finished_job = False
        with open(log_file, "w") as logfile, open(
            os.path.join(compute_dir, name_file)
        ) as datafile:
            logger.notice("running: ", self.command, "\n in directory ", compute_dir)
            warnings.filterwarnings("ignore", message=".*subprocess .*")
            proc = Popen(
                [self.command],
                stdin=datafile,
                stdout=PIPE,
                stderr=STDOUT,
                cwd=compute_dir,
                universal_newlines=False,
            )
            progress = logger.progress(type_computation)
            while True:
                line = proc.stdout.readline()
                line = line.decode("utf-8")
                if line is None or line == "":
                    break
                if "fin du job metis" in line:
                    finished_job = True
                logfile.write(line)
                if line.find("Time = ") != -1:
                    current = float(line.split(" = ")[1])
                    progress.set_ratio(current / total_time)
            del progress
            proc.stdout.close()

        if finished_job != True:
            flag = self.nok(log_file)
            if flag:
                if MPI:
                    MPI.COMM_WORLD.Abort(1)
                raise RuntimeError(
                    "METIS calculation aborted : refer back to file " + log_file
                )

        logger.debug(timer.reset(compute_dir))

        self.compute_probes(compute_dir)

    def compute_probes(self, compute_dir):
        """modifies results file _probes.sat in order to obtain a gnuplot readable file _probes_plot.sat

        :param compute_dir: path of compute folder
        :type compute_dir: string
        """

        if not self.points_interet:
            return

        timer = Timer()

        dates_simulation = read_one_column_file(
            os.path.join(compute_dir, "dates_simulation.txt")
        )
        header = " ".join([pt.nom for pt in self.points_interet])

        files = os.listdir(compute_dir)
        for f in files:
            if not "_probes.sat" in f:
                continue
            results = numpy.insert(
                self.read_file(os.path.join(compute_dir, f), dates_simulation),
                0,
                dates_simulation,
                axis=1,
            )

            numpy.savetxt(
                os.path.join(compute_dir, f.replace(".sat", ".txt")),
                results,
                fmt="%.8e",
                header="Time " + header,
            )

        logger.debug(timer.reset("computing probes"))

    def compute_hynverse(self, compute_dir):
        """computes saturated for inversion

        :param compute_dir: path of compute folder
        :type compute_dir: string
        """

        timer = Timer()

        solver = self.command

        log_file = os.path.join(compute_dir, self.codename + ".log")
        logger.debug("compute_hynverse:compute_dir = ", compute_dir)
        logger.debug("compute_hynverse:log_file = ", log_file)
        finished_job = False
        with open(log_file, "w") as logfile, open(
            os.path.join(compute_dir, "data.sat")
        ) as datafile:
            logger.notice(
                "running: ",
                " ".join([solver, " < data.sat"]),
                "\n in directory ",
                compute_dir,
            )
            proc = Popen(
                [solver],
                stdin=datafile,
                stdout=PIPE,
                stderr=STDOUT,
                cwd=compute_dir,
                universal_newlines=False,
            )
            while True:
                line = proc.stdout.readline()
                line = line.decode("utf-8")
                if line is None or line == "":
                    break
                if "fin du job metis" in line:
                    finished_job = True
                logfile.write(line)
            proc.wait()

        if finished_job != True:
            with open(log_file, "w") as logfile:
                for i in proc.stdout:
                    logfile.write(i)
            if not os.path.exists(log_file):
                raise FileExistsError(log_file)
            flag = self.nok(log_file)
            if flag:
                raise RuntimeError(
                    "METIS calculation aborted : refer back to file " + log_file
                )

        proc.stdout.close()
        logger.debug(timer.reset(compute_dir))

    def settings_to_dict(self, settings):
        """save settings in dictionnary

        :param settings: Thyrsis settings
        :type settings: Settings
        """
        save_dict = {}
        for option in settings.options("Variables"):
            if option[-5:] == "Check":
                save_dict[option[:-5].upper()] = (
                    "" if settings.value("Variables", option) else "%%"
                )
        save_dict["SATURATION_PERMZNS"] = save_dict["SATURATIONZNS"]
        save_dict["DARCY_PERMZNS"] = save_dict["DARCYZNS"]
        return save_dict

    def modif_data(self, rep, tini=0.0):
        """modifies the METIS data file and corrects the initial time

        :param rep: file path
        :type rep: string
        :param tini: initial time
        :type tini: float
        """

        if os.path.basename(rep)[:3] == "sat":
            suffixe = ".sat"
        else:
            suffixe = ".insat"
        if not os.path.exists(os.path.join(rep, "data_tmp" + suffixe)):
            return
        with open(os.path.join(rep, "data_tmp" + suffixe), "r") as tmp:
            with open(os.path.join(rep, "data" + suffixe), "w") as fil:
                for line in tmp:
                    if not "%% " in line:
                        if "TINI" in line:
                            spl = line.split()[1:]
                            spl.insert(0, str(tini))
                            fil.write(" ".join(spl) + "\n")
                        else:
                            fil.write(line.lstrip())

        os.remove(os.path.join(rep, "data_tmp" + suffixe))

    def create_sat_files(self, sat_dir, simulation_data, param, settings):
        """builds the input files for METIS saturated

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param simulation_data: dictionnary of simulation data
        :type simulation_data: dict
        :param param: parameters dictionnary
        :type param: dict
        :param settings: Thyrsis settings
        :type settings: Settings
        """

        with sqlite.connect(self.database) as conn:
            cur = conn.cursor()

            self.create_maillage_sat(sat_dir, cur)

            self.create_permeabilities_sat(sat_dir, cur=cur)

            self.create_mur_sat(sat_dir, cur)

            # infiltration
            with open(
                os.path.join(sat_dir, "infiltration.sat"), "w"
            ) as infiltration_sat:
                infiltration_sat.write("esui 4\n")
                cur.execute(
                    "SELECT OGC_FID, -infiltration FROM noeuds ORDER BY OGC_FID"
                )
                for row in cur.fetchall():
                    infiltration_sat.write("%8d%15.7E 0\n" % row)
                infiltration_sat.write("n\n")

            with open(os.path.join(sat_dir, "ehim.sat"), "w") as ehim_sat:
                if not self.has_second_milieu:
                    ehim_sat.write("ehim 4\n")
                cur.execute("SELECT nid, valeur FROM potentiel_impose ORDER BY nid")
                for row in cur.fetchall():
                    ehim_sat.write("%5i%10.2f\n" % row)
                ehim_sat.write("n\n")

            if self.has_second_milieu:
                shutil.copy2(
                    os.path.join(sat_dir, "ehim.sat"),
                    os.path.join(sat_dir, "ehim2.sat"),
                )

            with open(os.path.join(sat_dir, "epaisseur.sat"), "w") as epaisseur_sat:
                cur.execute("SELECT epaisseur_zs FROM noeuds ORDER BY OGC_FID")
                for row in cur.fetchall():
                    epaisseur_sat.write("%15.7E\n" % row)

            with open(os.path.join(sat_dir, "epaquifere.sat"), "w") as aquifere_sat:
                cur.execute("SELECT epaisseur_aquifere FROM noeuds ORDER BY OGC_FID")
                for row in cur.fetchall():
                    aquifere_sat.write("%15.7E\n" % row)

            if self.has_second_milieu:
                with open(
                    os.path.join(sat_dir, "epaquifere2.sat"), "w"
                ) as aquifere_sat:
                    cur.execute(
                        """SELECT sn.epaisseur_aquifere
                        FROM noeuds AS n JOIN noeuds_second_milieu AS sn ON sn.OGC_FID=n.OGC_FID
                        ORDER BY sn.OGC_FID"""
                    )
                    for row in cur.fetchall():
                        aquifere_sat.write("%15.7E\n" % row)

            with open(os.path.join(sat_dir, "gsm.sat"), "w") as gsm_sat:
                cur.execute("SELECT nid FROM noeuds_contour ORDER BY nid")
                nb_nodes_on_line = 0
                max_nodes_on_line = 10
                for row in cur.fetchall():
                    if not nb_nodes_on_line:
                        gsm_sat.write("l")
                    gsm_sat.write("%8i" % row)
                    nb_nodes_on_line += 1
                    if nb_nodes_on_line == max_nodes_on_line:
                        gsm_sat.write("\n")
                        nb_nodes_on_line = 0
                if nb_nodes_on_line:
                    gsm_sat.write("\n")

            if len(self.points_interet) > 0:
                with open(os.path.join(sat_dir, "probes_nodes.sat"), "w") as fil:
                    nb_nodes_on_line = 0
                    max_nodes_on_line = 10
                    for p in self.points_interet:
                        if not nb_nodes_on_line:
                            fil.write("l")
                        fil.write("%8i" % p.nodes[0])
                        nb_nodes_on_line += 1
                        if nb_nodes_on_line == max_nodes_on_line:
                            fil.write("\n")
                            nb_nodes_on_line = 0
                    if nb_nodes_on_line:
                        fil.write("\n")

            rho = 1000.0
            with open(os.path.join(sat_dir, "htsoec.sat"), "w") as htsoec_sat:
                b = numpy.array(
                    cur.execute(
                        "SELECT potentiel, flux_eau, infiltration, surface FROM noeuds ORDER BY OGC_FID"
                    ).fetchall()
                )
                a = numpy.zeros((b.shape[0], 3))
                a[:, 0] = b[:, 0]
                a[:, 1] = (b[:, 1] - b[:, 2]) * b[:, 3] * rho
                if self.has_second_milieu:
                    b2 = numpy.array(
                        cur.execute(
                            """SELECT potentiel, flux_eau, infiltration, surface
                            FROM noeuds_second_milieu ORDER BY OGC_FID"""
                        ).fetchall()
                    )
                    a2 = numpy.zeros((b2.shape[0], 3))
                    a2[:, 0] = b2[:, 0]
                    a2[:, 1] = (b2[:, 1] - b2[:, 2]) * b2[:, 3] * rho

                # potentiel
                for row in a[:, 0]:
                    htsoec_sat.write("%15.7E" % row)
                htsoec_sat.write("\n")
                if self.has_second_milieu:
                    for row in a2[:, 0]:
                        htsoec_sat.write("%15.7E" % row)
                    htsoec_sat.write("\n")
                # gradients
                for i in [1, 2]:
                    for row in a[:, i]:
                        htsoec_sat.write("%15.7E" % row)
                    htsoec_sat.write("\n")
                if self.has_second_milieu:
                    for i in [1, 2]:
                        for row in a2[:, i]:
                            htsoec_sat.write("%15.7E" % row)
                        htsoec_sat.write("\n")

        # fichier data.sat
        if os.path.exists("data.sat"):
            shutil.copy2("data.sat", sat_dir)
            return

        dispersion = simulation_data["TYPE_INJECTION"] != "aucune"
        type_infiltration = simulation_data["TYPE_INFILTRATION"]

        aectran = type_infiltration != "permanente" or simulation_data["FUITE"]
        aecditr = dispersion and aectran

        if aecditr:
            code_calcul = "aecditr"
            mode_calcul = "%ecoulement + dispersion transitoire"
        elif dispersion:
            code_calcul = "aditran"
            mode_calcul = "%dispersion transitoire"
        else:
            code_calcul = "aectran"
            mode_calcul = "%ecoulement transitoire"

        if simulation_data["INSATURE"] == "oui":
            simulation_data["UNSATURATED"] = ""
            simulation_data["SATURATED"] = "%"
        else:
            simulation_data["UNSATURATED"] = "%"
            simulation_data["SATURATED"] = ""

        # todo : pompages
        pompages = False
        templates_dir = os.path.join(os.path.dirname(__file__), "templates", "metis")

        dict_substitute = {
            "PROBES": "" if len(self.points_interet) > 0 else "%%",
            "DISPERSION": "" if dispersion else "%%",
            "INF_PERMANENTE": "" if type_infiltration == "permanente" else "%%",
            "INF_TRANSITOIRE": "" if type_infiltration != "permanente" else "%%",
            "AECTRAN": "" if aectran else "%%",
            "NOT_AECTRAN": "%%" if aectran else "",
            "AECDITR": "" if aecditr else "%%",
            "POMPAGES": "" if pompages else "%%",
            "TITRE_CALCUL": "% ecoulement + dispersion"
            if dispersion
            else "% ecoulement seul",
            "TYPE_CALCUL": 1 if dispersion else 0,
            "MODE_CALCUL": mode_calcul,
            "CODE_CALCUL": code_calcul,
            "DTZS": 0.2 * param["DLZS"],
            "NBDATES": number_lines(os.path.join(sat_dir, "dates_simulation.txt")),
        }
        if "DLZS2" in param:
            dict_substitute["DTZS2"] = 0.2 * param["DLZS2"]
        dict_substitute.update(param)
        dict_substitute.update(self.settings_to_dict(settings))
        dict_substitute.update(simulation_data)
        dict_substitute.update(self.get_options())

        if "WC2" in dict_substitute:
            with open(os.path.join(sat_dir, "data_tmp.sat"), "w") as data_sat, open(
                os.path.join(templates_dir, "metis_double_milieu.sat")
            ) as metis_sat:
                data_sat.write(
                    string.Template(metis_sat.read()).substitute(dict_substitute)
                )
        else:
            with open(os.path.join(sat_dir, "data_tmp.sat"), "w") as data_sat, open(
                os.path.join(templates_dir, "metis.sat")
            ) as metis_sat:
                data_sat.write(
                    string.Template(metis_sat.read()).substitute(dict_substitute)
                )

    def read_insat_flux(self, insat_dir, name="debit"):
        """reads the outgoing flux from a ZNS column

        :param insat_dir: path of insaturated folder
        :type insat_dir: string
        :param name: flux name
        :type name: string

        :return ldates: dates list
        :rtype ldates: list
        :return lflux: outgoing flux value list
        :rtype lflux": list
        """
        logger.debug("read_insat_flux")

        ldates = []
        lflux = []
        with open(os.path.join(insat_dir, name + ".insat")) as fil:
            for line in fil:
                if "#" not in line and line.split() != []:
                    ldates.append(float(line.split()[0]))
                    lflux.append(check_float(line.split()[1]))

        return ldates, lflux

    def write_sat_flux(
        self, sat_dir, name, dates, nodes, meshes, ldates, lnodes, lmeshes, lflux
    ):
        """creates the file giving the incoming flux into the groundwater

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param name: flux name
        :type name: string
        :param dates: list of dates
        :type dates: list
        :param nodes: total injection nodes list
        :type nodes: list
        :param meshes: total injection meshes list
        :type meshes: list
        :param ldates: dates list
        :type ldates: list
        :param lnodes: list of injection nodes lists per injection
        :type lnodes: list
        :param lmeshes: list of injection meshes lists per injection
        :type lmeshes: list
        :param lflux: list of time-dependant flux lists per injection
        :type lflux": list
        """

        logger.debug("write_sat_flux")

        with sqlite.connect(self.database) as conn:
            cur = conn.cursor()
            (type_infiltration,) = cur.execute(
                "SELECT type_infiltration FROM simulations"
            ).fetchone()
            infiltration_transitoire = type_infiltration != "permanente"
            tup = cur.execute(
                """SELECT OGC_FID, surface, infiltration
                FROM noeuds ORDER BY OGC_FID"""
            ).fetchall()

            if name != "debit_eau":
                # décroissance radioactive pour le bilan de masse
                (nb_elem,) = cur.execute(
                    """SELECT COUNT(1) FROM elements_chimiques
                    WHERE id = (SELECT eid FROM simulations LIMIT 1)"""
                ).fetchone()
                decrad = (
                    cur.execute(
                        """
                    SELECT decroissance_radioactive FROM elements_chimiques
                    WHERE id = (SELECT eid FROM simulations LIMIT 1)"""
                    ).fetchone()[0]
                    if nb_elem
                    else 0.0
                )

            nodes_all = [x[0] for x in tup]
            surf = [x[1] for x in tup]

            # si l'infiltration est transitoire, on crée un seul fichier debit_eau_entrant.sat
            # avec les flux à tous les noeuds (infiltration + fuite éventuelle = sortie ZNS)
            # sinon le fichier debit_eau_entrant.sat ne contient que le flux correspondant à la fuite
            # et l'infiltration (permanente) est lue dans le fichier infiltration.sat
            flux = [0.0] * len(tup)
            flux_init = [(0.0 if infiltration_transitoire else -x[2]) for x in tup]

        tini = (
            0.0  # date à partir de laquelle le flux de matière devient non nul (debit)
        )
        # ou première date de flux d'eau (debit_eau)
        nodes_sorted = set(nodes)
        dates_sorted = sorted(dates)

        if name == "debit_eau":

            logger.notice("Computing groundwater infiltration")
            with open(os.path.join(sat_dir, name + "_entrant.sat"), "w") as fil, open(
                os.path.join(sat_dir, name + "_entrant_plot.sat"), "w"
            ) as filp:
                # if dates_sorted[0] > 0.:
                #    fil.write("date %e\n"%(0))
                #    for node in nodes_sorted:
                #        fil.write("%d %e 0\n"%(node, 0))
                #    fil.write("date %e\n"%(0.999*dates_sorted[0]))
                #    for node in nodes_sorted:
                #        fil.write("%d %e 0\n"%(node, 0))

                tini = dates_sorted[0]

                for date in dates_sorted:
                    fil.write("date %e\n" % (date))
                    for node in nodes_sorted:
                        flux[node - 1] = 0.0
                    for inj in range(len(ldates)):
                        if date in ldates[inj]:
                            idt = ldates[inj].index(date)
                            for node, ratio, ratio_surface in lnodes[inj]:
                                flux[node - 1] += (
                                    max(0.0, flux_init[node - 1] + lflux[inj][idt])
                                    * ratio_surface
                                )

                    for node in nodes_sorted:
                        flux[node - 1] = -max(0, flux[node - 1])
                        # metis n'accepte que des flux négatifs
                        fil.write("%d %e 0\n" % (node, flux[node - 1]))

                    ftot_nodes_sorted = 0
                    for node in nodes_sorted:
                        ftot_nodes_sorted += flux[node - 1] * surf[node - 1]
                    filp.write("%e %e\n" % (date, ftot_nodes_sorted))

        else:

            logger.notice("Computing groundwater mass input")
            fluxmin = 0.0

            with open(os.path.join(sat_dir, name + "_entrant.sat"), "w") as fil, open(
                os.path.join(sat_dir, name + "_entrant_plot.sat"), "w"
            ) as filp:
                tini = (
                    -1.0
                )  # signifie que le flux est constamment nul => pas de calcul saturé

                mtot = 0
                ftot = []
                for i, date in enumerate(dates_sorted):
                    fil.write("date  %e\n" % (date))
                    for node in nodes_sorted:
                        flux[node - 1] = 0.0
                    for inj in range(len(ldates)):
                        if date in ldates[inj]:
                            idt = ldates[inj].index(date)
                            for node, ratio, ratio_surface in lnodes[inj]:
                                flux[node - 1] += lflux[inj][idt] * ratio

                    for node in nodes_sorted:
                        if flux[node - 1] > fluxmin and tini < 0.0:
                            tini = dates_sorted[i - 1] if i > 0 else dates_sorted[0]

                    ftot.append(0)
                    for node in nodes_sorted:
                        ftot[i] += -flux[node - 1]
                        fil.write("%d  %e\n" % (node, -flux[node - 1]))
                    filp.write(
                        " ".join((2 + len(nodes_sorted)) * ["%e"])
                        % (
                            (date, ftot[i])
                            + tuple(-flux[node - 1] for node in nodes_sorted)
                        )
                        + "\n"
                    )
                    if i == 0:
                        mtot = -0.5 * ftot[i] * date
                    else:
                        mtot = mtot * numpy.exp(
                            -decrad * (dates_sorted[i] - dates_sorted[i - 1])
                        ) - 0.5 * (ftot[i] + ftot[i - 1]) * (
                            dates_sorted[i] - dates_sorted[i - 1]
                        )

                logger.notice("total mass from unsaturated zone is ", mtot)

        logger.debug("tini = ", tini)
        return tini

    def create_debit_sat(self, sat_dir, flux):
        """builds the input file for METIS in case of no unsaturated calculation

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param flux: flux value list
        :type flux": list
        """
        with open(os.path.join(sat_dir, "debit_entrant.sat"), "w") as fil:
            for (t, f) in flux:
                fil.write("date %15.7E\n" % (t))
                for (n, x, y, v) in f:
                    fil.write("%8d%15.7E\n" % (n, v))

    def create_hydrostationary_files(self, sat_dir, with_permeabilities=True):
        """builds the input files for METIS saturated stationary flow and inversion

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param with_permeabilities: boolean used to create or not the permeability file
        :type with_permeabilities: boolean
        """

        with sqlite.connect(self.database) as conn:
            cur = conn.cursor()

            # potentiel set to NULL
            cur.execute("UPDATE noeuds SET potentiel = NULL")
            cur.execute("UPDATE mailles SET potentiel = NULL")

            conn.commit()

            self.create_maillage_sat(sat_dir, cur)

            if with_permeabilities:
                self.create_permeabilities_sat(
                    sat_dir, cur=cur
                )  # pas utile pour l'inversion, utile pour create_hydrostationary
            self.create_mnt_sat(
                sat_dir, cur
            )  # pas utile, seulement pour comparaison avec hynverse

            self.create_mur_sat(sat_dir, cur)

            (nnoeuds,) = cur.execute("SELECT COUNT(1) FROM noeuds").fetchone()

            # infiltration
            with open(
                os.path.join(sat_dir, "infiltration.sat"), "w"
            ) as infiltration_sat:
                infiltration_sat.write("esui 4\n")
                cur.execute(
                    "SELECT OGC_FID, -infiltration FROM noeuds ORDER BY OGC_FID"
                )
                for row in cur.fetchall():
                    infiltration_sat.write("%8d%15.7E 0\n" % row)
                infiltration_sat.write("n\n")

            # potentiel imposé
            with open(os.path.join(sat_dir, "ehim.sat"), "w") as ehim_sat:
                ehim_sat.write("ehim 4\n")
                cur.execute("SELECT nid, valeur FROM potentiel_impose ORDER BY nid")
                data = cur.fetchall()
                for row in data:
                    ehim_sat.write("%5i%10.2f\n" % row)
                ehim_sat.write("n\n")

            # épaisseur aquifère = (mnt-mur)
            with open(os.path.join(sat_dir, "epaquifere.sat"), "w") as aquifere_sat:
                cur.execute("SELECT epaisseur_aquifere FROM noeuds ORDER BY OGC_FID")
                for row in cur.fetchall():
                    aquifere_sat.write("%15.7E\n" % row)

            if len(self.points_interet) > 0:
                with open(os.path.join(sat_dir, "probes_nodes.sat"), "w") as fil:
                    nb_nodes_on_line = 0
                    max_nodes_on_line = 10
                    for p in self.points_interet:
                        if not nb_nodes_on_line:
                            fil.write("l")
                        fil.write("%8i" % p.nodes[0])
                        nb_nodes_on_line += 1
                        if nb_nodes_on_line == max_nodes_on_line:
                            fil.write("\n")
                            nb_nodes_on_line = 0
                    if nb_nodes_on_line:
                        fil.write("\n")

        # fichier data.sat
        if os.path.exists(os.path.join(sat_dir, "data.sat")):
            logger.notice("Fichier data.sat utilisateur")
            return

        # todo : pompages
        pompages = False
        templates_dir = os.path.join(os.path.dirname(__file__), "templates", "metis")

        dict_substitute = {
            "PROBES": "" if len(self.points_interet) > 0 else "%%",
            "POMPAGES": "" if pompages else "%%",
        }
        dict_substitute.update(self.get_hynverse_options())

        with open(os.path.join(sat_dir, "data.sat"), "w") as data_sat, open(
            os.path.join(templates_dir, "metis_hynverse.sat")
        ) as metis_sat:
            data_sat.write(
                string.Template(metis_sat.read()).substitute(dict_substitute)
            )

    def create_maillage_sat(self, sat_dir, cur):
        """builds the mesh saturated METIS file

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param cur: cursor
        :type cur: sqlite3.Cursor
        """

        (nnoeuds,) = cur.execute("SELECT COUNT(1) FROM noeuds").fetchone()
        (nmailles,) = cur.execute("SELECT COUNT(1) FROM mailles").fetchone()
        with open(os.path.join(sat_dir, "data.mail"), "w") as maillage:
            maillage.write("%12d%12d\n" % (nnoeuds, nmailles))
            cur.execute("SELECT X(GEOMETRY), Y(GEOMETRY) FROM noeuds ORDER BY OGC_FID")
            for row in cur.fetchall():
                maillage.write("%15.3f%15.3f\n" % row)

            cur.execute("SELECT a, b, c, d FROM mailles ORDER BY OGC_FID")
            for [a, b, c, d] in cur.fetchall():
                if not d:
                    maillage.write("%12d%12d%12d%12d\n" % (2, a, b, c))
                else:
                    maillage.write("%12d%12d%12d%12d%12d\n" % (1, a, b, d, c))

    def create_mnt_sat(self, sat_dir, cur):
        """builds the mnt saturated METIS file

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param cur: cursor
        :type cur: sqlite3.Cursor
        """

        with open(os.path.join(sat_dir, "mnt.sat"), "w") as mnt_sat:
            cur.execute("SELECT altitude FROM noeuds ORDER BY OGC_FID")
            for row in cur.fetchall():
                mnt_sat.write("%f\n" % row)

    def create_mur_sat(self, sat_dir, cur):
        """builds the mur saturated METIS file

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param cur: cursor
        :type cur: sqlite3.Cursor
        """

        with open(os.path.join(sat_dir, "mur.sat"), "w") as mur_sat:
            cur.execute("SELECT altitude_mur FROM noeuds ORDER BY OGC_FID")
            for row in cur.fetchall():
                mur_sat.write("%f\n" % row)

        if not self.has_second_milieu:
            return

        shutil.copyfile(
            os.path.join(sat_dir, "mur.sat"), os.path.join(sat_dir, "mur2.sat")
        )

    def create_permeabilities_sat(self, sat_dir, cur=None, liste_pm=None):
        """builds the permeabilities saturated METIS file

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param cur: cursor
        :type cur: sqlite3.Cursor
        :param liste_pm: permeabilities data per mesh element
        :type liste_pm: list
        """

        with open(os.path.join(sat_dir, "permeabilites.sat"), "w") as fil, open(
            os.path.join(sat_dir, "permeabilites_mesh.sat"), "w"
        ) as fmesh, open(
            os.path.join(sat_dir, "permeabilites_nodes.sat"), "w"
        ) as fnodes:
            fil.write("#METIS\n" "#param\n" "#ncomp  2\n")
            if liste_pm:
                for x in liste_pm:
                    fil.write("%15.7E%15.7E\n" % (x[0], x[1]))
            elif cur:
                cur.execute(
                    "SELECT X(CENTROID(GEOMETRY)), Y(CENTROID(GEOMETRY)), permeabilite_x, permeabilite_y FROM mailles ORDER BY OGC_FID"
                )
                for row in cur.fetchall():
                    fil.write("%15.7E%15.7E\n" % (row[2], row[3]))
                    fmesh.write("%f  %f %15.7E%15.7E\n" % row)
                cur.execute(
                    "SELECT X(GEOMETRY), Y(GEOMETRY), permeabilite_x, permeabilite_y FROM noeuds ORDER BY OGC_FID"
                )
                for row in cur.fetchall():
                    fnodes.write("%f  %f %15.7E%15.7E\n" % row)

        if not self.has_second_milieu:
            return

        with open(os.path.join(sat_dir, "permeabilites2.sat"), "w") as fil:
            fil.write("#METIS\n" "#param\n" "#ncomp  2\n")

            # something to do in case of second_milieu ?
            if liste_pm:
                for x in liste_pm:
                    fil.write("%15.7E%15.7E\n" % (x[0], x[1]))
            elif cur:
                cur.execute(
                    """
                    SELECT sm.permeabilite_x, sm.permeabilite_y
                    FROM mailles AS m JOIN mailles_second_milieu AS sm ON sm.OGC_FID=m.OGC_FID
                    ORDER BY sm.OGC_FID
                    """
                )
                for row in cur.fetchall():
                    fil.write("%15.7E%15.7E\n" % row)

    def create_velocities_sat(self, sat_dir, cur=None, liste_vm=None, init=False):
        """builds the velocities saturated METIS file (unused)

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param cur: cursor
        :type cur: sqlite3.Cursor
        :param liste_vm: velocities data per mesh element
        :type liste_vm: list
        :param init: boolean, True if only initialisation to (0 0 0)
        :type init: boolean
        """

        return

    def update_database(self, conn, compute_dir):
        """update database after inversion

        :param conn: conn on a sqlite database
        :type conn: sqlite3.Connection
        :param compute_dir: path of compute folder
        :type compute_dir: string
        """

        cur = conn.cursor()
        (nnoeuds,) = cur.execute("SELECT COUNT(1) FROM noeuds").fetchone()
        (nmailles,) = cur.execute("SELECT COUNT(1) FROM mailles").fetchone()

        logger.notice("updating potential and water flux")
        rho = 1000.0
        surface = numpy.array(
            [
                x[0]
                for x in cur.execute(
                    "SELECT surface FROM noeuds ORDER BY OGC_FID"
                ).fetchall()
            ]
        )
        infiltration = numpy.array(
            [
                x[0]
                for x in cur.execute(
                    "SELECT infiltration FROM noeuds ORDER BY OGC_FID"
                ).fetchall()
            ]
        )

        data = parse_file(os.path.join(compute_dir, "htsoec.met"), nnoeuds, 3)[:2]
        values = [(v, j + 1) for j, v in enumerate(data[0][:-1])]
        cur.executemany("UPDATE noeuds SET potentiel = ? WHERE OGC_FID = ?", values)

        values = infiltration + numpy.array(data[1][:-1]) / surface / rho
        values = [(v, j + 1) for j, v in enumerate(values)]
        cur.executemany("UPDATE noeuds SET flux_eau = ? WHERE OGC_FID = ?", values)

        logger.notice("updating ZS and ZNS thicknesses")
        cur.execute(
            "SELECT altitude, altitude_mur, potentiel FROM noeuds ORDER BY OGC_FID"
        )

        for i, [mnt, mur, potentiel] in enumerate(cur.fetchall()):
            cur.execute(
                "UPDATE noeuds SET epaisseur_zs = ? WHERE OGC_FID = ? ",
                (potentiel - mur, i + 1),
            )
            cur.execute(
                "UPDATE noeuds SET epaisseur_zns = ? WHERE OGC_FID = ? ",
                (mnt - potentiel, i + 1),
            )

        columns = ["epaisseur_zs", "epaisseur_zns", "potentiel", "flux_eau"]

        for column in columns:
            logger.debug("updating mesh", column)
            values = cur.execute(
                "SELECT " + column + ", OGC_FID FROM noeuds ORDER BY OGC_FID"
            ).fetchall()
            db_node_to_elem(cur, column, values)

        logger.notice("updating Darcy velocities")
        data = parse_file(os.path.join(compute_dir, "darcy.sat"), 3, nmailles)
        v = numpy.array(data)
        cur.executemany(
            "UPDATE mailles SET v_x = ?, v_y = ?, v_norme = ? WHERE OGC_FID = ? ", v
        )

        columns = ["v_x", "v_y", "v_norme"]

        for column in columns:
            logger.debug("updating nodes", column)
            values = cur.execute(
                "SELECT " + column + ", OGC_FID FROM mailles ORDER BY OGC_FID"
            ).fetchall()
            db_elem_to_node(cur, column, values)

    def check_maillage(self, a, b, c):
        """Unused"""
        with sqlite.connect(self.database) as conn:
            cur = conn.cursor()
            xa, ya, = cur.execute(
                "SELECT X(GEOMETRY), Y(GEOMETRY) FROM noeuds WHERE OGC_FID = " + str(a)
            ).fetchone()
            xb, yb, = cur.execute(
                "SELECT X(GEOMETRY), Y(GEOMETRY) FROM noeuds WHERE OGC_FID = " + str(b)
            ).fetchone()
            xc, yc, = cur.execute(
                "SELECT X(GEOMETRY), Y(GEOMETRY) FROM noeuds WHERE OGC_FID = " + str(c)
            ).fetchone()

        if (xc - xb) * (ya - yb) > (xa - xb) * (yc - yb):
            return (2, a, b, c)
        else:
            logger.notice("inversion du triangle ", a, b, c)
            return (2, a, c, b)
