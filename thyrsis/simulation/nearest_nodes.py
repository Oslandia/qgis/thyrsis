"""
Calculates nearest nodes for each point of interest
 and return results in nearest_node.csv

USAGE:
    python -m thyrsis.simulation.nearest_nodes database.sqlite [table_name, nodes_number]

OPTIONS

   table_name : name of the points table, among ['points_interet', 'points_pilote'] (default points_interet)
   nodes_number : number of nearest nodes to find out (default 1)
"""
import os
import sys
from math import sqrt

try:
    from rtree import index
except Exception:
    import site

    from thyrsis.__about__ import DIR_PLUGIN_ROOT

    site.addsitedir(DIR_PLUGIN_ROOT / "embedded_external_libs")
    from thyrsis.embedded_external_libs.rtree import index

from ..database import sqlite
from ..log import logger

PRECISION = 0.01


class Point(object):
    """Class object for pilot point"""

    def __init__(self, x, y, num, nom):
        """Constructor

        :param x: x coord
        :type x: float
        :param y: y coord
        :type y: float
        :param num: zone id
        :type num: int
        :param nom: pilot point name
        :type nom: string
        """

        self.x = x
        self.y = y
        self.num = num  # numero dans liste_pp totale
        self.nom = nom
        self.nodes = []  # node numbers, not node indexes (number = index + 1)
        self.distances = []


def distance(x1, y1, x2, y2):
    """Compute distance between two points

    :param x1: x1 coord
    :type x1: float
    :param y1: y1 coord
    :type y1: float
    :param x2: x2 coord
    :type x2: float
    :param y2: y2 coord
    :type y2: float

    :return: distance
    :rtype: float
    """
    return sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))


class Nearest(object):
    """Process class for inversion calculation"""

    def __init__(self, database, compute_dir, table="points_interet", nodes_number=1):
        """Constructor

        :param database: simulation database
        :type database: sqlite database
        :param compute_dir: directory where to create the nearest_node.csv file
        :type database: string
        :param table: points table
        :type table: string
        :param nodes_number: nodes number to find out
        :type nodes_number: integer

        """
        self.__database = database
        self.__table = table
        self.__nodes_number = nodes_number
        self.__liste_pp = self.points()
        self.__n_pp = len(self.__liste_pp)
        if self.__n_pp == 0:
            return
        self.__idx_n, self.__xy_n = self.index_nodes()
        self.index_npp(compute_dir)

    def points(self):
        """Defines points and indexes them

        :return list_pp: points list
        :rtype list_pp: list
        """

        logger.notice("Getting points from table %s" % (self.__table))
        with sqlite.connect(self.__database) as conn:
            cur = conn.cursor()
            n_pp = 0
            liste_pp = []
            cur.execute(
                """SELECT X(GEOMETRY), Y(GEOMETRY), nom
                FROM %s WHERE groupe='calcul' ORDER BY nom"""
                % (self.__table)
            )
            for x, y, nom in cur.fetchall():
                liste_pp.append(Point(x, y, n_pp + 1, nom))
                n_pp += 1
            logger.notice("Number of points = ", n_pp)

        return liste_pp

    def index_nodes(self):
        """Computes index for nodes coordinates

        :return idx_n: nodes index
        :rtype idx_n: rtree.Index
        :return xy_n: nodes coordinates list
        :rtype xy_n: list
        """
        # indexage des noeuds
        logger.notice("Indexing nodes")
        idx_n = index.Index()
        i_n = 0
        xy_n = []
        with sqlite.connect(self.__database) as conn:
            cur = conn.cursor()
            cur.execute("SELECT X(GEOMETRY), Y(GEOMETRY) FROM noeuds ORDER BY OGC_FID")
            for x, y in cur.fetchall():
                idx_n.insert(i_n, (x, y, x, y))
                xy_n.append((x, y))
                i_n += 1
        return idx_n, xy_n

    def index_npp(self, compute_dir):
        """Computes index of nearest nodes to pilot point

        :return idx_npp: npp index
        :rtype idx_npp: list
        :return pds_npp: weight of nearest nodes
        :rtype pds_npp: list

        """
        # index des noeuds les plus proches des points
        logger.notice("Indexing %d nearest node(s) from points" % (self.__nodes_number))

        idx_npp = []
        pds_npp = []
        for pp in self.__liste_pp:
            inodes = list(
                self.__idx_n.nearest((pp.x, pp.y, pp.x, pp.y), self.__nodes_number)
            )
            pp.distances = [
                distance(pp.x, pp.y, self.__xy_n[n][0], self.__xy_n[n][1])
                for n in inodes
            ]
            pp.nodes = [i + 1 for i in inodes]
            if pp.distances[0] > PRECISION:
                logger.warning(
                    "Point %s is %e m from node %d"
                    % (pp.nom, pp.distances[0], pp.nodes[0])
                )
            else:
                logger.notice("Point %s is on node %d" % (pp.nom, pp.nodes[0]))

        if not os.path.isfile(os.path.join(compute_dir, "nearest_node.csv")):
            with open(os.path.join(compute_dir, "nearest_node.csv"), "w") as fil:
                logger.notice("Creating nearest_node.csv")
                for pp in self.__liste_pp:
                    fil.write("%s;" % (pp.nom))
                    for i, n in enumerate(pp.nodes):
                        fil.write("%d;%f;" % (n, pp.distances[i]))
                    fil.write("\n")

    def get_points(self):
        """Returns list of points

        :return list_pp: Point list
        :rtype list_pp: list
        """

        return self.__liste_pp


def nearest_nodes(dbname, table, nodes_number):
    """Computes nearest node of points in table

    :param dbname: path to database
    :type dbname: string
    :param table: points table
    :type run: string

    :return: file nearest_node.csv
    :rtype: file
    """
    logger.set_level("notice")

    logger.notice("\nSearching for nearest nodes for table %s" % (table))

    Nearest(dbname, os.getcwd(), table, nodes_number)


# run as script if invoqued as such
if __name__ == "__main__":

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    logger.enable_console(True)

    if len(sys.argv) not in [2, 3, 4]:
        logger.error(
            "wrong number of arguments (try python -m thyrsis.simulation.nearest --help)"
        )
        exit(1)

    table_list = ["points_interet", "points_pilote"]
    dbname = None
    table = "points_interet"
    nodes_number = 1
    for arg in sys.argv[1:]:
        if arg[-7:] == ".sqlite":
            dbname = os.path.join(os.getcwd(), arg)
        elif arg[:14] == "-nodes_number=":
            nodes_number = int(arg.split("=")[1])
        elif arg in table_list:
            table = arg
        else:
            exit("unknown argument, exit")

    nearest_nodes(dbname, table, nodes_number)
