import os
import re
import struct
import sys
import tempfile
from builtins import range, str, zip
from itertools import groupby
from math import ceil
from subprocess import PIPE, Popen

import numpy
from osgeo import gdal, ogr, osr
from shapely import ops, wkt
from shapely.geometry import  (
    LineString, 
    MultiLineString, 
    Point, 
    MultiPoint,
)

from .log import logger


def discretize_line(line, elem_length):
    """Get line part, splitted by length

    :param line: Linestring
    :type line: Linestring
    :param elem_length: length split
    :type elem_length: float

    :return:
    :rtype: LineString
    """

    def split_line(l, nb_elem):
        return LineString(
            (
                l.interpolate(x, True).coords[0]
                for x in (float(i) / nb_elem for i in range(nb_elem + 1))
            )
        )

    points = [line.coords[0]]
    for point in line.coords[1:]:
        d = Point(points[-1]).distance(Point(point))
        if d > elem_length:
            points += list(
                split_line(
                    LineString([points[-1], point]), int(ceil(d / elem_length))
                ).coords[1:]
            )
        else:
            points.append(point)
    return LineString([xy[:2] for xy in points])


def substring(line, start, end):
    """return the substring bewteen curvilinear coordinates start and end
    the line must be a ring if start > end

    :param line: Linestring
    :type line: Linestring
    :param start: start distance
    :type start: float
    :param end: end distance
    :type end: float
    """
    line = LineString(line) if not isinstance(line, LineString) else line
    if not len(line.coords):
        return line
    assert start < end or line.coords[0] == line.coords[-1]
    assert 0 <= start <= 1 and 0 <= end <= 1
    length = line.length
    line = LineString(line)  # in case it's a LinearRing

    # get the start point
    result = list(line.interpolate(start, True).coords)

    points = list(line.coords)

    # get the start index
    start_length, end_length = start * length, end * length
    start_idx, end_idx = None, None
    total_length = 0
    for i in range(len(points)):
        segment = LineString([points[i], points[(i + 1) % len(points)]])
        total_length += segment.length
        if start_idx is None and total_length > start_length:
            start_idx = (i + 1) % len(points)
        if end_idx is None and total_length > end_length:
            end_idx = i

    if end_idx is None:
        end_idx = len(points) - 2

    if start_idx is None:
        start_idx = len(points) - 1

    result += (
        points[start_idx : end_idx + 1]
        if end > start
        else points[start_idx:] + points[0 : end_idx + 1]
    )

    result += line.interpolate(end, True).coords

    return LineString([x[0] for x in groupby(result)])  # remove dupes


def split(line, lines, tolerance):
    """Split line with lines

    :param line: line to split
    :type line: LineString
    :param lines: lines
    :type line: MultiLineString
    :param tolerance: tolerance value
    :type tolerance: float

    :return: splitted line
    :rtype: MultiLineString
    """
    lines = lines if isinstance(lines, MultiLineString) else MultiLineString([lines])
    if not len(lines.geoms):
        return MultiLineString([line])
    inter = line.intersection(lines)
    if isinstance(inter, Point):
        inter = MultiPoint([inter])
    splits = sorted([line.project(p, True) for p in inter.geoms])    
    if splits[0] != 0.0:
        splits = [0] + splits
    if splits[-1] != 1.0:
        splits += [1]

    parts = [substring(line, s, e) for s, e in zip(splits[:-1], splits[1:])]
    return MultiLineString([p for p in parts if p.length >= tolerance])


def polygonize(cur):
    """Polygonize domaines from coutour entities

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    """
    project_srid = str(
        cur.execute(
            "SELECT srid FROM geometry_columns WHERE f_table_name = 'contours'"
        ).fetchone()[0]
    )
    sql = """
        delete from domaines
        ;;
        delete from extremites
        ;;
        delete from bords
        ;;

        insert into extremites(GEOMETRY)
        select makepoint(X(ST_StartPoint(e.GEOMETRY)), Y(ST_StartPoint(e.GEOMETRY)), st_srid(e.GEOMETRY)) from contours as e
            where (select count(1) from contours as b where b.OGC_FID != e.OGC_FID and st_intersects(b.GEOMETRY, ST_StartPoint(e.GEOMETRY))) = 0
        union
        select makepoint(X(ST_EndPoint(e.GEOMETRY)), Y(ST_EndPoint(e.GEOMETRY)), st_srid(e.GEOMETRY)) from contours as e
            where (select count(1) from contours as b where b.OGC_FID != e.OGC_FID and st_intersects(b.GEOMETRY, ST_EndPoint(e.GEOMETRY))) = 0
        ;;
        """.format(
        epsilon=1
    )

    for statement in sql.split(";;"):
        cur.execute(statement)

    edges = []
    for line, elem_length, lines in cur.execute(
        """
            select st_astext(a.GEOMETRY), a.longueur_element, st_astext(st_collect(b.GEOMETRY))
            from contours as a, contours as b
            where a.OGC_FID != b.OGC_FID
            and st_intersects(a.GEOMETRY, b.GEOMETRY)
            group by a.OGC_FID
            union
            select st_astext(a.GEOMETRY), a.longueur_element, 'MULTILINESTRING EMPTY'
            from contours as a
            where (select count(1) from contours as b where st_intersects(a.GEOMETRY, b.GEOMETRY) and a.OGC_FID!=b.OGC_FID)=0
            """
    ):
        if line is not None:
            edges += [
                discretize_line(l, elem_length)
                for l in split(wkt.loads(line), wkt.loads(lines), 0.001).geoms
            ]

    cur.executemany(
        "INSERT INTO bords(GEOMETRY) VALUES(SnapToGrid(GeomFromText(?, %s), .001))"
        % (project_srid),
        [(p.wkt,) for p in edges],
    )

    polygons = ops.polygonize(edges)
    cur.executemany(
        "INSERT INTO domaines(GEOMETRY) VALUES(SnapToGrid(GeomFromText(?, %s), .001))"
        % (project_srid),
        [(p.wkt,) for p in polygons],
    )


def _get_raster_transformation(src_ds, project_srid):
    """Get transformation

    :param src_ds: SpatialReference
    :type src_ds: SpatialReference
    :param project_srid: epsg code
    :type project_srid: string

    :return: coordinate transformation
    :rtype: CoordinateTransformation
    """

    # Proj4 string of IGNF LAMBERT2 CARTO (including local deformation grids)
    raster_sr = osr.SpatialReference()
    raster_sr.ImportFromWkt(src_ds.GetProjectionRef())
    trans = None
    if raster_sr.GetAuthorityName("PROJCS") != "EPSG" or raster_sr.GetAuthorityCode(
        "PROJCS"
    ) != str(project_srid):
        # we have to transform raster data
        src_sr = osr.SpatialReference()
        src_sr.ImportFromEPSG(int(project_srid))
        trans = osr.CoordinateTransformation(src_sr, raster_sr)
    return trans


def _raster_values(cur, source, nodes=True):
    """Get raster values at each node/mesh element

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param source: raster path
    :type source: string
    :param nodes: node/mesh flag
    :type nodes: bool
    """
    src_ds = gdal.Open(source)
    project_srid = str(
        cur.execute(
            "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
        ).fetchone()[0]
    )
    trans = _get_raster_transformation(src_ds, project_srid)

    data = []
    if nodes:
        for id_, x, y in cur.execute(
            """
            SELECT OGC_FID, X(GEOMETRY), Y(GEOMETRY) FROM noeuds
            """
        ).fetchall():
            data.append((_get_z(src_ds, x, y, trans), id_))
    else:
        for id_, x, y in cur.execute(
            """
            SELECT OGC_FID, X(CENTROID(GEOMETRY)), Y(CENTROID(GEOMETRY)) FROM mailles
            """
        ).fetchall():
            data.append((_get_z(src_ds, x, y, trans), id_))
    return data


def _get_z(src_ds, x, y, trans=None):
    """Get z value at a point

    :param src_ds: SpatialReference
    :type src_ds: SpatialReference
    :param x: x coordinate
    :type x: float
    :param y: y coordinate
    :type y: float
    :param trans: coordinates transformation
    :type trans: CoordinateTransformation

    :return: z value
    :rtype: float
    """
    # Convert from map to pixel coordinates.
    # Only works for geotransforms with no rotation.
    if trans is not None:
        p = ogr.Geometry(ogr.wkbPoint)
        p.AddPoint(x, y)
        p.Transform(trans)
        nx, ny = p.GetX(), p.GetY()
    else:
        nx, ny = x, y

    gt = src_ds.GetGeoTransform()
    rb = src_ds.GetRasterBand(1)
    px = int((nx - gt[0]) / gt[1])  # x pixel
    py = int((ny - gt[3]) / gt[5])  # y pixel

    # gestion des points sur les bords
    if nx == gt[0] + gt[1] * src_ds.RasterXSize:
        px = px - 1
    if ny == gt[3] + gt[5] * src_ds.RasterYSize:
        py = py - 1

    if px < 0 or py < 0 or px >= src_ds.RasterXSize or py >= src_ds.RasterYSize:
        logger.notice("No value at point {} {}, taking nearest point".format(nx, ny))
        if px < 0:
            px = 0
        elif px >= src_ds.RasterXSize:
            px = src_ds.RasterXSize - 1
        if py < 0:
            py = 0
        elif py >= src_ds.RasterYSize:
            py = src_ds.RasterYSize - 1

    value = rb.ReadRaster(px, py, 1, 1, buf_type=gdal.GDT_Float32)
    z = struct.unpack("f", value)[0]

    # extrapolation near the contour to avoid nodata values
    if z == -9999.0:
        if px == 0:
            lpx = [px, px + 1]
        elif px == src_ds.RasterXSize - 1:
            lpx = [px - 1, px]
        else:
            lpx = [px - 1, px, px + 1]
        if py == 0:
            lpy = [py, py + 1]
        elif py == src_ds.RasterYSize - 1:
            lpy = [py - 1, py]
        else:
            lpy = [py - 1, py, py + 1]
        lz = []
        for _px in lpx:
            for _py in lpy:
                v = rb.ReadRaster(_px, _py, 1, 1, buf_type=gdal.GDT_Float32)
                _z = struct.unpack("f", v)[0]
                if _z != -9999.0:
                    lz.append(_z)
        if len(lz) == 0:
            logger.notice(
                "Unable to extrapolate at point {} {} with pixels {} {}".format(
                    x, y, px, py
                )
            )
        else:
            z = sum(lz) / len(lz)
            logger.notice(
                "Extrapolated value at point {} {} is {} ({})".format(x, y, z, len(lz))
            )
    return z


def mnt(cur, source, project_srid="2154"):
    """Update database with DEM raster

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param source: raster path
    :type source: string
    :param project_srid: epsg code
    :type project_srid: string
    """
    raster_nodes = _raster_values(cur, source)
    cur.executemany("UPDATE noeuds SET altitude=? WHERE OGC_FID=?", raster_nodes)
    raster_mesh = _raster_values(cur, source, nodes=False)
    cur.executemany("UPDATE mailles SET altitude=? WHERE OGC_FID=?", raster_mesh)

    src_ds = gdal.Open(source)
    trans = _get_raster_transformation(src_ds, project_srid)
    for fid, line in cur.execute(
        "SELECT OGC_FID, st_astext(GEOMETRY) FROM contours"
    ).fetchall():
        l = LineString(
            [
                (p[0], p[1], _get_z(src_ds, p[0], p[1], trans))
                for p in wkt.loads(line).coords
            ]
        )
        cur.execute(
            "UPDATE contours SET GEOMETRY=st_GeomFromText('{}', {}) WHERE OGC_FID={}".format(
                l.wkt, project_srid, fid
            )
        )


def potentiel_reference(cur, source):
    """Update database with potential reference raster

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param source: raster path
    :type source: string
    """
    cur.executemany(
        "UPDATE noeuds SET potentiel_reference=MIN(?, altitude-0.1) WHERE OGC_FID=?",
        _raster_values(cur, source),
    )
    cur.executemany(
        "UPDATE mailles SET potentiel_reference=MIN(?, altitude-0.1) WHERE OGC_FID=?",
        _raster_values(cur, source, nodes=False),
    )


def mur(cur, source):
    """Update database with water table altitude raster

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param source: raster path
    :type source: string
    """
    raster_nodes = _raster_values(cur, source)
    if len(
        cur.execute(
            "SELECT potentiel_reference from noeuds WHERE potentiel_reference IS NULL"
        ).fetchall()
    ):
        cur.executemany(
            "UPDATE noeuds SET altitude_mur=MIN(?, altitude-0.2) WHERE OGC_FID=?",
            raster_nodes,
        )
    else:
        cur.executemany(
            "UPDATE noeuds SET altitude_mur=MIN(?, potentiel_reference-0.1) WHERE OGC_FID=?",
            raster_nodes,
        )

    raster_mesh = _raster_values(cur, source, nodes=False)
    if len(
        cur.execute(
            "SELECT potentiel_reference from mailles WHERE potentiel_reference IS NULL"
        ).fetchall()
    ):
        logger.notice("no reference potential")
        cur.executemany(
            "UPDATE mailles SET altitude_mur=MIN(?, altitude-0.2) WHERE OGC_FID=?",
            raster_mesh,
        )
    else:
        logger.notice("with reference potential")
        cur.executemany(
            "UPDATE mailles SET altitude_mur=MIN(?, potentiel_reference-0.1) WHERE OGC_FID=?",
            raster_mesh,
        )


def permeabilite(cur, source, const):
    """Update database with permeability raster

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param source: raster path
    :type source: string
    :param const: constant permeability flag
    :type const: bool
    """
    if const:
        cur.execute("UPDATE noeuds SET permeabilite_x={}".format(source))
        cur.execute("UPDATE noeuds SET permeabilite_y=permeabilite_x")

        cur.execute("UPDATE mailles SET permeabilite_x={}".format(source))
        cur.execute("UPDATE mailles SET permeabilite_y=permeabilite_x")
    else:
        cur.executemany(
            "UPDATE noeuds SET permeabilite_x=? WHERE OGC_FID=?",
            _raster_values(cur, source),
        )
        cur.execute("UPDATE noeuds SET permeabilite_y=permeabilite_x")

        cur.executemany(
            "UPDATE mailles SET permeabilite_x=? WHERE OGC_FID=?",
            _raster_values(cur, source, nodes=False),
        )
        cur.execute("UPDATE mailles SET permeabilite_y=permeabilite_x")


def potentiel_impose(cur):
    """Insert imposed potential node from contour

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    """
    cur.execute("DELETE FROM potentiel_impose")
    # FR210730 : 0.1 or 0.2 are not large enough : setting PtDistWithin parameter to 1 m.
    # FR221006 : if potentiel_reference is available we use it.
    if len(
        cur.execute(
            "SELECT potentiel_reference from noeuds WHERE potentiel_reference IS NULL"
        ).fetchall()
    ):
        logger.notice("no reference potential")
        cur.execute(
            """INSERT INTO potentiel_impose(nid,valeur) SELECT DISTINCT
            noeuds.OGC_FID, noeuds.altitude - 0.1 FROM noeuds, contours
            WHERE contours.potentiel_impose=1 AND PtDistWithin(noeuds.GEOMETRY, contours.GEOMETRY, 1)"""
        )
    else:
        logger.notice("with reference potential")
        cur.execute(
            """INSERT INTO potentiel_impose(nid,valeur) SELECT DISTINCT
            noeuds.OGC_FID, noeuds.potentiel_reference FROM noeuds, contours
            WHERE contours.potentiel_impose=1 AND PtDistWithin(noeuds.GEOMETRY, contours.GEOMETRY, 1)"""
        )
    (npotentiel_impose,) = cur.execute(
        "SELECT COUNT(1) FROM potentiel_impose"
    ).fetchone()
    logger.notice("Nombre de noeuds à potentiel imposé = ", npotentiel_impose)
