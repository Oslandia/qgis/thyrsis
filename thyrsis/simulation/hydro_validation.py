"""Creates all hydrologic cases from mesh databases, computes and compares the results from each hydrologic code

USAGE:
    python -m thyrsis.simulation.hydro_validation
        [-dir=DIRNAME -raz -init -run -results -plot -a -v] [mesh] [-mesh] [-code=CODENAME_LIST]

OPTIONS

   -dir : name of results directory [default: thyrsis_hydro_validation]
   -raz : delete previous results directory
   -init : create databases [default: False]
   -run : run simulations (compute) [default: False]
   -results : calculate results [default: False]
   -plot : plot results [default: False]
   -a : all : init, compute and results [default: False]
   -v : verbose : print debug informations [default: False]
   mesh : only deals with the case "mesh"
   -mesh : do not deal with the case "mesh"
   -code : calculation code name, otherwise calculation code defined by Settings
"""
import os
import shutil
import sys
from builtins import str
from datetime import date, datetime
from shutil import which
from subprocess import STDOUT, Popen
import matplotlib.pyplot as plt

import numpy
from scipy.stats import pearsonr

from ..database import create_computation_database, sqlite
from ..log import logger
from ..settings import Settings
from ..spatialitemeshdataprovider import SpatialiteMeshDataProvider
from ..utilities import Timer
from .metis import Metis
from .openfoam import OpenFoam


def hydro_validation(topdir, init, run, resu, plot, codes, raz, dblist):

    top_dir = os.path.join(os.getcwd(), topdir)
    if raz and os.path.exists(top_dir):
        shutil.rmtree(top_dir)
    if not os.path.exists(top_dir):
        os.mkdir(top_dir)

    cputimes = {}
    coefs = {}
    for db in dblist:
        template = "_".join(os.path.basename(db).split(".")[0].split("_")[:2])
        template_dir = os.path.join(top_dir, template)
        # DIF hydro model is not valid
        if "DIF" in template:
            logger.notice("template", template, "is not valid")
            continue
        logger.notice("++++++++++++++++++++")
        logger.notice("  ", template)
        logger.notice("++++++++++++++++++++")
        if not os.path.exists(template_dir):
            os.mkdir(template_dir)

        point_names = None
        point_values = {}
        cputimes[template] = []
        for codename in codes:

            sat_dir = os.path.join(template_dir, codename)
            dbhydro = os.path.join(sat_dir, template + ".sqlite")
            if init:
                if os.path.isdir(sat_dir):
                    shutil.rmtree(sat_dir)
                os.makedirs(sat_dir)
                if db.endswith(".mesh.sqlite"):
                    logger.notice("creating hydro database", dbhydro)
                    create_computation_database(dbhydro, mesh_db=db)
                else:
                    logger.notice("copying database to", dbhydro)
                    shutil.copyfile(db, dbhydro)

            if not os.path.isfile(dbhydro):
                exit("database %s must be initialized first"%(dbhydro))

            logger.notice("++++++++++++++++++++")
            logger.notice("  ", codename.upper())
            logger.notice("++++++++++++++++++++")
            if codename == "metis" and which("metis"):
                code = Metis(dbhydro, which("metis"))
                solver = "metis"
                cmd = [solver, ""]
            elif codename == "openfoam" and which("groundwater2DFoam"):
                code = OpenFoam(
                    dbhydro, Settings(os.path.dirname(os.path.abspath(dbhydro)))
                )
                solver = "groundwater2DFoam"
                cmd = [solver, "-steady"]
            else:
                logger.warning("code name is unknown")
                exit(0)

            if init:
                logger.notice("generating files for code", codename)
                code.create_hydrostationary_files(sat_dir, with_permeabilities=True)

            if run and os.path.isdir(sat_dir):
                timer = Timer()
                inputfile = (
                    open(os.path.join(sat_dir, "data.sat"))
                    if codename == "metis"
                    else None
                )
                with open(os.path.join(sat_dir, solver + ".log"), "w") as logfile:
                    logger.notice("running: ", cmd[0], cmd[1], "\n in directory ", sat_dir)
                    Popen(
                        cmd,
                        stdin=inputfile,
                        stdout=logfile,
                        stderr=STDOUT,
                        cwd=sat_dir,
                        universal_newlines=False,
                    ).wait()

                if codename == "openfoam":
                    times_dir_names = OpenFoam.times_dir_names(sat_dir)
                    for name in times_dir_names[:-1]:
                        shutil.rmtree(os.path.join(sat_dir, name))
                    # last time directory renamed to '0'
                    shutil.move(
                        os.path.join(sat_dir, times_dir_names[-1]),
                        os.path.join(sat_dir, "0"),
                    )

                ext = (
                    ".node.npy"
                    if code.variables_sat["potentiel"] == "nodes"
                    else ".elem.npy"
                )
                numpy.save(
                    os.path.join(sat_dir, template + ".potentiel" + ext),
                    code.read_file(os.path.join(sat_dir, "potentiel.sat"), [0]),
                )
                cputime = timer.reset("").strip()
                logger.notice("cpu time is", cputime)
                cputimes[template].append(cputime)

            if resu:
                if not os.path.exists(sat_dir):
                    exit("initializing and running have to be done first")
                with sqlite.connect(dbhydro) as conn:
                    cur = conn.cursor()
                    cur.execute(
                        "INSERT INTO dates_simulation (date) SELECT debut FROM simulations"
                    )
                    points = cur.execute(
                        """SELECT nom, X(GEOMETRY), Y(GEOMETRY)
                        FROM points_interet WHERE groupe='calcul' ORDER BY nom"""
                    ).fetchall()
                    project_SRID = str(
                        cur.execute(
                            "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
                        ).fetchone()[0]
                    )

                provider = SpatialiteMeshDataProvider(
                    "dbname=%s crs=epsg:%s resultColumn=%s"
                    % (dbhydro, project_SRID, "potentiel")
                )
                provider.setUnits("m")
                points = [
                    (name, x, y) for name, x, y in points if provider.valuesAt((x, y))
                ]
                if not point_names:
                    point_names = [name for name, x, y in points]
                point_values[codename] = [
                    provider.valuesAt((x, y))[0] for name, x, y in points
                ]
                print(point_values[codename])

        if resu:
            with open(os.path.join(template_dir, "results.csv"), "w") as fil:
                fil.write("NAME;")
                for codename in codes:
                    fil.write("%s;" % (codename.upper()))
                fil.write("\n")
                for ipt, point_name in enumerate(point_names):
                    fil.write("%s;" % (point_name))
                    for codename in codes:
                        fil.write("%f;" % (point_values[codename][ipt]))
                    fil.write("\n")

        if plot:
            coefs[template] = create_plot_hydro(template_dir)

    if run:
        with open(os.path.join(top_dir, "cputimes.csv"), "w") as fil:
            fil.write("NAME;")
            for codename in codes:
                fil.write("%s;" % (codename.upper()))
            fil.write("\n")
            for template, cpus in cputimes.items():
                fil.write("%s;" % (template))
                for cpu in cpus:
                    fil.write("%s;" % (cpu))
                fil.write("\n")

    if plot:
        with open(os.path.join(top_dir, "coefs.csv"), "w") as fil:
            fil.write("NAME;R;RMSD;\n")
            for template, cs in coefs.items():
                fil.write("%s;" % (template))
                for c in cs:
                    fil.write("%.5f;" % (c))
                fil.write("\n")


def create_plot_hydro(template_dir):
    dpisize = 100
    logger.notice("creating correlation plot at probes")
    template = os.path.basename(template_dir)
    with open(os.path.join(template_dir, "results.csv")) as fil:
        codes = next(fil).split(";")[1:-1]
        a = numpy.array(
            [[float(x) for x in line.split(";")[1:-1]] for line in fil.readlines()]
        )
        x = a[:, 0]
        y = a[:, 1]
        coef_correlation = pearsonr(x, y)[0]
        logger.notice("correlation coefficient is", coef_correlation)
        rmsd = numpy.sqrt(numpy.mean((y - x) * (y - x)))
        logger.notice("root mean square deviation is", rmsd, "m")
        a = a.flatten()
        hmin = numpy.min(a) * 0.99
        hmax = numpy.max(a) * 1.01

    fig, ax = plt.subplots(figsize = (8.25, 8.25))
    ax.set_xlabel("%s potential (m)" % (codes[0].upper()))
    ax.set_ylabel("%s potential (m)" % (codes[1].upper()))
    ax.set_xlim(hmin, hmax)
    ax.set_ylim(hmin, hmax)
    ax.grid()
    ax.plot(x, y, "ro", markersize=6, label="simulations")
    ax.plot([hmin, hmax], [hmin, hmax], "r", label="y = x")
    legend = ax.legend(loc="lower right")

    ax.text(0.05, 0.95, template, ha="left", fontsize=12, transform=ax.transAxes)
    ax.text(0.05, 0.90, "correlation is %.5f"%(coef_correlation), ha="left", transform=ax.transAxes)
    ax.text(0.05, 0.85, "rmsd is %.5f m"%(rmsd), ha="left", transform=ax.transAxes)
    plotname = os.path.join(template_dir, template + ".png")
    fig.savefig(plotname, format='png', dpi=dpisize)
    plt.close(fig)

    return coef_correlation, rmsd


if __name__ == "__main__":
    logger.enable_console(True)
    logger.set_level("notice")

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    init = False
    run = False
    resu = False
    plot = False
    raz = False
    dbname = None
    topdir = "thyrsis_hydro_validation_" + datetime.strftime(date.today(), "%y%m%d")

    mesh_dir = str(Settings().value("General", "defaultMeshDir"))
    mesh_list = [
        os.path.join(mesh_dir, filepath) for filepath in sorted(os.listdir(mesh_dir))
    ]
    mesh_names = [x.split(".")[0] for x in sorted(os.listdir(mesh_dir))]
    logger.notice(mesh_names)

    mesh_raz = False
    db_list = mesh_list

    codes = {c: which(c) for c in ["metis", "openfoam"] if which(c)}
    if which("groundwater2DFoam"):
        codes["openfoam"] = which("groundwater2DFoam")
    codes_all = codes

    for arg in sys.argv[1:]:
        if arg == "-init":
            init = True
        elif arg == "-run":
            run = True
        elif arg == "-raz":
            raz = True
        elif arg == "-results":
            resu = True
        elif arg == "-v":
            logger.set_level("debug")
        elif arg[:5] == "-dir=":
            topdir = arg[5:]
        elif arg == "-plot":
            plot = True
        elif arg == "-a":
            init = True
            run = True
            resu = True
            plot = True
        elif arg[:6] == "-code=":
            codes = {c: codes_all[c] for c in arg[6:].split(",") if c in codes_all}
        elif arg[-7:] == ".sqlite":
            db_list = [os.path.join(os.getcwd(), arg)]
        elif arg[0] == "-" and arg[1:] in mesh_names:
            db_list.remove(arg[1:])
        elif arg in mesh_names:
            if not mesh_raz:
                db_list = []
                mesh_raz = True
            db_list.append(os.path.join(mesh_dir, arg + ".mesh.sqlite"))
        else:
            logger.error("argument not recognized", arg)
            exit(1)

    hydro_validation(topdir, init, run, resu, plot, sorted(codes.keys()), raz, db_list)
