<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>AdjustWidget</name>
    <message>
        <location filename="../../gui/time_plot.py" line="206"/>
        <source>months</source>
        <translation>mois</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot.py" line="206"/>
        <source>hours</source>
        <translation>heures</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot.py" line="206"/>
        <source>minutes</source>
        <translation>minutes</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot.py" line="206"/>
        <source>seconds</source>
        <translation>secondes</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot.py" line="206"/>
        <source>years</source>
        <translation>ans</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot.py" line="206"/>
        <source>days</source>
        <translation>jours</translation>
    </message>
</context>
<context>
    <name>BoreHoleScene</name>
    <message>
        <location filename="../../gui/lithology.py" line="207"/>
        <source>stratigraphic
logs</source>
        <translation>Log
stratigraphique</translation>
    </message>
    <message>
        <location filename="../../gui/lithology.py" line="210"/>
        <source>Fracturing
rate</source>
        <translation>Taux de
Fracturation</translation>
    </message>
    <message>
        <location filename="../../gui/lithology.py" line="203"/>
        <source>Depth [m]</source>
        <translation>Profondeur [m]</translation>
    </message>
</context>
<context>
    <name>ChemicalManagementDlg</name>
    <message>
        <location filename="../../gui/chemical_management.py" line="134"/>
        <source> times in the mesures table of the computation database
</source>
        <translation> fois dans la table mesures de la base de calcul
</translation>
    </message>
    <message>
        <location filename="../../gui/chemical_management.py" line="134"/>
        <source>You will erase a chemical element that is used :
</source>
        <translation>Vous allez supprimer un élément chimique qui est utilisé :
</translation>
    </message>
    <message>
        <location filename="../../gui/chemical_management.py" line="134"/>
        <source> times in the simulations table of the sites database
</source>
        <translation> fois dans la table simulations de la base sites
</translation>
    </message>
    <message>
        <location filename="../../gui/chemical_management.py" line="134"/>
        <source> times in the mesures table of the sites database
</source>
        <translation> fois dans la table mesures de la base sites
</translation>
    </message>
    <message>
        <location filename="../../gui/chemical_management.py" line="134"/>
        <source> times in the simulations table of the computation database
</source>
        <translation> fois dans la table simulations de la base de calcul
</translation>
    </message>
    <message>
        <location filename="../../gui/chemical_management.py" line="134"/>
        <source>Do you want to confirm ?</source>
        <translation>Voulez vous confirmer ?</translation>
    </message>
</context>
<context>
    <name>CrisisWidget</name>
    <message>
        <location filename="../../gui/crisis_dialog.py" line="231"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../../gui/mesh_rasters.ui" line="20"/>
        <source>DEM</source>
        <translation>Terrain</translation>
    </message>
    <message>
        <location filename="../../gui/mesh_rasters.ui" line="30"/>
        <source>Aquifer substratum altitude</source>
        <translation>Altitude du substratum</translation>
    </message>
    <message>
        <location filename="../../gui/mesh_rasters.ui" line="57"/>
        <source>Potential</source>
        <translation>Potentiel</translation>
    </message>
    <message>
        <location filename="../../gui/make_model.ui" line="29"/>
        <source>Create model with inversion (from control points)</source>
        <translation>Création du modèle par inversion (avec points pilotes)</translation>
    </message>
    <message>
        <location filename="../../gui/make_model.ui" line="39"/>
        <source>Create model from a permeability field</source>
        <translation>Création du modèle depuis un champ de perméabilité</translation>
    </message>
    <message>
        <location filename="../../gui/make_model.ui" line="79"/>
        <source>Create model with a constant permeability field value</source>
        <translation>Création du modèle avec une valeur constante de perméabilité</translation>
    </message>
    <message>
        <location filename="../../gui/srid_confirm.ui" line="29"/>
        <source>You will create a database project with the current projection :</source>
        <translation>Vous aller créer un projet avec la projection actuelle :</translation>
    </message>
    <message>
        <location filename="../../gui/make_model.ui" line="14"/>
        <source>Build model</source>
        <translation>Construction du modèle</translation>
    </message>
    <message>
        <location filename="../../gui/mesh_rasters.ui" line="14"/>
        <source>Build mesh</source>
        <translation>Construction de maillage</translation>
    </message>
    <message>
        <location filename="../../gui/srid_confirm.ui" line="14"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../../gui/chemical_management.ui" line="29"/>
        <source>Add chemical</source>
        <translation>Ajout élément chimique</translation>
    </message>
    <message>
        <location filename="../../gui/chemical_management.ui" line="39"/>
        <source>Delete</source>
        <translation>Supprimer</translation>
    </message>
    <message>
        <location filename="../../gui/chemical_management.ui" line="14"/>
        <source>Chemical management</source>
        <translation>Gestion élément chimique</translation>
    </message>
    <message>
        <location filename="../../gui/choose_raster_resolution.ui" line="14"/>
        <source>Raster resolution</source>
        <translation>Résolution du raster</translation>
    </message>
    <message>
        <location filename="../../gui/choose_raster_resolution.ui" line="20"/>
        <source>Please enter raster cell resolution :</source>
        <translation>Veuillez entrer la résolution du raster :</translation>
    </message>
    <message>
        <location filename="../../gui/choose_raster_resolution.ui" line="42"/>
        <source>pixel per meter</source>
        <translation>pixel par mètre</translation>
    </message>
</context>
<context>
    <name>DurationEditWidget</name>
    <message>
        <location filename="../../gui/duration_edit_widget.py" line="33"/>
        <source>year</source>
        <translation>an</translation>
    </message>
    <message>
        <location filename="../../gui/duration_edit_widget.py" line="35"/>
        <source>month</source>
        <translation>mois</translation>
    </message>
    <message>
        <location filename="../../gui/duration_edit_widget.py" line="37"/>
        <source>day</source>
        <translation>jour</translation>
    </message>
    <message>
        <location filename="../../gui/duration_edit_widget.py" line="39"/>
        <source>hour</source>
        <translation>heure</translation>
    </message>
    <message>
        <location filename="../../gui/duration_edit_widget.py" line="41"/>
        <source>minute</source>
        <translation>minute</translation>
    </message>
    <message>
        <location filename="../../gui/duration_edit_widget.py" line="43"/>
        <source>second</source>
        <translation>seconde</translation>
    </message>
    <message>
        <location filename="../../gui/duration_edit_widget.py" line="27"/>
        <source>Invalid duration format : {}</source>
        <translation>Format de durée invalide : {}</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../../viewer_3d/viewer_3d_controls.ui" line="14"/>
        <source>Form</source>
        <translation>Formulaire</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="258"/>
        <source>Begin</source>
        <translation>Début</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="166"/>
        <source>Flux</source>
        <translation>Flux</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="179"/>
        <source>Concentration</source>
        <translation>Concentration</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="192"/>
        <source>Mass</source>
        <translation>Masse</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="423"/>
        <source>Transient flow</source>
        <translation>Écoulement transitoire</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="430"/>
        <source>Unsaturated zone</source>
        <translation>Zone non saturée</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="69"/>
        <source>Retention
coefficient</source>
        <translation>Coefficient
de rétention</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="140"/>
        <source>Solubility
limit</source>
        <translation>Limite de
solubilité</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="44"/>
        <source>Duration</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="58"/>
        <source>Step</source>
        <translation>Pas</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="207"/>
        <source>Injection</source>
        <translation>Injection</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="37"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../../gui/plot_adjust.ui" line="125"/>
        <source>+0</source>
        <translation>+0</translation>
    </message>
    <message>
        <location filename="../../gui/plot_adjust.ui" line="89"/>
        <source>minutes</source>
        <translation>minutes</translation>
    </message>
    <message>
        <location filename="../../gui/plot_adjust.ui" line="69"/>
        <source>years</source>
        <translation>ans</translation>
    </message>
    <message>
        <location filename="../../gui/plot_adjust.ui" line="74"/>
        <source>months</source>
        <translation>mois</translation>
    </message>
    <message>
        <location filename="../../gui/plot_adjust.ui" line="79"/>
        <source>days</source>
        <translation>jours</translation>
    </message>
    <message>
        <location filename="../../gui/plot_adjust.ui" line="84"/>
        <source>hours</source>
        <translation>heures</translation>
    </message>
    <message>
        <location filename="../../gui/plot_adjust.ui" line="94"/>
        <source>seconds</source>
        <translation>secondes</translation>
    </message>
    <message>
        <location filename="../../gui/plot_adjust.ui" line="132"/>
        <source>units</source>
        <translation>units</translation>
    </message>
    <message>
        <location filename="../../gui/plot_adjust.ui" line="175"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../../gui/step.ui" line="53"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../../viewer_3d/viewer_3d_controls.ui" line="68"/>
        <source>Texture</source>
        <translation>Texture</translation>
    </message>
    <message>
        <location filename="../../viewer_3d/viewer_3d_controls.ui" line="75"/>
        <source>Contours</source>
        <translation>Contours</translation>
    </message>
    <message>
        <location filename="../../viewer_3d/viewer_3d_controls.ui" line="123"/>
        <source>Boreholes</source>
        <translation>Forages</translation>
    </message>
    <message>
        <location filename="../../viewer_3d/viewer_3d_controls.ui" line="133"/>
        <source>Calculation points</source>
        <translation>Points de calcul</translation>
    </message>
    <message>
        <location filename="../../gui/time_control.ui" line="61"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;first date&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;première date&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
    <message>
        <location filename="../../gui/time_control.ui" line="75"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;previous date&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;date précédente&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
    <message>
        <location filename="../../gui/time_control.ui" line="95"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;play&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;lancer&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
    <message>
        <location filename="../../gui/time_control.ui" line="122"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;next date&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;date suivante&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
    <message>
        <location filename="../../gui/time_control.ui" line="136"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;last date&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&amp;lt;html&amp;gt;&amp;lt;head/&amp;gt;&amp;lt;body&amp;gt;&amp;lt;p&amp;gt;dernière date&amp;lt;/p&amp;gt;&amp;lt;/body&amp;gt;&amp;lt;/html&amp;gt;</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="87"/>
        <source>Chemical
element</source>
        <translation>Elément
chimique</translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="110"/>
        <source>m³/kg</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/crisis.ui" line="131"/>
        <source>kg/m³</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>HypercubeDialog</name>
    <message>
        <location filename="../../gui/create_latin_hypercube.py" line="97"/>
        <source>Max value</source>
        <translation>Valeur max</translation>
    </message>
    <message>
        <location filename="../../gui/create_latin_hypercube.py" line="97"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../../gui/create_latin_hypercube.py" line="97"/>
        <source>Min value</source>
        <translation>Valeur min</translation>
    </message>
    <message>
        <location filename="../../gui/create_latin_hypercube.py" line="97"/>
        <source>Variation law</source>
        <translation>Loi de variation</translation>
    </message>
</context>
<context>
    <name>HypercubeLatin</name>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="14"/>
        <source>Hypercube Latin</source>
        <translation>Hypercube Latin</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="20"/>
        <source>Number of simulations</source>
        <translation>Nombre de simulations</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="54"/>
        <source>WC</source>
        <translation>WC</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="59"/>
        <source>VGA</source>
        <translation>VGA</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="64"/>
        <source>VGN</source>
        <translation>VGN</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="69"/>
        <source>VGR</source>
        <translation>VGR</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="74"/>
        <source>VGS</source>
        <translation>VGS</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="79"/>
        <source>VGE</source>
        <translation>VGE</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="84"/>
        <source>WT</source>
        <translation>WT</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="89"/>
        <source>VM</source>
        <translation>VM</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="94"/>
        <source>DK</source>
        <translation>DK</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="99"/>
        <source>SL</source>
        <translation>SL</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="104"/>
        <source>DLZNS</source>
        <translation>DLZNS</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="109"/>
        <source>DLZS</source>
        <translation>DLZS</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="119"/>
        <source>Value</source>
        <translation>Valeur</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="124"/>
        <source>Min value</source>
        <translation>Valeur min</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="129"/>
        <source>Max value</source>
        <translation>Valeur max</translation>
    </message>
    <message>
        <location filename="../../gui/latin_hypercube.ui" line="134"/>
        <source>Variation law</source>
        <translation>Loi de variation</translation>
    </message>
</context>
<context>
    <name>InjectionWidget</name>
    <message>
        <location filename="../../gui/injection_widget.py" line="117"/>
        <source>year</source>
        <translation>an</translation>
    </message>
    <message>
        <location filename="../../gui/injection_widget.py" line="313"/>
        <source>{} must be a positive value.</source>
        <translation>{} doit être une valeur positive.</translation>
    </message>
    <message>
        <location filename="../../gui/injection_widget.py" line="322"/>
        <source>water volume must be a positive value.</source>
        <translation>volume d'eau doit être une valeur positive.</translation>
    </message>
    <message>
        <location filename="../../gui/injection_widget.py" line="329"/>
        <source>depth must be a positive value.</source>
        <translation>la profondeur doit être une valeur positive.</translation>
    </message>
</context>
<context>
    <name>Isovalues</name>
    <message>
        <location filename="../../gui/isovalue.ui" line="14"/>
        <source>Isovalues</source>
        <translation>Isovaleurs</translation>
    </message>
    <message>
        <location filename="../../gui/isovalue.ui" line="81"/>
        <source>refresh</source>
        <translation>actualiser</translation>
    </message>
    <message>
        <location filename="../../gui/isovalue.ui" line="58"/>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <location filename="../../gui/isovalue.ui" line="113"/>
        <source>Logarithmic scale</source>
        <translation>Échelle log</translation>
    </message>
    <message>
        <location filename="../../gui/isovalue.ui" line="139"/>
        <source>+</source>
        <translation>+</translation>
    </message>
    <message>
        <location filename="../../gui/isovalue.ui" line="152"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="../../gui/isovalue.ui" line="171"/>
        <source>Minimal value</source>
        <translation>Valeur minimale</translation>
    </message>
    <message>
        <location filename="../../gui/isovalue.ui" line="181"/>
        <source>Maximal value</source>
        <translation>Valeur maximale</translation>
    </message>
    <message>
        <location filename="../../gui/isovalue.ui" line="191"/>
        <source>Number of values</source>
        <translation>Nombre de valeurs</translation>
    </message>
    <message>
        <location filename="../../gui/isovalue.ui" line="208"/>
        <source>Pixels/m</source>
        <translation>Nombre de pixels/m</translation>
    </message>
    <message>
        <location filename="../../gui/probability.ui" line="14"/>
        <source>Probability threshold</source>
        <translation>Seuil de probabilité</translation>
    </message>
    <message>
        <location filename="../../gui/probability.ui" line="45"/>
        <source>Threshold :</source>
        <translation>Seuil :</translation>
    </message>
</context>
<context>
    <name>LoadChemicalsAlgorithm</name>
    <message>
        <location filename="../../processing/load_chemicals.py" line="46"/>
        <source>Import a .csv for chemicals load in THRYSIS</source>
        <translation>Importer un CSV de produits chimiques à charger dans Thyrsis</translation>
    </message>
    <message>
        <location filename="../../processing/load_chemicals.py" line="49"/>
        <source>Input .csv</source>
        <translation>Fichier CSV en entrée</translation>
    </message>
    <message>
        <location filename="../../processing/load_chemicals.py" line="62"/>
        <source>Input .sqlite</source>
        <translation>Base SQLite en entrée</translation>
    </message>
    <message>
        <location filename="../../processing/load_chemicals.py" line="37"/>
        <source>Load chemicals</source>
        <translation>Charger les produits chimiques</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../gui/zns.ui" line="14"/>
        <source>MainWindow</source>
        <translation>Fenêtre principale</translation>
    </message>
    <message>
        <location filename="../../gui/zns.ui" line="41"/>
        <source>caption</source>
        <translation>légende</translation>
    </message>
    <message>
        <location filename="../../gui/zns.ui" line="49"/>
        <source>Caption</source>
        <translation>Légende</translation>
    </message>
    <message>
        <location filename="../../gui/zns.ui" line="57"/>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <location filename="../../gui/zns.ui" line="65"/>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <location filename="../../gui/zns.ui" line="73"/>
        <source>Below</source>
        <translation>En dessous</translation>
    </message>
    <message>
        <location filename="../../gui/zns.ui" line="87"/>
        <source>Automatic scale update</source>
        <translation>Mise à jour automatique de l'échelle</translation>
    </message>
</context>
<context>
    <name>PreferenceDialog</name>
    <message>
        <location filename="../../gui/preferences_dialog.py" line="425"/>
        <source>Experimental database import</source>
        <translation>Import de la base expérimentale</translation>
    </message>
    <message>
        <location filename="../../gui/preferences_dialog.py" line="425"/>
        <source>This action will erase content of the following table :
                          - points_interet
                          - mesures
                          - forages
                          - stratigraphies
                          - lithologies
                          - fracturations
                    Do you really want to do it ?</source>
        <translation>Cette action va supprimer le contenu des tables suivantes :
                          - points_interet
                          - mesures
                          - forages
                          - stratigraphies
                          - lithologies
                          - fracturations
                    Confirmez vous la suppression ?</translation>
    </message>
</context>
<context>
    <name>PreferencesDialog</name>
    <message>
        <location filename="../../gui/preferences.ui" line="92"/>
        <source>Installation</source>
        <translation>Installation</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="352"/>
        <source>Variables</source>
        <translation>Variables</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="989"/>
        <source>Animation</source>
        <translation>Animation</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="1041"/>
        <source>Matplotlib</source>
        <translation>Matplotlib</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="32"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="279"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="77"/>
        <source>Database</source>
        <translation>Base de données</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="25"/>
        <source>Cancel</source>
        <translation>Annuler</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="165"/>
        <source>Background maps</source>
        <translation>Fonds de carte</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="207"/>
        <source>MPI configuration </source>
        <translation>Configuration MPI </translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="304"/>
        <source>Number of MPI  processes</source>
        <translation>Nombre de processus MPI</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="114"/>
        <source>Maximal number of columns for display</source>
        <translation>Nombre maximal de colonnes pour l'affichage</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="147"/>
        <source>Log in console</source>
        <translation>Impression dans la console</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="154"/>
        <source>Detailed log</source>
        <translation>Impression détaillée</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="100"/>
        <source>Hydrogeologic software</source>
        <translation>Code hydrogéologique</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="369"/>
        <source>Mass balance</source>
        <translation>Bilan de masse</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="430"/>
        <source>Hide units</source>
        <translation>Cacher les unités</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="468"/>
        <source>Unit</source>
        <translation>Unité</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="481"/>
        <source>Scale</source>
        <translation>Echelle</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="501"/>
        <source>Potential</source>
        <translation>Potentiel</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="558"/>
        <source>Darcy&apos;s velocity</source>
        <translation>Vitesse Darcy</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="769"/>
        <source>Activity</source>
        <translation>Activité</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="1008"/>
        <source>Images per second</source>
        <translation>Images par seconde</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="1155"/>
        <source>Connection channel</source>
        <translation>Service de connexion</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="455"/>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="14"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="494"/>
        <source>Classes</source>
        <translation>Classes</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="531"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="755"/>
        <source>1e-3</source>
        <translation>1e-3</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="618"/>
        <source>Concentration</source>
        <translation>Concentration</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="880"/>
        <source>Saturation</source>
        <translation>Saturation</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="955"/>
        <source>ZNS</source>
        <translation>ZNS</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="971"/>
        <source>ZS</source>
        <translation>ZS</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="1047"/>
        <source>Ylabel Font Size</source>
        <translation>Ylabel Font Size</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="1067"/>
        <source>Ticks Label Size</source>
        <translation>Ticks Label Size</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="1100"/>
        <source>Legend Font Size</source>
        <translation>Legend Font Size</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="1162"/>
        <source>service=bdlhes</source>
        <translation>service=bdlhes</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="193"/>
        <source>MPI protocol</source>
        <translation>Protocole MPI</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="1140"/>
        <source>Use the experimental database</source>
        <translation>Utiliser une base de données expérimentale</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="1169"/>
        <source>Synchronize experimental database with sites database</source>
        <translation>Synchroniser avec la base sites</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="290"/>
        <source>Mesh Generator</source>
        <translation>Outil de génération du maillage</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="238"/>
        <source>PMF install folder:</source>
        <translation>Dossier d'installation de PMF :</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="252"/>
        <source>GMSH:</source>
        <translation>GMSH :</translation>
    </message>
    <message>
        <location filename="../../gui/preferences.ui" line="272"/>
        <source>OpenFoam project folder:</source>
        <translation>Dossier du projet OpenFOAM :</translation>
    </message>
</context>
<context>
    <name>SridConfirmDialog</name>
    <message>
        <location filename="../../gui/srid_confirm.py" line="16"/>
        <source>You will create a computational database.
It will have the same projection as your mesh database.</source>
        <translation>Vous allez créer une base de donnée de calcul.
Celle-ci aura la même projection que la base de données maillage.</translation>
    </message>
</context>
<context>
    <name>ThyrsisPlugin</name>
    <message>
        <location filename="../../plugin.py" line="318"/>
        <source>Error importing dependencies. Plugin disabled.</source>
        <translation>Une erreur est survenue lors de l'import des dépendances. Le plugin a été désactivé.</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="337"/>
        <source>Plugin disabled. Please install all dependencies and then restart QGIS. Refer to the documentation for more information.</source>
        <translation>Plugin désactivé. Merci d'installer toutes les dépendances requises puis de relancer QGIS. Voir la documentation pour plus de détails.</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="346"/>
        <source>Dependencies satisfied</source>
        <translation>Toutes les dépendances sont satisfaites</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="615"/>
        <source>Export</source>
        <translation>Exporter</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="569"/>
        <source>Simulate</source>
        <translation>Simuler</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="584"/>
        <source>Create</source>
        <translation>Créer</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="760"/>
        <source>Help</source>
        <translation>Documentation</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="769"/>
        <source>time_control_toolbar</source>
        <translation>barre de contrôle du temps</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="772"/>
        <source>run_simulation</source>
        <translation>lancer simulation</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="787"/>
        <source>result_control_toolbar</source>
        <translation>barre de controle des résultats</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="794"/>
        <source>display value</source>
        <translation>afficher valeur</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="800"/>
        <source>plot</source>
        <translation>graphique</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="806"/>
        <source>plot mass balance</source>
        <translation>graphique de bilan de masse</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1745"/>
        <source>ZNS mass</source>
        <translation>Masse ZNS</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1746"/>
        <source>ZS mass</source>
        <translation>Masse ZS</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1747"/>
        <source>Output mass</source>
        <translation>Masse sortie</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1748"/>
        <source>Total mass</source>
        <translation>Masse totale</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1749"/>
        <source>mass </source>
        <translation>masse </translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1754"/>
        <source>Balance</source>
        <translation>Bilan</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="283"/>
        <source>Tasks manager</source>
        <translation>Gestionnaire de tâches</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="448"/>
        <source>The project SRID (EPSG:</source>
        <translation>La projection du projet (EPSG:</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="448"/>
        <source>) doesn&apos;t fit with the data (EPSG:</source>
        <translation>) ne correspond pas à celle des données (EPSG:</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="448"/>
        <source>)
Do you want to reproject the project ?</source>
        <translation>)
Voulez-vous reprojeter le projet ?</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="485"/>
        <source>Borehole</source>
        <translation>Forage</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="512"/>
        <source>mesh</source>
        <translation>maillage</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="517"/>
        <source>model</source>
        <translation>modèle</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="573"/>
        <source>Initialize</source>
        <translation>Initialiser</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="576"/>
        <source>Compute without initialization</source>
        <translation>Calculer sans initialiser</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="579"/>
        <source>Initialize and compute</source>
        <translation>Initialiser et calculer</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="591"/>
        <source>Injection zones</source>
        <translation>Zones d'injection</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="594"/>
        <source>Latin hypercube</source>
        <translation>Hypercube latin</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="597"/>
        <source>Probability map</source>
        <translation>Carte de probabilités</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="600"/>
        <source>Isovalues</source>
        <translation>Isovaleurs</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="603"/>
        <source>Isovalues (gdal)</source>
        <translation>Isovaleurs (gdal)</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="606"/>
        <source>Evolution graph</source>
        <translation>Courbes d'évolution</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="607"/>
        <source>Second medium</source>
        <translation>Second milieu</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="610"/>
        <source>Chemical element</source>
        <translation>Elément chimique</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="682"/>
        <source>borehole info</source>
        <translation>info forage</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="689"/>
        <source>correlation errors</source>
        <translation>erreurs de corrélation</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="695"/>
        <source>invert</source>
        <translation>inverser</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="739"/>
        <source>New</source>
        <translation>Nouveau</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="747"/>
        <source>Open</source>
        <translation>Ouvrir</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="753"/>
        <source>Preferences</source>
        <translation>Préférences</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="782"/>
        <source>Adjust scale</source>
        <translation>Ajuster l'échelle</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2499"/>
        <source>Warning</source>
        <translation>Attention</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="997"/>
        <source>No pilot point found in points_pilote layer, inversion aborted</source>
        <translation>Aucun point pilote trouvé dans la couche points_pilotes, inversion interrompue</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1015"/>
        <source>No parameters found in hynverse_parametres layer, inversion aborted</source>
        <translation>Aucun paramètre trouvé dans la couche hynverse_parametres, inversion interrompue</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1048"/>
        <source>Infiltration parameters in hynverse_parametres layer has 0 value but no infiltration user file is provided , inversion aborted</source>
        <translation>La valeur du paramètre d'infiltration est égale à 0 mais aucun fichier utilisateur d'infiltration n'est fourni, inversion interrompue</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1196"/>
        <source>Inversion</source>
        <translation>Inversion</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1223"/>
        <source>This action removes existing injections</source>
        <translation>Cette action supprime les injections existantes</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1374"/>
        <source>Export raster</source>
        <translation>Exporter un raster</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1374"/>
        <source>PNG file (*.png);; Geotiff file (*.tif)</source>
        <translation>Fichier PNG (*.png);; Fichier Geotiff (*.tif)</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2308"/>
        <source>Error</source>
        <translation>Erreur</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1438"/>
        <source>The raster export in tif format is only available for thyrsis mesh layer</source>
        <translation>L'export de raster au format tif est seulement disponible pour les couches maillages de Thyrsis</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1517"/>
        <source>Video file</source>
        <translation>Fichier vidéo</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1517"/>
        <source>video file (*.avi, *.mp4, *.mpeg)</source>
        <translation>Fichier vidéo (*.avi, *.mp4, *.mpeg)</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1530"/>
        <source>The video export file must have a recognized extension (.avi, .mpeg ou .mp4)</source>
        <translation>Le fichier d'export vidéo doit avoir une extension reconnue (.avi, .mpeg ou .mp4)</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1577"/>
        <source>Image file</source>
        <translation>Fichier image</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1577"/>
        <source>image file (*.png *.jpg)</source>
        <translation>Fichier image (*.png *.jpg)</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1590"/>
        <source>The animation export file must have a recognized extension (.jpg or .png)</source>
        <translation>Le fichier d'export d'animation doit avoir une extension reconnue (.jgg ou .png)</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1719"/>
        <source>All simulation parameters are equal to 0.0 , please fulfill &apos;parametres&apos; table</source>
        <translation>Tous les paramètres de simulation sont égales à 0.0, veuillez remplir la table 'parametres'</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1755"/>
        <source>No mass balance for this study</source>
        <translation>Pas de bilan de masse pour cette étude</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1825"/>
        <source>unnamed %d</source>
        <translation>sans nom %d</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1936"/>
        <source>Open a computational database</source>
        <translation>Ouvrir une base de données de calcul</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1936"/>
        <source>project file (*.sqlite)</source>
        <translation>fichier projet (*.sqlite)</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1949"/>
        <source>File {} does not exist</source>
        <translation>Le fichier {} n'existe pas</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="1957"/>
        <source>The .qgs project file exists, to you want to overwrite it ?</source>
        <translation>Le fichier .qgs existe, voulez-vous le supprimer ?</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2016"/>
        <source>Models:</source>
        <translation>Modèles :</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2032"/>
        <source>Studies:</source>
        <translation>Études :</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2050"/>
        <source>Mesh...</source>
        <translation>Maillage...</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2058"/>
        <source>New mesh database</source>
        <translation>Nouvelle base de données maillage</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2549"/>
        <source>New computational database</source>
        <translation>Nouvelle base de données calcul</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2246"/>
        <source>3D parameters</source>
        <translation>Paramètres 3D</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2308"/>
        <source>You must choose a valid layer first</source>
        <translation>Vous devez d'abord choisir une couche valide</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2445"/>
        <source>No valid result layer</source>
        <translation>Pas de couche de résultat valide</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2461"/>
        <source>simulation code not implemented</source>
        <translation>Le code de simulation n'est pas implémenté</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2483"/>
        <source>Cannot compute probability map for the layer </source>
        <translation>Impossible de calculer une carte de probabilité pour la couche </translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2499"/>
        <source>Several sets of parameters are needed to obtain a probability map</source>
        <translation>Plusieurs jeux de paramètres sont nécessaires pour obtenir une carte de probabilités</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2522"/>
        <source>directory {} doesn&apos;t exist</source>
        <translation>le répertoire {} n'existe pas</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2546"/>
        <source>project already has a second medium</source>
        <translation>le projet a déjà un second milieu</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2058"/>
        <source>SpatiaLite file (*.mesh.sqlite)</source>
        <translation>Fichier SpatiaLite Mesh (*.mesh.sqlite)</translation>
    </message>
    <message>
        <location filename="../../plugin.py" line="2549"/>
        <source>SpatiaLite file (*.sqlite)</source>
        <translation>Fichier SpatiaLite (*.sqlite)</translation>
    </message>
</context>
<context>
    <name>TimePlotWidget</name>
    <message>
        <location filename="../../gui/time_plot_widget.py" line="34"/>
        <source>Style</source>
        <translation>Style</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot_widget.py" line="36"/>
        <source>vertical fit</source>
        <translation>tout ajuster verticalement</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot_widget.py" line="39"/>
        <source>horizontal fit</source>
        <translation>tout ajuster horizontalement</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot_widget.py" line="42"/>
        <source>RAZ</source>
        <translation>RAZ</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot_widget.py" line="79"/>
        <source>File basename (with extension)</source>
        <translation>Nom de base fichier (avec extension)</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot_widget.py" line="79"/>
        <source>Image files (*.png, *.jpg)</source>
        <translation>Fichiers image (*.png, *.jpg)</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot_widget.py" line="31"/>
        <source>Fit</source>
        <translation>Ajuster</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot_widget.py" line="35"/>
        <source>fit all</source>
        <translation>Tout ajuster</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot_widget.py" line="44"/>
        <source>Save</source>
        <translation>Enregistrer</translation>
    </message>
    <message>
        <location filename="../../gui/time_plot_widget.py" line="45"/>
        <source>Close</source>
        <translation>Fermer</translation>
    </message>
</context>
<context>
    <name>ZnsWindow</name>
    <message>
        <location filename="../../gui/zns_window.py" line="52"/>
        <source>Options</source>
        <translation>Options</translation>
    </message>
    <message>
        <location filename="../../gui/zns_window.py" line="56"/>
        <source>Automatic scale update</source>
        <translation>Mise à jour automatique de l'échelle</translation>
    </message>
    <message>
        <location filename="../../gui/zns_window.py" line="148"/>
        <source>Export file must have a recognized extension</source>
        <translation>Le fichier d'export doit avoir une extension reconnue</translation>
    </message>
    <message>
        <location filename="../../gui/zns_window.py" line="60"/>
        <source>Legend</source>
        <translation>Légende</translation>
    </message>
    <message>
        <location filename="../../gui/zns_window.py" line="61"/>
        <source>Legend location</source>
        <translation>Position légende</translation>
    </message>
    <message>
        <location filename="../../gui/zns_window.py" line="62"/>
        <source>Left</source>
        <translation>Gauche</translation>
    </message>
    <message>
        <location filename="../../gui/zns_window.py" line="63"/>
        <source>Right</source>
        <translation>Droite</translation>
    </message>
    <message>
        <location filename="../../gui/zns_window.py" line="64"/>
        <source>Below</source>
        <translation>En dessous</translation>
    </message>
    <message>
        <location filename="../../gui/zns_window.py" line="117"/>
        <source>Save the plot ...</source>
        <translation>Enregistrer le graphique ...</translation>
    </message>
    <message>
        <location filename="../../gui/zns_window.py" line="117"/>
        <source>Image files (*.png *.svg *.bmp *.jpg *.pdf)</source>
        <translation>Fichiers images (*.png *.svg *.bmp *.jpg *.pdf)</translation>
    </message>
</context>
<context>
    <name>fluxWidget</name>
    <message>
        <location filename="../../gui/crisis_flux.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_flux.ui" line="20"/>
        <source>sans nom</source>
        <translation>sans nom</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_flux.ui" line="69"/>
        <source>x</source>
        <translation>xx</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_concentration.ui" line="33"/>
        <source>kg/m3</source>
        <translation>kg/m³</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_concentration.ui" line="40"/>
        <source>Concentration</source>
        <translation>Concentration</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_flux.ui" line="177"/>
        <source>Depth</source>
        <translation>Profondeur</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_flux.ui" line="47"/>
        <source>Duration</source>
        <translation>Durée</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_flux.ui" line="27"/>
        <source>Begin</source>
        <translation>Début</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_flux.ui" line="97"/>
        <source>Location</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_flux.ui" line="138"/>
        <source>Area</source>
        <translation>Surface</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_flux.ui" line="217"/>
        <source>Water volume</source>
        <translation>Volume eau</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_concentration.ui" line="112"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_flux.ui" line="83"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_concentration.ui" line="228"/>
        <source>m3</source>
        <translation>m³</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_flux.ui" line="76"/>
        <source>Flux</source>
        <translation>Flux</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_flux.ui" line="40"/>
        <source>dd/MM/yyyy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/crisis_flux.ui" line="170"/>
        <source>◎</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../gui/crisis_concentration.ui" line="167"/>
        <source>m²</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>massWidget</name>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="14"/>
        <source>Form</source>
        <translation>Form</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="74"/>
        <source>x</source>
        <translation>x</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="81"/>
        <source>Mass</source>
        <translation>Masse</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="88"/>
        <source>Begin</source>
        <translation>Début</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="116"/>
        <source>Location</source>
        <translation>Position</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="123"/>
        <source>Depth</source>
        <translation>Profondeur</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="144"/>
        <source>Area</source>
        <translation>Surface</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="95"/>
        <source>sans nom</source>
        <translation>sans nom</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="109"/>
        <source>kg</source>
        <translation>kg</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="183"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="190"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="29"/>
        <source>dd/MM/yyyy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="102"/>
        <source>m&#xc2;&#xb2;</source>
        <translation type="unfinished">m²</translation>
    </message>
    <message>
        <location filename="../../gui/crisis_mass.ui" line="42"/>
        <source>◎</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>thyrsisDockWidget</name>
    <message>
        <location filename="../../gui/main_panel.ui" line="20"/>
        <source>Thyrsis</source>
        <translation>Thyrsis</translation>
    </message>
    <message>
        <location filename="../../gui/main_panel.ui" line="35"/>
        <source>&amp;Project</source>
        <translation>&amp;Projet</translation>
    </message>
    <message>
        <location filename="../../gui/main_panel.ui" line="50"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="../../gui/main_panel.ui" line="110"/>
        <source>Concentration</source>
        <translation>Concentration</translation>
    </message>
    <message>
        <location filename="../../gui/main_panel.ui" line="115"/>
        <source>Potential</source>
        <translation>Potentiel</translation>
    </message>
    <message>
        <location filename="../../gui/main_panel.ui" line="120"/>
        <source>Darcy&apos;s velocity</source>
        <translation>Vitesse Darcy</translation>
    </message>
    <message>
        <location filename="../../gui/main_panel.ui" line="337"/>
        <source>Automatic scale
update</source>
        <translation>Mise à jour automatique de l'échelle</translation>
    </message>
    <message>
        <location filename="../../gui/main_panel.ui" line="273"/>
        <source>m</source>
        <translation>m</translation>
    </message>
    <message>
        <location filename="../../gui/main_panel.ui" line="325"/>
        <source>◎</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
