# Windows

:::{note}
The plugin has been tested against Windows 10 and QGIS 3.22.
:::

## Requirements

- {{ qgis_version_min }} =< QGIS =< {{ qgis_version_max }}

### Simulation softwares

#### OpenFOAM and porousMultiphaseFoam

1. Install OpenFOAM:
    - pick the desired version from the [tree folders](https://dl.openfoam.com/source/)
    - [direct link for version 2406](https://dl.openfoam.com/source/v2406/OpenFOAM-v2406-windows-mingw.exe)
2. Install porousMultiphaseFoam (PMF) in the same version as OpenFOAM (2406):
    - from [the official repository](https://github.com/phorgue/porousMultiphaseFoam)
    - [direct link for version v2406.2](https://github.com/phorgue/porousMultiphaseFoam/releases/download/v2406.2/pmf-opensuse-mingw-v2406.zip)
:::{note}
Pre-compiled version of OpenFOAM needs [MSMPI](https://github.com/microsoft/Microsoft-MPI/releases/download/v10.1.1/msmpisetup.exe) installation. Administrator privileges are needed.
:::

:::{important}
For CFMESH use on Windows, minimum version of OpenFoam is v2112. There is an issue with cfMesh use on previous pre-compiled version
:::

#### Metis

> TO DOC

### Others

- Install Gmsh (version 4.9.*): get it from [the official website](https://gmsh.info/).

### Python packages

- Python 3.9 (strictly)

The plugin's logic is mainly based on third-party Python packages. It's quite a challenge to install it on QGIS for Windows, because QGIS uses its own Python interpreter and doesn't make it easy to use packages manager (`pip`).

To make it easier for Windows end-users, we did our best to embed the dependencies within the released version of the plugin (during CI), inside the `embedded_external_libs` folder.

Technically, the plugin tries to:

1. import packages from the Python interpreter used by QGIS (system on Linux, specific on Windows)
1. if it does not succeed, then it adds the `embedded_external_libs` subfolder to the `PYTHONPATH` and then import packages from it
1. it it still fails, then the plugin is disabled and the user is warned with a button leading him to here:

![Dependencies missing](/_static/images/dependencies_missing_warning.png)

**BUT** there are some caveats because those packages require to be compiled with the same exact Python version than the one used by QGIS and sometimes the QGIS packagers change the Python version...

#### Manual installation

Then, if the plugin fails to import a package, you can try to install it manually using the `pip` command.

> Mainly tested on Windows 10.0.19044.

1. Launch the `OSGeo4W Shell`. Look for it in your Windows Search or directly into your QGIS install folder:
    - installed with the _all inclusive_ standalone `.msi`: `C:\Program Files\QGIS`
    - installed with the OSGeo4W with default settings: `C:\OSGeo4W`
1. Run (example with QGIS LTR installed):

  ```batch
  python-qgis-ltr -m pip install -U pip
  python-qgis-ltr -m pip install -U setuptools wheel
  python-qgis-ltr -m pip install -U "mpi4py==3.1.3" "rtree==0.9.7" "portalocker==2.3.2"
  ```

:::{important}
**Remember** to look at the [`requirements/embedded.txt`](https://gitlab.com/Oslandia/qgis/thyrsis/-/blob/master/requirements/embedded.txt) file to make sure you are not missing any package and using the correct versions. The above command is just an example, a pattern.
:::
