from qgis.core import QgsApplication

from .meshlayer.meshdataproviderregistry import MeshDataProviderRegistry
from .meshlayer.meshlayer import MeshLayer
from .spatialitemeshdataprovider import SpatialiteMeshDataProvider

if __name__ == "__main__":
    import sys

    app = QgsApplication(sys.argv, True)
    QgsApplication.initQgis()

    MeshDataProviderRegistry.instance().addDataProviderType(
        SpatialiteMeshDataProvider.PROVIDER_KEY, SpatialiteMeshDataProvider
    )

    m = MeshLayer(
        "dbname='/home/vmo/cea_dam/test/PEM_2008_U/PEM_2008_U.sqlite' crs='epsg:27572' resultColumn='concentration' units='mg/L' table=\"\" sql=",
        "my_layer",
        SpatialiteMeshDataProvider.PROVIDER_KEY,
    )

    m.isovalues(
        [
            0.532304344233,
            0.247073790028,
            0.114681494487,
            0.0532304344233,
            0.0247073790028,
            0.0114681494487,
            0.00532304344233,
            0.00247073790028,
            0.00114681494487,
            0.000532304344233,
        ]
    )
