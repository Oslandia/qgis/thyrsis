from PyQt5.QtCore import Qt
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import (
    QComboBox,
    QDialog,
    QDialogButtonBox,
    QHBoxLayout,
    QLabel,
    QSpinBox,
    QVBoxLayout,
)

# from qgis.core import *
from qgis.gui import QgsColorButton


class TimePlotControl(QDialog):
    """Control toolbar of the 1D panel"""

    def __init__(self, parent=None):
        """Constructor

        :param parent: Qt parent
        :type parent: QtWidget
        """
        super(TimePlotControl, self).__init__(parent)

        self.resize(400, 300)
        self.layout = QVBoxLayout(self)

        layout = QHBoxLayout()
        self.label = QLabel("Epaisseur", self)
        layout.addWidget(self.label)
        self.width = QSpinBox(self)
        self.width.setValue(3)
        layout.addWidget(self.width)
        self.layout.addLayout(layout)

        layout = QHBoxLayout()
        self.label_2 = QLabel("Couleur", self)
        layout.addWidget(self.label_2)
        self.color = QgsColorButton(self)
        self.color.setColor(QColor("#000000"))
        layout.addWidget(self.color)
        self.layout.addLayout(layout)

        layout = QHBoxLayout()
        self.label_3 = QLabel("Style", self)
        layout.addWidget(self.label_3)
        self.style = QComboBox(self)
        self.style.addItem("solid")
        self.style.addItem("dashed")
        self.style.addItem("dashdot")
        self.style.addItem("dotted")
        self.style.addItem("None")
        layout.addWidget(self.style)
        self.layout.addLayout(layout)

        layout = QHBoxLayout()
        self.label_4 = QLabel("Marker", self)
        layout.addWidget(self.label_4)
        self.marker = QComboBox(self)
        self.marker.addItem("None")
        self.marker.addItem(".")
        self.marker.addItem(",")
        self.marker.addItem("o")
        self.marker.addItem("v")
        self.marker.addItem("^")
        self.marker.addItem("<")
        self.marker.addItem(">")
        self.marker.addItem("s")
        self.marker.addItem("p")
        self.marker.addItem("*")
        self.marker.addItem("+")
        layout.addWidget(self.marker)
        self.layout.addLayout(layout)

        self.buttonBox = QDialogButtonBox(self)
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(
            QDialogButtonBox.Cancel | QDialogButtonBox.Apply | QDialogButtonBox.Ok
        )
        self.layout.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
