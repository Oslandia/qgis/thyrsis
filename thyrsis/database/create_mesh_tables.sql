PRAGMA foreign_keys = ON;;

CREATE TABLE contours(
    OGC_FID INTEGER PRIMARY KEY AUTOINCREMENT,
    groupe TEXT,
    nom TEXT,
    longueur_element REAL NOT NULL DEFAULT 500,
    epaisseur REAL,
    potentiel_impose INTEGER DEFAULT 0 CHECK (potentiel_impose IN (0, 1)));;
SELECT AddGeometryColumn('contours', 'GEOMETRY', SRID, 'LINESTRING', 'XYZ');;
SELECT CreateSpatialIndex('contours', 'GEOMETRY');;

CREATE TABLE bords(
    OGC_FID INTEGER PRIMARY KEY);;
SELECT AddGeometryColumn('bords', 'GEOMETRY', SRID, 'LINESTRING', 'XY');;
SELECT CreateSpatialIndex('bords', 'GEOMETRY');;

CREATE TABLE domaines(
    OGC_FID INTEGER PRIMARY KEY);;
SELECT AddGeometryColumn('domaines', 'GEOMETRY', SRID, 'POLYGON', 'XY');;
SELECT CreateSpatialIndex('domaines', 'GEOMETRY');;

CREATE TABLE extremites(
    OGC_FID INTEGER PRIMARY KEY);;
SELECT AddGeometryColumn('extremites', 'GEOMETRY', SRID, 'POINT', 'XY');;
SELECT CreateSpatialIndex('extremites', 'GEOMETRY');;

CREATE TABLE points_fixes(
    OGC_FID INTEGER PRIMARY KEY AUTOINCREMENT,
    nom TEXT,
    groupe TEXT,
    longueur_element REAL,
    epaisseur REAL);;
SELECT AddGeometryColumn('points_fixes', 'GEOMETRY', SRID, 'POINT', 'XY');;
SELECT CreateSpatialIndex('points_fixes', 'GEOMETRY');;

-- default parameters for simulation
CREATE TABLE parametres(
    nom TEXT PRIMARY KEY,
    valeur REAL NOT NULL,
    valeur_min REAL,
    valeur_max REAL,
    loi_de_variation TEXT CONSTRAIN loi_de_variation_cstr DEFAULT 'constant'
        CHECK (loi_de_variation IN ('constant', 'normal', 'lognormal',
                                    'uniform', 'loguniform')),
    description TEXT);;

INSERT INTO parametres(nom, valeur, description) VALUES ('WC', 0, 'Porosité cinématique');;
INSERT INTO parametres(nom, valeur, description) VALUES ('VGA', 0, 'Alpha van Genuchten (m-1)');;
INSERT INTO parametres(nom, valeur, description) VALUES ('VGN', 0, 'n van Genuchten');;
INSERT INTO parametres(nom, valeur, description) VALUES ('VGR', 0, 'Saturation résiduelle');;
INSERT INTO parametres(nom, valeur, description) VALUES ('VGS', 0, 'Saturation maximale');;
INSERT INTO parametres(nom, valeur, description) VALUES ('VGE', 0, 'Pression d''entrée (m)');;
INSERT INTO parametres(nom, valeur, description) VALUES ('WT', 0, 'Porosité totale');;
INSERT INTO parametres(nom, valeur, description) VALUES ('VM', 0, 'Masse volumique (kg/m3)');;
INSERT INTO parametres(nom, valeur, description) VALUES ('DK', 0, 'Coefficient de partage (m3/kg)');;
INSERT INTO parametres(nom, valeur, description) VALUES ('SL', 0, 'Limite de solubilité (kg/m3)');;
INSERT INTO parametres(nom, valeur, description) VALUES ('DLZNS', 0, 'Dispersivité longitudinale ZNS (m)');;
INSERT INTO parametres(nom, valeur, description) VALUES ('DLZS', 0, 'Dispersivité longitudinale ZS (m)');;

CREATE TABLE noeuds (
    OGC_FID INTEGER PRIMARY KEY,
    surface REAL,
    altitude REAL,
    altitude_mur REAL,
    infiltration REAL,
    epaisseur_aquifere REAL,
    potentiel_reference REAL,
    potentiel REAL,
    flux_eau REAL,
    epaisseur_zs REAL,
    epaisseur_zns REAL,
    permeabilite_zns REAL, -- used for permnoeuds.insat
    permeabilite_x REAL,
    permeabilite_y REAL,
    permeabilite_z REAL,
    v_x REAL,
    v_y REAL,
    v_z REAL,
    v_norme REAL,
    groupe INTEGER);;
SELECT AddGeometryColumn('noeuds', 'GEOMETRY', SRID, 'POINT', 'XY');;
SELECT CreateSpatialIndex('noeuds', 'GEOMETRY');;

CREATE TABLE noeuds_second_milieu(
    OGC_FID INTEGER PRIMARY KEY REFERENCES noeuds(OGC_FID) ON UPDATE CASCADE ON DELETE CASCADE,
    infiltration REAL,
    epaisseur_aquifere REAL,
    potentiel_reference REAL,
    potentiel REAL,
    flux_eau REAL,
    epaisseur_zs REAL,
    epaisseur_zns REAL,
    permeabilite_zns REAL,
    permeabilite_x REAL,
    permeabilite_y REAL,
    permeabilite_z REAL,
    v_x REAL,
    v_y REAL,
    v_z REAL,
    v_norme REAL);;


CREATE TABLE potentiel_impose (
    id INTEGER PRIMARY KEY,
    nid INTEGER REFERENCES noeuds(OGC_FID) ON UPDATE CASCADE ON DELETE CASCADE,
    valeur REAL);;

-- vérifier si potentiel identique sur deux milieux, a priori oui

CREATE TABLE noeuds_contour (
    id INTEGER PRIMARY KEY,
    nid INTEGER REFERENCES noeuds(OGC_FID) ON UPDATE CASCADE ON DELETE CASCADE);;

CREATE TABLE mailles (
    OGC_FID INTEGER PRIMARY KEY,
    a INTEGER REFERENCES noeuds(OGC_FID)
        ON UPDATE CASCADE ON DELETE CASCADE,
    b INTEGER REFERENCES noeuds(OGC_FID)
        ON UPDATE CASCADE ON DELETE CASCADE,
    c INTEGER REFERENCES noeuds(OGC_FID)
        ON UPDATE CASCADE ON DELETE CASCADE,
    d INTEGER REFERENCES noeuds(OGC_FID)
        ON UPDATE CASCADE ON DELETE CASCADE,
    surface REAL,
    altitude REAL,
    altitude_mur REAL,
    infiltration REAL,
    epaisseur_aquifere REAL,
    potentiel_reference REAL,
    potentiel REAL,
    flux_eau REAL,
    epaisseur_zs REAL,
    epaisseur_zns REAL,
    permeabilite_zns REAL, -- used for permnoeuds.insat
    permeabilite_x REAL,
    permeabilite_y REAL,
    permeabilite_z REAL,
    v_x REAL,
    v_y REAL,
    v_z REAL,
    v_norme REAL);;
SELECT AddGeometryColumn('mailles', 'GEOMETRY', SRID, 'POLYGON', 'XY');;
SELECT CreateSpatialIndex('mailles', 'GEOMETRY');;
CREATE INDEX mailles_a_idx ON mailles(a);;
CREATE INDEX mailles_b_idx ON mailles(b);;
CREATE INDEX mailles_c_idx ON mailles(c);;
CREATE INDEX mailles_d_idx ON mailles(d);;

CREATE TABLE mailles_second_milieu(
    OGC_FID INTEGER PRIMARY KEY REFERENCES mailles(OGC_FID) ON UPDATE CASCADE ON DELETE CASCADE,
    infiltration REAL,
    epaisseur_aquifere REAL,
    potentiel_reference REAL,
    potentiel REAL,
    flux_eau REAL,
    epaisseur_zs REAL,
    epaisseur_zns REAL,
    permeabilite_zns REAL, -- used for permnoeuds.insat
    permeabilite_x REAL,
    permeabilite_y REAL,
    permeabilite_z REAL,
    v_x REAL,
    v_y REAL,
    v_z REAL,
    v_norme REAL);;

CREATE TABLE zones(
    id INTEGER PRIMARY KEY,
    numero INTEGER,
    permeabilite REAL,
    permeabilite_min REAL,
    permeabilite_max REAL);;

CREATE TABLE points_pilote(
    OGC_FID INTEGER PRIMARY KEY AUTOINCREMENT,
    nom TEXT,
    groupe INTEGER,
    zone INTEGER,
    altitude_piezo_mesuree REAL,
    altitude_piezo_calculee REAL,
    difference_calcul_mesure REAL,
    permeabilite REAL);;
SELECT AddGeometryColumn('points_pilote', 'GEOMETRY', SRID, 'POINT', 'XY');;
SELECT CreateSpatialIndex('points_pilote', 'GEOMETRY');;

CREATE TABLE metis_hynverse_options(
    id INTEGER PRIMARY KEY,
    niter_gc_zs INTEGER DEFAULT 100,
    max_iter INTEGER DEFAULT 100,
    relax_phreat REAL DEFAULT 0.5,
    critere_pot REAL DEFAULT 0.01,
    phrea_min REAL DEFAULT 1.0);;
INSERT INTO metis_hynverse_options DEFAULT VALUES;;

CREATE TABLE openfoam_hynverse_options(
    id INTEGER PRIMARY KEY,
    fluid_density REAL DEFAULT 1.e3,
    dynamic_viscosity REAL DEFAULT 1.e-3,
    potential_tolerance REAL DEFAULT 1.e-9,
    potential_tolerance_relative REAL DEFAULT 0.1,
    potential_residualControl REAL DEFAULT 1.e-8,
    potential_relaxationFactor REAL DEFAULT 0.1,
    hwatermin REAL DEFAULT 0.1);;
INSERT INTO openfoam_hynverse_options DEFAULT VALUES;;

CREATE TABLE hynverse_parametres(
    OGC_FID INTEGER PRIMARY KEY,
    infiltration REAL DEFAULT 0.,
    icalc INTEGER DEFAULT 3,
    niter INTEGER DEFAULT 20,
    errlim REAL DEFAULT 0.1,
    alfmin REAL DEFAULT 1.e-5,
    alfa REAL DEFAULT 0.5,
    terr REAL DEFAULT 1.2,
    nessaismax INTEGER DEFAULT 10,
    permini REAL DEFAULT 2.e-5,
    permin REAL DEFAULT 2.e-7,
    permax REAL DEFAULT 2.e-3,
    nv_npp INTEGER DEFAULT 4,
    nv_ppm INTEGER DEFAULT 10,
    dv_pp REAL DEFAULT 300.,
    d_mesh REAL DEFAULT 100.);;
INSERT INTO hynverse_parametres DEFAULT VALUES;;

CREATE TABLE hynverse_erreurs(
    OGC_FID INTEGER PRIMARY KEY,
    iteration REAL DEFAULT 0.,
    iteration_vraie INTEGER DEFAULT 0,
    nessais INTEGER DEFAULT 0,
    erreur REAL DEFAULT 0.,
    alfa REAL DEFAULT 0.5,
    terr REAL DEFAULT 1.2);;

-- Create views
-- TO DO : distance buffer set to 20, but depend on projection (EPSG:4326, 20 degree of delta will be hell...)
-- c1.OGC_FID < c2.OGC_FID if to avoid duplicates (c1, c2) and (c2, c1) in result
create view topology_errors as
select /*ROW_NUMBER() OVER(ORDER BY c1.OGC_FID) AS id,*/ "Contour " || c1id || " and " || c2id || " must share a node" as error, intersections.GEOMETRY as geometry
from contours c1, contours c2, (
    select st_intersection(c1.GEOMETRY,c2.GEOMETRY) as GEOMETRY, c1.OGC_FID as c1id, c2.OGC_FID as c2id
    from contours c1, contours c2
    where c1.OGC_FID < c2.OGC_FID
    and st_intersects(c1.GEOMETRY, c2.GEOMETRY)) intersections
where c1.OGC_FID = c1id and c2.OGC_FID = c2id
    and not ( st_intersects(c1.GEOMETRY, intersections.GEOMETRY) and st_intersects(c2.GEOMETRY, intersections.GEOMETRY) )
UNION ALL
select "Extremites " || e.OGC_FID || " is not closed" as error, e.GEOMETRY as geometry
from
    extremites e,
    (SELECT st_buffer(e.GEOMETRY,20) as GEOMETRY, e.GEOMETRY as OGEOM
     from extremites e) b
where st_coveredby(e.GEOMETRY, b.GEOMETRY) = True AND st_equals(e.GEOMETRY, b.OGEOM)=false;;

INSERT INTO geometry_columns (f_table_name, f_geometry_column, geometry_type, coord_dimension, srid, spatial_index_enabled)
    VALUES ("topology_errors", "geometry", 1, 2, SRID, 0);;

create view bad_triangles as
select min(dist1,dist2)/max(dist1,dist2) as ratio, id, geometry
from (
    select st_distance(st_closestpoint(makeline(p1, p2), p3),p3) as dist1, st_distance(p1, p2) as dist2, id, geometry
    from (
	select st_geometryn(points,1) as p1, st_geometryn(points,2) as p2, st_geometryn(points,3) as p3, id, geometry
	from (select st_dissolvepoints(GEOMETRY) as points, OGC_FID as id, GEOMETRY as geometry from mailles)))
where min(dist1,dist2)/max(dist1,dist2) < 0.01 ;;

INSERT INTO geometry_columns (f_table_name, f_geometry_column, geometry_type, coord_dimension, srid, spatial_index_enabled)
    VALUES ("bad_triangles", "geometry", 3, 2, SRID, 0);;
