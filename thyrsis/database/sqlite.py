import sqlite3
from builtins import object

# -*- coding: utf-8 -*-


class DbCon(object):
    """Wrapper for sqlite connection"""

    def __init__(self, conn_info, comment=""):
        """Constructor of DbCon class, a wrapper for sqlite connection

        :param conn_info: information to establish connection, path of the database
        :type conn_info: string
        :param comment: comment
        :type rasterName: string
        """
        # print("opening connection", comment)
        self.__comment = comment
        self.__conn = sqlite3.connect(conn_info)
        self.__conn.enable_load_extension(True)
        libs = [
            # SpatiaLite >= 4.2 and Sqlite >= 3.7.17, should work on all platforms
            ("mod_spatialite", "sqlite3_modspatialite_init"),
            # SpatiaLite >= 4.2 and Sqlite < 3.7.17 (Travis)
            ("mod_spatialite.so", "sqlite3_modspatialite_init"),
            # SpatiaLite < 4.2 (linux)
            ("libspatialite.so", "sqlite3_extension_init"),
        ]
        found = False
        for lib, entry_point in libs:
            try:
                self.__conn.cursor().execute(
                    "select load_extension('{}', '{}')".format(lib, entry_point)
                )
            except sqlite3.OperationalError as e:
                continue
            else:
                found = True
                break
        if not found:
            raise RuntimeError("Cannot find any suitable spatialite module")

    def cursor(self):
        """Return cursor of the connection

        :return: cursor
        :rtype: sqlite3.Cursor
        """
        return self.__conn.cursor()

    def close(self):
        """Close the connection

        :return: success state
        :rtype: bool
        """
        return self.__conn.close()

    def execute(self, query):
        """Execute the query

        :param query: sql query
        :type: string
        :return: success state
        :rtype: bool
        """
        return self.__conn.execute(query)

    def commit(self):
        """Commit changes
        :return: success state
        :rtype: bool
        """
        return self.__conn.commit()

    def rollback(self):
        """Cancel changes

        :return: success state
        :rtype: bool
        """
        return self.__conn.rollback()

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__conn.close()
        # print("connection closed", self.__comment)

    def __enter__(self):
        return self


def connect(conn_info, comment=""):
    """Create a sqlite connection

    :param conn_info: information to establish connection, path of the database
    :type conn_info: string
    :param comment: comment
    :type rasterName: string

    :return: sqlite connection
    :rtype: DbCon
    """
    return DbCon(conn_info, comment)
