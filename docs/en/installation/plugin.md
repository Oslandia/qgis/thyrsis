# Install the {{ title }} plugin

## Stable version (recommended)

This plugin is published on the official QGIS plugins repository: <https://plugins.qgis.org/plugins/thyrsis/>.

Go to `Plugins` -> `Manage and Install Plugins`, look for the plugin and install it.

:::{note}
This version doesn't contain documentation for offline use.
:::

----

## Beta versions released

Enable experimental extensions in the QGIS plugins manager settings panel.

----

## Earlier development version

If you define yourself as early adopter or a tester and can't wait for the release, the plugin is automatically packaged for each commit to master, so you can use this address as repository URL in your QGIS extensions manager settings:

:::{warning}
Be careful, this version can be unstable. Do not use it in production.
:::

### Through the custom plugin repository

You can add the following URL to the QGIS extensions repositories, in the `Settings` tab of the `Plugins Manager` window (see [official documentation](https://docs.qgis.org/3.16/en/docs/user_manual/plugins/plugins.html#the-settings-tab)):

```url
https://oslandia.gitlab.io/qgis/thyrsis/plugins.xml
```

:::{note}
The URL must be reachable on your network.
:::

### Downloading latest CI/CD artifact

For every commit to main branch, the plugin is packaged and stored as artifact with a certain lifetime (i.e; it expires after a few days):

1. Download the [latest builder artifact](https://gitlab.com/Oslandia/qgis/thyrsis/-/jobs/artifacts/master/download?job=build:master)
2. Extract the plugin archive
3. Install it through the menu `Install from ZIP` in the plugins manager of QGIS
