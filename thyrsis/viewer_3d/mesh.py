from builtins import object

import numpy
from OpenGL.GL import *
from OpenGL.GL import shaders

from ..meshlayer.glmesh import ColorLegend, GlMesh
from .utility import computeNormals, printCompileError

# -*- coding: UTF-8 -*-


class Mesh(object):
    """3D mesh"""

    def __init__(
        self, dataProvider, colorLegend, zColumn, invertNrml=False, parent=None
    ):
        """Constructor

        :param dataProvider:  data provider
        :type dataProvider: MeshDataProvider
        :param colorLegend: ColorLegend
        :type colorLegend: ColorLegend
        :param zColumn: formula for column height or altitude
        :type zColumn: string
        :param invertNrml: invert flag
        :type invertNrml: bool
        :param parent: widget parent
        :type parent: QWidget
        """
        self.__resultShaders = None
        self.__vtx = dataProvider.nodeCoord(zColumn)
        self.__idx = dataProvider.triangles()
        self.__dataProvider = dataProvider
        self.__invertNrml = invertNrml
        self.__refLegend = colorLegend
        self.__legend = ColorLegend()
        self.__legend.setParent(parent)
        self.__refLegend.symbologyChanged.connect(self.updateSymbology)
        self.__glMesh = GlMesh(
            dataProvider.nodeCoord(), dataProvider.triangles(), self.__legend
        )
        self.__glMesh.setParent(parent)
        self.scaleZ(1)  # to compute nrml
        self.__colorPerElement = False

    def setColorPerElement(self, flag):
        """Apply color per element, is the mesh is based on element

        :param flag: flag for element base mesh
        :param flag: bool
        """
        if self.__colorPerElement == flag:
            return  # nothing to do
        self.__colorPerElement = flag
        if self.__colorPerElement:
            # we duplicate vertices
            idx = self.__idx
            self.__origVtx = self.__vtx
            self.__vtx = numpy.concatenate(
                (
                    self.__origVtx[self.__idx[:, 0]],
                    self.__origVtx[self.__idx[:, 1]],
                    self.__origVtx[self.__idx[:, 2]],
                )
            )
            self.__origIdx = self.__idx
            nbElem = len(self.__idx)
            self.__idx = numpy.reshape(
                numpy.array(
                    [
                        numpy.arange(nbElem),
                        numpy.arange(nbElem, 2 * nbElem),
                        numpy.arange(2 * nbElem, 3 * nbElem),
                    ]
                ),
                (3, -1),
            ).transpose()
            self.__nrml = computeNormals(self.__vtx, self.__idx)
        else:
            self.__idx = self.__origIdx
            self.__vtx = self.__origVtx
            self.__nrml = computeNormals(self.__vtx, self.__idx)

        if self.__invertNrml:
            self.__nrml *= -1

    def compileShaders(self):
        """Compile GLSL shader into binaries"""
        if self.__legend:
            try:
                vertex_shader = shaders.compileShader(
                    """
                    varying float value;
                    varying vec3 normal;
                    varying vec4 ecPos;
                    void main()
                    {
                        ecPos = gl_ModelViewMatrix * gl_Vertex;
                        normal = normalize(gl_NormalMatrix * gl_Normal);
                        value = gl_MultiTexCoord0.st.x;
                        gl_Position = ftransform();
                    }
                    """,
                    GL_VERTEX_SHADER,
                )
                fragment_shader = shaders.compileShader(
                    self.__refLegend._fragmentShader(), GL_FRAGMENT_SHADER
                )
                self.__resultShaders = shaders.compileProgram(
                    vertex_shader, fragment_shader
                )
                self.__legend._setUniformsLocation(self.__resultShaders)

            except Exception as e:
                printCompileError(e)
                raise e

    def updateSymbology(self):
        """Update 3D rendering"""
        self.__legend.setColorRamp(self.__refLegend.colorRampImg())
        self.__legend.setTitle(self.__refLegend.title())
        self.__legend.setUnits(self.__refLegend.units())
        self.__legend.setMinValue(self.__refLegend.minValue())
        self.__legend.setMaxValue(self.__refLegend.maxValue())
        self.__legend.setLogScale(self.__refLegend.hasLogScale())
        if self.__refLegend.graduated():
            self.__legend.setGraduation(self.__refLegend.graduation())
        self.__legend.parent().makeCurrent()
        self.render()

    def render(self, context=None):
        """Create render

        :param context: openGL context
        :type context: QOpenGLContext
        """
        context = context or self.__legend.parent()
        glEnableClientState(GL_VERTEX_ARRAY)
        glEnableClientState(GL_NORMAL_ARRAY)
        glEnable(GL_LIGHTING)
        glEnable(GL_LIGHT0)
        self.setColorPerElement(self.__dataProvider.valueAtElement())
        val = (
            self.__dataProvider.elementValues()
            if self.__colorPerElement
            else self.__dataProvider.nodeValues()
        )
        glVertexPointerf(self.__vtx)
        glNormalPointerf(self.__nrml)
        if len(val):
            if self.__glMesh._GlMesh__recompileShader:
                self.compileShaders()
            if self.__colorPerElement:
                val = numpy.concatenate((val, val, val))
            assert len(val) == len(self.__vtx)
            glDisable(GL_TEXTURE_2D)
            glDisable(GL_COLOR_MATERIAL)
            glEnableClientState(GL_TEXTURE_COORD_ARRAY)
            glUseProgram(self.__resultShaders)
            self.__legend._setUniforms(context, True)
            glTexCoordPointer(1, GL_FLOAT, 0, val)
        else:
            glDisableClientState(GL_TEXTURE_COORD_ARRAY)
            glEnable(GL_COLOR_MATERIAL)
            glDisable(GL_TEXTURE_2D)
            glColor4f(0, 0, 1, 1)

        glDrawElementsui(GL_TRIANGLES, self.__idx)
        glUseProgram(0)

    def scaleZ(self, factor):
        """Apply a x factor to the z value

        :param factor: factor
        :type factor: float
        """
        self.__vtx[:, 2] *= factor
        self.__nrml = computeNormals(self.__vtx, self.__idx)
        if self.__invertNrml:
            self.__nrml *= -1

    def colorLegend(self):
        """Return the color legend

        :return: legend
        :rtype: ColorLegend
        """
        return self.__legend
