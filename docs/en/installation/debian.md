# Debian and related distributions

:::{note}
The plugin has been developed and tested against Ubuntu 20.04 and QGIS 3.22.
:::

## Requirements

- {{ qgis_version_min }} =< QGIS =< {{ qgis_version_max }}

### Simulation softwares

#### OpenFOAM and porousMultiphaseFoam

:::{note}
The plugin has been tested against OpenFOAM v2406 with the `dev` flavor (see the [official documentation about sub-packages](https://develop.openfoam.com/Development/openfoam/-/wikis/precompiled#sub-packages)).
:::

For convenience or offline use, we replicate here some of the OpenFOAM installation instructions but please refer to [the official documentation](https://develop.openfoam.com/Development/openfoam/-/wikis/precompiled/debian) for updated steps:

```bash
curl -s https://dl.openfoam.com/add-debian-repo.sh | sudo bash
sudo apt install -y openfoam2406-dev
```

`cartesian2DMesh` binary is not available in OpenFoam binary package in 2406 version. There was an [issue](https://develop.openfoam.com/Development/openfoam/-/issues/3198) with packaging.

To have support of mesh with `cfMesh`, you need to install `integration-cfmesh` package from OpenFoam repository:

```bash
git clone https://develop.openfoam.com/Community/integration-cfmesh
cd integration-cfmesh
. /usr/lib/openfoam/openfoam2406/etc/bashrc
./Allwmake
./Allwclean
```

Then install the `porousMultiphaseFoam` package:

```bash
git clone --depth=1 --branch=v2406.2 --single-branch https://github.com/phorgue/porousMultiphaseFoam.git
cd porousMultiphaseFoam/
. /usr/lib/openfoam/openfoam2406/etc/bashrc
./Allwmake
./Allwclean
```

#### Metis

> TO DOC

### Others

:::{note}
3rd party softwares and related Python packages are listed into the [requirements/apt.txt](https://gitlab.com/Oslandia/qgis/thyrsis/-/blob/master/requirements/apt.txt) file. Please refer to it to get updated information about what to install.
:::

- Gmsh (version 4.*): get it from [the official website](https://gmsh.info/).
- [FFMPEG](https://ffmpeg.org/) (for video export)
- [MPICH](https://www.mpich.org/)

Typically:

```bash
sudo apt install gmsh=4* ffmpeg mpich
```

### Python packages

From Debian repositories:

```bash
sudo apt install python3-mpi4py python3-opengl python3-rtree
```

From the Python Package Index (PyPi):

```bash
python3 -m pip install --upgrade mpi4py==3.1.3 rtree==0.9.7
```
