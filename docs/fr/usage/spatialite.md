# Bases de données Spatialite

Ce chapitre décrit les trois types de base de données manipulées par le plugin THYRSIS.

## Base sites.sqlite

La base **sites.sqlite**, définie dans les {ref}`Préférences<preferences_fr>` d'installation, contient différents types de tables, caractérisant les éléments chimiques utilisables, les sites étudiés et les simulations de référence.

### Table elements_chimiques

La table **elements_chimiques** contient les principales caractéristiques des éléments chimiques et de leurs isotopes : numéro et masse atomiques, activité spécifique, décroissance radioactive.

### Tables caractérisant les sites d'étude

- **sites** : cette table associe à chaque site un identifiant numérique.
- **points_interet** : cette table regroupe tous les points d'intérêt pour chaque site étudié, en leur associant un nom et un groupe d'appartenance.
- **mesures** : cette table regroupe toutes les mesures associées aux points d'intérêt, avec la date, l'identifiant de l'élément chimique, la concentration et son incertitude, le niveau piézométrique et son incertitude.
- **forages** : cette table regroupe les caractéristiques générales des forages dont la stratigraphie est connue (nom, type, date de réalisation, profondeur, hauteur du tube par rapport au sol).
- **lithologies** : cette table fournit pour chaque forage le type de roche et son code USGS en fonction de la profondeur.
- **stratigraphies** : cette table fournit pour chaque forage la formation géologique et son code BRGM en fonction de la profondeur.
- **fracturations** : cette table fournit pour chaque forage les taux de fracturation en fonction de la profondeur.

(ref_simulation_table_fr)=
### Tables caractérisant les simulations de référence

Les simulations peuvent être stockées dans la base sites.sqlite (cf. {ref}`Exporter<export_fr>`, § Template), et peuvent servir de point de départ pour de nouvelles simulations. Plusieurs tables servent à stocker les informations permettant de les caractériser :

(simulation_table_fr)=
- **simulations** :

Cette table fait référence à un site et à un modèle hydrogéologique donnés,et définit certains paramètres comme :

  - `nombre_de_simulations` désigne le nombre de simulations à réaliser, supérieur à 1 dans le cas de calculs d'incertitudes.
  - `coef_maillage` désigne le rapport entre l'épaisseur de la maille au milieu de la colonne et celle de la première maille en surface, égal par défaut à 5.
  - `pas_echant_zns`  désigne le pas d'échantillonnage des colonnes, dans le cas du regroupement des colonnes par taille. Il définit la taille des colonnes d'un même groupe. Ainsi, un pas de 5 m définit des groupes de colonnes de taille \[0,5\], \[5,10\], \[10,15\], etc. Le nombre de groupes est d'autant plus réduit que le pas est élevé.
  - `rapport_max_permeabilite` désigne le rapport maximal de perméabilité qui peut exister entre les perméabilités (à saturation) de deux colonnes d'un même groupe. Il est égal à 10 par défaut.
  - `permeabilite_zns_uniforme` désigne une valeur de perméabilité à saturation qui, si elle n'est pas nulle et si le fichier u_permeabilite_zns.node.sat or u_permeabilite_zns.elem.sat est absent, est affectée à chaque colonne non saturée. Cette valeur est nulle par défaut.
  - `type_injection` définit le type d'injection, et peut prendre la valeur 'aucune' (défaut), 'flux', 'concentration' ou 'masse'.
  - `type_infiltration` définit le type d'infiltration, et peut prendre la valeur 'permanente' (défaut) ou 'transitoire'.
  - `insature` prend la valeur 'oui' (défaut) ou 'non' et indique si la zone non saturée doit être modélisée.

- **parametres** :

Cette table fait référence à la table simulations, et définit les valeurs de chaque paramètre pour la simulation considérée :

  - `nom` est le nom du pramètre, parmi:
    - WC, porosité cinématique,
    - VGA, Alpha van Genuchten ($m^{-1}$),
    - VGN, n van Genuchten,
    - VGR, Saturation résiduelle,
    - VGS, Saturation maximale,
    - VGE, Pression d'entrée (m) ($m$),
    - WT, Porosité totale,
    - VM, Masse volumique (kg/m³),
    - DK, Coefficient de partage (m³/kg),
    - SL, Limite de solubilité (kg/m³),
    - DLZNS, Dispersivité longitudinale ZNS (m),
    - DLZS, Dispersivité longitudinale ZS (m),

  - `valeur` désigne la valeur du paramètre lorsque nombre_de_simulations = 1,
  - `valeur_min` désigne la valeur minimale du paramètre dans le cadre de l'{ref}`Hypercube Latin<latin_hypercube_fr>`,
  - `valeur_max` désigne la valeur maximale du paramètre dans le cadre de l'{ref}`Hypercube Latin<latin_hypercube_fr>`,
  - `loi_de_variation` désigne la loi de probabilité du paramètre dans le cadre de l'{ref}`Hypercube Latin<latin_hypercube_fr>`. Elle peut prendre comme valeur 'constant' (défaut), 'normal', 'lognormal', 'uniform' ou 'loguniform'.

Les paramètres VGA, VGN, VGR, VGS, VGE ne sont autres que les coefficients $\alpha$, $n$, $\theta_{r}$, $\theta_{s}$, $h_{e}$ de la formulation d'Ippisch & Vogel ({ref}`ZNS <zns_fr>`).

Dans le cas d'un modèle double-milieu, les paramètres ci-dessus sont dupliqués, avec un suffixe 2 désignant le second milieu, et quatre paramètres d'interaction sont ajoutés :

    - PL, perméabilité de liaison (m/s),
    - ASHAPE, facteur représentant la demi-distance moyenne entre deux fractures,
    - BSHAPE, facteur dépendant de la géométrie de la fracturation (une valeur de 3 correspond à des fractures orthogonales régulièrement espacées),
    - CSHAPE, coefficient empirique, égal généralement à 0,4.

Ces paramètres PL, ASHAPE, BSHAPE et CSHAPE correspondent respectivement aux coefficients $K_{a}$, $a$, $\beta$ et $\gamma_{w}$ du modèle {ref}`Double milieu <dual-porosity_fr>`.

- **openfoam_options** :

Cette table, qui fait référence à la table simulations, regroupe toutes les options du code OPENFOAM. Les valeurs par défaut qui sont attribuées permettent de couvrir la quasi-totalité des cas.
Des modifications peuvent être cependant nécessaires pour traiter certains cas particulièrement difficiles numériquement. Ces options sont définies succinctement ci-après, avec, entre parenthèses, la valeur par défaut.

  - Paramètres numériques

    - h_tolerance: tolérance pour le solveur ZNS "h" (1.e-12).
    - h_tolerance_relative: tolérance relative pour le solveur ZNS "h" (1.e-2).
    - h_relaxationFactor: facteur de relaxation pour le solveur ZNS stationnaire "h" (1.e-2).
    - h_relaxationFactor_transient: facteur de relaxation pour le solveur ZNS transitoire "h" (2.e-1).
    - picard_tolerance: tolérance pour l'algorithme de Picard en écoulement permanent (1.e-8).
    - picard_maxiter: nombre maximal d'itérations pour l'algorithme de Picard en écoulement permanent (20).
    - picard_tolerance_transient: tolérance pour l'algorithme de Picard en écoulement transitoire (1.e-1).
    - picard_maxiter_transient: nombre maximal d'itérations pour l'algorithme de Picard en écoulement transitoire (3).
    - newton_tolerance: tolérance pour l'algorithme de Newton en transitoire (1.e-8).
    - newton_maxiter: nombre maximal d'itérations pour l'algorithme de Newton en transitoire (20).
    - potential_tolerance: tolérance pour le solveur ZS "potential" (1.e-8).
    - potential_tolerance_relative: tolérance relative pour le solveur ZS transitoire "potential" (1.e-1).
    - potential_residualControl: contrôle du résidu pour le solveur ZS stationnaire "potential" (1.e-6).
    - potential_relaxationFactor: facteur de relaxation pour le solveur ZS stationnaire "potential" (5.e-1).

  - Gestion du pas de temps

    - delta_time_init: pas de temps initial (10).
    - dtfact_decrease: taux de décroissance du pas de temps si l'algorithme de Picard ou de Newton n'a pas convergé (0,8).
    - truncationError: erreur de troncation du schéma temporel pour le calcul du pas de temps (0,01).

  - Propriétés

    - fluid_density: densité du fluide \[kg/m3]\ (1.e3).
    - dynamic_viscosity: viscosité dynamique du fluide \[Pa.s\] (1.e-3).
    - molecular_diffusion: diffusion moléculaire \[m/s2\] (0).
    - tortuosity: tortuosité \[-\] (1).
    - specific_storage: coefficient d'emmagasinement \[-\] (0).
    - hwatermin: épaisseur minimale de zone saturée pour le solveur ZS "potential" \[m\] (0,1).

- **metis_options** :

Cette table, qui fait référence à la table simulations, regroupe toutes les options du code METIS. Les valeurs par défaut qui sont attribuées permettent de couvrir la quasi-totalité des cas.
Des modifications peuvent être cependant nécessaires pour traiter certains cas particulièrement difficiles numériquement.
Ces options sont définies succinctement ci-après, avec, entre parenthèses, la valeur par défaut.

  - `niter_gc_zns`: nombre maximum d'itérations ZNS pour le gradient conjugué (100).
  - `niter_gc_zs`: nombre maximum d'itérations ZS pour le gradient conjugué (100).

Les options suivantes concernent la résolution de l'écoulement en régime permanent dans la ZNS :

  - saturation_initiale: valeur de la saturation initiale pour le calcul d'écoulement ZNS en régime permanent (0.8),
  - rmult_cormax: coefficient multiplicateur de la dimension maximale, définissant la variation maximale de la charge hydraulique (0.1),
  - c_pstep: valeur initiale du fractionnement de la solution (0.1),
  - c_ps_inc: valeur initiale de l'incrément (0.1),
  - c_ps_tol: tolérance sur le paramètre de parameter stepping: on considère qu'on a convergé quand c_pstep est plus grand que 1 - c_ps_tol (permet d'abréger une convergence laborieuse où on tend vers 1 sans jamais exactement l'atteindre) (1.e-5),
  - ratio_max_res: comparaison de deux modes de calcul des résidus. Permet de détecter plus précisément la convergence locale dans des cas où les résidus oscillent un peu (1.e-6),
  - critereres: critère de convergence des résidus. Le calcul converge quand c_pstep vaut 1 et redres < critereres (1.e-5),
  - niterns: nombre d'itérations pour le calcul d'écoulement ZNS en régime permanent (1000).
  - courant: égal à 'oui' ou 'non'. Le nombre de Courant est calculé à partir d'une pseudo-vitesse du front de saturation. Si courant = 'oui', le pas de temps sera ajusté pour que le front ne franchisse pas plus d'un élément par pas. Utile en 1D pour des fronts très abrupts. ('oui').
  - emmagasinement: emmagasinement spécifique ($m^{-1}$), utilisé pour gérer les fuites importantes avec l'option sat_nonsat = on, associée à courant = 'oui' (1.e-3).
  - kdecoup_delt_zns: pas de temps initial ZNS (10).
  - kdecoup_dmin_zns: pas de temps minimal ZNS (1).
  - kdecoup_delt_zs: pas de temps initial ZS (100).
  - kdecoup_dmin_zs: pas de temps minimal ZS (1).
  - force_fin_phase: égal à 'oui' ou 'non', force la fin de phase avec un grand pas de temps lorsque le pas de temps devient trop petit ('oui').
  - max_iter: nombre maximal d'itérations pour le calcul phréatique (200),
  - relax_phreat: coefficient de relaxation (poids attribué à la dernière itération) (0.5).
  - critere_pot: critère de convergence sur le potentiel (0.1 m),
  - phrea_min: epaisseur_mouillee_mini (1.0 m).

(injection_table_fr)=

- **injections** :

Cette table, qui fait référence à la table simulations, définit les caractéristiques des injections :

  - profondeur: profondeur de l'injection (m)
  - debut: date de début d'injection, au format JJ/MM/AAAA
  - duree: durée d'injection, en unité 'jour', 'mois' ou 'an'
  - nom: nom de l'injection
  - volume_eau: volume d'eau injecté, en m³
  - coefficient_surface: rapport de la surface réelle d'injection à la surface des noeuds (cas d'une injection en puits, où la surface réelle d'injection est limitée à la surface des puits, très inférieure à la surface d'influence des noeuds du maillage)
  - coefficient_injection: rapport de la quantité réellement injectée à la quantité injectable en terme de solubilité (permet de ne prendre en compte qu'une partie de la masse totale, pour une durée d'injection calculée à partir de la masse totale - intéressant seulement dans le cas d'une injection en masse. Dans le cas d'une injection en concentration ou flux, c'est seulement un coefficient multiplicatif de la concentration ou du flux
  - permeabilite: valeur de la perméabilité à saturation, déduite automatiquement du champ de perméabilité de la zone saturée, ou d'un fichier u_permeabilite_zns.node.sat or u_permeabilite_zns.elem.sat s'il existe, ou égale à la valeur de permeabilite_zns_uniforme de la table {ref}`simulations<simulation_table_fr>`, si elle n'est pas nulle.
  - infiltration: valeur d'infiltration en haut de la colonne non saturée, déduite automatiquement du modèle hydrogéologique.
  - epaisseur: épaisseur de la colonne ZNS, déduite automatiquement du modèle hydrogéologique.
  - altitude: altitude du haut de la colonne ZNS, déduite automatiquement du modèle hydrogéologique.

- **dates_resultats** :

Cette table fait référence à la table simulations et définit les dates de sortie des résultats, en spécifiant autant de fois que nécessaire :

  - duree: une durée, exprimée en unité 'jour', 'mois' ou 'an',
  - pas_de_temps: un pas de temps, qui échantillonne la durée, exprimé en unité 'jour', 'mois' ou 'an'.

## Bases de données de modèle

Les modèles hydrogéologiques sont stockés dans des bases Spatialite indépendantes, placées dans le répertoire des modèles défini dans les {ref}`Préférences<preferences_fr>` d'installation. Chacune de ces bases contient les tables définissant le maillage 2D horizontal et les caractéristiques du modèle hydrogéologique, ainsi, le cas échéant, que les paramétrages utilisés pour l'inversion.

### Tables de maillage

Les tables suivantes permettent de définir et construire le maillage. Seules les tables **contours** et **points_fixes** sont réellement définies par l'utilisateur. Les autres tables sont créées automatiquement et secondaires.

- bords: table secondaire utilisée par la procédure de maillage,

- **contours** :

Cette table contient la définition des différentes parties du contour du domaine, indiquant les champs suivants :

  - groupe: définit l'appartenance à une catégorie de contour (site, rivière, etc.) (champ non utilisé actuellement),
  - nom: définit le nom de l'élément de contour,
  - longueur_element: définit, en mètres, la distance entre deux noeuds le long de l'élément de contour considéré (permet de contraindre la taille du maillage),
  - potentiel_impose: égal à 1 si l'élément de contour considéré définit un contour à potentiel imposé, égal à NULL ou 0 sinon.

  - domaines: table secondaire définissant les différents domaines à mailler, selon les contours créés,

  - extremites: table secondaire contenant les extrémités des différents éléments de contour,

_ **points_fixes** :

Table définissant les points à intégrer dans le maillage, avec leur distance de noeuds caractéristique :

  - nom: nom du point fixe,
  - groupe: nom du groupe associé au point fixe,
  - longueur_element: distance, en mètres, entre deux noeuds à proximité du point fixe.

:::{figure} /_static/images/table_contours.png
Exemple de table de contours
:::

:::{figure} /_static/images/table_points_fixes.png
Exemple de table de points fixes
:::

### Tables pour l'inversion

Les tables suivantes permettent de définir les paramètres du calcul d'inversion, les options du code METIS utilisées pour l'inversion, et les points pilotes à prendre en compte dans l'inversion (cf. {ref}`Inversion<inversion_fr>`) :

- **hynverse_parametres** :  paramètres de l'inversion (cf. {ref}`Inversion<inversion_fr>`),

- **openfoam_hynverse_options** : OPENFOAM options :

  - fluid_density: fluid density (1.e3).
  - dynamic_viscosity: viscosité dynamique du fluide [Pa.s] (1.e-3).
  - potential_tolerance: tolérance pour le solveur ZS \"potential\"  (1.e-8).
  - potential_tolerance_relative: tolérance relative pour le solveur ZS "transitoire" (0.1).
  - potential_residualControl: contrôle du résidu pour le solveur ZS stationnaire "potential" (1.e-6).
  - potential_relaxationFactor: facteur de relaxation pour le solveur ZS stationnaire "potential" (0.5).
  - hwatermin: minimal groundwater thickness (0.1).

- **metis_hynverse_options** : METIS options :

  - niter_gc_zs: nombre maximum d'itérations ZS pour le gradient conjugué (100),
  - max_iter: nombre maximal d'itérations pour le calcul phréatique  (200),
  - relax_phreat: coefficient de relaxation (poids attribué à la dernière itération) (0.5).
  - critere_pot: critère de convergence sur le potentiel (0.1 m),
  - phrea_min: epaisseur_mouillee_mini (1.0 m).

- **points_pilote** : points pilotes à prendre en compte dans l'inversion :

  - nom : nom du point pilote,
  - groupe : groupe du point pilote, utilisé pour le graphique de corrélation,
  - zone : pilot point zone,
  - altitude_piezo_mesuree : niveau piézométrique mesuré (m), à renseigner avant l'inversion
  - altitude_piezo_calculee : niveau piézométrique calculé (m), valeur mise à jour par le calcul d'inversion
  - difference_calcul_mesure : différence entre les niveaux piézométriques calculé et mesuré (m), valeur mise à jour par le calcul d'inversion

- **zones** : zone permeability, and min/max permeability values tolerated during inversion process.

### Tables du modèle

Les tables suivantes définissent le maillage et les caractéristiques du modèle hydrogéologique :

- **mailles** :

Cette table définit chaque maille du maillage, par une liste ordonnée de trois (triangle) ou quatre (quadrangle) numéros de noeuds, ainsi que les champs :

  - surface: surface de la maille ($m^{2}$),
  - altitude: altitude topographique (m),
  - altitude_mur: altitude du mur de l'aquifère (m),
  - infiltration: valeur de l'infiltration (m/s),
  - epaisseur_zns: épaisseur de la zone non saturée (m),
  - potentiel_reference: niveau piézométrique de référence (m, donnée facultative),
  - potentiel: niveau piézométrique m),
  - flux_eau: terme de suintement (m/s),
  - epaisseur_zs: épaisseur de la nappe (zone saturée) (m),
  - epaisseur_zns: épaisseur de la zone non saturée (m),
  - permeabilite_zns: perméabilité de la zone non saturée (m/s),
  - permeabilite_x: perméabilité selon x (m/s),
  - permeabilite_y: perméabilité selon y (m/s),
  - permeabilite_z: perméabilité selon z (m/s) (inutilisé),
  - v_x: vitesse de Darcy selon x (m/s),
  - v_y: vitesse de Darcy selon y (m/s),
  - v_z: vitesse de Darcy selon z (m/s) (inutilisé),
  - v_norme: norme de la vitesse de Darcy (m/s),

- **noeuds** : cette table contient les mêmes champs que la table **mailles**, mais calculés en chaque noeud.

- **noeuds_contour** : table contenant les numéros des noeuds du contour.

- **parametres** : table définissant les paramètres associés au modèle hydrogéologiques (cf. {ref}`Tables caractérisant les simulations de référence<ref_simulation_table_fr>`).

- **potentiel_impose** : table indiquant les numéros des noeuds à potentiel imposé et la valeur de ce potentiel.

----

## Base de calcul

Pour chaque projet, une base de calcul est créée de la manière suivante :

- par copie de la base du modèle spécifié (depuis le répertoire mesh),
- par création des tables de la base sites.sqlite et copie des lignes faisant référence au site considéré,
- par création des tables de résultats.

La base de calcul ainsi créée contient donc les tables suivantes :

- en provenance de la base de modèle :

  - bords
  - contours
  - domaines
  - extremites
  - hynverse_parametres
  - mailles
  - metis_hynverse_options
  - noeuds
  - noeuds_contour
  - openfoam_hynverse_options
  - parametres
  - points_fixes
  - points_pilote
  - potentiel_impose
  - zones

- en provenance de la base sites pour le site considéré :

  - dates_resultats
  - elements_chimiques
  - forages
  - fracturations
  - injections
  - lithologies
  - mesures
  - metis_options
  - openfoam_options
  - points_interet
  - simulations
  - sites
  - stratigraphies

- par création des tables de résultats :

  - bilan_masse
  - dates_simulation
  - isovaleur
  - mailles_zns
  - noeuds_zns
  - noeuds_injections
  - parametres_simulation
