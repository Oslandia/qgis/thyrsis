"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.core import QgsProcessingAlgorithm, QgsProcessingParameterFile
from qgis.PyQt.QtCore import QCoreApplication

from ..database import sqlite
from ..database.load_chemicals import load_chemicals
from ..log import logger
from ..settings import Settings


class LoadChemicalsAlgorithm(QgsProcessingAlgorithm):
    INPUT_CSV = "INPUT_CSV"
    INPUT_SITE = "INPUT_SITE"

    def tr(self, string):
        return QCoreApplication.translate(
            "Imports the characteristics of new chemical elements from a .csv file", string
        )

    def createInstance(self):
        return LoadChemicalsAlgorithm()

    def name(self):
        return "load_chemicals"

    def displayName(self):
        return self.tr("Load chemicals")

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def shortHelpString(self):
        return self.tr("Imports the characteristics of new chemical elements from a .csv file")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_CSV,
                self.tr("Enter .csv file to import"),
                QgsProcessingParameterFile.File,
                "csv",
            )
        )

        sites_db = "sites.sqlite"
        if Settings().contains("General", "defaultSite"):
            sites_db = str(Settings().value("General", "defaultSite"))

        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_SITE,
                self.tr("Enter sites.sqlite database location"),
                QgsProcessingParameterFile.File,
                "sqlite",
                defaultValue=sites_db,
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        input_csv_file = self.parameterAsFile(parameters, self.INPUT_CSV, context)
        input_site_file = self.parameterAsFile(parameters, self.INPUT_SITE, context)

        logger.enable_console(True)
        logger.set_processing_feedback(feedback)

        conn = sqlite.connect(input_site_file)

        load_chemicals(conn.cursor(), input_csv_file, False)
        conn.commit()

        logger.disable_processing_feedback()

        return {}
