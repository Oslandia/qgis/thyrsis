import datetime
import os
import tempfile
from builtins import range, str
from subprocess import PIPE, Popen

from qgis.PyQt.QtCore import QObject, Qt, QTimer, QUrl, pyqtSignal
from qgis.PyQt.QtGui import QDesktopServices
from qgis.PyQt.QtWidgets import QMenu, QTableWidget, QTableWidgetItem

from ..log import logger


class Task(QObject):
    """Task for subprocess hynverse"""

    finished = pyqtSignal(str)

    def __init__(
        self,
        name,
        cmd,
        refresh_delay_ms=1000,
        logfile=None,
        env=os.environ.copy(),
        parent=None,
    ):
        """Constructor

        :param name: task name
        :type name: string
        :param cmd: cmd to launch
        :type cmd: list
        :param refresh_delay_ms: refresh time for ZNS dialog
        :type refresh_delay_ms: int
        :param logfile: logfile path
        :type logfile: string
        :param env: environment variables
        :type env: list
        :param parent: widget parent
        :type parent: QWidget
        """
        super(Task, self).__init__(parent)
        if logfile is None:
            f, logfile = logfile or tempfile.mkstemp()
        else:
            f = open(logfile, "a")

        self.name = name
        self.setObjectName(name)
        self.cmd = cmd
        self.logfile = logfile
        self.__log = open(logfile, errors="replace")
        self.start = datetime.datetime.now().strftime("%d/%m %H:%M:%S")
        self.end = None
        self.__timer = QTimer(self)

        self.__process = None
        self.__process = Popen(cmd, stdin=PIPE, stdout=f, stderr=f, env=env)
        self.__process.stdin.close()
        self.pid = str(self.__process.pid)
        logger.debug(
            "started {} {} {}".format(self.name, self.pid, self.__process.poll())
        )

        self.__timer.start(refresh_delay_ms)
        self.__timer.timeout.connect(self.__checkProcess)

    def __del__(self):
        """Stop task"""
        self.terminate()

    def __checkProcess(self):
        """Loop to check progress on task"""
        while True:
            l = self.__log.readline()
            if not l:
                break
            logger.task_log(l.rstrip())

        if self.__process and (self.__process.poll() is not None):
            logger.debug(
                "finished {} {} {}".format(self.name, self.pid, self.__process.poll())
            )
            self.__process = None
            self.__timer.stop()
            self.end = datetime.datetime.now().strftime("%d/%m %H:%M:%S")
            self.finished.emit(self.pid)
            self.__log.close()

    def terminate(self):
        """Stop the task"""
        logger.debug("terminate " + self.name)
        self.__timer.stop()
        if self.__process and (self.__process.poll() is None):
            logger.notice("killed task".format(self.__process.pid))
            self.end = datetime.datetime.now().strftime("%d/%m %H:%M:%S")
            self.__process.terminate()
            self.__process = None
            self.finished.emit(self.pid)
            self.__log.close()

    def isFinished(self):
        """Triggered at the end of the task, with a success state

        :return: success state
        :rtype: bool
        """
        return self.__process and self.__process.poll() is None


class TaskManager(QTableWidget):
    """Widget to display tasks"""

    COLUMS = ["Nom", "Commande", "Status", "Log", "Début", "Fin", "PID"]

    def __init__(self, parent=None):
        """Constructor

        :param parent: widget parent
        :type parent: QWidget
        """
        super(TaskManager, self).__init__(0, len(TaskManager.COLUMS), parent)
        self.setHorizontalHeaderLabels(TaskManager.COLUMS)
        self.resize(800, 200)
        self.__tasks = {}

    def addTask(
        self,
        name,
        cmd_str,
        cmd,
        env=os.environ.copy(),
        refresh_delay_ms=1000,
        logfile=None,
    ):
        """add a new task

        :param name: task name
        :type name: string
        :param cmd: cmd to launch
        :type cmd: str
        :param env: environment variables
        :type env: list
        :param refresh_delay_ms: refresh time for ZNS dialog
        :type refresh_delay_ms: int
        :param logfile: logfile path
        :type logfile: string

        :return: task
        :rtype: Task
        """
        if name in self.__tasks:
            logger.error(
                "erreur: une tache du même nom ({}) est déjà en cours".format(name)
            )
            return

        r = self.rowCount()
        self.setRowCount(r + 1)
        self.setItem(r, 0, QTableWidgetItem(name))
        self.setItem(r, 1, QTableWidgetItem(cmd_str))

        try:
            logger.debug()
            task = Task(
                name,
                cmd,
                logfile=logfile,
                refresh_delay_ms=refresh_delay_ms,
                env=env,
                parent=self,
            )
            self.__tasks[task.pid] = task
            task.finished.connect(self.__taskFinished)
            self.setItem(r, 2, QTableWidgetItem("running"))
            self.setItem(r, 3, QTableWidgetItem(task.logfile))
            self.setItem(r, 4, QTableWidgetItem(task.start))
            self.setItem(r, 6, QTableWidgetItem(task.pid))
            return task
        except OSError as e:
            self.setItem(r, 2, QTableWidgetItem("launch error"))
            logger.error("erreur de démarrage de la commande '{}'".format(cmd_str))
            return None

    def mousePressEvent(self, event):
        """Triggered on mouse press

        :param event: event
        :type event: QEvent
        """
        QTableWidget.mousePressEvent(self, event)

        if event.button() == Qt.RightButton:
            contextMenu = QMenu("Actions", self)
            contextMenu.addAction("Arrêter la tâche").triggered.connect(
                self.__cancelTask
            )
            contextMenu.addAction("Supprimer la tâche").triggered.connect(
                self.__removeTask
            )
            contextMenu.addAction("Ouvrir le log").triggered.connect(self.__openLog)
            contextMenu.addAction("Suivre le log").triggered.connect(self.__followLog)
            contextMenu.exec_(self.mapToGlobal(event.pos()))

    def __taskFinished(self, pid):
        """Triggered after the end of a task

        :param pid: id of the finished task
        :type pid: string
        """
        for r in range(self.rowCount()):
            if self.item(r, 6):
                if self.item(r, 6).text() == pid:
                    if self.item(r, 2).text() == "running":
                        self.item(r, 2).setText("finished")
                    self.setItem(r, 5, QTableWidgetItem(self.__tasks[pid].end))
                    logger.debug("finished " + self.__tasks[pid].name)
                    del self.__tasks[pid]

    def __removeTask(self):
        """Remove the selected task"""
        self.__cancelTask()
        self.removeRow(self.currentRow())

    def __cancelTask(self):
        """cancel the selected task"""
        if self.item(self.currentRow(), 6):
            pid = self.item(self.currentRow(), 6).text()
            if pid in self.__tasks:
                self.__tasks[pid].terminate()
                self.item(self.currentRow(), 2).setText("cancelled")

    def __openLog(self):
        """Open the log file"""
        if self.item(self.currentRow(), 3):
            QDesktopServices.openUrl(QUrl(self.item(self.currentRow(), 3).text()))

    def __followLog(self):
        """Track log"""
        if self.item(self.currentRow(), 3):
            Popen(
                [
                    "xterm",
                    "-e",
                    "tail -f {}\n".format(self.item(self.currentRow(), 3).text()),
                ]
            ).wait()


if __name__ == "__main__":

    import sys

    from qgis.PyQt.QtWidgets import QApplication

    app = QApplication(sys.argv)

    logger.enable_console(True)

    tm = TaskManager()
    tm.show()
    tm.addTask("ls task", "ls")
    tm.addTask("ls task", "ls")
    tm.addTask("du task", "du /")
    tm.addTask("la task", "latey")
    tm.addTask(
        "hynverse",
        "python3 -u -m thyrsis.simulation.hynverse -v -r /home/vmo/cea_dam/saclay_2015_hynverse/saclay.sqlite",
    )
    exit(app.exec_())
