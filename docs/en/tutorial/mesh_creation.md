# Mesh creation

*{sub-ref}`wordcount-minutes` min read*

## Mesh project

The first step is to create a mesh project, which will be used for 2D horizontal groundwater flow and transport calculation.

If THYRSIS is activated, there must be a *Thyrsis* menu in the main QGIS toolbar. To create a new mesh project, click on : `Thyrsis > New > Mesh...` {numref}`tutorial_newmesh` :

:::{figure} /_static/images/tutorial_newmesh.png
:name: tutorial_newmesh
New mesh creation
:::

:::{note}
THYRSIS will use the current projection to create the mesh database. Change the projection in QGIS to be sure you are using the right projection. In this tutorial, all data are projected in EPSG: 27572 (Extended Lambert II)
:::

You must first fulfill the path where the mesh database will be saved. The `~/.thyrsis/mesh` directory is the right place to save the database for a later use by THYRSIS but another place can be chosen temporarily. The database must be named like `SITE_MODEL.mesh.sqlite`, where:

> - *SITE* is the site name - created and saved in the `sites.sqlite` database if it doesn't exist,
> - *MODEL* is the model name - a model is composed of a mesh and of several hydrogeological parameters and fields.

Here we will call it *SACLAY_TUTORIAL*, to indicate that it is a mesh model based on the SACLAY site and that it is the TUTORIAL mesh model. At the same time, THYRSIS will create a QGIS project (.qgs file). At the end your mesh directory should be like {numref}`tutorial_meshdirectory`.

:::{figure} /_static/images/tutorial_meshdirectory.png
:name: tutorial_meshdirectory
:scale: 50 %

Mesh directory
:::

A background layer is useful for locating the project environment.
For example, OpenStreetMap can be opened through the QGIS browser menu {numref}`tutorial_osmlayer`.

:::{figure} /_static/images/tutorial_osmlayer.png
:name: tutorial_osmlayer
QGIS canvas with OSM layer
:::

You can also add a river layer, which will be useful for delimiting the project area. For the tutorial, we provide a `river.gpkg` file. Click on *Layer>Add Layer>Add Vector Layer* and add the river layer {numref}`tutorial_addVectorLayer`.

:::{figure} /_static/images/tutorial_addVectorLayer.png
:name: tutorial_addVectorLayer
Add Vector Layer
:::

At last, two rasters are needed for the mesh project creation:

- a digital elevation model (DEM), named `mnt.tif`
- a "wall" raster of the aquifer substratum's altitude, named `mur.tif`

To add them, click on `Layer > Add Layer > Add Raster Layer`.

We recommend you to put the background layer and the raster layers below the other layers. You can also add some transparency in your DEM symbology properties. Your canvas should look like.

:::{figure} /_static/images/tutorial_mntlayer.png
:name: tutorial_mntlayer
QGIS canvas with DEM layer
:::

Don't forget to save your project (`CTRL + S` or click on `Project > Save`)

----

## Meshing

The next step is the 2D mesh creation.

The *contours* layer must be defined first. It is a layer of polylines with the followings attributes :

- *groupe* : group name of the contour,
- *nom* : contour name,
- *longueur_element* : element length, which defines the mesh density in the neighbourhood of the contour,
- *potentiel_impose* : 1 if the contour corresponds to an imposed potential boundary condition, 0 or NULL otherwise.

:::{warning}
To create the contours, enable the [snapping tools](https://docs.qgis.org/3.16/en/docs/user_manual/working_with_vector/editing_geometry_attributes.html#setting-the-snapping-tolerance-and-search-radius) of QGIS, and select the active layer option. It is necessary to create true topological layer with closed contours.
:::

:::{figure} /_static/images/tutorial_snapping.png
:name: tutorial_snapping
Enabling snapping
:::

Then select the contours layer, click on the editing button,

:::{figure} /_static/images/tutorial_editing.png
:name: tutorial_editing
Editing contours layer
:::

and activate the *Add Line Feature* ({numref}`tutorial_addLineFeature`).

:::{figure} /_static/images/tutorial_addLineFeature.png
:name: tutorial_addLineFeature
Adding line feature on the contours layer
:::

For the tutorial, we want to create a mesh for the Plateau de Saclay. We recommand you to create 4 contours :

- a north contour on the river,
- an east contour between the two rivers,
- a south contour on the river,
- a west contour perpendicular to the north and south rivers.

All contours must be included within the DEM and wall raster extents. For each validated contour, the windows of {numref}`tutorial_contoursAttributes` opens.

:::{figure} /_static/images/tutorial_contoursAttributes.png
:name: tutorial_contoursAttributes
*Contours* layer's attributes
:::

The field *longueur_element* must be filled and the field *potentiel_impose* must be set to 1 if the contour corresponds to an imposed potential boundary condition.

Nested contours can be created in order to define several meshing zones.

Because it is not possible to use snapping on itself (QGIS limitation), a closed contour must be created by defining first an almost closed contour, then using snapping and the QGIS node tool to move the last vertex on the first one.

Save the contours layer. THYRSIS will populate the *domaines* layer with polygons obtained from the contours layer.

If no polygon is created in the *domaines* layer, there is probably a topological error with non closing contours.

Your canvas should look like {numref}`tutorial_contoursCanvas`, with similar contours attributes.

:::{figure} /_static/images/tutorial_contoursCanvas.png
:name: tutorial_contoursCanvas
QGIS canvas after validating the *contours* layer
:::

Finally use the *Mesh* button ({numref}`tutorial_meshButton`) in the THYRSIS toolbar to create the mesh.

:::{figure} /_static/images/tutorial_meshButton.png
:name: tutorial_meshButton
Mesh button
:::

The window of {numref}`tutorial_meshDialog` opens.

:::{figure} /_static/images/tutorial_meshDialog.png
:name: tutorial_meshDialog
Mesh dialog
:::

Verify the layers and launch the mesh creation. The *noeuds* and *mailles* layers will be updated and the *altitude* and *altitude_mur* layers created.

By checking the "Potential" box and specifying a raster layer of type "potential.tif", a new layer *potentiel_reference* is created and the corresponding field in the *noeuds* and *mailles* tables is calculated. This layer can then be used in the inversion procedure to create new pilot points.

You should obtain the QGIS canvas of {numref}`tutorial_meshResult`.

:::{figure} /_static/images/tutorial_meshResult.png
:name: tutorial_meshResult
QGIS canvas with mesh result
:::
