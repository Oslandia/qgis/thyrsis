# coding = UTF-8

from qgis.PyQt.QtCore import pyqtSignal
from qgis.PyQt.QtWidgets import QComboBox, QHBoxLayout, QWidget


class ResultControl(QWidget):
    """Toolbar widget, used to define the current mesh display"""

    columnChanged = pyqtSignal(str)
    unitsChanged = pyqtSignal(str)

    def __init__(self, provider=None, parent=None):
        """Constructor

        :param provider: provider if defined
        :type provider: None or ZnsDataProvider
        :param parent: widget parent
        :type parent: QWidget
        """
        super(ResultControl, self).__init__(parent)

        layout = QHBoxLayout()

        self.__provider = None

        self.columnComboBox = QComboBox()
        self.columnComboBox.setMinimumWidth(150)
        layout.addWidget(self.columnComboBox)
        self.unitsComboBox = QComboBox()
        layout.addWidget(self.unitsComboBox)
        self.setLayout(layout)

        self.setProvider(provider)

        self.columnComboBox.currentIndexChanged.connect(self.__columnChanged)
        self.unitsComboBox.currentIndexChanged.connect(self.__unitsChanged)

    def setProvider(self, provider):
        """Set the provider, to distinguish ZNS provider and mesh provider"""
        self.columnComboBox.clear()
        self.unitsComboBox.clear()
        self.__provider = provider
        if provider is not None:
            for text, column in self.__provider.availableColumns():
                self.columnComboBox.addItem(text, column)
            column = self.__provider.resultColumn()
            self.columnComboBox.setCurrentIndex(self.columnComboBox.findData(column))
            self.__setupUnits()

    def __setupUnits(self):
        """Add units to combobox and display the provider units"""
        if self.__provider is not None:
            for units in self.__provider.availableUnits():
                self.unitsComboBox.addItem(units)
            self.unitsComboBox.setCurrentIndex(
                self.unitsComboBox.findText(self.__provider.units())
            )

    def __columnChanged(self, idx=None):
        """Triggered when columns data changes

        :param idx: idx of the combo box item
        :type idx: integer
        """
        if self.__provider is not None:
            self.__provider.setResultColumn(
                self.columnComboBox.itemData(self.columnComboBox.currentIndex())
            )
            self.unitsComboBox.blockSignals(True)
            self.unitsComboBox.clear()
            self.__setupUnits()
            self.unitsComboBox.blockSignals(False)

    def __unitsChanged(self, idx=None):
        """Triggered when columns data changes

        :param idx: idx of the combo box item
        :type idx: integer
        """
        if self.__provider is not None:
            self.__provider.setUnits(self.unitsComboBox.currentText())


if __name__ == "__main__":
    import sys

    from qgis.PyQt.QtWidgets import QApplication, QDialog

    from thyrsis.spatialitemeshdataprovider import SpatialiteMeshDataProvider

    QApplication.setOrganizationName("QGIS")
    QApplication.setOrganizationDomain("qgis.org")
    QApplication.setApplicationName("QGIS2")

    app = QApplication(sys.argv)

    dbname = sys.argv[1]

    provider = SpatialiteMeshDataProvider(
        "dbname=" + dbname + " resultColumn=concentration"
    )
    dlg = QDialog()
    w = ResultControl(provider, dlg)
    dlg.resize(400, 40)

    dlg.show()

    app.exec_()
