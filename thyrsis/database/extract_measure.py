"""
extract measures of a site database

USAGE

    python -m thyrsis.database.extract_measure site element_chimique unit
    python -m thyrsis.database.extract_measure site

    extract measures from the site database into csv file
"""
import io
from builtins import str

from ..log import logger
from ..utilities import unitConversionFactor


def extract_measure(conn, siteName, chemicalName=None, unitName=None):
    """Extract measure value saved in a database for a site. Output saved as site_chemical.csv

    :param conn: conn on a sqlite database
    :type conn: sqlite3.Connection
    :param siteName: name of the site
    :type siteName: string
    :param chemicalName: chemical name to extract
    :type chemicalName: string
    :param unitName: name of the unit format to use
    :type unitName: string

    :return: None
    """
    if unitName:
        if isinstance(unitName, str):
            logger.debug("unit unicode")
        else:
            logger.debug("unit not unicode")
        logger.notice("unit", unitName)

    cur = conn.cursor()

    sid = cur.execute("SELECT id FROM sites WHERE nom='%s'" % (siteName)).fetchone()
    if not sid:
        logger.error("site", siteName, " introuvable dans ", sitesdb)
        for (nom,) in cur.execute("SELECT nom FROM sites").fetchall():
            logger.debug("    ", nom)
        exit(1)
    sid = sid[0]
    logger.notice("site", siteName, "found with id", sid)

    eid = None
    specificActivity = 0.0
    if chemicalName:
        eid = cur.execute(
            "SELECT id FROM elements_chimiques WHERE nom='%s'" % (chemicalName)
        ).fetchone()
        if not eid:
            logger.error("element chimique", chemicalName, "introuvable dans ", sitesdb)
            exit(1)
        eid = eid[0]
        specificActivity = cur.execute(
            "SELECT activite_specifique FROM elements_chimiques WHERE id='%d'" % (eid)
        ).fetchone()[0]
        logger.notice("chemical element", chemicalName, "found with id", eid)

    fileName = siteName + "_"

    points = cur.execute(
        "SELECT OGC_FID, nom, X(GEOMETRY), Y(GEOMETRY) FROM points_interet WHERE sid=%d"
        % (sid)
    ).fetchall()
    pid = [x[0] for x in points]
    npoints = {}
    xpoints = {}
    ypoints = {}
    for p in points:
        npoints[p[0]] = p[1]
        xpoints[p[0]] = p[2]
        ypoints[p[0]] = p[3]

    if not pid:
        logger.error("no points for site ", siteName)
        exit(1)

    if chemicalName:
        fileName += chemicalName + ".csv"
        tup = cur.execute(
            "SELECT * FROM mesures WHERE eid={} AND pid IN ({seq})".format(
                eid, seq=",".join(["?"] * len(pid))
            ),
            pid,
        ).fetchall()
        uConversionFactor = (
            unitConversionFactor("kg/m³", unitName, specificActivity)
            if unitName
            else 1.0
        )
        unit = unitName if unitName else "kg/m³"
        logger.notice("unit conversion factor =", uConversionFactor, "for unit", unit)
    else:
        fileName += "potentiel.csv"
        tup = cur.execute(
            "SELECT * FROM mesures WHERE eid IS NULL AND pid IN ({seq})".format(
                seq=",".join(["?"] * len(pid))
            ),
            pid,
        ).fetchall()

    with io.open(fileName, "w", encoding="utf-8") as fil:
        fil.write(
            "{:20s}{:>12s}{:>12s} {:12s}{:20s}{:10s}{:>15s}{:>15s} {:10s}\n".format(
                "Nom",
                "X",
                "Y",
                "Date",
                "Type",
                "Element",
                "Concentration",
                "Incertitude",
                "Unite",
            )
        )
        for line in tup:
            pid = line[1]
            if chemicalName:
                value = float(line[5]) * uConversionFactor
                incert = float(line[6]) * uConversionFactor if line[6] else None
                if incert:
                    fil.write(
                        "{:20s}{:12.2f}{:12.2f} {:12s}{:20s}{:10s}{:15.7e}{:15.7e} {:10s}\n".format(
                            npoints[pid],
                            xpoints[pid],
                            ypoints[pid],
                            line[2],
                            line[3],
                            chemicalName,
                            value,
                            incert,
                            unit,
                        )
                    )
                else:
                    fil.write(
                        "{:20s}{:12.2f}{:12.2f} {:12s}{:20s}{:10s}{:15.7e}{:>15s} {:10s}\n".format(
                            npoints[pid],
                            xpoints[pid],
                            ypoints[pid],
                            line[2],
                            line[3],
                            chemicalName,
                            value,
                            "-",
                            unit,
                        )
                    )


if __name__ == "__main__":
    import sys

    from ..database import sqlite
    from ..settings import Settings

    logger.enable_console(True)

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    if len(sys.argv) < 3:
        logger.error("wrong number of arguments (try database.py --help)")
        exit(1)

    if not Settings().contains("General", "defaultSite"):
        logger.error("defaultSite undefined, use preferences_dialog.py")
        exit(1)

    sitesdb = str(Settings().value("General", "defaultSite"))

    conn = sqlite.connect(sitesdb)

    extract_measure(
        conn,
        sys.argv[1],
        sys.argv[2] if len(sys.argv) >= 3 else None,
        sys.argv[3].decode(sys.getfilesystemencoding()) if len(sys.argv) == 4 else None,
    )
