import numpy
from PyQt5.QtCore import pyqtSignal
from qgis.core import QgsCoordinateReferenceSystem, QgsDataProvider, QgsDataSourceUri


class MeshDataProvider(QgsDataProvider):
    """base class for mesh data providers, please note that this class
    is called in a multithreaded context"""

    PROVIDER_KEY = "mesh_provider"

    dataChanged = pyqtSignal()
    xmlLoaded = pyqtSignal()

    @classmethod
    def providerKey(cls):
        """Returns the mesh provider key"""
        return "mesh_provider"

    @classmethod
    def description(cls):
        """Returns the mesh provider description"""
        return "Mesh Provider"

    @classmethod
    def createProvider(cls, uri, providerOptions, flags=QgsDataProvider.ReadFlags()):
        return MeshDataProvider(uri, providerOptions, flags)

    def __init__(
        self, uri="", providerOptions=QgsDataProvider.ProviderOptions(), flags=None
    ):
        """Constructor

        :param uri: source
        :type uri: string
        """
        self.__uri = QgsDataSourceUri(uri)
        self._provider_options = providerOptions
        QgsDataProvider.__init__(self, uri)
        self.__didx = 0
        self.__dates = []

    def name(self):
        """Return data provider name attribute

        :return: name
        :rtype: string
        """
        return MeshDataProvider.PROVIDER_KEY

    def crs(self):
        """Return crs attribute

        :return: crs
        :rtype: QgsCoordinateReferenceSystem
        """
        assert self.isValid()
        return QgsCoordinateReferenceSystem(self.__uri.param("crs"))

    def setCrs(self, coordRef):
        """Set Qgs"""
        epsg = coordRef.srsid()
        self.__uri.setParam("crs", str(epsg))

    def isValid(self):
        """Return true if data provider is valid

        :return: valid
        :rtype: bool
        """
        return self.__uri.hasParam("crs")

    def nodeCoord(self):
        """return a list of coordinates

        :return: return a list of coordinates
        :rtype: ndarray
        """
        return numpy.empty((0, 3), dtype=numpy.float64)

    def triangles(self):
        """return a list of triangles described by node indices,
        watch out: indices start at zero

        :return: return a list of coordinates
        :rtype: ndarray
        """
        return numpy.empty((0, 3), dtype=numpy.int32)

    def setDates(self, dates):
        """set list of dates in case node values vary with time

        :param dates: dates list
        :type dates: list
        """
        self.__dates = dates

    def dates(self):
        """return a list of dates in case node values vary with time

        :return: dates list
        :rtype: list
        """
        return self.__dates

    def setDate(self, didx):
        """Set current date index

        :param didx: date index
        :type didx: integer
        """
        self.__didx = didx
        self.dataChanged.emit()

    def date(self):
        """return a current date

        :return: dates list
        :rtype: list
        """
        return self.__didx

    def valueAtElement(self):
        """return if mesh is elements based (True) or nodes based (False)

        :return: flag
        :rtype: bool
        """
        return False

    def nodeValues(self):
        """return values at nodes

        :return: values at nodes
        :rtype: ndarray
        """
        return numpy.empty((0,), dtype=numpy.float32)

    def elementValues(self):
        """return values at elements

        :return: values at nodes
        :rtype: ndarray
        """
        return numpy.empty((0,), dtype=numpy.float32)

    def dataSourceUri(self, expandAuthConfig=False):
        """Return data provider uri

        :return: uri
        :rtype: string
        """
        return self.__uri.uri()

    def uri(self):
        """Return data provider name attribute

        :return: uri
        :rtype: QgsDataSourceUri
        """
        return self.__uri

    def readXml(self, node, rwcontext):
        """Function to read mesh legend information from project

        :param node: base element of the xml
        :type node: QDomElement
        :param rwcontext: QgsReadWriteContext
        :type rwcontext: QgsReadWriteContext
        """
        element = node.toElement()
        self.__uri = QgsDataSourceUri(element.attribute("uri"))
        self.__didx = int(element.attribute("dateIndex"))
        self.xmlLoaded.emit()
        return True

    def writeXml(self, node, doc, rwcontext):
        """Function to write mesh legend information from project

        :param node: base element of the xml
        :type node: QDomElement
        :param doc: document
        :type doc: QDomDocument
        :param rwcontext: QgsReadWriteContext
        :type rwcontext: QgsReadWriteContext
        """
        element = node.toElement()
        element.setAttribute("name", self.name())
        element.setAttribute("uri", self.dataSourceUri())
        element.setAttribute("dateIndex", self.__didx)
        return True
