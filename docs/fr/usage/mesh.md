---
substitutions:
  img_inversion: |-
    :::{image} /_static/images/inverse.png
    :scale: 10 %
    :::
  img_maillage: |-
    :::{image} /_static/images/mesh.png
    :scale: 10 %
    :::
  img_warning: |-
    :::{image} /_static/images/warning.png
    :scale: 80 %
    :::
---

(model_building_fr)=
# Construction de modèle

Un modèle hydrogéologique complet peut être construit avec THYRSIS, en deux étapes :

- définition d'un domaine d'étude et maillage à l'aide du mailleur [GMSH](http://gmsh.info/) {cite}`geuzaine-remacle:2009`,
- définition ou calcul d'un champ de perméabilité et calcul de l'ensemble des champs paramètriques du modèle

## Maillage

### Principe

La création d’un maillage est fondée sur l’utilisation du logiciel GMSH {cite}`geuzaine-remacle:2009`. Elle consiste à définir les contours du domaine et des sous-domaines à modéliser, en indiquant pour chacun la taille caractéristique de la maille. Il est possible de spécifier des lignes et des points fixes, i.e. des éléments à intégrer dans le maillage.

Pour chaque élément de contour, il est possible de préciser s’il constitue une limite à potentiel imposé, de manière à identifier les noeuds du maillage correspondant à ces conditions aux limites. Par défaut, la valeur de potentiel imposé est prise égale à la valeur de l’altitude topographique (MNT), ce qui correspond à des affleurements ou des rivières.

La construction d'un maillage nécessite les deux couches raster :

- MNT, modèle numérique de terrain,
- altitude du mur de l'aquifère,"

Une fois le maillage réalisé, il est nécessaire de renseigner les différents champs des tables noeuds et mailles afin de définir un modèle hydrogéologique. Cela peut être fait manuellement ou par la procédure d'inversion décrite au chapitre suivant.

### Mise en oeuvre

#### Création d'un nouveau projet de maillage

Le projet maillage sera créé selon la projection géographique courante, visible dans le coin inférieur droit de l'interface QGIS ({numref}`qgis_srid`).

:::{figure} /_static/images/qgis_srid.png
:name: qgis_srid
QGIS SRID
:::

La projection souhaitée doit être définie avant la création du projet.

Pour créer un nouveau maillage, utiliser le menu *Thyrsis>Nouveau>Maillage...* ({numref}`mesh_creation`).

:::{figure} /_static/images/mesh_creation.png
:name: mesh_creation
:scale: 150 %

New mesh creation
:::

Puis entrer le nom du projet maillage, e.g. SACLAY_TEST. Le nom du fichier doit :

- commencer par le nom du site ( {{ img_warning }} Attention à la casse),
- être suivi d'un complément de nom commençant par `_` et désignant le nouveau modèle.

Une base de données Spatialite **SACLAY_TEST.mesh.sqlite** est créée, associée à un projet `SACLAY_TEST.mesh.qgs`, qui est ouvert dans QGIS ({numref}`mesh_NewProject`).

:::{figure} /_static/images/mesh_NewProject.png
:name: mesh_NewProject
Projet maillage nouvellement créé
:::

#### Définition des couches MNT et MUR

Il est impératif de disposer d’une couche MNT et d’une couche MUR, de préférences sous la forme de fichiers mnt.tif et mur.tif respectivement. Ces couches doivent être ouvertes dans le projet QGIS ({numref}`mesh_layerOpening`).

Il est possible d'utiliser le script python xyz_to_tif pour transformer un fichier file.xyz au format (x,y,z) en un fichier file.tif au format tif :

```bash
python -m thyrsis.utilities.xyz_to_tif file.xyz`
```

Ajouter autant de couches vecteur et raster utiles à la définition du maillage (e.g. orthophoto, bâti, contour du site).

:::{figure} /_static/images/mesh_layerOpening.png
:name: mesh_layerOpening
Ouverture des couches raster MNT et MUR.
:::

Zoomer sur l'emprise d'une des couches raster ({numref}`mesh_mntLayer`).

:::{figure} /_static/images/mesh_mntLayer.png
:name: mesh_mntLayer
Couche MNT ouverte dans QGIS, après zoom sur la couche.
:::

#### Création des éléments de contour

Afin de définir les contours du modèle, il peut être utile d'importer une couche de contours déjà existante ({numref}`mesh_contoursAdding`). Les contours peuvent alors être définis directement dans QGIS.

:::{figure} /_static/images/mesh_contoursAdding.png
:name: mesh_contoursAdding
Ajout d'une couche contour contenant les contours du site et du domaine à mailler (s'ils existent).
:::

Sélectionner la couche *contours* et configurer l'accrochage aux sommets dans le menu Préférences de QGIS ({numref}`mesh_snappingSelection` et {numref}`mesh_snappingOptions`).

:::{figure} /_static/images/mesh_snappingSelection.png
:name: mesh_snappingSelection
Sélection du menu des options d'accrochage
:::

:::{figure} /_static/images/mesh_snappingOptions.png
:name: mesh_snappingOptions
Menu des options d'accrochage. Accrochage aux sommets avec un précision de 10 pixels. L'édition topologique permet d'améliorer l’édition et le maintien des limites communes dans les couches de polygones
:::

Passer la couche *contours* en mode édition, puis ajouter des contours, soit par numérisation directe, soit par copie de contours existants :

> - Des contours existants peuvent être copiés dans la couche *contours* par simple "copier-coller"
> - Des morceaux de contour peuvent être définis en sélectionnant un contour et en le coupant avec l'outil "Éditer > Séparer les entités",
> - Des morceaux de contour peuvent être fusionnés en les sélectionnant et en utilisant l'outil "Éditer > Fusionner les entités sélectionnées".

Il est impératif de définir une longeur d’élement sur chaque élément du contour, en ouvrant la table d’attribut de la couche *contours*. Si l’élément est à potentiel imposé, spécifier la valeur 1. Dans ce cas, le potentiel sera déduit de la couche MNT ({numref}`mesh_contoursAttribute`).

Lorsque tous les éléments du contour sont définis, sortir du mode édition et enregistrer les modifications.

:::{figure} /_static/images/mesh_contoursAttribute.png
:name: mesh_contoursAttribute
Spécifier pour chaque élément du contour, sa longueur (en mètres) et s'il s'agit d'un élément à potentiel imposé (=1).
:::

Après enregistrement des modifications, les couches *domaines* et "*extremites* sont automatiquement mise à jour. Elles permettent de vérifier la cohérence topologique du réseau de contour ({numref}`mesh_domain`).

:::{figure} /_static/images/mesh_domain.png
:name: mesh_domain
Surface après l'enregistrement de la couche *contours* définissant un domaine fermé.
:::

:::{note}
- Une extrémité au milieu d'un contour est souvent liée à un accrochage mal configuré,
- QGIS ne permet pas l’accrochage d’une entité sur elle même lors de la création de l’entité. Pour définir un contour fermé d’un seul tenant, créer un contour presque fermé, renseigner la longueur élémentaire puis utiliser l’outil de noeud pour déplacer le dernier point sur le premier (avec accrochage),
- Les contours ouverts à l'intérieur d'un domaine sont acceptés et considérés comme des lignes fixes du maillage.
:::

#### Définition des points fixes

Il est aussi possible de spécifier des points fixes qui seront des noeuds du maillage. Le maillage au voisinage des points fixes est spécifié par l'attribut *longueur_element*. Les points fixes peuvent être copiés d'une autre couche par copier-coller ({numref}`mesh_fixedPoints`).

{{ img_warning }} Attention: la longeur des éléments est requise pour tous les points fixes.

:::{figure} /_static/images/mesh_fixedPoints.png
:name: mesh_fixedPoints
Ajout de points fixes.
:::

#### Génération du maillage

Une fois les domaines et les points fixes définis, cliquer sur l'icône {{ img_maillage }} pour générer le maillage ({numref}`mesh_meshDialog`). Dans la fenêtre de dialogue, choisir les couches correspondant au terrain et au mur, par défaut les couches raster mnt et mur. Si elle est disponible, spécifier la couche potentiel. THYRSIS va générer plusieurs couches ({numref}`mesh_meshResult`) :

- *maillage*, avec les triangles correspondant au maillage,
- *noeuds*, avec les noeuds du maillage,
- *altitude*, couche donnant l'altitude aux noeuds du maillage, selon la couche mnt,
- *altitude_mur*, couche donnant l'altitude du mur aux noeuds du maillage, selon la couche mur.
- *potentiel_reference*, donnant la valeur du potentiel au noeuds du maillage, si la couche potentiel a été spécifiée.

:::{figure} /_static/images/mesh_meshDialog.png
:name: mesh_meshDialog
Fenêtre de dialogue pour la construction du maillage, avec sélection des couches mnt, mur et potentiel.
:::

:::{figure} /_static/images/mesh_meshResult.png
:name: mesh_meshResult
Maillage final avec transparence.
:::

## Définition des paramètres

La définition du modèle commence en renseignant les valeurs des paramètres scalaires de la table **parametres** ({numref}`mesh_parametersTable`).

:::{figure} /_static/images/parametersTable.png
:name: mesh_parametersTable
Table parametres à renseigner.
:::

Puis le champ de perméabilité doit être défini en cliquant sur le bouton {{ img_inversion }} qui propose 3 choix ({numref}`mesh_modelDialog`) :

- *Créer un modèle avec une valeur constante de perméabilité* : cela crée un modèle hydrogéologique fondé sur un champ de perméabilité uniforme, dont la valeur doit être renseignée dans le dialogue,
- *Créer un modèle à partir d'un champ de perméabilité donné* : cela crée un modèle hydrogéologique fondé sur un champ de perméabilité fourni sous la forme d'un fichier externe .tif, qui doit être sélectionné dans le dialogue,
- *Créer un modèle par inversion* : cela lance le processus d'inversion, fondé sur des mesures de niveau piézométrique, {ref}`Inversion<inversion_fr>`.

:::{figure} /_static/images/modelDialog.png
:name: mesh_modelDialog
Dialogue du bouton Modèle
:::
