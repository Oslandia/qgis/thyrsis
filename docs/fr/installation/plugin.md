# Installer le plugin {{ title }}

## Version stable (recommandé)

Ce plugin est publié sur le dépôt officiel des plugins QGIS : <https://plugins.qgis.org/plugins/thyrsis/>.

Aller dans `Extensions` -> `Installer/Gérer les extensions`, chercher le plugin et l'installer.

----

## Version Beta

Activer les extensions expérimentales dans le panneau de configuration des plugins QGIS.

----

## Dernière version de développement

Si vous souhaitez tester les dernières fonctionnalités sans attendre la prochaine version, un paquet du plugin est automatiquement généré pour chaque commit sur la branche principale. Vous pouvez utiliser cette adresse en tant qu'adresse de dépôt dans le panneau de configuration des plugins de QGIS :

:::{warning}
Soyez prudent, cette version peut être instable. Ne pas l'utiliser dans un environnement de production.
:::

### Depuis un dépôt spécifique au plugin

Vous pouvez ajouter l'adresse suivante dans les dépôts d'extension QGIS, voir l'onglet `Paramètres` de la `fenêtre des Extensions` (void la [documentation officielle](https://docs.qgis.org/3.16/en/docs/user_manual/plugins/plugins.html#the-settings-tab)):

```url
https://oslandia.gitlab.io/qgis/thyrsis/plugins.xml
```

:::{note}
Cette adresse doit être accessible depuis votre réseau.
:::

### Télécharger le dernier artifact de l'intégration continue

Pour chaque commit sur la branche principale, un paquet du plugin est automatiquement généré et stocké en tant qu'artifact pendant un certain temps (par exemple, il est supprimé après quelques jours) :

1. Télécharger le [dernier artifact du builder](https://gitlab.com/Oslandia/qgis/thyrsis/-/jobs/artifacts/master/download?job=builder)
2. Extraire l'archive du plugin
3. L'installer depuis le menu `Installer depuis un ZIP` dans le gestionnaire de plugins de QGIS
