"""
Simple gui to define isovalues and their units
"""
import os
from builtins import range, str
from math import exp, log

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from qgis.PyQt import uic

from ..utilities import format_, unitConversionFactor


class IsovalueDialog(QDialog):
    """Dialog to configure the isovalue process"""

    def __init__(self, values, unit, scale, nbPixelsPerMeter=0):
        """Constructor

        :param values: saved value to load
        :type values: dict
        :param unit: unit
        :type unit: string
        :param scale: scale type (linear, log)
        :type scale: string
        :param nbPixelsPerMeter: used for image output
        :type nbPixelsPerMeter: int
        """
        super(IsovalueDialog, self).__init__()
        uic.loadUi(os.path.join(os.path.dirname(__file__), "isovalue.ui"), self)

        self.__values = values
        self.__unit = unit
        self.__minValue = min(values)
        self.__maxValue = max(values)
        self.__nbValues = len(values)
        self.__nbPixelsPerMeter = nbPixelsPerMeter
        self.__scale = scale
        self.nbPixelsPerMeter.setVisible(nbPixelsPerMeter != 0)
        self.labelPixelsPerMeter.setVisible(nbPixelsPerMeter != 0)

        fmt = format_(self.__minValue, self.__maxValue)
        self.minValue.setText(fmt % self.__minValue)
        self.maxValue.setText(fmt % self.__maxValue)
        self.nbValues.setText(str(self.__nbValues))
        self.nbPixelsPerMeter.setText(str(nbPixelsPerMeter))
        self.logCheckBox.setChecked(scale == "log")

        unitGroups = {
            "kg/m³": [
                "kg/L",
                "g/L",
                "mg/L",
                "µg/L",
                "kg/m³",
                "g/m³",
                "mg/m³",
                "µg/m³",
                "GBq/L",
                "MBq/L",
                "kBq/L",
                "Bq/L",
                "mBq/L",
                "µBq/L",
                "GBq/m³",
                "MBq/m³",
                "kBq/m³",
                "Bq/m³",
                "mBq/m³",
                "µBq/m³",
            ],
            "m": ["m"],
            "m/s": ["m/s", "m/an", "cm/j"],
        }

        for unitsGroup in list(unitGroups.values()):
            if unit in unitsGroup:
                self.unitsComboBox.addItems([u for u in unitsGroup])
                self.unitsComboBox.setCurrentIndex(unitsGroup.index(unit))

        self.listWidget.addItems([str(v) for v in values])
        for row in range(self.listWidget.count()):
            item = self.listWidget.item(row)
            item.setFlags(item.flags() | Qt.ItemIsEditable)

        self.__previous_unit = self.unitsComboBox.currentIndex()
        self.unitsComboBox.currentIndexChanged.connect(self.__unitChanged)

        self.refreshButton.clicked.connect(self.__refresh)
        self.minValue.textChanged.connect(self.__setMinValue)
        self.maxValue.textChanged.connect(self.__setMaxValue)
        self.nbValues.textChanged.connect(self.__setNbValues)
        self.nbPixelsPerMeter.textChanged.connect(self.__setNbPixelsPerMeter)
        self.logCheckBox.toggled.connect(self.__setScale)
        self.plusButton.clicked.connect(self.__add)
        self.minusButton.clicked.connect(self.__remove)

    def __setMinValue(self):
        """set min value"""
        self.__minValue = float(self.minValue.text())
        self.__refresh()

    def __setMaxValue(self):
        """set max value"""
        self.__maxValue = float(self.maxValue.text())
        self.__refresh()

    def __setNbValues(self):
        """set number of isovalues"""
        self.__nbValues = (
            int(self.nbValues.text()) if len(self.nbValues.text()) > 0 else 0
        )
        self.__refresh()

    def __setNbPixelsPerMeter(self):
        """set number of pixel per meter"""
        self.__nbPixelsPerMeter = float(self.nbPixelsPerMeter.text())

    def __setScale(self):
        """set scale type"""
        self.__scale = "log" if self.logCheckBox.isChecked() else "linear"
        self.__refresh()

    def __unitChanged(self, idx):
        """triggered if unit changed"""
        self.__unit = self.unitsComboBox.currentText()
        fct = unitConversionFactor(
            self.unitsComboBox.itemText(self.__previous_unit),
            self.unitsComboBox.currentText(),
        )
        self.__previous_unit = idx
        for row in range(self.listWidget.count()):
            self.listWidget.item(row).setText(
                str(float(self.listWidget.item(row).text()) * fct)
            )

    def __remove(self):
        """remove an isovalue from the list"""
        for item in self.listWidget.selectedItems():
            self.listWidget.takeItem(self.listWidget.row(item))

    def __add(self):
        """add an isovalue in the list"""
        item = QListWidgetItem("0")
        item.setFlags(item.flags() | Qt.ItemIsEditable)
        self.listWidget.addItem(item)
        self.listWidget.setCurrentItem(item)
        self.listWidget.editItem(item)

    def __refresh(self):
        """refrash all widget from the current values"""
        if self.__nbValues <= 1:
            return

        self.listWidget.clear()

        values = []
        for i in range(self.__nbValues):
            alpha = 1.0 - float(i) / (self.__nbValues - 1)
            # inverse of the scale function in shader
            value = (
                exp(
                    alpha * (log(self.__maxValue) - log(self.__minValue))
                    + log(self.__minValue)
                )
                if self.__scale == "log"
                else self.__minValue + alpha * (self.__maxValue - self.__minValue)
            )
            values.append(value)

        self.listWidget.addItems([str(v) for v in values])
        self.__values = [
            float(self.listWidget.item(row).text())
            for row in range(self.listWidget.count())
        ]

    def exec_(self):
        """return

        :return: tuple of 4 value (isovalues [list], unit [string], \
        scale type [string], nb pixel per metter [int])
        :rtype: tuple
        """
        return (
            (self.__values, self.__unit, self.__scale, self.__nbPixelsPerMeter)
            if QDialog.exec_(self)
            else (None, None, None, None)
        )


if __name__ == "__main__":

    import sys

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    app = QApplication(sys.argv)

    dlg = IsovalueDialog([2, 3.5, 1e-2, 1e-6], "kg/m³", "log", 0.1)
    values, units, scale, nbPixels = dlg.exec_()
    print(values, units, scale, nbPixels)
