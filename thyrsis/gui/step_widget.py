"""
Time step widget
"""

import os

from qgis.PyQt import uic
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QWidget


class StepWidget(QWidget):
    """Step widget in the crisis dialog"""

    def __init__(self, attr=None, parent=None):
        """Constructor

        :param attr: dictionnary of injection attributes
        :type attr: dict
        :param parent: widget parent
        :type parent: QWidget
        """
        super(StepWidget, self).__init__(parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "step.ui"), self)
        if isinstance(attr, dict):
            self.durationEdit.set_duration(attr["duree"])
            self.stepEdit.set_duration(attr["pas_de_temps"])
        else:
            self.durationEdit.set_duration("10 year")
            self.stepEdit.set_duration("1 year")

        self.deleteButton.setIcon(QIcon(":images/themes/default/mIconDelete.svg"))
        self.deleteButton.clicked.connect(self.deleteLater)

    def attributes(self):
        """return a dict of attributes

        :return: dict of step attributes
        :rtype: dict
        """
        return {
            "duree": self.durationEdit.get_duration(),
            "pas_de_temps": self.stepEdit.get_duration(),
        }
