PRAGMA foreign_keys = ON;;

CREATE TABLE dates_simulation(
    id INTEGER PRIMARY KEY,
    date DATETIME);;

CREATE TABLE noeuds_zns(
    OGC_FID INTEGER PRIMARY KEY,
    pid INTEGER REFERENCES injections(OGC_FID) ON DELETE CASCADE ON UPDATE CASCADE,
    surface REAL);;
SELECT AddGeometryColumn('noeuds_zns', 'GEOMETRY', SRID, 'POINT', 'XYZ');;
SELECT CreateSpatialIndex('noeuds_zns', 'GEOMETRY');;
CREATE INDEX noeuds_zns_pid_idx ON noeuds_zns(pid);;

CREATE TABLE mailles_zns(
    OGC_FID INTEGER PRIMARY KEY,
    pid INTEGER REFERENCES injections(OGC_FID) ON DELETE CASCADE ON UPDATE CASCADE,
    a INTEGER REFERENCES noeuds_zns(OGC_FID) ON DELETE CASCADE ON UPDATE CASCADE,
    b INTEGER REFERENCES noeuds_zns(OGC_FID) ON DELETE CASCADE ON UPDATE CASCADE);;
SELECT AddGeometryColumn('mailles_zns', 'GEOMETRY', SRID, 'LINESTRING', 'XYZ');;
SELECT CreateSpatialIndex('mailles_zns', 'GEOMETRY');;
CREATE INDEX mailles_zns_pid_idx ON mailles_zns(pid);;
CREATE INDEX mailles_zns_a_idx ON mailles_zns(a);;
CREATE INDEX mailles_zns_b_idx ON mailles_zns(b);;

CREATE TABLE bilan_masse(
    did INTEGER REFERENCES dates_simulation(id) ON DELETE CASCADE ON UPDATE CASCADE,
    sat REAL, -- venant de bmass.sat
    zns REAL, -- venant de bmass.insat
    out REAL -- venant de bmass.out
    );;
CREATE INDEX bilan_masse_did_idx ON bilan_masse(did);;

CREATE TABLE isovaleur(
    did INTEGER REFERENCES dates_simulation(id) ON DELETE CASCADE ON UPDATE CASCADE,
    valeur REAL,
    valeur_formate TEXT,
    type TEXT
    );;
SELECT AddGeometryColumn('isovaleur', 'GEOMETRY', SRID, 'MULTILINESTRING', 'XY');;
CREATE INDEX isovaleur_did_idx ON isovaleur(did);;
CREATE INDEX isovaleur_type_idx ON isovaleur(type);;
