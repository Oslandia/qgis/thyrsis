from builtins import range, str

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

# CI can't import matplotlib as it is headless
try:
    import matplotlib.pyplot as plt
    from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
    from matplotlib.backends.backend_qt5agg import (
        NavigationToolbar2QT as NavigationToolbar,
    )
    from matplotlib.figure import Figure
except:
    print("Ok for packaging, else please install sip")
import numpy as np
from scipy.stats import pearsonr

from ..database import sqlite
from ..log import logger
from ..meshlayer.utilities import format_
from ..utilities.lock import ExclusiveLock

LOCK_NAME = "hynverse"


class HynverseWindow(QMainWindow):
    """Hynverse panel displayed during the hynverse process"""

    def __init__(
        self, database, settings, qgis_canvas=None, layer=None, autoUpdateCheck=None, parent=None
    ):
        """Constructor of the panel

        :param database: database
        :type database: string
        :param settings: settings used by Thyrsis
        :type settings: Settings
        :param qgis_canvas: canvas of the qgis interface
        :type qgis_canvas: QgsMapCanvas
        :param layer: mesh layer
        :type layer: MeshLayer
        :param autoUpdateCheck: update scale
        :type autoUpdateCheck: bool
        :param parent: widget parent
        :type parent: QWidget

        :return: None
        :rtype: None
        """
        super(HynverseWindow, self).__init__(parent)

        self.layer = layer
        self.autoUpdateCheck = autoUpdateCheck
        self.qgis_canvas = qgis_canvas
        self.database = database
        self.__legendFontSize = int(settings.value("Matplotlib", "legendFontSize", 10))
        self.__ticksLabelSize = int(settings.value("Matplotlib", "ticksLabelSize", 10))
        self.__labelFontSize = int(settings.value("Matplotlib", "ylabelFontSize", 10))
        ax = []
        plt.rcParams["figure.constrained_layout.use"] = True
        for i in range(2):
            figure = Figure()
            canvas = FigureCanvas(figure)
            w = QDockWidget()
            w.setWidget(QMainWindow())
            w.widget().setCentralWidget(canvas)
            w.widget().addToolBar(NavigationToolbar(canvas, self))
            self.addDockWidget(Qt.RightDockWidgetArea, w)
            ax.append(canvas.figure.add_subplot(111))

        self._error_ax = ax[0]
        self._correlation_ax = ax[1]

        self._error_ax.set_xlabel("Itération", fontsize=self.__labelFontSize)
        self._error_ax.set_ylabel("Erreur (m)", fontsize=self.__labelFontSize)
        self._error_ax.tick_params(labelsize=self.__ticksLabelSize)
        self._error_ax.figure.canvas.draw()

        self._correlation_ax.set_xlabel(
            "Niveau piézométrique mesuré (m)",
            fontsize=self.__labelFontSize,
        )
        self._correlation_ax.set_ylabel(
            "Niveau piézométrique\ncalculé (m)",
            fontsize=self.__labelFontSize,
        )
        self._correlation_ax.tick_params(labelsize=self.__ticksLabelSize)
        self._correlation_ax.figure.canvas.draw()

        self.update()

    def reset(self, database):
        """set the database property

        :param database: database path
        :type database: string

        :return: None
        :rtype: None
        """
        self.database = database

    def update(self):
        """Update hynverse graphs

        :return: None
        :rtype: None
        """
        if self.autoUpdateCheck and self.autoUpdateCheck.isChecked() and self.layer:
            min_ = self.layer.dataProvider().minValue()
            max_ = self.layer.dataProvider().maxValue()
            fmt = format_(min_, max_)
            self.layer.colorLegend().setMinValue(fmt % min_)
            self.layer.colorLegend().setMaxValue(fmt % max_)

        with ExclusiveLock(LOCK_NAME):
            with sqlite.connect(self.database, "HynverseWindow:update") as conn:
                cur = conn.cursor()
                npp = cur.execute("SELECT COUNT(1) from points_pilote").fetchone()[0]
                if npp == 0:
                    logger.warning("aucun point pilote dans la base")
                    return
                nerr = cur.execute("SELECT COUNT(1) from hynverse_erreurs").fetchone()[
                    0
                ]
                if nerr == 0:
                    # logger.notice("aucune erreur dans la base")
                    return

                dict_hmes = {}
                dict_hcal = {}
                x = []
                y = []
                cur.execute(
                    "SELECT groupe, altitude_piezo_mesuree, altitude_piezo_calculee FROM points_pilote ORDER BY OGC_FID"
                )
                for groupe, hmes, hcal in cur.fetchall():
                    if groupe in list(dict_hmes.keys()):
                        dict_hmes[groupe].append(hmes)
                        dict_hcal[groupe].append(hcal if hcal else 0)
                    else:
                        dict_hmes[groupe] = [hmes]
                        dict_hcal[groupe] = [hcal if hcal else 0]
                    if hcal:
                        x.append(hcal)
                        y.append(hmes)

                coef_correlation = pearsonr(x, y)[0]

                errors = [
                    x[0]
                    for x in cur.execute(
                        "SELECT erreur from hynverse_erreurs ORDER BY OGC_FID"
                    ).fetchall()
                ]

        self._error_ax.clear()
        self._error_ax.set_xlabel("Itération", fontsize=self.__labelFontSize)
        self._error_ax.set_ylabel("Erreur (m)", fontsize=self.__labelFontSize)
        len_errors = len(errors)
        rge = range(0, len_errors, int(len_errors / 20) + 1)
        self._error_ax.set_xticks(list(rge))
        self._error_ax.set_xticklabels([str(i + 1) for i in rge])
        self._error_ax.tick_params(labelsize=self.__ticksLabelSize)

        self._error_ax.plot(list(range(len(errors))), errors, "k-+")
        self._error_ax.figure.tight_layout(rect=[0.05, 0.05, 1, 1])
        self._error_ax.figure.canvas.draw()

        self._correlation_ax.clear()
        self._correlation_ax.set_xlabel(
            "Niveau piézométrique mesuré (m)",
            fontsize=self.__labelFontSize,
        )
        self._correlation_ax.set_ylabel(
            "Niveau piézométrique\ncalculé (m)",
            fontsize=self.__labelFontSize,
        )
        self._correlation_ax.tick_params(labelsize=self.__ticksLabelSize)

        hmin = 1e4
        hmax = -hmin
        for g in dict_hmes.keys():
            hmin = min(hmin, min(dict_hmes[g]), min(dict_hcal[g]))
            hmax = max(hmax, max(dict_hmes[g]), max(dict_hcal[g]))
        hmin = hmin * 0.99
        hmax = hmax * 1.01
        self._correlation_ax.plot([hmin, hmax], [hmin, hmax], "k", label="y = x")
        self._correlation_ax.set_aspect("equal", "box")

        colors = plt.cm.Set1(np.linspace(0, 1, len(list(dict_hmes.keys()))))
        for i, g in reversed(list(enumerate(dict_hmes.keys()))):
            self._correlation_ax.plot(
                dict_hmes[g],
                dict_hcal[g],
                "+",
                color=colors[i],
                label="groupe " + str(g),
            )
            self._correlation_ax.text(
                0.9,
                0.1,
                "r = {:5.3f}".format(coef_correlation),
                horizontalalignment="right",
                verticalalignment="bottom",
                transform=self._correlation_ax.transAxes,
            )
            self._correlation_ax.legend(
                loc='lower left',
                bbox_to_anchor=(1, 0),
                fontsize=self.__legendFontSize
            )
        self._correlation_ax.figure.canvas.draw()

        if self.layer:
            with ExclusiveLock(LOCK_NAME):
                self.layer.dataProvider().update_values()

        if self.qgis_canvas:
            self.qgis_canvas.refresh()


if __name__ == "__main__":
    import sys

    qapp = QtWidgets.QApplication(sys.argv)
    app = HynverseWindow(sys.argv[1])
    app.show()
    qapp.exec_()
