"""
load measures in site database

USAGE

    - for concentration :
        python -m thyrsis.database.load_measure [-d] site element_chimique units concPoint.dat
    - for potential :
        python -m thyrsis.database.load_measure [-d] site potFile.dat

    try to load measure into the site database

    - site is the site name (str),
    - potPoint.dat is a file with measured potential at the point 'Point',
    - concPoint.dat is a file with measured concentration at the point 'Point',
    - chemical is a chemical element of the elements_chimiques table (str),
    - unit is the unit of the concentration,
    - -d is an option used to delete corresponding measured values

    potPoint.dat and concPoint.dat are blank-separated text files with format :
        dd/mm/YYYY[ HH:MM[:SS]] value[ incertitude]


"""
import os
from builtins import str
from datetime import datetime

from thyrsis.exception import MeasureLoadError

from ..log import logger
from ..utilities import unitConversionFactor


def load_measure(cur, siteName, fileName, chemicalName=None, units=None, delete=False):
    """load measures values into the current database for a site

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param siteName: name of the site
    :type siteName: string
    :param fileName: path to a csv file of measures
    :type fileName: string
    :param chemicalName: name of chemicals to delete if already existing
    :type chemicalName: string
    :param units: units of the measures
    :type units: string
    :param delete: flag to delete existing measure with the same name
    :type delete: bool

    :return: None
    """
    sid = cur.execute("SELECT id FROM sites WHERE nom='%s'" % (siteName)).fetchone()
    if not sid:
        error = f"site {siteName} introuvable dans {sitesdb}"
        logger.error(error)
        for (nom,) in cur.execute("SELECT nom FROM sites").fetchall():
            logger.debug("    ", nom)
        raise MeasureLoadError(error)
    sid = sid[0]

    valueType = (
        "conc"
        if os.path.basename(fileName)[:3] == "con"
        else os.path.basename(fileName)[:3]
    )

    # looks for a point with the same name in the site simulations
    pointName = os.path.basename(fileName).split(".")[0][len(valueType) :]
    point = None
    pid = cur.execute(
        "SELECT OGC_FID FROM points_interet \
            WHERE nom='%s' AND sid=%d"
        % (pointName, sid)
    ).fetchone()
    if not pid:
        error = f"no point {pointName} in table points_interet"
        logger.error("no point ", pointName, " in table points_interet")
        raise MeasureLoadError(error)
    pid = pid[0]

    eid = None
    specificActivity = 0.0
    if chemicalName:
        eid = cur.execute(
            "SELECT id FROM elements_chimiques WHERE nom='%s'" % (chemicalName)
        ).fetchone()
        if not eid:
            logger.error("element chimique", chemicalName, "introuvable dans ", sitesdb)
            exit(1)
        eid = eid[0]
        specificActivity = cur.execute(
            "SELECT activite_specifique FROM elements_chimiques WHERE id='%d'" % (eid)
        ).fetchone()[0]

    if delete:
        if eid:
            logger.notice("pid =", pid, " eid =", eid)
            logger.notice(
                "deleting measured concentrations for element ",
                chemicalName,
                " and point ",
                pointName,
            )
            cur.execute("DELETE FROM mesures WHERE pid=%d AND eid=%d" % (pid, eid))
        else:
            logger.notice("pid =", pid, " eid =", eid)
            logger.notice("deleting measured potential for point ", pointName)
            cur.execute("DELETE FROM mesures WHERE pid=%d AND eid IS NULL" % (pid))

    with open(fileName) as fil:
        type_ = next(fil).rstrip()
        for line in fil:
            spl = line.split()
            if len(spl) not in [2, 3, 4]:
                raise MeasureLoadError(
                    f"Measure file {fileName} must have 2 or 3 or 4 values by line. {len(spl)} found."
                )

            incertFlag = False
            hflag = ":" in spl[1]
            if hflag:
                date_time = spl[0] + " " + spl[1]
                if spl[1].count(":") == 1:
                    date_time = date_time + ":00"
                date_time = datetime.strptime(date_time, "%d/%m/%Y %H:%M:%S").strftime(
                    "%Y-%m-%d %H:%M:%S"
                )
                if len(spl) > 2:
                    value = spl[2]
                else:
                    continue
                if len(spl) > 3:
                    incertFlag = True
                    incert = spl[3]
            else:
                date_time = datetime.strptime(spl[0], "%d/%m/%Y").strftime("%Y-%m-%d")
                if len(spl) > 1:
                    value = spl[1]
                else:
                    continue
                if len(spl) > 2:
                    incertFlag = True
                    incert = spl[2]

            if incertFlag:
                if valueType == "conc":
                    logger.notice(
                        "inserting measured concentration for point ", pointName
                    )
                    cur.execute(
                        "INSERT INTO mesures\
                            (pid, date, type, eid, concentration, incertitude_concentration) \
                            VALUES (%d, '%s', '%s', %d, %g, %g)"
                        % (
                            pid,
                            date_time,
                            type_,
                            eid,
                            float(value)
                            / unitConversionFactor("kg/m³", units, specificActivity),
                            float(incert)
                            / unitConversionFactor("kg/m³", units, specificActivity),
                        )
                    )
                elif valueType == "pot":
                    logger.notice("inserting measured potential for point ", pointName)
                    cur.execute(
                        "INSERT INTO mesures\
                            (pid, date, type, potentiel, incertitude_potentiel) \
                            VALUES (%d, '%s', '%s', %d, %g, %g)"
                        % (pid, date_time, type_, float(value), float(incert))
                    )
            else:
                if valueType == "conc":
                    logger.notice(
                        "inserting measured concentration for point ", pointName
                    )
                    cur.execute(
                        "INSERT INTO mesures\
                            (pid, date, type, eid, concentration) \
                            VALUES (%d, '%s', '%s', %d, %g)"
                        % (
                            pid,
                            date_time,
                            type_,
                            eid,
                            float(value)
                            / unitConversionFactor("kg/m³", units, specificActivity),
                        )
                    )
                elif valueType == "pot":
                    logger.notice("inserting measured potential for point ", pointName)
                    cur.execute(
                        "INSERT INTO mesures\
                            (pid, date, type, potentiel) \
                            VALUES (%d, '%s', '%s', %g)"
                        % (pid, date_time, type_, float(value))
                    )


if __name__ == "__main__":
    import sys

    from ..database import sqlite
    from ..settings import Settings

    logger.enable_console(True)

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    if len(sys.argv) < 3:
        logger.error("wrong number of arguments (try database.py --help)")
        exit(1)

    if not Settings().contains("General", "defaultSite"):
        logger.error("defaultSite undefined, use preferences_dialog.py")
        exit(1)

    sitesdb = str(Settings().value("General", "defaultSite"))

    conn = sqlite.connect(sitesdb)

    delete = False
    argv = sys.argv
    for arg in argv:
        if arg == "-d":
            argv.remove("-d")
            delete = True

    #        argv[3].decode(sys.getfilesystemencoding()) if len(argv)==5 else None,
    try:
        load_measure(
            conn.cursor(),
            argv[1],
            argv[-1],
            argv[2] if len(argv) == 5 else None,
            argv[3] if len(argv) == 5 else None,
            delete,
        )
        conn.commit()
    except MeasureLoadError:
        exit(1)
