# Packaging and deployment

## Packaging

This plugin is using the [qgis-plugin-ci](https://github.com/opengisch/qgis-plugin-ci/) tool to perform packaging operations.   The package command is performing a `git archive` run based on changelog.

```bash
# package a specific version
qgis-plugin-ci package 1.3.1
# package latest version
qgis-plugin-ci package latest
```
## Package versions
2 versions of THYRSIS are available.

### Latest version from master branch

This version can be installed by using the THYRSIS custom plugin repository :<https://oslandia.gitlab.io/qgis/thyrsis/plugins.xml>

:::{note}
This version contains documentation for offline use and embedded libraries for Windows installation
:::

:::{warning}
Be careful, this version can be unstable. Do not use it in production.
:::

### Latest version from tag

This version can be installed from the official QGIS plugins repository: <https://plugins.qgis.org/plugins/thyrsis/>

Go to `Plugins` -> `Manage and Install Plugins`, look for the plugin and install it.

:::{note}
This version contains embedded libraries for Windows installation but documentation for offline use is not available (size limitation on QGIS plugins repository)
:::

## Release a version

Package are created using GitLab CI.

### Latest version from master branch

Package is created for each commit on master branch.


### Latest version from tag

1. Add the new version to the `CHANGELOG.md`
1. Change the version number in `__about__.py`
1. Apply a git tag with the relevant version: `git tag -a 0.3.0 {git commit hash} -m "This version rocks!"`
1. Push tag to main branch: `git push origin 0.3.0`
