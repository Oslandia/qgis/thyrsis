"""This module contains funstions that interact with the database
"""
from builtins import range

from thyrsis.exception import SimulationError

from ..log import logger
from ..utilities import toSeconds


def get_simulation_data(conn):
    """Return simulation data dictionnary

    :param conn: conn on a sqlite database
    :type conn: sqlite3.Connection

    :return: dictionnary of simulation data
    :rtype: dict
    """
    simulation_names = [
        "TYPE_INFILTRATION",
        "TYPE_INJECTION",
        "INSATURE",
        "NB_SIMULATIONS",
    ]

    cur = conn.cursor()
    cur.execute(
        """SELECT
        type_infiltration,
        type_injection,
        insature,
        nombre_de_simulations
        FROM simulations"""
    )
    tup = cur.fetchone()
    simulation_data = {
        simulation_names[i]: tup[i] for i in range(len(simulation_names))
    }
    simulation_data["FUITE"] = any(
        [a["VOLUME_EAU"] > 0 for a in get_injections_data(conn)]
    )

    # dates résultats
    tfin = 0.0
    dtmax = 0.0
    cur.execute("SELECT duree, pas_de_temps FROM dates_resultats")
    for duree_totale, pas_min in cur.fetchall():
        logger.debug("duree_totale, pas_min =", duree_totale, pas_min)
        tfin += toSeconds(duree_totale)
        dtmax = toSeconds(pas_min) if dtmax == 0.0 else min(dtmax, toSeconds(pas_min))
    # dtmax = 0.5*dtmax
    logger.debug("tfin, dtmax =", tfin, dtmax)
    dtmax = tfin / round(tfin / dtmax)
    simulation_data["TFIN"] = tfin
    simulation_data["DTMAX"] = dtmax

    # élément chimique
    (nb_elem,) = cur.execute(
        """
        SELECT COUNT(1) FROM elements_chimiques
        WHERE id = (SELECT eid FROM simulations LIMIT 1)"""
    ).fetchone()
    if nb_elem > 1:
        raise SimulationError(
            f"Only one chemical element supported for mhynos result. {nb_elem} found in simulation"
        )

    cur.execute(
        """SELECT
        decroissance_radioactive,
        activite_specifique,
        limite_qualite_eau
        FROM elements_chimiques
        WHERE id = (SELECT eid FROM simulations LIMIT 1)"""
    )
    (
        simulation_data["DECRAD"],
        simulation_data["ACTSPEC"],
        simulation_data["LIMQUAL"],
    ) = (
        cur.fetchone() if nb_elem else (0.0, 0.0, 0.0)
    )

    # nombre de noeuds saturé
    (simulation_data["NNOEUDS"],) = cur.execute(
        "SELECT COUNT(1) FROM noeuds"
    ).fetchone()
    # nombre de mailles saturé
    (simulation_data["NMAILLES"],) = cur.execute(
        "SELECT COUNT(1) FROM mailles"
    ).fetchone()

    # nombre de noeuds d'injection
    simulation_data["NTEDMA"] = (
        cur.execute(
            """
            SELECT COUNT(DISTINCT noeud) FROM noeuds_injections"""
        ).fetchone()[0]
        if simulation_data["TYPE_INJECTION"] != "aucune"
        else 0
    )

    logger.debug("NTEDMA = ", simulation_data["NTEDMA"])

    return simulation_data


def get_injections_pids(conn):
    """Return injections ids

    :param conn: conn on a sqlite database
    :type conn: sqlite3.Connection

    :return: list of injections ids
    :rtype: list
    """
    cur = conn.cursor()
    return [
        ogc_fid
        for ogc_fid, in cur.execute(
            "SELECT OGC_FID FROM injections ORDER BY OGC_FID"
        ).fetchall()
    ]


def get_injections_data(conn):
    """Return injection data dictionnary

    :param conn: conn on a sqlite database
    :type conn: sqlite3.Connection

    :return: dictionnary of injection data
    :rtype: dict
    """
    injections_names = [
        "INFILTRATION",
        "PERMEABILITE",
        "T0",
        "T1",
        "SURFACE",
        "PROFONDEUR",
        "VOLUME_EAU",
        "INPUT",
        "COEF_INJECTION",
        "COEF_SURFACE",
        "CUMUL_SURFACE",
    ]

    cur = conn.cursor()
    pids = get_injections_pids(conn)

    tup = cur.execute(
        """SELECT
        i.infiltration,
        i.permeabilite,
        (JULIANDAY(i.debut)-JULIANDAY(s.debut))*24*3600,
        (JULIANDAY(DATETIME(i.debut, '+'||i.duree))-JULIANDAY(s.debut))*24*3600,
        AREA(i.GEOMETRY),
        i.profondeur,
        i.volume_eau,
        i.input,
        i.coefficient_injection,
        i.coefficient_surface,
        SUM(n.surface)
        FROM noeuds AS n, injections AS i, simulations AS s, noeuds_injections AS ni
        WHERE ni.noeud = n.OGC_FID AND ni.injection = i.OGC_FID
        GROUP BY i.OGC_FID
        ORDER BY i.OGC_FID
        """
    ).fetchall()

    injections_data = []
    for inj, pid in enumerate(pids):
        injections_data.append(
            {injections_names[i]: tup[inj][i] for i in range(len(injections_names))}
        )
        cumul_surface = injections_data[inj]["CUMUL_SURFACE"]
        inj_surface = injections_data[inj]["SURFACE"]
        diff_surface = 100 * (cumul_surface - inj_surface) / inj_surface
        # logger.debug(u"Somme des surfaces des noeuds d'injection,",
        #    u"surface d'injection et différence : %.1f %.1f %.1f%%"%(cumul_surface, inj_surface, diff_surface))

    second_injections_names = ["INFILTRATION2", "PERMEABILITE2"]
    tup = cur.execute(
        """SELECT
        infiltration,
        permeabilite
        FROM injections_second_milieu
        ORDER BY OGC_FID
        """
    ).fetchall()
    if tup:
        for inj, pid in enumerate(pids):
            injections_data[inj].update(
                {
                    second_injections_names[i]: tup[inj][i]
                    for i in range(len(second_injections_names))
                }
            )

    return injections_data
