import os
from functools import partial

from PyQt5.QtCore import QSize

# from PyQt5.QtCore import *
from PyQt5.QtWidgets import QApplication, QHBoxLayout, QLabel, QWidget
from qgis.core import (
    QgsLayerTreeLayer,
    QgsMapRendererParallelJob,
    QgsMapSettings,
    QgsProject,
)
from qgis.gui import QgsColorButton

# from PyQt5.QtGui import *
from qgis.PyQt import uic


class ViewerControls(QWidget):
    """ViewerControls"""

    def __init__(self, viewer, iface=None, parent=None):
        """Constructor

        :param viewer: Viewer3d class
        :type viewer: ViewerWindow
        :param iface: qgis interface
        :type iface: QgisInterface
        :param parent: parent
        :type parent: QWidget
        """
        super(ViewerControls, self).__init__(parent)
        uic.loadUi(
            os.path.join(os.path.dirname(__file__), "viewer_3d_controls.ui"), self
        )

        self.__viewer = viewer
        # label = QLabel("Terrain", self)
        label = QLabel("Terrain")
        colorButton = QgsColorButton(self, "Couleur du terrain")
        colorButton.setAllowOpacity(True)
        colorButton.setColor(self.__viewer.scene.terrainColor())
        colorButton.colorChanged.connect(self.__viewer.scene.setTerrainColor)
        layout = QHBoxLayout()
        layout.addWidget(label)
        layout.addWidget(colorButton)
        self.scrollLayout.addLayout(layout)

        def colorChanger(name):
            def changer(newColor):
                self.__viewer.scene.contourColors()[name] = newColor

            return changer

        for name, color in list(self.__viewer.scene.contourColors().items()):
            name_str = str(name[0])
            label = QLabel(name_str)
            colorButton = QgsColorButton(self, "Couleur " + name_str)
            colorButton.setAllowOpacity(True)
            colorButton.setColor(color)
            colorButton.colorChanged.connect(colorChanger(name))
            layout = QHBoxLayout()
            layout.addWidget(label)
            layout.addWidget(colorButton)
            self.scrollLayout.addLayout(layout)

        self.contoursCheckBox.toggled.connect(self.__viewer.scene.toggleContours)
        self.forageCheckBox.toggled.connect(
            partial(self.__viewer.scene.toggleScatters, group="forage")
        )
        self.calculCheckBox.toggled.connect(
            partial(self.__viewer.scene.toggleScatters, group="calcul")
        )

        self.__viewer.scene.setZscale(self.zScaleSlider.value())
        self.zScaleSlider.valueChanged.connect(self.__viewer.scene.setZscale)

        self.__iface = iface
        self.textureCheckBox.toggled.connect(self.textureChanged)

    def textureChanged(self, flag):
        def recurse_(root, layers):
            for child in root.children():
                if (
                    isinstance(child, QgsLayerTreeLayer)
                    and child.isVisible()
                    and child.layer().name() != "resultats"
                ):
                    layers.append(child.layer().id())
                else:
                    recurse_(child, layers)

        if flag:
            root = QgsProject.instance().layerTreeRoot()
            layers = []
            recurse_(root, layers)
            settings = QgsMapSettings()
            dpi = settings.outputDpi()
            settings.setOutputSize(QSize(2048, 2048))
            settings.setFlag(QgsMapSettings.UseRenderingOptimization)
            settings.setLayers(QgsProject().instance().mapLayer(lyr) for lyr in layers)
            crs = QgsProject().instance().crs()
            settings.setDestinationCrs(crs)
            extent = QgsProject.instance().mapLayersByName("mailles")[0].extent()
            if (extent.xMaximum() - extent.xMinimum()) > (
                extent.yMaximum() - extent.yMinimum()
            ):
                extent.setYMaximum(
                    extent.yMinimum() + (extent.xMaximum() - extent.xMinimum())
                )
            else:
                extent.setXMaximum(
                    extent.xMinimum() + (extent.yMaximum() - extent.yMinimum())
                )
            settings.setExtent(extent)
            job = QgsMapRendererParallelJob(settings)
            job.start()
            while job.isActive():  # only necessary for meshlayers, keep it
                # just in case the result name is changed
                QApplication.instance().processEvents()
            job.waitForFinished()
            img = job.renderedImage()
            img.save("/tmp/texture.png")
            self.__viewer.scene.setTerrainTexture(
                img,
                [
                    extent.xMinimum(),
                    extent.yMinimum(),
                    extent.xMaximum(),
                    extent.yMaximum(),
                ],
            )
        else:
            self.__viewer.scene.setTerrainTexture(None)
