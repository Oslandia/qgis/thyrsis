# Installation

Les simulations 1D et 2D sont réalisées à l’aide d’un code extérieur d’écoulement et transport en milieu poreux.

Différents codes peuvent être connectés à THYRSIS et deux codes sont aujourd'hui implémentés :

- OpenFOAM, via la boîte à outils *open source* [porousMultiphaseFOAM](https://github.com/phorgue/porousMultiphaseFoam/) {cite}`horgue-soulaine:2015`.
- METIS (*Modélisation des Écoulements et des Transports avec Interaction en milieu Saturé*), développé par l'Ecole des Mines de Paris {cite}`goblet:1989`.

Le logiciel de simulation à utiliser est paramétrable dans le menu {ref}`Préférences<preferences_fr>` de THYRSIS.

```{toctree}
---
maxdepth: 2
glob: on
---
installation/debian.md
installation/windows.md
installation/centos.md
installation/simulation_software_preferences.md
installation/plugin.md
installation/mpi.md
```
