#! python3  # noqa: E265

"""
    Helper to manage access to plugin documentation, remote or local.
"""

# standard
from functools import lru_cache
from pathlib import Path
from typing import Union

# PyQGIS
from qgis.core import QgsBlockingNetworkRequest
from qgis.PyQt.Qt import QUrl
from qgis.PyQt.QtCore import QCoreApplication
from qgis.PyQt.QtGui import QDesktopServices
from qgis.PyQt.QtNetwork import QNetworkRequest

# package
from thyrsis.__about__ import DIR_PLUGIN_ROOT, __uri_homepage__
from thyrsis.toolbelt.log_handler import PlgLogger

# ############################################################################
# ########## Classes ###############
# ##################################


class PlgDocumentationHelpers:
    """Plugin documentation helpers."""

    is_online: bool = False

    def __init__(self):
        """Initialize plugin documentation helpers."""
        self.log = PlgLogger().log

    @lru_cache(maxsize=128)
    def has_online_access(self, url: str = __uri_homepage__) -> bool:
        """Check if online access is available, performing a HEAD request to the given
        URL.

        :param url: URL to test against, defaults to __uri_homepage__
        :type url: str, optional

        :return: online status
        :rtype: bool
        """
        ntwk_requester = QgsBlockingNetworkRequest()
        qreq = QNetworkRequest(url=QUrl(url))

        req_status = ntwk_requester.head(
            request=qreq,
            forceRefresh=True,
        )

        # check if request is fine
        if req_status != QgsBlockingNetworkRequest.NoError:
            self.log(
                message=f"Remote URL not reachable. Using local documentation. "
                f"Network trace: {ntwk_requester.errorMessage()}",
                log_level=1,
                push=0,
            )
            self.is_online = False
            self.log(self.get_local_documentation())
            return False
        else:
            self.log(
                message=f"Request to {url} succeeded.",
                log_level=3,
                push=0,
            )
            self.is_online = True
            return True

    @lru_cache(maxsize=128)
    def get_local_documentation(self) -> Union[Path, bool]:
        """Check if local documentation is available.

        :return: local documentation availability
        :rtype: Union[Path, bool]
        """
        if Path(DIR_PLUGIN_ROOT / "docs/index.html").exists():
            self.log(message="Local doc found: docs/index.html", log_level=4)
            return Path(DIR_PLUGIN_ROOT / "docs/index.html")
        elif len(list(DIR_PLUGIN_ROOT.glob("docs/*/*index.html"))):
            self.log(message="Local doc found: docs/*/index.html", log_level=4)
            return list(DIR_PLUGIN_ROOT.glob("docs/*/*index.html"))[0]
        else:
            self.log(message="Local documentation not found.", log_level=1)
            return False

    def open_help(self, url_path: str = None) -> None:
        """Open documentation, online if reachable or local as fallback. Handle additional path.

        :param url_path: additional URl path, defaults to None
        :type url_path: str, optional
        """
        if self.has_online_access():
            if url_path:
                QDesktopServices.openUrl(QUrl(f"{__uri_homepage__}{url_path}"))
            else:
                QDesktopServices.openUrl(QUrl(__uri_homepage__))
        elif self.get_local_documentation():
            if url_path:
                # QDesktopServices.openUrl(QUrl(f"{__uri_homepage__}{url_path}"))
                QDesktopServices.openUrl(
                    QUrl.fromLocalFile(
                        str(self.get_local_documentation().parent.resolve() / url_path)
                    )
                )
            else:
                QDesktopServices.openUrl(
                    QUrl.fromLocalFile(str(self.get_local_documentation().resolve()))
                )
        else:
            self.log(
                message=self.tr(
                    "Documentation is not available, nor online nor local."
                ),
                log_level=1,
                push=True,
                duration=30,
            )

    def tr(self, message: str) -> str:
        """Translation method.

        :param message: text to be translated
        :type message: str

        :return: translated text
        :rtype: str
        """
        return QCoreApplication.translate(self.__class__.__name__, message)
