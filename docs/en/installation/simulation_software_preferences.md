# Simulation software preferences

After THYRSIS installation, user must define path to simulation software in THYRSIS preference dialog

:::{figure} /_static/images/Preferences_installation.png
:name: Preferences_installation
Preferences menu, installation window
:::

## GMSH

GMSH is used for mesh creation. Path to GMSH executable must be defined by user.

By default, value is defined with search from `PATH`.

## OpenFoam

For OpenFoam use, user must define OpenFoam project directory (`WM_PROJECT_DIR`).

By default, value is defined with search of `blockMesh` executable from `PATH`.

If executable is not available, default search paths are used:

- Windows: `%APPDATA%/ESI-OpenCFD/OpenFOAM/v2206/msys64/home/ofuser/OpenFOAM/OpenFOAM-v2206/platforms/win64MingwDPnt32Opt/bin`
- Linux:  `/usr/lib/openfoam/openfoam2106/platforms/linux64GccDPInt32Opt/bin`

## PMF

For PMF use, user must define PMF installation directory.

By default, value is defined with search of `groundwaterFoam` executable from `PATH`.

If executable is not available, default search paths are used:

- Linux: `$HOME/OpenFOAM/$USERNAME-v2106/platforms/linux64GccDPInt32Opt/bin`

On Windows, please indicate path to extracted pre-compiled binaries.
