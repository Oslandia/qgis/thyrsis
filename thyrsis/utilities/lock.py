import platform
from builtins import object
import os
import tempfile

if platform.system() == "Windows":
    try:
        import portalocker
    except Exception:
        import site

        from thyrsis.__about__ import DIR_PLUGIN_ROOT

        site.addsitedir(DIR_PLUGIN_ROOT / "embedded_external_libs")
        import thyrsis.embedded_external_libs.portalocker as portalocker


def get_exclusive_lock(name):
    """Lock

    :param name: lock name
    :type name: string

    :return: lock file path
    :rtype: string
    """
    if platform.system() == "Windows":
        tempdir = tempfile.gettempdir()
        lock_file = os.path.join(tempdir, "{}.lock".format(name))
        fd = open(lock_file, "w")
        portalocker.lock(fd, portalocker.LOCK_EX)
        return fd
    else:
        import fcntl

        fd = open("/tmp/{}.lock".format(name), "w")
        fcntl.lockf(fd.fileno(), fcntl.LOCK_EX)
        return fd


def unlock_exclusive_lock(fd):
    """Unlock

    :param fd: lock file path
    :type fd: string
    """
    if platform.system() == "Windows":
        portalocker.unlock(fd)
        fd.close()
    else:
        import fcntl

        fcntl.lockf(fd.fileno(), fcntl.LOCK_UN)


class ExclusiveLock(object):
    """Exclusive Lock class"""

    def __init__(self, name):
        """Constructor

        :param name: lock name
        :type name: string"""
        self.__name = name

    def __enter__(self):
        """Activate lock

        :return: lock tmp file
        :rtype: string
        """
        self.__fd = get_exclusive_lock(self.__name)
        return self.__fd

    def __exit__(self, exc_type, exc_value, traceback):
        """Unlock, protected method"""
        unlock_exclusive_lock(self.__fd)
        self.__fd.close()
