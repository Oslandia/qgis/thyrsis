"""
load points in site database

USAGE

    python -m thyrsis.database.raster_points points_file raster_file

    try to interpolate value from a raster to a points file
"""

import logging
import struct

from osgeo import gdal, ogr


def raster_points(fileName, rasterName, delimiter=";"):
    """Load contours entries into the current database

    :param fileName: path of an output csv file
    :type fileName: string
    :param rasterName: path of a raster file
    :type rasterName: string
    :param delimiter: delimiter used for the csv creation
    :type contour_dir: string

    :return: None
    """
    src_ds = gdal.Open(rasterName)
    trans = None

    z = "altitude_mur" if rasterName == "mur.tif" else "altitude"

    data = []
    i = 0
    with open(fileName) as fil, open("file_raster.csv", "w") as fil_raster:
        for line in fil:
            if i == 0:
                id_map = {
                    name.replace("\n", "").lower(): j
                    for j, name in enumerate(line.split(delimiter))
                }
                fil_raster.write(line)
            else:
                values = line.replace("\n", "").split(delimiter)
                values[id_map[z]] = "%.2f" % (
                    _get_z(
                        src_ds,
                        float(values[id_map["x"]]),
                        float(values[id_map["y"]]),
                        trans,
                    )
                )
                fil_raster.write(delimiter.join(values) + "\n")
            i += 1


def _get_z(src_ds, x, y, trans=None):
    """Convert from map to pixel coordinates.
    Only works for geotransforms with no rotation.

    :param src_ds: spatial reference
    :type src_ds: osr.SpatialReference
    :param x: x coordinate
    :type x: float
    :param y: y coordinate
    :type y: float
    :param trans: CoordinateTransformation
    :type trans: osr.CoordinateTransformation

    :return: None
    """
    if trans is not None:
        p = ogr.Geometry(ogr.wkbPoint)
        p.AddPoint(x, y)
        p.Transform(trans)
        nx, ny = p.GetX(), p.GetY()
    else:
        nx, ny = x, y

    gt = src_ds.GetGeoTransform()
    rb = src_ds.GetRasterBand(1)
    px = int((nx - gt[0]) / gt[1])  # x pixel
    py = int((ny - gt[3]) / gt[5])  # y pixel

    # gestion des points sur les bords
    if nx == gt[0] + gt[1] * src_ds.RasterXSize:
        px = px - 1
    if ny == gt[3] + gt[5] * src_ds.RasterYSize:
        py = py - 1

    if px < 0 or py < 0 or px >= src_ds.RasterXSize or py >= src_ds.RasterYSize:
        raise Exception("Pas de valeur au point {} {}".format(nx, ny))
    value = rb.ReadRaster(px, py, 1, 1, buf_type=gdal.GDT_Float32)
    return struct.unpack("f", value)[0]


if __name__ == "__main__":
    import sys

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    if len(sys.argv) not in [3, 4]:
        logging.error(
            "wrong number of arguments (try python -m thyrsis.database.raster_points --help)"
        )
        exit(1)

    delimiter = ";"
    if len(sys.argv) == 4:
        delimiter = sys.argv[3]
    raster_points(sys.argv[1], sys.argv[2], delimiter)
