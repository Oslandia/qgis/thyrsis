"""
Combines IGN L93 asc files into one global mnt.tif file
Then convert mnt.tif to mnt.xyz
Then convert mnt.tif to hillshade.tif

USAGE

    python -m thyrsis.utilities.create_mnt

    must be run in the current directory to create tif files

    the asc files must be in a mnt_files.txt file
"""

from subprocess import Popen

if __name__ == "__main__":

    with open("mnt_files.txt") as fil:
        cmd = (
            ["gdalwarp", "-s_srs", "EPSG:2154", "-t_srs", "EPSG:2154"]
            + fil.read().splitlines()
            + ["mnt.tif"]
        )

        print("gdalwarp => mnt.tif")
        Popen(cmd).communicate()

        print("gdal_translate mnt.tif => mnt.xyz")
        Popen(["gdal_translate", "-of", "XYZ", "mnt.tif", "mnt.xyz"]).communicate()

        print("gdaldem mnt.tif => hillshade.tif")
        Popen(["gdaldem", "hillshade", "mnt.tif", "hillshade.tif"]).communicate()
