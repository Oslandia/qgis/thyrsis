---
substitutions:
  img_inversion: |-
    :::{image} /_static/images/inverse.png
    :scale: 10 %
    :::
---

# Création d'un modèle

(model_parameters_fr)=
## Paramètres du modèle

La définition du modèle commence en renseignant les valeurs des paramètres scalaires de la table **parametres** ({numref}`tutorial_parametersTable`).

:::{figure} /_static/images/parametersTable.png
:name: tutorial_parametersTable
Table **parametres** à renseigner.
:::

Ces paramètres sont caractéristiques du site et pourront être modifiés ultérieurement dans le cadre de chaque étude. Ils sont définis comme suit :

- WC, porosité cinématique,
- VGA, Alpha van Genuchten ($m^{-1}$),
- VGN, n van Genuchten,
- VGR, Saturation résiduelle,
- VGS, Saturation maximale,
- VGE, Pression d'entrée (m) ($m$),
- WT, Porosité totale,
- VM, Masse volumique (kg/m³),
- DK, Coefficient de partage (m³/kg),
- SL, Limite de solubilité (kg/m³),
- DLZNS, Dispersivité longitudinale ZNS (m),
- DLZS, Dispersivité longitudinale ZS (m),

Puis le champ de perméabilité doit être défini en cliquant sur le bouton {{ img_inversion }} qui propose 3 choix :

- *Créer un modèle avec une valeur constante de perméabilité* : cela crée un modèle hydrogéologique fondé sur un champ de perméabilité uniforme, dont la valeur doit être renseignée dans le dialogue,
- *Créer un modèle à partir d'un champ de perméabilité donné* : cela crée un modèle hydrogéologique fondé sur un champ de perméabilité fourni sous la forme d'un fichier externe .tif, qui doit être sélectionné dans le dialogue,
- *Créer un modèle par inversion* : cela lance le processus d'inversion, fondé sur des mesures de niveau piézométrique, cf. {ref}`Inversion<inversion_fr>`.

---

## Inversion

### Points pilotes

Commencer par spécifier les points pilotes dans la couche *points_pilote*, en indiquant obligatoirement leur zone d'appartenance et le niveau piézométrique mesuré (altitude_piezo_mesuree). Les zones doivent être numérotées à partir de 1 et sans trou. Plusieurs points pilotes peuvent appartenir à une même zone.

La couche *points_pilote* contient les champs suivants :

- *OGC_FID* : identifiant unique
- *nom* : nom du point pilote
- *groupe* : groupe du point pilote, utilisé pour le graphique de corrélation,
- *zone* : zone d'appartenance du point pilote, regroupant les points pilotes avec perméabilité similaire
- *altitude_piezo_mesuree* : niveau piézométrique mesuré (m), à renseigner avant l'inversion
- *altitude_piezo_calculee* : niveau piézométrique calculé (m), valeur mise à jour par le calcul d'inversion
- *difference_calcul_mesure* : différence entre les niveaux piézométriques calculé et mesuré (m), valeur mise à jour par le calcul d'inversion
- *permeabilite* : perméabilité calculée (m/s)

:::{note}
Les quatre attributs *nom*, *groupe*, *zone* et *altitude_piezo_mesuree* doivent être renseignés, les autres sont calculés durant le processus d'inversion.
:::

Il est possible d'importer des fichiers texte (menu QGIS *Couche>Ajouter une couche>Ajouter une couche de texte délimité*) et de procéder par copier-coller entre la couche importée et la couche *points_pilote*. Afin de faciliter le copier-coller, il est conseillé de nommer les colonnes du fichier importé selon les noms utilisés dans la table **points_pilote** (nom, zone, altitude_piezo_mesuree).

Un fichier *point_pilote.csv* est fourni, qui peut être ouvert en utilisant *Couche>Ajouter une couche>Ajouter une couche de texte délimité* ({numref}`tutorial_addTextDelimited`).

:::{figure} /_static/images/tutorial_addTextDelimited.png
:name: tutorial_addTextDelimited
Chargement du fichier *point_pilote.csv* avec *Ajouter une couche de texte délimité*
:::

Le formulaire QGIS doit être renseigné en sélectionnant les colonnes contenant les coordonnées X et Y ({numref}`tutorial_textDelimitedDialog`).

:::{figure} /_static/images/tutorial_textDelimitedDialog.png
:name: tutorial_textDelimitedDialog
Sélection des colonnes des coordonnées X et Y dans le dialogue de texte délimité
:::

La couche *points_pilote* peut être renseignée par copier/coller depuis la couche csv chargée. Seules les colonnes ayant le même nom que celles de la couche *points_pilote* peuvent être copiées.

Pour finir, la table d'attribut de la couche *points_pilote* devrait ressembler à la {numref}`tutorial_pilotPointsAttributes`.

(tutorial-pilotpointsattributes)=

:::{figure} /_static/images/tutorial_pilotPointsAttributes.png
:name: tutorial_pilotPointsAttributes
Table d'attribut de la couche *points_pilote*
:::

### Paramètres d'inversion

Le processus d'inversion dépend de paramètres stockés dans la table **hynverse_parametres** :

- *infiltration* : valeur de l'infiltration moyenne annuelle en m/s. Si l'infiltration est hétérogène, indiquer une valeur nulle et fournir un fichier [u_infiltration.node.sat or u_infiltration.elem.sat].
- *icalc* : type de calcul (3 par défaut),
- *niter* : maximum iteration number (default value : 20, but higher values are useful to obtain a good convergence),
- *errlim* : valeur objectif du critère d'erreur : le calcul s'interrompt lorsque l'erreur est inférieure à errlim (0.1 m par défaut),
- *alfmin* : valeur minimale du paramètre $\alpha$ : le calcul s'interrompt lorsque $\alpha$ devient inférieur à alfmin ($10^{-5}$ par défaut),
- *alfa* : valeur initiale du paramètre $\alpha$ (0.5 par défaut),
- *terr* : valeur initiale du taux d'accroissement maximal admissible de l'erreur (1.2 par défaut) : si le taux d'accroissement de l'erreur est supérieur à ce taux, la dernière itération n'est pas prise en compte et une nouvelle itération est réalisée, en diminuant le paramètre $\alpha$ d'un facteur 2. Cette procédure peut être renouvelée un nombre de fois inférieur ou égal à *nbessaismax* (voir ci-dessous),
- *nessaismax* : nombre maximal d'itérations possibles en réduisant le paramètre $\alpha$ d'un facteur 2(10 par défaut),
- *permini* : valeur initiale de la perméabilité ($2.10^{-5}$ par défaut),
- *permin* : valeur minimale de la perméabilité ($2.10^{-7}$ par défaut),
- *permax* : valeur maximale de la perméabilité ($2.10^{-4}$ par défaut),
- *nv_npp* : nombre de noeuds les plus proches des points pilotes, pour le calcul du potentiel aux points pilotes (4 par défaut),
- *nv_ppm* : nombre de points pilotes les plus proches des centres de mailles, pour le calcul du champ de perméabilité aux mailles (10 par défaut),
- *dv_pp* : rayon (en mètres) autour des points pilotes pour lisser la perméabilité par moyenne mobile (300 m par défaut),
- *d_mesh* : distance minimale d'un centre de maille à un point pilote existant, pour ajouter le centre de maille comme nouveau point pilote (dans le cas d'un potentiel de référence externe) (100 m par défaut).

Pour changer ces paramètres, sélectionner la couche *hynverse_parametres* et cliquer sur le bouton d'édition, puis ouvrir la table attributaire ({numref}`tutorial_hynverseParameters`).

:::{figure} /_static/images/tutorial_hynverseParameters.png
:name: tutorial_hynverseParameters
:scale: 50 %

Ouverture de la table attributaire de la couche *hynverse_parametres*
:::

La valeur d'infiltration est ici de $4.75 10^{-9}$ (écrire 4.75e-09 dans la table attributaire).

### Calcul

L'inversion est lancée en cliquant sur le bouton Modèle ({numref}`tutorial_modelButton`).

:::{figure} /_static/images/tutorial_modelButton.png
:name: tutorial_modelButton
Bouton Modèle
:::

et en sélectionnant *Créer un modèle par inversion* dans le dialogue ({numref}`modelDialog`).

:::{figure} /_static/images/modelDialog.png
:name: modelDialog
Dialogue du bouton Modèle
:::

Plusieurs couches sont créées :

- *potentiel_reference* : potentiel de référence, si un potentiel de référence externe a été spécifié,
- *potentiel* : niveau piézométrique calculé (m),
- *epaisseur_zs* : épaisseur de la nappe (zone saturée) (m),
- *epaisseur_zns* : épaisseur de la zone non saturée (m),"
- *permeabilite_x* : perméabilité selon x (m/s),
- *v_norme* : norme de la vitesse de Darcy (m/s)

Durant le procesus d'inversion, la couche *permeabilite_x* est mise à jour à chaque itération.
Les graphiques montrant la convergence sont également mis à jour ({numref}`tutorial_inversionCanvas`).

:::{figure} /_static/images/tutorial_inversionCanvas.png
:name: tutorial_inversionCanvas
Canevas QGIS avec graphiques permettant de suivre l'inversion
:::

Le graphique supérieur montre l'erreur moyenne entre les niveaux piézométriques mesurés et calculés. Le graphique inférieur montre la corrélation entre ces valeurs.

Pour finir, sauvegarder le projet maillage en cliquant sur *Projet>Sauvegarder* ou en utilisant *Ctrl + S*
