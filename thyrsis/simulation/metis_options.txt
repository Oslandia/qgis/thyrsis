% parametres gradient conjugue mot-clé reso
(inutile) igrconj = 1 # obligatoire pour résoudre le gradient conjugué
niveau = 2 en ZNS, 11 en ZS # niveau de préconditionnement
mnconn = 6 en ZNS, 10 en ZS # nombre maximum de connexions d'un noeud
(inutile) lmat5 = 0 # longueur des matrices d'éléments finis
(inutile) lmac5 = 0 # longueur de la matrice de préconditionnement
epsh = 0. # tolérance sur la résolution en écoulement
epsc = 0. # tolérance sur la résolution en transport
(inutile) istgrc = 1 # indice obligatoire d'arret en cas de non convergence
niter_gc = 100 # nombre maximum d'itérations


% options insature mot-clé knonsat
rmult_cormax = 0.1 # coefficient multiplicateur de la dimension maximale, définissant la variation maximale de la charge hydraulique
c_pstep = 0.1 # valeur initiale du fractionnement de la solution
c_ps_inc = 0.1 # valeur initiale de l'incrément
c_ps_tol = 1.e-5
ratio_max_res = 1.e-6
niterns = 1000 # nombre d'itérations
courant = off

% option de kdecoup
KDECOUP_DELT_ZNS = "10." # pas de temps initial ZNS
KDECOUP_DTMIN_ZNS = "1." # pas de temps minimal ZNS

KDECOUP_DELT_ZS = "100." # pas de temps initial ZNS
KDECOUP_DTMIN_ZS = "1." # pas de temps minimal ZS

force_fin_phase # force la fin de phase avec un grand pas de temps lorsque le pas de temps devient trop petit

% opt_phreatique
max_iter 200 # nombre maximal d'itérations en phréatique
relax_phreat 0.5 # coefficient de relaxation (poids attribué à deux itérations successives)
critere_pot 0.1 # critère de convergence sur le potentiel (m)
