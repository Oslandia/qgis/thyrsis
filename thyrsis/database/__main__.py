"""
usage:

    python -m thyrsis.database <simulation_name> <output>.sqlite

        extract a simulation database from stored simulations in site database

    python -m thyrsis.database <mesh_name> <output>.sqlite

        extract a simulation datatbase from mesh database

    python -m thyrsis.database upgrade <db>.sqlite

        upgrade database schema. This script creates a new schema and copy database

    python -m thyrsis.database double <db>.sqlite

        convert to dual-porosity simulation
"""

import os
import sys

from ..log import logger
from . import add_second_milieu, create_computation_database, upgrade

logger.enable_console(True)
logger.set_level("debug")

if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
    help(sys.modules[__name__])
    exit(0)

# update and exists
if len(sys.argv) == 3 and sys.argv[1] == "upgrade":
    upgrade(sys.argv[2])
    exit(0)

if len(sys.argv) == 3 and sys.argv[1] == "double":
    add_second_milieu(sys.argv[2])
    exit(0)

# extract computation db
if len(sys.argv) != 3:
    logger.error("wrong number of arguments (try database.py --help)")
    exit(1)

if os.path.exists(sys.argv[2]):
    os.remove(sys.argv[2])

if sys.argv[1].endswith(".mesh.sqlite"):
    create_computation_database(sys.argv[2], mesh_db=sys.argv[1])
else:
    create_computation_database(sys.argv[2], simulation_name=sys.argv[1])
