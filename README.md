# THYRSIS

[![pipeline status](https://gitlab.com/Oslandia/qgis/thyrsis/badges/master/pipeline.svg)](https://gitlab.com/Oslandia/qgis/thyrsis/-/commits/master)
[![documentation badge](https://img.shields.io/badge/documentation-autobuilt%20with%20Sphinx-blue)](https://oslandia.gitlab.io/qgis/thyrsis/)

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
[![coverage report](https://gitlab.com/Oslandia/qgis/thyrsis/badges/master/coverage.svg)](https://gitlab.com/Oslandia/qgis/thyrsis/-/commits/master)

THYRSIS is a QGIS plugin (3.22 and above) that allows to build hydrogeological models of watersheds, and to simulate the flow and transport of chemical and radiological substances in a saturated or unsaturated porous environment.

## Installation

For installation procedure, see documentation for available systems:

- [Windows](docs/en/installation/windows.md)
- [Debian](docs/en/installation/debian.md)
- [Centos](docs/en/installation/centos.md)

## Documentation

Documentation includes a General description of the plugin, an Installation guide, a Tutorial and an User manual:

- [English](https://oslandia.gitlab.io/qgis/thyrsis/en/)
- [French](https://oslandia.gitlab.io/qgis/thyrsis/fr/).

## License

- GPL 2
- CeCILL 2.1

This program is free software: you can redistribute it and/or modify it under the terms of (i) the GNU General Public License as published by the Free Software Foundation, either version 2 of the License, or any later version, and (ii) the CeCILL 2.1 FREE SOFTWARE LICENSE. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
