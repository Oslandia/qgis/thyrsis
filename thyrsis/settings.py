"""

settings thyrsis

USAGE
    python -m thyrsis.settings

"""
import codecs
import os
import shutil
import sys
from builtins import object, str
from configparser import ConfigParser
from distutils.spawn import find_executable
from pathlib import Path

from .log import logger
from .utilities import available_mpi


class Settings(object):
    """Thyrsis settings"""

    def __init__(self, dbpath=None):
        """Constructor

        :param dbpath: db path
        :type dbpath: string
        """
        # logger.notice("settings.init with dbpath = ", dbpath)
        self.__config = ConfigParser()
        self.__config.optionxform = str
        self.__dbpath = dbpath
        self.install_dir = os.path.expanduser("~/.thyrsis")
        if not os.path.isdir(self.install_dir):
            os.mkdir(self.install_dir)
        default_ini_file = os.path.join(self.install_dir, "thyrsis.ini")

        if not os.path.isfile(default_ini_file):
            logger.debug("creating default settings")
            self.__default()
            self.__dbpath = None
            self.save()  # create default init BEFORE assigning self.__dbpath
            self.__dbpath = dbpath

        if dbpath:
            if os.path.isfile(os.path.join(dbpath, ".thyrsis.ini")):
                logger.debug("reading", os.path.join(dbpath, ".thyrsis.ini"))
                with codecs.open(
                    os.path.join(dbpath, ".thyrsis.ini"), encoding="utf-8"
                ) as fil:
                    self.__config.read_file(fil)
                # logger.debug("codeHydro =", self.value("General", "codeHydro"))
                # logger.debug("codeHydroCommand = ", self.value("General", "codeHydroCommand"))
            else:
                logger.debug(
                    "copying",
                    default_ini_file,
                    "to",
                    os.path.join(dbpath, ".thyrsis.ini"),
                )
                os.makedirs(dbpath, exist_ok=True)
                shutil.copyfile(default_ini_file, os.path.join(dbpath, ".thyrsis.ini"))
                with codecs.open(
                    os.path.join(dbpath, ".thyrsis.ini"), encoding="utf-8"
                ) as fil:
                    self.__config.read_file(fil)

        else:
            settings_file = (
                os.path.join(os.getcwd(), ".thyrsis.ini")
                if os.path.isfile(os.path.join(os.getcwd(), ".thyrsis.ini"))
                else default_ini_file
            )

            logger.debug("(settings no dbpath)reading", settings_file)
            with codecs.open(settings_file, encoding="utf-8") as fil:
                self.__config.read_file(fil)
            # logger.debug("(settings no dbpath)codeHydro =", self.value("General", "codeHydro"))
            # logger.debug("(settings no dbpath)codeHydroCommand = ", self.value("General", "codeHydroCommand"))

        self.__config.set("General", "installDir", self.install_dir)
        self.__config.set(
            "General", "defaultSite", os.path.join(self.install_dir, "sites.sqlite")
        )
        self.__config.set(
            "General", "defaultMeshDir", os.path.join(self.install_dir, "mesh")
        )

        protocols = [p.lower() for p in available_mpi()]
        default_protocol = protocols[0] if len(protocols) > 0 else ""

        current_protocol = self.__config.get("General", "mpiProcess")
        if current_protocol not in protocols:
            self.__config.set(
                "General",
                "mpiProcess",
                default_protocol,
            )

        self.__convert()
        self.save(dbpath)

    def value(self, section, option, defaultValue=None) -> str:
        """Return value of an option

        :param section: section name
        :type section: string
        :param option: option name
        :type option: string
        :param defaultValue: defaultValue
        :type defaultValue: string

        :return: value
        :rtype: string
        """
        # logger.debug("settings.value ", section, option)
        if self.__config.has_option(section, option):
            return self.__config.get(section, option)
        elif defaultValue is not None:
            return str(defaultValue)
        else:
            raise RuntimeError("value unknown " + section + ":" + option)

    def setValue(self, section, option, value):
        """Set value of an option

        :param section: section name
        :type section: string
        :param option: option name
        :type option: string
        :param value: defaultValue
        :type value: string
        """
        # logger.debug("settings.setValue ", section, option, value, type(value))
        if not self.__config.has_section(section):
            self.__config.add_section(section)
        self.__config.set(section, option, str(value))

    def contains(self, section, option):
        """Check if an option exists

        :param section: section name
        :type section: string
        :param option: option name
        :type option: string

        :return: value
        :rtype: string
        """
        # logger.debug("settings.contains ", section, option)
        return self.__config.has_option(section, option)

    def options(self, section):
        """list options of a section

        :param section: section name
        :type section: string
        """
        # logger.debug("settings.options")
        return self.__config.options(section)

    def allKeys(self):
        """List all keys"""
        # logger.debug("settings.allKeys")
        options = []
        for section in self.__config.sections():
            options += self.__config.options(section)
        return options

    def save(self, databasePath=None):
        """Save settings in settings file"""
        # logger.debug("settings.save")
        # self.display()
        if not os.path.isdir(self.install_dir):
            os.mkdir(self.install_dir)
        if not os.path.isdir(os.path.join(self.install_dir, "mesh")):
            os.mkdir(os.path.join(self.install_dir, "mesh"))

        # logger.debug("(settings saving)codeHydro =", self.value("General", "codeHydro"))
        # logger.debug("(settings saving)codeHydroCommand = ", self.value("General", "codeHydroCommand"))
        if databasePath and os.path.isdir(databasePath):
            logger.debug(
                "saving settings in", os.path.join(databasePath, ".thyrsis.ini")
            )
            with open(os.path.join(databasePath, ".thyrsis.ini"), "w") as configfile:
                self.__config.write(configfile)

        # on sauve toujours dans ~/.thyrsis/thyrsis.ini
        logger.debug(
            "saving settings in", os.path.join(self.install_dir, "thyrsis.ini")
        )
        with open(os.path.join(self.install_dir, "thyrsis.ini"), "w") as configfile:
            self.__config.write(configfile)

    def __convert(self):
        # logger.debug("settings.convert")
        for section in self.__config.sections():
            for (option, value) in self.__config.items(section):
                self.setValue(section, option, value)

    def display(self):
        """Display all settings"""
        # logger.debug("settings.display")
        for section in self.__config.sections():
            for (option, value) in self.__config.items(section):
                print(section, option, value, type(value))

    def get_mpiconfig_file(self):
        files = [
            f
            for f in os.listdir(self.install_dir)
            if f[:7] == "process" and os.path.isfile(os.path.join(self.install_dir, f))
        ]
        if files:
            return os.path.join(self.install_dir, files[0])
        else:
            return ""

    def get_nbmpiprocess(self):
        nprocs = 1
        config_file = self.get_mpiconfig_file()
        if config_file:
            nprocs = 0
            with open(config_file) as fil:
                for line in fil:
                    if ":" in line and len(line.split(":")) > 0:
                        nprocs += int(line.split(":")[1])
        return str(nprocs)

    def get_pmf_root_dir(self):
        ex = find_executable("groundwaterFoam")
        if not ex and sys.platform.startswith("linux"):
            username = os.getenv("USER") or os.getenv("USERNAME") or ""
            pmf_default_bin_path = os.path.join(
                Path.home(),
                "OpenFOAM",
                username + "-v2106",
                "platforms",
                "linux64GccDPInt32Opt",
                "bin",
            )
            ex = find_executable("groundwaterFoam", pmf_default_bin_path)

        if ex and "bin" in ex:
            return ex[: ex.index("bin")]
        else:
            return ""

    def get_openfoam_root_dir(self):
        ex = find_executable("blockMesh")
        if not ex:
            openfoam_root_dir = os.path.join("usr", "lib", "openfoam", "openfoam2106")
            openfoam_default_bin_path = os.path.join(
                openfoam_root_dir, "platforms", "linux64GccDPInt32Opt", "bin"
            )
            ex = find_executable("blockMesh", openfoam_default_bin_path)

            if sys.platform.startswith("win") and os.getenv("APPDATA"):
                openfoam_root_dir = os.path.join(
                    os.getenv("APPDATA"),
                    "ESI-OpenCFD",
                    "OpenFOAM",
                    "v2206",
                    "msys64",
                    "home",
                    "ofuser",
                    "OpenFOAM",
                    "OpenFOAM-v2206",
                )
                openfoam_default_bin_path = os.path.join(
                    openfoam_root_dir, "platforms", "win64MingwDPInt32Opt", "bin"
                )
                ex = find_executable("blockMesh.exe", openfoam_default_bin_path)

        if ex and "platforms" in ex:
            return ex[: ex.index("platforms")]
        else:
            return ""

    def get_meshGenerator(self):
        if self.get_openfoam_root_dir():
            return "cfmesh"
        elif find_executable("gmsh"):
            return "gmsh"
        else:
            return ""

    def __default(self):
        logger.notice("settings.default")
        self.__config.add_section("General")
        self.__config.set("General", "installDir", self.install_dir)
        self.__config.set(
            "General", "defaultSite", os.path.join(self.install_dir, "sites.sqlite")
        )
        self.__config.set(
            "General", "defaultMeshDir", os.path.join(self.install_dir, "mesh")
        )
        self.__config.set("General", "meshGenerator", self.get_meshGenerator())
        self.__config.set(
            "General", "codeHydro", "openfoam" if self.get_openfoam_root_dir() else ""
        )
        self.__config.set("General", "codeHydroCommand", "")
        self.__config.set("General", "maps", "")
        self.__config.set("General", "mpiConfig", self.get_mpiconfig_file())
        self.__config.set("General", "mpiProcess", "")
        self.__config.set("General", "nbMpiProcess", self.get_nbmpiprocess())
        self.__config.set("General", "nbColumnsMax", "200")
        self.__config.set("General", "console", "0")
        self.__config.set("General", "debug", "0")
        self.__config.set("General", "gmsh", find_executable("gmsh") or "")
        self.__config.set("General", "openfoam", self.get_openfoam_root_dir())
        self.__config.set("General", "pmf", self.get_pmf_root_dir())

        self.__config.add_section("Variables")
        self.__config.set("Variables", "potentielZSCheck", "2")
        self.__config.set("Variables", "darcyZNSCheck", "0")
        self.__config.set("Variables", "darcyZSCheck", "0")
        self.__config.set("Variables", "concentrationZNSCheck", "2")
        self.__config.set("Variables", "concentrationZSCheck", "2")
        self.__config.set("Variables", "activityZNSCheck", "0")
        self.__config.set("Variables", "saturationZNSCheck", "2")
        self.__config.set("Variables", "debitZSCheck", "1")
        self.__config.set("Variables", "potentielUnit", "m")
        self.__config.set("Variables", "darcyUnit", "m/s")
        self.__config.set("Variables", "concentrationUnit", "mg/L")
        self.__config.set("Variables", "activityUnit", "Bq/kg")
        self.__config.set("Variables", "massUnit", "kg")
        self.__config.set("Variables", "concentrationScale", "1e-3")
        self.__config.set("Variables", "darcyScale", "1e-3")
        self.__config.set("Variables", "concentrationClasses", "10")
        self.__config.set("Variables", "potentielClasses", "10")
        self.__config.set("Variables", "darcyClasses", "10")
        self.__config.set("Variables", "maskUnitsCheck", "0")

        self.__config.add_section("Animation")
        self.__config.set("Animation", "framePerSecond", "25")

        self.__config.add_section("Matplotlib")
        self.__config.set("Matplotlib", "ticksLabelSize", "10")
        self.__config.set("Matplotlib", "ylabelFontSize", "10")
        self.__config.set("Matplotlib", "legendFontSize", "10")

        self.__config.add_section("Database")
        self.__config.set("Database", "useRemoteDb", "False")
        self.__config.set("Database", "connectionString", "")


if __name__ == "__main__":
    import sys

    settings = Settings()
