# Description générale
Le code THYRSIS (*Transport HYdrogéologique Rapide Simulé en Insaturé et Saturé*) permet la simulation, sur différents sites prédéfinis, de l'écoulement et du transport hydrogéologique, en milieux poreux non saturé et saturé, de substances radiologiques ou chimiques injectées en différentes zones.

Il se présente sous la forme d'un plugin du logiciel QGIS, de sorte qu'il intègre toutes les fonctionnalités de ce SIG (Système d'Information Géographique).

La modélisation repose sur une décomposition en colonnes 1D non saturées couplées à un modèle de nappe libre 2D horizontal. Le modèle d'écoulement de nappe libre horizontal peut être importé ou créé directement avec THYRSIS, qui inclut le mailleur [GMSH](http://gmsh.info/) {cite}`geuzaine-remacle:2009` et une procédure d'inversion {cite}`renard-jeannee:2008`, {cite}`renard-tognelli:2016`.

Les simulations 1D et 2D sont réalisées à l'aide d'un code extérieur d'écoulement et transport en milieu poreux. Différents outils peuvent être connectés à THYRSIS et 2 codes sont actuellement disponibles :

- OPENFOAM, via la boite à outil opensource [porousMultiphaseFOAM](https://github.com/phorgue/porousMultiphaseFoam/) {cite}`horgue-soulaine:2015`.

- METIS (*Modélisation des Écoulements et des Transports avec Interaction en milieu Saturé*), développé à l'Ecole des Mines de Paris {cite}`goblet:1989`.

THYRSIS peut prendre en compte plusieurs types d'injection (flux, concentration ou masse), et plusieurs zones d'injection, en surface ou en profondeur, ainsi qu'une injection diffuse sur tout le domaine modélisé. L'injection peut aussi être réalisée directement dans la nappe, sans prise en compte de la zone non saturée.

THYRSIS inclut le traitement des incertitudes par `Hypercube Latin`. En attribuant des lois de probabilités aux paramètres du modèle, l'utilisateur peut générer plusieurs simulations avec différents jeux de paramètres, et THYRSIS fournit les statistiques élémentaires : moyenne, écart-type et intervalles de confiance, ainsi que des cartes de probabilité de dépassement de seuil.

Le plugin permet une parallélisation des calculs par protocole MPI, aussi bien pour la simulation de plusieurs colonnes non saturées que pour les calculs d'incertitudes avec plusieurs jeux de paramètres.

La modélisation chimique est limitée à la prise en compte d'une limite de solubilité, permettant de définir le flux de soluté en surface, et d'un coefficient de rétention, ayant l'effet de ralentir le transport du soluté. Il n'est pas tenu compte de la spéciation du composé en solution, ni de sa complexation avec les matières organiques de type substances humiques, ni d'interactions complexes avec la matrice rocheuse.

Le plugin THYRSIS, associé au code METIS, permet également de simuler l'écoulement et le transport en "double milieu", selon la formulation de Gerke & Van Genuchten  {cite}`gerke-genuchten:1993` (disponible pour l'instant uniquement via METIS).

THYRSIS est intégré à QGIS de sorte que l'utilisateur peut définir de manière interactive les zones d'injection et leurs caractéristiques et les paramètres des simulations, lancer les simulations et visualiser les résultats, soit directement en 2D dans la fenêtre standard de QGIS, soit en 3D dans une fenêtre ancrable spécifique, soit en 1D pour générer les bilans de masse et des courbes de résultats en fonction du temps, avec comparaison à des mesures éventuelles. Une autre fenêtre ancrable est dédiée à la représentation des colonnes verticales de la zone non saturée.

Le plugin s'appuie sur des bases de données Spatialite, qui stockent ausi bien les caractéristiques des modèles hydrogéologiques des différents sites étudiés que les paramètres de simulation.
