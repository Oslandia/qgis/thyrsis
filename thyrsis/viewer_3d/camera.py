from builtins import object

from PyQt5.QtGui import QVector3D
from qgis.PyQt.QtCore import Qt

# -*- coding: UTF-8 -*-


class Camera(object):
    """a simple camera"""

    def __init__(self, eye, at, up=QVector3D(0, 0, 1)):
        """Constructor

        :param eye: eye of the camera
        :type eye: QVector3D
        :param at: distance
        :type at: QVector3D
        :param up: z
        :type up: QVector3D
        """
        self.at0 = QVector3D(at)
        self.eye0 = QVector3D(eye)
        self.up0 = QVector3D(up)
        self.reset()

    def reset(self):
        """Reset camera with initial parameters"""
        self.at = QVector3D(self.at0)
        self.eye = QVector3D(self.eye0)
        self.up = QVector3D(self.up0)

    def move(self, delta_x, delta_y, buttons, modifiers):
        """Move the camera

        :parameter delta_x: x coordinates
        :type delta_x: float
        :parameter delta_y: y coordinates
        :type delta_y: float
        :params buttons: button type
        :type buttons: Qt.MouseButton
        :params modifiers: button type
        :type modifiers: Qt.Modifier
        """
        # user is dragging
        if int(buttons) & Qt.LeftButton:
            to = self.at - self.eye
            distance = to.length()
            if int(modifiers) & Qt.ShiftModifier:
                translation = to * delta_y
                self.eye -= translation
            elif int(modifiers) & Qt.ControlModifier:
                right = QVector3D.crossProduct(to, self.up).normalized()
                up = QVector3D.crossProduct(right, to).normalized()
                translation = right * (delta_x * distance) + up * (delta_y * distance)
                self.at -= translation
                self.eye -= translation
            else:
                right = QVector3D.crossProduct(to, self.up).normalized()
                up = QVector3D.crossProduct(right, to).normalized()
                translation = right * (delta_x * distance) + up * (delta_y * distance)
                self.eye -= translation * 5
                # correct eye position to maintain distance
                to = self.at - self.eye
                self.eye = self.at - to.normalized() * distance

    def wheel_zoom(self, dt):
        """Zoom in scene using wheel in the QGraphicsScene"""
        self.eye += (self.at - self.eye) * dt / 100
