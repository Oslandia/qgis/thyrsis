# Development

## Environment setup

Typically on Ubuntu:

```bash
# create virtual environment linking to system packages (for pyqgis)
python3 -m venv .venv --system-site-packages
source .venv/bin/activate

# bump dependencies inside venv
python -m pip install -U pip
python -m pip install -U -r requirements/development.txt

# install git hooks (pre-commit)
pre-commit install
```

## Register the repository as QGIS plugin path

:::{important}
It's strongly recomended to work on a specific QGIS profile to avoid conflicts with other plugins or variables. See [the related documentation](https://docs.qgis.org/3.22/en/docs/user_manual/introduction/qgis_configuration.html#working-with-user-profiles).
:::

1. In QGIS, go to `Settings` > `Options` > `System`
1. In the `Environment` section, add the `QGIS_PLUGINPATH` variable with the absolute path to your local repository

![](/_static/images/dev_qgis_env_var_path_to_repo.png)
