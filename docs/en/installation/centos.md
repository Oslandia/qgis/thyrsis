# CentOS and related distributions

:::{note}
Depending on you CentOS version, package name can change. Please take a look of [yum_requirements.txt](https://gitlab.com/Oslandia/qgis/thyrsis/-/blob/master/requirements/yum.txt)
:::

With some distribution (CentOS6 for instance), it is not possible to access hidden folders through QGIS file explorer. To work around this problem, we recommand to create a symbolic link between the plugin installation folder and an accessible folder, for instance :

```sh
ln -s .local/share/QGIS/QGIS3/profiles/default/python/plugins/thyrsis ~/.
```
