"""
Crisis interface to define simulation with a mininumum of user interface

"""
import os
from builtins import range, str

from PyQt5.QtCore import QDate
from PyQt5.QtWidgets import QMessageBox, QWidget
from qgis.core import Qgis, QgsProject
from qgis.PyQt import uic

from ..database import sqlite
from ..log import logger
from .injection_widget import InjectionWidget
from .step_widget import StepWidget


class CrisisWidget(QWidget):
    """Crisis dialog, where injections are defined"""

    def __init__(self, dbname, iface, parent=None):
        """Constructor

        :param dbname: path to db
        :type dbname: string
        :param iface: qgis interface
        :type iface: QgisInterface
        :param parent: Qt parent
        :type parent: QtWidget
        """
        super(CrisisWidget, self).__init__(parent)
        uic.loadUi(os.path.join(os.path.dirname(__file__), "crisis.ui"), self)

        self.__canvas = iface.mapCanvas()
        self.__iface = iface

        self.__dbname = dbname

        self.fillChemCombobox()

        self.__setupInterface()

        self.chemicalComboBox.currentIndexChanged.connect(self.__chemicalChanged)
        self.massButton.clicked.connect(self.__addMass)
        self.fluxButton.clicked.connect(self.__addFlux)
        self.concentrationButton.clicked.connect(self.__addConcentration)
        self.addStepButton.clicked.connect(self.__addStep)
        self.buttonBox.rejected.connect(self.cancel)
        self.buttonBox.accepted.connect(self.updateDatabase)

        logger.debug("CrisisWidget.__init__ connected stuff")

        # connect to dataChanged signals for layers that will affect the form
        QgsProject.instance().mapLayersByName("simulations")[0].editingStopped.connect(
            self.__setupInterface
        )
        QgsProject.instance().mapLayersByName("injections")[0].editingStopped.connect(
            self.__setupInterface
        )
        lyrs = QgsProject.instance().mapLayersByName("dates_resultats")
        if lyrs:
            lyrs[0].editingStopped.connect(self.__setupInterface)
        QgsProject.instance().mapLayersByName("parametres_simulation")[
            0
        ].editingStopped.connect(self.__setupInterface)
        QgsProject.instance().mapLayersByName("elements_chimiques")[
            0
        ].editingStopped.connect(self.__setupInterface)

        iface.actionDraw().triggered.connect(self.__setupInterface)

        logger.debug("CrisisWidget.__init__ done")

    def project(self):
        """Getter of the project attribute

        :return: db path
        :rtype: string
        """
        return self.__dbname

    def cancel(self):
        """Cancel changes and reload interface with the db data

        :return: None
        :rtype: None
        """
        self.__setupInterface()

    def fillChemCombobox(self):
        current_text = None
        if self.chemicalComboBox.count:
            current_text = self.chemicalComboBox.currentText()

        self.chemicalComboBox.clear()
        with sqlite.connect(self.__dbname, "CrisisWidget:init") as conn:
            cur = conn.cursor()
            cur.execute("SELECT nom, id FROM elements_chimiques ORDER BY nom")
            self.chemicalComboBox.addItem("")
            for nom, id_ in cur.fetchall():
                self.chemicalComboBox.addItem(nom, id_)

        if self.chemicalComboBox.findText(current_text) != -1:
            self.chemicalComboBox.setCurrentIndex(
                self.chemicalComboBox.findText(current_text)
            )

    def __setupInterface(self):
        """Load dialog interface using db data

        :return: None
        :rtype: None
        """
        # clear injections
        logger.debug("CrisisWidget:__setupInterface")
        for i in reversed(list(range(self.injectionsLayout.count()))):
            item = self.injectionsLayout.takeAt(i)
            if item.widget():
                item.widget().setParent(None)

        for i in reversed(list(range(self.durationsLayout.count()))):
            item = self.durationsLayout.takeAt(i)
            if item.widget():
                item.widget().setParent(None)

        with sqlite.connect(self.__dbname, "CrisisWidget:__setupInterface") as conn:
            logger.debug(
                "CrisisWidget:__setupInterface connected to {}".format(self.__dbname)
            )
            cur = conn.cursor()

            # setup the interface according to existing simulation
            sid, site_name = cur.execute("SELECT id, nom FROM sites").fetchone()

            eid = None
            eid, type_injection, debut, nom, type_infiltration, insature = cur.execute(
                "SELECT eid, type_injection, debut, nom, type_infiltration, insature FROM simulations"
            ).fetchone()
            assert not cur.fetchone()
            self.simulationNameEdit.setText(nom)
            self.dateEdit.setDate(QDate.fromString(debut.split()[0], "yyyy-MM-dd"))
            self.insatureCheckBox.setChecked(insature == "oui")
            self.transientCheckBox.setChecked(type_infiltration == "transitoire")

            cur.execute(
                "SELECT OGC_FID, nom, debut, duree, input, \
                    profondeur, volume_eau, AsText(GEOMETRY), coefficient_surface, coefficient_injection \
                    FROM injections"
            )
            for (
                fid,
                nom,
                debut,
                duree,
                input_,
                profondeur,
                volume_eau,
                wkt,
                coefficient_surface,
                coefficient_injection,
            ) in cur.fetchall():
                attr = {
                    "nom": nom,
                    "debut": debut,
                    "duree": duree,
                    "input": input_,
                    "profondeur": profondeur,
                    "volume_eau": volume_eau,
                    "wkt": wkt,
                    "coefficient_surface": coefficient_surface,
                    "coefficient_injection": coefficient_injection,
                }
                if type_injection == "concentration":
                    self.__addConcentration(attr)
                elif type_injection == "flux":
                    self.__addFlux(attr)
                elif type_injection == "masse":
                    self.__addMass(attr)
                else:
                    self.__addTypeless(attr)

            logger.debug("CrisisWidget:__setupInterface got injections")

            if eid:
                (chemical_name,) = cur.execute(
                    "SELECT nom FROM elements_chimiques WHERE id=%s" % (eid)
                ).fetchone()
                self.chemicalComboBox.setCurrentIndex(
                    self.chemicalComboBox.findText(chemical_name)
                )

            self.slEdit.setText(
                str(
                    cur.execute("SELECT AVG(sl) FROM parametres_simulation").fetchone()[
                        0
                    ]
                )
            )
            self.kdEdit.setText(
                str(
                    cur.execute("SELECT AVG(dk) FROM parametres_simulation").fetchone()[
                        0
                    ]
                )
            )

            logger.debug("CrisisWidget:__setupInterface got stuff")

            cur.execute("SELECT duree, pas_de_temps  FROM dates_resultats")
            for duree, pas_de_temps in cur.fetchall():
                logger.debug("duree =", duree, "pas de temps =", pas_de_temps)
                self.__addStep({"duree": duree, "pas_de_temps": pas_de_temps})

        logger.debug("CrisisWidget:__setupInterface done")

    def __warn(self, msg):
        """Display a warning in Qgis interface

        :param msg: message to display
        :type msg: string

        :return: None
        :rtype: None
        """
        if self.__iface:
            self.__iface.messageBar().pushMessage(
                self.tr("Warning"), msg, level=Qgis.Warning
            )
        else:
            QMessageBox.warning(self, self.tr("Warning"), msg)

    def updateDatabase(self, update_injection_geometries=True):
        """Update database with the information of the dialog form

        :param update_injection_geometries: flag to update the saved injection geometries
        :type update_injection_geometries: bool

        :return: success state
        :rtype: bool
        """
        with sqlite.connect(self.__dbname, "CrisisWidget:updateDatabase") as conn:
            conn.execute("PRAGMA foreign_keys = ON")
            cur = conn.cursor()

            project_SRID = str(
                cur.execute(
                    "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
                ).fetchone()[0]
            )

            (type_injection,) = cur.execute(
                "SELECT type_injection FROM simulations"
            ).fetchone()
            if (
                int(self.massButton.isHidden())
                + int(self.fluxButton.isHidden())
                + int(self.concentrationButton.isHidden() == 2)
            ):
                if self.massButton.isHidden() == False:
                    type_injection = "masse"
                elif self.fluxButton.isHidden() == False:
                    type_injection = "flux"
                elif self.concentrationButton.isHidden() == False:
                    type_injection = "concentration"
            else:
                type_injection = "aucune"

            eid = self.chemicalComboBox.itemData(self.chemicalComboBox.currentIndex())
            logger.debug(
                "updateDatabase: eid = {}, type_injection = {}".format(
                    eid, type_injection
                )
            )
            if not eid and type_injection != "aucune":
                self.__warn("A chemical element must be selected")
                conn.rollback()
                return False

            if self.injectionsLayout.count() and type_injection != "aucune":
                minInjDate = min(
                    [
                        self.injectionsLayout.itemAt(i).widget().dateEdit.date()
                        for i in range(self.injectionsLayout.count())
                    ]
                )
                if minInjDate < self.dateEdit.date():
                    self.__warn(
                        "Le début d'une injection doit être postérieur au début de la simulation"
                    )
                    conn.rollback()
                    return False

            insature = "oui" if self.insatureCheckBox.isChecked() else "non"
            infiltration = (
                "transitoire" if self.transientCheckBox.isChecked() else "permanente"
            )

            if eid:
                sql = (
                    "UPDATE simulations SET eid='%(eid)d', type_injection='%(type)s', \
                        debut='%(debut)s', nom='%(nom)s', type_infiltration='%(infiltration)s', insature='%(insature)s'"
                    % {
                        "eid": eid,
                        "type": type_injection,
                        "debut": self.dateEdit.date().toString("yyyy-MM-dd"),
                        "nom": self.simulationNameEdit.text(),
                        "infiltration": infiltration,
                        "insature": insature,
                    }
                )
            else:
                sql = (
                    "UPDATE simulations SET type_injection='%(type)s', \
                        debut='%(debut)s', nom='%(nom)s', type_infiltration='%(infiltration)s', insature='%(insature)s'"
                    % {
                        "type": type_injection,
                        "debut": self.dateEdit.date().toString("yyyy-MM-dd"),
                        "nom": self.simulationNameEdit.text(),
                        "infiltration": infiltration,
                        "insature": insature,
                    }
                )
            # logger.debug(sql)
            cur.execute(sql)

            if not self.durationsLayout.count():
                self.__warn("Aucun pas de calcul défini")
                conn.rollback()
                return False

            cur.execute("DELETE FROM dates_resultats")
            for i in range(self.durationsLayout.count()):
                step = self.durationsLayout.itemAt(i).widget()
                try:
                    attr = step.attributes()
                except ValueError as e:
                    self.__warn(
                        "Erreur dans la définition des pas de temps (%s)" % (str(e))
                    )
                    conn.rollback()
                    return False

                cur.execute(
                    "INSERT INTO dates_resultats(idx, duree, pas_de_temps) VALUES ({idx}, '{duree}', '{pas_de_temps}')".format(
                        idx=i + 1, **attr
                    )
                )

            if update_injection_geometries:
                cur.execute("DELETE FROM noeuds_injections")
                cur.execute("DELETE FROM mailles_injections")
                cur.execute("DELETE FROM injections")
                logger.debug(
                    "NOMBRE D'INJECTIONS DANS L'INTERFACE",
                    self.injectionsLayout.count(),
                )

                for i in range(self.injectionsLayout.count()):
                    inj = self.injectionsLayout.itemAt(i).widget()
                    # logger.debug("injection", i)
                    try:
                        attr = inj.attributes()
                    except ValueError as e:
                        self.__warn(
                            "Erreur dans la définition des injections (%s)" % (str(e))
                        )
                        conn.rollback()
                        return False
                    if (
                        hasattr(inj, "comboBoxInputUnit")
                        and inj.comboBoxInputUnit.currentText() == "Bq"
                    ):
                        cur.execute(
                            "SELECT activite_specifique \
                                FROM elements_chimiques WHERE id={eid}".format(
                                eid=eid
                            )
                        )
                        (activite_specifique,) = cur.fetchone()
                        attr["input"] /= activite_specifique

                    canvas_crs = str(QgsProject.instance().crs()).split("EPSG:")[-1][
                        :-1
                    ]
                    sql = """
                        INSERT INTO injections(
                            nom, debut, input, profondeur, duree, volume_eau, coefficient_surface, coefficient_injection, GEOMETRY)
                            VALUES
                            ({nom}, {debut}, {input}, {profondeur}, {duree}, {volume_eau}, {coefficient_surface}, {coefficient_injection},
                                CastToMultiPolygon(ST_Transform(GeomFromText({wkt}, {canvas_srid}),{srid})))
                        """.format(
                        srid=project_SRID, canvas_srid=canvas_crs, **attr
                    )
                    # logger.debug(sql)
                    cur.execute(sql)

                notInDomain = cur.execute(
                    "SELECT nom FROM injections AS i \
                       WHERE (SELECT COUNT(1) FROM mailles AS m \
                              WHERE Intersects(m.GEOMETRY, i.GEOMETRY) AND i.GEOMETRY IS NOT NULL) == 0"
                ).fetchall()
                if len(notInDomain):
                    self.__warn(
                        "Point d'injection hors du domaine: "
                        + " ".join([v if v else "sans nom" for v, in notInDomain])
                    )
                    return False

            if self.slEdit.text():
                if float(self.slEdit.text()) <= 0.0 and type_injection == "masse":
                    self.__warn(
                        "La limite de solubilité doit être strictement positive"
                    )
                    return False
            elif type_injection == "masse":
                self.__warn("Vous devez définir la limite de solubilité")
                return False

            if not self.kdEdit.text() and type_injection != "aucune":
                self.__warn("Vous devez définir le coefficient de rétention ")
                return False

            if (
                cur.execute("SELECT nombre_de_simulations FROM simulations").fetchone()[
                    0
                ]
                == 1
            ):
                cur.execute(
                    "UPDATE parametres_simulation SET sl=%g"
                    % (float(self.slEdit.text()))
                )
                cur.execute(
                    "UPDATE parametres_simulation SET dk=%g"
                    % (float(self.kdEdit.text()))
                )
            else:
                self.__warn(
                    "Les paramètres de simulation ne sont pas mis à jour automatiquement "
                    " dans le cas de simulations multiples"
                )

            conn.commit()
        # self.__canvas.refreshAllLayers()
        QgsProject.instance().mapLayersByName("injections")[0].triggerRepaint()
        return True

    def __chemicalChanged(self, idx):
        """Trigerred when user changes the chemical, clear some edit line

        :param idx: index
        :type idx: integer

        :return:
        :rtype:
        """
        if idx == 0:
            self.slEdit.setText("")
            self.slEdit.setToolTip("")
            self.kdEdit.setText("")
            self.kdEdit.setToolTip("")
        else:
            self.slEdit.setText("0")
            self.kdEdit.setText("0")

    def __refreshButtons(self):
        """Refresh injection button to fit with the chosen injection type

        :return: None
        :rtype: None
        """
        if self.injectionsLayout.count() == 1:  # this one is the last widget
            self.massButton.show()
            self.fluxButton.show()
            self.concentrationButton.show()

    def __addMass(self, attr=None):
        """Add a mass injection widget in the injection layout

        :param attr: dictionnary of injection attributes
        :type attr: dict

        :return: widget of injection
        :rtype: InjectionWidget
        """
        inj = InjectionWidget("mass", self.__canvas, attr, self)
        self.massButton.show()
        self.fluxButton.hide()
        self.concentrationButton.hide()
        self.slEdit.show()
        self.slLabel.show()
        self.slUnits.show()
        inj.destroyed.connect(self.__refreshButtons)
        self.injectionsLayout.addWidget(inj)
        return inj

    def __addFlux(self, attr=None):
        """Add a flux injection widget in the injection layout

        :param attr: dictionnary of injection attributes
        :type attr: dict

        :return: widget of injection
        :rtype: InjectionWidget
        """
        inj = InjectionWidget("flux", self.__canvas, attr, self)
        self.fluxButton.show()
        self.massButton.hide()
        self.concentrationButton.hide()
        self.slEdit.hide()
        self.slLabel.hide()
        self.slUnits.hide()
        inj.destroyed.connect(self.__refreshButtons)
        self.injectionsLayout.addWidget(inj)
        return inj

    def __addConcentration(self, attr=None):
        """Add a concentration injection widget in the injection layout

        :param attr: dictionnary of injection attributes
        :type attr: dict

        :return: widget of injection
        :rtype: InjectionWidget
        """
        inj = InjectionWidget("concentration", self.__canvas, attr, self)
        self.concentrationButton.show()
        self.massButton.hide()
        self.fluxButton.hide()
        self.slEdit.hide()
        self.slLabel.hide()
        self.slUnits.hide()
        inj.destroyed.connect(self.__refreshButtons)
        self.injectionsLayout.addWidget(inj)
        return inj

    def __addTypeless(self, attr=None):
        """Add a injection (without type) widget in the injection layout

        :param attr: dictionnary of injection attributes
        :type attr: dict

        :return: widget of injection
        :rtype: InjectionWidget
        """
        inj = InjectionWidget("aucune", self.__canvas, attr, self)
        self.concentrationButton.hide()
        self.massButton.hide()
        self.fluxButton.hide()
        inj.destroyed.connect(self.__refreshButtons)
        self.injectionsLayout.addWidget(inj)
        return inj

    def __addStep(self, attr=None):
        """Add a concentration injection widget in the injection layout

        :param attr: dictionnary of step attributes
        :type attr: dict

        :return: widget of the step
        :rtype: StepWidget
        """
        step = StepWidget(attr, self)
        self.durationsLayout.addWidget(step)
        return step
