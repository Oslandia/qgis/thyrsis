"""This module is a new python version of mhynos

USAGE
   python -m thyrsis.simulation.compute [-init, -run, -verbose, -mpi] dbname.sqlite

   calcul mhynos in the spatial database dbname.sqlite

OPTIONS

   -init : initialize
   -run : compute
   -verbose : verbose
   -mpi : MPI
"""

import os
import shutil
import subprocess
import sys
from builtins import range, str
from pathlib import Path

from ..database import db_elem_to_node, db_node_to_elem, delete_results, sqlite
from ..log import logger
from ..settings import Settings
from ..utilities import Timer, toSeconds, zns_nodes
from .create_injection_zones import create_injection_zones
from .jobqueue import JobQueue
from .metis import Metis
from .mhynos_compute import mhynos_compute
from .mhynos_database import (
    get_injections_data,
    get_injections_pids,
    get_simulation_data,
)
from .openfoam import OpenFoam

CPERM = 0.9


def readfile(filename):
    """Read sat file

    :param filename: path of the file to read
    :type filename: string

    :return: data (file lines)
    :rtype: list
    """

    data = []
    if os.path.exists(filename):
        logger.debug("Lecture du fichier", filename)
        with open(filename) as fil:
            for line in fil:
                if line[0] == "#" or len(line.split()) == 0:
                    continue
                data.append(float(line))
    return data


def create_mesh_zns(conn, settings, vgamax):
    """Create the Zns mesh

    :param conn: connection on a sqlite database
    :type conn: sqlite3.Connection
    :param settings: Thyrsis settings
    :type settings: Settings
    :param vgamax: max value of Van Genuchten alpha parameter
    :type coef: float

    :return: znodes (list of z coordinate)
    :rtype: list
    """
    logger.debug("mesh_zns")
    timer = Timer()

    cur = conn.cursor()

    assert (
        not cur.execute("SELECT COUNT(1) FROM mailles_zns").fetchone()[0]
        and not cur.execute("SELECT COUNT(1) FROM noeuds_zns").fetchone()[0]
    )

    project_SRID = str(
        cur.execute(
            "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
        ).fetchone()[0]
    )

    (coef_mail,) = cur.execute("SELECT coef_maillage FROM simulations").fetchone()
    if not coef_mail > 0:
        logger.warning(
            "le coefficient de maillage doit être positif : il est pris égal à 5"
        )
        coef_mail = 5

    # find the node with minimal unsaturated zone thickness in the injection zone
    nb_of_injections = cur.execute("SELECT COUNT(1) FROM injections").fetchone()[0]
    data = []
    nodes = []
    znodes = []
    min_id = 1
    max_id = None
    cur.execute(
        """
        SELECT OGC_FID, X(Centroid(GEOMETRY)), Y(Centroid(GEOMETRY)), altitude, epaisseur
        FROM injections ORDER BY OGC_FID
        """
    )
    for pid, x, y, z, epaisseur_zns in cur.fetchall():
        # logger.debug("create_mesh_zns : epaisseur_zns = ", epaisseur_zns)
        mesh = zns_nodes((x, y, z), epaisseur_zns, coef_mail, vgamax)
        nodes += [(pid, s, x, y, z, int(project_SRID)) for (x, y, z, s) in mesh]
        znodes.append([z for (x, y, z, s) in mesh])
        max_id = len(nodes)
        data += [(pid, nid, nid + 1) for nid in range(min_id, max_id)]
        min_id = max_id + 1

    if nb_of_injections:
        logger.debug(timer.reset("creating zns mesh"))

    save_zns = False
    for option in settings.options("Variables"):
        if "ZNS" in option:
            save_zns = save_zns or bool(int(settings.value("Variables", option)))

    if save_zns:

        # cur.execute("PRAGMA journal_mode = OFF")
        # cur.execute("PRAGMA synchronous = OFF")
        cur.execute("SELECT DisableSpatialIndex('noeuds_zns', 'GEOMETRY')")
        cur.execute("DROP INDEX noeuds_zns_pid_idx")
        cur.execute("DROP TABLE idx_noeuds_zns_GEOMETRY")

        cur.executemany(
            "INSERT INTO noeuds_zns(pid, surface, GEOMETRY)\
            VALUES (?, ?, MakePointZ(?, ?, ?, ?))",
            nodes,
        )

        cur.execute("CREATE INDEX noeuds_zns_pid_idx ON noeuds_zns(pid)")
        cur.execute("SELECT CreateSpatialIndex('noeuds_zns', 'GEOMETRY')")
        logger.debug(timer.reset("inserting zns nodes"))

        cur.execute("SELECT DisableSpatialIndex('mailles_zns', 'GEOMETRY')")
        cur.execute("DROP INDEX mailles_zns_pid_idx")
        cur.execute("DROP INDEX mailles_zns_a_idx")
        cur.execute("DROP INDEX mailles_zns_b_idx")
        cur.execute("DROP TABLE idx_mailles_zns_GEOMETRY")

        cur.executemany(
            "INSERT INTO mailles_zns(pid, a, b)\
            VALUES (?, ?, ?)",
            data,
        )

        cur.execute("CREATE INDEX mailles_zns_pid_idx ON mailles_zns(pid)")
        cur.execute("CREATE INDEX mailles_zns_a_idx ON mailles_zns(a)")
        cur.execute("CREATE INDEX mailles_zns_b_idx ON mailles_zns(b)")
        cur.execute("SELECT CreateSpatialIndex('mailles_zns', 'GEOMETRY')")
        logger.debug(timer.reset("inserting zns mailles"))

    return znodes


def create_dates_simulation(conn, tmpdir):
    """Create  the dates simulation file

    :param conn: connection on a sqlite database
    :type conn: sqlite3.Connection
    :param tmpdir: temporary folder path
    :type tmpdir: string

    :return: nbDates number of dates
    :rtype: int
    """

    if os.path.isfile(os.path.join(tmpdir, "dates_simulation.txt")):
        with open(os.path.join(tmpdir, "dates_simulation.txt")) as fil:
            return len(
                [y[0] for y in [x.split() for x in fil.readlines()] if len(y) > 0]
            )

    cur = conn.cursor()
    cur.execute(
        """
        SELECT duree, pas_de_temps
        FROM dates_resultats ORDER BY id"""
    )

    current_time = 0
    total_time = 0
    nbDates = 0
    with open(os.path.join(tmpdir, "dates_simulation.txt"), "w") as file_dates:
        for duree, pas in cur.fetchall():
            duree_seconds = toSeconds(duree)
            pas_seconds = toSeconds(pas)
            total_time += duree_seconds
            while current_time <= total_time:
                file_dates.write("%d\n" % (int(current_time)))
                nbDates += 1
                current_time += pas_seconds
            else:
                continue

    return nbDates


def create_debit_sat(code, sat_dir, injections_data, simulation_data, param):
    """Builds the input file in cas of no unsaturated calculation

    :param code: Simulation code
    :type code: Metis or OpenFoam
    :param sat_dir: saturation path
    :type sat_dir: string
    :param injections_data: list, containing a dictionnary for each injection
    :type injections_data: list
    :param simulation_data: dictionnary of simulation data
    :type simulation_data: dict
    :param param: parameters dictionnary
    :type param: dict
    """

    with sqlite.connect(code.database) as conn:

        cur = conn.cursor()
        tflux = set()
        times = []
        lflux = []
        nodes = []
        nb_nodes = []
        pids = get_injections_pids(conn)
        for inj, pid in enumerate(pids):
            cur.execute(
                """
                SELECT ni.noeud, X(n.GEOMETRY), Y(n.GEOMETRY)
                FROM noeuds_injections as ni, noeuds as n
                WHERE injection = """
                + str(pid)
                + """ AND ni.noeud = n.OGC_FID
                ORDER BY noeud"""
            )
            nodes.append(cur.fetchall())
            nb_nodes.append(len(nodes[-1]))

            t0, t1, flux = calcul_flux(injections_data[inj], simulation_data, param)
            tflux.add(t0)
            tflux.add(t1)
            lflux.append(flux)
            times.append((t0, t1))

    flux = []
    # faut-il écrire une valeur à la date 0 ?
    # if sorted(tflux)[0] > 0:
    #    flux.append((0, []))
    #    for i, pid in enumerate(pids):
    #        for [nid] in nodes[i]:
    #            flux[-1][1].append((nid, 0))

    for time in sorted(tflux):
        flux.append((time, []))
        for i, pid in enumerate(pids):
            for [nid, x, y] in nodes[i]:
                flux[-1][1].append(
                    (
                        nid,
                        x,
                        y,
                        (
                            -lflux[i] / nb_nodes[i]
                            if time >= times[i][0] and time <= times[i][1]
                            else 0
                        ),
                    )
                )

    code.create_debit_sat(sat_dir, flux)


def calcul_flux(injection_data, simulation_data, param):
    """Compute the flux

    :param injection_data: data for a injection
    :type injection_data: dict
    :param simulation_data: dictionnary of simulation data
    :type simulation_data: dict
    :param param: parameters dictionnary
    :type param: dict

    :return t0: begin date
    :rtype t0: date
    :return t1: end date
    :rtype t1: date
    :return flux: flux value (as input or calculated)
    :rtype flux: float
    """
    dfui = injection_data["INFILTRATION"]
    volume_eau = injection_data["VOLUME_EAU"]
    t0 = injection_data["T0"]
    t1 = injection_data["T1"]
    logger.debug("calcul_flux: t0 = ", t0, " t1 = ", t1)
    surface = injection_data["SURFACE"]
    cumul_surface = injection_data["CUMUL_SURFACE"]
    permeabilite = injection_data["PERMEABILITE"]
    logger.debug("permeabilite = ", permeabilite, " m/s")
    input_ = injection_data["INPUT"]
    coef_injection = injection_data["COEF_INJECTION"]
    coef_surface = injection_data["COEF_SURFACE"]

    # TINJ = date de fin d'injection
    injection_data["TINJ"] = t1

    if volume_eau > 0:
        logger.debug("calcul_flux: volume_eau = ", volume_eau)
        # simulation_data['FUITE'] = "oui"
        if t1 and t1 > t0:
            dfui = volume_eau / surface / (t1 - t0)
            logger.debug("calcul_flux: dfui = ", dfui)
            if dfui > CPERM * permeabilite:
                s = volume_eau / CPERM / permeabilite / (t1 - t0)
                logger.notice("la durée d'injection va être allongée")
                logger.notice(
                    "ou bien considérer une surface plus grande de ", s, " m2"
                )
        if not t1 or dfui > CPERM * permeabilite or t1 <= t0:
            dfui = CPERM * permeabilite
            t1 = t0 + volume_eau / surface / dfui
            logger.notice("la durée d'injection est : ", t1 - t0, "secondes")
            injection_data["TINJ"] = t1
    injection_data["DFUI"] = dfui

    if input_ == None or input_ == 0.0:
        flux = None
    elif simulation_data["TYPE_INJECTION"] == "flux":
        if not t1:
            raise RuntimeError("La durée du flux doit être positive")
        flux = input_
    elif simulation_data["TYPE_INJECTION"] == "concentration":
        if not t1:
            raise RuntimeError("La durée d'injection doit être positive")
        flux = input_ * surface * dfui
    elif simulation_data["TYPE_INJECTION"] == "masse":
        flux = dfui * surface * param["SL"]
        injection_data["TINJ"] = t0 + input_ / coef_surface / flux
    else:
        flux = None

    if flux != None:
        if simulation_data["INSATURE"] == "oui":
            # flux = flux*coef_injection*coef_surface/cumul_surface
            flux = flux * coef_injection * coef_surface
        else:
            # FR140613
            # si on injecte un flux sur une surface (de noeuds)
            # différente de la surface de mise en solution
            # (surface du puits par exemple),
            # il n'y a pas de correction à apporter.
            # Il y aura seulement un effet de dilution très local.
            flux = flux * coef_injection * coef_surface

    injection_data["FLUX"] = flux
    return t0, t1, flux


def create_insat_files(
    code, insat_dir, injection_data, simulation_data, param, settings, znodes
):
    """Construct the input files for insaturated calculations

    :param code: Simulation code
    :type code: Metis or OpenFoam
    :param insat_dir: saturation path
    :type insat_dir: string
    :param injections_data: list, containing a dictionnary for each injection
    :type injections_data: list
    :param simulation_data: dictionnary of simulation data
    :type simulation_data: dict
    :param param: parameters dictionnary
    :type param: dict
    :param settings: Thyrsis settings
    :type settings: Settings
    :param znodes: znodes (list of z coordinate)
    :type znodes: list

    :return t0: begin date
    :rtype t0: date
    :return t1: end date
    :rtype t1: date
    :return flux: flux value (as input or calculated)
    :rtype flux: float
    """

    # nombre de noeuds
    injection_data["NNOEUDS"] = len(znodes)
    # altitude de la base de la colonne
    injection_data["ZMIN"] = min(znodes)
    # nombre de mailles
    injection_data["NMAILLES"] = injection_data["NNOEUDS"] - 1

    # noeud d'injection
    injection_node = -1
    z0 = None
    for i, z in enumerate(znodes):
        injection_node = i + 1
        if not z0:
            z0 = z
        if abs(z - z0) >= injection_data["PROFONDEUR"]:
            break
    injection_data["NOEUD_INJ"] = injection_node

    t0, t1, flux = calcul_flux(injection_data, simulation_data, param)

    logger.debug(
        "create_insat_files: epaisseur =",
        max(znodes) - min(znodes),
        "permeabilite = ",
        injection_data["PERMEABILITE"],
    )

    code.create_insat_files(
        insat_dir, simulation_data, injection_data, param, settings, znodes
    )


def check_before_compute(dbname):
    """Return an error if database is not ready for computation

    :param dbname: path to database
    :type dbname: string

    :return: success state
    :rtype: string
    """
    conn = sqlite.connect(dbname)
    if not conn:
        return "Impossible de se connecter à la base %s", dbname
    nbSim = conn.execute("SELECT COUNT(1) FROM simulations").fetchone()[0]
    conn.close()
    if not nbSim:
        return "Aucune simulation définie dans ce projet"
    if nbSim > 1:
        return "Plus d'une simulation définie dans ce projet"

    return None


def update_injections(conn):
    """Update injections data in database

    :param conn: connection on a sqlite database
    :type conn: sqlite3.Connection
    """
    logger.debug("update_injections")

    cur = conn.cursor()
    cur.execute(
        """
          UPDATE injections SET
            permeabilite = (
              SELECT Coalesce(EXP(SUM(LN(m.permeabilite_zns))/COUNT(1)),
                               EXP(SUM(LN(m.permeabilite_x))/COUNT(1)))
              FROM mailles AS m, mailles_injections AS mi
              WHERE mi.injection = injections.OGC_FID
              AND m.OGC_FID = mi.maille ),
            infiltration = (
              SELECT AVG(m.infiltration)
              FROM mailles AS m, mailles_injections AS mi
              WHERE mi.injection = injections.OGC_FID
              AND m.OGC_FID = mi.maille ),
            epaisseur = (
              SELECT AVG(m.epaisseur_zns)
              FROM mailles AS m, mailles_injections AS mi
              WHERE mi.injection = injections.OGC_FID
              AND m.OGC_FID = mi.maille ),
            altitude = (
              SELECT AVG(m.altitude)
              FROM mailles AS m, mailles_injections AS mi
              WHERE mi.injection = injections.OGC_FID
              AND m.OGC_FID = mi.maille )
        """
    )

    cur.execute("DELETE FROM injections_second_milieu")
    cur.execute(
        """
              INSERT INTO injections_second_milieu(OGC_FID, permeabilite, infiltration, epaisseur)
              SELECT
                OGC_FID,
                (
                  SELECT Coalesce(EXP(SUM(LN(n.permeabilite_zns))/COUNT(1)),
                                   EXP(SUM(LN(n.permeabilite_x))/COUNT(1)))
                  FROM noeuds_second_milieu AS n, noeuds_injections AS ni
                  WHERE ni.injection = injections.OGC_FID
                  AND n.OGC_FID = ni.noeud ),
                (
                  SELECT AVG(n.infiltration)
                  FROM noeuds_second_milieu AS n, noeuds_injections AS ni
                  WHERE ni.injection = injections.OGC_FID
                  AND n.OGC_FID = ni.noeud ),
                (
                  SELECT MIN(n.epaisseur_zns)
                  FROM noeuds_second_milieu AS n, noeuds_injections AS ni
                  WHERE ni.injection = injections.OGC_FID
                  AND n.OGC_FID = ni.noeud )
              FROM injections
              WHERE (SELECT COUNT(1) FROM noeuds_second_milieu) > 0
              """
    )


def utilisateurs(dbname):
    """Update db with users files

    :param dbname: path to database
    :type dbname: string
    """
    # traitement des fichiers utilisateur u_*
    logger.debug("traitement des fichiers utilisateur u_*")

    dir_name = os.path.dirname(os.path.abspath(dbname))
    tmpdir = os.path.join(dir_name, os.path.basename(dbname)[:-7] + "_tmp")

    for filename in os.listdir(dir_name):
        if filename[0:2] == "u_":
            if os.path.isdir(filename):
                logger.notice(
                    "Copie du répertoire ",
                    filename,
                    " dans ",
                    os.path.join(tmpdir, filename[2:]),
                )
                shutil.copytree(
                    os.path.join(dir_name, filename), os.path.join(tmpdir, filename[2:])
                )
            else:
                logger.notice(
                    "Copie du fichier ",
                    filename,
                    " dans ",
                    os.path.join(tmpdir, filename[2:]),
                )
                shutil.copyfile(
                    os.path.join(dir_name, filename), os.path.join(tmpdir, filename[2:])
                )

    with sqlite.connect(dbname) as conn:
        cur = conn.cursor()

        for column in ["infiltration", "epaisseur_zns", "permeabilite_zns"]:

            for m, s in [("", ""), ("2", "_second_milieu")]:
                for ext in ["node", "elem"]:
                    table = "noeuds" if ext == "node" else "mailles"
                    filename = os.path.join(
                        dir_name, "u_{}{}.{}.sat".format(column, m, ext)
                    )
                    values = readfile(filename)
                    if len(values) > 0:
                        logger.notice(
                            "user file {} taken into account".format(filename)
                        )
                        cur.executemany(
                            "UPDATE {}{} SET {}=? WHERE OGC_FID=?".format(
                                table, s, column
                            ),
                            ([(p, n + 1) for n, p in enumerate(values)]),
                        )
                        logger.notice(
                            "{} modified by u_{}{}.{}.sat".format(
                                column, column, m, ext
                            )
                        )

                        values = cur.execute(
                            "SELECT {}, OGC_FID FROM {}{} ORDER BY OGC_FID".format(
                                column, table, s
                            )
                        ).fetchall()
                        if ext == "node":
                            db_node_to_elem(cur, column, values)
                        else:
                            db_elem_to_node(cur, column, values)

                    elif column == "permeabilite_zns":
                        cur.execute(
                            "SELECT permeabilite_zns_uniforme FROM simulations{}".format(
                                s
                            )
                        )
                        permeabilite_zns_uniforme = cur.fetchone()
                        if permeabilite_zns_uniforme and permeabilite_zns_uniforme[0]:

                            cur.execute(
                                "UPDATE noeuds%s SET permeabilite_zns = %g"
                                % (s, permeabilite_zns_uniforme[0])
                            )
                            cur.execute(
                                "UPDATE mailles%s SET permeabilite_zns = %g"
                                % (s, permeabilite_zns_uniforme[0])
                            )

        update_injections(conn)

        conn.commit()


def mhynos_init(code, settings, MPI=None):
    """Initialize calculation and data files

    :param code: Simulation code
    :type code: Metis or OpenFoam
    :param settings: Thyrsis settings
    :type settings: Settings
    :param MPI: MPI parameter
    :type MPI: MPI
    """

    num_process = "process " + str(MPI.COMM_WORLD.Get_rank()) if MPI else ""

    logger.notice("\nINITIALISATION", num_process)

    dbname = code.database
    dir_name = os.path.dirname(os.path.abspath(dbname))
    tmpdir = os.path.join(dir_name, os.path.basename(dbname)[:-7] + "_tmp")

    if not MPI or MPI.COMM_WORLD.Get_rank() == 0:

        if os.path.isdir(tmpdir):
            shutil.rmtree(tmpdir)
        os.makedirs(tmpdir)
        logger.debug("tmpdir = ", tmpdir)

        for f in os.listdir(dir_name):
            if ".probes" in f or ".npy" in f:
                os.remove(os.path.join(dir_name, f))

        utilisateurs(dbname)

        with sqlite.connect(dbname) as conn:
            simulation_data = get_simulation_data(conn)
            injections_data = get_injections_data(conn)
            pids = get_injections_pids(conn)

            cur = conn.cursor()
            delete_results(cur)

            nb_of_injections = cur.execute(
                "SELECT COUNT(1) FROM injections"
            ).fetchone()[0]
            second_milieu = (
                cur.execute("SELECT COUNT(1) FROM noeuds_second_milieu").fetchone()[0]
                > 0
            )
            nbDates = create_dates_simulation(conn, tmpdir)
            names = cur.execute("PRAGMA table_info(parametres_simulation)").fetchall()
            param_names = [name[1].upper() for name in names]
            param_values = cur.execute(
                "SELECT %s FROM parametres_simulation ORDER BY id"
                % (", ".join(param_names))
            ).fetchall()
            vgamax = max([x[2] for x in param_values])
            nodes_zns = create_mesh_zns(conn, settings, vgamax)

            conn.commit()

    else:
        nodes_zns = None
        nb_of_injections = None
        second_milieu = None
        nbDates = None
        param_names = None
        param_values = None
        simulation_data = None
        injections_data = None
        pids = None

    if MPI:
        MPI.COMM_WORLD.barrier()
        nodes_zns = MPI.COMM_WORLD.bcast(nodes_zns, root=0)
        nb_of_injections = MPI.COMM_WORLD.bcast(nb_of_injections, root=0)
        second_milieu = MPI.COMM_WORLD.bcast(second_milieu, root=0)
        nbDates = MPI.COMM_WORLD.bcast(nbDates, root=0)
        param_names = MPI.COMM_WORLD.bcast(param_names, root=0)
        param_values = MPI.COMM_WORLD.bcast(param_values, root=0)
        simulation_data = MPI.COMM_WORLD.bcast(simulation_data, root=0)
        injections_data = MPI.COMM_WORLD.bcast(injections_data, root=0)
        pids = MPI.COMM_WORLD.bcast(pids, root=0)

    sat_needed = True
    insat_needed = simulation_data["INSATURE"] == "oui"

    # nb_of_injections = cur.execute("SELECT COUNT(1) FROM injections").fetchone()[0]
    # second_milieu = cur.execute("SELECT COUNT(1) FROM noeuds_second_milieu").fetchone()[0] > 0
    aectran = simulation_data["TYPE_INFILTRATION"] == "transitoire"
    dispersion = simulation_data["TYPE_INJECTION"] != "aucune"

    nsamples = len(param_values)
    sample_dirs = [
        os.path.join(tmpdir, "sample%05d" % (sample_idx + 1))
        for sample_idx in range(nsamples)
    ]

    # les copies de fichiers ne marchent pas en MPI !
    if not MPI or MPI.COMM_WORLD.Get_rank() == 0:

        for sample_dir in sample_dirs:

            if os.path.isdir(sample_dir):
                shutil.rmtree(sample_dir)
            os.makedirs(sample_dir)

            sat_dir = os.path.join(sample_dir, "sature")
            os.makedirs(sat_dir)
            logger.notice(sat_dir, "created")

            shutil.copy2(os.path.join(tmpdir, "dates_simulation.txt"), sat_dir)

            sample_idx = int(os.path.basename(sample_dir)[6:]) - 1
            param = {
                param_names[i]: param_values[sample_idx][i]
                for i in range(len(param_names))
            }

            # fichier infiltration_transitoire.sat
            if aectran and simulation_data["INSATURE"] == "non":
                file_esui = os.path.join(tmpdir, "infiltration_transitoire.sat")
                if os.path.isfile(file_esui):
                    code.convert_infiltration_transitoire_sature(sat_dir)
                else:
                    raise RuntimeError(
                        "Fichier infiltration_transitoire.sat introuvable"
                    )

                if second_milieu:
                    file_esui = os.path.join(tmpdir, "infiltration_transitoire2.sat")
                    if os.path.isfile(file_esui):
                        os.rename(
                            file_esui,
                            os.path.join(sat_dir, "infiltration_transitoire2.sat"),
                        )
                    else:
                        raise RuntimeError(
                            "Fichier infiltration_transitoire2.sat introuvable"
                        )

            # fichier debit_entrant.sat
            file_deb = os.path.join(tmpdir, "debit_entrant.sat")
            if dispersion and simulation_data["INSATURE"] == "non":
                if nb_of_injections > 0:
                    create_debit_sat(
                        code, sat_dir, injections_data, simulation_data, param
                    )
                else:
                    if os.path.isfile(file_deb):
                        os.rename(file_deb, os.path.join(sat_dir, "debit_entrant.sat"))
                    else:
                        raise RuntimeError("Fichier debit_entrant.sat introuvable")

            # fichier debit_eau_entrant.sat
            file_deb = os.path.join(tmpdir, "debit_eau_entrant.sat")
            if os.path.isfile(file_deb):
                os.rename(file_deb, os.path.join(sat_dir, "debit_eau_entrant.sat"))

            if not dispersion and not aectran:
                raise RuntimeError("Pas de dispersion ni d'ecoulement !")

    if MPI:
        MPI.COMM_WORLD.barrier()

    for sample_dir in JobQueue(sample_dirs, MPI):

        if sat_needed:
            sat_dir = os.path.join(sample_dir, "sature")
            logger.debug("initializing saturated", sat_dir)
            if MPI:
                logger.debug("process", MPI.COMM_WORLD.Get_rank(), "building", sat_dir)

            sample_idx = int(os.path.basename(sample_dir)[6:]) - 1
            param = {
                param_names[i]: param_values[sample_idx][i]
                for i in range(len(param_names))
            }

            code.create_sat_files(sat_dir, simulation_data, param, settings)

    if MPI:
        MPI.COMM_WORLD.barrier()

    if insat_needed:

        # apparemment sample_dirs est vide après JobQueue !
        sample_dirs = [
            os.path.join(tmpdir, "sample%05d" % (sample_idx + 1))
            for sample_idx in range(nsamples)
        ]
        insat_dirs = [
            os.path.join(sample_dir, "insat%05d" % (inj + 1))
            for inj in range(len(pids))
            for sample_dir in sample_dirs
        ]

        # les copies de fichiers ne marchent pas en MPI !
        if MPI is None or MPI.COMM_WORLD.Get_rank() == 0:
            for insat_dir in insat_dirs:
                if os.path.isdir(insat_dir):
                    shutil.rmtree(insat_dir)
                os.makedirs(insat_dir)
                logger.notice(insat_dir, "created")

                shutil.copy2(os.path.join(tmpdir, "dates_simulation.txt"), insat_dir)

                # fichier infiltration_transitoire.insat
                file_esui = os.path.join(tmpdir, "infiltration_transitoire.insat")
                if (
                    aectran
                    and simulation_data["TYPE_INJECTION"] == "aucune"
                    and not os.path.exists(file_esui)
                ):
                    raise RuntimeError("Fichier inexistant : %s" % (file_esui))
                elif os.path.exists(file_esui):
                    code.convert_infiltration_transitoire_insature(insat_dir)

        if MPI:
            MPI.COMM_WORLD.barrier()

        for insat_dir in JobQueue(insat_dirs, MPI):
            logger.debug("initializing", insat_dir)
            if MPI:
                logger.debug(
                    "process ", MPI.COMM_WORLD.Get_rank(), "building", insat_dir
                )

            inj = int(os.path.basename(insat_dir)[5:]) - 1
            sample_idx = int(os.path.basename(os.path.dirname(insat_dir))[6:]) - 1
            param = {
                param_names[i]: param_values[sample_idx][i]
                for i in range(len(param_names))
            }

            create_insat_files(
                code,
                insat_dir,
                injections_data[inj],
                simulation_data,
                param,
                settings,
                nodes_zns[inj],
            )


def compute(dbname, init=False, run=False, MPI=None, verbose=False):
    """Launch MHYNOS process

    :param dbname: path to database
    :type dbname: string
    :param init: init flag
    :type init: bool
    :param run: run flag
    :type run: bool
    :param MPI: MPI parameter
    :type MPI: MPI
    :param verbose: verbose flag
    :type verbose: bool

    :return: success state
    :rtype: bool
    """

    if not MPI or MPI.COMM_WORLD.Get_rank() == 0:
        logger.notice("*************************")
        logger.notice("*                       *")
        logger.notice("*      M H Y N O S      *")
        logger.notice("*                       *")
        logger.notice("*  F.Renard 13/02/2024  *")
        logger.notice("*************************")

        logger.notice("reading settings for compute")
        settings = Settings(os.path.dirname(os.path.abspath(dbname)))

    else:
        settings = None

    if MPI:
        MPI.COMM_WORLD.barrier()
        settings = MPI.COMM_WORLD.bcast(settings, root=0)
        logger.notice("Get MPI rank : ", MPI.COMM_WORLD.Get_rank())

    debug = int(settings.value("General", "debug")) or verbose
    logger.set_level("debug" if debug else "notice")

    codeHydro = settings.value("General", "codeHydro")
    codeCommand = settings.value("General", "codeHydroCommand")
    logger.debug("compute:codeHydro =", codeHydro)
    logger.debug("compute:codeCommand =", codeCommand)

    if not codeHydro:
        raise RuntimeError("le code de calcul n'est pas configuré")

    if not MPI or MPI.COMM_WORLD.Get_rank() == 0:
        logger.notice("\n-------------------------")
        logger.notice("CODE HYDRO = ", codeHydro.upper())
        logger.notice("-------------------------\n")

    if codeHydro == "metis" and os.path.exists(codeCommand):
        code = Metis(dbname, codeCommand)
    elif codeHydro == "openfoam":
        code = OpenFoam(dbname, settings)
    else:
        raise NotImplementedError("code de calcul non encore implémenté")

    # if init and (not MPI or MPI.COMM_WORLD.Get_rank() == 0):
    if not MPI or MPI.COMM_WORLD.Get_rank() == 0:
        check_injection_zones(dbname)

    if MPI:
        MPI.COMM_WORLD.barrier()

    if init:
        mhynos_init(code, settings, MPI)

    if MPI:
        MPI.COMM_WORLD.barrier()

    if run:
        mhynos_compute(code, settings, MPI)

    if not MPI or MPI.COMM_WORLD.Get_rank() == 0:
        logger.notice("*************************")
        logger.notice("*  F I N   M H Y N O S  *")
        logger.notice("*************************")

    if MPI:
        MPI.Finalize()

    return 0


def check_injection_zones(dbname):
    """Checks if injection zones must be created and creates them"

    :param dbname: path to database
    :type dbname: string
    """

    with sqlite.connect(dbname) as conn:
        (nb_of_injections,) = (
            conn.cursor().execute("SELECT COUNT(1) FROM injections").fetchone()
        )
        logger.debug("check_injection_zones : nb_of_injections = ", nb_of_injections)
        simulation_data = get_simulation_data(conn)

    # création des zones d'injection en insaturé si pas de zone d'injection existante
    if simulation_data["INSATURE"] == "oui" and not nb_of_injections:
        create_injection_zones(dbname)


def compute_mpi(dbname, init=True, run=True):
    """Launch parrallel process as configured

    :param dbname: path to database
    :type dbname: string
    :param init: init flag
    :type init: bool
    :param run: run flag
    :type run: bool
    """

    options = []

    # il faut commencer par créer les injections si nécessaire
    if init:
        check_injection_zones(dbname)
        options.append("-init")

    if run:
        options.append("-run")

    logger.debug("reading settings for compute_mpi")
    dbname_dir = os.path.dirname(os.path.abspath(dbname))
    settings = Settings(dbname_dir)

    nbColumnsMax = int(settings.value("General", "nbColumnsMax"))
    (nbColumns,) = (
        sqlite.connect(dbname)
        .cursor()
        .execute("SELECT COUNT(1) from injections")
        .fetchone()
    )
    (nbSimus,) = (
        sqlite.connect(dbname)
        .cursor()
        .execute("SELECT COUNT(1) from parametres_simulation")
        .fetchone()
    )
    for option in settings.options("Variables"):
        if nbColumns > nbColumnsMax and "ZNSCheck" in option:
            settings.setValue("Variables", option, 0)

    # number of available processors
    nprocs = 0
    mpiconfig_file = settings.value("General", "mpiConfig")
    mpi_process = settings.value("General", "mpiProcess")
    if os.path.exists(mpiconfig_file):
        with open(mpiconfig_file) as fil, open(
            os.path.join(dbname_dir, ".hosts"), "w"
        ) as hostfil:
            for line in fil:
                if line.split() == []:
                    continue
                spl = line.split()[0].split(":")
                if len(spl) > 1:
                    if sys.platform.startswith("win"):
                        if mpi_process == "msmpi":
                            host_file = os.path.join(dbname_dir, ".hosts")
                            out = subprocess.Popen(
                                ["ping", "-n", "1", spl[0]],
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT,
                            )
                        else:
                            host_file = (
                                os.path.join(dbname_dir, ".hosts")
                                .replace(
                                    "C:\\Users\\" + os.environ["USERNAME"],
                                    "/mnt/c/Users/" + os.environ["USERNAME"],
                                )
                                .replace(os.sep, "/")
                            )
                            out = subprocess.Popen(
                                ["bash", "~", "--login", "-c"]
                                + ["ping -c 1 " + spl[0]],
                                stdout=subprocess.PIPE,
                                stderr=subprocess.STDOUT,
                            )
                    else:
                        host_file = os.path.join(dbname_dir, ".hosts")
                        out = subprocess.Popen(
                            ["ping", "-c", "1", spl[0]],
                            stdout=subprocess.PIPE,
                            stderr=subprocess.STDOUT,
                        )
                    stdout, stderr = out.communicate()
                    if (
                        "unknown" in str(stdout).lower()
                        or "unreachable" in str(stdout).lower()
                    ):
                        logger.notice("host", spl[0], "is not reachable")
                    else:
                        logger.notice("host", spl[0], "is reachable")
                        nprocs += int(spl[1])
                        if mpi_process == "hydra":
                            hostfil.write(line)
                        elif mpi_process == "openmpi":
                            hostfil.write(spl[0] + " slots=" + spl[1])
                        elif mpi_process == "msmpi":
                            hostfil.write(line.replace(":", " "))

        logger.notice(nprocs, "available processors")

    if nbColumns * nbSimus == 1:
        numprocs = 1
    else:
        numprocs = min(
            nprocs,
            int(settings.value("General", "nbMpiProcess")),
            max(nbColumns, 1) * nbSimus + 1,
        )

    if (
        os.path.exists(os.path.join(dbname_dir, ".hosts"))
        and mpi_process
        and numprocs > 1
    ):
        home_dir = (
            os.environ.get("HOME")
            if "HOME" in os.environ
            else os.environ.get("HOMEPATH")
        )
        thyrsis_dir = os.path.join(home_dir, ".thyrsis")
        if not os.path.isdir(thyrsis_dir):
            os.makedirs(thyrsis_dir)

        numprocs = min(nprocs, numprocs)

        logger.notice(
            f"computing with mpi (process : {mpi_process}) and  {numprocs} processors"
        )
        if mpi_process == "msmpi":
            cmd = (
                [
                    f"{os.environ['MSMPI_BIN']}mpiexec",
                    "-n",
                    str(numprocs),
                    "-machinefile",
                    host_file,
                    "-genvlist",
                    "PYTHONPATH,PYTHONHOME,PATH",
                    "python",
                    "-m",
                    "thyrsis.simulation.compute",
                ]
                + options
                + ["-mpi", dbname]
            )
        else:
            if mpi_process == "hydra":
                cmd = (
                    [
                        "mpiexec.hydra",
                        "-n",
                        str(numprocs),
                        "-f",
                        host_file,
                        "python",
                        "-m",
                        "thyrsis.simulation.compute",
                    ]
                    + options
                    + ["-mpi", dbname]
                )
            elif mpi_process == "openmpi":
                cmd = (
                    [
                        "mpiexec.openmpi",
                        "-n",
                        str(numprocs),
                        "--hostfile",
                        host_file,
                        "-x",
                        "PYTHONPATH",
                        "python",
                        "-m",
                        "thyrsis.simulation.compute",
                    ]
                    + options
                    + ["-mpi", dbname]
                )

        # Add thyrsis to PYTHON PATH
        my_env = os.environ.copy()
        current_dir = Path(os.path.dirname(os.path.realpath(__file__)))
        thyrsis_path = str(current_dir.parent.parent)
        my_env["PYTHONPATH"] = (
            my_env["PYTHONPATH"] + os.pathsep + thyrsis_path
            if "PYTHONPATH" in my_env
            else thyrsis_path
        )

        # Add QGIS to PYTHONPATH
        for path in sys.path:
            my_env["PYTHONPATH"] = my_env["PYTHONPATH"] + os.pathsep + path

        logger.debug("compute_mpi:cmd =", " ".join(cmd))
        with subprocess.Popen(
            cmd,
            env=my_env,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            encoding="utf-8",
            errors="ignore",
        ) as process:
            for line in process.stdout:
                logger.error(line.strip())

            for line in process.stderr:
                logger.debug(line.strip())
            returncode = process.wait()
        logger.debug("project returncode =", returncode)
    else:
        logger.notice("computing without mpi")
        returncode = compute(dbname, init, run)
    return 0


# run as script if invoqued as such
if __name__ == "__main__":
    import sys

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    logger.enable_console(True)

    if len(sys.argv) not in [2, 3, 4, 5, 6]:
        logger.error("wrong number of arguments (try compute.py --help)")
        exit(1)

    logger.debug("computing with args", sys.argv[1:])

    init = False
    run = False
    verbose = False
    dbname = None
    mpi = False
    for arg in sys.argv[1:]:
        if arg == "-init":
            init = True
        elif arg == "-run":
            run = True
        elif arg == "-verbose":
            verbose = True
        elif arg == "-mpi":
            mpi = True
        elif arg[-7:] == ".sqlite":
            dbname = arg

    if not dbname:
        logger.error(
            "cannot find a '.sqlite' file in arguments (try python -m thyrsis.simulation.compute --help)"
        )
        exit(1)

    if mpi:
        try:
            from mpi4py import MPI
        except Exception:
            import site

            from thyrsis.__about__ import DIR_PLUGIN_ROOT

            site.addsitedir(DIR_PLUGIN_ROOT / "embedded_external_libs")
            from thyrsis.embedded_external_libs.mpi4py import MPI
    else:
        MPI = None

    err = check_before_compute(dbname)
    if err:
        logger.error(err)
        exit(1)

    logger.set_level("notice")
    if verbose:
        logger.set_level("debug")

    compute(dbname, init, run, MPI, verbose)
