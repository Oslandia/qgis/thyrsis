# English documentation

Welcome to the THYRSIS plugin documentation !

The documentation is divided in 4 main parts:

- the first chapter provides a **General description**  of the THYRSIS plugin,
- the chapter **Installation**  explains how to install the THYRSIS plugin on Linux and Windows platforms,
- the chapter **Tutorial** describes how to build a new hydrogeologic model and how to use some basic THYRSIS functionalities,
- the **User manual** provides all details on both graphical interface and theoretical basis,

```{toctree}
---
caption: User guide
maxdepth: 2
---
introduction
installation
tutorial
manual
appendices/index
```
