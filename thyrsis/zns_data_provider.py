import os
from builtins import str

import numpy
from qgis.core import QgsDataSourceUri
from qgis.PyQt.QtCore import QObject, pyqtSignal

from thyrsis.exception import ZnsDisplayError

from .database import sqlite
from .log import logger
from .settings import Settings
from .utilities import unitConversionFactor


class ZnsDataProvider(QObject):

    dataChanged = pyqtSignal()
    columnChanged = pyqtSignal(str)
    unitsChanged = pyqtSignal(str)

    sourceUnits = {
        "concentration": "kg/m³",
        "activity": "Bq/kg",
        "vitesse_darcy_norme": "m/s",
        "saturation": "",
    }

    displayName = {
        "concentration": "Concentration",
        "activity": "Activité",
        "vitesse_darcy_norme": "Vitesse de Darcy",
        "saturation": "Saturation",
    }

    def __init__(self, uri, parent=None):
        super(ZnsDataProvider, self).__init__(parent)
        self.uri = QgsDataSourceUri(uri)
        if not self.uri.database():
            raise ZnsDisplayError("No database defined for ZnsDataProvider")
        if self.uri.database()[-7:] != ".sqlite":
            raise ZnsDisplayError("Database suffix must be .sqlite")
        self.__database = str(self.uri.database())
        if not self.__database:
            raise RuntimeError("Missing dbname in uri:" + uri)

        self.__settings = Settings(os.path.dirname(os.path.abspath(self.__database)))
        self.__dates = []
        self.__xyz = None
        self.__labels = []
        self.__mailles = []
        self.__geometry = []
        with sqlite.connect(self.__database, "ZnsDataProvider:init") as conn:
            cur = conn.cursor()
            cur.execute("SELECT date FROM dates_simulation ORDER BY id")
            for [date] in cur.fetchall():
                self.__dates.append(date)
            c = cur.execute(
                """SELECT activite_specifique
                 FROM elements_chimiques WHERE id = (SELECT eid FROM simulations)"""
            ).fetchone()
            self.__activite_specifique = c[0] if c else 0.0
            (insature,) = cur.execute("SELECT insature FROM simulations").fetchone()
            if insature == "oui":
                self.__xyz = cur.execute(
                    """SELECT X(GEOMETRY), Y(GEOMETRY), Z(GEOMETRY)
                 FROM noeuds_zns ORDER BY OGC_FID"""
                ).fetchall()
                self.__labels = cur.execute(
                    "SELECT nom FROM injections ORDER BY OGC_FID"
                ).fetchall()
                for [pid] in cur.execute(
                    "SELECT OGC_FID FROM injections ORDER BY OGC_FID"
                ).fetchall():
                    self.__mailles.append(
                        cur.execute(
                            "SELECT a, b FROM mailles_zns WHERE pid = %d ORDER BY OGC_FID"
                            % (pid)
                        ).fetchall()
                    )
                self.__geometry = cur.execute(
                    "SELECT AsText(GEOMETRY) FROM injections ORDER BY OGC_FID"
                ).fetchall()

        self.__didx = 0
        self.__nodeValues = numpy.array([], dtype=numpy.float32)
        self.__units = ""
        self.__unitConvertionFactor = 1.0

        available = self.availableColumns()
        if self.uri.hasParam("resultColumn"):
            self.setResultColumn(str(self.uri.param("resultColumn")))
        elif len(available):
            self.setResultColumn(available[0][1])

    def settings(self):
        """Return Thyrsis settings

        :return: Thyrsis settings
        :rtype: Settings
        """
        return self.__settings

    def setUnits(self, units):
        """Set units

        :param units: units
        :type units: string
        """
        self.__units = units
        sourceUnits = ZnsDataProvider.sourceUnits[self.resultColumn()]
        if units != sourceUnits:
            self.__unitConvertionFactor = unitConversionFactor(
                sourceUnits, units, self.__activite_specifique
            )
        else:
            self.__unitConvertionFactor = 1.0
        self.unitsChanged.emit(units)

    def setResultColumn(self, columnName):
        """Set the current column

        :param columnName: columnName
        :type columnName: string
        """
        reloadNeeded = columnName and (
            self.resultColumn() != columnName or not len(self.__nodeValues)
        )
        if self.uri.hasParam("resultColumn"):
            self.uri.removeParam("resultColumn")
        self.uri.setParam("resultColumn", columnName)
        if reloadNeeded:
            basename = self.uri.database()[:-7]
            if os.path.exists(basename + "." + self.resultColumn() + ".insat.npy"):
                self.__nodeValues = numpy.load(
                    basename + "." + self.resultColumn() + ".insat.npy"
                )
            else:
                self.__nodeValues = numpy.array([], dtype=numpy.float32)
            if columnName == "concentration":
                self.setUnits(self.__settings.value("Variables", "concentrationUnit"))
            elif columnName == "vitesse_darcy_norme":
                self.setUnits(self.__settings.value("Variables", "darcyUnit"))
            elif columnName == "saturation":
                self.setUnits("")
            elif columnName == "activity":
                self.setUnits("Bq/kg")
            self.columnChanged.emit(columnName)

    def availableColumns(self):
        """returns a list (display name, colummn name)
        for available results

        :return: available column
        :rtype: list
        """
        basename = self.uri.database()[:-7]
        return [
            (ZnsDataProvider.displayName[column], column)
            for column in list(ZnsDataProvider.sourceUnits.keys())
            if os.path.exists(basename + "." + column + ".insat.npy")
        ]

    def availableUnits(self):
        """Return available units

        :return: available units
        :rtype: dict
        """
        return (
            {
                "concentration": [
                    "kg/L",
                    "g/L",
                    "mg/L",
                    "µg/L",
                    "kg/m³",
                    "g/m³",
                    "mg/m³",
                    "µg/m³",
                    "GBq/L",
                    "MBq/L",
                    "kBq/L",
                    "Bq/L",
                    "mBq/L",
                    "µBq/L",
                    "GBq/m³",
                    "MBq/m³",
                    "kBq/m³",
                    "Bq/m³",
                    "mBq/m³",
                    "µBq/m³",
                ],
                "activity": [
                    "GBq/kg",
                    "MBq/kg",
                    "kBq/kg",
                    "Bq/kg",
                    "mBq/kg",
                    "µBq/kg",
                    "Bq/g",
                    "Bq/mg",
                    "Bq/µg",
                ],
                "vitesse_darcy_norme": ["m/s", "m/s", "m/an", "cm/j"],
                "saturation": [""],
            }[self.resultColumn()]
            if self.resultColumn()
            else []
        )

    def resultColumn(self):
        """Return current column

        :return: current column
        :rtype: string
        """
        return str(self.uri.param("resultColumn"))

    def units(self):
        """Return current units

        :return: units
        :rtype: string
        """
        return self.__units

    def setDate(self, didx):
        """set list of dates in case node values vary with time

        :param dates: list of dates
        :type dates: list
        """
        if didx >= len(self.__dates):
            raise ZnsDisplayError(
                f"date idx {didx} not in range {self.__dates} for {self.dataSourceUri()}"
            )
        self.__didx = didx
        self.dataChanged.emit()

    def date(self):
        """Return a current date

        :return: dates list
        :rtype: list
        """
        return self.__didx

    def dates(self):
        """return a list of dates in case node values vary with time

        :return: list of dates
        :rtype: list"""
        return self.__dates

    def maxValue(self, index=None):
        """Return max mesh value

        :param index: node index
        :type index: dict

        :return: max value
        :rtype: float
        """
        if not len(self.__nodeValues):
            return float("NaN")
        return self.__unitConvertionFactor * (
            numpy.max(self.__nodeValues[index])
            if index
            else numpy.max(self.__nodeValues)
        )

    def minValue(self, index=None):
        """Return min mesh value

        :param index: node index
        :type index: dict

        :return: min value
        :rtype: float
        """
        if not len(self.__nodeValues):
            return float("NaN")
        return self.__unitConvertionFactor * (
            numpy.min(self.__nodeValues[index])
            if index
            else numpy.min(self.__nodeValues)
        )

    def dataSourceUri(self, expandAuthConfig=False):
        """Return uri

        :return: uri
        :rtype: string"""
        return self.uri.uri()

    def nodeCoord(self):
        """return a list of coordinates

        :return: return a list of coordinates
        :rtype: ndarray
        """
        if self.__xyz:
            return numpy.require(self.__xyz, numpy.float32, "F")
        else:
            return numpy.array([])

    def nodeValues(self):
        """return values at nodes

        :return: values at nodes
        :rtype: ndarray
        """
        if len(self.__nodeValues):
            return self.__unitConvertionFactor * self.__nodeValues[self.__didx]
        else:
            return numpy.array([], dtype=numpy.float32)

    def labels(self):
        """Return labels

        :return: labels
        :rtype: list
        """
        return [name for [name] in self.__labels]

    def bars(self):
        """return a list of numpy arrays, one array per injection point

        :return: bars
        :rtype: list
        """
        bars = []
        for maille in self.__mailles:
            idx = numpy.require(maille, numpy.int32, "F").reshape((-1,))
            if not len(idx):
                raise ZnsDisplayError("Missing index for bar definition")
            idx -= 1
            bars.append(idx)
        return bars

    def barsNodes(self):
        """return a sorted list of bar nodes

        :return: bars
        :rtype: list
        """
        bars = []
        for bar in self.bars():
            bars.append(numpy.unique(bar))
        return bars

    def contours(self):
        """Return a map with the contour (name, group) as key and an array of vertices
        as values, contours are 3D

        :return: contours
        :rtype: dict
        """
        contours = []
        for (multipoly,) in self.__geometry:
            multipoly = multipoly.replace(", ", ",")
            contours.append([])
            for poly in multipoly.split(")),(("):
                contours[-1].append([])
                for ring in poly.split("),("):
                    ring = (
                        ring.replace("MULTIPOLYGON", "")
                        .replace("(", "")
                        .replace(")", "")
                        .split(",")
                    )
                    contours[-1][-1].append(
                        numpy.array(
                            [
                                (float(x), float(y))
                                for x, y in [point.split() for point in ring]
                            ]
                        )
                    )
        return contours
