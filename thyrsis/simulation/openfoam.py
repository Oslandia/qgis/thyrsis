"""OPENFOAM specific functions
"""

import os
import re
import shutil
import stat
import sys
from builtins import map, object, range
from math import sqrt
from pathlib import Path
from subprocess import PIPE, STDOUT, Popen
from typing import Optional

import numpy

from ..database import db_elem_to_node, elem_to_node, sqlite, table_exists
from ..log import logger
from ..utilities import Timer, convert_nodes, read_one_column_file
from .mhynos_database import get_simulation_data

EPAISSEUR_2D = 100.0
END_TIME_STEADY = 1000

class OpenFoam(object):
    """OpenFoam process class"""

    def __init__(self, database, settings):
        """Constructor

        :param database: database
        :type database: string
        """
        self._settings = settings
        self.codename = "openfoam"
        self.database = database
        self.has_second_milieu = False
        self.noeuds = self.get_nodes()
        self.mailles = self.get_mailles()

        self.variables_sat = {
            "concentration": "mailles",
            "potentiel": "mailles",
            "darcy": "mailles",
            "debit": "contour",
        }
        self.variables_sat2 = {
            "concentration2": "mailles",
            "potentiel2": "mailles",
            "darcy2": "mailles",
            "debit2": "contour",
        }
        self.variables_insat = {
            "saturation": False,
            "saturation_perm": False,
            "darcy": False,
            "darcy_perm": False,
            "concentration": False,
            "activity": False,
        }
        self.variables_dictionary = {
            "concentration": "C",
            "potentiel": "potential",
            "darcy": "U",
            "darcy_perm": "U",
            "saturation": "theta",
            "saturation_perm": "theta",
            "flux_eau": "seepageTerm",
            "debit": "CmassBalance.csv",
            "debit_eau": "waterMassBalance.csv",
        }

        self.points_interet = False
        # nearest nodes of points of interest in order to compute at this points
        if table_exists(self.database, "points_interet"):
            self.points_interet = True

    def __del__(self):
        return

    def get_nodes(self):
        """Return nodes

        :return: list of node ids
        :rtype: list
        """
        with sqlite.connect(self.database) as conn:
            return [
                x[0]
                for x in conn.cursor()
                .execute("SELECT OGC_FID FROM noeuds ORDER BY OGC_FID")
                .fetchall()
            ]

    def get_mailles(self):
        """Return nodes

        :return: list of mesh element ids and coords
        :rtype: list
        """
        with sqlite.connect(self.database) as conn:
            return (
                conn.cursor()
                .execute("SELECT OGC_FID, a, b, c, d FROM mailles ORDER BY OGC_FID")
                .fetchall()
            )

    def get_number_mailles_per_node(self):
        """Return number of mesh connected to each node

        :return: dict giving the number of mesh connected to each node
        :rtype: dict
        """
        dic = {n: 0 for n in self.noeuds}
        for [mid, a, b, c, d] in self.mailles:
            dic[a] += 1
            dic[b] += 1
            dic[c] += 1
            if d:
                dic[d] += 1

        return dic

    def get_options(self):
        """specific options for OPENFOAM

        :return: OpenFoam options
        :rtype: dict
        """

        with sqlite.connect(self.database) as conn:
            (
                idx,
                delta_time_init,
                h_tolerance,
                h_tolerance_relative,
                h_relaxationFactor,
                h_relaxationFactor_transient,
                picard_tolerance,
                picard_tolerance_transient,
                picard_maxiter,
                picard_maxiter_transient,
                newton_tolerance,
                newton_maxiter,
                dtfact_decrease,
                truncationError,
                fluid_density,
                dynamic_viscosity,
                molecular_diffusion,
                tortuosity,
                potential_tolerance,
                potential_tolerance_relative,
                potential_residualControl,
                potential_relaxationFactor,
                hwatermin,
                specific_storage,
            ) = (
                conn.cursor().execute("SELECT * FROM openfoam_options").fetchone()
            )

        # delta_time_init = 10.

        # --algorithmic parameters
        # h_tolerance = 1e-12
        # h_tolerance_relative = 0.01
        # h_relaxationFactor = 0.01
        # h_relaxationFactor_transient = 0.2
        # picard_tolerance = 1e-08
        # picard_tolerance_transient = 1e-01
        # picard_maxiter = 20
        # picard_maxiter_transient = 3
        # newton_tolerance = 1e-08
        # newton_maxiter = 20

        # --time step managing
        # dtfact_decrease = 0.8
        # truncationError = 0.01

        # --properties
        # fluid_density = 1.e3    # rho
        # dynamic_viscosity = 1.e-3   # mu (Pa.s = kg/m/s)
        # molecular_diffusion = 0
        # tortuosity = 1.

        # potential_tolerance = 1e-9
        # potential_tolerance_relative = 0.1
        # potential_residualControl = 1e-8
        # potential_relaxationFactor = 0.5
        # hwatermin = 0.1

        # specific_storage = 0.

        return {
            "delta_time_init": delta_time_init,
            "h_tolerance": h_tolerance,
            "h_tolerance_relative": h_tolerance_relative,
            "h_relaxationFactor": h_relaxationFactor,
            "h_relaxationFactor_transient": h_relaxationFactor_transient,
            "picard_tolerance": picard_tolerance,
            "picard_tolerance_transient": picard_tolerance_transient,
            "picard_maxiter": picard_maxiter,
            "picard_maxiter_transient": picard_maxiter_transient,
            "newton_tolerance": newton_tolerance,
            "newton_maxiter": newton_maxiter,
            "dtfact_decrease": dtfact_decrease,
            "truncationError": truncationError,
            "fluid_density": fluid_density,
            "dynamic_viscosity": dynamic_viscosity,
            "molecular_diffusion": molecular_diffusion,
            "tortuosity": tortuosity,
            "potential_tolerance": potential_tolerance,
            "potential_tolerance_relative": potential_tolerance_relative,
            "potential_residualControl": potential_residualControl,
            "potential_relaxationFactor": potential_relaxationFactor,
            "hwatermin": hwatermin,
            "specific_storage": specific_storage,
        }

    def get_hynverse_options(self):
        """specific options for inversion with OPENFOAM


        :return: OpenFoam options
        :rtype: dict
        """

        with sqlite.connect(self.database) as conn:
            (
                idx,
                fluid_density,
                dynamic_viscosity,
                potential_tolerance,
                potential_tolerance_relative,
                potential_residualControl,
                potential_relaxationFactor,
                hwatermin,
            ) = (
                conn.cursor()
                .execute("SELECT * FROM openfoam_hynverse_options")
                .fetchone()
            )

        # --properties
        # fluid_density = 1.e3    # rho
        # dynamic_viscosity = 1.e-3   # mu (Pa.s = kg/m/s)

        # potential_tolerance = 1e-8
        # potential_tolerance_relative = 0.1
        # potential_residualControl = 1e-6
        # potential_relaxationFactor = 0.5
        # hwatermin = 0.1

        return {
            "fluid_density": fluid_density,
            "dynamic_viscosity": dynamic_viscosity,
            "potential_tolerance": potential_tolerance,
            "potential_tolerance_relative": potential_tolerance_relative,
            "potential_residualControl": potential_residualControl,
            "potential_relaxationFactor": potential_relaxationFactor,
            "hwatermin": hwatermin,
        }

    def nok(self, log_file):
        """returns False if the OPENFOAM computation is OK, by testing the End string in the log file

        :param log_file: log file path
        :type log_file: string

        :return: success state
        :rtype: bool
        """
        with open(log_file, "r") as fil:
            lines = fil.readlines()
            if len(lines) >= 10:
                for i in range(10):
                    if "End" in lines[-i]:
                        return False

            logger.notice(
                "OPENFOAM calculation aborted : last lines of file " + log_file + " :\n"
            )
            for i in range(20):
                logger.notice(lines[-20 + i][:-1])

            return True
            # raise RuntimeError("OPENFOAM calculation aborted : refer back to file "+log_file)

    @staticmethod
    def read_openfoam_file(file_name, norm=True, nbVal=None):
        """reads an OPENFOAM file and returns list of values

        :param file_name: path of openfoam file
        :type file_name: string
        :param norm: norm flag
        :type norm: bool
        :param nbVal: number of values
        :type nbVal: integer

        :return: number of cells
        :rtype: int
        """
        logger.debug("reading", file_name)
        nbValues = 0
        values = []
        flag_continue = False
        with open(file_name, "r") as fil:
            for line in fil:
                if line.replace("\n", "").isdigit():
                    nbValues = int(line.replace("\n", ""))
                    next(fil)
                    flag_continue = True
                    break

                if "internalField" in line:
                    if " uniform" in line:
                        compute_dir = os.path.dirname(os.path.dirname(file_name))
                        nbValues = (
                            OpenFoam.get_ncells(compute_dir) if not nbVal else nbVal
                        )
                        return [float(line.split()[2].replace(";", ""))] * nbValues
                    elif "nonuniform" in line and len(line.split()) > 3:
                        nbValues = int(line.split()[3].split("(")[0])
                        if "((" in line:
                            values = (
                                re.search("(\(\(.*?\)\))", line)
                                .group(1)[2:-2]
                                .split(") (")
                            )
                        else:
                            values = re.search("(\(.*?\))", line).group(1)[1:-1].split()
                        break
                    elif "nonuniform" in line and len(line.split()) == 3:
                        nbValues = int(next(fil).replace("\n", ""))
                        next(fil)
                        flag_continue = True
                        break

            if flag_continue:
                for line in fil:
                    values.append(
                        line.replace("\n", "").replace("(", "").replace(")", "")
                    )

        res = [list(map(float, x.split())) for x in values[0:nbValues]]

        name = os.path.basename(file_name).split(".")[0]
        if norm and len(res[0]) == 3:
            return [sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]) for x in res]
        elif len(res[0]) == 1:
            return [x[0] for x in res]
        else:
            return res

    def file_exists(self, result_file):
        """returns True if the result file exists, with the name connections of variables_dictionary

        :return: path of result file
        :rtype: string
        """
        compute_dir = os.path.dirname(result_file)
        name = os.path.basename(result_file)

        if name.split(".")[0] not in self.variables_dictionary:
            return False
        else:
            name_openfoam = self.variables_dictionary[name.split(".")[0]]

        times_dir_names = OpenFoam.times_dir_names(compute_dir)

        if not times_dir_names:
            return False
        if "_perm" in name:
            if "0" in times_dir_names:
                return os.path.exists(os.path.join(compute_dir, "0", name_openfoam))
            else:
                return False
        elif name[:5] == "debit":
            return os.path.exists(os.path.join(compute_dir, name_openfoam))
        else:
            exists = True
            for date in times_dir_names:
                exists &= os.path.exists(os.path.join(compute_dir, date, name_openfoam))
            return exists

    def read_mesh_insat(self, insat_dir):
        """reads the OPENFOAM points file and returns the nodes number and the nodes surfaces

        :param insat_dir: path of insaturated folder
        :type insat_dir: string

        :return nnodes: nodes number
        :rtype nnodes: list
        :return surfaces: nodes surfaces
        :rtype surfaces: list
        """

        points_file = os.path.join(insat_dir, "constant", "polyMesh", "points")

        znodes = sorted(
            list(set([x[-1] for x in OpenFoam.read_openfoam_file(points_file, False)]))
        )
        znodes.reverse()
        nnodes = len(znodes)

        surfaces = (
            [0.5 * (znodes[0] - znodes[1])]
            + [0.5 * (znodes[i - 1] - znodes[i + 1]) for i in range(1, nnodes - 1)]
            + [0.5 * (znodes[-2] - znodes[-1])]
        )
        return nnodes, surfaces

    @staticmethod
    def get_meshid(compute_dir):
        """returns the list of cells renumbered by renumberMesh

        :param compute_dir: path of compute folder
        :type compute_dir: string

        :return: list of cells
        :rtype: list
        """
        meshid_file = os.path.join(compute_dir, "constant", "meshid")
        if os.path.exists(meshid_file):
            return [int(x) for x in OpenFoam.read_openfoam_file(meshid_file)]
        else:
            return list(range(OpenFoam.get_ncells(compute_dir)))

    @staticmethod
    def get_ncells(compute_dir):
        """returns the number of cells

        :param compute_dir: path of compute folder
        :type compute_dir: string

        :return: number of cells
        :rtype: int
        """
        return int(
            1
            + max(
                OpenFoam.read_openfoam_file(
                    os.path.join(compute_dir, "constant", "polyMesh", "owner")
                )
            )
        )

    def read_potential_at_nodes(self, sat_dir, dates):
        """Reads the potential at node for each dates

        :param sat_dir: path of insaturated folder
        :type sat_dir: string
        :param dates: list of dates
        :type dates: list

        :return: result
        :rtype: numpy.ndarray
        """
        potentiel_mesh = self.read_file(os.path.join(sat_dir, "potentiel.sat"), dates)
        potentiel_node = []
        for i in range(potentiel_mesh.shape[0]):
            elem_values = [
                (potentiel_mesh[i, j], j + 1) for j in range(potentiel_mesh.shape[1])
            ]
            potentiel_node.append(
                [x[0] for x in elem_to_node(self.noeuds, self.mailles, elem_values)]
            )
        return numpy.array(potentiel_node)

    def read_file(self, result_file, dates, convert=False):
        """reads an OPENFOAM results file

        :param result_file: path of the result file
        :type result_file: string
        :param dates: list of dates
        :type dates: list
        :param convert: convert flag to get ZNS columns data
        :type convert: bool

        :return: result
        :rtype: numpy.ndarray
        """
        logger.notice("converting formated", result_file)
        name_file = os.path.basename(result_file)
        if name_file[:5] == "debit":
            return self.read_debit(result_file, dates)
        else:
            return self.read_file_formate(result_file, dates, convert)

    def read_debit(self, result_file, dates):
        """reads saturated CmassBalance.csv file

        :param result_file: path of the result file
        :type result_file: string
        :param dates: list of dates
        :type dates: list

        :return: results
        :rtype: numpy.ndarray
        """

        compute_dir = os.path.dirname(result_file)
        filename = os.path.join(
            compute_dir,
            self.variables_dictionary[os.path.basename(result_file).split(".")[0]],
        )

        nbDates = len(dates)

        ldates = []
        lflux = []
        logger.debug("reading", filename)
        with open(filename) as fil:
            for line in fil:
                if line[0] != "#" and line.split() != []:
                    ldates.append(float(line.split()[0]))
                    lflux.append(float(line.split()[3]) + float(line.split()[4]))

        results = numpy.zeros((nbDates, 1), dtype=numpy.float32)
        if ldates[0] in dates:
            dmin = dates.index(ldates[0])
        else:
            raise RuntimeError("read_debit: openfoam first date not recognized")

        logger.debug("nbDates, dmin = ", nbDates, dmin)
        results[dmin:, 0] = lflux

        logger.debug("debit shape = ", results.shape)
        return results

    def read_file_formate(self, result_file, dates, convert=False):
        """reads an OPENFOAM results file from time directories
        and adds values in case of missing dates

        :param result_file: path of the result file
        :type result_file: string
        :param dates: list of dates
        :type dates: list
        :param convert: convert flag to get ZNS columns data
        :type convert: bool

        :return: results
        :rtype: numpy.ndarray
        """

        logger.debug("reading", result_file)
        compute_dir = os.path.dirname(result_file)
        name_file = os.path.basename(result_file)
        if name_file.split(".")[0] not in self.variables_dictionary:
            raise RuntimeError("OPENFOAM file not recognized")

        openfoam_dates = OpenFoam.times_dir_names(compute_dir)
        if not openfoam_dates:
            raise RuntimeError("No OPENFOAM results")
        logger.debug(
            len(openfoam_dates),
            "openfoam time directories from",
            openfoam_dates[0],
            "to",
            openfoam_dates[-1],
        )
        if not os.path.isfile(os.path.join(compute_dir, "openfoam_dates.txt")):
            with open(os.path.join(compute_dir, "openfoam_dates.txt"), "w") as fil:
                for date in openfoam_dates:
                    fil.write("%d\n" % (int(float(date))))

        nbDates = len(dates)
        logger.debug("nbDates =", nbDates)
        nbVal = OpenFoam.get_ncells(compute_dir)
        logger.debug("nbVal =", nbVal)
        results = numpy.zeros((nbDates, nbVal), dtype=numpy.float32)

        # cas d'une seule date => permanent
        if "_perm" in name_file and "0" in openfoam_dates:
            dmin = len(dates) - 1
            openfoam_dates = ["0"]
        elif float(openfoam_dates[0]) in dates:
            dmin = dates.index(float(openfoam_dates[0]))
        else:
            raise RuntimeError("read_file_formate: openfoam first date not recognized")

        d, n = dmin, 0
        logger.debug("nbDates, nbVal, d, n = ", nbDates, nbVal, d, n)
        meshid = OpenFoam.get_meshid(compute_dir)
        for date in openfoam_dates:
            results[d, meshid] = OpenFoam.read_openfoam_file(
                os.path.join(
                    compute_dir,
                    date,
                    self.variables_dictionary[name_file.split(".")[0]],
                ),
                True,
                nbVal,
            )
            d += 1

        dmax = d
        logger.debug("dmax =", dmax)

        # on remplit les valeurs pour les dates non renseignées :
        #  = 0 pour la concentration
        #  = première valeur renseignée pour potentiel et darcy
        if dmin > 0:
            if name_file[:4] == "conc":
                results[0:dmin, :] = 0.0
            else:
                results[0:dmin, :] = results[dmin, :]

        # for d in range(len(openfoam_dates), len(dates)+1):
        if dmax < nbDates:
            if name_file[:4] == "conc":
                results[dmax:nbDates, :] = 0.0
            else:
                results[dmax:nbDates, :] = results[dmax - 1, :]

        if convert:
            return convert_nodes(results)
        else:
            return results

    def create_maillage_insat(self, mesh_dir, znodes):
        """creates the OPENFOAM mesh files for unsaturated computations

        :param insat_dir: path of insaturated folder
        :type insat_dir: string
        :param znodes: znodes (list of z coordinate)
        :type znodes: list
        """

        nnoeuds = len(znodes)
        # points
        nPoints = 4 * nnoeuds
        logger.debug("Nombre de points = ", nPoints)

        OpenFoam.entete(mesh_dir, "points", "vectorField", "constant/polyMesh")
        with open(os.path.join(mesh_dir, "points"), "a") as fil:

            fil.write("%d\n" % (nPoints))
            fil.write("(\n")
            # points rangés de haut en bas !
            for row in znodes:
                fil.write("(0 0 %.5f)\n" % (row))
                fil.write("(1 0 %.5f)\n" % (row))
                fil.write("(1 1 %.5f)\n" % (row))
                fil.write("(0 1 %.5f)\n" % (row))
            fil.write(")\n\n")
            fil.write(
                "// ************************************************************************* //\n"
            )

        # faces
        nInternalFaces = nnoeuds - 2
        logger.debug("Nombre de faces internes = ", nInternalFaces)
        nExternalFaces = 4 * (nnoeuds - 1) + 2
        logger.debug("Nombre de faces externes = ", nExternalFaces)
        nFaces = nInternalFaces + nExternalFaces
        logger.debug("Nombre total de faces = ", nFaces)

        OpenFoam.entete(mesh_dir, "faces", "faceList", "constant/polyMesh")
        with open(os.path.join(mesh_dir, "faces"), "a") as fil:

            fil.write("%d\n" % (nFaces))
            fil.write("(\n")

            # faces internes + face inférieure
            for n in range(1, nnoeuds):
                fil.write("4(%d %d %d %d)\n" % (4 * n + 3, 4 * n + 2, 4 * n + 1, 4 * n))
            # face supérieure (il faut changer le sens)
            n = 0
            fil.write("4(%d %d %d %d)\n" % (4 * n, 4 * n + 1, 4 * n + 2, 4 * n + 3))

            # côtés
            for n in range(nnoeuds - 1):
                fil.write("4(%d %d %d %d)\n" % (4 * n + 4, 4 * n + 5, 4 * n + 1, 4 * n))
            for n in range(nnoeuds - 1):
                fil.write(
                    "4(%d %d %d %d)\n" % (4 * n + 5, 4 * n + 6, 4 * n + 2, 4 * n + 1)
                )
            for n in range(nnoeuds - 1):
                fil.write(
                    "4(%d %d %d %d)\n" % (4 * n + 6, 4 * n + 7, 4 * n + 3, 4 * n + 2)
                )
            for n in range(nnoeuds - 1):
                fil.write("4(%d %d %d %d)\n" % (4 * n + 7, 4 * n + 4, 4 * n, 4 * n + 3))

            fil.write(")\n\n")
            fil.write(
                "// ************************************************************************* //\n"
            )

        # cells
        nCells = nnoeuds - 1
        logger.debug("Nombre de cellules = ", nCells)

        logger.debug("owners ...")
        OpenFoam.entete(
            mesh_dir,
            "owner",
            "labelList",
            "constant/polyMesh",
            "npoints:%d  nCells:%d  nFaces:%d  nInternalFaces:%d"
            % (nPoints, nCells, nFaces, nInternalFaces),
        )
        with open(os.path.join(mesh_dir, "owner"), "a") as fil:

            fil.write("%d\n" % (nFaces))
            fil.write("(\n")
            for n in range(nnoeuds - 1):
                fil.write("%d\n" % (n))
            fil.write("0\n")
            for n in range(nnoeuds - 1):
                fil.write("%d\n" % (n))
            for n in range(nnoeuds - 1):
                fil.write("%d\n" % (n))
            for n in range(nnoeuds - 1):
                fil.write("%d\n" % (n))
            for n in range(nnoeuds - 1):
                fil.write("%d\n" % (n))

            fil.write(")\n\n")
            fil.write(
                "// ************************************************************************* //\n"
            )

        logger.debug("neighbours ...")
        OpenFoam.entete(
            mesh_dir,
            "neighbour",
            "labelList",
            "constant/polyMesh",
            "npoints:%d  nCells:%d  nFaces:%d  nInternalFaces:%d"
            % (nPoints, nCells, nFaces, nInternalFaces),
        )
        with open(os.path.join(mesh_dir, "neighbour"), "a") as fil:

            fil.write("%d\n" % (nInternalFaces))
            fil.write("(\n")
            for n in range(nnoeuds - 2):
                fil.write("%d\n" % (n + 1))

            fil.write(")\n\n")
            fil.write(
                "// ************************************************************************* //\n"
            )

        logger.debug("boundary ...")
        OpenFoam.entete(mesh_dir, "boundary", "polyBoundaryMesh", "constant/polyMesh")
        with open(os.path.join(mesh_dir, "boundary"), "a") as fil:

            fil.write("%d\n" % (3))
            fil.write("(\n")
            fil.write("    bottom\n")
            fil.write("    {\n")
            fil.write("        type            patch;\n")
            fil.write("        nFaces          %d;\n" % (1))
            fil.write("        startFace       %d;\n" % (nInternalFaces))
            fil.write("    }\n")
            fil.write("    top\n")
            fil.write("    {\n")
            fil.write("        type            patch;\n")
            fil.write("        nFaces          %d;\n" % (1))
            fil.write("        startFace       %d;\n" % (nInternalFaces + 1))
            fil.write("    }\n")
            fil.write("    sides\n")
            fil.write("    {\n")
            fil.write("        type            empty;\n")
            fil.write("        nFaces          %d;\n" % (nExternalFaces - 2))
            fil.write("        startFace       %d;\n" % (nInternalFaces + 2))
            fil.write("    }\n")
            fil.write(")\n\n")
            fil.write(
                "// ************************************************************************* //\n"
            )

    @staticmethod
    def times_dir_names(compute_dir):
        """returns a sorted list of the times directories

        :param compute_dir: path of compute folder
        :type compute_dir: string
        """
        names = [x for x in os.listdir(compute_dir) if x[0].isdigit()]
        dict_names = {}
        for name in names:
            dict_names[int(float(name))] = name
        return [dict_names[x] for x in sorted(dict_names.keys())]

    @staticmethod
    def create_tree(compute_dir, foam_name):
        """builds the directory tree for openfoam computation

        :param compute_dir: path of compute folder
        :type compute_dir: string
        :param foam_name: foam solver name from templates directory
        :type foam_name: string"""

        templates_dir = os.path.join(os.path.dirname(__file__), "templates", "openfoam")
        # list of time directories
        times_dir_names = OpenFoam.times_dir_names(compute_dir)
        logger.debug("times directories", times_dir_names)

        if times_dir_names:
            create_dirs = ["system"]
            delete_dirs = times_dir_names[:-1] + create_dirs
            for name in delete_dirs:
                name_dir = os.path.join(compute_dir, name)
                if os.path.isdir(name_dir):
                    logger.notice("deleting", name_dir)
                    shutil.rmtree(name_dir)
            # last time directory renamed to '0'
            shutil.move(
                os.path.join(compute_dir, times_dir_names[-1]),
                os.path.join(compute_dir, "0"),
            )
            if os.path.isdir(os.path.join(compute_dir, "0", "uniform")):
                shutil.rmtree(os.path.join(compute_dir, "0", "uniform"))
            if os.path.isfile(os.path.join(templates_dir, foam_name, "0", "C")):
                shutil.copyfile(
                    os.path.join(templates_dir, foam_name, "0", "C"),
                    os.path.join(compute_dir, "0", "C"),
                )
            shutil.copy2(
                os.path.join(
                    templates_dir, foam_name, "constant", "transportProperties"
                ),
                os.path.join(compute_dir, "constant", "transportProperties"),
            )
            if os.path.isfile(os.path.join(compute_dir, "0", "Utheta")):
                if "porousScalar" in foam_name:
                    shutil.copy2(
                        os.path.join(compute_dir, "0", "Utheta"),
                        os.path.join(compute_dir, "0", "U"),
                    )
                else:
                    shutil.copyfile(
                        os.path.join(templates_dir, foam_name, "0", "Utheta"),
                        os.path.join(compute_dir, "0", "Utheta"),
                    )
        else:
            create_dirs = ["0", "constant", "system"]

        for name in create_dirs:
            name_dir = os.path.join(compute_dir, name)
            if os.path.isdir(os.path.join(templates_dir, foam_name, name)):
                logger.notice("copying", foam_name, "directory", name_dir)
                shutil.copytree(os.path.join(templates_dir, foam_name, name), name_dir)
            else:
                logger.notice("creating", foam_name, "directory", name_dir)
                os.makedirs(name_dir)

            # adding setgid mode for working in shared directories
            os.chmod(name_dir, stat.S_IMODE(os.stat(name_dir).st_mode) | stat.S_ISGID)

        shutil.copy2(
            os.path.join(templates_dir, "clean"), os.path.join(compute_dir, "clean")
        )
        os.chmod(os.path.join(compute_dir, "clean"), 0o755)

    def create_insat_files(
        self, insat_dir, simulation_data, injection_data, param, settings, znodes
    ):
        """builds the input files for OPENFOAM unsaturated

        :param insat_dir: path of insaturated folder
        :type insat_dir: string
        :param simulation_data: dictionnary of simulation data
        :type simulation_data: dict
        :param injections_data: list, containing a dictionnary for each injection
        :type injections_data: list
        :param param: parameters dictionnary
        :type param: dict
        :param settings: Thyrsis settings
        :type settings: Settings
        :param znodes: znodes (list of z coordinate)
        :type znodes: list
        """

        coef = 1.0e-6 / 9.81  # m/s => m2
        OpenFoam.create_tree(insat_dir, "steadyGroundwaterFoam")

        mesh_dir = os.path.join(insat_dir, "constant", "polyMesh")
        os.makedirs(mesh_dir)
        self.create_maillage_insat(mesh_dir, znodes)

        dates_simulation = read_one_column_file(
            os.path.join(insat_dir, "dates_simulation.txt")
        )
        start_time = dates_simulation[0]
        end_time = dates_simulation[-1]
        # on prend une marge pour être sûr d'avoir la dernière date :
        end_time = 1.01 * end_time
        # write_interval must me greater than end_time to use outputEventFile
        write_interval = 1.01 * end_time
        # on prend pour delta_time_max l'écart de dates d'impression le plus petit, en attendant mieux !
        delta_time_max = 0.1 * min(
            numpy.array(dates_simulation[1:]) - numpy.array(dates_simulation[:-1])
        )

        # openfoam options
        options = self.get_options()

        with open(os.path.join(insat_dir, "parameters.txt"), "w") as fil:

            fil.write("//--time\n")
            fil.write("start_time %e;\n" % (start_time))
            fil.write("end_time %e;\n" % (end_time))
            fil.write("delta_time_init %e;\n" % (options["delta_time_init"]))
            fil.write("delta_time_max %e;\n" % (delta_time_max))
            fil.write("write_interval %e;\n\n" % (write_interval))

            fil.write("//--algorithmic parameters\n")
            fil.write("h_tolerance %e;\n" % (options["h_tolerance"]))
            fil.write("h_tolerance_relative %e;\n" % (options["h_tolerance_relative"]))
            fil.write("h_relaxationFactor %e;\n" % (options["h_relaxationFactor"]))
            fil.write(
                "h_relaxationFactor_transient %e;\n"
                % (options["h_relaxationFactor_transient"])
            )
            fil.write("picard_tolerance %e;\n" % (options["picard_tolerance"]))
            fil.write(
                "picard_tolerance_transient %e;\n"
                % (options["picard_tolerance_transient"])
            )
            fil.write("picard_maxiter %d;\n" % (options["picard_maxiter"]))
            fil.write(
                "picard_maxiter_transient %d;\n" % (options["picard_maxiter_transient"])
            )
            fil.write("newton_tolerance %e;\n" % (options["newton_tolerance"]))
            fil.write("newton_maxiter %d;\n\n" % (options["newton_maxiter"]))

            fil.write("//--time step managing\n")
            fil.write("dtfact_decrease %e;\n" % (options["dtfact_decrease"]))
            fil.write("truncationError %e;\n\n" % (options["truncationError"]))

            fil.write("//--properties\n")
            fil.write("fluid_density %e;\n" % (options["fluid_density"]))
            fil.write("dynamic_viscosity %e;\n" % (options["dynamic_viscosity"]))
            fil.write("molecular_diffusion %e;\n" % (options["molecular_diffusion"]))
            fil.write("tortuosity %e;\n" % (options["tortuosity"]))
            fil.write("kinematic_porosity %e ;\n" % (param["WC"]))
            fil.write("total_porosity %e ;\n" % (param["WT"]))
            fil.write(
                "intrinsic_permeability %e ;\n"
                % (injection_data["PERMEABILITE"] * coef)
            )
            fil.write("longitudinal_dispersivity %e;\n" % (param["DLZNS"]))
            fil.write("transversal_dispersivity %e;\n" % (0.2 * param["DLZNS"]))
            fil.write("retention_coefficient %e;\n" % (param["DK"]))
            fil.write("density %e;\n" % (param["VM"]))
            fil.write("radioactive_decay %e;\n" % (simulation_data["DECRAD"]))

            fil.write("vangenuchten_thetamin %f;\n" % (param["VGR"] * param["WC"]))
            fil.write("vangenuchten_thetamax %f;\n" % (param["VGS"] * param["WC"]))
            fil.write("vangenuchten_m %f;\n" % (1.0 - 1.0 / param["VGN"]))
            fil.write("vangenuchten_alpha %f;\n" % (param["VGA"]))

            fil.write("param_Uinlet (0 0 %e);\n" % (-injection_data["INFILTRATION"]))
            fil.write("param_Uinit (0 0 0);\n")
            fil.write("param_houtlet %f;\n" % (0))
            fil.write("param_hinit %f;\n" % (0))

            fil.write("specific_storage %e;\n" % (options["specific_storage"]))

        solver = "groundwaterFoam"
        log_file = os.path.join(insat_dir, solver + "_steady.log")
        with open(log_file, "w") as logfile:
            logger.notice("running: ", solver, "-steady", "\n in directory ", insat_dir)
            cmd = [solver, "-steady"]
            self.run_pmf_solver(cmd, logfile, insat_dir)

        self.nok(log_file)

        OpenFoam.create_tree(insat_dir, self.get_solver(simulation_data, "unsaturated"))

        tmpdir = os.path.dirname(os.path.dirname(insat_dir))
        # fichier debit_entrant.insat
        if simulation_data["TYPE_INJECTION"] != "aucune":
            if os.path.isfile(
                os.path.join(tmpdir, "debit_entrant.insat_%05d" % (int(insat_dir[-5:])))
            ):
                shutil.copyfile(
                    os.path.join(
                        tmpdir, "debit_entrant.insat_%05d" % (int(insat_dir[-5:]))
                    ),
                    os.path.join(insat_dir, "debit_entrant.insat"),
                )
            elif injection_data["FLUX"] == None:
                raise RuntimeError(
                    "Le flux est nul ou inconnu, debit_entrant.insat ne peut être créé"
                )
            else:
                with open(os.path.join(insat_dir, "debit_entrant.insat"), "w") as fil:
                    if injection_data["PROFONDEUR"] > 0:
                        fil.write(
                            "date %e\n"
                            "0.5 0.5 %.3f %e\n"
                            "date %e\n"
                            "0.5 0.5 %.3f %e\n"
                            % (
                                injection_data["T0"],
                                znodes[0] - injection_data["PROFONDEUR"],
                                -injection_data["FLUX"],
                                injection_data["TINJ"],
                                znodes[0] - injection_data["PROFONDEUR"],
                                -injection_data["FLUX"],
                            )
                        )
                    else:
                        fil.write(
                            "date %e\n"
                            "top %e\n"
                            "date %e\n"
                            "top %e\n"
                            % (
                                injection_data["T0"],
                                -injection_data["FLUX"],
                                injection_data["TINJ"],
                                -injection_data["FLUX"],
                            )
                        )

            if simulation_data["FUITE"]:
                with open(os.path.join(insat_dir, "eau_entrant.insat"), "w") as fil:
                    if injection_data["PROFONDEUR"] > 0:
                        fil.write(
                            "date %e\n"
                            "0.5 0.5 %.3f %e\n"
                            "date %e\n"
                            "0.5 0.5 %.3f %e\n"
                            % (
                                injection_data["T0"],
                                znodes[0] - injection_data["PROFONDEUR"],
                                -injection_data["DFUI"],
                                injection_data["TINJ"],
                                znodes[0] - injection_data["PROFONDEUR"],
                                -injection_data["DFUI"],
                            )
                        )
                    else:
                        fil.write(
                            "date %e\n"
                            "top %e\n"
                            "date %e\n"
                            "top %e\n"
                            % (
                                injection_data["T0"],
                                -injection_data["DFUI"],
                                injection_data["TINJ"],
                                -injection_data["DFUI"],
                            )
                        )

            if injection_data["PROFONDEUR"] > 0:
                with open(os.path.join(insat_dir, "eventFile.txt"), "w") as fil:
                    fil.write('sourceEventFileTracer "debit_entrant.insat";\n')
                    if simulation_data["FUITE"]:
                        fil.write('sourceEventFileWater "eau_entrant.insat";\n')
                # else eventFile "debit_entrant.insat" in 0/C (patch top)

            with open(os.path.join(insat_dir, "0", "C"), "a") as fil:
                if injection_data["PROFONDEUR"] > 0:
                    fil.write("        type zeroGradient;\n")
                else:
                    fil.write("        type          eventFlux;\n")
                    fil.write("        value         uniform 0;\n")
                    fil.write("        constantValue 0;\n")
                    fil.write('        eventFile     "debit_entrant.insat";\n')
                fil.write("    }\n")
                fil.write("}\n")

            with open(os.path.join(insat_dir, "0", "Utheta"), "a") as fil:
                if not simulation_data["FUITE"] or injection_data["PROFONDEUR"] > 0:
                    fil.write("        type          eventInfiltration;\n")
                    fil.write("        value         uniform (0 0 0);\n")
                    fil.write("        constantValue 0;\n")
                else:
                    fil.write("        type          eventInfiltration;\n")
                    fil.write('        eventFile     "eau_entrant.insat";\n')
                    fil.write("        value         uniform (0 0 0);\n")
                    fil.write("        constantValue 0;\n")
                fil.write("    }\n")
                fil.write("}\n")

    def convert_infiltration_transitoire_insature(self, compute_dir):
        """Copy transient infiltration file to compute_dir

        :param compute_dir: path of compute folder
        :type compute_dir: string
        """

        # metis format esui file must be adapted for openfoam
        # the esui-like infiltration_transitoire.insat file is modified from tmpdir to compute_dir
        file_esui = os.path.join(
            os.path.dirname(os.path.dirname(compute_dir)),
            "infiltration_transitoire.insat",
        )
        if os.path.exists(file_esui):
            with open(file_esui) as filin, open(
                os.path.join(compute_dir, "infiltration_transitoire.insat"), "w"
            ) as filout:
                for line in filin:
                    if "date" in line:
                        filout.write(line)
                    else:
                        filout.write("top %e\n" % (float(line.split()[0])))

    def convert_infiltration_transitoire_sature(self, compute_dir):
        """Copy transient infiltration file to compute_dir

        :param compute_dir: path of compute folder
        :type compute_dir: string
        """

        # metis format esui file must be adapted for openfoam
        # the esui-like infiltration_transitoire.insat file is modified from tmpdir to compute_dir
        # in order to have plateau in place of interpolation
        file_esui = os.path.join(
            os.path.dirname(os.path.dirname(compute_dir)),
            "infiltration_transitoire.sat",
        )
        file_out = os.path.join(compute_dir, "infiltration_transitoire.sat")

        # as differences between plateau and interpolation are very small, we keep the file not modified
        shutil.copyfile(file_esui, file_out)
        return

        # TO INVESTIGATE: the following code is never executed.
        # if os.path.exists(file_esui):
        #     # first calculating number of dates
        #     with open(file_esui) as filin:
        #         ndates = 0
        #         for line in filin:
        #             if "date" in line:
        #                 ndates += 1
        #     with open(file_esui) as filin, open(file_out, "w") as filout:
        #         init = True
        #         idate = 0
        #         for line in filin:
        #             if "date" in line:
        #                 idate += 1
        #                 filout.write(line)
        #                 if not init:
        #                     filout.write("%e 0\n" % (value))
        #                     if idate == ndates:
        #                         break
        #                     filout.write(line)
        #                 init = False
        #             else:
        #                 filout.write(line)
        #                 value = float(line.split()[0])

    def get_solver(self, simulation_data, mode="saturated"):
        """Returns the OPENFOAM solver name, depending of the computation type

        :param simulation_data: dictionnary of simulation data
        :type simulation_data: dict
        :param mode: foam mode
        :type mode: string

        :return: solver_saturated
        :rtype: string
        """

        logger.debug("type_injection", simulation_data["TYPE_INJECTION"])
        logger.debug("type_infiltration", simulation_data["TYPE_INFILTRATION"])
        logger.debug("fuite", simulation_data["FUITE"])

        dispersion = simulation_data["TYPE_INJECTION"] != "aucune"
        aectran = (
            simulation_data["TYPE_INFILTRATION"] != "permanente"
            or simulation_data["FUITE"]
        )
        aecditr = dispersion and aectran

        if aecditr:
            solver_unsaturated = "groundwaterTransportFoam"
            solver_saturated = "groundwaterTransport2DFoam"
        elif dispersion:
            solver_unsaturated = "porousScalarTransportFoam"
            solver_saturated = "porousScalarTransport2DFoam"
        else:
            solver_unsaturated = "groundwaterFoam"
            solver_saturated = "groundwater2DFoam"

        return solver_saturated if mode == "saturated" else solver_unsaturated

    def modif_data(self, rep, tini=0.0):
        """modifies the OPENFOAM data file and corrects the initial time
            and modifies the dates_simulation.txt file in order to keep only dates greater than tini
            and concerts dates to integers.

        :param rep: file path
        :type rep: string
        :param tini: initial time
        :type tini: float
        """

        dates_simulation = read_one_column_file(
            os.path.join(rep, "dates_simulation.txt")
        )
        with open(os.path.join(rep, "dates_simulation.txt"), "w") as fild:
            for date in dates_simulation:
                if date >= tini:
                    fild.write("%d\n" % (int(float(date))))

        if tini > 0:
            shutil.move(os.path.join(rep, "0"), os.path.join(rep, str(int(tini))))
            shutil.move(
                os.path.join(rep, "parameters.txt"),
                os.path.join(rep, "parameters_tmp.txt"),
            )
            with open(os.path.join(rep, "parameters_tmp.txt"), "r") as filr:
                with open(os.path.join(rep, "parameters.txt"), "w") as filw:
                    for line in filr:
                        if "start_time" in line:
                            filw.write("start_time %d;\n" % (int(tini)))
                        else:
                            filw.write(line)

            os.remove(os.path.join(rep, "parameters_tmp.txt"))

    def compute(self, compute_dir, total_time, MPI=None):
        """computes saturated or unsaturated

        :param compute_dir: path of compute folder
        :type compute_dir: string
        :param total_time: simulation time duration in second
        :type total_time: float
        """

        timer = Timer()

        with sqlite.connect(self.database) as conn:
            simulation_data = get_simulation_data(conn)

        if os.path.basename(compute_dir)[0] == "s":
            solver = self.get_solver(simulation_data, "saturated")
            type_computation = "OPENFOAM saturated"
        else:
            solver = self.get_solver(simulation_data, "unsaturated")
            type_computation = "OPENFOAM unsaturated"

        log_file = os.path.join(compute_dir, solver + ".log")
        finished_job = False
        with open(log_file, "w") as logfile:
            logger.notice("running: ", solver, "\n in directory ", compute_dir)
            solver = str(self.get_pmf_root_dir() / "bin" / solver)
            my_env = self.get_solver_env()
            proc = Popen(
                [solver],
                stdout=PIPE,
                stderr=STDOUT,
                cwd=compute_dir,
                universal_newlines=False,
                env=my_env,
            )
            progress = logger.progress(type_computation)
            while True:
                line = proc.stdout.readline()
                line = line.decode("utf-8")
                if line is None or line == "":
                    break
                if "End" in line:
                    finished_job = True
                logfile.write(line)
                if len(line) > 7 and line[:7] == "Time = ":
                    current = float(line.split(" = ")[1])
                    progress.set_ratio(current / total_time)
            del progress
            proc.stdout.close()

        if finished_job != True:
            flag = self.nok(log_file)
            if flag:
                if MPI:
                    MPI.COMM_WORLD.Abort(1)
                raise RuntimeError(
                    "OPENFOAM calculation aborted : refer back to file " + log_file
                )

        print(timer.reset(compute_dir))

        self.compute_probes(compute_dir)

    def compute_probes(self, compute_dir):
        """computes results at probes

        :param compute_dir: path of compute folder
        :type compute_dir: string
        """

        timer = Timer()

        # probes
        if os.path.isfile(os.path.join(compute_dir, "system", "probes")):
            solver = "postProcess"
            log_file = os.path.join(compute_dir, solver + ".log")
            cmd = [solver, "-func", "probes"]
            with open(log_file, "w") as logfile:
                logger.notice("running: ", solver, "\n in directory ", compute_dir)
                self.run_openfoam_solver(cmd, logfile, compute_dir)

        print(timer.reset("computing probes"))

    def compute_hynverse(self, compute_dir):
        """computes saturated for inversion

        :param compute_dir: path of compute folder
        :type compute_dir: string
        """

        timer = Timer()

        solver = "groundwater2DFoam"

        log_file = os.path.join(compute_dir, solver + ".log")
        with open(log_file, "w") as logfile:
            logger.notice(
                "running: ", solver, "-steady", "\n in directory ", compute_dir
            )
            cmd = [solver, "-steady"]
            self.run_pmf_solver(cmd, logfile, compute_dir)

        self.nok(log_file)

        times_dir_names = OpenFoam.times_dir_names(compute_dir)
        logger.debug("times directories", times_dir_names)

        if len(times_dir_names) > 1 and int(times_dir_names[-1]) < END_TIME_STEADY:
            for name in times_dir_names[:-1]:
                name_dir = os.path.join(compute_dir, name)
                if os.path.isdir(name_dir):
                    logger.notice("deleting", name_dir)
                    shutil.rmtree(name_dir)
            # last time directory renamed to '0'
            shutil.move(
                os.path.join(compute_dir, times_dir_names[-1]),
                os.path.join(compute_dir, "0"),
            )

        else:
            raise RuntimeError("groundwater2DFoam -steady not converged")

        logger.debug(timer.reset(compute_dir))

    def settings_to_dict(self, settings):
        """save settings in dictionnary, yet not used for OPENFOAM

        :param settings: Thyrsis settings
        :type settings: Settings
        """
        save_dict = {}
        for option in settings.options("Variables"):
            if option[-5:] == "Check":
                save_dict[option[:-5].upper()] = (
                    "" if settings.value("Variables", option) else "%%"
                )
        save_dict["SATURATION_PERMZNS"] = save_dict["SATURATIONZNS"]
        save_dict["DARCY_PERMZNS"] = save_dict["DARCYZNS"]
        return save_dict

    def renumberMesh(self, sat_dir):
        """renumber the mesh to fit OPENFOAM constraints

        :param sat_dir: path of insaturated folder
        :type sat_dir: string
        """
        logger.debug("renumberMesh ...", sat_dir)
        cmd = ["renumberMesh", "-overwrite", "-constant"]
        with open(os.path.join(sat_dir, "renumberMesh.log"), "w") as logfile:
            self.run_openfoam_solver(cmd, logfile, sat_dir)

    def run_openfoam_solver(self, cmd, logfile, sat_dir):
        cmd[0] = str(self._get_openfoam_bin_dir() / cmd[0])
        self.run_solver(cmd, logfile, sat_dir)

    def _get_openfoam_bin_dir(self) -> Path:
        return (
            self.get_openfoam_root_dir()
            / "platforms"
            / self.get_openfoam_platform()
            / "bin"
        )

    def run_pmf_solver(self, cmd, logfile, sat_dir):
        cmd[0] = str(self.get_pmf_root_dir() / "bin" / cmd[0])
        self.run_solver(cmd, logfile, sat_dir)

    def get_cartesian_2D_mesh_path(self) -> Optional[Path]:
        # Check in OpenFoam
        cartesian_2d_mesh_path = self._get_openfoam_bin_dir() / "cartesian2DMesh"
        if cartesian_2d_mesh_path.exists():
            return cartesian_2d_mesh_path

        # Check in PMF
        cartesian_2d_mesh_path = self.get_pmf_root_dir() / "bin" / "cartesian2DMesh"
        if cartesian_2d_mesh_path.exists():
            return cartesian_2d_mesh_path

        # No value available
        return None

    def run_cartesian_2D_mesh(self, logfile, sat_dir):
        self.run_solver([str(self.get_cartesian_2D_mesh_path())], logfile, sat_dir)

    def get_pmf_root_dir(self):
        return Path(self._settings.value("General", "pmf"))

    def get_openfoam_root_dir(self):
        return Path(self._settings.value("General", "openfoam"))

    def get_openfoam_platform(self):
        if sys.platform.startswith("linux"):
            return "linux64GccDPInt32Opt"
        else:
            return "win64MingwDPInt32Opt"

    def run_solver(self, cmd, logfile, sat_dir):
        my_env = self.get_solver_env()
        Popen(
            cmd,
            stdout=logfile,
            stderr=STDOUT,
            cwd=sat_dir,
            universal_newlines=False,
            env=my_env,
        ).wait()

    def get_solver_env(self):
        my_env = os.environ.copy()
        my_env["WM_PROJECT_DIR"] = str(self.get_openfoam_root_dir())
        my_env["PATH"] += os.pathsep + str(self.get_openfoam_root_dir() / "bin")
        my_env["PATH"] += os.pathsep + str(
            self.get_openfoam_root_dir()
            / "platforms"
            / self.get_openfoam_platform()
            / "bin"
        )
        my_env["PATH"] += os.pathsep + str(self.get_pmf_root_dir() / "bin")

        lib_env_name = "LD_LIBRARY_PATH" if sys.platform.startswith("linux") else "PATH"
        if lib_env_name not in my_env:
            my_env[lib_env_name] = ""

        my_env[lib_env_name] += os.pathsep + str(
            self.get_openfoam_root_dir()
            / "platforms"
            / self.get_openfoam_platform()
            / "lib"
        )
        my_env[lib_env_name] += os.pathsep + str(
            self.get_openfoam_root_dir()
            / "platforms"
            / self.get_openfoam_platform()
            / "lib"
            / "sys-openmpi"
        )
        my_env[lib_env_name] += os.pathsep + str(
            self.get_openfoam_root_dir()
            / "platforms"
            / self.get_openfoam_platform()
            / "lib"
            / "dummy"
        )
        my_env[lib_env_name] += os.pathsep + str(
            self.get_openfoam_root_dir()
            / "ThirdParty"
            / "platforms"
            / self.get_openfoam_platform()
            / "lib"
        )
        my_env[lib_env_name] += os.pathsep + str(self.get_pmf_root_dir() / "lib")

        return my_env

    def create_sat_files(self, sat_dir, simulation_data, param, settings):
        """builds the input files for OPENFOAM saturated

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param simulation_data: dictionnary of simulation data
        :type simulation_data: dict
        :param param: parameters dictionnary
        :type param: dict
        :param settings: Thyrsis settings
        :type settings: Settings
        """

        OpenFoam.create_tree(sat_dir, self.get_solver(simulation_data, "saturated"))
        probes_field = (
            "C" if simulation_data["TYPE_INJECTION"] != "aucune" else "potential"
        )

        with sqlite.connect(self.database) as conn:
            cur = conn.cursor()

            if simulation_data["TYPE_INFILTRATION"] == "permanente":
                self.create_infiltration_sat(sat_dir, cur)
            self.create_permeabilities_sat(sat_dir, cur=cur)
            self.create_velocities_sat(sat_dir, cur=cur)
            self.create_mur_sat(sat_dir, cur)
            self.create_mnt_sat(sat_dir, cur)
            self.create_probes_sat(sat_dir, cur, field=probes_field)

            self.create_maillage_sat(sat_dir, cur)

        dates_simulation = read_one_column_file(
            os.path.join(sat_dir, "dates_simulation.txt")
        )
        start_time = dates_simulation[0]
        end_time = dates_simulation[-1]
        # on prend une marge pour être sûr d'avoir la dernière date :
        end_time = 1.01 * end_time
        # write_interval must me greater than end_time to use outputEventFile
        write_interval = 1.01 * end_time
        # on prend pour delta_time_max l'écart de dates d'impression le plus petit, en attendant mieux !
        delta_time_max = 0.2 * min(
            numpy.array(dates_simulation[1:]) - numpy.array(dates_simulation[:-1])
        )

        # openfoam options
        options = self.get_options()

        with open(os.path.join(sat_dir, "parameters.txt"), "w") as fil:

            fil.write("//--time\n")
            fil.write("start_time %e;\n" % (start_time))
            fil.write("end_time %e;\n" % (end_time))
            fil.write("delta_time_init %e;\n" % (options["delta_time_init"]))
            fil.write("delta_time_max %e;\n" % (delta_time_max))
            fil.write("write_interval %e;\n" % (write_interval))

            fil.write("//--algorithmic parameters\n")
            fil.write("phiReconstruction %s;\n" % ("true"))
            fil.write("potential_tolerance %e;\n" % (options["potential_tolerance"]))
            fil.write(
                "potential_tolerance_relative %e;\n"
                % (options["potential_tolerance_relative"])
            )
            fil.write(
                "potential_residualControl %e;\n"
                % (options["potential_residualControl"])
            )
            fil.write(
                "potential_relaxationFactor %e;\n"
                % (options["potential_relaxationFactor"])
            )
            fil.write("hwatermin %f;\n" % (options["hwatermin"]))

            fil.write("//--time step managing\n")
            fil.write("dtfact_decrease %e;\n" % (options["dtfact_decrease"]))
            fil.write("truncationError %e;\n" % (options["truncationError"]))

            fil.write("//--properties\n")
            fil.write("fluid_density %e;\n" % (options["fluid_density"]))
            fil.write("dynamic_viscosity %e;\n" % (options["dynamic_viscosity"]))
            fil.write("molecular_diffusion %e;\n" % (options["molecular_diffusion"]))
            fil.write("tortuosity %e;\n" % (options["tortuosity"]))
            fil.write("kinematic_porosity %e ;\n" % (param["WC"]))
            fil.write("total_porosity %e ;\n" % (param["WT"]))
            fil.write("longitudinal_dispersivity %e;\n" % (param["DLZS"]))
            fil.write("transversal_dispersivity %e;\n" % (0.2 * param["DLZS"]))
            fil.write("retention_coefficient %e;\n" % (param["DK"]))
            fil.write("density %e;\n" % (param["VM"]))
            fil.write("radioactive_decay %e;\n" % (simulation_data["DECRAD"]))

            fil.write("specific_storage %e;\n" % (options["specific_storage"]))

        with open(os.path.join(sat_dir, "waterEventFile.txt"), "w") as fil:

            if simulation_data["INSATURE"] == "oui":
                fil.write('sourceEventFileWater "debit_eau_entrant.sat";')
            else:
                fil.write('infiltrationEventFile "infiltration_transitoire.sat";')

        self.renumberMesh(sat_dir)

        # some files must be transfered to 0 directory
        if os.path.exists(os.path.join(sat_dir, "constant", "infiltration")):
            shutil.move(
                os.path.join(sat_dir, "constant", "infiltration"),
                os.path.join(sat_dir, "0"),
            )
        if os.path.exists(os.path.join(sat_dir, "constant", "potential")):
            shutil.move(
                os.path.join(sat_dir, "constant", "potential"),
                os.path.join(sat_dir, "0"),
            )
        if os.path.exists(os.path.join(sat_dir, "constant", "hwater")):
            shutil.move(
                os.path.join(sat_dir, "constant", "hwater"), os.path.join(sat_dir, "0")
            )
        if os.path.exists(os.path.join(sat_dir, "constant", "U")):
            shutil.move(
                os.path.join(sat_dir, "constant", "U"), os.path.join(sat_dir, "0")
            )
        if os.path.exists(os.path.join(sat_dir, "constant", "seepageTerm")):
            shutil.move(
                os.path.join(sat_dir, "constant", "seepageTerm"),
                os.path.join(sat_dir, "0"),
            )

    def read_insat_flux(self, insat_dir, name="debit"):
        """reads the outgoing flux from a ZNS column

        :param insat_dir: path of insaturated folder
        :type insat_dir: string
        :param name: flux name
        :type name: string

        :return ldates: dates list
        :rtype ldates: list
        :return lflux: outgoing flux value list
        :rtype lflux": list
        """
        logger.debug("read_insat_flux")

        ldates = []
        lflux = []
        filename = "CmassBalance.csv" if name == "debit" else "waterMassBalance.csv"
        num_colonne = 1 if "water" in filename else 2
        with open(os.path.join(insat_dir, filename)) as fil:
            for line in fil:
                if line[0] != "#" and line.split() != []:
                    ldates.append(float(line.split()[0]))
                    lflux.append(float(line.split()[num_colonne]))

        return ldates, lflux

    def write_sat_flux(
        self, sat_dir, name, dates, nodes, meshes, ldates, lnodes, lmeshes, lflux
    ):
        """creates the file giving the incoming flux into the groundwater

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param name: flux name
        :type name: string
        :param dates: list of dates
        :type dates: list
        :param nodes: total injection nodes list
        :type nodes: list
        :param meshes: total injection meshes list
        :type meshes: list
        :param ldates: dates list
        :type ldates: list
        :param lnodes: list of injection nodes lists per injection
        :type lnodes: list
        :param lmeshes: list of (injection meshes, area ratio) lists per injection
        :type lmeshes: list
        :param lflux: list of time-dependant flux lists per injection
        :type lflux": list
        """

        logger.debug("write_sat_flux")

        with sqlite.connect(self.database) as conn:
            cur = conn.cursor()
            (type_infiltration,) = cur.execute(
                "SELECT type_infiltration FROM simulations"
            ).fetchone()
            infiltration_transitoire = type_infiltration != "permanente"
            tup = cur.execute(
                """SELECT OGC_FID, surface, infiltration
                FROM mailles ORDER BY OGC_FID"""
            ).fetchall()
            xy = cur.execute(
                """SELECT X(CENTROID(GEOMETRY)), Y(CENTROID(GEOMETRY))
                FROM mailles ORDER BY OGC_FID"""
            ).fetchall()

            if name != "debit_eau":
                # décroissance radioactive pour le bilan de masse
                (nb_elem,) = cur.execute(
                    """SELECT COUNT(1) FROM elements_chimiques
                    WHERE id = (SELECT eid FROM simulations LIMIT 1)"""
                ).fetchone()
                decrad = (
                    cur.execute(
                        """
                    SELECT decroissance_radioactive FROM elements_chimiques
                    WHERE id = (SELECT eid FROM simulations LIMIT 1)"""
                    ).fetchone()[0]
                    if nb_elem
                    else 0.0
                )

            meshes_all = [x[0] for x in tup]
            surf = [x[1] for x in tup]

            # si l'infiltration est transitoire, on crée un seul fichier debit_eau_entrant.sat
            # avec les flux à tous les noeuds (infiltration + fuite éventuelle = sortie ZNS)
            # sinon le fichier debit_eau_entrant.sat ne contient que le flux correspondant à la fuite
            # et l'infiltration (permanente) est lue dans le fichier infiltration.sat
            flux = [0.0] * len(tup)
            flux_init = [(0.0 if infiltration_transitoire else -x[2]) for x in tup]

        tini = (
            0.0  # date à partir de laquelle le flux de matière devient non nul (debit)
        )
        # ou première date de flux d'eau (debit_eau)
        meshes_sorted = set(meshes)
        dates_sorted = sorted(dates)
        meshid = OpenFoam.get_meshid(sat_dir)

        if name == "debit_eau":

            logger.notice("Computing groundwater infiltration")
            with open(os.path.join(sat_dir, name + "_entrant.sat"), "w") as fil, open(
                os.path.join(sat_dir, name + "_entrant_plot.sat"), "w"
            ) as filp:

                tini = dates_sorted[0]
                for date in dates_sorted:
                    fil.write("date %e\n" % (date))
                    for mesh in meshes_sorted:
                        flux[mesh - 1] = 0.0
                    for inj in range(len(ldates)):
                        if date in ldates[inj]:
                            idt = ldates[inj].index(date)
                            for mesh, ratio, ratio_surface in lmeshes[inj]:
                                flux[mesh - 1] += (
                                    max(0.0, flux_init[mesh - 1] + lflux[inj][idt])
                                    * ratio_surface
                                )

                    ftot_mesh_sorted = 0.0
                    for mesh in meshes_sorted:
                        flux[mesh - 1] = -flux[mesh - 1] * surf[mesh - 1]
                        fil.write(
                            "%.3f %.3f 100 %e\n"
                            % (xy[mesh - 1][0], xy[mesh - 1][1], flux[mesh - 1])
                        )
                        ftot_mesh_sorted += flux[mesh - 1]

                    # fil.write(" ".join((len(meshes_all))*["%e"])%( \
                    #        tuple(flux[meshid[m]] for m in range(len(meshes_all))))+"\n")

                    filp.write("%e %e\n" % (date, ftot_mesh_sorted))

        else:

            logger.notice("Computing groundwater mass input")
            fluxmin = 0.0

            with open(os.path.join(sat_dir, name + "_entrant.sat"), "w") as fil, open(
                os.path.join(sat_dir, name + "_entrant_plot.sat"), "w"
            ) as filp:
                tini = (
                    -1.0
                )  # signifie que le flux est constamment nul => pas de calcul saturé

                mtot = 0
                ftot = []
                for i, date in enumerate(dates_sorted):
                    fil.write("date  %e\n" % (date))
                    for mesh in meshes_sorted:
                        flux[mesh - 1] = 0.0
                    for inj in range(len(ldates)):
                        if date in ldates[inj]:
                            idt = ldates[inj].index(date)
                            for mesh, ratio, ratio_surface in lmeshes[inj]:
                                flux[mesh - 1] += lflux[inj][idt] * ratio

                    for mesh in meshes_sorted:
                        if flux[mesh - 1] > fluxmin and tini < 0.0:
                            tini = dates_sorted[i - 1] if i > 0 else dates_sorted[0]

                    ftot.append(0)
                    for mesh in meshes_sorted:
                        ftot[i] += -flux[mesh - 1]
                        fil.write(
                            "%.3f %.3f 100 %e\n"
                            % (xy[mesh - 1][0], xy[mesh - 1][1], -flux[mesh - 1])
                        )
                    filp.write(
                        " ".join((2 + len(meshes_sorted)) * ["%e"])
                        % (
                            (date, ftot[i])
                            + tuple(-flux[mesh - 1] for mesh in meshes_sorted)
                        )
                        + "\n"
                    )
                    if i == 0:
                        mtot = -0.5 * ftot[i] * date
                    else:
                        mtot = mtot * numpy.exp(
                            -decrad * (dates_sorted[i] - dates_sorted[i - 1])
                        ) - 0.5 * (ftot[i] + ftot[i - 1]) * (
                            dates_sorted[i] - dates_sorted[i - 1]
                        )

                logger.notice("total mass from unsaturated zone is ", mtot)

        logger.debug("tini = ", tini)
        return tini

    def create_debit_sat(self, sat_dir, flux):
        """builds the input file for OPENFOAM in case of no unsaturated calculation

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param flux: flux value list
        :type flux": list
        """
        with open(os.path.join(sat_dir, "debit_entrant.sat"), "w") as fil:
            for t, f in flux:
                fil.write("date %15.7E\n" % (t))
                for n, x, y, v in f:
                    fil.write("%3f %3f %f %15.7E\n" % (x, y, EPAISSEUR_2D, v))

    def create_hydrostationary_files(self, sat_dir, with_permeabilities=True):
        """builds the input files for OPENFOAM saturated inversion

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        """

        OpenFoam.create_tree(sat_dir, "steadyGroundwater2DFoam")

        with sqlite.connect(self.database) as conn:
            cur = conn.cursor()

            # potentiel set to NULL
            cur.execute("UPDATE noeuds SET potentiel = NULL")
            cur.execute("UPDATE mailles SET potentiel = NULL")

            conn.commit()

            self.create_infiltration_sat(sat_dir, cur)
            self.create_mur_sat(sat_dir, cur)
            self.create_mnt_sat(sat_dir, cur)
            self.create_velocities_sat(sat_dir, init=True)
            if with_permeabilities:
                self.create_permeabilities_sat(
                    sat_dir, cur=cur
                )  # pas utile pour l'inversion, utile pour create_hydrostationary
            self.create_probes_sat(sat_dir, cur, field="potential")

            self.create_maillage_sat(sat_dir, cur, True)

        end_time = END_TIME_STEADY
        write_interval = 100

        # openfoam options
        options = self.get_hynverse_options()

        with open(os.path.join(sat_dir, "parameters.txt"), "w") as fil:

            fil.write("end_time %e;\n" % (end_time))
            fil.write("write_interval %e;\n" % (write_interval))

            fil.write("potential_tolerance %e;\n" % (options["potential_tolerance"]))
            fil.write(
                "potential_tolerance_relative %e;\n"
                % (options["potential_tolerance_relative"])
            )
            fil.write(
                "potential_residualControl %e;\n"
                % (options["potential_residualControl"])
            )
            fil.write(
                "potential_relaxationFactor %e;\n"
                % (options["potential_relaxationFactor"])
            )
            fil.write("hwatermin %f;\n" % (options["hwatermin"]))

            fil.write("fluid_density %e;\n" % (options["fluid_density"]))
            fil.write("dynamic_viscosity %e;\n" % (options["dynamic_viscosity"]))

        self.renumberMesh(sat_dir)

        # nm = len(self.mailles)
        # self.create_permeabilities_sat(sat_dir, liste_pm=[(parametres["permini"], parametres["permini"])]*nm)
        # liste_vm = [(0, 0)]*nm
        # self.create_velocities_sat(sat_dir, liste_vm)

        # some files must be transfered to 0 directory
        if os.path.exists(os.path.join(sat_dir, "constant", "potential")):
            shutil.move(
                os.path.join(sat_dir, "constant", "potential"),
                os.path.join(sat_dir, "0"),
            )
        if os.path.exists(os.path.join(sat_dir, "constant", "hwater")):
            shutil.move(
                os.path.join(sat_dir, "constant", "hwater"), os.path.join(sat_dir, "0")
            )
        if os.path.exists(os.path.join(sat_dir, "constant", "U")):
            shutil.move(
                os.path.join(sat_dir, "constant", "U"), os.path.join(sat_dir, "0")
            )
        if os.path.exists(os.path.join(sat_dir, "constant", "seepageTerm")):
            shutil.move(
                os.path.join(sat_dir, "constant", "seepageTerm"),
                os.path.join(sat_dir, "0"),
            )

    def create_maillage_sat(self, sat_dir, cur, hynverse=False):
        """builds the mesh saturated OPENFOAM files

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param cur: cursor
        :type cur: sqlite3.Cursor
        :param hynverse: hynverse flag
        :type hynverse: bool

        """

        xy_noeuds = cur.execute(
            "SELECT X(GEOMETRY), Y(GEOMETRY) FROM noeuds ORDER BY OGC_FID"
        ).fetchall()
        nnoeuds = len(xy_noeuds)
        noeuds_contour = [
            n[0]
            for n in cur.execute(
                "SELECT nid FROM noeuds_contour ORDER BY id"
            ).fetchall()
        ]
        nnoeuds_contour = len(noeuds_contour)

        noeuds_potentiel = []
        potentiel_impose = {}
        for v in cur.execute(
            "SELECT nid, valeur FROM potentiel_impose ORDER BY id"
        ).fetchall():
            noeuds_potentiel.append(v[0])
            potentiel_impose[v[0]] = v[1]
        nnoeuds_potentiel = len(noeuds_potentiel)
        # on suppose qu'on n'a que des triangles
        xy_mailles = cur.execute(
            "SELECT X(CENTROID(GEOMETRY)), Y(CENTROID(GEOMETRY)) FROM mailles ORDER BY OGC_FID"
        ).fetchall()
        nmailles = len(xy_mailles)
        with open(os.path.join(sat_dir, "mesh.xyz"), "w") as fil:
            for [xm, ym] in xy_mailles:
                fil.write("%f %f %f\n" % (xm, ym, EPAISSEUR_2D))

        # points
        nPoints = 2 * nnoeuds
        logger.debug("Nombre de points =", nPoints)

        zero_dir = os.path.join(sat_dir, "0")
        constant_dir = os.path.join(sat_dir, "constant")
        mesh_dir = os.path.join(sat_dir, "constant", "polyMesh")
        os.makedirs(mesh_dir)

        OpenFoam.entete(mesh_dir, "points", "vectorField", "constant/polyMesh")
        with open(os.path.join(mesh_dir, "points"), "a") as fil:

            fil.write("%d\n" % (nPoints))
            fil.write("(\n")
            for row in xy_noeuds:
                fil.write("(%.3f %.3f 0)\n" % (row[0], row[1]))
            for row in xy_noeuds:
                fil.write("(%.3f %.3f %f)\n" % (row[0], row[1], EPAISSEUR_2D))
            fil.write(")\n\n")
            fil.write(
                "// ************************************************************************* //\n"
            )

        # edges
        logger.debug("Edges ...")
        contour_map = {}  # lignes du contour extérieur
        lines_map = {}  # lignes internes (comptées dans les deux sens)

        for [mid, a, b, c, d] in self.mailles:
            if d:
                for e in [(a, b), (b, c), (c, d), (d, a)]:
                    lines_map[e] = mid - 1
                    if (e[1], e[0]) in contour_map:
                        del contour_map[(e[1], e[0])]
                    else:
                        contour_map[e] = mid - 1
            else:
                for e in [(a, b), (b, c), (c, a)]:
                    lines_map[e] = mid - 1
                    if (e[1], e[0]) in contour_map:
                        del contour_map[(e[1], e[0])]
                    else:
                        contour_map[e] = mid - 1

        logger.debug("Nombre total de faces sur le contour = ", len(contour_map))
        logger.debug("Nombre de lignes algebriques = ", len(lines_map))
        nExternalFaces = len(contour_map) + 2 * nmailles
        logger.debug("Nombre de faces externes = ", nExternalFaces)

        internal_map = {}  # lignes internes (comptées une seule fois)
        internal_cells = []
        internal_edges = []
        for [mid, a, b, c, d] in self.mailles:
            if d:
                for e in [(a, b), (b, c), (c, d), (d, a)]:
                    if e not in contour_map and (e[1], e[0]) not in internal_map:
                        internal_map[e] = mid - 1
                        internal_cells.append(mid - 1)
                        internal_edges.append(e)
            else:
                for e in [(a, b), (b, c), (c, a)]:
                    if e not in contour_map and (e[1], e[0]) not in internal_map:
                        internal_map[e] = mid - 1
                        internal_cells.append(mid - 1)
                        internal_edges.append(e)

        potentiel_map = {}  # lignes du contour à potentiel imposé
        for e in contour_map:
            if e[0] in noeuds_potentiel and e[1] in noeuds_potentiel:
                potentiel_map[e] = contour_map[e]
        for e in potentiel_map:
            del contour_map[e]

        # searching for internal nodes with fixed potential
        # = nodes in noeuds_potentiel and not in potentiel_map
        nodes_potentiel_map = sorted(
            list(
                dict.fromkeys(
                    [x for sublist in list(potentiel_map.keys()) for x in sublist]
                )
            )
        )

        internal_potential_nodes = sorted(
            list(set(noeuds_potentiel) - set(nodes_potentiel_map))
        )

        logger.debug(
            "Nombre de noeuds internes avec potentiel impose = ",
            len(internal_potential_nodes),
        )

        internal_potential_cells = {}
        for [mid, a, b, c, d] in self.mailles:
            sm = set([a, b, c, d]) if d else set([a, b, c])
            s = set(internal_potential_nodes).intersection(sm)
            if len(s):
                internal_potential_cells[mid] = sorted(list(s))

        internal_potential_cellvalues = {}
        for m in internal_potential_cells:
            p = 0.0
            for n in internal_potential_cells[m]:
                p += potentiel_impose[n]
            internal_potential_cellvalues[m] = p / len(internal_potential_cells[m])

        with open(os.path.join(sat_dir, "fixedPotentialList.txt"), "w") as fil:
            fil.write("fixedPotentialList\n")
            fil.write("(\n")
            for m in internal_potential_cells:
                fil.write(
                    "    ( ( %f %f %f ) %f )\n"
                    % (
                        xy_mailles[m - 1][0],
                        xy_mailles[m - 1][1],
                        EPAISSEUR_2D,
                        internal_potential_cellvalues[m],
                    )
                )
            fil.write(");\n")

        logger.debug("Nombre de faces avec potentiel impose = ", len(potentiel_map))
        logger.debug(
            "Nombre de faces sur le contour sans potentiel impose = ", len(contour_map)
        )

        # faces
        nInternalFaces = len(internal_map)
        logger.debug("Nombre de faces internes = ", nInternalFaces)
        nFaces = nInternalFaces + nExternalFaces
        logger.debug("Nombre total de faces = ", nFaces)

        OpenFoam.entete(mesh_dir, "faces", "faceList", "constant/polyMesh")
        with open(os.path.join(mesh_dir, "faces"), "a") as fil:

            fil.write("%d\n" % (nFaces))
            fil.write("(\n")

            # faces internes
            for a, b in internal_edges:
                fil.write(
                    "4(%d %d %d %d)\n"
                    % (a - 1, b - 1, b - 1 + nnoeuds, a - 1 + nnoeuds)
                )

            # mur (il faut inverser le sens)
            for [mid, a, b, c, d] in self.mailles:
                if d:
                    fil.write("4(%d %d %d %d)\n" % (d - 1, c - 1, b - 1, a - 1))
                else:
                    fil.write("3(%d %d %d)\n" % (c - 1, b - 1, a - 1))

            # mnt
            for [mid, a, b, c, d] in self.mailles:
                if d:
                    fil.write(
                        "4(%d %d %d %d)\n"
                        % (
                            a - 1 + nnoeuds,
                            b - 1 + nnoeuds,
                            c - 1 + nnoeuds,
                            d - 1 + nnoeuds,
                        )
                    )
                else:
                    fil.write(
                        "3(%d %d %d)\n"
                        % (a - 1 + nnoeuds, b - 1 + nnoeuds, c - 1 + nnoeuds)
                    )

            # contour
            for a, b in contour_map:
                fil.write(
                    "4(%d %d %d %d)\n"
                    % (a - 1, b - 1, b - 1 + nnoeuds, a - 1 + nnoeuds)
                )

            # potentiel impose
            for a, b in potentiel_map:
                fil.write(
                    "4(%d %d %d %d)\n"
                    % (a - 1, b - 1, b - 1 + nnoeuds, a - 1 + nnoeuds)
                )

            fil.write(")\n\n")
            fil.write(
                "// ************************************************************************* //\n"
            )

        # cells
        nCells = nmailles
        logger.debug("Nombre de cellules = ", nCells)

        logger.debug("owners ...")
        OpenFoam.entete(
            mesh_dir,
            "owner",
            "labelList",
            "constant/polyMesh",
            "npoints:%d  nCells:%d  nFaces:%d  nInternalFaces:%d"
            % (nPoints, nCells, nFaces, nInternalFaces),
        )
        with open(os.path.join(mesh_dir, "owner"), "a") as fil:

            fil.write("%d\n" % (nFaces))
            fil.write("(\n")
            for cell in internal_cells:
                fil.write("%d\n" % (cell))
            for num in range(nmailles):
                fil.write("%d\n" % (num))
            for num in range(nmailles):
                fil.write("%d\n" % (num))
            for a, b in contour_map:
                fil.write("%d\n" % (contour_map[(a, b)]))
            for a, b in potentiel_map:
                fil.write("%d\n" % (potentiel_map[(a, b)]))

            fil.write(")\n\n")
            fil.write(
                "// ************************************************************************* //\n"
            )

        logger.debug("neighbours ...")
        OpenFoam.entete(
            mesh_dir,
            "neighbour",
            "labelList",
            "constant/polyMesh",
            "npoints:%d  nCells:%d  nFaces:%d  nInternalFaces:%d"
            % (nPoints, nCells, nFaces, nInternalFaces),
        )
        with open(os.path.join(mesh_dir, "neighbour"), "a") as fil:

            fil.write("%d\n" % (nInternalFaces))
            fil.write("(\n")
            for a, b in internal_edges:
                fil.write("%d\n" % (lines_map[(b, a)]))

            fil.write(")\n\n")
            fil.write(
                "// ************************************************************************* //\n"
            )

        logger.debug("boundary ...")
        OpenFoam.entete(mesh_dir, "boundary", "polyBoundaryMesh", "constant/polyMesh")
        with open(os.path.join(mesh_dir, "boundary"), "a") as fil:

            fil.write("%d\n" % (4))
            fil.write("(\n")
            fil.write("    mur\n")
            fil.write("    {\n")
            fil.write("        type            empty;\n")
            fil.write("        nFaces          %d;\n" % (nmailles))
            fil.write("        startFace       %d;\n" % (nInternalFaces))
            fil.write("    }\n")
            fil.write("    mnt\n")
            fil.write("    {\n")
            fil.write("        type            empty;\n")
            fil.write("        nFaces          %d;\n" % (nmailles))
            fil.write("        startFace       %d;\n" % (nInternalFaces + nmailles))
            fil.write("    }\n")
            fil.write("    wall\n")
            fil.write("    {\n")
            fil.write("        type            patch;\n")
            fil.write("        nFaces          %d;\n" % (len(contour_map)))
            fil.write("        startFace       %d;\n" % (nInternalFaces + 2 * nmailles))
            fil.write("    }\n")
            fil.write("    contour\n")
            fil.write("    {\n")
            fil.write("        type            patch;\n")
            fil.write("        nFaces          %d;\n" % (len(potentiel_map)))
            fil.write(
                "        startFace       %d;\n"
                % (nInternalFaces + 2 * nmailles + len(contour_map))
            )
            fil.write("    }\n")
            fil.write(")\n\n")
            fil.write(
                "// ************************************************************************* //\n"
            )

        logger.debug("potential ...")
        OpenFoam.entete(constant_dir, "potential", "volScalarField", "0")
        with open(os.path.join(constant_dir, "potential"), "a") as fil:

            fil.write("dimensions      [0 1 0 0 0 0 0];\n\n")
            fil.write("internalField   nonuniform List<scalar>\n")

            rows = cur.execute(
                """SELECT potentiel, potentiel_reference, altitude
                                        FROM noeuds ORDER BY OGC_FID"""
            ).fetchall()

            fil.write("%d\n" % (nmailles))
            fil.write("(\n")

            if rows[0][0]:
                ip = 0
            elif rows[0][1]:
                ip = 1
                logger.debug("using potentiel_reference")
            else:
                ip = 2
                logger.debug("using altitude")

            dmin = 0.1
            for [mid, a, b, c, d] in self.mailles:
                value = min(rows[a - 1][ip], rows[a - 1][2] - dmin)
                value += min(rows[b - 1][ip], rows[b - 1][2] - dmin)
                value += min(rows[c - 1][ip], rows[c - 1][2] - dmin)
                if d:
                    value += min(rows[d - 1][ip], rows[d - 1][2] - dmin)
                    value = 0.25 * value
                else:
                    value = value / 3.0

                fil.write("%f\n" % (value))

            fil.write(")\n")
            fil.write(";\n\n")

            fil.write("boundaryField\n")
            fil.write("{\n")
            fil.write("    mur\n")
            fil.write("    {\n")
            fil.write("        type            empty;\n")
            fil.write("    }\n")
            fil.write("    mnt\n")
            fil.write("    {\n")
            fil.write("        type            empty;\n")
            fil.write("    }\n")
            fil.write("    wall\n")
            fil.write("    {\n")
            fil.write("        type            zeroGradient;\n")
            fil.write("    }\n")
            fil.write("    contour\n")
            fil.write("    {\n")
            fil.write("        type            fixedValue;\n")
            fil.write("        value           nonuniform List<scalar>\n")
            fil.write("%d\n" % (len(potentiel_map)))
            fil.write("(\n")
            for a, b in potentiel_map:
                fil.write("%f\n" % (0.5 * (potentiel_impose[a] + potentiel_impose[b])))
            fil.write(");\n")
            fil.write("    }\n")
            fil.write("}\n\n")
            fil.write(
                "// ************************************************************************* //\n"
            )

        logger.debug("hwater ...")
        OpenFoam.entete(constant_dir, "hwater", "volScalarField", "0")
        with open(os.path.join(constant_dir, "hwater"), "a") as fil:

            fil.write("dimensions      [0 1 0 0 0 0 0];\n\n")
            fil.write("internalField   nonuniform List<scalar>\n")

            rows = cur.execute(
                """SELECT potentiel, potentiel_reference, altitude, altitude_mur
                                    FROM noeuds ORDER BY OGC_FID"""
            ).fetchall()

            fil.write("%d\n" % (nmailles))
            fil.write("(\n")

            if rows[0][0]:
                ip = 0
                logger.debug("using potentiel for hwater")
            elif rows[0][1]:
                ip = 1
                logger.debug("using potentiel_reference for hwater")
            else:
                ip = 2
                logger.debug("using altitude for hwater")

            dmin = 0.1
            for [mid, a, b, c, d] in self.mailles:
                value = max(rows[a - 1][ip] - rows[a - 1][3], dmin)
                value += max(rows[b - 1][ip] - rows[b - 1][3], dmin)
                value += max(rows[c - 1][ip] - rows[c - 1][3], dmin)
                if d:
                    value += max(rows[d - 1][ip] - rows[d - 1][3], dmin)
                    value = 0.25 * value
                else:
                    value = value / 3.0

                fil.write("%f\n" % (value))

            fil.write(")\n")
            fil.write(";\n\n")

            fil.write("boundaryField\n")
            fil.write("{\n")
            fil.write("    mur\n")
            fil.write("    {\n")
            fil.write("        type            empty;\n")
            fil.write("    }\n")
            fil.write("    mnt\n")
            fil.write("    {\n")
            fil.write("        type            empty;\n")
            fil.write("    }\n")
            fil.write("    wall\n")
            fil.write("    {\n")
            fil.write("        type            zeroGradient;\n")
            fil.write("    }\n")
            fil.write("    contour\n")
            fil.write("    {\n")
            fil.write("        type            fixedValue;\n")
            fil.write("        value           nonuniform List<scalar>\n")
            fil.write("%d\n" % (len(potentiel_map)))
            fil.write("(\n")
            for a, b in potentiel_map:
                value = 0.5 * (potentiel_impose[a] + potentiel_impose[b])
                mur = 0.5 * (rows[a - 1][3] + rows[b - 1][3])
                fil.write("%f\n" % (max(value - mur, dmin)))
                if value < mur + dmin:
                    logger.notice(
                        "hwater lower than", dmin, "m on contour", value, mur, a, b
                    )
            fil.write(");\n")
            fil.write("    }\n")
            fil.write("}\n\n")
            fil.write(
                "// ************************************************************************* //\n"
            )

        if not hynverse:
            logger.debug("seepage ...")
            OpenFoam.entete(constant_dir, "seepageTerm", "volScalarField", "0")
            with open(os.path.join(constant_dir, "seepageTerm"), "a") as fil:

                fil.write("dimensions      [0 1 -1 0 0 0 0];\n\n")
                fil.write("internalField   nonuniform List<scalar>\n")

                rows = [
                    x[0]
                    for x in cur.execute(
                        "SELECT flux_eau FROM mailles ORDER BY OGC_FID"
                    ).fetchall()
                ]

                fil.write("%d\n" % (nmailles))
                fil.write("(\n")
                for row in rows:
                    fil.write("%e\n" % (row))

                fil.write(")\n")
                fil.write(";\n\n")

                fil.write("boundaryField\n")
                fil.write("{\n")
                fil.write("    mur\n")
                fil.write("    {\n")
                fil.write("        type            empty;\n")
                fil.write("    }\n")
                fil.write("    mnt\n")
                fil.write("    {\n")
                fil.write("        type            empty;\n")
                fil.write("    }\n")
                fil.write("    wall\n")
                fil.write("    {\n")
                fil.write("        type            calculated;\n")
                fil.write("        value           uniform 0;\n")
                fil.write("    }\n")
                fil.write("    contour\n")
                fil.write("    {\n")
                fil.write("        type            calculated;\n")
                fil.write("        value           uniform 0;\n")
                fil.write("    }\n")
                fil.write("}\n\n")
                fil.write(
                    "// ************************************************************************* //\n"
                )

        logger.debug("meshid ...")
        OpenFoam.entete(constant_dir, "meshid", "volScalarField", "constant")
        with open(os.path.join(constant_dir, "meshid"), "a") as fil:

            fil.write("dimensions      [0 0 0 0 0 0 0];\n\n")
            fil.write("internalField   nonuniform List<scalar>\n")

            fil.write("%d\n" % (nmailles))
            fil.write("(\n")
            for [mid, a, b, c, d] in self.mailles:
                fil.write("%d\n" % (mid - 1))

            fil.write(");\n\n")

            OpenFoam.boundaryField(fil)

    def create_mnt_sat(self, sat_dir, cur):
        """builds the mnt saturated OPENFOAM file

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param cur: cursor
        :type cur: sqlite3.Cursor
        """

        logger.debug("mnt ...")
        constant_dir = os.path.join(sat_dir, "constant")

        nmailles = len(self.mailles)

        OpenFoam.entete(constant_dir, "potentialDEM", "volScalarField", "constant")
        with open(os.path.join(constant_dir, "potentialDEM"), "a") as fil:

            fil.write("dimensions      [0 1 0 0 0 0 0];\n\n")
            fil.write("internalField   nonuniform List<scalar>\n")

            rows = [
                x[0]
                for x in cur.execute(
                    "SELECT altitude FROM mailles ORDER BY OGC_FID"
                ).fetchall()
            ]

            fil.write("%d\n" % (nmailles))
            fil.write("(\n")
            for row in rows:
                fil.write("%3f\n" % (row))

            fil.write(");\n\n")

            OpenFoam.boundaryField(fil)

    def create_mur_sat(self, sat_dir, cur):
        """builds the bedrock saturated OPENFOAM file

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param cur: cursor
        :type cur: sqlite3.Cursor
        """
        logger.debug("mur ...")
        constant_dir = os.path.join(sat_dir, "constant")

        nmailles = len(self.mailles)

        OpenFoam.entete(constant_dir, "z0", "volScalarField", "constant", "z0")
        with open(os.path.join(constant_dir, "z0"), "a") as fil:

            fil.write("dimensions      [0 1 0 0 0 0 0];\n\n")
            fil.write("internalField   nonuniform List<scalar>\n")

            rows = [
                x[0]
                for x in cur.execute(
                    "SELECT altitude_mur FROM mailles ORDER BY OGC_FID"
                ).fetchall()
            ]

            fil.write("%d\n" % (nmailles))
            fil.write("(\n")
            for row in rows:
                fil.write("%3f\n" % (row))

            fil.write(");\n\n")

            OpenFoam.boundaryField(fil)

    def create_infiltration_sat(self, sat_dir, cur):
        """builds the infiltration saturated OPENFOAM file

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param cur: cursor
        :type cur: sqlite3.Cursor
        """

        logger.debug("infiltration ...")
        constant_dir = os.path.join(sat_dir, "0")

        nmailles = len(self.mailles)

        OpenFoam.entete(constant_dir, "infiltration", "volScalarField", "0")
        with open(os.path.join(constant_dir, "infiltration"), "a") as fil:

            fil.write("dimensions      [0 1 -1 0 0 0 0];\n\n")
            fil.write("internalField   nonuniform List<scalar>\n")

            rows = [
                -x[0]
                for x in cur.execute(
                    "SELECT infiltration FROM mailles ORDER BY OGC_FID"
                ).fetchall()
            ]

            fil.write("%d\n" % (nmailles))
            fil.write("(\n")
            for row in rows:
                fil.write("%e\n" % (row))

            fil.write(");\n\n")

            OpenFoam.boundaryField(fil)

    def create_permeabilities_sat(self, sat_dir, cur=None, liste_pm=None):
        """builds the permeabilities saturated OPENFOAM file

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param cur: cursor
        :type cur: sqlite3.Cursor
        :param liste_pm: permeabilities data per mesh element
        :type liste_pm: list
        """

        coef = 1.0e-6 / 9.81  # m/s => m2

        logger.debug("permeability ...")
        constant_dir = os.path.join(sat_dir, "constant")

        OpenFoam.entete(constant_dir, "K", "volScalarField", "constant")
        with open(os.path.join(constant_dir, "K"), "a") as fil:

            fil.write("dimensions      [0 2 0 0 0 0 0];\n\n")
            fil.write("internalField   nonuniform List<scalar>\n")

            if liste_pm:
                meshid = OpenFoam.get_meshid(sat_dir)
                nm = len(liste_pm)
                fil.write("%d\n" % (nm))
                fil.write("(\n")
                for m in meshid:
                    fil.write("%15.7e\n" % (liste_pm[m][0] * coef))
            elif cur:
                rows = [
                    x[0]
                    for x in cur.execute(
                        "SELECT permeabilite_x FROM mailles ORDER BY OGC_FID"
                    ).fetchall()
                ]
                nm = len(rows)
                fil.write("%d\n" % (nm))
                fil.write("(\n")
                for row in rows:
                    fil.write("%15.7e\n" % (row * coef))

            fil.write(");\n\n")

            OpenFoam.boundaryField(fil)

    def create_velocities_sat(self, sat_dir, cur=None, liste_vm=None, init=False):
        """builds the velocities saturated OPENFOAM file

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param cur: cursor
        :type cur: sqlite3.Cursor
        :param liste_vm: velocities data per mesh element
        :type liste_vm: list
        :param init: boolean, True if only initialisation to (0 0 0)
        :type init: boolean
        """

        logger.debug("velocity ...")
        constant_dir = os.path.join(sat_dir, "constant")

        OpenFoam.entete(constant_dir, "U", "volVectorField", "0")
        with open(os.path.join(constant_dir, "U"), "a") as fil:

            fil.write("dimensions      [0 1 -1 0 0 0 0];\n\n")

            if init:
                fil.write("internalField   uniform (0 0 0);\n")
            elif liste_vm:
                fil.write("internalField   nonuniform List<vector>\n")
                nm = len(liste_vm)
                fil.write("%d\n" % (nm))
                fil.write("(\n")
                for x in liste_vm:
                    fil.write("%e %e 0\n" % (x[0], x[1]))
                fil.write(");\n\n")
            elif cur:
                fil.write("internalField   nonuniform List<vector>\n")
                rows = cur.execute(
                    "SELECT v_x, v_y FROM mailles ORDER BY OGC_FID"
                ).fetchall()
                nm = len(rows)
                fil.write("%d\n" % (nm))
                fil.write("(\n")
                for row in rows:
                    fil.write("(%e %e 0)\n" % (row[0], row[1]))
                fil.write(");\n\n")

            OpenFoam.boundaryField(fil, wall="fixedValue (0 0 0)")

    def create_probes_sat(self, sat_dir, cur, field="C"):
        """builds the probes file for OPENFOAM

        :param sat_dir: path of saturated folder
        :type sat_dir: string
        :param cur: cursor
        :type cur: sqlite3.Cursor
        """

        if not self.points_interet:
            return

        logger.debug("probes ...")
        probes_file = os.path.join(sat_dir, "system", "probes")

        templates_dir = os.path.join(os.path.dirname(__file__), "templates", "openfoam")
        shutil.copyfile(os.path.join(templates_dir, "openfoam_entete"), probes_file)

        cur.execute(
            """SELECT nom, X(GEOMETRY), Y(GEOMETRY)
            FROM points_interet WHERE groupe='calcul' ORDER BY nom"""
        )
        with open(probes_file, "a") as fil:

            fil.write("type            probes;\n")
            fil.write('libs            ("libsampling.so");\n')
            fil.write("interpolationScheme cellPoint;\n")
            fil.write("writeControl    timeStep;\n")
            fil.write("writeInterval   1;\n\n")

            fil.write("fields (%s);\n" % (field))
            fil.write("probeLocations\n")
            fil.write("(\n")

            for row in cur.fetchall():
                fil.write(
                    "    (%.2f %.2f %.0f) // %s\n"
                    % (row[1], row[2], 0.5 * EPAISSEUR_2D, row[0])
                )

            fil.write(");\n\n")

    def update_database(self, conn, compute_dir):
        """update database after inversion

        :param conn: conn on a sqlite database
        :type conn: sqlite3.Connection
        :param compute_dir: path of compute folder
        :type compute_dir: string
        """
        cur = conn.cursor()
        (nnoeuds,) = cur.execute("SELECT COUNT(1) FROM noeuds").fetchone()
        (nmailles,) = cur.execute("SELECT COUNT(1) FROM mailles").fetchone()

        last_time = OpenFoam.times_dir_names(compute_dir)[-1]
        meshid = OpenFoam.get_meshid(compute_dir)

        logger.notice("updating potential")
        data = OpenFoam.read_openfoam_file(
            os.path.join(compute_dir, last_time, "potential"), False
        )
        values = [(v, meshid[j] + 1) for j, v in enumerate(data)]
        cur.executemany("UPDATE mailles SET potentiel = ? WHERE OGC_FID = ?", values)
        db_elem_to_node(cur, "potentiel", values)

        logger.notice("updating water flux")
        data = OpenFoam.read_openfoam_file(
            os.path.join(compute_dir, last_time, "seepageTerm"), False
        )
        values = [(v, meshid[j] + 1) for j, v in enumerate(data)]
        cur.executemany("UPDATE mailles SET flux_eau = ? WHERE OGC_FID = ?", values)
        db_elem_to_node(cur, "flux_eau", values)

        logger.notice("updating Darcy velocities")
        data = OpenFoam.read_openfoam_file(
            os.path.join(compute_dir, last_time, "U"), False
        )
        values = numpy.array(
            [
                (vx, vy, vz, sqrt(vx * vx + vy * vy + vz * vz), meshid[j] + 1)
                for j, (vx, vy, vz) in enumerate(data)
            ]
        )
        cur.executemany(
            "UPDATE mailles SET v_x = ?, v_y = ?, v_z = ?, v_norme = ? WHERE OGC_FID = ?",
            values,
        )
        db_elem_to_node(cur, "v_x", values[:, [0, 4]])
        db_elem_to_node(cur, "v_y", values[:, [1, 4]])
        db_elem_to_node(cur, "v_z", values[:, [2, 4]])
        db_elem_to_node(cur, "v_norme", values[:, [3, 4]])

        logger.notice("updating ZS and ZNS thicknesses")
        cur.execute(
            "SELECT altitude, altitude_mur, potentiel FROM noeuds ORDER BY OGC_FID"
        )

        for i, [mnt, mur, potentiel] in enumerate(cur.fetchall()):
            cur.execute(
                "UPDATE noeuds SET epaisseur_zs = ? WHERE OGC_FID = ? ",
                (potentiel - mur, i + 1),
            )
            cur.execute(
                "UPDATE noeuds SET epaisseur_zns = ? WHERE OGC_FID = ? ",
                (mnt - potentiel, i + 1),
            )

        cur.execute(
            "SELECT altitude, altitude_mur, potentiel FROM mailles ORDER BY OGC_FID"
        )

        for i, [mnt, mur, potentiel] in enumerate(cur.fetchall()):
            cur.execute(
                "UPDATE mailles SET epaisseur_zs = ? WHERE OGC_FID = ? ",
                (potentiel - mur, i + 1),
            )
            cur.execute(
                "UPDATE mailles SET epaisseur_zns = ? WHERE OGC_FID = ? ",
                (mnt - potentiel, i + 1),
            )

    @staticmethod
    def boundaryField(fil, wall="zeroGradient"):
        """builds the boundary section for OPENFOAM file

        :param fil: buffered file
        :type fil: BufferedIOBase
        :param wall: wall value if fixed
        :type wall: string
        """
        fil.write("boundaryField\n")
        fil.write("{\n")
        fil.write("    mur\n")
        fil.write("    {\n")
        fil.write("        type            empty;\n")
        fil.write("    }\n")
        fil.write("    mnt\n")
        fil.write("    {\n")
        fil.write("        type            empty;\n")
        fil.write("    }\n")
        fil.write("    wall\n")
        fil.write("    {\n")
        if wall == "zeroGradient":
            fil.write("        type            zeroGradient;\n")
        elif wall[:10] == "fixedValue":
            fil.write("        type            fixedValue;\n")
            fil.write("        value           uniform %s;\n" % (wall[11:]))
        fil.write("    }\n")
        fil.write("    contour\n")
        fil.write("    {\n")
        fil.write("        type            zeroGradient;\n")
        fil.write("    }\n")
        fil.write("}\n\n")
        fil.write(
            "// ************************************************************************* //\n"
        )

    @staticmethod
    def entete(name_dir, name_file, classe, location, note=None):
        """builds the leading section of OPENFOAM files

        :param name_dir: path of folder
        :type name_dir: string
        :param classe: classe name
        :type classe: string
        :param location: path to location
        :type location: string
        :param note: optional note
        :type note: string
        """
        templates_dir = os.path.join(os.path.dirname(__file__), "templates", "openfoam")
        shutil.copyfile(
            os.path.join(templates_dir, "openfoam_entete"),
            os.path.join(name_dir, name_file),
        )
        with open(os.path.join(name_dir, name_file), "a") as fil:
            fil.write("FoamFile\n")
            fil.write("{\n")
            fil.write("    version     2.0;\n")
            fil.write("    format      ascii;\n")
            fil.write("    class       %s;\n" % (classe))
            if note:
                fil.write('    note        "%s";\n' % (note))
            fil.write('    location    "%s";\n' % (location))
            fil.write("    object      %s;\n" % (name_file))
            fil.write("}\n")
            fil.write(
                "// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * //\n\n"
            )


# run as script if invoqued as such
if __name__ == "__main__":
    import sys

    from ..settings import Settings

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    logger.enable_console(True)

    if len(sys.argv) not in [2, 3, 4, 5]:
        logger.error("wrong number of arguments (try compute.py --help)")
        exit(1)

    dbname = None
    for arg in sys.argv[1:]:
        if arg[-7:] == ".sqlite":
            dbname = arg

    if not dbname:
        logger.error(
            "cannot find a '.sqlite' file in arguments (try python -m thyrsis.simulation.compute --help)"
        )
        exit(1)

    code = OpenFoam(dbname, Settings(os.path.dirname(os.path.abspath(dbname))))
    sat_dir = "sature"
    constant_dir = os.path.join(sat_dir, "constant")
    if os.path.isdir(constant_dir):
        shutil.rmtree(constant_dir)
    os.makedirs(constant_dir)
    system_dir = os.path.join(sat_dir, "system")
    if os.path.isdir(system_dir):
        shutil.rmtree(system_dir)
    os.makedirs(system_dir)

    with sqlite.connect(dbname) as conn:
        cur = conn.cursor()

        code.create_permeabilities_sat(sat_dir, cur)
        code.create_mur_sat(sat_dir, cur)
        code.create_mnt_sat(sat_dir, cur)
        code.create_maillage_sat(sat_dir, cur)
        code.create_probes_sat(sat_dir, cur)
