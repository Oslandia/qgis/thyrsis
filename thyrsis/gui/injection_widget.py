"""
Crisis interface to define simulation with
a mininumum of user interface
"""
import math
import os
from builtins import object, range, str

from PyQt5.QtCore import QDate, Qt
from PyQt5.QtWidgets import QLineEdit, QWidget
from qgis.core import Qgis, QgsGeometry, QgsPointXY, QgsWkbTypes
from qgis.gui import QgsRubberBand
from qgis.PyQt import uic
from qgis.PyQt.QtGui import QIcon
from qgis.utils import iface

from ..log import logger
from .point_tool import PointTool


class RaiiRubberBand(object):
    """Decorator that ensures that rubberband is properly removed"""

    def __init__(self, canvas, geom_type):
        """Constructor

        :param canvas: Qgis interface canvas
        :type canvas: QgsMapCanvas
        :param geom_type: Geometry type
        :type canvas: int

        """
        self.__band = QgsRubberBand(canvas, geom_type)
        self.__band.setColor(Qt.red)
        self.__band.setWidth(2)
        self.__band.setStrokeColor(Qt.black)
        self.__scene = canvas.scene()

    def __del__(self):
        """delete properly the rubberband

        :return: None
        :rtype: None
        """
        self.__scene.removeItem(self.__band)

    def addGeometry(self, geom, layer):
        """Add a geometry as rubberband

        :param geom: geometry
        :type geom: QgsGeometry
        :param layer: layer
        :type layer: QgsVectorLayer

        :return: None
        :rtype: None
        """
        self.__band.addGeometry(geom, layer)

    def addPoint(self, point):
        """set the database property

        :param point: point geometry
        :type point: QgsPoint

        :return: None
        :rtype: None
        """
        self.__band.addPoint(QgsPointXY(point))

    def asGeometry(self):
        """return the rubberband as a QgsGeometry

        :return: rubberband geometry
        :rtype: QgsGeometry
        """
        return self.__band.asGeometry()


class InjectionWidget(QWidget):
    """Widget to define the injection properties"""

    def __init__(self, type_, canvas, attr=None, parent=None):
        """Constructor

        :param type_: injection type
        :type type_: string
        :param canvas: Qgis interface canvas
        :type canvas: QgsMapCanvas
        :param attr: dictionnary of injection attributes
        :type attr: dict
        :param parent: widget parent
        :type parent: QWidget
        """
        super(InjectionWidget, self).__init__(parent)
        if type_ == "mass":
            uic.loadUi(os.path.join(os.path.dirname(__file__), "crisis_mass.ui"), self)
        elif type_ == "flux":
            uic.loadUi(os.path.join(os.path.dirname(__file__), "crisis_flux.ui"), self)
        elif type_ == "concentration":
            uic.loadUi(
                os.path.join(os.path.dirname(__file__), "crisis_concentration.ui"), self
            )
        else:
            uic.loadUi(
                os.path.join(os.path.dirname(__file__), "crisis_aucune.ui"), self
            )

        self.__type = type_
        self.__canvas = canvas
        self.__oldTool = None
        self.__pickTool = None
        self.__rubberband = None
        self.__geom = []

        attr = attr or {
            "nom": "Sans nom",
            "debut": QDate.currentDate().toString("yyyy-MM-dd"),
            "duree": "0 {}".format(self.tr("year")),
            "input": 0,
            "profondeur": 0,
            "volume_eau": 0,
            "wkt": InjectionWidget.circle(0, 0, 1).asWkt(),
            "coefficient_surface": 1,
            "coefficient_injection": 1,
        }
        self.__attr = attr

        self.nameEdit.setText(attr["nom"])
        if hasattr(self, "dateEdit"):
            self.dateEdit.setDate(
                QDate.fromString(attr["debut"].split()[0], "yyyy-MM-dd")
            )
        if hasattr(self, "inputEdit"):
            self.inputEdit.setText(str(attr["input"]))
            self._add_value_check(self.inputEdit, "input", 0.0)
        if hasattr(self, "depthEdit"):
            self.depthEdit.setText(str(attr["profondeur"]))
            self._add_value_check(self.depthEdit, "profondeur", 0.0)
        if hasattr(self, "volumeEdit"):
            self.volumeEdit.setText(str(attr["volume_eau"]))
            self._add_value_check(self.volumeEdit, "volume_eau", 0.0)
        if hasattr(self, "durationEdit"):
            self.durationEdit.set_duration(attr["duree"])
        self.__geom = QgsGeometry.fromWkt(attr["wkt"])
        if hasattr(self, "surfaceEdit"):
            self._define_edit_from_geom()
            area = self.__geom.area()
            if area > 0:
                centroid = self.__geom.centroid().asPoint()
                circle = InjectionWidget.circle(centroid.x(), centroid.y(), area)
                # self.__rubberband.addGeometry(self.__geom,
                #        QgsMapLayerRegistry.instance().mapLayersByName("injections")[0])
                isCircle = (
                    abs(self.__geom.difference(circle).area() / circle.area()) < 0.01
                )
                if isCircle:
                    self.xEdit.setEnabled(True)
                    self.yEdit.setEnabled(True)
                    self.surfaceEdit.setEnabled(True)
                else:
                    self.xEdit.setEnabled(False)
                    self.yEdit.setEnabled(False)
                    self.surfaceEdit.setEnabled(False)
        self.deleteButton.setIcon(QIcon(":images/themes/default/mIconDelete.svg"))
        self.deleteButton.clicked.connect(self.deleteLater)
        if hasattr(self, "surfaceEdit"):
            self.pickButton.clicked.connect(self.__selectPoint)
            self.drawButton.clicked.connect(self.__drawZone)

            self.xEdit.editingFinished.connect(self.__createCircle)
            self.yEdit.editingFinished.connect(self.__createCircle)
            self.surfaceEdit.editingFinished.connect(self.__createCircle)

    def enterEvent(self, event):
        """display rubberband

        :param event: event description
        :type event: string

        :return: None
        :rtype: None
        """
        self.__rubberband = RaiiRubberBand(self.__canvas, QgsWkbTypes.PolygonGeometry)
        self.__rubberband.addGeometry(self.__geom, None)

    def leaveEvent(self, event):
        """clear rubberband

        :param event: event description
        :type event: string

        :return: None
        :rtype: None
        """
        self.__rubberband = None

    def __restoreTool(self, dummy=None):
        """disable the pick tool"""
        if self.__pickTool:
            self.__pickTool.pointClicked.disconnect()
            # self.__canvas.setMapTool(self.__oldTool)
            # self.__oldTool = None
            self.__pickTool = None

    def __endDrawZone(self, dummy):
        """set attributes in widget after drawing injection zone and
        disable the pick tool
        """
        self.__pickTool.deactivate()
        self.__pickTool.setParent(None)
        self.__pickTool = None
        geom = self.__drawingband.asGeometry()
        logger.debug("geom ", geom.asWkt())
        self.__drawingband = None
        if geom:  # and geom.isGeosValid():
            self.__geom = geom
            if hasattr(self, "surfaceEdit"):
                self._define_edit_from_geom()
                self.xEdit.setEnabled(False)
                self.yEdit.setEnabled(False)
                self.surfaceEdit.setEnabled(False)
        self.__canvas.setMapTool(self.__oldTool)

    def __drawZone(self):
        """activate the draw zone to define injection zone"""
        self.__drawingband = RaiiRubberBand(self.__canvas, QgsWkbTypes.PolygonGeometry)
        self.__pickTool = PointTool(self.__canvas)
        self.__oldTool = self.__canvas.mapTool()
        self.__canvas.setMapTool(self.__pickTool)
        self.__pickTool.pointClicked.connect(self.__drawingband.addPoint)
        self.__pickTool.rightClicked.connect(self.__endDrawZone)

    def __modifyXY(self, point):
        """set x, y properties from a point

        :param point: point
        :type point: QgsPoint
        """
        self.__pickTool.deactivate()
        self.__pickTool.setParent(None)
        self.__pickTool = None
        self.xEdit.setText("{:0.2f}".format(point.x()))
        self.yEdit.setText("{:0.2f}".format(point.y()))
        # Need to ask for circle creation because slot won't be called
        # Because we use editingFinished signal to avoid multiple calls
        self.__createCircle()

    def __createCircle(self, dummy=None):
        """create a circle as injection zone"""
        try:
            x = float(self.xEdit.text())
            y = float(self.yEdit.text())
            surface = float(self.surfaceEdit.text())
            if surface <= 0.0:
                raise ValueError("surface must be a positive value.")

            self.__geom = InjectionWidget.circle(x, y, surface)
        except ValueError as exc:
            iface.messageBar().pushMessage(
                self.tr("Warning"),
                self.tr(
                    "Invalid values for injection area definition : {}".format(exc)
                ),
                level=Qgis.Warning,
            )
            # Restore last valid value from geom
            self._define_edit_from_geom()

    def _define_edit_from_geom(self) -> None:
        """Define edit from saved geom"""
        self.surfaceEdit.blockSignals(True)
        self.xEdit.blockSignals(True)
        self.yEdit.blockSignals(True)

        area = self.__geom.area()
        centroid = self.__geom.centroid().asPoint()
        self.surfaceEdit.setText("{:0.2f}".format(area))
        self.xEdit.setText("{:0.2f}".format(centroid.x()))
        self.yEdit.setText("{:0.2f}".format(centroid.y()))

        self.surfaceEdit.blockSignals(False)
        self.xEdit.blockSignals(False)
        self.yEdit.blockSignals(False)

    def __selectPoint(self):
        """activate the pick point mode"""
        self.xEdit.setEnabled(True)
        self.yEdit.setEnabled(True)
        self.surfaceEdit.setEnabled(True)
        self.__pickTool = PointTool(self.__canvas)
        self.__canvas.setMapTool(self.__pickTool)
        self.__pickTool.pointClicked.connect(self.__modifyXY)

    def _add_value_check(
        self, line_edit: QLineEdit, attribute: str, min_val: float = None
    ) -> None:
        """Add connection for value check of a QLineEdit

        :param line_edit: QLineEdit to check
        :type line_edit: QLineEdit
        :param attribute: attribute associated to QLineEdit, used for value restore
        :type attribute: str
        :param min_val: Minimum acceptable value, defaults to None
        :type min_val: float, optional
        """
        line_edit.editingFinished.connect(
            lambda: self._line_edit_value_changed(
                line_edit=line_edit, attribute=attribute, min_val=min_val
            )
        )

    def _line_edit_value_changed(
        self, line_edit: QLineEdit, attribute: str, min_val: float = None
    ) -> None:
        """Check QLineEdit value for float conversion and minimu value

        :param line_edit: QLineEdit to check
        :type line_edit: QLineEdit
        :param attribute: attribute associated to QLineEdit, used for value restore
        :type attribute: str
        :param min_val: Minimum acceptable value, defaults to None
        :type min_val: float, optional
        """
        new_value_str = line_edit.text()
        try:
            value = float(new_value_str)
            if min_val is not None and value < min_val:
                raise ValueError(self.tr("Must be over {}").format(min_val))
        except ValueError as exc:
            iface.messageBar().pushMessage(
                self.tr("Warning"),
                self.tr(
                    "Invalid value for attribute {} definition : {}".format(
                        attribute, exc
                    )
                ),
                level=Qgis.Warning,
            )
            # Restore last valid value
            line_edit.blockSignals(True)
            line_edit.setText(str(self.__attr[attribute]))
            line_edit.blockSignals(False)

    @staticmethod
    def circle(x, y, area):
        """Create a circle QgsGeometry

        :param x: x coordinate
        :type x: float
        :param y: y coordinate
        :type y: float
        :param area: area value
        :type area: float

        :return: circle
        :rtype: QgsGeometry
        """
        nb_seg = 32
        d_theta = 2 * math.pi / 32
        ring = []
        r = math.sqrt(area / math.pi)
        for t in range(nb_seg):
            ring.append(
                QgsPointXY(x + r * math.cos(t * d_theta), y + r * math.sin(t * d_theta))
            )
        return QgsGeometry.fromMultiPolygonXY([[ring]])

    def attributes(self):
        """return a dic of attributes

        :return: dictionnary of injection attributes
        :rtype: dict
        """

        new_attr = {"nom": self.nameEdit.text(), "wkt": self.__geom.asWkt()}

        if hasattr(self, "durationEdit"):
            new_attr["duree"] = self.durationEdit.get_duration()

        if hasattr(self, "dateEdit"):
            new_attr["debut"] = self.dateEdit.date().toString("yyyy-MM-dd")

        if hasattr(self, "inputEdit"):
            new_attr["input"] = (
                float(self.inputEdit.text()) if self.inputEdit.text() else 0.0
            )
            if new_attr["input"] < 0:
                raise ValueError(
                    self.tr("{} must be a positive value.").format(self.__type)
                )

        if hasattr(self, "volumeEdit"):
            new_attr["volume_eau"] = (
                float(self.volumeEdit.text()) if self.volumeEdit.text() else 0.0
            )
            if new_attr["volume_eau"] < 0:
                raise ValueError(self.tr("water volume must be a positive value."))

        if hasattr(self, "depthEdit"):
            new_attr["profondeur"] = (
                float(self.depthEdit.text()) if self.depthEdit.text() else 0.0
            )
            if new_attr["profondeur"] < 0:
                raise ValueError(self.tr("depth must be a positive value."))

        self.__attr.update(new_attr)

        for att in list(self.__attr.keys()):
            if (
                att in ["debut", "duree", "wkt", "nom"]
                and self.__attr[att] is not None
                and len(self.__attr[att])
                and self.__attr[att][0] != "'"
            ):
                self.__attr[att] = "'%s'" % (self.__attr[att])
            elif self.__attr[att] is None:
                self.__attr[att] = "NULL"

        return self.__attr
