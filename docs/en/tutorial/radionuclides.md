# Radionuclides

THYRSIS takes into account radioactive decay and specific activity of radionuclides. Radioactive decay is used in radionuclide transport simulations and specific activity is used for concentration to activity conversion.

These values are read from the **elements_chimiques** table, which can be filled by using the following command :

```bash
python -m thyrsis.database.load_chemicals radionuclides.csv
```

The `radionuclides.csv` file is provided in the `thyrsis/tutorial_data` directory. It is a csv file with `;` separator and the following **sorted** columns:

- `name`
- `period`
- `atomic_mass`
- `radioactive_decay`
- `specific_activity`
- `water_quality_limit`
- `atomic_number`
- `masse_number`
