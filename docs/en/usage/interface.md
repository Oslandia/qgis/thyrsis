---
substitutions:
  img_backward: |-
    :::{image} /_static/images/media-seek-backward.png
    :scale: 20 %
    :::
  img_draw: |-
    :::{image} /_static/images/draw.png
    :scale: 10 %
    :::
  img_first: |-
    :::{image} /_static/images/media-skip-backward.png
    :scale: 20 %
    :::
  img_forage: |-
    :::{image} /_static/images/forage.png
    :scale: 10 %
    :::
  img_forward: |-
    :::{image} /_static/images/media-seek-forward.png
    :scale: 20 %
    :::
  img_inverse_plot: |-
    :::{image} /_static/images/inverse_plot.png
    :scale: 10 %
    :::
  img_inversion: |-
    :::{image} /_static/images/inverse.png
    :scale: 10 %
    :::
  img_last: |-
    :::{image} /_static/images/media-skip-forward.png
    :scale: 20 %
    :::
  img_list-add: |-
    :::{image} /_static/images/list-add.png
    :scale: 50 %
    :::
  img_list-remove: |-
    :::{image} /_static/images/list-remove.png
    :scale: 50 %
    :::
  img_maillage: |-
    :::{image} /_static/images/mesh.png
    :scale: 10 %
    :::
  img_pause: |-
    :::{image} /_static/images/media-playback-pause.png
    :scale: 20 %
    :::
  img_pick: |-
    :::{image} /_static/images/pick.png
    :scale: 10 %
    :::
  img_play: |-
    :::{image} /_static/images/media-playback-start.png
    :scale: 20 %
    :::
  img_plot: |-
    :::{image} /_static/images/plot.png
    :scale: 10 %
    :::
  img_plot_bilan: |-
    :::{image} /_static/images/plot_bilan.png
    :scale: 10 %
    :::
  img_run_simulation: |-
    :::{image} /_static/images/config_simulation.png
    :scale: 10 %
    :::
  img_view_refresh: |-
    :::{image} /_static/images/view-refresh.png
    :scale: 50 %
    :::
---

# Interface

(preferences)=
## Preferences

The Preferences menu allows to define the general configuration which is saved :

- in the `.thyrsis.ini` file of the project folder where the computation database is saved,
- in the `thyrsis.ini` file of the `$HOME/.thyrsis` folder.

At the first use of the plugin, the `$HOME/.thyrsis` folder is created and the `thyrsis.ini` file is set with default values. The Preferences dialog is opened and the user is asked to set his configuration (cf. infra).

When a project is re-opened, the configuration loaded is the `.thyrsis.ini` file of the database folder. Therefore it is possible to define a configuration for each project.

### Installation

:::{figure} /_static/images/Preferences_installation.png
:name: Preferences_installation
Preferences menu, installation window
:::

Installation preferences define the location of files and folders needed by the plugin :

- GMSH executable path

- OpenFoam project and PMF installation directory

> These variable are defined with default values. Please check values before THYRSIS use.

- the folder where the background layer maps are saved (.vrt files),

- the MPI configuration file, which includes computational units to use, and the number of processes available for each unit, written like :

```
tata:8
titi:8
toto:4
```

Futhermore, it is also possible to define :

- the number of processes to use for parallelized computation,
- the maximum number of vertical 1D columns to display.
- the mesh generator use (GMSH or CFMESH)
:::{important}
For CFMESH use on Windows, minimum version of OpenFoam is v2112. There is an issue with cfMesh use on previous pre-compiled version.
:::
- the hydrogeologic software used (OPENFOAM or METIS)

Finally, two checkboxes are present in order to define :

- if the logs have to be displayed in the terminal,
- if detailed logs are wanted (verbose mode).

### Variables

:::{figure} /_static/images/Preferences_variables.png
:name: Preferences_variables
Preferences menu, Variables window
:::

The variables preferences provide the opportunity to define which variables should be saved :

- potential,
- Darcy's velocity,
- concentration,
- activity,
- saturation,

and the display configuration, by defining:

- the unit,
- the scale,
- the number of classes.

The scale defines the ratio between the minimal value to display and the maximal value obtained from simulation. By default the scale is set to 0.001, but a lower value can be chosen to get a better view of the plume extent.

The number of classes is used to display the plume with a graduated scale.

This preferences window enables also to define the mass unit for the mass balance and to check a box avoiding to display units, for communication purposes.

### Animation

:::{figure} /_static/images/Preferences_animation.png
:name: Preferences_animation
:scale: 50 %

Preferences menu, Animation window
:::

The refresh rate used for video export can be configure in this window.

### Matplotlib

:::{figure} /_static/images/Preferences_matplotlib.png
:name: Preferences_matplotlib
:scale: 50 %

Preferences menu, Matplotlib window
:::

This window allows the user to configure some matplotlib parameters such as :

- tick label size,
- y label font size,
- legend font size.

### Database

:::{figure} /_static/images/Preference_database.png
:name: Preferences_database
:scale: 50 %

Preference menu, Database window
:::

This window allows the user to connect to an experimental database by defining a connection service.

The push button *Synchronize experimental database with site database* enables the loading of boreholes, points of interest, hydraulic heads and volumic concentrations from the experimental base to the **sites.sqlite** database.

----

## Main menu

:::{figure} /_static/images/Thyrsis_menu_base.png
:name: Thyrsis_menu_base
:scale: 300 %

THYRSIS menu before opening any THYRSIS project.
:::

When QGIS is launched, if no THYRSIS project is opened, THYRSIS menu will only show the simplified form of {numref}`Thyrsis_menu_base`, with the following actions :

- opening a new THYRSIS project,
- opening an existing THYRSIS computation database,
- opening the {ref}`Preferences<preferences>` menu,
- opening the online help, using the default web browser.

### New

The creation of a new project can be made through the menu, by selecting an existing site or by creating a mesh for a new model ({numref}`Thyrsis_Nouveau`) :

:::{figure} /_static/images/Thyrsis_Nouveau.png
:name: Thyrsis_Nouveau
New menu allowing to select a site or build a new mesh
:::

- When selecting an existing site, a new drop-down menu is opened, which shows the available models and the available saved studies (defined by the **simulations** table in the **sites.sqlite** database) ({numref}`Thyrsis_Nouveau_SACLAY`).
- When selecting the entry *Mesh...*, a mesh project is created and a new model can be created (see {ref}`Model building<model_building>`).

:::{figure} /_static/images/Thyrsis_Nouveau_SACLAY.png
:name: Thyrsis_Nouveau_SACLAY
Open menu for selecting a study or a model
:::

### Open

The "Open" item in the THYRSIS menu enables to open an existing spatialite computation database **calcul.sqlite** by selecting it in a file dialog.
If a `calcul.qgs` project exists in the same folder as **calcul.sqlite**, an alert dialog is opened, asking the user if the project has to be overwrite :

- if the answer is "Yes", a new `calcul.qgs` project file is built using the **calcul.sqlite** database and the existing project is overwritten;
- if the answer is "No", no action is done.

To open an existing `calcul.qgs` file - including the **calcul.sqlite** file - the *Project>Open* menu of QGIS should be used.

When a THYRSIS project is opened, several actions are added to the THYRSIS menu as shown in {numref}`Thyrsis_menu_detail`. These actions are detailed below.

:::{figure} /_static/images/Thyrsis_menu_detail.png
:name: Thyrsis_menu_detail
:scale: 300 %

THYRSIS menu after a project opening
:::

### Simulate

The *Simulate* action from the Thyrsis menu is used to ({numref}`Thyrsis_menu_simuler`) :

- initialize a computation,
- launch a computation after initialization,
- initialize and launch a computation. This action can be done directly by clicking on the {{ img_run_simulation }} button in the {ref}`Menu bar<menu_bar>` or the {ref}`THYRSIS panel<thyrsis_panel>`.

:::{figure} /_static/images/Thyrsis_menu_simuler.png
:name: Thyrsis_menu_simuler
:scale: 300 %

Menu *Thyrsis>Simulate*
:::

After computation, toolbars are updated (dates, variables, units), the "results" layer is added to the QGIS canvas and set on the last simulation date, and the vertical columns of the unsaturated zone are displayed in the ZNS window.

#### Computation management

A simulation creates a new subfolder *calcul_tmp* in the computation database *calcul.sqlite* folder. This folder contains all input/output files of the hydrogeological computations. These files are managed in subfolders as follows :

- *samplexxxxx* folders, where xxxxx is the id number of the parameters set (cf. table **parametres_simulation**),

- in each of these folders :

  > - *insatyyyyy* folders, where yyyyyy is the injection area number. These folders contains inpout/output files of each computation in unsaturated area,
  > - a *sature* folder, which contains input/output files for computation in saturated zone.

Based on these results, THYRSIS generates numpy files in the computation database folder and these numpy files are then displayed in the interface. These files are :

- calcul.variable.type.npy : values of the variable "variable" for each requested date (saturated zone), at each node if type=node or at each center of mesh if type=elem,
- calcul.variable.insat.npy : values of the variable "variable" at each node of the ZNS column, for each requested date (unsaturated zone).

The variable is one of the following (according to the parameters chosen in {ref}`Preferences<preferences>`) :

- in saturated zone :

  > - concentration,
  > - potential,
  > - Darcy's velocity,
  > - flow rate

- in unsaturated zone :

  > - saturation,
  > - Darcy's velocity,
  > - concentration,
  > - activity

The *calcul_tmp* folder can be deleted without consequence for Qgis interface use, exception made for probability map computation, which needs to access to the whole results for each parameters set.
This folder is overwritten at each "Simulate" action.

#### External files

All input files needed by the hydrogeological software are automatically generated. But some particular cases need to use specific files, called “ user files ”. These files have a "u\_" prefix and are placed at the same level that the computation database. They are described in the {ref}`User files<user_files>` appendix.

### Create

:::{figure} /_static/images/Thyrsis_menu_creer.png
:name: Thyrsis_menu_creer
:scale: 300 %

Menu *Thyrsis>Create*
:::

The menu *Thyrsis>Create* provides the following actions ({numref}`Thyrsis_menu_creer`) :

- **Injection zones**

> This action removes the existing injections and automatically creates new injection zones covering the whole surface of the area study. THYRSIS groups ZNS columns by thickness and permeability, according to the pas_echant_zns and rapport_max_permeability values, defined in the {ref}`simulations<simulation_table>` table ({ref}`Diffuse injection on the whole domain <full_domain_injection>`). Cf. example {numref}`Thyrsis_zones_injection`.

:::{figure} /_static/images/Thyrsis_zones_injection.png
:name: Thyrsis_zones_injection
Example of a study area covered by injection zones.
:::

- **Latin Hypercube**

  > This action allows to create several parameter sets with the {ref}`Latin Hypercube <latin_hypercube>` method. Requested informations are ({numref}`Thyrsis_Hypercube_Latin`) :
  >
  > - the number of simulations - defining the number of parameter sets to create,
  >
  > and for each parameter (from the "parametres" table) :
  >
  > - the min value,
  > - the max value,
  > - the probability law to use, chosen betwen "constant", "uniform", "loguniform", "normal", "lognormal".
  >
  > Parameter sets are created in the **parametres_simulation** table.
  >
  > The *simulate* action will generate as many simulations as there are parameter sets. These simulations are parallelized on the available processors.
  >
  > After computation, THYRSIS creates two result files with the mean and standard deviation for each node or element and each date. Probability maps are available through the menu *Thyrsis>Create>Probability map*

:::{figure} /_static/images/Thyrsis_Hypercube_Latin.png
:name: Thyrsis_Hypercube_Latin
Menu *Thyrsis>Create>Latin Hypercube*
:::

- **Probability map**

  > *Probability map* action creates a probability layer based on several simulations. Threshold and unit should be specified in a pop-up window ({numref}`Thyrsis_seuil_probabilite`).
  > The result is added as a new result layer, as concentration or potential. In consequence the probability map is available through the combobox of the THYRSIS menu ({ref}`Menu bar<menu_bar>`).

:::{figure} /_static/images/Thyrsis_seuil_probabilite.png
:name: Thyrsis_seuil_probabilite
Menu *Thyrsis>Create>Probability map*
:::

- **Isovalues**

  > *Isovalues* action generates isovalues polylines on the active layer which can be the result layer or any of the raster layers describing the model properties (cf. {ref}`2D Panel<2D_panel>`) as long as the layer is connected to a legend.
  > Isovalues definition is specified in a pop-up dialog ({numref}`Thyrsis_Isovaleurs`). By default, the values are extracted from the legend. These values can be manually modified/removed/added. Values can also be generated automatically, by providing the unit, the number of isovalues, the min and the max values, and the scale type (linear, log). The creation of values is triggered by the following button {{ img_view_refresh }}.

:::{figure} /_static/images/Thyrsis_Isovaleurs.png
:name: Thyrsis_Isovaleurs
:scale: 50 %

Menu *Thyrsis>Isovalues*
:::

- **Isovalues (gdal)**

  > *Isovalues (gdal)* action generates isovalues polylines on the active layer, as the previous action, but this action uses the gdal_contour tools which needs to create an intermediate raster. Resolution can be provided in pixel/meter (default value : 0.1) ({numref}`Thyrsis_Isovaleurs_gdal`). This method avoids some problems encountered with the previous action.

:::{figure} /_static/images/Thyrsis_Isovaleurs_gdal.png
:name: Thyrsis_Isovaleurs_gdal
:scale: 50 %

Menu *Thyrsis>Isovalues (gdal)*
:::

- **Evolution graph**

  > This action opens the *1D panel* which displays evolution graphs of the selected variable for each selected point. This action can also be triggered using the {{ img_plot }} button of the {ref}`Menu bar<menu_bar>`.

- **Second medium**

  > This action duplicates the current computation base in a new one, adapted to the {ref}`Dual porosity model<dual-porosity>` :
  >
  > - the tables **noeuds_second_milieu**, **mailles_second_milieu**, **simulations_second_milieu** and **injections_second_milieu** are copied from the tables **noeuds**, **mailles**, **simulations** et **injections**;
  > - ASHAPE, BSHAPE, CSHAPE and PL entries are added to the parameters table
  > - the **parametres_simulation** table is reset.
  >
  > To exploit this new dual-porosity computation base, it is necessary to open a new projet (*Thyrsis>Open*) based on this database.

- **Chemical element**

  > This action allows to create or remove chemical elements.

(thyrsis-chemical-element)=

:::{figure} /_static/images/Thyrsis_chemical_element.png
:name: Thyrsis_chemical_element
:scale: 50 %

Chemical element modification dialog
:::

(export)=

### Export

(thyrsis-menu-exporter)=

:::{figure} /_static/images/Thyrsis_menu_exporter.png
:name: Thyrsis_menu_exporter
:scale: 200%

Menu *Thyrsis>Export*
:::

The menu *Thyrsis>Export* provides the following actions :

- **Raster**

  > The *Raster* action exports a PNG or TIF image of the current 2D layer, with the current date and the legend.

- **Animation**

  > The *Animation* action generates several PNG images on a simulation period, including the 2D display (Qgis canvas) but also the ZNS and the 1D panel. The record starts by clicking on the {{ img_play }} button of the {ref}`Menu bar<menu_bar>`, and stops by clicking on the same button, became {{ img_pause }}.

- **Video**

  > The *Video* action generates an AVI video including the 2D display (Qgis canvas) but also the ZNS and the 1D panel. The video is created by concatenating the PNG images. The record starts at the current date and stops by clicking on the {{ img_pause }} button.

- **Template**

  > The *Template* action saves the current study in the **sites.sqlite** database, adding an entry to the following table :
  >
  > > - **simulations**,
  > > - **dates_resultats**,
  > > - **parametres**,
  > > - **metis_options**,
  > > - **injections**.
  >
  > If a study with the same name already exists, the old one is overwritten.

(menu_bar)=
## Menu bar

:::{figure} /_static/images/Thyrsis_barre_menu.png
:name: Thyrsis_barre_menu
Thyrsis menu bar
:::

Menu bar provides the main actions of THYRSIS :

- {{ img_forage }} opens a panel to display the lithology of the boreholes contained in the database,
- {{ img_inverse_plot }} displays the results of the inversion process, like the iteration dependent mean error value and the correlation between measured and calculated piezometric values,
- {{ img_inversion }} opens the inversion process dialog, where user can choose between pilot points or permeability field method,
- {{ img_run_simulation }} launches the simulation (cf. {ref}`THYRSIS panel<thyrsis_panel>` to define injection parameters),
- the first combobox contains the simulation dates. Selecting a date displays the result layer at the selected date,
- the cursor offers a faster way to access to a specific date,
- {{ img_play }} starts an animation, displaying result at each date,
- {{ img_first }} displays the first simulation date results,
- {{ img_backward }} displays the previous simulation date results,
- {{ img_forward }} displays the next simulation date results,
- {{ img_last }} displays the last simulation date results,
- the checkbox "Adjust scale" adjusts the color scale for each date displayed. If uncheck, the color scale is calculated on the whole simulation,
- the two next comboboxes are used to select the variable to display and its unit,
- {{ img_pick }} allows to request the value of the result layer on the selected location in the QGIS canvas,
- {{ img_plot }} opens a matplotlib panel displaying evolution graphs at interest points,
- {{ img_plot_bilan }} opens a matplotlib panel displaying the mass balance, which shows the time dependent values of the injected masses, distributed between the unsaturated and saturated zones, and leaving the domain.

## Dialogs and panels

(2D_panel)=
### 2D Panel

:::{figure} /_static/images/Thyrsis_2D.png
:name: Thyrsis_2D
Example of 2D panel, with result layer displayed after a simulation
:::

The 2D panel is the main QGIS interface, more precisely the map canvas. It is connected with the "Layers" panel and displays on various background maps the specific parameters of the hydrogeological model :

- mesh nodes,
- mesh,
- points of interest,
- boreholes,
- contours,
- potential (piezometric level),
- permeability,
- topographic elevation,
- aquifer substratum altitude,
- Darcy's velocity,
- unsatured zone thickness,
- groundwater thickness,
- reference potential, which is obtained by interpolation of the piezometric data, and could have been used to build the hydrogeological model, through inversion computation.

Injection zones, defined in the {ref}`THYRSIS panel<thyrsis_panel>`, are displayed in the 2D panel.

At the end of a computation, a result layer is also displayed in the 2D panel. The dates and the variable to display are controled by the {ref}`Menu bar<menu_bar>`.

(thyrsis_panel)=

### THYRSIS panel

:::{figure} /_static/images/Thyrsis_Injection_defaut.png
:name: Thyrsis_Injection_defaut
:scale: 50 %

Example of a Thyrsis panel, used to define injection zones
:::

The THYRSIS panel is used to define injection zones.

The first values to define are :

- the simulation name,
- the chemical to use, selected in the drop-down menu,
- the partition coefficient and the solubility limit in case of mass injection,
- the starting date of the simulation, with format DD/MM/YYYY \[hh:mm:ss\],
- a set of tuples (duration, step), that define the simulation output dates.

Then the type of injection can be chosen by clicking on :

- flux,
- concentration,
- mass.

:::{figure} /_static/images/Thyrsis_Flux.png
:name: Thyrsis_Flux
:scale: 50 %

Example of THYRSIS panel to define a flux injection zone
:::

According to the injection type selected, relevant fields may differ, as mention in the {ref}`injection<injection_table>` part. A non-null water volume is a leak that induces a transient flow simulation.

It is possible to add as many injections as wanted by clicking many times on the selected button, but it is impossible to mix several injection types in the same simulation.

The center coordinates of the injection zone can be defined manually or by clicking on ${\odot}$ and then clicking on the wanted location in the map canvas.

Similary the surface can be defines manually in the Area field, or automatically calculated by clicking on {{ img_draw }} and then drawing the shape on the map canvas.

Below the THYRSIS panel, three buttons allows to cancel or save the modifications, and also to launch computation :

- "Cancel" will cancel current modifications and reload the saved informations that are stored in the database.
- "Save" will save the modifications (data and geometry) in the computation database.
- {{ img_run_simulation }} will launch the simulation.

:::{note}
If you made modifications, it is important to save it by clicking on "Save" before launching the computation by clicking on {{ img_run_simulation }} .
:::

### Borehole

:::{figure} /_static/images/Thyrsis_Forage.png
:name: Thyrsis_Forage
:scale: 50 %

Example of stratigraphic log diplayed in the borehole panel
:::

When stratigraphic and lithologic informations are provided in the sites.sqlite database, the stratigraphic log can be displayed in the borehole panel. Click on {{ img_forage }} in the bar menu and select the borehole directly in the map canvas.
If the fracturated rate is known, it is displayed in the stratigraphic log.

This graphic representation is based on differents codifications presented in the {ref}`Stratigraphic logs <stratigraphic_logs>` annexe :

- codification on the geological time scale (BRGM),
- RGB colorimetric codification of the Commission for the geological map of the world (CGMW),
- the lithology pattern chart from USGS.

### 3D panel

:::{figure} /_static/images/Thyrsis_3D.png
:name: Thyrsis_3D
Exemple of 3D panel
:::

The 3D panel is used to display a three dimensional representation of the following three levels :

- the aquifer substratum,
- the piezometric level,
- the topographic altitude.

If results from a simulation are available, they are displayed on the potential layer, with the same symbology used in the map canvas.

This panel is controled through the options of the "3D parameters panel" :

- the cursor is used to ajust vertical scale,
- checkbox can be activated to display :
    - texture,
    - contours,
    - boreholes,
    - computation points.

Colors of the topographic altitude and contours can be modified.

### Unsaturated zone (ZNS)

:::{figure} /_static/images/Thyrsis_ZNS.png
:name: Thyrsis_ZNS
Example of results displayed in the panel dedicated to the unsaturated zone
:::

The ZNS panel displays in the unsatured zone columns the temporal evolution of the simulated variables.

These variables and units must be selected in the combobox of the bar menu.

Some options are provided to define the legend :

- Automatic scale update of the legend : the legend scale is updated for each date. If desactivated, the legend is calculated on the whole timeseries.
- Legend : this action opens a dialog to custom the legend,
- Legend position : used to choose the legend position (left, right or bottom).

### 1D panel

The temporal evolution curves are obtained by clicking on the {{ img_plot }} button or the {{ img_plot_bilan }} button in the {ref}`Menu bar<menu_bar>`, and are displayed in a new panel :

- {{ img_plot_bilan }} displays the temporal evolution of mass balance (if existing).
- {{ img_plot }} displays the temporal evolution of the selected variable in the combobox of the {ref}`Menu bar<menu_bar>`, for each selected points in the QGIS map canvas.

:::{figure} /_static/images/Thyrsis_1D_bilan.png
:name: Thyrsis_1D_bilan
Example of 1D graphic with temporal evolution of mass balance
:::

:::{figure} /_static/images/Thyrsis_1D_potentiel.png
:name: Thyrsis_1D_potentiel
Example of 1D graphic with temporal evolution of simulated and measured potential
:::
