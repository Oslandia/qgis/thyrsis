"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterCrs,
    QgsProcessingParameterEnum,
    QgsProcessingParameterFile,
)
from qgis.PyQt.QtCore import QCoreApplication

from ..database import sqlite
from ..database.load_forages import load_forages
from ..log import logger
from ..settings import Settings


class LoadForagesAlgorithm(QgsProcessingAlgorithm):
    INPUT_FORAGE_CSV = "INPUT_FORAGE_CSV"
    INPUT_STRATIGRAPHIE_CSV = "INPUT_STRATIGRAPHIE_CSV"
    INPUT_FRACTURATION_CSV = "INPUT_FRACTURATION_CSV"
    INPUT_SITESDB = "INPUT_SITESDB"
    INPUT_SITENAME = "INPUT_SITENAME"
    INPUT_CRS = "INPUT_CRS"
    sites_list = []

    def tr(self, string):
        return QCoreApplication.translate(
            "Import borehole data into site database from .csv",
            string,
        )

    def createInstance(self):
        return LoadForagesAlgorithm()

    def name(self):
        return "load_forages"

    def displayName(self):
        return self.tr("Load boreholes")

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def shortHelpString(self):
        return self.tr("Import borehole data into site database from .csv")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_FORAGE_CSV,
                self.tr("Enter borehole .csv file to import"),
                QgsProcessingParameterFile.File,
                "csv",
            )
        )
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_STRATIGRAPHIE_CSV,
                self.tr("Enter stratigraphy .csv file to import"),
                QgsProcessingParameterFile.File,
                "csv",
            )
        )
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_FRACTURATION_CSV,
                self.tr("Enter fracturation .csv file to import"),
                QgsProcessingParameterFile.File,
                "csv",
                optional=True,
            )
        )

        sites_db = "sites.sqlite"
        if Settings().contains("General", "defaultSite"):
            sites_db = str(Settings().value("General", "defaultSite"))

        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_SITESDB,
                self.tr("Enter sites.sqlite database location"),
                QgsProcessingParameterFile.File,
                "sqlite",
                defaultValue=sites_db,
            )
        )

        self.sites_list = sorted(
            [
                x[0]
                for x in sqlite.connect(sites_db)
                .execute("select nom from sites")
                .fetchall()
            ]
        )
        self.addParameter(
            QgsProcessingParameterEnum(
                self.INPUT_SITENAME,
                self.tr("Select site name"),
                self.sites_list,
            )
        )

        self.addParameter(QgsProcessingParameterCrs(self.INPUT_CRS, self.tr("CRS")))

    def processAlgorithm(self, parameters, context, feedback):
        input_forage_csv_file = self.parameterAsFile(
            parameters, self.INPUT_FORAGE_CSV, context
        )
        input_stratigrahie_csv_file = self.parameterAsFile(
            parameters, self.INPUT_STRATIGRAPHIE_CSV, context
        )
        input_fracturation_csv_file = self.parameterAsFile(
            parameters, self.INPUT_FRACTURATION_CSV, context
        )
        input_sitesdb = self.parameterAsFile(parameters, self.INPUT_SITESDB, context)
        crs = self.parameterAsCrs(parameters, self.INPUT_CRS, context)

        input_sitenumber = self.parameterAsEnum(
            parameters, self.INPUT_SITENAME, context
        )
        input_sitename = self.sites_list[input_sitenumber]

        logger.enable_console(True)
        logger.set_processing_feedback(feedback)

        conn = sqlite.connect(input_sitesdb)

        load_forages(
            cur=conn.cursor(),
            site=input_sitename,
            srid=crs.authid().replace("EPSG:", ""),
            forage=input_forage_csv_file,
            stratigraphie=input_stratigrahie_csv_file,
            fracturation=input_fracturation_csv_file,
            delete_confirm=False,
        )
        conn.commit()

        logger.disable_processing_feedback()

        return {}
