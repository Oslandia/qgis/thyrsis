"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterBoolean,
    QgsProcessingParameterEnum,
    QgsProcessingParameterFile,
    QgsProcessingParameterString,
)
from qgis.PyQt.QtCore import QCoreApplication

from thyrsis.exception import MeasureLoadError

from ..database import sqlite
from ..database.load_measure import load_measure
from ..log import logger
from ..settings import Settings


class LoadMeasureAlgorithm(QgsProcessingAlgorithm):
    INPUT_DAT = "INPUT_DAT"
    INPUT_SITESDB = "INPUT_SITESDB"
    INPUT_SITENAME = "INPUT_SITENAME"
    CHEMICAL_NAME = "CHEMICAL_NAME"
    UNIT = "UNIT"
    DELETE_EXISTING_MEASURE = "DELETE_EXISTING_MEASURE"

    sites_list = []

    def tr(self, string):
        return QCoreApplication.translate(
            "Load measures in site database from a .dat file", string
        )

    def createInstance(self):
        return LoadMeasureAlgorithm()

    def name(self):
        return "load_measure"

    def displayName(self):
        return self.tr("Load measure")

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def shortHelpString(self):
        return self.tr("Load measures in site database from a .dat file")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_DAT,
                self.tr("Enter .dat file to import"),
                QgsProcessingParameterFile.File,
            )
        )

        sites_db = "sites.sqlite"
        if Settings().contains("General", "defaultSite"):
            sites_db = str(Settings().value("General", "defaultSite"))

        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_SITESDB,
                self.tr("Enter sites.sqlite database location"),
                QgsProcessingParameterFile.File,
                "sqlite",
                defaultValue=sites_db,
            )
        )

        self.sites_list = sorted(
            [
                x[0]
                for x in sqlite.connect(sites_db)
                .execute("select nom from sites")
                .fetchall()
            ]
        )

        self.addParameter(
            QgsProcessingParameterEnum(
                self.INPUT_SITENAME,
                self.tr("Select site name"),
                self.sites_list,
            )
        )

        self.addParameter(
            QgsProcessingParameterString(
                self.CHEMICAL_NAME,
                self.tr("Chemical name"),
                optional=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterString(
                self.UNIT,
                self.tr("Unit"),
                optional=True,
            )
        )

        self.addParameter(
            QgsProcessingParameterBoolean(
                self.DELETE_EXISTING_MEASURE,
                self.tr("Delete existing measure"),
                optional=True,
                defaultValue=False,
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        input_dat_file = self.parameterAsFile(parameters, self.INPUT_DAT, context)
        input_site_file = self.parameterAsFile(parameters, self.INPUT_SITESDB, context)

        input_sitenumber = self.parameterAsEnum(
            parameters, self.INPUT_SITENAME, context
        )
        input_sitename = self.sites_list[input_sitenumber]

        chemical = self.parameterAsString(parameters, self.CHEMICAL_NAME, context)
        unit = self.parameterAsString(parameters, self.UNIT, context)
        delete_measure = self.parameterAsBoolean(
            parameters, self.DELETE_EXISTING_MEASURE, context
        )

        logger.enable_console(True)
        logger.set_processing_feedback(feedback)

        conn = sqlite.connect(input_site_file)

        try:
            load_measure(
                cur=conn.cursor(),
                siteName=input_sitename,
                fileName=input_dat_file,
                chemicalName=chemical if chemical else None,
                units=unit if unit else None,
                delete=delete_measure,
            )
            conn.commit()
        except MeasureLoadError as exc:
            raise QgsProcessingException(str(exc))

        logger.disable_processing_feedback()

        return {}
