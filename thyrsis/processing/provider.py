from qgis.core import QgsProcessingProvider
from qgis.PyQt.QtGui import QIcon

from thyrsis.__about__ import __icon_path__, __title__

from .load_chemicals import LoadChemicalsAlgorithm
from .load_measure import LoadMeasureAlgorithm
from .load_forages import LoadForagesAlgorithm
from .load_points import LoadPointsAlgorithm


class Provider(QgsProcessingProvider):
    def loadAlgorithms(self, *args, **kwargs):
        self.addAlgorithm(LoadChemicalsAlgorithm())
        self.addAlgorithm(LoadPointsAlgorithm())
        self.addAlgorithm(LoadMeasureAlgorithm())
        self.addAlgorithm(LoadForagesAlgorithm())

    def id(self, *args, **kwargs):
        """The ID of your plugin, used for identifying the provider.

        This string should be a unique, short, character only string,
        eg "qgis" or "gdal". This string should not be localised.
        """
        return "thyrsis"

    def name(self, *args, **kwargs):
        """The human friendly name of your plugin in Processing.

        This string should be as short as possible (e.g. "Lastools", not
        "Lastools version 1.0.1 64-bit") and localised.
        """
        return __title__

    def icon(self):
        """Should return a QIcon which is used for your provider inside
        the Processing toolbox.
        """
        return QIcon(str(__icon_path__))
