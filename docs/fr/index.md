# Documentation en français

Bienvenue dans la documentation de THYRSIS !

La documentation est divisée en grands chapitres :

- le premier chapitre propose une **Description générale** du plugin THYRSIS,
- le chapitre **Installation** explique comment installer THYRSIS,
- le chapitre **Tutoriel** décrit comment construire un nouveau modèle hydrogéologique et comment utiliser les fonctionnalités de base de THYRSIS,
- le **Manuel utilisateur** présente THYRSIS dans le détail, à la fois son interface graphique et les bases théoriques

```{toctree}
---
caption: User guide
maxdepth: 2
---
introduction
installation
tutorial
manual
appendices/index
```
