"""
load points for a specified site in the sites.sqlite database

USAGE

    python -m thyrsis.database.load_points site points.csv

    :param: site : site name str
    :param: points.csv : File to import

    The points.csv can have separators blanks or "," or ";"
    The first line is not imported but is read to determine the separators
    The (ordered) columns of points.csv must be :
    1- x coordinate
    2- y coordinate
    3- name
    4- SRID (optional, "4326" by default)
    5- group (optional, "calcul" by default)

"""
from builtins import str

from ..log import logger


def load_points(cur, siteName, fileName):
    """load interest points into the current database for a site

    :param cur: cursor on a sqlite connection
    :type cur: sqlite3.Cursor
    :param siteName: name of the site
    :type siteName: string
    :param fileName: path to a csv file of interest points
    :type fileName: string

    :return: None
    """
    logger.notice("load_points")
    logger.notice("siteName is", siteName)

    sid = cur.execute("SELECT id FROM sites WHERE nom='%s'" % (siteName)).fetchone()
    if not sid:
        logger.error("site", siteName, " not found in sites.sqlite database")
        for (nom,) in cur.execute("SELECT nom FROM sites").fetchall():
            logger.notice("    ", nom)
        exit(1)
    sid = sid[0]

    points = []
    sep = None
    with open(fileName, "r") as file_points:
        line = next(file_points)
        if ";" in line:
            sep = ";"
        elif "," in line:
            sep = ","
        for line in file_points:
            spl = line.replace("\n", "").split(sep)
            points.append(
                {
                    "x": float(spl[0]),
                    "y": float(spl[1]),
                    "nom": spl[2],
                    "srid": "4326" if len(spl) < 4 else spl[3],
                    "groupe": "calcul" if len(spl) < 5 else spl[4],
                }
            )

    logger.notice(len(points), " points read")
    for pca in points:
        if not cur.execute(
            "SELECT COUNT(1) FROM points_interet \
                            WHERE nom='%s' AND sid=%d"
            % (pca["nom"], sid)
        ).fetchone()[0]:
            logger.notice("Adding point " + pca["nom"])
            cur.execute(
                (
                    "\
                INSERT INTO points_interet \
                (GEOMETRY, nom, groupe, sid) \
                VALUES \
                (ST_TRANSFORM(MakePoint(%(x)f, %(y)f, %(srid)s), 4326)"
                    + ", '%(nom)s', '%(groupe)s', '"
                    + str(sid)
                    + "')"
                )
                % pca
            )
        else:
            # check that the point is at the same place
            dist = cur.execute(
                (
                    "SELECT Distance(GEOMETRY, ST_TRANSFORM(MakePoint(%(x)f, %(y)f, %(srid)s"
                    + "), 4326)) \
                    FROM points_interet WHERE nom='%(nom)s' AND sid="
                    + str(sid)
                )
                % pca
            ).fetchone()[0]
            if dist > 1:
                ref, pt = cur.execute(
                    (
                        "\
                    SELECT AsText(GEOMETRY), \
                        AsText(ST_TRANSFORM(MakePoint(%(x)f, %(y)f, %(srid)s"
                        + "), 4326))\
                    FROM points_interet WHERE nom='%(nom)s' AND sid="
                        + str(sid)
                    )
                    % pca
                ).fetchone()
                raise Exception(
                    "Point %s %s with the same name exists at position %s (distance %f > 1)"
                    % (pca["nom"], pt, ref, dist)
                )
            else:
                logger.warning("Point " + pca["nom"] + " is already in the database")


if __name__ == "__main__":
    import sys

    from ..database import sqlite
    from ..settings import Settings

    logger.enable_console(True)

    if len(sys.argv) == 2 and sys.argv[1] in ["-h", "--help"]:
        help(sys.modules[__name__])
        exit(0)

    if len(sys.argv) != 3:
        logger.error(
            "wrong number of arguments (try python -m thyrsis.database.load_points --help)"
        )
        exit(1)

    if not Settings().contains("General", "defaultSite"):
        logger.error("defaultSite undefined, use preferences_dialog.py")
        exit(1)

    sitesdb = str(Settings().value("General", "defaultSite"))

    conn = sqlite.connect(sitesdb)

    load_points(conn.cursor(), sys.argv[1], sys.argv[2])

    conn.commit()
