import os
from builtins import object, str

from osgeo.osr import SpatialReference
from qgis.core import (
    QgsCoordinateReferenceSystem,
    QgsDataSourceUri,
    QgsMapLayerType,
    QgsProject,
    QgsRasterLayer,
    QgsVectorLayer,
)
from qgis.gui import QgsMapCanvas
from qgis.PyQt.QtCore import QFileInfo, Qt

from thyrsis.exception import IsoValueCreationError, ProjectLoadError
from thyrsis.mesh_project import get_layer

from .database import (
    create_computation_database,
    export_simulation,
    sqlite,
    check_and_upgrade,
)
from .gui.create_latin_hypercube import HypercubeDialog
from .log import logger
from .meshlayer.meshlayer import MeshLayer
from .settings import Settings
from .simulation.compute import check_before_compute, compute_mpi
from .simulation.create_injection_zones import create_injection_zones
from .spatialitemeshdataprovider import (
    SpatialiteMeshConstDataProvider,
    SpatialiteMeshDataProvider,
)
from .utilities import complete_filename


class Project(object):
    """A thyrsis project is a .sqlite database along with a .qgs project file
    and optional simulation results"""

    def __init__(self, database, layers=None):
        """Constructor

        :param database: database path
        :type database: string
        :param layers: layers to display
        :type layers: list
        """
        check_and_upgrade(database)
        self.__databasePath = os.path.dirname(os.path.abspath(database))
        self.__database = database
        self.__conn = sqlite.connect(database, "Project:init")
        self.__cur = self.__conn.cursor()

        self.__layers = layers
        self.__settings = Settings(self.__databasePath)
        logger.set_level(
            "debug" if int(self.__settings.value("General", "debug")) else "notice"
        )

    def __del__(self):
        self.__cur = None
        self.__conn.close()

    def get_project_SRID(self, table="noeuds"):
        return str(
            self.__cur.execute(
                "SELECT srid FROM geometry_columns WHERE f_table_name = '{}'".format(
                    table
                )
            ).fetchone()[0]
        )

    def export_simulation(self):
        """Export simulation template"""
        site_db = self.__settings.value("General", "defaultSite")
        with sqlite.connect(site_db, "Project:export_simulation") as conn_site:
            export_simulation(self.__conn, conn_site)

    def __getattr__(self, name):
        if name == "database":
            return self.__database
        elif name == "layers":
            return self.__layers
        elif name == "nb_simulation":
            return self.__cur.execute(
                """
                SELECT nombre_de_simulations FROM simulations
                """
            ).fetchone()[0]
        elif name == "site":
            return self.__cur.execute("SELECT nom FROM sites").fetchone()[0]
        elif name == "has_results":
            if len(
                self.__cur.execute("pragma table_info('dates_simulation')").fetchall()
            ):
                return (
                    self.__cur.execute(
                        "SELECT COUNT(1) FROM dates_simulation"
                    ).fetchone()[0]
                    > 0
                )
            else:
                return False
        elif name == "qgs":
            return self.__database[:-7] + ".qgs"
        elif name == "result_layer":
            return get_layer("resultats", MeshLayer)
        elif name == "permeabilite_x_layer":
            return get_layer("permeabilite_x", MeshLayer)
        elif name == "crs":
            res = QgsProject.instance().crs()
            return res
        else:
            raise AttributeError

    def setSettings(self, settings):
        """Set Thyrsis settings

        :param settings: settings for Thyrsis use
        :type settings: Settings
        """
        self.__settings = settings

    def has_second_milieu(self):
        """Check if the current database is made for dual-porosity simulation

        :param cur: cursor on a sqlite connection
        :type cur: sqlite3.Cursor
        """
        return (
            self.__cur.execute("SELECT COUNT(1) FROM noeuds_second_milieu").fetchone()[
                0
            ]
            > 0
        )

    def simulate(self, init=True, run=True):
        """run the simulation

        :param init: init flag
        :type init: bool
        :param run: run flag
        :type run: bool
        """
        err = check_before_compute(self.__database)
        if err:
            raise RuntimeError(err)
        if self.result_layer:
            if "probability" in [
                i[1] for i in self.result_layer.dataProvider().availableColumns()
            ]:
                self.result_layer.dataProvider().removeColumn("probability")
            QgsProject.instance().removeMapLayer(self.result_layer.id())

        compute_mpi(self.__database, init, run)

        if run:
            self.add_results()

    def add_results(self):
        """Add results layer"""
        logger.debug("project:adding results")
        if self.result_layer:
            logger.debug("project:removing results layer")
            QgsProject.instance().removeMapLayer(self.result_layer.id())

        logger.debug("project:creating results layer")
        layer = MeshLayer(
            "dbname=" + self.__database + " crs=epsg:" + self.get_project_SRID(),
            "resultats",
            SpatialiteMeshDataProvider.PROVIDER_KEY,
        )
        logger.debug("project:adding results layer")
        QgsProject.instance().addMapLayer(layer)
        column = layer.dataProvider().resultColumn()
        logger.debug("resultColumn = ", column)
        layer.colorLegend().setTitle(column)

        column = column.replace("2", "")
        layer.colorLegend().setLogScale(
            column in ["concentration", "activity", "vitesse_darcy_norme"]
        )

        column = column.replace("vitesse_darcy_norme", "darcy")
        layer.dataProvider().setUnits(
            self.__settings.value("Variables", column + "Unit", "-")
        )

        column = column.replace("activity", "concentration")
        scale = float(self.__settings.value("Variables", column + "Scale", 0))
        maxValue = layer.dataProvider().maxValue()
        minValue = layer.dataProvider().minValue()
        layer.colorLegend().setMaxValue(maxValue)
        layer.colorLegend().setMinValue(maxValue * scale if scale else minValue)

        layer.colorLegend().setTransparencyPercent(40)
        self.__layers.append(layer)
        QgsProject.instance().write()

    def create_latin_hypercube(self):
        """Open Hypercube dialog"""
        HypercubeDialog(self.__conn).exec_()

    def create_injection_zones(self):
        """Create automatic injection zones"""
        create_injection_zones(self.__database)

    def add_isovalues(self, date_id, str_date, column, lines, values, str_values):
        """Create isovalue from the result value

        :param date_id: date id
        :type date_id: string
        :param str_date: real date
        :type str_date: string
        :param column: result column
        :type column: string
        :param lines: list of linestring
        :type lines: list
        :param values: threshold values
        :type values: list
        :param str_values: threshold values string
        :type str_values: list
        """
        self.__cur.execute(
            """
        DELETE FROM isovaleur WHERE did=%(did)d AND type='%(type)s'
        """
            % {"did": date_id, "type": column}
        )
        for i, mutilineline in enumerate(lines):
            self.__cur.execute(
                """
                INSERT INTO isovaleur(did, valeur, valeur_formate, type, GEOMETRY)\
                VALUES (%(did)d, %(valeur)g, '%(valeur_txt)s',
                        '%(type)s', GeomFromText('%(wkt)s', %(srid)s))"""
                % {
                    "did": date_id,
                    "valeur": values[i],
                    "valeur_txt": str_values[i],
                    "type": column,
                    "wkt": "MULTILINESTRING(%s)"
                    % (
                        ",".join(
                            [
                                "("
                                + ",".join(["%f %f" % point[0:2] for point in line])
                                + ")"
                                for line in mutilineline
                            ]
                        )
                    ),
                    "srid": self.get_project_SRID(),
                }
            )
        self.__conn.commit()

        uri = QgsDataSourceUri()
        uri.setDatabase(self.__database)
        uri.setDataSource("", "isovaleur", "GEOMETRY")
        uri.setSql("did=%d and type='%s'" % (date_id, column))
        isolines = QgsVectorLayer(uri.uri(), column + " " + str_date, "spatialite")
        if not isolines.isValid():
            raise IsoValueCreationError("Invalid layer after isoline creation")
        QgsProject.instance().addMapLayer(isolines)

    def add_qgis_layers(self):
        """Add layers in __layers parameter"""
        self.__layers = []
        for layer in list(QgsProject.instance().mapLayers().values()):
            if layer.type() == QgsMapLayerType.PluginLayer:
                self.__layers.append(layer)

    @staticmethod
    def load(database, settings, canvas=None):
        """load project

        :return: project
        :rtype: Project
        """
        if not database[-7:] == ".sqlite":
            raise ProjectLoadError("Database suffix must be .sqlite")
        if not os.path.exists(database):
            raise ProjectLoadError(f"File {database} is not available.")
        if os.path.exists(database[:-7] + ".qgs"):
            raise ProjectLoadError(f"File {database[:-7] + '.qgs'} must not exists.")
        layers = Project.create_qgis_project(database, settings, canvas)
        project = Project(database, layers)
        if project.has_results:
            project.add_results()
        return project

    @staticmethod
    def create(database, settings, mesh_db, simulation=None, canvas=None):
        """create a new project from a mesh, the name is given without extension
        nor path

        :param mesh_db: mesh database path
        :type mesh_db: string
        :param simulation: simulation name
        :type simulation: string
        :param exp_db: experimental db string definition
        :type exp_db: string
        :param canvas: canvas
        :type canvas: QgsMapCanvas
        :return: project
        :rtype: Project
        """
        exp_db = (
            settings.value("Database", "connectionString")
            if settings.value("Database", "useRemoteDb")
            else None
        )
        create_computation_database(database, mesh_db, simulation, exp_db)
        layers = Project.create_qgis_project(database, settings, canvas)
        return Project(database, layers)

    @staticmethod
    def create_qgis_project(database, settings, canvas=None):
        """returns a list of layers

        :param database: database path
        :type database: string
        :param canvas: canvas
        :type canvas: QgsMapCanvas
        """

        logger.debug("create_qgis_project", database[:-7] + ".qgs")
        QgsProject.instance().setFileName(database[:-7] + ".qgs")

        QgsProject.instance().removeAllMapLayers()

        layers = []

        QgsProject.instance().writeEntry("Paths", "/Absolute", True)

        # base maps
        maps_dir = str(settings.value("General", "maps"))
        if maps_dir and os.path.exists(maps_dir):
            for fil in os.listdir(maps_dir):
                if fil[-4:] == ".vrt":
                    file_name = os.path.join(maps_dir, fil)
                    file_info = QFileInfo(file_name)
                    base_name = file_info.baseName()
                    layer = QgsRasterLayer(file_name, base_name)
                    if not layer.isValid():
                        raise ProjectLoadError(f"Invalid base map layer {file_name}")
                    QgsProject.instance().addMapLayer(layer)
                    layers.append(layer)
                    layer_node = (
                        QgsProject.instance().layerTreeRoot().findLayer(layer.id())
                    )
                    layer_node.setExpanded(False)
                    layer_node.setItemVisibilityChecked(Qt.Unchecked)

        # test si le projet a un second milieu
        project = Project(database)
        project_SRID = project.get_project_SRID()
        second_milieu = project.has_second_milieu()

        # constant layers
        for column, units in SpatialiteMeshConstDataProvider.sourceUnits.items():
            for sm in ["no"] + (["yes"] if second_milieu else []):
                if sm == "yes" and column in ["altitude", "altitude_mur"]:
                    continue
                layer = MeshLayer(
                    "dbname="
                    + database
                    + " crs=epsg:{} column=".format(project_SRID)
                    + column
                    + " second_milieu="
                    + sm,
                    column + ("_second_milieu" if sm == "yes" else ""),
                    SpatialiteMeshConstDataProvider.PROVIDER_KEY,
                )
                logger.notice("adds constant layer ", column)
                layer.colorLegend().setTitle(column)
                layer.colorLegend().setUnits(units)
                maxValue = layer.dataProvider().maxValue()
                layer.colorLegend().setMaxValue(layer.dataProvider().maxValue())
                if column == "v_norme":
                    layer.colorLegend().setLogScale(True)
                    layer.colorLegend().setMinValue(
                        float(settings.value("Variables", "darcyScale"))
                        * layer.colorLegend().maxValue()
                    )
                else:
                    layer.colorLegend().setMinValue(layer.dataProvider().minValue())

                if column == "permeabilite_x":
                    layer.colorLegend().setLogScale(True)

                if not layer.isValid():
                    raise ProjectLoadError(
                        f"Can't load layer for column {column} from spatialite databse"
                    )
                QgsProject.instance().addMapLayer(layer)
                layers.append(layer)
                layer_node = QgsProject.instance().layerTreeRoot().findLayer(layer.id())
                layer_node.setExpanded(False)
                layer_node.setItemVisibilityChecked(Qt.Unchecked)

        for layer_name in [
            "parametres_simulation",
            "parametres",
            "metis_options",
            "openfoam_options",
            "sites",
            "simulations",
            "dates_resultats",
            "elements_chimiques",
            "mesures",
        ] + (
            [
                "simulations_second_milieu",
                "injections_second_milieu",
                "noeuds_second_milieu",
                "mailles_second_milieu",
            ]
            if second_milieu
            else []
        ):
            if layer_name != "dates_resultats":
                layer = QgsVectorLayer(
                    "{}|layername={}".format(database, layer_name), layer_name, "ogr"
                )
            else:
                uri = QgsDataSourceUri()
                uri.setDatabase(database)
                uri.setDataSource("", layer_name, "")
                layer = QgsVectorLayer(uri.uri(), layer_name, "spatialite")
            if layer.isValid():
                QgsProject.instance().addMapLayer(layer)
                layers.append(layer)
            else:
                logger.notice("layer", layer_name, "is not valid")

        for layer_name in [
            "mailles",
            "noeuds",
            "contours",
            "injections",
            "points_interet",
            "forages",
        ]:
            if layer_name != "forages":
                layer = QgsVectorLayer(
                    "{}|layername={}".format(database, layer_name), layer_name, "ogr"
                )
            else:
                uri = QgsDataSourceUri()
                uri.setDatabase(database)
                uri.setDataSource("", layer_name, "POINT")
                layer = QgsVectorLayer(uri.uri(), layer_name, "spatialite")
            if not layer.isValid():
                continue

            style = complete_filename("styles/" + layer_name + ".qml")
            if os.path.exists(style):
                layer.loadNamedStyle(style)
            if not layer.isValid():
                raise ProjectLoadError(
                    f"Can't load layer {layer_name} from spatialite databse"
                )
            QgsProject.instance().addMapLayer(layer)
            layers.append(layer)
            if layer_name in ["mailles", "noeuds"]:
                QgsProject.instance().layerTreeRoot().findLayer(
                    layer.id()
                ).setItemVisibilityChecked(Qt.Unchecked)

        QgsProject.instance().writeEntry("thyrsis", "compdb", database)

        canvas = canvas or QgsMapCanvas()
        proj = SpatialReference()
        proj.ImportFromEPSG(int(project_SRID))
        QgsProject.instance().setCrs(QgsCoordinateReferenceSystem(proj.ExportToWkt()))
        canvas.setDestinationCrs(QgsCoordinateReferenceSystem(proj.ExportToWkt()))
        canvas.setExtent(QgsProject.instance().mapLayersByName("mailles")[0].extent())
        canvas.refresh()

        QgsProject.instance().write()
        logger.notice("saving ", database[:-7] + ".qgs")
        return layers

    def execute(self, sql):
        """Execute sql query

        :param sql: sql query
        :param sql: string

        :return: cursor
        :rtype: sqlite3.cursor
        """
        return self.__cur.execute(sql)

    def commit(self):
        """Commit changes"""
        self.__conn.commit()
