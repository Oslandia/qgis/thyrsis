"""Thyrsis unit test for mesh building

.. note:: This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
"""

import os
import unittest

import pytest
from qgis.core import QgsProject

from thyrsis.database import sqlite

TEST_DIR = os.path.join(os.path.dirname(__file__), os.path.splitext(__file__)[0])
TUTORIAL_DATA_DIR = os.path.join(os.path.dirname(__file__), "../..", "tutorial_data")
TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "../..", "test_data")

SITE_DB = os.path.join(TEST_DIR, ".thyrsis", "sites.sqlite")
MESH_DB = os.path.join(TEST_DIR, ".thyrsis", "mesh", "SACLAY_mesh.mesh.sqlite")


@pytest.fixture()
def test_dir() -> str:
    return TEST_DIR


@pytest.fixture()
def mesh_db() -> str:
    return MESH_DB


@pytest.fixture()
def site_db() -> str:
    return SITE_DB


@pytest.fixture()
def mesh_generator():
    return "gmsh"


def test_load_thyrsis_chemicals_forages_csv(
        clean_test_dir, thyrsis, load_chemicals_csv, insert_saclay_site, load_forages_csv
):
    """
    Test loading from chemicals and forages csv

    @param clean_test_dir: fixture to have a clean test directory
    @param thyrsis: fixture to initialize thyrsis and return thyrsis plugin
    @param load_chemicals_csv: fixture to insert chemical into site database
    @param insert_saclay_site: fixture to insert saclay site
    @param load_forages_csv: fixture to insert forage into site database
    """
    assert os.path.exists(SITE_DB)

    with sqlite.connect(SITE_DB, "ThyrsisMeshTest") as conn:
        cur = conn.cursor()

        res = cur.execute("SELECT COUNT(*) FROM elements_chimiques").fetchone()[0]
        assert res == 617

        res = cur.execute("SELECT COUNT(*) FROM sites").fetchone()[0]
        assert res == 1

        res = cur.execute("SELECT COUNT(*) FROM forages").fetchone()[0]
        assert res == 17


def test_domains_creation(clean_test_dir, mesh_project, domains_creation):
    """
    Test domains creation from tutorial data

    @param clean_test_dir: fixture to have a clean test directory
    @param mesh_project: fixture to create a default mesh project and initialize thyrsis plugin
    @param domains_creation: fixture to create domains from tutorial data
    """
    db_lyr_contours = QgsProject.instance().mapLayersByName("contours")[0]
    db_lyr_domaines = QgsProject.instance().mapLayersByName("domaines")[0]

    # Data is inserted
    assert db_lyr_contours.featureCount() == 4

    # A new domain is created
    assert len([i for i in db_lyr_domaines.getFeatures()]) == 1


def test_copy_point_fixes(clean_test_dir, copy_point_fixes):
    """
    Test point fixes copy from tutorial data

    @param clean_test_dir: fixture to have a clean test directory
    @param copy_point_fixes: fixture to copy point fixe from tutorial data
    """

    # Check point fixe are available
    origin_lyr = QgsProject.instance().mapLayersByName("points_fixes")[0]
    assert len([i for i in origin_lyr.getFeatures()]) == 17


def test_copy_point_pilote(clean_test_dir, mesh_project, copy_point_pilotes):
    """
    Test copy of tutorial data point pilote csv

    @param clean_test_dir: fixture to have a clean test directory
    @param mesh_project: fixture to initialize a mesh project for point pilote import
    @param copy_point_pilotes: fixture to copy point pilotes from tutorial data
    """
    origin_lyr = QgsProject.instance().mapLayersByName("points_pilote")[0]
    assert len([i for i in origin_lyr.getFeatures()]) == 11


if __name__ == "__main__":
    unittest.main()
