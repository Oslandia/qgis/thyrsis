from builtins import object

import numpy
from OpenGL.GL import *
from OpenGL.GL import shaders

from thyrsis.exception import ZnsDisplayError

from ..meshlayer.glmesh import ColorLegend, GlMesh
from .utility import printCompileError

# -*- coding: UTF-8 -*-


class ZnsColumn(object):
    """Unsaturated column mesh"""

    def __init__(
        self, bar, contour, result_start_idx, znsLegend, znsDataProvider, parent=None
    ):
        """Constructor

        :param bar: list of column nodes
        :type bar: list
        :param contour: list of linestring
        :type contour: list
        :param result_start_idx: first idx of the column
        :type result_start_idx: integer
        :param znsLegend: color legend
        :type znsLegend: ColorLegend
        :param znsDataProvider:  unsaturated zone data provider
        :type znsDataProvider: znsDataProvider
        :param parent: widget parent
        :type parent: QWidget
        """
        self.__znsDataProvider = znsDataProvider
        self.__refZnsLegend = znsLegend
        self.__znsLegend = ColorLegend()
        self.__znsLegend.setParent(parent)
        self.__znsLegend.setTitle(self.__znsDataProvider.resultColumn())
        self.__znsLegend.setUnits(self.__znsDataProvider.units())
        self.__znsLegend.setLogScale(True)
        self.__settings = self.__znsDataProvider.settings()
        self.__znsLegend.maskUnits(
            int(self.__settings.value("Variables", "maskUnitsCheck"))
        )
        self.__vtx = numpy.empty((0, 3), dtype=numpy.float32)
        self.__idx = numpy.empty((0), dtype=numpy.int32)
        znsLegend.symbologyChanged.connect(self.render)
        self.__glMesh = GlMesh(self.__vtx, self.__idx, self.__znsLegend)
        self.__glMesh.setParent(parent)
        self.__nbVtxPerBar = len(bar)
        self.__startIdx = result_start_idx
        b = numpy.copy(bar)
        self.__nbVtxContour = 0
        for rings in contour:
            for ring in rings:
                begin = True  # to avoid creating band for first vtx of ring
                for vtx in ring:
                    self.__nbVtxContour += 1
                    b[:, 0:2] = vtx
                    nvtx = len(self.__vtx)
                    self.__vtx = numpy.concatenate((self.__vtx, b))
                    if not begin:
                        band = numpy.vstack(
                            (
                                numpy.arange(nvtx - self.__nbVtxPerBar, nvtx),
                                numpy.arange(nvtx, nvtx + self.__nbVtxPerBar),
                            )
                        ).reshape((-1,), order="F")
                        self.__idx = numpy.concatenate(
                            (
                                self.__idx,
                                numpy.array([nvtx - self.__nbVtxPerBar]),
                                band,
                                numpy.array([nvtx + self.__nbVtxPerBar - 1]),
                            )
                        )
                        # duplicate the firts and last point to create a degenerate triangles
                        # to link from bottom to top
                    else:
                        begin = False
        if len(self.__vtx) != self.__nbVtxPerBar * self.__nbVtxContour:
            raise ZnsDisplayError(
                f"Invalid number of vertex. Expecting {self.__nbVtxPerBar * self.__nbVtxContour}. {len(self.__vtx)} defined."
            )

    def scaleZ(self, factor):
        """Apply the factor scale

        :param factor: factor scale
        :type factor: float
        """
        self.__vtx[:, 2] *= factor

    def render(self, context=None):
        """OpenGl column rendering

        :param context: openGL context
        :type context: QOpenGLContext
        """
        values = self.__znsDataProvider.nodeValues()
        context = context or self.__znsLegend.parent()
        context.makeCurrent()
        glDisable(GL_TEXTURE_2D)
        glDisable(GL_LIGHT0)
        glEnableClientState(GL_VERTEX_ARRAY)
        glEnableClientState(GL_TEXTURE_COORD_ARRAY)
        glDisableClientState(GL_NORMAL_ARRAY)
        if not len(values):
            return
        else:
            if self.__glMesh._GlMesh__recompileShader:
                self.compileShaders()
            glUseProgram(self.__znsResultShaders)
            self.__znsLegend._setUniforms(context)
            val = numpy.concatenate(
                (values[self.__startIdx : (self.__startIdx + self.__nbVtxPerBar)],)
                * self.__nbVtxContour
            )
            assert len(val) == len(self.__vtx)
            # glColor4f(1, 1, 1, 1);
            glTexCoordPointer(1, GL_FLOAT, 0, val)
            glVertexPointerf(self.__vtx)
            glDrawElementsui(GL_TRIANGLE_STRIP, self.__idx)
        glUseProgram(0)

    def compileShaders(self):
        """Compile GLSL shader into binaries"""
        try:
            vertex_shader = shaders.compileShader(
                """
                varying float value;
                varying vec3 normal;
                varying vec4 ecPos;
                void main()
                {
                    ecPos = gl_ModelViewMatrix * gl_Vertex;
                    normal = normalize(gl_NormalMatrix * gl_Normal);
                    value = gl_MultiTexCoord0.st.x;
                    gl_Position = ftransform();
                }
                """,
                GL_VERTEX_SHADER,
            )
            fragment_shader = shaders.compileShader(
                self.__refZnsLegend._fragmentShader(), GL_FRAGMENT_SHADER
            )
            self.__znsResultShaders = shaders.compileProgram(
                vertex_shader, fragment_shader
            )
            self.__znsLegend._setUniformsLocation(self.__znsResultShaders)
        except Exception as e:
            printCompileError(e)
            raise e

    def colorLegend(self):
        """Return the color legend

        :return: legend
        :rtype: ColorLegend
        """

        return self.__znsLegend
