# Docker

The project hosts a Docker container with QGIS + OpenFOAM + PMF (Porous Multiphase Foam).

## Build

To build (from the root of the repository):

```bash
docker build \
    --pull --rm \
    -f "containers/ubuntu_qgis_openfoam_pmf/qgis_ltr_openfoam.dockerfile" \
    -t thyrsis:focal-qgis-openfoam-pfm \
    .
```

It's possible to pass OpenFOAM version to use:

```bash
docker build \
    --pull --rm \
    --build-arg OPENFOAM_PKG_VERSION=2106 \
    -f "containers/ubuntu_qgis_openfoam_pmf/qgis_ltr_openfoam.dockerfile" \
    -t thyrsis:focal-qgis-openfoam-pfm \
    .
```

----

## Use it

For example, to list the available openfoam versions inside the container:

```bash
docker run -v "$(pwd):/tmp/plugin" thyrsis:focal-qgis-openfoam-pfm openfoam-selector --list
```
