# coding = UTF-8


"""
Logging utility for thyrsis

By default we log in temporary file

A signal is emmited for each log if required
"""
import os
import sys
import tempfile
from builtins import object, str

LEVELS = {"debug": 0, "notice": 1, "warning": 2, "error": 3}


def _format(kwd, args):
    return (
        kwd
        + ": "
        + " ".join([str(arg) for arg in args]).replace("\n", "\n" + str(kwd) + ": ")
        + "\n"
    )


class NullProgressDisplay(object):
    """Null Progress Display"""

    def __init__(self):
        pass

    def set_ratio(self, ratio):
        pass


class ConsoleProgressDisplay(object):
    """Console Progress Display"""

    def __init__(self):
        if sys.stdout.isatty():
            sys.stdout.write("  0%")
            sys.stdout.flush()

    def set_ratio(self, ratio):
        """Display progress through simulation dates

        :param ratio: ratio of calculated dates
        :type ratio: float
        """
        if sys.stdout.isatty():
            sys.stdout.write("\b" * 4 + "% 3d%%" % (int(ratio * 100)))
            sys.stdout.flush()

    def __del__(self):
        if sys.stdout.isatty():
            sys.stdout.write("\n")
        else:
            sys.stdout.write("100%\n")
        sys.stdout.flush()


class QGisProgressDisplay(object):
    """QProgressBar wrapper to provide minimal interface"""

    def __init__(self):
        """Constructor"""
        from qgis.PyQt.QtCore import Qt
        from qgis.PyQt.QtWidgets import QProgressBar

        self.__widget = QProgressBar()
        self.__widget.setMaximum(100)
        self.__widget.setAlignment(Qt.AlignLeft | Qt.AlignVCenter)

    def widget(self):
        """Return the QProgressBar

        :return: progress bar
        :rtype: QProgressBar
        """
        return self.__widget

    def set_ratio(self, ratio):
        """set ratio of calculated dates

        :param ratio: ratio of calculated dates
        :type ratio: float
        """
        self.__widget.setValue(int(ratio * 100))


class Logger(object):
    """Thyrsis Logger"""

    def __init__(
        self,
        file_=os.path.join(
            tempfile.gettempdir(),
            (
                os.environ.get("USERNAME")
                if "USERNAME" in os.environ
                else os.environ.get("USER")
            )
            + "_thyrsis.log",
        ),
        iface=None,
        level="notice",
        enable_console=False,
        processing_feedback=None,
    ):
        """Constructor

        :param file: log file path
        :type file: string
        :param iface: qgis interface
        :type iface: QgisInterface
        :param level: logger level
        :type level: string
        :param enable_console: flag, enable console output
        :type enable_console: bool
        """
        assert level in LEVELS

        self.__logfile = open(file_, "w+") if file_ else None
        self.__iface = iface
        self.__level = level
        self.__enable_console = enable_console
        self.__processing_feedback = processing_feedback

    def __del__(self):
        self.__logfile.close()

    def set_iface(self, iface):
        """Set iface attribute

        :param iface: qgis interface
        :type iface: QgisInterface
        """
        self.__iface = iface

    def set_processing_feedback(self, processing_feedback):
        self.__processing_feedback = processing_feedback

    def disable_processing_feedback(self):
        self.__processing_feedback = None

    def enable_console(self, flag):
        """Activate output in the console

        :param flag: flag, enable console output
        :type flag: bool
        """
        self.__enable_console = flag

    def set_level(self, level):
        """Set logger level

        :param level: logger level
        :type level: string
        """
        self.__level = level

    def error(self, *args):
        """Write error"""
        if LEVELS["error"] >= LEVELS[self.__level]:
            message = _format("", args)
            if self.__iface and not self.__processing_feedback:
                self.__iface.messageBar().pushCritical("thyrsis", _format("", args))
            if self.__logfile:
                self.__logfile.write(_format("error", args))
                self.__logfile.flush()
            if self.__enable_console and sys.stdout is not None:
                sys.stdout.write(_format("error", args))
            if self.__processing_feedback:
                self.__processing_feedback.reportError(message)

    def warning(self, *args):
        """Write warning"""
        if LEVELS["warning"] >= LEVELS[self.__level]:
            message = _format("", args)
            if self.__iface and not self.__processing_feedback:
                self.__iface.messageBar().pushWarning("thyrsis", message)
            if self.__logfile:
                self.__logfile.write(_format("warning", args))
                self.__logfile.flush()
            if self.__enable_console and sys.stdout is not None:
                sys.stdout.write(_format("warning", args))
            if self.__processing_feedback:
                self.__processing_feedback.pushWarning(message)

    def notice(self, *args):
        """Write notation"""
        if LEVELS["notice"] >= LEVELS[self.__level]:
            message = _format("", args)
            # we log too much notice in thyrsis, we don't want theme in the message bar
            # if self.__iface:
            #    self.__iface.messageBar().pushInfo('thyrsis', message)
            if self.__logfile:
                self.__logfile.write(_format("notice", args))
                self.__logfile.flush()
            if self.__enable_console and sys.stdout is not None:
                sys.stdout.write(_format("notice", args))
            if self.__processing_feedback:
                self.__processing_feedback.pushInfo(message)

    def debug(self, *args):
        """Write debug info"""
        if LEVELS["debug"] >= LEVELS[self.__level]:
            if self.__logfile:
                self.__logfile.write(_format("debug", args))
                self.__logfile.flush()
            if self.__enable_console and sys.stdout is not None:
                sys.stdout.write(_format("debug", args))
            if self.__processing_feedback:
                self.__processing_feedback.pushDebugInfo(_format("debug", args))

    def task_log(self, log: str):
        """
        Add log from a task, no level information

        @param log: log string
        """
        # Add a return
        log = log + "\n"
        if self.__logfile:
            self.__logfile.write(log)
            self.__logfile.flush()
        if self.__enable_console and sys.stdout is not None:
            sys.stdout.write(log)
        if self.__processing_feedback:
            self.__processing_feedback.pushDebugInfo(log)

    def progress(self, message):
        """Write progress

        :param message: message to write
        :type message: string"""
        from qgis.core import Qgis

        if self.__iface and not self.__processing_feedback:
            progressMessageBar = self.__iface.messageBar().createMessage(message)
            progress = QGisProgressDisplay()
            progressMessageBar.layout().addWidget(progress.widget())
            self.__iface.messageBar().pushWidget(
                progressMessageBar, Qgis.Info, duration=1
            )
            return progress
        if self.__enable_console:
            sys.stdout.write("Progress: %s " % (message))
            return ConsoleProgressDisplay()

        if self.__processing_feedback:
            self.__processing_feedback.setProgressText(message)

        return NullProgressDisplay()


logger = Logger()
