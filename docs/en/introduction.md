# General description

THYRSIS (*Transport HYdrogéologique Rapide Simulé en Insaturé et Saturé*) is a software to simulate hydrogeological flow and transport of chemical and radiologic substances in both unsaturated and saturated porous environments.

It is a QGIS extension which is able to use all features provided by this GIS (Geographical Information System).

The modeling is based of 1D vertical unsaturated columns, coupled with a 2D horizontal groundwater model. The groundwater flow model can be imported or directly created with THYRSIS, which includes the [GMSH](http://gmsh.info/) mesh builder {cite}`geuzaine-remacle:2009`  and provides an inversion process  {cite}`renard-jeannee:2008`, {cite}`renard-tognelli:2016`.

1D and 2D simulations are realized with an external software for hydrogeological flow and transport in porous environnement. Different softwares can be connected to THYRSIS and two softwares are currently implemented :

- OPENFOAM, through the (opensource) [porousMultiphaseFOAM](https://github.com/phorgue/porousMultiphaseFoam/) toolbox {cite}`horgue-soulaine:2015`.

- METIS (*Modélisation des Écoulements et des Transports avec Interaction en milieu Saturé*), developed by the Ecole des Mines de Paris {cite}`goblet:1989`.

THYRSIS can handle several injection types (flux, concentration or mass) and several injection zones, which can be defined in surface or underground. Injection can also be defined on the whole study area and directly into the groundwater, ignoring the unsaturated zone.

THYRSIS includes uncertainty management using `Latin Hypercube`. By defining probability law for each parameters, the user can generate several simulations with different parameters and THYRSIS provides main statistics : mean, standard deviation and confidence interval. It also provides probability maps according to a threshold on a value.

The plugin uses the MPI protocol to implement parallel simulations in case of several unsaturated columns or uncertainty simulations with several parameter sets.

Chemical modeling is limited to the solubility limit approach, that helps to define solute flux on surface, and to the retention coefficient, which induces a slow-down in the solute transport. Solute compound speciation, complexation with organic material like humic substances and complex interactions with rock matrix are ignored.

THYRSIS plugin can simulate flow and material transport in "dual-porosity" model, as formulated by Gerke & Van Genuchten  {cite}`gerke-genuchten:1993` (but currently only with the METIS software).

THYRSIS is integrated to QGIS, so that users can define in a interactive way the injection zones and their characteristics, and the simulation parameters. Users can also launch simulations and display the results 1- in the 2D interface of QGIS, 2- in a 3D specific window, or 3- in time-dependant graphics showing the results as a function of time at specific locations, allowing for comparison with real measures. A mass balance graph is also created, showing total unsaturated and saturated mass as a function of time.

The plugin relies on Spatialite databases, which store all hydrogeological characteristics of the different models and also the simulation parameters.
