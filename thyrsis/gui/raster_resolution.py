"""
Simple gui to define raster resolution
"""

import os

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from qgis.PyQt import uic


class RasterResolutionDialog(QDialog):
    """Dialog to define raster resolution"""

    def __init__(self):
        """Constructor"""
        super(RasterResolutionDialog, self).__init__()
        uic.loadUi(
            os.path.join(os.path.dirname(__file__), "choose_raster_resolution.ui"), self
        )
