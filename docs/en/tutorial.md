# Tutorial

This tutorial explains with an example how to create a new hydrogeologic model and how to create and use a new study.

Tutorial data can be found in the `thyrsis/tutorial_data` folder.

A hydrogeologic model is associated to a site and is a combination of a mesh and some scalar and spatial characterics.

The site can be characterized by points of interest, measurements and boreholes descriptions.

```{toctree}
---
maxdepth: 2
---
tutorial/mesh_creation
tutorial/model_creation
tutorial/radionuclides
tutorial/site_characterization
tutorial/study_creation
```
