import calendar
import os
import time
from builtins import range
from datetime import datetime, timedelta

import numpy

# CI can't import matplotlib as it is headless
try:
    import matplotlib.dates as mdates
    import matplotlib.pyplot as plt
    from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
    from matplotlib.backends.backend_qt5agg import (
        NavigationToolbar2QT as NavigationToolbar,
    )
except:
    print("Ok for packaging, else please install sip")

import scipy.optimize as optimization
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QMargins, QSize

from ..meshlayer.utilities import format_
from ..spatialitemeshdataprovider import _getElementContaining

# matplotlib.rc('xtick', labelsize=10)
# matplotlib.rc('ytick', labelsize=10)


class AdjustWidget(QWidget):
    """Widget to adjust plot in the 1D panel"""

    def __init__(
        self,
        curve,
        dates,
        values,
        allMeasureDates,
        allMeasureValues,
        units,
        canvas,
        timePlot,
    ):
        """Constructor

        :param curve: plot
        :type curve: matplotlib.pyplot
        :param dates: list of dates
        :type dates: list
        :param values: list of values
        :type values: list
        :param allMeasureDates: list  of all measure point dates
        :type allMeasureDates: list
        :param allMeasureValues: list of all measure point values
        :type allMeasureValues: list
        :param units: units
        :type units: string
        :param canvas: image canvas
        :type canvas: QCanvas
        :param timePlot: Timeplot QWidget
        :type timePlot: QWidget

        """
        super(AdjustWidget, self).__init__()
        uic.loadUi(os.path.join(os.path.dirname(__file__), "plot_adjust.ui"), self)

        self.unitsLabel.setText(units)
        self.__curve = curve
        self.__canvas = canvas
        self.__dates = dates
        self.__values = values
        self.__time_plot = timePlot
        self.timeOffsetEdit.editingFinished.connect(self.offset)
        self.timeUnitsComboBox.currentIndexChanged.connect(self.offset)
        self.valueOffsetEdit.editingFinished.connect(self.offset)
        self.resetButton.clicked.connect(self.reset)
        self.timeButton.clicked.connect(self.adjustX)
        self.valueButton.clicked.connect(self.adjustY)
        self.fitButton.clicked.connect(self.adjustXY)

        self.__xOffset = timedelta(days=0)
        self.__yOffset = 0

        self.__xDataOrig = numpy.array(
            [float(calendar.timegm(date.timetuple())) for date in self.__dates]
        )
        self.__yDataOrig = numpy.array(self.__values)

        self.__xData = numpy.array(
            [float(calendar.timegm(date.timetuple())) for date in allMeasureDates]
        )
        self.__yData = numpy.array(allMeasureValues)

    def reset(self, dummy=None):
        """Reset custom display"""
        self.setOffsetX("+0")
        self.setOffsetY("+0")

    def setOffsetX(self, ox):
        """Set x offset

        :param ox: offset value
        :type ox: string
        """

        self.timeOffsetEdit.setText(ox)
        self.offset()

    def setOffsetY(self, oy):
        """Set y offset

        :param oy: offset value
        :type oy: string
        """
        self.valueOffsetEdit.setText(oy)
        self.offset()

    def offset(self, scratch=None):
        """Apply offset"""
        self.__yOffset = float(self.valueOffsetEdit.text())
        delta = (
            float(self.timeOffsetEdit.text())
            * {
                self.tr("years"): 365.25,
                self.tr("months"): 365.25 / 12,
                self.tr("days"): 1,
                self.tr("hours"): 1.0 / 24,
                self.tr("minutes"): 1.0 / (24 * 60),
                self.tr("seconds"): 1.0 / (24 * 3600),
            }[self.timeUnitsComboBox.currentText()]
        )
        self.__xOffset = timedelta(days=delta)
        self.__curve.set_xdata([d + self.__xOffset for d in self.__dates])
        self.__curve.set_ydata([y + self.__yOffset for y in self.__values])
        # self.__canvas.draw()
        self.__time_plot.autoscale()

    def func(self, t):
        """Interpolate with the model data"""
        return numpy.interp(t, self.__xDataOrig, self.__yDataOrig)

    def adjustX(self):
        """Automatically set x offset"""

        def func(t, dt):  # interpolate with the model data
            return self.func(t - dt) + self.__yOffset

        res = optimization.curve_fit(
            func,
            self.__xData,
            self.__yData,
            self.__xOffset.total_seconds()
            + 0.001 * (numpy.max(self.__xDataOrig) - numpy.min(self.__xDataOrig)),
        )
        delta = (
            res[0][0]
            / {
                self.tr("years"): 365.25 * 24 * 3600,
                self.tr("months"): (365.25 / 12) * 24 * 3600,
                self.tr("days"): 24 * 3600,
                self.tr("hours"): 3600,
                self.tr("minutes"): 60,
                self.tr("seconds"): 1,
            }[self.timeUnitsComboBox.currentText()]
        )
        self.setOffsetX("%.2g" % (delta))

    def adjustY(self):
        """Automatically set y offset"""

        def func(t, dy):  # interpolate with the model data
            return self.func(t - self.__xOffset.total_seconds()) + dy

        res = optimization.curve_fit(
            func,
            self.__xData,
            self.__yData,
            self.__yOffset
            + 0.001 * (numpy.max(self.__yDataOrig) - numpy.min(self.__yDataOrig)),
        )
        self.setOffsetY("%.2g" % (res[0][0]))

    def adjustXY(self):
        """Automatically set xy offset"""

        def func(t, dt, dy):  # interpolate with the model data
            return self.func(t - dt) + dy

        if len(self.__xData) > 1:
            res = optimization.curve_fit(
                func,
                self.__xData,
                self.__yData,
                (
                    self.__xOffset.total_seconds()
                    + 0.001
                    * (numpy.max(self.__xDataOrig) - numpy.min(self.__xDataOrig)),
                    self.__yOffset
                    + 0.001
                    * (numpy.max(self.__yDataOrig) - numpy.min(self.__yDataOrig)),
                ),
            )
            delta = (
                res[0][0]
                / {
                    self.tr("years"): 365.25 * 24 * 3600,
                    self.tr("months"): (365.25 / 12) * 24 * 3600,
                    self.tr("days"): 24 * 3600,
                    self.tr("hours"): 3600,
                    self.tr("minutes"): 60,
                    self.tr("seconds"): 1,
                }[self.timeUnitsComboBox.currentText()]
            )
            self.setOffsetX("%.2g" % (delta))
            self.setOffsetY("%.2g" % (res[0][1]))


class TimePlot(QWidget):
    """Time plot figure"""

    def __init__(self, figure, provider, plots, ax, settings, parent=None):
        """Constructor

        :param figure: figure
        :type figure: QFigure
        :param provider: provider of the mesh
        :type provider: MeshDataProvider
        :param plots: plot
        :type plots: matplotlib.pyplot
        :param ax: axes
        :type ax: matplotlib.pyplot.axes
        :param settings: settings used by Thyrsis
        :type settings: Settings
        :param parent: widget parent
        :type parent: QWidget
        """
        super(TimePlot, self).__init__(parent)

        self.__canvas = FigureCanvas(figure)
        self.__canvas.setMinimumSize(QSize(1, 1))

        self.__toolbar = NavigationToolbar(self.__canvas, None)
        layout = QVBoxLayout()
        layout.setContentsMargins(QMargins(0, 0, 0, 0))
        layout.setSpacing(0)
        self.setLayout(layout)
        layout.addWidget(self.__toolbar)
        layout.addWidget(self.__canvas)

        self.__settings = settings
        self.__provider = provider
        self.__plots = plots
        self.__ax = ax
        self.__figure = figure
        # self.__figure.tight_layout(rect=[0.05, 0., 1, 1])

        self.__xvalues = []
        self.__yvalues = []
        for i in range(len(plots)):
            self.__xvalues.append(numpy.array(plots[i].get_xdata()))
            self.__yvalues.append(numpy.array(plots[i].get_ydata()))

        for i in range(len(self.__plots)):
            self.__plots[i].set_xdata([])
            self.__plots[i].set_ydata([])
        self.__provider.dataChanged.connect(self.set_end)
        # self.__background = self.copy_from_bbox(self.__ax.bbox)
        self.__fps = []
        self.__last_didx = 0

        self.legendPosition = QComboBox()
        self.legendPosition.addItem("En bas à droite", "lower right")
        self.legendPosition.addItem("En bas à gauche", "lower left")
        self.legendPosition.addItem("En haut à droite", "upper right")
        self.legendPosition.addItem("En haut à gauche", "upper left")
        self.__setLegendPosition(0)
        self.legendPosition.currentIndexChanged.connect(self.__setLegendPosition)
        self.__toolbar.addWidget(self.legendPosition)
        self.__figure.set_tight_layout(True)

    def ax(self):
        """Get axes

        :return: axes
        :rtype: matplotlib.pyplot.axes
        """
        return self.__ax

    def autoscale(self):
        """Automatic scale the axes"""
        self.__ax.relim()
        self.__ax.autoscale_view()
        self.__canvas.draw()

    def __setLegendPosition(self, idx):
        """Set the legend position

        :param idx: idx position
        :type idx: int
        """
        self.__ax.legend(
            loc=self.legendPosition.itemData(idx),
            numpoints=1,
            fontsize=self.__settings.value("Matplotlib", "legendFontSize", 10),
        )
        self.__canvas.draw()

    def toolbar(self):
        """Get navigation toolbar

        :return: navigation toolbar
        :rtype: NavigationToolbar2QT
        """
        return self.__toolbar

    def set_linestyle(self, style=None, width=None, color=None, marker=None):
        """Set the line style

        :param style: style of the line '-','-.'
        :type style: string
        :param width: width line
        :type width: float
        :param color: color line
        :type color: string
        :param marker: marker style
        :type marker: string
        """
        if len(self.__plots) == 1:
            p = self.__plots[0]
            style and p.set_linestyle(style)
            width and p.set_linewidth(width)
            color and p.set_color(color)
            marker and p.set_marker(marker)
            self.__canvas.draw()

    def __update_data(self, draw=False):
        """Update the plot data, and eventually draw it

        :param draw: flag
        :type draw: bool
        """
        date_idx = self.__provider.date()  # if not didx else didx
        if self.__last_didx == date_idx:
            return True
        for i in range(len(self.__plots)):
            self.__plots[i].set_xdata(self.__xvalues[i][0 : (date_idx + 1)])
            self.__plots[i].set_ydata(self.__yvalues[i][0 : (date_idx + 1)])
            if draw:
                self.__ax.draw_artist(self.__plots[i])
        update = (self.__last_didx <= date_idx) and (date_idx != 0)
        self.__last_didx = date_idx
        return update

    def save(self, fileName, sz, dpi=150):
        """Save the plot as an image

        :param fileName: file path
        :type fileName: str
        :param sz: size
        :type sz: QSize
        :param dpi: image resolution
        :type dpi: int
        """
        self.__update_data()
        w, h = self.__figure.get_size_inches()
        dpi_save = self.__figure.get_dpi()
        self.__figure.set_size_inches(float(sz.width()) / dpi, float(sz.height()) / dpi)
        self.__figure.set_dpi(dpi)
        self.__figure.tight_layout(rect=[0.05, 0.0, 1, 1])
        self.__figure.savefig(fileName, dpi=(dpi))  # , bbox_inches='tight')
        self.__figure.set_dpi(dpi_save)
        self.__figure.set_size_inches(w, h)
        self.__canvas.draw()

    def resizeEvent(self, event):
        """Resize plot"""
        self.__update_data()
        QWidget.resizeEvent(self, event)

    def showEvent(self, event):
        """Show plot"""
        self.__update_data()
        QWidget.showEvent(self, event)

    def set_end(self, force=False):
        """Triggered when dataprovider changes

        :param force: flag to force
        :type force: bool
        """
        if not self.isVisible():
            return
        start_time = time.time()
        if self.__update_data(True):
            self.__canvas.update()
        else:
            self.__canvas.draw()

        # self.__fps.append(int(1/(time.time() - start_time)))

    def fps(self):
        """Return fps

        :return: fps
        :rtype: narray"""
        return numpy.array(self.__fps)

    @staticmethod
    def mass_balance(provider, settings, labels):
        """Display mass balance graph

        :param provider: provider of the mesh
        :type provider: MeshDataProvider
        :param settings: settings used by Thyrsis
        :type settings: Settings
        :param labels: labels axes
        :type labels: list
        """
        dates = [
            datetime.strptime(date, "%Y-%m-%d %H:%M:%S") for date in provider.dates()
        ]
        sat, out, insat = provider.mass_balance()

        if sat is None:
            return None

        figure = plt.figure()
        ax = figure.add_subplot(111)
        ax.tick_params(
            labelsize=int(settings.value("Matplotlib", "ticksLabelSize", 10))
        )

        plots = []
        if len(insat):
            insat = insat[:, 0]
            tot = sat + insat + out
            plots.append(
                ax.plot(dates, insat, label=labels[0], color="red", linewidth=3)[0]
            )
        else:
            tot = sat + out

        plots.append(
            ax.plot(dates, sat, label=labels[1], color="green", linewidth=3)[0]
        )
        plots.append(ax.plot(dates, out, label=labels[2], color="blue", linewidth=3)[0])
        plots.append(
            ax.plot(dates, tot, label=labels[3], color="black", linewidth=3)[0]
        )
        ax.format_xdata = mdates.DateFormatter("%d/%m/%Y %H:%M:%S")
        ax.yaxis.set_major_formatter(
            plt.FuncFormatter(lambda x, pos: format_(0, numpy.max(tot)) % (x))
        )
        ax.set_ylim([0, 1.1 * numpy.max(tot)])
        # ax.set_xlabel(u"dates")
        ax.set_ylabel(
            labels[4]
            + (
                "[kg]" if not int(settings.value("Variables", "maskUnitsCheck")) else ""
            ),
            fontsize=int(settings.value("Matplotlib", "ylabelFontSize", 10)),
        )
        ax.legend(
            loc="lower right",
            fontsize=int(settings.value("Matplotlib", "legendFontSize", 10)),
        )
        figure.autofmt_xdate()

        plt.grid(True)
        canvas = TimePlot(figure, provider, plots, ax, settings)
        return canvas

    @staticmethod
    def create(provider, point, name, colorsMeasures, colors, settings):
        """Display graph at a point

        :param provider: provider of the mesh
        :type provider: MeshDataProvider
        :param point: point
        :type point: QgPoint
        :param name: point name
        :type name: string
        :param colorsMeasures: dict of colors for type measure
        :type colorsMeasures: dict
        :param colors: colors
        :type colors: list
        :param settings: settings used by Thyrsis
        :type settings: Settings
        """
        dates = [
            datetime.strptime(date, "%Y-%m-%d %H:%M:%S") for date in provider.dates()
        ]

        figure = plt.figure()
        ax = figure.add_subplot(111)
        ax.tick_params(
            labelsize=int(settings.value("Matplotlib", "ticksLabelSize", 10))
        )
        result = None
        plots = []

        mes = False
        allMeasureDates = []
        allMeasureValues = []
        for type_, val in provider.measuresAt(point).items():
            if type_ not in list(colorsMeasures.keys()):
                colorsMeasures[type_] = colors[
                    len(list(colorsMeasures.keys())) % len(colors)
                ]
            measureValues = [
                v[1] for v in val if v[0] >= dates[0] and v[0] <= dates[-1]
            ]
            measureErr = [v[2] for v in val if v[0] >= dates[0] and v[0] <= dates[-1]]
            measuresDates = [
                v[0] for v in val if v[0] >= dates[0] and v[0] <= dates[-1]
            ]
            allMeasureValues += measureValues
            allMeasureDates += measuresDates
            if len(measuresDates):
                if max(measureErr) > 0:
                    ax.errorbar(
                        measuresDates,
                        measureValues,
                        yerr=measureErr,
                        fmt="o",
                        label=type_,
                        color=colorsMeasures[type_],
                    )
                else:
                    ax.plot(
                        measuresDates,
                        measureValues,
                        "+",
                        label=type_,
                        color=colorsMeasures[type_],
                    )

                mes = True
                ax.legend(
                    loc="upper left",
                    numpoints=1,
                    fontsize=int(settings.value("Matplotlib", "legendFontSize", 10)),
                )
                if not len(dates):
                    ax.yaxis.set_major_formatter(
                        plt.FuncFormatter(
                            lambda x, pos: format_(
                                min(measureValues), max(measureValues)
                            )
                            % (x)
                        )
                    )

        if len(dates):
            values, svalues = provider.valuesAt(point, True)
            assert len(dates) == len(values)
            # confidence area if available (95% = 2*sigma)
            if len(svalues):
                tol = 2.0 * svalues
                ax.fill_between(
                    dates,
                    numpy.maximum(0.0, values - tol),
                    values + tol,
                    alpha=0.5,
                    interpolate=True,
                )

            # curve
            (result,) = ax.plot(dates, values, linewidth=3, color="black")
            plots.append(result)
            ax.yaxis.set_major_formatter(
                plt.FuncFormatter(
                    lambda x, pos: format_(numpy.min(values), numpy.max(values)) % (x)
                )
            )
            ax.set_xlim([dates[0], dates[-1]])

        if not len(dates) and not mes:
            return None

        # name
        # ax.text(0.99*ax.get_xlim()[1]+0.01*ax.get_xlim()[0], 0.99*ax.get_ylim()[1]+0.01*ax.get_ylim()[0], name, ha='right', va='top')
        ax.text(
            0.99,
            0.99,
            name,
            horizontalalignment="right",
            verticalalignment="top",
            transform=ax.transAxes,
        )

        ax.format_xdata = mdates.DateFormatter("%d/%m/%Y")
        ax.set_ylabel(
            provider.resultColumn()
            + (
                "[" + provider.units() + "]"
                if not int(settings.value("Variables", "maskUnitsCheck"))
                else ""
            ),
            fontsize=int(settings.value("Matplotlib", "ylabelFontSize", 10)),
        )
        figure.autofmt_xdate()

        plt.grid(True)
        canvas = TimePlot(figure, provider, plots, ax, settings)
        if mes:
            adjust = AdjustWidget(
                result,
                dates,
                values,
                allMeasureDates,
                allMeasureValues,
                provider.units(),
                figure.canvas,
                canvas,
            )
            adjust.setSizePolicy(QSizePolicy(QSizePolicy.Maximum, QSizePolicy.Fixed))
            canvas.layout().addWidget(adjust)
        return canvas


if __name__ == "__main__":
    import os
    import sys

    from PyQt5.QtCore import *
    from PyQt5.QtGui import *

    from thyrsis.gui.time_plot_control import TimePlotControl
    from thyrsis.log import logger
    from thyrsis.spatialitemeshdataprovider import SpatialiteMeshDataProvider

    from ..database import sqlite

    QCoreApplication.setOrganizationName("QGIS")
    QCoreApplication.setOrganizationDomain("qgis.org")
    QCoreApplication.setApplicationName("QGIS2")

    app = QApplication(sys.argv)
    timer = QTimer(None)

    class Animator(QObject):
        def __init__(self, provider):
            super(Animator, self).__init__()
            self.provider = provider
            self.maxIdx = len(provider.dates())

        def animate(self):
            self.provider.setDate((self.provider.date() + 1) % self.maxIdx)

    ctrl = TimePlotControl()
    ctrl.exec_()

    if len(sys.argv) == 2:
        plw = QDockWidget()
        plw.resize(800, 600)
        plw.show()
        plw.setWidget(QTabWidget())
        with sqlite.connect(sys.argv[1]) as conn:
            cur = conn.cursor()
            project_SRID = str(
                cur.execute(
                    "SELECT srid FROM geometry_columns WHERE f_table_name = 'noeuds'"
                ).fetchone()[0]
            )
        provider = SpatialiteMeshDataProvider(
            "dbname={} crs=epsg:{} resultColumn=concentration".format(
                sys.argv[1], project_SRID
            )
        )
        settings = Settings(sys.argv[1])
        provider.setDate(len(provider.dates()) - 1)
        provider.setUnits("µg/L")
        with sqlite.connect(sys.argv[1]) as conn:
            canvast = TimePlot.mass_balance(provider)
            plw.widget().addTab(canvast, "Bilan de masse")
            canvast.save("/tmp/test.png", QSize(800, 600))
            animator = Animator(provider)
            timer.timeout.connect(animator.animate)

            logger.notice("concentration plot")
            colorsMeasures = {}  # stocke les couleurs de chaque type de mesure
            # colors = ('red','green','blue','orange','yellowGreen','purple','pink','cyan','violet','salmon','turquoise','brown','yellow')
            colors = (
                "#ff0000",
                "#00ff00",
                "#0000ff",
                "#ffa500",
                "#9acd32",
                "#a020f0",
                "#ffc0cb",
                "#00ffff",
                "#ee82ee",
                "#fa8072",
                "#40e0d0",
                "#a52a2a",
                "#ffff00",
            )
            for x, y, nom in conn.execute(
                "SELECT X(GEOMETRY), Y(GEOMETRY), nom FROM points_interet WHERE groupe='calcul' LIMIT 3"
            ).fetchall():
                if _getElementContaining(conn, (x, y)):
                    try:
                        canvas = TimePlot.create(
                            provider, (x, y), nom, colorsMeasures, colors, settings
                        )
                        plw.widget().addTab(canvas, nom)
                        canvas.set_linestyle(
                            style=ctrl.style.currentText(),
                            width=ctrl.width.value(),
                            color=ctrl.color.color().name(),
                            marker=ctrl.marker.currentText(),
                        )
                    except Exception as e:
                        logger.error(e)
            # timer.start(1000./20)
            provider.setDate(len(provider.dates()))
            app.exec_()

        for fps in canvast.fps():
            logger.debug(fps)
