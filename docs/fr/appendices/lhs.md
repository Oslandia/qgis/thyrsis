(latin_hypercube_fr)=
# Hypercube Latin

L'Hypercube Latin est une technique d'échantillonnage adaptée au calcul d'incertitude, permettant de réduire le nombre d'échantillons par rapport à une technique de Monte Carlo. Il est désigné par LHS pour Latin Hypercube Sampling.

Le principe est :
- de diviser l'intervalle de variation de chaque paramètre en un nombre égal n de sous-intervalles équiprobables
- de sélectionner aléatoirement une valeur dans chacun de ces sous-intervalles
- de construire n échantillons par permutation aléatoire de manière que chaque sous-intervalle de chaque paramètre soit représenté une et une seule fois.

La {numref}`lhs1` propose une représentation schématique de cet échantillonnage dans le cas de deux paramètres et de 5 échantillons.

(lhs1)=

:::{figure} /_static/images/lhs1.png
Représentation bidimensionnelle d'un échantillonnage par Hypercube Latin dans le cas de deux paramètres et de 5 échantillons (n=5). X1 a une "distribution normale entre A1 et B1, X2 une distribution uniforme entre A2 et B2
:::

Pour définir les n sous-intervalles équiprobables de chaque densité de probabilité, il suffit de diviser l'image de la fonction de répartition correspondante en autant d'intervalles réguliers, puis d'inverser la fonction de répartition.

## Loi uniforme ou log-uniforme

Dans le cas d'une loi uniforme ou log-uniforme dans l'intervalle \[A,B\], l'inversion est immédiate :

$$
\begin{cases} X=A+r\left(B-A\right)\\ \ln X=\ln A+r\left(\ln B-\ln A\right) \end{cases}
$$

où r désigne un nombre généré aléatoirement dans chacun des n intervalles réguliers échantillonnant \[0,1\].

Un exemple est fourni {numref}`lhs2`, dans le cas de 5 échantillons et d'une distribution uniforme entre A2 et B2.

(lhs2)=

:::{figure} /_static/images/lhs2.png
Correspondance entre les intervalles échantillonnés d'une part, la densité de probabilité et la fonction de répartition d'autre part. Exemple avec n=5 , distribution uniforme entre A2 et B2.
:::

## Loi normale ou log-normale

Dans le cas d'une loi normale (ou log-normale), l'inversion est plus complexe et utilise la fonction erreur définie par :

$$
erf\left(x\right)=\frac{2}{\sqrt{\pi}}\int_{0}^{x}e^{-t^{2}}dt
$$

qui est classiquement tabulée et disponible dans des librairies informatiques.

En effet, la fonction de répartition d'une distribution normale de moyenne \mu et d'écart-type \sigma s'écrit :

$$
F_{\mu,\sigma}\left(x\right)=\frac{1}{\sigma\sqrt{2\pi}}\int_{-\infty}^{x}e^{-\frac{\left(t-\mu\right)^{2}}{2\sigma^{2}}}=\frac{1}{2}\left[1+erf\left(\frac{x-\mu}{\sigma\sqrt{2}}\right)\right]
$$

d'où :

$$
x=\mu+\sigma\sqrt{2}\: erf^{-1}\left(2z-1\right)
$$

où z désigne une valeur tirée aléatoirement dans chacun des sous-intervalles décomposant l'image de la fonction de répartition.

Dans la pratique, plutôt que de préciser les paramètres $\mu$ et $\sigma$ de la loi normale, on préfère souvent imposer les bornes A et B de l'intervalle de variation du paramètre, définies comme les quantiles 0,001 et 0,999 respectivement. On a ainsi :

$$
B=\mu+\sigma\sqrt{2}\: erf^{-1}\left(0,998\right)=\mu+3,09\:\sigma
$$

puis, comme: $\mu=\frac{A+B}{2}$ :

$$
\sigma=\frac{B-A}{2\sqrt{2}\: erf^{-1}\left(0,998\right)}=\frac{B-A}{6,18}
$$

Un exemple est fourni :{numref}`lhs3`, dans le cas de 5 échantillons et d'une distribution normale entre A1 et B1.

(lhs3)=

:::{figure} /_static/images/lhs3.png
Correspondance entre les intervalles échantillonnés d'une part, la densité de probabilité et la fonction de répartition d'autre part. Exemple avec n=5 , distribution normale entre A1 et B1.
:::
