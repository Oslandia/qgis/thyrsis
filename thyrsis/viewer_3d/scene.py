import numpy
from OpenGL.GL import *
from PyQt5.QtCore import *
from PyQt5.QtGui import *
from qgis.PyQt.QtWidgets import QGraphicsScene

from ..log import logger
from ..utilities import Timer
from .mesh import Mesh
from .utility import computeNormals
from .zns_column import ZnsColumn


class Scene(QObject):
    """3D Scene"""

    changed = pyqtSignal()

    # def __del__(self):
    #    print "Scene.__del__", self

    def __init__(
        self, dataProvider, colorLegend, znsDataProvider, znsLegend, parent=None
    ):
        """Constructor

        :param dataProvider:  data provider
        :type dataProvider: MeshDataProvider
        :param colorLegend: ColorLegend
        :type colorLegend: ColorLegend
        :param znsDataProvider:  unsaturated zone data provider
        :type znsDataProvider: znsDataProvider
        :param znsLegend: color legend
        :type znsLegend: ColorLegend
        :param parent: widget parent
        :type parent: QWidget
        """
        super(Scene, self).__init__(parent)
        self.dataProvider = dataProvider
        self.znsDataProvider = znsDataProvider
        self.__zScale = 1.0  # must be one here
        self.zsVtx = {}
        self.zsNrml = {}
        self.zsIdx = self.dataProvider.triangles()
        self.shaderNeedRecompile = True
        self.__terrainUV = None
        self.__terrainTexture = None
        self.__terrainImage = None

        colorLegend.symbologyChanged.connect(self.requireShaderRecompile)

        self.__displayScatters = ["calcul", "forage"]
        self.__displayContours = True

        self.__terrainVtx = self.dataProvider.nodeCoord("altitude")

        self.__bottomMesh = Mesh(
            self.dataProvider,
            colorLegend,
            "altitude - epaisseur_zns - epaisseur_zs",
            invertNrml=True,
            parent=parent,
        )
        self.__topMesh = Mesh(
            self.dataProvider, colorLegend, "altitude - epaisseur_zns", parent=parent
        )

        self.__znsColumns = []
        if self.znsDataProvider and len(self.znsDataProvider.labels()) <= int(
            znsDataProvider.settings().value("General", "nbColumnsMax")
        ):
            znsVtx = self.znsDataProvider.nodeCoord()
            if len(znsVtx):
                znsLegend.symbologyChanged.connect(self.requireShaderRecompile)
                contours = self.znsDataProvider.contours()
                bars = self.znsDataProvider.barsNodes()
                assert len(contours) == len(bars)
                idx = 0
                for i, contour in enumerate(contours):
                    self.__znsColumns.append(
                        ZnsColumn(
                            znsVtx[bars[i], :],
                            contour,
                            idx,
                            znsLegend,
                            self.znsDataProvider,
                            parent=parent,
                        )
                    )
                    idx += len(bars[i])

        self.__contours = self.dataProvider.contours()
        self.__contourColors = {
            name: QColor(0, 200, 200) if name[1] == "riviere" else QColor(255, 0, 0)
            for name in list(self.__contours.keys())
        }

        self.__scatters = []
        for scat in self.dataProvider.scatters():
            scene = QGraphicsScene()
            scene.setSceneRect(scene.itemsBoundingRect())
            scene.addText(scat["name"])  # , QFont('Arial', 32))
            image = QImage(scene.sceneRect().size().toSize(), QImage.Format_ARGB32)
            image.fill(Qt.transparent)
            painter = QPainter(image)
            scene.render(painter)
            image.save("/tmp/img.png")
            scat["texture"] = None
            scat["image"] = image
            self.__scatters.append(scat)

        self.setZscale(self.__zScale)

        self.extent = (
            numpy.min(self.__terrainVtx[:, 0]),
            numpy.min(self.__terrainVtx[:, 1]),
            numpy.max(self.__terrainVtx[:, 0]),
            numpy.max(self.__terrainVtx[:, 1]),
        )

        self.center = QVector3D(
            numpy.mean(self.__terrainVtx[:, 0]),
            numpy.mean(self.__terrainVtx[:, 1]),
            numpy.mean(self.__terrainVtx[:, 2]),
        )

        # if we want an image on terrain
        self.zsTexCoord = self.__terrainVtx[:, :2] - numpy.array(
            [self.extent[0], self.extent[1]]
        )
        self.zsTexCoord[:, 0] /= self.extent[2] - self.extent[0]
        self.zsTexCoord[:, 1] /= self.extent[3] - self.extent[1]

        pass

    def rendergl(self, leftv, upv, eye, height, context):
        """3D OpenGL rendering

        :param leftv: left value
        :type leftv: QVector3D
        :param upv: up value
        :type upv: QVector3D
        :param eye: camera eye
        :type eye: QVector3D
        :param height: height
        :type height: float
        :param context: openGL context
        :type context: QOpenGLContext
        """
        glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE)
        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, [1.0, 1.0, 1.0, 1.0])
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, [1.0, 1.0, 1.0, 1.0])
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, [1.0, 1.0, 1.0, 1.0])

        if self.shaderNeedRecompile:
            self.compileShaders()

        # render contours
        if self.__displayContours:
            glEnableClientState(GL_VERTEX_ARRAY)
            glDisableClientState(GL_TEXTURE_COORD_ARRAY)
            glDisableClientState(GL_NORMAL_ARRAY)

            glDisable(GL_TEXTURE_2D)
            glDisable(GL_LIGHTING)
            glEnable(GL_BLEND)
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
            glEnable(GL_LIGHT0)
            glEnable(GL_COLOR_MATERIAL)
            for name, contour in list(self.__contours.items()):
                color = self.__contourColors[name]
                glColor4f(color.redF(), color.greenF(), color.blueF(), color.alphaF())
                glVertexPointerf(contour)
                glLineWidth(3)
                glDrawArrays(GL_LINE_STRIP, 0, len(contour))

        # zns
        # 3d rendering
        timer = Timer()
        for col in self.__znsColumns:
            col.render(context)
        glUseProgram(0)

        self.__bottomMesh.render(context)
        self.__topMesh.render(context)

        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
        glEnable(GL_LIGHTING)
        glEnable(GL_LIGHT0)
        glEnable(GL_COLOR_MATERIAL)
        glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 100)
        glUseProgram(0)
        if self.__terrainTexture:
            glEnable(GL_TEXTURE_2D)
            glBindTexture(GL_TEXTURE_2D, self.__terrainTexture)
            color = QColor(255, 255, 255, self.__terrainColor.alpha())
            glEnableClientState(GL_TEXTURE_COORD_ARRAY)
            glTexCoordPointer(2, GL_FLOAT, 0, self.__terrainUV)
        else:
            glDisableClientState(GL_TEXTURE_COORD_ARRAY)
            glDisable(GL_TEXTURE_2D)
            color = QColor(self.__terrainColor)
        glEnableClientState(GL_VERTEX_ARRAY)
        glEnableClientState(GL_NORMAL_ARRAY)
        glVertexPointerf(self.__terrainVtx)
        glNormalPointerf(self.__terrainNrml)
        glColor4f(color.redF(), color.greenF(), color.blueF(), color.alphaF())
        glDrawElementsui(GL_TRIANGLES, self.zsIdx)

        # render scatters
        if len(self.__displayScatters):
            glDisableClientState(GL_VERTEX_ARRAY)
            glDisableClientState(GL_NORMAL_ARRAY)
            glDisableClientState(GL_TEXTURE_COORD_ARRAY)
            glEnable(GL_COLOR_MATERIAL)
            glDisable(GL_LIGHT0)
            glDisable(GL_DEPTH_TEST)
            for scatter in self.__scatters:
                if scatter["group"] in self.__displayScatters:
                    pt = scatter["point"]
                    point = QVector3D(pt[0], pt[1], pt[2])
                    dist = 0.8 * (point - eye).length() / height
                    w = dist * scatter["image"].width()
                    h = dist * scatter["image"].height()
                    glDisable(GL_TEXTURE_2D)
                    glColor4f(0, 0, 0, 1)
                    glPointSize(4)
                    glBegin(GL_POINTS)
                    glVertex3f(point.x(), point.y(), point.z())
                    glEnd()
                    glEnable(GL_TEXTURE_2D)
                    glBindTexture(GL_TEXTURE_2D, scatter["texture"])
                    glColor4f(1, 1, 1, 1)
                    glBegin(GL_QUADS)
                    glNormal3f(0, 0, 1)
                    glTexCoord2f(0, 0)
                    glVertex3f(point.x(), point.y(), point.z())
                    point -= leftv * w
                    glNormal3f(0, 0, 1)
                    glTexCoord2f(1, 0)
                    glVertex3f(point.x(), point.y(), point.z())
                    point += upv * h
                    glNormal3f(0, 0, 1)
                    glTexCoord2f(1, 1)
                    glVertex3f(point.x(), point.y(), point.z())
                    point += leftv * w
                    glNormal3f(0, 0, 1)
                    glTexCoord2f(0, 1)
                    glVertex3f(point.x(), point.y(), point.z())
                    glEnd()

    def initializeGL(self, textureBinder=None):
        """Initialize the OpenGL scene

        :param textureBinder: texture
        :type textureBinder: QOpenGLTexture
        """
        for scatter in self.__scatters:
            scatter["texture"] = textureBinder(scatter["image"])
        if self.__terrainImage is not None:
            self.__terrainTexture = textureBinder(self.__terrainImage)
        self.compileShaders()

    def setTerrainColor(self, color):
        """Set the field color

        :param color: color
        :type color: QColor"""
        self.__terrainColor = color
        self.changed.emit()

    def contourColors(self):
        """Get the contour color

        :return: contour color
        :rtype: QColor"""
        return self.__contourColors

    def setTerrainTexture(self, image, extend=[]):
        """Set the field texture

        :param image: image texture
        :type image: QImage
        :param extend: extend coordinates
        :type extend: list
        """
        if image is not None:
            self.__terrainUV = numpy.array(self.__terrainVtx[:, 0:2])
            self.__terrainUV -= numpy.array([extend[0], extend[1]])
            self.__terrainUV[:, 0] /= extend[2] - extend[0]
            self.__terrainUV[:, 1] /= extend[3] - extend[1]
            self.__terrainImage = image
            self.parent().makeCurrent()
            self.__terrainTexture = self.parent().bindTexture(image)
            logger.debug(
                "texture coords in",
                numpy.min(self.__terrainUV[:, 0]),
                numpy.max(self.__terrainUV[:, 0]),
                numpy.min(self.__terrainUV[:, 1]),
                numpy.max(self.__terrainUV[:, 1]),
                self.__terrainTexture,
            )
        else:
            self.__terrainTexture = None

        self.changed.emit()

    def terrainColor(self):
        """Get the field color

        :return: field color
        :rtype: QColor"""
        return self.__terrainColor

    def toggleScatters(self, flag, group):
        """Display/hide scatter (borehole, interest point)

        :param flag: flag
        :type flag: bool
        :param group: group name to display/hide
        :type group: string
        """
        if not bool(flag) and group in self.__displayScatters:
            self.__displayScatters.remove(group)
        elif bool(flag) and group not in self.__displayScatters:
            self.__displayScatters.append(group)
        self.changed.emit()

    def toggleContours(self, flag):
        """Display/hide contour

        :param flag: flag
        :type flag: bool
        """
        self.__displayContours = bool(flag)
        self.changed.emit()

    def requireShaderRecompile(self):
        """Set shaderNeedRecompile attribute to True"""
        self.shaderNeedRecompile = True

    def compileShaders(self):
        """Compile GLSL shader into binaries"""
        self.__bottomMesh.compileShaders()
        self.__topMesh.compileShaders()
        for col in self.__znsColumns:
            col.compileShaders()
        self.shaderNeedRecompile = False

    def zScale(self):
        """Get zScale

        :return: z scale
        :rtype: float"""
        return self.__zScale

    def setZscale(self, scale):
        """Set zScale

        :param scale: z scale
        :type scale: float"""
        factor = float(scale) / self.__zScale

        self.__terrainVtx[:, 2] *= factor
        self.__terrainNrml = computeNormals(self.__terrainVtx, self.zsIdx)

        self.__bottomMesh.scaleZ(factor)
        self.__topMesh.scaleZ(factor)

        for name, contour in list(self.__contours.items()):
            contour[:, 2] *= factor

        for scatter in self.__scatters:
            scatter["point"][2] *= factor

        for col in self.__znsColumns:
            col.scaleZ(factor)

        self.__zScale = scale

        self.changed.emit()

    def bottomMesh(self):
        """Get bottom mesh

        :return: bottom mesh
        :rtype: Mesh"""
        return self.__bottomMesh

    def topMesh(self):
        """Get top mesh

        :return: top mesh
        :rtype: Mesh"""
        return self.__topMesh

    def znsColumns(self):
        """Get unsaturated columns

        :return: unsaturated columns mesh
        :rtype: list"""
        return self.__znsColumns
