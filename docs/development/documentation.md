# Documentation

Project uses Sphinx to generate documentation from docstrings (documentation in-code) and custom pages written in RST and Markdown (througth the [MyST parser](https://myst-parser.readthedocs.io/en/latest/)).

## Build documentation website

To build it:

```bash
# install aditionnal dependencies
python -m pip install -U -r requirements/documentation.txt
# build it
sphinx-build -b html docs docs/_build
# optimized (quiet, multiprocessing, doctrees separated)
sphinx-build -b html -d docs/_build/cache -j auto -q docs docs/_build/html
```

Open `docs/_build/index.html` in a web browser.

## Write documentation using live render

```bash
sphinx-autobuild -b html -d docs/_build/cache docs/ docs/_build
```

Open <http://localhost:8000> in a web browser to see the HTML render updated when a file is saved.

## Add a new page to the documentation

For example, to add a usage in a new language:

1. Create a markdown file into `docs/{locale}/usage/my_new_page.md` where `{locale}` is the language code (en, fr, it, sp...)
2. Reference this new page in the homepage toctree (`index.md`) of the language:

````markdown
```{toctree}
---
caption: How to use the plugin
maxdepth: 1
---
usage/index
usage/my_new_page
```
````

----

## Bibliography

References are managed with the [bibtex extension for Sphinx](https://sphinxcontrib-bibtex.readthedocs.io/) and stored in the `docs/_static/bibliography/thyrsis.bib` file.

To add a new reference:

1. Update the file
1. Use the syntax to add a new reference:

    ```markdown
    {cite}`gerke-genuchten:1993`
    ```
