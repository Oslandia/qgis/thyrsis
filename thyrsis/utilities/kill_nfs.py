import os
import subprocess


def kill_nfs():
    """Kill any nfs subprocess"""
    thyrsis_dir = os.path.join(
        os.environ["HOME"],
        ".local",
        "share",
        "QGIS",
        "QGIS3",
        "profiles",
        "default",
        "python",
        "plugins",
        "thyrsis",
    )

    for name_dir in ("i18n", "simulation"):
        out = subprocess.Popen(
            ["lsof", "+D", os.path.join(thyrsis_dir, name_dir)],
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
        )

        stdout, stderr = out.communicate()

        for line in stdout.decode("utf-8").split("\n")[1:]:
            # print(len(line), line)
            spl = line.split()
            if len(spl) > 0 and ".nfs" in spl[-1]:
                print("deleting", spl[-1], "in", os.path.join(thyrsis_dir, name_dir))
                subprocess.Popen(["kill", "-9", spl[1]])


if __name__ == "__main__":

    kill_nfs()
