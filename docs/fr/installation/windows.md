# Windows

:::{note}
Ce plugin a été testé avec Windows 10 et QGIS 3.22.
:::

## Exigences

- {{ qgis_version_min }} =< QGIS =< {{ qgis_version_max }}

### Outils de simulation

#### OpenFOAM et porousMultiphaseFoam

1. Installer OpenFOAM:
    - choisir la version depuis l'[arbre de répertoire](https://dl.openfoam.com/source/)
    - [lien direct pour une version 2406](https://dl.openfoam.com/source/v2406/OpenFOAM-v2406-windows-mingw.exe)
2. Installer porousMultiphaseFoam (PMF) avec la même version qu'OpenFOAM (2406) :
    - depuis le [dépôt officiel](https://github.com/phorgue/porousMultiphaseFoam)
    - [lien direct pour une version v2406.2](https://github.com/phorgue/porousMultiphaseFoam/releases/download/v2406.2/pmf-opensuse-mingw-v2406.zip)

:::{note}
La version pré-compilé d'OpenFOAM nécessite l'installation de [MSMPI](https://github.com/microsoft/Microsoft-MPI/releases/download/v10.1.1/msmpisetup.exe). Des privilèges administrateurs sont requis.
:::

:::{important}
Pour une utilisation de CFMESH sous Windows, la version minimale d'OpenFOAM est v2112. Il y a un problème pour l'utilisation de cfMesh avec les versions précédentes
::::

#### Metis

> TO DOC

### Autres

- Installer Gmsh (version 4.9.*): télécharger depuis [le site officiel](https://gmsh.info/).

### Paquets Python

- Python 3.9 (strictement)

Le plugin utilise en grande partie des paquets python externes. Il est assez compliqué d'installer ces paquets pour QGIS sous Windows, car QGIS utilise son propre interpreter Python ce qui rend difficile l'utilisation d'un gestionnaire de paquet (`pip`).

Pour faciliter l'installation pour les utilisateurs Windows, nous avons fait tout notre possible pour intégrer les dépendances dans le paquet généré (durant l'intégration). Elles sont disponibles dans le répertoire `embedded_external_libs`.

Techniquement, le plugin va tenter des imports de différentes façons :

1. importer les paquets depuis l'interpreter Python utilisé par QGIS (celui du système sous Linux, une version spécifique sous Windows)
1. si l'import échoue, le répertoire `embedded_external_libs` est ajouté au `PYTHONPATH` and un nouvel import est tenté
1. si l'import échoue à nouveau, le plugin et désactiver et l'utilisateur est averti avec un bouton lui indiquant :

![Dependencies missing](/_static/images/dependencies_missing_warning.png)

**MAIS** il y a certains inconvénients, car ces paquets doivent absolument avoir été avec la même version de Python que celle utilisée par QGIS et parfois la version de python est mise à jour lors de la livraison d'une nouvelle version de QGIS...

#### Installation manuelle

Si le plugin n'arrive pas à importer un des paquets python, vous pouvez essayer de les installer manuellement avec la commande `pip`.

> Testé sur Windows 10.0.19044.

1. Lancer le `Shell OSGeo4W`. Effectuer une recherche via Windows Search ou directement depuis votre répertoire d'installation de QGIS :
    - installé avec l'installeur _complet_ `.msi`: `C:\Program Files\QGIS`
    - installed avec OSGeo4W et les paramètres par défaut : `C:\OSGeo4W`
1. Lancer (exemple avec une installation QGIS LTR):

  ```batch
  python-qgis-ltr -m pip install -U pip
  python-qgis-ltr -m pip install -U setuptools wheel
  python-qgis-ltr -m pip install -U "mpi4py==3.1.3" "rtree==0.9.7" "portalocker==2.3.2"
  ```

:::{important}
**N'oubliez pas** de regarder le fichier [`requirements/embedded.txt`](https://gitlab.com/Oslandia/qgis/thyrsis/-/blob/master/requirements/embedded.txt) pour vérifier qu'aucun paquet n'est manquant et que les bonnes versions sont utilisées. La commande précédente n'est qu'un exemple.
:::
