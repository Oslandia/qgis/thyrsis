"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.core import (
    QgsProcessingAlgorithm,
    QgsProcessingParameterEnum,
    QgsProcessingParameterFile,
)
from qgis.PyQt.QtCore import QCoreApplication

from ..database import sqlite
from ..database.load_points import load_points
from ..log import logger
from ..settings import Settings


class LoadPointsAlgorithm(QgsProcessingAlgorithm):
    INPUT_CSV = "INPUT_CSV"
    INPUT_SITESDB = "INPUT_SITESDB"
    INPUT_SITENAME = "INPUT_SITENAME"
    sites_list = []

    def tr(self, string):
        return QCoreApplication.translate(
            "Imports the coordinates of new points of interest from a .csv file with format (x,y,name[,srid=4326[,groupe=calcul]]) or (x;y;name[;srid=4326[;groupe=calcul]])",
            string,
        )

    def createInstance(self):
        return LoadPointsAlgorithm()

    def name(self):
        return "load_points"

    def displayName(self):
        return self.tr("Load points")

    def group(self):
        return self.tr("")

    def groupId(self):
        return ""

    def shortHelpString(self):
        return self.tr(
            "Imports the coordinates of new points of interest from a .csv file with format (x,y,name[,srid=4326[,groupe=calcul]]) or (x;y;name[;srid=4326[;groupe=calcul]])"
        )

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_CSV,
                self.tr("Enter .csv file to import"),
                QgsProcessingParameterFile.File,
                "csv",
            )
        )

        sites_db = "sites.sqlite"
        if Settings().contains("General", "defaultSite"):
            sites_db = str(Settings().value("General", "defaultSite"))

        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT_SITESDB,
                self.tr("Enter sites.sqlite database location"),
                QgsProcessingParameterFile.File,
                "sqlite",
                defaultValue=sites_db,
            )
        )

        self.sites_list = sorted(
            [
                x[0]
                for x in sqlite.connect(sites_db)
                .execute("select nom from sites")
                .fetchall()
            ]
        )
        self.addParameter(
            QgsProcessingParameterEnum(
                self.INPUT_SITENAME,
                self.tr("Select site name"),
                self.sites_list,
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        input_csv_file = self.parameterAsFile(parameters, self.INPUT_CSV, context)
        input_sitesdb = self.parameterAsFile(parameters, self.INPUT_SITESDB, context)

        input_sitenumber = self.parameterAsEnum(
            parameters, self.INPUT_SITENAME, context
        )
        input_sitename = self.sites_list[input_sitenumber]

        logger.enable_console(True)
        logger.set_processing_feedback(feedback)

        conn = sqlite.connect(input_sitesdb)

        load_points(conn.cursor(), input_sitename, input_csv_file)
        conn.commit()

        logger.disable_processing_feedback()

        return {}
